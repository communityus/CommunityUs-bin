﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void DelegateList`1::.ctor(System.Func`2<System.Action`1<T>,System.Collections.Generic.LinkedListNode`1<System.Action`1<T>>>,System.Action`1<System.Collections.Generic.LinkedListNode`1<System.Action`1<T>>>)
// 0x00000002 System.Int32 DelegateList`1::get_Count()
// 0x00000003 System.Void DelegateList`1::Add(System.Action`1<T>)
// 0x00000004 System.Void DelegateList`1::Remove(System.Action`1<T>)
// 0x00000005 System.Void DelegateList`1::Invoke(T)
// 0x00000006 System.Void DelegateList`1::Clear()
// 0x00000007 DelegateList`1<T> DelegateList`1::CreateWithGlobalCache()
// 0x00000008 System.Void ListWithEvents`1::add_OnElementAdded(System.Action`1<T>)
// 0x00000009 System.Void ListWithEvents`1::remove_OnElementAdded(System.Action`1<T>)
// 0x0000000A System.Void ListWithEvents`1::add_OnElementRemoved(System.Action`1<T>)
// 0x0000000B System.Void ListWithEvents`1::remove_OnElementRemoved(System.Action`1<T>)
// 0x0000000C System.Void ListWithEvents`1::InvokeAdded(T)
// 0x0000000D System.Void ListWithEvents`1::InvokeRemoved(T)
// 0x0000000E T ListWithEvents`1::get_Item(System.Int32)
// 0x0000000F System.Void ListWithEvents`1::set_Item(System.Int32,T)
// 0x00000010 System.Int32 ListWithEvents`1::get_Count()
// 0x00000011 System.Boolean ListWithEvents`1::get_IsReadOnly()
// 0x00000012 System.Void ListWithEvents`1::Add(T)
// 0x00000013 System.Void ListWithEvents`1::Clear()
// 0x00000014 System.Boolean ListWithEvents`1::Contains(T)
// 0x00000015 System.Void ListWithEvents`1::CopyTo(T[],System.Int32)
// 0x00000016 System.Collections.Generic.IEnumerator`1<T> ListWithEvents`1::GetEnumerator()
// 0x00000017 System.Int32 ListWithEvents`1::IndexOf(T)
// 0x00000018 System.Void ListWithEvents`1::Insert(System.Int32,T)
// 0x00000019 System.Boolean ListWithEvents`1::Remove(T)
// 0x0000001A System.Void ListWithEvents`1::RemoveAt(System.Int32)
// 0x0000001B System.Collections.IEnumerator ListWithEvents`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000001C System.Void ListWithEvents`1::.ctor()
// 0x0000001D System.Void MonoBehaviourCallbackHooks::add_OnUpdateDelegate(System.Action`1<System.Single>)
extern void MonoBehaviourCallbackHooks_add_OnUpdateDelegate_m71F16EDF13A73B69505988BF80F3BD3C15D657B6 (void);
// 0x0000001E System.Void MonoBehaviourCallbackHooks::remove_OnUpdateDelegate(System.Action`1<System.Single>)
extern void MonoBehaviourCallbackHooks_remove_OnUpdateDelegate_m5A983C728E2EECA2E549DD44203A30BC9EA4488F (void);
// 0x0000001F System.String MonoBehaviourCallbackHooks::GetGameObjectName()
extern void MonoBehaviourCallbackHooks_GetGameObjectName_m00804DAD86A9F70E1F9241F0CE2F8DE141D41D9B (void);
// 0x00000020 System.Void MonoBehaviourCallbackHooks::Update()
extern void MonoBehaviourCallbackHooks_Update_mF0F182D68DFD4098DEB677350B6C87935C23D9E2 (void);
// 0x00000021 System.Void MonoBehaviourCallbackHooks::.ctor()
extern void MonoBehaviourCallbackHooks__ctor_m6DA4F0AF02FEBFF30AA23A97945582D2884EB1C1 (void);
// 0x00000022 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000023 System.Void UnityEngine.ResourceManagement.ChainOperation`2::Execute()
// 0x00000024 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::.ctor()
// 0x00000025 System.String UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::get_DebugName()
// 0x00000026 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000027 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Init(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000028 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Execute()
// 0x00000029 System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::OnWrappedCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x0000002A System.Void UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::Destroy()
// 0x0000002B UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x0000002C System.Single UnityEngine.ResourceManagement.ChainOperationTypelessDepedency`1::get_Progress()
// 0x0000002D System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception> UnityEngine.ResourceManagement.ResourceManager::get_ExceptionHandler()
extern void ResourceManager_get_ExceptionHandler_m7AA5E78ACAE04A2406EBA923EE0CBC2BFE39EB01 (void);
// 0x0000002E System.Void UnityEngine.ResourceManagement.ResourceManager::set_ExceptionHandler(System.Action`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Exception>)
extern void ResourceManager_set_ExceptionHandler_mA6F5EBE9E65F68C8A8610638985E34304FE15BA7 (void);
// 0x0000002F System.Func`2<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.String> UnityEngine.ResourceManagement.ResourceManager::get_InternalIdTransformFunc()
extern void ResourceManager_get_InternalIdTransformFunc_mE09C9ABCDE9E35C821FFBEEF42FCE9FDA8FEB308 (void);
// 0x00000030 System.String UnityEngine.ResourceManagement.ResourceManager::TransformInternalId(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_TransformInternalId_m99309A473F0354A9A003C8269967FB49300DC06C (void);
// 0x00000031 System.Void UnityEngine.ResourceManagement.ResourceManager::AddUpdateReceiver(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_AddUpdateReceiver_m8E85F81F33E25C178B60C465322AADE11C03AA1F (void);
// 0x00000032 System.Void UnityEngine.ResourceManagement.ResourceManager::RemoveUpdateReciever(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_RemoveUpdateReciever_mE9CC3445844CC1E22DDD4815184FD27412E07925 (void);
// 0x00000033 UnityEngine.ResourceManagement.Util.IAllocationStrategy UnityEngine.ResourceManagement.ResourceManager::get_Allocator()
extern void ResourceManager_get_Allocator_mEEC5A006F0FDDB3771E377F1A40C9AE319B5A474 (void);
// 0x00000034 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider> UnityEngine.ResourceManagement.ResourceManager::get_ResourceProviders()
extern void ResourceManager_get_ResourceProviders_m7B8444862B525921DCA90FFF0F89BAE11825677A (void);
// 0x00000035 System.Void UnityEngine.ResourceManagement.ResourceManager::set_CertificateHandlerInstance(UnityEngine.Networking.CertificateHandler)
extern void ResourceManager_set_CertificateHandlerInstance_m0DED0F9FE32208F8C3F3A995E93DE2AC7CFF2F9D (void);
// 0x00000036 System.Void UnityEngine.ResourceManagement.ResourceManager::.ctor(UnityEngine.ResourceManagement.Util.IAllocationStrategy)
extern void ResourceManager__ctor_m528366A74390F0F37D4F8D327B311E131484895F (void);
// 0x00000037 System.Void UnityEngine.ResourceManagement.ResourceManager::OnObjectAdded(System.Object)
extern void ResourceManager_OnObjectAdded_m0A54698AC9BA86E496F1E3BA0DFF1A8DDB00600E (void);
// 0x00000038 System.Void UnityEngine.ResourceManagement.ResourceManager::OnObjectRemoved(System.Object)
extern void ResourceManager_OnObjectRemoved_mFB8B23D34D88A56665DD72B6487456D7B0F73294 (void);
// 0x00000039 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterForCallbacks()
extern void ResourceManager_RegisterForCallbacks_mDFD4D66966A06B27431E6CFA21C62653613C5395 (void);
// 0x0000003A System.Void UnityEngine.ResourceManagement.ResourceManager::ClearDiagnosticCallbacks()
extern void ResourceManager_ClearDiagnosticCallbacks_mC2ACDDB95C7291580AB454B5FEA5D76F5C953A5F (void);
// 0x0000003B System.Void UnityEngine.ResourceManagement.ResourceManager::UnregisterDiagnosticCallback(System.Action`1<UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext>)
extern void ResourceManager_UnregisterDiagnosticCallback_m94060E96C7D51B594484713FB4DE87A9B0A13454 (void);
// 0x0000003C System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterDiagnosticCallback(System.Action`1<UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext>)
extern void ResourceManager_RegisterDiagnosticCallback_m7C36E880E0E8C7924E0B74A6769D3BFBD76BD65C (void);
// 0x0000003D System.Void UnityEngine.ResourceManagement.ResourceManager::PostDiagnosticEvent(UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext)
extern void ResourceManager_PostDiagnosticEvent_m774EFC2B6BC9364BCF3B242FD3115BD3F94EA6BE (void);
// 0x0000003E UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider UnityEngine.ResourceManagement.ResourceManager::GetResourceProvider(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_GetResourceProvider_m38873363AA6A66617312E8F2ECE4277BA17B24A3 (void);
// 0x0000003F System.Type UnityEngine.ResourceManagement.ResourceManager::GetDefaultTypeForLocation(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceManager_GetDefaultTypeForLocation_mEB119E0ACD2ECD658112DCE606E3BD8B74B8C3A2 (void);
// 0x00000040 System.Int32 UnityEngine.ResourceManagement.ResourceManager::CalculateLocationsHash(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Type)
extern void ResourceManager_CalculateLocationsHash_m9BE52AD42B5E6FB2082A455391477A87AF4A18AE (void);
// 0x00000041 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager::ProvideResource(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,System.Type)
extern void ResourceManager_ProvideResource_m594F8B2A6122F7744DBCCE7316450F302D6AB7C2 (void);
// 0x00000042 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::ProvideResource(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x00000043 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::StartOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000044 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager::StartOperation(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_StartOperation_mF350C51B1D49B0A1900C7B955595E50A560DE47A (void);
// 0x00000045 System.Void UnityEngine.ResourceManagement.ResourceManager::OnInstanceOperationDestroy(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnInstanceOperationDestroy_m0519061477A75DB2BF0F3F8A18CB34F25D908E9F (void);
// 0x00000046 System.Void UnityEngine.ResourceManagement.ResourceManager::OnOperationDestroyNonCached(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnOperationDestroyNonCached_m98C9B9996EE251C99419A7C6D4AC674C41B487E7 (void);
// 0x00000047 System.Void UnityEngine.ResourceManagement.ResourceManager::OnOperationDestroyCached(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_OnOperationDestroyCached_mB4AFABDBC148F73495D45930E03AE05055ACABCC (void);
// 0x00000048 T UnityEngine.ResourceManagement.ResourceManager::CreateOperation(System.Type,System.Int32,System.Int32,System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x00000049 System.Void UnityEngine.ResourceManagement.ResourceManager::AddOperationToCache(System.Int32,UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void ResourceManager_AddOperationToCache_mF821E49A9CAE2772E817ECB95460480AC32AFC23 (void);
// 0x0000004A System.Boolean UnityEngine.ResourceManagement.ResourceManager::RemoveOperationFromCache(System.Int32)
extern void ResourceManager_RemoveOperationFromCache_m42791732D849ED8CB5B858AF11CA1A0D3ABA6590 (void);
// 0x0000004B System.Boolean UnityEngine.ResourceManagement.ResourceManager::IsOperationCached(System.Int32)
extern void ResourceManager_IsOperationCached_m83330F9DD03DE37659319CC3EE6B607B49EEBE63 (void);
// 0x0000004C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateCompletedOperation(TObject,System.String)
// 0x0000004D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateCompletedOperation(TObject,System.Boolean,System.String)
// 0x0000004E System.Void UnityEngine.ResourceManagement.ResourceManager::Release(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void ResourceManager_Release_mD67A0632D3D7D0B7F97841BC1E4C7CF2FC191C57 (void);
// 0x0000004F UnityEngine.ResourceManagement.AsyncOperations.GroupOperation UnityEngine.ResourceManagement.ResourceManager::AcquireGroupOpFromCache(System.Int32)
extern void ResourceManager_AcquireGroupOpFromCache_m5782A7CFB719A51E06134911422E3C5F9567B355 (void);
// 0x00000050 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::CreateGenericGroupOperation(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean)
extern void ResourceManager_CreateGenericGroupOperation_m148289B454ABDC6D18F9BA386EFEE2C4FFA640F4 (void);
// 0x00000051 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>> UnityEngine.ResourceManagement.ResourceManager::ProvideResourceGroupCached(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Int32,System.Type,System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean)
extern void ResourceManager_ProvideResourceGroupCached_mDD72A490BA7A3DF7BAFD0F29EA499BBFDD71910A (void);
// 0x00000052 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager::ProvideResources(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>,System.Boolean,System.Action`1<TObject>)
// 0x00000053 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObjectDependency>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000054 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.ResourceManager::CreateChainOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,System.Func`2<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000055 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceManager::ReleaseScene(UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
extern void ResourceManager_ReleaseScene_mBD5161D48EFD7ACAD02CB9A12378CAE0EA8A828E (void);
// 0x00000056 System.Void UnityEngine.ResourceManagement.ResourceManager::CleanupSceneInstances(UnityEngine.SceneManagement.Scene)
extern void ResourceManager_CleanupSceneInstances_m2BBE6BCBFBA7DC2CEA5D38DE680FA6A52B0EA528 (void);
// 0x00000057 System.Void UnityEngine.ResourceManagement.ResourceManager::ExecuteDeferredCallbacks()
extern void ResourceManager_ExecuteDeferredCallbacks_m75C200EC542CDBCEB07DBD4720835323A34D144F (void);
// 0x00000058 System.Void UnityEngine.ResourceManagement.ResourceManager::RegisterForDeferredCallback(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Boolean)
extern void ResourceManager_RegisterForDeferredCallback_m0436E85EDF6A341FEE22C1C7A360EC0643042272 (void);
// 0x00000059 System.Void UnityEngine.ResourceManagement.ResourceManager::Update(System.Single)
extern void ResourceManager_Update_m1038C44C2B864801F1D40DC503AA7F1029AAAF70 (void);
// 0x0000005A System.Void UnityEngine.ResourceManagement.ResourceManager::Dispose()
extern void ResourceManager_Dispose_m90EE4D872BF46DE7D69ED70BD540A2B46064D0B8 (void);
// 0x0000005B System.Void UnityEngine.ResourceManagement.ResourceManager::.cctor()
extern void ResourceManager__cctor_m9E01A63275781A7065E780992DFD1A0269EE76CB (void);
// 0x0000005C System.Void UnityEngine.ResourceManagement.ResourceManager::<.ctor>b__45_0(UnityEngine.ResourceManagement.IUpdateReceiver)
extern void ResourceManager_U3C_ctorU3Eb__45_0_mCECD009288795FC98DBCE498BA37927C1AD49273 (void);
// 0x0000005D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_OperationHandle()
extern void DiagnosticEventContext_get_OperationHandle_m6884404709298E7C22B1F7F8205C1B7E8796EE05_AdjustorThunk (void);
// 0x0000005E UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventType UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_Type()
extern void DiagnosticEventContext_get_Type_mC90B555707F6837DA6A22722EF4353065334C905_AdjustorThunk (void);
// 0x0000005F System.Int32 UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_EventValue()
extern void DiagnosticEventContext_get_EventValue_mCD0F3458A7BCBE06550729AC816B34E5E19E14EF_AdjustorThunk (void);
// 0x00000060 System.Object UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_Context()
extern void DiagnosticEventContext_get_Context_m4B8F30E7A090EB33FE0C1F8720C40273B3859C04_AdjustorThunk (void);
// 0x00000061 System.String UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::get_Error()
extern void DiagnosticEventContext_get_Error_m1CF5543DF39540144965910BC6FFF71862753E60_AdjustorThunk (void);
// 0x00000062 System.Void UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventContext::.ctor(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,UnityEngine.ResourceManagement.ResourceManager_DiagnosticEventType,System.Int32,System.String,System.Object)
extern void DiagnosticEventContext__ctor_mC21627E8722C2E92B119E731AA890319A131FECA_AdjustorThunk (void);
// 0x00000063 System.Void UnityEngine.ResourceManagement.ResourceManager_CompletedOperation`1::Init(TObject,System.Boolean,System.String)
// 0x00000064 System.Void UnityEngine.ResourceManagement.ResourceManager_CompletedOperation`1::Execute()
// 0x00000065 UnityEngine.SceneManagement.Scene UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::InstanceScene()
extern void InstanceOperation_InstanceScene_mAC44625C564DE76E956CA8BCF4EC2C9F6178D9DE (void);
// 0x00000066 System.Void UnityEngine.ResourceManagement.ResourceManager_InstanceOperation::Execute()
extern void InstanceOperation_Execute_m8792B88CB3F74349E1FD1D35E9426D58894F19D3 (void);
// 0x00000067 System.Void UnityEngine.ResourceManagement.ResourceManager_<>c__DisplayClass82_0`1::.ctor()
// 0x00000068 System.Void UnityEngine.ResourceManagement.ResourceManager_<>c__DisplayClass82_0`1::<ProvideResources>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000069 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TObject>> UnityEngine.ResourceManagement.ResourceManager_<>c__DisplayClass82_0`1::<ProvideResources>b__1(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x0000006A System.Void UnityEngine.ResourceManagement.IUpdateReceiver::Update(System.Single)
// 0x0000006B System.Boolean UnityEngine.ResourceManagement.WebRequestQueueOperation::get_IsDone()
extern void WebRequestQueueOperation_get_IsDone_mD60F4EB09496B7A2F86C696815E34491375CC9D0 (void);
// 0x0000006C System.Void UnityEngine.ResourceManagement.WebRequestQueueOperation::.ctor(UnityEngine.Networking.UnityWebRequest)
extern void WebRequestQueueOperation__ctor_m68CB315ACE251B662B8A4A603751432C8048F38A (void);
// 0x0000006D System.Void UnityEngine.ResourceManagement.WebRequestQueueOperation::Complete(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void WebRequestQueueOperation_Complete_m045307756A8EB68F5ADC3E6FF182361C60AABBF8 (void);
// 0x0000006E System.Void UnityEngine.ResourceManagement.WebRequestQueue::SetMaxConcurrentRequests(System.Int32)
extern void WebRequestQueue_SetMaxConcurrentRequests_m20043AB0BAC3523B55EB84AE01E4CA67F03CB936 (void);
// 0x0000006F UnityEngine.ResourceManagement.WebRequestQueueOperation UnityEngine.ResourceManagement.WebRequestQueue::QueueRequest(UnityEngine.Networking.UnityWebRequest)
extern void WebRequestQueue_QueueRequest_m9E882E5334EC6CB6FB517C1A0DF985B83779B78F (void);
// 0x00000070 System.Void UnityEngine.ResourceManagement.WebRequestQueue::OnWebAsyncOpComplete(UnityEngine.AsyncOperation)
extern void WebRequestQueue_OnWebAsyncOpComplete_mF0B3325CA9B1386C3DAB80E1D87C4C5DB16F624C (void);
// 0x00000071 System.Void UnityEngine.ResourceManagement.WebRequestQueue::.cctor()
extern void WebRequestQueue__cctor_m387136624EFB374B41B0EEE1A63506F97F28D9B6 (void);
// 0x00000072 System.Void UnityEngine.ResourceManagement.Exceptions.ResourceManagerException::.ctor()
extern void ResourceManagerException__ctor_mAEDC8C69DA13800EC322613C6829018DBB1E07CB (void);
// 0x00000073 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::get_Location()
extern void UnknownResourceProviderException_get_Location_mB274BAF53E19FE874A24CC67C522D494CCC8F4F0 (void);
// 0x00000074 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::set_Location(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void UnknownResourceProviderException_set_Location_mF93EB4AAA41E957EFFACCEA94DFF45AEC67EB619 (void);
// 0x00000075 System.Void UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::.ctor(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void UnknownResourceProviderException__ctor_mB1E23AB4989DE1993682D165DD042F52E042AAE8 (void);
// 0x00000076 System.String UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::get_Message()
extern void UnknownResourceProviderException_get_Message_mC59322E40728F5119EB36A38F38BA109AFA55E28 (void);
// 0x00000077 System.String UnityEngine.ResourceManagement.Exceptions.UnknownResourceProviderException::ToString()
extern void UnknownResourceProviderException_ToString_mE320AC807B6D64A5AAD350EB35DFB6781B571648 (void);
// 0x00000078 System.Boolean UnityEngine.ResourceManagement.Util.ComponentSingleton`1::get_Exists()
// 0x00000079 T UnityEngine.ResourceManagement.Util.ComponentSingleton`1::get_Instance()
// 0x0000007A T UnityEngine.ResourceManagement.Util.ComponentSingleton`1::FindInstance()
// 0x0000007B System.String UnityEngine.ResourceManagement.Util.ComponentSingleton`1::GetGameObjectName()
// 0x0000007C T UnityEngine.ResourceManagement.Util.ComponentSingleton`1::CreateNewSingleton()
// 0x0000007D System.Void UnityEngine.ResourceManagement.Util.ComponentSingleton`1::Awake()
// 0x0000007E System.Void UnityEngine.ResourceManagement.Util.ComponentSingleton`1::DestroySingleton()
// 0x0000007F System.Void UnityEngine.ResourceManagement.Util.ComponentSingleton`1::.ctor()
// 0x00000080 System.Boolean UnityEngine.ResourceManagement.Util.IInitializableObject::Initialize(System.String,System.String)
// 0x00000081 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.ResourceManagement.Util.IInitializableObject::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
// 0x00000082 System.Object UnityEngine.ResourceManagement.Util.IAllocationStrategy::New(System.Type,System.Int32)
// 0x00000083 System.Void UnityEngine.ResourceManagement.Util.IAllocationStrategy::Release(System.Int32,System.Object)
// 0x00000084 System.Object UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::New(System.Type,System.Int32)
extern void DefaultAllocationStrategy_New_mB78CC5AFBF1B07035814191AB926E4ACBD41A6CE (void);
// 0x00000085 System.Void UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::Release(System.Int32,System.Object)
extern void DefaultAllocationStrategy_Release_mD958EA6925889742484435B1CD1AAF4087F60E4A (void);
// 0x00000086 System.Void UnityEngine.ResourceManagement.Util.DefaultAllocationStrategy::.ctor()
extern void DefaultAllocationStrategy__ctor_mC395409A503C0199396C35AA3B939CE682AB3C8A (void);
// 0x00000087 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void LRUCacheAllocationStrategy__ctor_m1DE9B78742C8D5457040C65AFA15854983E33FC6 (void);
// 0x00000088 System.Collections.Generic.List`1<System.Object> UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::GetPool()
extern void LRUCacheAllocationStrategy_GetPool_mFC7368E7F5D84E6CFDCC693520C2B2B6965627A8 (void);
// 0x00000089 System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::ReleasePool(System.Collections.Generic.List`1<System.Object>)
extern void LRUCacheAllocationStrategy_ReleasePool_m72D0EB4F039EE69BDB8BBB68AFB8BA7D62B92BCF (void);
// 0x0000008A System.Object UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::New(System.Type,System.Int32)
extern void LRUCacheAllocationStrategy_New_m9A93EA16BC5C9C0F1933D6A19C8C19F854F9D2E3 (void);
// 0x0000008B System.Void UnityEngine.ResourceManagement.Util.LRUCacheAllocationStrategy::Release(System.Int32,System.Object)
extern void LRUCacheAllocationStrategy_Release_m42CAEAF3890309E4396717B9CE28D17082BA6300 (void);
// 0x0000008C System.Collections.Generic.LinkedListNode`1<T> UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::Acquire(T)
// 0x0000008D System.Void UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::Release(System.Collections.Generic.LinkedListNode`1<T>)
// 0x0000008E System.Void UnityEngine.ResourceManagement.Util.LinkedListNodeCache`1::.ctor()
// 0x0000008F System.Collections.Generic.LinkedListNode`1<T> UnityEngine.ResourceManagement.Util.GlobalLinkedListNodeCache`1::Acquire(T)
// 0x00000090 System.Void UnityEngine.ResourceManagement.Util.GlobalLinkedListNodeCache`1::Release(System.Collections.Generic.LinkedListNode`1<T>)
// 0x00000091 System.String UnityEngine.ResourceManagement.Util.SerializedType::ToString()
extern void SerializedType_ToString_m1148F8BA12B705068191BD22F924E4570E0CF2BF_AdjustorThunk (void);
// 0x00000092 System.Type UnityEngine.ResourceManagement.Util.SerializedType::get_Value()
extern void SerializedType_get_Value_m1BC4883C0CA3F911AC298F99BA3820D28B116AE4_AdjustorThunk (void);
// 0x00000093 System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_Id()
extern void ObjectInitializationData_get_Id_mEC5373B1DDFE78FB72F291806ED9C869DA2B27B5_AdjustorThunk (void);
// 0x00000094 UnityEngine.ResourceManagement.Util.SerializedType UnityEngine.ResourceManagement.Util.ObjectInitializationData::get_ObjectType()
extern void ObjectInitializationData_get_ObjectType_m14F9142B5C61D1F8285BA84C34B2747EBDE59DD0_AdjustorThunk (void);
// 0x00000095 System.String UnityEngine.ResourceManagement.Util.ObjectInitializationData::ToString()
extern void ObjectInitializationData_ToString_mA1CB0E03EDC5E862A576AC96DF6E768E018BE8EF_AdjustorThunk (void);
// 0x00000096 TObject UnityEngine.ResourceManagement.Util.ObjectInitializationData::CreateInstance(System.String)
// 0x00000097 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.Util.ObjectInitializationData::GetAsyncInitHandle(UnityEngine.ResourceManagement.ResourceManager,System.String)
extern void ObjectInitializationData_GetAsyncInitHandle_m1A3A5DA8AD774DC20CC561528A6305CC4F5999A6_AdjustorThunk (void);
// 0x00000098 System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::ExtractKeyAndSubKey(System.Object,System.String&,System.String&)
extern void ResourceManagerConfig_ExtractKeyAndSubKey_m10E4749CC55275EB25BB3341FF729050BEFCABB0 (void);
// 0x00000099 System.Boolean UnityEngine.ResourceManagement.Util.ResourceManagerConfig::ShouldPathUseWebRequest(System.String)
extern void ResourceManagerConfig_ShouldPathUseWebRequest_m20ED59B08CD29D311DE947B60B3020B6447EA19A (void);
// 0x0000009A System.Void UnityEngine.ResourceManagement.ResourceProviders.AtlasSpriteProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void AtlasSpriteProvider_Provide_m1EEB6E65B16F76C067088BEAF5CD12D9E040BA8E (void);
// 0x0000009B UnityEngine.GameObject UnityEngine.ResourceManagement.ResourceProviders.IInstanceProvider::ProvideInstance(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.GameObject>,UnityEngine.ResourceManagement.ResourceProviders.InstantiationParameters)
// 0x0000009C System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::.ctor(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation)
extern void ProvideHandle__ctor_m6162616DB274E0DF650DB36B4ECD1AB8FF7FF0AA_AdjustorThunk (void);
// 0x0000009D UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_InternalOp()
extern void ProvideHandle_get_InternalOp_mBAFE92C7761F1994A9AC5542BC7085AE67C57051_AdjustorThunk (void);
// 0x0000009E UnityEngine.ResourceManagement.ResourceManager UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_ResourceManager()
extern void ProvideHandle_get_ResourceManager_m05D4E79DEBFDD6BD466CD6A2219958B00C6C3E8E_AdjustorThunk (void);
// 0x0000009F System.Type UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_Type()
extern void ProvideHandle_get_Type_m9569FAA40332A3EFD53227FABC324CC03B4BAD3D_AdjustorThunk (void);
// 0x000000A0 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::get_Location()
extern void ProvideHandle_get_Location_m5F7540397789E7214F8B9DD88C8E9B8A7A6ABE53_AdjustorThunk (void);
// 0x000000A1 TDepObject UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::GetDependency(System.Int32)
// 0x000000A2 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
extern void ProvideHandle_GetDependencies_m50B8C3466AED683B0F8581207B09D887350CB741_AdjustorThunk (void);
// 0x000000A3 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::SetProgressCallback(System.Func`1<System.Single>)
extern void ProvideHandle_SetProgressCallback_mCADBA6D92915BE6956E55E25A84F9A5E61BD289F_AdjustorThunk (void);
// 0x000000A4 System.Void UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle::Complete(T,System.Boolean,System.Exception)
// 0x000000A5 System.String UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::get_ProviderId()
// 0x000000A6 System.Type UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x000000A7 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::CanProvide(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
// 0x000000A8 System.Void UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
// 0x000000A9 UnityEngine.ResourceManagement.ResourceProviders.ProviderBehaviourFlags UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider::get_BehaviourFlags()
// 0x000000AA UnityEngine.SceneManagement.Scene UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::get_Scene()
extern void SceneInstance_get_Scene_m941C7B9506F1E8C45785CD5F6AE03DF8939F3ABE_AdjustorThunk (void);
// 0x000000AB System.Int32 UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::GetHashCode()
extern void SceneInstance_GetHashCode_m1D0926F7DDB54F2D00B59B84CE8E3D25BB225204_AdjustorThunk (void);
// 0x000000AC System.Boolean UnityEngine.ResourceManagement.ResourceProviders.SceneInstance::Equals(System.Object)
extern void SceneInstance_Equals_mD0482B6D8FF701E433A23F4047E02634128BF0AF_AdjustorThunk (void);
// 0x000000AD UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance> UnityEngine.ResourceManagement.ResourceProviders.ISceneProvider::ReleaseScene(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.ResourceManagement.ResourceProviders.SceneInstance>)
// 0x000000AE System.Object UnityEngine.ResourceManagement.ResourceProviders.JsonAssetProvider::Convert(System.Type,System.String)
extern void JsonAssetProvider_Convert_mD40A593AF7815BA0370BF56680B7BDB3244C68F8 (void);
// 0x000000AF System.Void UnityEngine.ResourceManagement.ResourceProviders.JsonAssetProvider::.ctor()
extern void JsonAssetProvider__ctor_mFAF69C1316EEF5EC237555CF441B02D5BAEA662B (void);
// 0x000000B0 System.String UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::get_ProviderId()
extern void ResourceProviderBase_get_ProviderId_m5860108653362B09CF615C607BF9234FD97DC11D (void);
// 0x000000B1 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Initialize(System.String,System.String)
extern void ResourceProviderBase_Initialize_m34E104D3A5D94257BE270AF01B9B58593EB2A83B (void);
// 0x000000B2 System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::CanProvide(System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceProviderBase_CanProvide_mD9D178400A79CEF5294BFAE7B6079BAFA3EF653A (void);
// 0x000000B3 System.String UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::ToString()
extern void ResourceProviderBase_ToString_m80D7CDE36BB26C8DE21290352CAF1A2C1D753D7C (void);
// 0x000000B4 System.Type UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::GetDefaultType(UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation)
extern void ResourceProviderBase_GetDefaultType_m2156E5A367DDA36A48496BDD01E01C4985700F57 (void);
// 0x000000B5 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
// 0x000000B6 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Boolean> UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::InitializeAsync(UnityEngine.ResourceManagement.ResourceManager,System.String,System.String)
extern void ResourceProviderBase_InitializeAsync_m80430AFAB1D2335A508216D5A110CB736F6171EA (void);
// 0x000000B7 UnityEngine.ResourceManagement.ResourceProviders.ProviderBehaviourFlags UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider.get_BehaviourFlags()
extern void ResourceProviderBase_UnityEngine_ResourceManagement_ResourceProviders_IResourceProvider_get_BehaviourFlags_mD7856EDEB725FA84A89DAD3F529B9D3FC656EEC6 (void);
// 0x000000B8 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase::.ctor()
extern void ResourceProviderBase__ctor_m7FCD7D94546A446A53C78AF135940F4F563DFAC7 (void);
// 0x000000B9 System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_BaseInitAsyncOp::Init(System.Func`1<System.Boolean>)
extern void BaseInitAsyncOp_Init_m50C6E027B3900B3DBCE301C140DE82F74CCE38D2 (void);
// 0x000000BA System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_BaseInitAsyncOp::Execute()
extern void BaseInitAsyncOp_Execute_mC12E09D151DA1D4D838FCB2E38B58F824AEB0E3F (void);
// 0x000000BB System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_BaseInitAsyncOp::.ctor()
extern void BaseInitAsyncOp__ctor_mE020A405B8D14A3269982E70A9C421A717A92FE3 (void);
// 0x000000BC System.Void UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mB1D7DDAE2A506B4A834A35D720C18095BA4653C3 (void);
// 0x000000BD System.Boolean UnityEngine.ResourceManagement.ResourceProviders.ResourceProviderBase_<>c__DisplayClass10_0::<InitializeAsync>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CInitializeAsyncU3Eb__0_m3D6B8698FD2339D3145463DFEB3BB21BD687E747 (void);
// 0x000000BE System.Boolean UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::get_IgnoreFailures()
extern void TextDataProvider_get_IgnoreFailures_m54B8CF389A2EF6D0C92E97D23541CD730CE45851 (void);
// 0x000000BF System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::set_IgnoreFailures(System.Boolean)
extern void TextDataProvider_set_IgnoreFailures_m42340E49DD6BFBEE98EA6D24A0FB5B0673372A74 (void);
// 0x000000C0 System.Object UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::Convert(System.Type,System.String)
extern void TextDataProvider_Convert_m2B991FF6B1CB9CD871439B6F5089BE5AE6DF2518 (void);
// 0x000000C1 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::Provide(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle)
extern void TextDataProvider_Provide_mF3742F8E9A17003FF5D555E4022419B9EE0F8989 (void);
// 0x000000C2 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider::.ctor()
extern void TextDataProvider__ctor_m7AD4D795A1480E3CEC6DCDD478A37FD51E79812D (void);
// 0x000000C3 System.Single UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::GetPercentComplete()
extern void InternalOp_GetPercentComplete_m7AC5C9BEC338F6C89783E233E8C532057E62BD2A (void);
// 0x000000C4 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::Start(UnityEngine.ResourceManagement.ResourceProviders.ProvideHandle,UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider,System.Boolean)
extern void InternalOp_Start_m2A6D3C16C95797FC3162CC981CFA28C4128E35E2 (void);
// 0x000000C5 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::RequestOperation_completed(UnityEngine.AsyncOperation)
extern void InternalOp_RequestOperation_completed_m587EACB3BF4B624F485E87D20E661A8F0192BADE (void);
// 0x000000C6 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::.ctor()
extern void InternalOp__ctor_mA96AC18D8B782C4508EB024210E260A8E899753E (void);
// 0x000000C7 System.Void UnityEngine.ResourceManagement.ResourceProviders.TextDataProvider_InternalOp::<Start>b__6_0(UnityEngine.Networking.UnityWebRequestAsyncOperation)
extern void InternalOp_U3CStartU3Eb__6_0_mBDCB8E098F9284FC7C3A90C6461A8C107E159F4A (void);
// 0x000000C8 System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_InternalId()
// 0x000000C9 System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_ProviderId()
// 0x000000CA System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_Dependencies()
// 0x000000CB System.Int32 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::Hash(System.Type)
// 0x000000CC System.Int32 UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_DependencyHashCode()
// 0x000000CD System.Boolean UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_HasDependencies()
// 0x000000CE System.String UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_PrimaryKey()
// 0x000000CF System.Type UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation::get_ResourceType()
// 0x000000D0 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_InternalId()
extern void ResourceLocationBase_get_InternalId_m446E746FAE3B40B1CDA9A588A319DDBDB1453ABB (void);
// 0x000000D1 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_ProviderId()
extern void ResourceLocationBase_get_ProviderId_mEE3324D2E4071C922810A050C10AFF5DDCA3EAA3 (void);
// 0x000000D2 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation> UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_Dependencies()
extern void ResourceLocationBase_get_Dependencies_m054FEB15A19CB1591D9E11587725EAD00100F3EB (void);
// 0x000000D3 System.Boolean UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_HasDependencies()
extern void ResourceLocationBase_get_HasDependencies_m25C0F08130902EFEAEF8DDD99D75073BD7377272 (void);
// 0x000000D4 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_PrimaryKey()
extern void ResourceLocationBase_get_PrimaryKey_mAA5E2116F41249FE94A19BFB0E102605B8941B24 (void);
// 0x000000D5 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_DependencyHashCode()
extern void ResourceLocationBase_get_DependencyHashCode_m1CF137823A47AE4C1EC0503AA257AB7CCDE88CEB (void);
// 0x000000D6 System.Type UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::get_ResourceType()
extern void ResourceLocationBase_get_ResourceType_m8621BD5A50D22E8F3C5933811823DB63034A62FD (void);
// 0x000000D7 System.Int32 UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::Hash(System.Type)
extern void ResourceLocationBase_Hash_mF2BC6233AC5F1F90E1AC29823D2F0CFB639374D4 (void);
// 0x000000D8 System.String UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::ToString()
extern void ResourceLocationBase_ToString_m828E1E56F19E2A35DF72ED842192DABE7386A9A0 (void);
// 0x000000D9 System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::.ctor(System.String,System.String,System.String,System.Type,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation[])
extern void ResourceLocationBase__ctor_m9F01B99ECB49EFF4B75BF41E169FBFEF707809AA (void);
// 0x000000DA System.Void UnityEngine.ResourceManagement.ResourceLocations.ResourceLocationBase::ComputeDependencyHash()
extern void ResourceLocationBase_ComputeDependencyHash_mB8A54AB1782AFCE00F30B2C22D0048B5006DBF52 (void);
// 0x000000DB System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_ObjectId()
extern void DiagnosticEvent_get_ObjectId_m1CCBE7BC50003514247D99544A60BD22DF70596D_AdjustorThunk (void);
// 0x000000DC System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Stream()
extern void DiagnosticEvent_get_Stream_m2811009448086191841A0EBAC768BD8DEDCB17E7_AdjustorThunk (void);
// 0x000000DD System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::get_Frame()
extern void DiagnosticEvent_get_Frame_mE28A9CF94A945044C148F653F95C4142ECA221B1_AdjustorThunk (void);
// 0x000000DE System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::.ctor(System.String,System.String,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32[])
extern void DiagnosticEvent__ctor_m6A631EBC9C9EA934424340F88116686A83511D9B_AdjustorThunk (void);
// 0x000000DF System.Byte[] UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent::Serialize()
extern void DiagnosticEvent_Serialize_m6255901EC86D38FBAFDE9069AF340608B45205E0_AdjustorThunk (void);
// 0x000000E0 System.Guid UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::get_PlayerConnectionGuid()
extern void DiagnosticEventCollectorSingleton_get_PlayerConnectionGuid_mB1565F345C559996E323B28DF44704E899F529DC (void);
// 0x000000E1 System.String UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::GetGameObjectName()
extern void DiagnosticEventCollectorSingleton_GetGameObjectName_m0147EE9822537F03A29E0101B0E207C608D82DC6 (void);
// 0x000000E2 System.Boolean UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>,System.Boolean,System.Boolean)
extern void DiagnosticEventCollectorSingleton_RegisterEventHandler_mB9A217A031D8D49AF18369AC4BACD572450F2C00 (void);
// 0x000000E3 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::RegisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollectorSingleton_RegisterEventHandler_mD140C18197FB2092B00061F727CE99BF53415FA7 (void);
// 0x000000E4 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::UnregisterEventHandler(System.Action`1<UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent>)
extern void DiagnosticEventCollectorSingleton_UnregisterEventHandler_m4A20D0D0105FD76EF279D864C462BE208F68225F (void);
// 0x000000E5 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::PostEvent(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void DiagnosticEventCollectorSingleton_PostEvent_m68C5FED777CFC48B166D761A798044D9BEE2A140 (void);
// 0x000000E6 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::Awake()
extern void DiagnosticEventCollectorSingleton_Awake_mFA6860EA3E76647FFC0AA0DDBD4DFD372B78B651 (void);
// 0x000000E7 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::Update()
extern void DiagnosticEventCollectorSingleton_Update_mCBB780A57E2F7CCC3C2A7EBAC591F3E63A6A1B5E (void);
// 0x000000E8 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton::.ctor()
extern void DiagnosticEventCollectorSingleton__ctor_mE8886C4DA6922CF87C372E6A6A22F75B32810681 (void);
// 0x000000E9 System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton_<>c::.cctor()
extern void U3CU3Ec__cctor_m3F87D5D65E8E8A85497441B86834CB9813261980 (void);
// 0x000000EA System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton_<>c::.ctor()
extern void U3CU3Ec__ctor_mFEA80F7FDDE93D4A56031F9A6A543EF3B26C9B78 (void);
// 0x000000EB System.Int32 UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton_<>c::<RegisterEventHandler>b__8_0(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void U3CU3Ec_U3CRegisterEventHandlerU3Eb__8_0_mD42E45C5B7C6624A4D689ECC03B702B07B8F8FE6 (void);
// 0x000000EC System.Void UnityEngine.ResourceManagement.Diagnostics.DiagnosticEventCollectorSingleton_<>c::<Awake>b__11_0(UnityEngine.ResourceManagement.Diagnostics.DiagnosticEvent)
extern void U3CU3Ec_U3CAwakeU3Eb__11_0_mE0640D82251431C9F7999D4133CE16BD752D2B44 (void);
// 0x000000ED System.Int32 UnityEngine.ResourceManagement.AsyncOperations.ICachable::get_Hash()
// 0x000000EE System.Void UnityEngine.ResourceManagement.AsyncOperations.ICachable::set_Hash(System.Int32)
// 0x000000EF System.Object UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetResultAsObject()
// 0x000000F0 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Version()
// 0x000000F1 System.String UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_DebugName()
// 0x000000F2 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::DecrementReferenceCount()
// 0x000000F3 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::IncrementReferenceCount()
// 0x000000F4 System.Single UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_PercentComplete()
// 0x000000F5 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x000000F6 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Status()
// 0x000000F7 System.Exception UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_OperationException()
// 0x000000F8 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_IsDone()
// 0x000000F9 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x000000FA System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000000FB System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000000FC System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000000FD System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000000FE System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x000000FF System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::InvokeCompletionEvent()
// 0x00000100 System.Void UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x00000101 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation::get_Handle()
// 0x00000102 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Execute()
// 0x00000103 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Destroy()
// 0x00000104 System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Progress()
// 0x00000105 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_DebugName()
// 0x00000106 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000107 TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Result()
// 0x00000108 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_Result(TObject)
// 0x00000109 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Version()
// 0x0000010A System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x0000010B System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_IsRunning(System.Boolean)
// 0x0000010C System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::.ctor()
// 0x0000010D System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::IncrementReferenceCount()
// 0x0000010E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::DecrementReferenceCount()
// 0x0000010F System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::ToString()
// 0x00000110 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::RegisterForDeferredCallbackEvent(System.Boolean)
// 0x00000111 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000112 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000113 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000114 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000115 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000116 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000117 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Status()
// 0x00000118 System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_OperationException()
// 0x00000119 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::set_OperationException(System.Exception)
// 0x0000011A System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_IsDone()
// 0x0000011B System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_PercentComplete()
// 0x0000011C System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::InvokeCompletionEvent()
// 0x0000011D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::get_Handle()
// 0x0000011E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UpdateCallback(System.Single)
// 0x0000011F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Complete(TObject,System.Boolean,System.String)
// 0x00000120 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Complete(TObject,System.Boolean,System.String,System.Boolean)
// 0x00000121 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x00000122 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::InvokeExecute()
// 0x00000123 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000124 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000125 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000126 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000127 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Version()
// 0x00000128 System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_PercentComplete()
// 0x00000129 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Status()
// 0x0000012A System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_OperationException()
// 0x0000012B System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_IsDone()
// 0x0000012C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_Handle()
// 0x0000012D System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.set_OnDestroy(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation>)
// 0x0000012E System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.get_DebugName()
// 0x0000012F System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetResultAsObject()
// 0x00000130 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000131 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.DecrementReferenceCount()
// 0x00000132 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.IncrementReferenceCount()
// 0x00000133 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.InvokeCompletionEvent()
// 0x00000134 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.Start(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle,DelegateList`1<System.Single>)
// 0x00000135 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation.GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x00000136 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::GetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x00000137 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1::<.ctor>b__37_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x00000138 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::op_Implicit(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000139 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject>)
// 0x0000013A UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::InternalGetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
// 0x0000013B System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32,System.String)
// 0x0000013C UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Acquire()
// 0x0000013D System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x0000013E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x0000013F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::add_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000140 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::remove_CompletedTypeless(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
// 0x00000141 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_DebugName()
// 0x00000142 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Equals(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x00000143 System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::GetHashCode()
// 0x00000144 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationBase`1<TObject> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_InternalOp()
// 0x00000145 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_IsDone()
// 0x00000146 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::IsValid()
// 0x00000147 System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_OperationException()
// 0x00000148 System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_PercentComplete()
// 0x00000149 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::Release()
// 0x0000014A TObject UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Result()
// 0x0000014B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::get_Status()
// 0x0000014C System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.get_Current()
// 0x0000014D System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.MoveNext()
// 0x0000014E System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1::System.Collections.IEnumerator.Reset()
// 0x0000014F System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::set_LocationName(System.String)
extern void AsyncOperationHandle_set_LocationName_m20D2D9A3B36B372F84634EBB47C4080CD6EC6E1F_AdjustorThunk (void);
// 0x00000150 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation)
extern void AsyncOperationHandle__ctor_m4A737468E34CBBCC7DDA85F5A0A5D461DB7F4913_AdjustorThunk (void);
// 0x00000151 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.String)
extern void AsyncOperationHandle__ctor_mE32C8239AFC99079BCC3113FCDEFCF339154ADA1_AdjustorThunk (void);
// 0x00000152 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::.ctor(UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation,System.Int32,System.String)
extern void AsyncOperationHandle__ctor_m8FB8DEFB2E7B3F1BF8D47B5790EAD508D1DCD66A_AdjustorThunk (void);
// 0x00000153 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Acquire()
extern void AsyncOperationHandle_Acquire_mC15C71C7BA8B95C49B8BCCD33CD2F7EB5B03BF7F_AdjustorThunk (void);
// 0x00000154 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::add_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_add_Completed_mBD4A54B7E0322F292C67A62A5461193758726B20_AdjustorThunk (void);
// 0x00000155 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::remove_Completed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_remove_Completed_mFF2A9DFC4D2E2A6D1302BFBC7EA49E01E3E14CEB_AdjustorThunk (void);
// 0x00000156 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<T> UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Convert()
// 0x00000157 System.String UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_DebugName()
extern void AsyncOperationHandle_get_DebugName_m9263AE2FF7206453566D317443292E71686910FE_AdjustorThunk (void);
// 0x00000158 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::add_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_add_Destroyed_mCDA263102547EEC0D53DD5B2B7F55881EC896235_AdjustorThunk (void);
// 0x00000159 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::remove_Destroyed(System.Action`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_remove_Destroyed_m3BE893CECF7F7273A884984B6025867D7AA90FB2_AdjustorThunk (void);
// 0x0000015A System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetDependencies(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AsyncOperationHandle_GetDependencies_mD420A5EA523C940C0F90A216B971E8B6A060F71E_AdjustorThunk (void);
// 0x0000015B System.Int32 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::GetHashCode()
extern void AsyncOperationHandle_GetHashCode_m7764BEBFB86D7C0EB3D46D4AC116ADD56AEEAC68_AdjustorThunk (void);
// 0x0000015C UnityEngine.ResourceManagement.AsyncOperations.IAsyncOperation UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_InternalOp()
extern void AsyncOperationHandle_get_InternalOp_m09CAB010CFD3205ADCB1128F3046132C538B8EFE_AdjustorThunk (void);
// 0x0000015D System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_IsDone()
extern void AsyncOperationHandle_get_IsDone_m7F8233755EDB537A312B42BFAC1B4E95FDEB0B62_AdjustorThunk (void);
// 0x0000015E System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::IsValid()
extern void AsyncOperationHandle_IsValid_mDDEEC690F9BFA3CFA09B8344947F1405EECA22F1_AdjustorThunk (void);
// 0x0000015F System.Exception UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_OperationException()
extern void AsyncOperationHandle_get_OperationException_mE66E9FDDCF99EF8B6D706A05F3156514D552C259_AdjustorThunk (void);
// 0x00000160 System.Single UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_PercentComplete()
extern void AsyncOperationHandle_get_PercentComplete_m56F5B0016C1AE1681A5670F8F50F716F94C4B9C1_AdjustorThunk (void);
// 0x00000161 UnityEngine.ResourceManagement.AsyncOperations.DownloadStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::InternalGetDownloadStatus(System.Collections.Generic.HashSet`1<System.Object>)
extern void AsyncOperationHandle_InternalGetDownloadStatus_m497FB080B6D909604490829B6F594FFCC155E6AC_AdjustorThunk (void);
// 0x00000162 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::Release()
extern void AsyncOperationHandle_Release_m75B6AFC0710FF804ECD0CBC6E7EFB85EEF095F67_AdjustorThunk (void);
// 0x00000163 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Result()
extern void AsyncOperationHandle_get_Result_m692BEC3C25DA9A5BFCD041C069C693B25F783DED_AdjustorThunk (void);
// 0x00000164 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationStatus UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::get_Status()
extern void AsyncOperationHandle_get_Status_m541A1E32905CA1F7092BFBB1299F939271FA57E7_AdjustorThunk (void);
// 0x00000165 System.Object UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.get_Current()
extern void AsyncOperationHandle_System_Collections_IEnumerator_get_Current_m902A963CD3B19733FB41AA0282DA4807D22668C8_AdjustorThunk (void);
// 0x00000166 System.Boolean UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.MoveNext()
extern void AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m49963C8BC42694191D9AF69FB63C1DEB8472BCB7_AdjustorThunk (void);
// 0x00000167 System.Void UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle::System.Collections.IEnumerator.Reset()
extern void AsyncOperationHandle_System_Collections_IEnumerator_Reset_mC3C2DA4955C33CDC282A3155FC1619EE5DE50810_AdjustorThunk (void);
// 0x00000168 System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::GetDependentOps()
extern void GroupOperation_GetDependentOps_m65A13CA9F754AB7B61EDE32ACD44E96EF66A2EB8 (void);
// 0x00000169 System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Execute()
extern void GroupOperation_Execute_mF71134E1DCBA889EF1ADB17EC1E16A6DAEEF4A1F (void);
// 0x0000016A System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::CompleteIfDependenciesComplete()
extern void GroupOperation_CompleteIfDependenciesComplete_m81CE72A80A868B8825BFBEC959976741EC5C399E (void);
// 0x0000016B System.Void UnityEngine.ResourceManagement.AsyncOperations.GroupOperation::Init(System.Collections.Generic.List`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>,System.Boolean)
extern void GroupOperation_Init_m9ADCA08E7D6809C51937CE3A225135E23D84080D (void);
// 0x0000016C System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::Init(UnityEngine.ResourceManagement.ResourceManager,UnityEngine.ResourceManagement.ResourceProviders.IResourceProvider,UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>>)
// 0x0000016D System.Int32 UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_ProvideHandleVersion()
// 0x0000016E UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_Location()
// 0x0000016F System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::GetDependencies(System.Collections.Generic.IList`1<System.Object>)
// 0x00000170 TDepObject UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::GetDependency(System.Int32)
// 0x00000171 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::SetProgressCallback(System.Func`1<System.Single>)
// 0x00000172 System.Void UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::ProviderCompleted(T,System.Boolean,System.Exception)
// 0x00000173 System.Type UnityEngine.ResourceManagement.AsyncOperations.IGenericProviderOperation::get_RequestedType()
// 0x00000174 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::ProviderCompleted(T,System.Boolean,System.Exception)
// 0x00000175 System.Void UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation`1::Execute()
static Il2CppMethodPointer s_methodPointers[373] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MonoBehaviourCallbackHooks_add_OnUpdateDelegate_m71F16EDF13A73B69505988BF80F3BD3C15D657B6,
	MonoBehaviourCallbackHooks_remove_OnUpdateDelegate_m5A983C728E2EECA2E549DD44203A30BC9EA4488F,
	MonoBehaviourCallbackHooks_GetGameObjectName_m00804DAD86A9F70E1F9241F0CE2F8DE141D41D9B,
	MonoBehaviourCallbackHooks_Update_mF0F182D68DFD4098DEB677350B6C87935C23D9E2,
	MonoBehaviourCallbackHooks__ctor_m6DA4F0AF02FEBFF30AA23A97945582D2884EB1C1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceManager_get_ExceptionHandler_m7AA5E78ACAE04A2406EBA923EE0CBC2BFE39EB01,
	ResourceManager_set_ExceptionHandler_mA6F5EBE9E65F68C8A8610638985E34304FE15BA7,
	ResourceManager_get_InternalIdTransformFunc_mE09C9ABCDE9E35C821FFBEEF42FCE9FDA8FEB308,
	ResourceManager_TransformInternalId_m99309A473F0354A9A003C8269967FB49300DC06C,
	ResourceManager_AddUpdateReceiver_m8E85F81F33E25C178B60C465322AADE11C03AA1F,
	ResourceManager_RemoveUpdateReciever_mE9CC3445844CC1E22DDD4815184FD27412E07925,
	ResourceManager_get_Allocator_mEEC5A006F0FDDB3771E377F1A40C9AE319B5A474,
	ResourceManager_get_ResourceProviders_m7B8444862B525921DCA90FFF0F89BAE11825677A,
	ResourceManager_set_CertificateHandlerInstance_m0DED0F9FE32208F8C3F3A995E93DE2AC7CFF2F9D,
	ResourceManager__ctor_m528366A74390F0F37D4F8D327B311E131484895F,
	ResourceManager_OnObjectAdded_m0A54698AC9BA86E496F1E3BA0DFF1A8DDB00600E,
	ResourceManager_OnObjectRemoved_mFB8B23D34D88A56665DD72B6487456D7B0F73294,
	ResourceManager_RegisterForCallbacks_mDFD4D66966A06B27431E6CFA21C62653613C5395,
	ResourceManager_ClearDiagnosticCallbacks_mC2ACDDB95C7291580AB454B5FEA5D76F5C953A5F,
	ResourceManager_UnregisterDiagnosticCallback_m94060E96C7D51B594484713FB4DE87A9B0A13454,
	ResourceManager_RegisterDiagnosticCallback_m7C36E880E0E8C7924E0B74A6769D3BFBD76BD65C,
	ResourceManager_PostDiagnosticEvent_m774EFC2B6BC9364BCF3B242FD3115BD3F94EA6BE,
	ResourceManager_GetResourceProvider_m38873363AA6A66617312E8F2ECE4277BA17B24A3,
	ResourceManager_GetDefaultTypeForLocation_mEB119E0ACD2ECD658112DCE606E3BD8B74B8C3A2,
	ResourceManager_CalculateLocationsHash_m9BE52AD42B5E6FB2082A455391477A87AF4A18AE,
	ResourceManager_ProvideResource_m594F8B2A6122F7744DBCCE7316450F302D6AB7C2,
	NULL,
	NULL,
	ResourceManager_StartOperation_mF350C51B1D49B0A1900C7B955595E50A560DE47A,
	ResourceManager_OnInstanceOperationDestroy_m0519061477A75DB2BF0F3F8A18CB34F25D908E9F,
	ResourceManager_OnOperationDestroyNonCached_m98C9B9996EE251C99419A7C6D4AC674C41B487E7,
	ResourceManager_OnOperationDestroyCached_mB4AFABDBC148F73495D45930E03AE05055ACABCC,
	NULL,
	ResourceManager_AddOperationToCache_mF821E49A9CAE2772E817ECB95460480AC32AFC23,
	ResourceManager_RemoveOperationFromCache_m42791732D849ED8CB5B858AF11CA1A0D3ABA6590,
	ResourceManager_IsOperationCached_m83330F9DD03DE37659319CC3EE6B607B49EEBE63,
	NULL,
	NULL,
	ResourceManager_Release_mD67A0632D3D7D0B7F97841BC1E4C7CF2FC191C57,
	ResourceManager_AcquireGroupOpFromCache_m5782A7CFB719A51E06134911422E3C5F9567B355,
	ResourceManager_CreateGenericGroupOperation_m148289B454ABDC6D18F9BA386EFEE2C4FFA640F4,
	ResourceManager_ProvideResourceGroupCached_mDD72A490BA7A3DF7BAFD0F29EA499BBFDD71910A,
	NULL,
	NULL,
	NULL,
	ResourceManager_ReleaseScene_mBD5161D48EFD7ACAD02CB9A12378CAE0EA8A828E,
	ResourceManager_CleanupSceneInstances_m2BBE6BCBFBA7DC2CEA5D38DE680FA6A52B0EA528,
	ResourceManager_ExecuteDeferredCallbacks_m75C200EC542CDBCEB07DBD4720835323A34D144F,
	ResourceManager_RegisterForDeferredCallback_m0436E85EDF6A341FEE22C1C7A360EC0643042272,
	ResourceManager_Update_m1038C44C2B864801F1D40DC503AA7F1029AAAF70,
	ResourceManager_Dispose_m90EE4D872BF46DE7D69ED70BD540A2B46064D0B8,
	ResourceManager__cctor_m9E01A63275781A7065E780992DFD1A0269EE76CB,
	ResourceManager_U3C_ctorU3Eb__45_0_mCECD009288795FC98DBCE498BA37927C1AD49273,
	DiagnosticEventContext_get_OperationHandle_m6884404709298E7C22B1F7F8205C1B7E8796EE05_AdjustorThunk,
	DiagnosticEventContext_get_Type_mC90B555707F6837DA6A22722EF4353065334C905_AdjustorThunk,
	DiagnosticEventContext_get_EventValue_mCD0F3458A7BCBE06550729AC816B34E5E19E14EF_AdjustorThunk,
	DiagnosticEventContext_get_Context_m4B8F30E7A090EB33FE0C1F8720C40273B3859C04_AdjustorThunk,
	DiagnosticEventContext_get_Error_m1CF5543DF39540144965910BC6FFF71862753E60_AdjustorThunk,
	DiagnosticEventContext__ctor_mC21627E8722C2E92B119E731AA890319A131FECA_AdjustorThunk,
	NULL,
	NULL,
	InstanceOperation_InstanceScene_mAC44625C564DE76E956CA8BCF4EC2C9F6178D9DE,
	InstanceOperation_Execute_m8792B88CB3F74349E1FD1D35E9426D58894F19D3,
	NULL,
	NULL,
	NULL,
	NULL,
	WebRequestQueueOperation_get_IsDone_mD60F4EB09496B7A2F86C696815E34491375CC9D0,
	WebRequestQueueOperation__ctor_m68CB315ACE251B662B8A4A603751432C8048F38A,
	WebRequestQueueOperation_Complete_m045307756A8EB68F5ADC3E6FF182361C60AABBF8,
	WebRequestQueue_SetMaxConcurrentRequests_m20043AB0BAC3523B55EB84AE01E4CA67F03CB936,
	WebRequestQueue_QueueRequest_m9E882E5334EC6CB6FB517C1A0DF985B83779B78F,
	WebRequestQueue_OnWebAsyncOpComplete_mF0B3325CA9B1386C3DAB80E1D87C4C5DB16F624C,
	WebRequestQueue__cctor_m387136624EFB374B41B0EEE1A63506F97F28D9B6,
	ResourceManagerException__ctor_mAEDC8C69DA13800EC322613C6829018DBB1E07CB,
	UnknownResourceProviderException_get_Location_mB274BAF53E19FE874A24CC67C522D494CCC8F4F0,
	UnknownResourceProviderException_set_Location_mF93EB4AAA41E957EFFACCEA94DFF45AEC67EB619,
	UnknownResourceProviderException__ctor_mB1E23AB4989DE1993682D165DD042F52E042AAE8,
	UnknownResourceProviderException_get_Message_mC59322E40728F5119EB36A38F38BA109AFA55E28,
	UnknownResourceProviderException_ToString_mE320AC807B6D64A5AAD350EB35DFB6781B571648,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DefaultAllocationStrategy_New_mB78CC5AFBF1B07035814191AB926E4ACBD41A6CE,
	DefaultAllocationStrategy_Release_mD958EA6925889742484435B1CD1AAF4087F60E4A,
	DefaultAllocationStrategy__ctor_mC395409A503C0199396C35AA3B939CE682AB3C8A,
	LRUCacheAllocationStrategy__ctor_m1DE9B78742C8D5457040C65AFA15854983E33FC6,
	LRUCacheAllocationStrategy_GetPool_mFC7368E7F5D84E6CFDCC693520C2B2B6965627A8,
	LRUCacheAllocationStrategy_ReleasePool_m72D0EB4F039EE69BDB8BBB68AFB8BA7D62B92BCF,
	LRUCacheAllocationStrategy_New_m9A93EA16BC5C9C0F1933D6A19C8C19F854F9D2E3,
	LRUCacheAllocationStrategy_Release_m42CAEAF3890309E4396717B9CE28D17082BA6300,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SerializedType_ToString_m1148F8BA12B705068191BD22F924E4570E0CF2BF_AdjustorThunk,
	SerializedType_get_Value_m1BC4883C0CA3F911AC298F99BA3820D28B116AE4_AdjustorThunk,
	ObjectInitializationData_get_Id_mEC5373B1DDFE78FB72F291806ED9C869DA2B27B5_AdjustorThunk,
	ObjectInitializationData_get_ObjectType_m14F9142B5C61D1F8285BA84C34B2747EBDE59DD0_AdjustorThunk,
	ObjectInitializationData_ToString_mA1CB0E03EDC5E862A576AC96DF6E768E018BE8EF_AdjustorThunk,
	NULL,
	ObjectInitializationData_GetAsyncInitHandle_m1A3A5DA8AD774DC20CC561528A6305CC4F5999A6_AdjustorThunk,
	ResourceManagerConfig_ExtractKeyAndSubKey_m10E4749CC55275EB25BB3341FF729050BEFCABB0,
	ResourceManagerConfig_ShouldPathUseWebRequest_m20ED59B08CD29D311DE947B60B3020B6447EA19A,
	AtlasSpriteProvider_Provide_m1EEB6E65B16F76C067088BEAF5CD12D9E040BA8E,
	NULL,
	ProvideHandle__ctor_m6162616DB274E0DF650DB36B4ECD1AB8FF7FF0AA_AdjustorThunk,
	ProvideHandle_get_InternalOp_mBAFE92C7761F1994A9AC5542BC7085AE67C57051_AdjustorThunk,
	ProvideHandle_get_ResourceManager_m05D4E79DEBFDD6BD466CD6A2219958B00C6C3E8E_AdjustorThunk,
	ProvideHandle_get_Type_m9569FAA40332A3EFD53227FABC324CC03B4BAD3D_AdjustorThunk,
	ProvideHandle_get_Location_m5F7540397789E7214F8B9DD88C8E9B8A7A6ABE53_AdjustorThunk,
	NULL,
	ProvideHandle_GetDependencies_m50B8C3466AED683B0F8581207B09D887350CB741_AdjustorThunk,
	ProvideHandle_SetProgressCallback_mCADBA6D92915BE6956E55E25A84F9A5E61BD289F_AdjustorThunk,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SceneInstance_get_Scene_m941C7B9506F1E8C45785CD5F6AE03DF8939F3ABE_AdjustorThunk,
	SceneInstance_GetHashCode_m1D0926F7DDB54F2D00B59B84CE8E3D25BB225204_AdjustorThunk,
	SceneInstance_Equals_mD0482B6D8FF701E433A23F4047E02634128BF0AF_AdjustorThunk,
	NULL,
	JsonAssetProvider_Convert_mD40A593AF7815BA0370BF56680B7BDB3244C68F8,
	JsonAssetProvider__ctor_mFAF69C1316EEF5EC237555CF441B02D5BAEA662B,
	ResourceProviderBase_get_ProviderId_m5860108653362B09CF615C607BF9234FD97DC11D,
	ResourceProviderBase_Initialize_m34E104D3A5D94257BE270AF01B9B58593EB2A83B,
	ResourceProviderBase_CanProvide_mD9D178400A79CEF5294BFAE7B6079BAFA3EF653A,
	ResourceProviderBase_ToString_m80D7CDE36BB26C8DE21290352CAF1A2C1D753D7C,
	ResourceProviderBase_GetDefaultType_m2156E5A367DDA36A48496BDD01E01C4985700F57,
	NULL,
	ResourceProviderBase_InitializeAsync_m80430AFAB1D2335A508216D5A110CB736F6171EA,
	ResourceProviderBase_UnityEngine_ResourceManagement_ResourceProviders_IResourceProvider_get_BehaviourFlags_mD7856EDEB725FA84A89DAD3F529B9D3FC656EEC6,
	ResourceProviderBase__ctor_m7FCD7D94546A446A53C78AF135940F4F563DFAC7,
	BaseInitAsyncOp_Init_m50C6E027B3900B3DBCE301C140DE82F74CCE38D2,
	BaseInitAsyncOp_Execute_mC12E09D151DA1D4D838FCB2E38B58F824AEB0E3F,
	BaseInitAsyncOp__ctor_mE020A405B8D14A3269982E70A9C421A717A92FE3,
	U3CU3Ec__DisplayClass10_0__ctor_mB1D7DDAE2A506B4A834A35D720C18095BA4653C3,
	U3CU3Ec__DisplayClass10_0_U3CInitializeAsyncU3Eb__0_m3D6B8698FD2339D3145463DFEB3BB21BD687E747,
	TextDataProvider_get_IgnoreFailures_m54B8CF389A2EF6D0C92E97D23541CD730CE45851,
	TextDataProvider_set_IgnoreFailures_m42340E49DD6BFBEE98EA6D24A0FB5B0673372A74,
	TextDataProvider_Convert_m2B991FF6B1CB9CD871439B6F5089BE5AE6DF2518,
	TextDataProvider_Provide_mF3742F8E9A17003FF5D555E4022419B9EE0F8989,
	TextDataProvider__ctor_m7AD4D795A1480E3CEC6DCDD478A37FD51E79812D,
	InternalOp_GetPercentComplete_m7AC5C9BEC338F6C89783E233E8C532057E62BD2A,
	InternalOp_Start_m2A6D3C16C95797FC3162CC981CFA28C4128E35E2,
	InternalOp_RequestOperation_completed_m587EACB3BF4B624F485E87D20E661A8F0192BADE,
	InternalOp__ctor_mA96AC18D8B782C4508EB024210E260A8E899753E,
	InternalOp_U3CStartU3Eb__6_0_mBDCB8E098F9284FC7C3A90C6461A8C107E159F4A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ResourceLocationBase_get_InternalId_m446E746FAE3B40B1CDA9A588A319DDBDB1453ABB,
	ResourceLocationBase_get_ProviderId_mEE3324D2E4071C922810A050C10AFF5DDCA3EAA3,
	ResourceLocationBase_get_Dependencies_m054FEB15A19CB1591D9E11587725EAD00100F3EB,
	ResourceLocationBase_get_HasDependencies_m25C0F08130902EFEAEF8DDD99D75073BD7377272,
	ResourceLocationBase_get_PrimaryKey_mAA5E2116F41249FE94A19BFB0E102605B8941B24,
	ResourceLocationBase_get_DependencyHashCode_m1CF137823A47AE4C1EC0503AA257AB7CCDE88CEB,
	ResourceLocationBase_get_ResourceType_m8621BD5A50D22E8F3C5933811823DB63034A62FD,
	ResourceLocationBase_Hash_mF2BC6233AC5F1F90E1AC29823D2F0CFB639374D4,
	ResourceLocationBase_ToString_m828E1E56F19E2A35DF72ED842192DABE7386A9A0,
	ResourceLocationBase__ctor_m9F01B99ECB49EFF4B75BF41E169FBFEF707809AA,
	ResourceLocationBase_ComputeDependencyHash_mB8A54AB1782AFCE00F30B2C22D0048B5006DBF52,
	DiagnosticEvent_get_ObjectId_m1CCBE7BC50003514247D99544A60BD22DF70596D_AdjustorThunk,
	DiagnosticEvent_get_Stream_m2811009448086191841A0EBAC768BD8DEDCB17E7_AdjustorThunk,
	DiagnosticEvent_get_Frame_mE28A9CF94A945044C148F653F95C4142ECA221B1_AdjustorThunk,
	DiagnosticEvent__ctor_m6A631EBC9C9EA934424340F88116686A83511D9B_AdjustorThunk,
	DiagnosticEvent_Serialize_m6255901EC86D38FBAFDE9069AF340608B45205E0_AdjustorThunk,
	DiagnosticEventCollectorSingleton_get_PlayerConnectionGuid_mB1565F345C559996E323B28DF44704E899F529DC,
	DiagnosticEventCollectorSingleton_GetGameObjectName_m0147EE9822537F03A29E0101B0E207C608D82DC6,
	DiagnosticEventCollectorSingleton_RegisterEventHandler_mB9A217A031D8D49AF18369AC4BACD572450F2C00,
	DiagnosticEventCollectorSingleton_RegisterEventHandler_mD140C18197FB2092B00061F727CE99BF53415FA7,
	DiagnosticEventCollectorSingleton_UnregisterEventHandler_m4A20D0D0105FD76EF279D864C462BE208F68225F,
	DiagnosticEventCollectorSingleton_PostEvent_m68C5FED777CFC48B166D761A798044D9BEE2A140,
	DiagnosticEventCollectorSingleton_Awake_mFA6860EA3E76647FFC0AA0DDBD4DFD372B78B651,
	DiagnosticEventCollectorSingleton_Update_mCBB780A57E2F7CCC3C2A7EBAC591F3E63A6A1B5E,
	DiagnosticEventCollectorSingleton__ctor_mE8886C4DA6922CF87C372E6A6A22F75B32810681,
	U3CU3Ec__cctor_m3F87D5D65E8E8A85497441B86834CB9813261980,
	U3CU3Ec__ctor_mFEA80F7FDDE93D4A56031F9A6A543EF3B26C9B78,
	U3CU3Ec_U3CRegisterEventHandlerU3Eb__8_0_mD42E45C5B7C6624A4D689ECC03B702B07B8F8FE6,
	U3CU3Ec_U3CAwakeU3Eb__11_0_mE0640D82251431C9F7999D4133CE16BD752D2B44,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AsyncOperationHandle_set_LocationName_m20D2D9A3B36B372F84634EBB47C4080CD6EC6E1F_AdjustorThunk,
	AsyncOperationHandle__ctor_m4A737468E34CBBCC7DDA85F5A0A5D461DB7F4913_AdjustorThunk,
	AsyncOperationHandle__ctor_mE32C8239AFC99079BCC3113FCDEFCF339154ADA1_AdjustorThunk,
	AsyncOperationHandle__ctor_m8FB8DEFB2E7B3F1BF8D47B5790EAD508D1DCD66A_AdjustorThunk,
	AsyncOperationHandle_Acquire_mC15C71C7BA8B95C49B8BCCD33CD2F7EB5B03BF7F_AdjustorThunk,
	AsyncOperationHandle_add_Completed_mBD4A54B7E0322F292C67A62A5461193758726B20_AdjustorThunk,
	AsyncOperationHandle_remove_Completed_mFF2A9DFC4D2E2A6D1302BFBC7EA49E01E3E14CEB_AdjustorThunk,
	NULL,
	AsyncOperationHandle_get_DebugName_m9263AE2FF7206453566D317443292E71686910FE_AdjustorThunk,
	AsyncOperationHandle_add_Destroyed_mCDA263102547EEC0D53DD5B2B7F55881EC896235_AdjustorThunk,
	AsyncOperationHandle_remove_Destroyed_m3BE893CECF7F7273A884984B6025867D7AA90FB2_AdjustorThunk,
	AsyncOperationHandle_GetDependencies_mD420A5EA523C940C0F90A216B971E8B6A060F71E_AdjustorThunk,
	AsyncOperationHandle_GetHashCode_m7764BEBFB86D7C0EB3D46D4AC116ADD56AEEAC68_AdjustorThunk,
	AsyncOperationHandle_get_InternalOp_m09CAB010CFD3205ADCB1128F3046132C538B8EFE_AdjustorThunk,
	AsyncOperationHandle_get_IsDone_m7F8233755EDB537A312B42BFAC1B4E95FDEB0B62_AdjustorThunk,
	AsyncOperationHandle_IsValid_mDDEEC690F9BFA3CFA09B8344947F1405EECA22F1_AdjustorThunk,
	AsyncOperationHandle_get_OperationException_mE66E9FDDCF99EF8B6D706A05F3156514D552C259_AdjustorThunk,
	AsyncOperationHandle_get_PercentComplete_m56F5B0016C1AE1681A5670F8F50F716F94C4B9C1_AdjustorThunk,
	AsyncOperationHandle_InternalGetDownloadStatus_m497FB080B6D909604490829B6F594FFCC155E6AC_AdjustorThunk,
	AsyncOperationHandle_Release_m75B6AFC0710FF804ECD0CBC6E7EFB85EEF095F67_AdjustorThunk,
	AsyncOperationHandle_get_Result_m692BEC3C25DA9A5BFCD041C069C693B25F783DED_AdjustorThunk,
	AsyncOperationHandle_get_Status_m541A1E32905CA1F7092BFBB1299F939271FA57E7_AdjustorThunk,
	AsyncOperationHandle_System_Collections_IEnumerator_get_Current_m902A963CD3B19733FB41AA0282DA4807D22668C8_AdjustorThunk,
	AsyncOperationHandle_System_Collections_IEnumerator_MoveNext_m49963C8BC42694191D9AF69FB63C1DEB8472BCB7_AdjustorThunk,
	AsyncOperationHandle_System_Collections_IEnumerator_Reset_mC3C2DA4955C33CDC282A3155FC1619EE5DE50810_AdjustorThunk,
	GroupOperation_GetDependentOps_m65A13CA9F754AB7B61EDE32ACD44E96EF66A2EB8,
	GroupOperation_Execute_mF71134E1DCBA889EF1ADB17EC1E16A6DAEEF4A1F,
	GroupOperation_CompleteIfDependenciesComplete_m81CE72A80A868B8825BFBEC959976741EC5C399E,
	GroupOperation_Init_m9ADCA08E7D6809C51937CE3A225135E23D84080D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[373] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	14,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4,
	168,
	14,
	28,
	26,
	26,
	14,
	14,
	26,
	26,
	26,
	26,
	23,
	23,
	26,
	26,
	2070,
	114,
	28,
	41,
	2071,
	-1,
	-1,
	2072,
	26,
	26,
	26,
	-1,
	62,
	30,
	30,
	-1,
	-1,
	2073,
	34,
	2074,
	2075,
	-1,
	-1,
	-1,
	2076,
	2077,
	23,
	102,
	339,
	23,
	3,
	26,
	2078,
	10,
	10,
	14,
	14,
	2079,
	-1,
	-1,
	1770,
	23,
	-1,
	-1,
	-1,
	339,
	89,
	26,
	26,
	178,
	0,
	168,
	3,
	23,
	14,
	26,
	26,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	90,
	2080,
	58,
	62,
	58,
	62,
	23,
	343,
	14,
	26,
	58,
	62,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	14,
	14,
	2081,
	14,
	-1,
	2071,
	367,
	109,
	2082,
	2083,
	27,
	14,
	14,
	14,
	14,
	-1,
	26,
	26,
	-1,
	14,
	28,
	90,
	2082,
	10,
	1770,
	10,
	9,
	2076,
	114,
	23,
	14,
	90,
	90,
	14,
	28,
	2082,
	2080,
	10,
	23,
	26,
	23,
	23,
	23,
	89,
	89,
	31,
	114,
	2082,
	23,
	761,
	2084,
	26,
	23,
	26,
	14,
	14,
	14,
	121,
	10,
	89,
	14,
	14,
	14,
	14,
	14,
	89,
	14,
	10,
	14,
	121,
	14,
	741,
	23,
	10,
	10,
	10,
	2085,
	14,
	435,
	14,
	2086,
	26,
	26,
	2087,
	23,
	23,
	23,
	3,
	23,
	2088,
	2087,
	10,
	32,
	14,
	10,
	14,
	23,
	23,
	761,
	2089,
	10,
	14,
	89,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	2090,
	2078,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	27,
	116,
	2078,
	26,
	26,
	-1,
	14,
	26,
	26,
	26,
	10,
	14,
	89,
	89,
	14,
	761,
	2089,
	23,
	14,
	10,
	14,
	89,
	23,
	14,
	23,
	23,
	102,
	2091,
	10,
	14,
	26,
	-1,
	26,
	-1,
	14,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[25] = 
{
	{ 0x02000002, { 0, 20 } },
	{ 0x02000003, { 20, 23 } },
	{ 0x02000005, { 43, 3 } },
	{ 0x02000006, { 46, 18 } },
	{ 0x0200000A, { 93, 3 } },
	{ 0x0200000C, { 96, 8 } },
	{ 0x02000012, { 104, 10 } },
	{ 0x02000017, { 114, 8 } },
	{ 0x02000018, { 122, 5 } },
	{ 0x02000031, { 130, 41 } },
	{ 0x02000032, { 171, 19 } },
	{ 0x02000038, { 192, 6 } },
	{ 0x06000042, { 64, 2 } },
	{ 0x06000043, { 66, 2 } },
	{ 0x06000048, { 68, 1 } },
	{ 0x0600004C, { 69, 1 } },
	{ 0x0600004D, { 70, 4 } },
	{ 0x06000052, { 74, 9 } },
	{ 0x06000053, { 83, 6 } },
	{ 0x06000054, { 89, 4 } },
	{ 0x06000096, { 127, 1 } },
	{ 0x060000A1, { 128, 1 } },
	{ 0x060000A4, { 129, 1 } },
	{ 0x06000156, { 190, 2 } },
	{ 0x06000174, { 198, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[202] = 
{
	{ (Il2CppRGCTXDataType)3, 48943 },
	{ (Il2CppRGCTXDataType)3, 48944 },
	{ (Il2CppRGCTXDataType)2, 27468 },
	{ (Il2CppRGCTXDataType)3, 48945 },
	{ (Il2CppRGCTXDataType)3, 48946 },
	{ (Il2CppRGCTXDataType)3, 48947 },
	{ (Il2CppRGCTXDataType)3, 48948 },
	{ (Il2CppRGCTXDataType)3, 48949 },
	{ (Il2CppRGCTXDataType)3, 48950 },
	{ (Il2CppRGCTXDataType)3, 48951 },
	{ (Il2CppRGCTXDataType)3, 48952 },
	{ (Il2CppRGCTXDataType)3, 48953 },
	{ (Il2CppRGCTXDataType)3, 48954 },
	{ (Il2CppRGCTXDataType)2, 14148 },
	{ (Il2CppRGCTXDataType)3, 48955 },
	{ (Il2CppRGCTXDataType)3, 48956 },
	{ (Il2CppRGCTXDataType)2, 14152 },
	{ (Il2CppRGCTXDataType)3, 48957 },
	{ (Il2CppRGCTXDataType)2, 14153 },
	{ (Il2CppRGCTXDataType)3, 48958 },
	{ (Il2CppRGCTXDataType)2, 14159 },
	{ (Il2CppRGCTXDataType)3, 48959 },
	{ (Il2CppRGCTXDataType)3, 48960 },
	{ (Il2CppRGCTXDataType)3, 48961 },
	{ (Il2CppRGCTXDataType)3, 48962 },
	{ (Il2CppRGCTXDataType)3, 48963 },
	{ (Il2CppRGCTXDataType)3, 48964 },
	{ (Il2CppRGCTXDataType)3, 48965 },
	{ (Il2CppRGCTXDataType)2, 27103 },
	{ (Il2CppRGCTXDataType)3, 48966 },
	{ (Il2CppRGCTXDataType)3, 48967 },
	{ (Il2CppRGCTXDataType)3, 48968 },
	{ (Il2CppRGCTXDataType)3, 48969 },
	{ (Il2CppRGCTXDataType)2, 27469 },
	{ (Il2CppRGCTXDataType)3, 48970 },
	{ (Il2CppRGCTXDataType)3, 48971 },
	{ (Il2CppRGCTXDataType)3, 48972 },
	{ (Il2CppRGCTXDataType)3, 48973 },
	{ (Il2CppRGCTXDataType)3, 48974 },
	{ (Il2CppRGCTXDataType)3, 48975 },
	{ (Il2CppRGCTXDataType)3, 48976 },
	{ (Il2CppRGCTXDataType)2, 27470 },
	{ (Il2CppRGCTXDataType)3, 48977 },
	{ (Il2CppRGCTXDataType)3, 48978 },
	{ (Il2CppRGCTXDataType)3, 48979 },
	{ (Il2CppRGCTXDataType)3, 48980 },
	{ (Il2CppRGCTXDataType)3, 48981 },
	{ (Il2CppRGCTXDataType)2, 14186 },
	{ (Il2CppRGCTXDataType)3, 48982 },
	{ (Il2CppRGCTXDataType)2, 27471 },
	{ (Il2CppRGCTXDataType)3, 48983 },
	{ (Il2CppRGCTXDataType)1, 14187 },
	{ (Il2CppRGCTXDataType)3, 48984 },
	{ (Il2CppRGCTXDataType)3, 48985 },
	{ (Il2CppRGCTXDataType)3, 48986 },
	{ (Il2CppRGCTXDataType)3, 48987 },
	{ (Il2CppRGCTXDataType)3, 48988 },
	{ (Il2CppRGCTXDataType)3, 48989 },
	{ (Il2CppRGCTXDataType)3, 48990 },
	{ (Il2CppRGCTXDataType)3, 48991 },
	{ (Il2CppRGCTXDataType)3, 48992 },
	{ (Il2CppRGCTXDataType)3, 48993 },
	{ (Il2CppRGCTXDataType)3, 48994 },
	{ (Il2CppRGCTXDataType)3, 48995 },
	{ (Il2CppRGCTXDataType)1, 14206 },
	{ (Il2CppRGCTXDataType)3, 48996 },
	{ (Il2CppRGCTXDataType)3, 48997 },
	{ (Il2CppRGCTXDataType)3, 48998 },
	{ (Il2CppRGCTXDataType)2, 14211 },
	{ (Il2CppRGCTXDataType)3, 48999 },
	{ (Il2CppRGCTXDataType)1, 27472 },
	{ (Il2CppRGCTXDataType)3, 49000 },
	{ (Il2CppRGCTXDataType)3, 49001 },
	{ (Il2CppRGCTXDataType)3, 49002 },
	{ (Il2CppRGCTXDataType)2, 27473 },
	{ (Il2CppRGCTXDataType)3, 49003 },
	{ (Il2CppRGCTXDataType)3, 49004 },
	{ (Il2CppRGCTXDataType)3, 49005 },
	{ (Il2CppRGCTXDataType)1, 14218 },
	{ (Il2CppRGCTXDataType)3, 49006 },
	{ (Il2CppRGCTXDataType)2, 27474 },
	{ (Il2CppRGCTXDataType)3, 49007 },
	{ (Il2CppRGCTXDataType)3, 49008 },
	{ (Il2CppRGCTXDataType)1, 27475 },
	{ (Il2CppRGCTXDataType)3, 49009 },
	{ (Il2CppRGCTXDataType)3, 49010 },
	{ (Il2CppRGCTXDataType)3, 49011 },
	{ (Il2CppRGCTXDataType)2, 14222 },
	{ (Il2CppRGCTXDataType)3, 49012 },
	{ (Il2CppRGCTXDataType)2, 27476 },
	{ (Il2CppRGCTXDataType)3, 49013 },
	{ (Il2CppRGCTXDataType)3, 49014 },
	{ (Il2CppRGCTXDataType)3, 49015 },
	{ (Il2CppRGCTXDataType)3, 49016 },
	{ (Il2CppRGCTXDataType)3, 49017 },
	{ (Il2CppRGCTXDataType)3, 49018 },
	{ (Il2CppRGCTXDataType)2, 14267 },
	{ (Il2CppRGCTXDataType)3, 49019 },
	{ (Il2CppRGCTXDataType)2, 27477 },
	{ (Il2CppRGCTXDataType)3, 49020 },
	{ (Il2CppRGCTXDataType)3, 49021 },
	{ (Il2CppRGCTXDataType)3, 49022 },
	{ (Il2CppRGCTXDataType)3, 49023 },
	{ (Il2CppRGCTXDataType)3, 49024 },
	{ (Il2CppRGCTXDataType)2, 14288 },
	{ (Il2CppRGCTXDataType)2, 14286 },
	{ (Il2CppRGCTXDataType)3, 49025 },
	{ (Il2CppRGCTXDataType)3, 49026 },
	{ (Il2CppRGCTXDataType)3, 49027 },
	{ (Il2CppRGCTXDataType)1, 14286 },
	{ (Il2CppRGCTXDataType)3, 49028 },
	{ (Il2CppRGCTXDataType)3, 49029 },
	{ (Il2CppRGCTXDataType)3, 49030 },
	{ (Il2CppRGCTXDataType)3, 49031 },
	{ (Il2CppRGCTXDataType)3, 49032 },
	{ (Il2CppRGCTXDataType)3, 49033 },
	{ (Il2CppRGCTXDataType)3, 49034 },
	{ (Il2CppRGCTXDataType)2, 14301 },
	{ (Il2CppRGCTXDataType)3, 49035 },
	{ (Il2CppRGCTXDataType)2, 27478 },
	{ (Il2CppRGCTXDataType)3, 49036 },
	{ (Il2CppRGCTXDataType)3, 49037 },
	{ (Il2CppRGCTXDataType)2, 27479 },
	{ (Il2CppRGCTXDataType)2, 27480 },
	{ (Il2CppRGCTXDataType)3, 49038 },
	{ (Il2CppRGCTXDataType)3, 49039 },
	{ (Il2CppRGCTXDataType)3, 49040 },
	{ (Il2CppRGCTXDataType)2, 14310 },
	{ (Il2CppRGCTXDataType)3, 49041 },
	{ (Il2CppRGCTXDataType)3, 49042 },
	{ (Il2CppRGCTXDataType)3, 49043 },
	{ (Il2CppRGCTXDataType)3, 49044 },
	{ (Il2CppRGCTXDataType)2, 14378 },
	{ (Il2CppRGCTXDataType)3, 49045 },
	{ (Il2CppRGCTXDataType)3, 49046 },
	{ (Il2CppRGCTXDataType)2, 14378 },
	{ (Il2CppRGCTXDataType)3, 49047 },
	{ (Il2CppRGCTXDataType)3, 49048 },
	{ (Il2CppRGCTXDataType)3, 49049 },
	{ (Il2CppRGCTXDataType)2, 14376 },
	{ (Il2CppRGCTXDataType)3, 49050 },
	{ (Il2CppRGCTXDataType)3, 49051 },
	{ (Il2CppRGCTXDataType)2, 27481 },
	{ (Il2CppRGCTXDataType)3, 49052 },
	{ (Il2CppRGCTXDataType)3, 49053 },
	{ (Il2CppRGCTXDataType)3, 49054 },
	{ (Il2CppRGCTXDataType)3, 49055 },
	{ (Il2CppRGCTXDataType)3, 49056 },
	{ (Il2CppRGCTXDataType)3, 49057 },
	{ (Il2CppRGCTXDataType)3, 49058 },
	{ (Il2CppRGCTXDataType)3, 49059 },
	{ (Il2CppRGCTXDataType)3, 49060 },
	{ (Il2CppRGCTXDataType)3, 49061 },
	{ (Il2CppRGCTXDataType)3, 49062 },
	{ (Il2CppRGCTXDataType)3, 49063 },
	{ (Il2CppRGCTXDataType)3, 49064 },
	{ (Il2CppRGCTXDataType)3, 49065 },
	{ (Il2CppRGCTXDataType)3, 49066 },
	{ (Il2CppRGCTXDataType)3, 49067 },
	{ (Il2CppRGCTXDataType)3, 49068 },
	{ (Il2CppRGCTXDataType)3, 49069 },
	{ (Il2CppRGCTXDataType)3, 49070 },
	{ (Il2CppRGCTXDataType)3, 49071 },
	{ (Il2CppRGCTXDataType)3, 49072 },
	{ (Il2CppRGCTXDataType)3, 49073 },
	{ (Il2CppRGCTXDataType)3, 49074 },
	{ (Il2CppRGCTXDataType)3, 49075 },
	{ (Il2CppRGCTXDataType)3, 49076 },
	{ (Il2CppRGCTXDataType)3, 49077 },
	{ (Il2CppRGCTXDataType)3, 49078 },
	{ (Il2CppRGCTXDataType)3, 49079 },
	{ (Il2CppRGCTXDataType)3, 49080 },
	{ (Il2CppRGCTXDataType)3, 49081 },
	{ (Il2CppRGCTXDataType)3, 49082 },
	{ (Il2CppRGCTXDataType)3, 49083 },
	{ (Il2CppRGCTXDataType)2, 14391 },
	{ (Il2CppRGCTXDataType)3, 49084 },
	{ (Il2CppRGCTXDataType)3, 49085 },
	{ (Il2CppRGCTXDataType)3, 49086 },
	{ (Il2CppRGCTXDataType)3, 49087 },
	{ (Il2CppRGCTXDataType)3, 49088 },
	{ (Il2CppRGCTXDataType)3, 49089 },
	{ (Il2CppRGCTXDataType)3, 49090 },
	{ (Il2CppRGCTXDataType)3, 49091 },
	{ (Il2CppRGCTXDataType)3, 49092 },
	{ (Il2CppRGCTXDataType)3, 49093 },
	{ (Il2CppRGCTXDataType)3, 49094 },
	{ (Il2CppRGCTXDataType)3, 49095 },
	{ (Il2CppRGCTXDataType)3, 49096 },
	{ (Il2CppRGCTXDataType)2, 14390 },
	{ (Il2CppRGCTXDataType)2, 14396 },
	{ (Il2CppRGCTXDataType)3, 49097 },
	{ (Il2CppRGCTXDataType)1, 14409 },
	{ (Il2CppRGCTXDataType)2, 14409 },
	{ (Il2CppRGCTXDataType)3, 49098 },
	{ (Il2CppRGCTXDataType)3, 49099 },
	{ (Il2CppRGCTXDataType)3, 49100 },
	{ (Il2CppRGCTXDataType)3, 49101 },
	{ (Il2CppRGCTXDataType)2, 27482 },
	{ (Il2CppRGCTXDataType)3, 49102 },
	{ (Il2CppRGCTXDataType)2, 14410 },
	{ (Il2CppRGCTXDataType)1, 14410 },
};
extern const Il2CppCodeGenModule g_Unity_ResourceManagerCodeGenModule;
const Il2CppCodeGenModule g_Unity_ResourceManagerCodeGenModule = 
{
	"Unity.ResourceManager.dll",
	373,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	25,
	s_rgctxIndices,
	202,
	s_rgctxValues,
	NULL,
};
