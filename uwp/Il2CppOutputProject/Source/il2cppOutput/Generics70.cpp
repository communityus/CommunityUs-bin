﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


struct VirtActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3, typename T4, typename T5>
struct VirtActionInvoker5
{
	typedef void (*Action)(void*, T1, T2, T3, T4, T5, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3, T4 p4, T5 p5)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, p4, p5, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R, typename T1, typename T2, typename T3>
struct VirtFuncInvoker3
{
	typedef R (*Func)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};
template <typename R, typename T1, typename T2>
struct VirtFuncInvoker2
{
	typedef R (*Func)(void*, T1, T2, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, p2, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// Mono.CSharp.AnonymousExpression
struct AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// Mono.CSharp.Block
struct Block_t6084BBFB00E424695AF02B74D74E6060CEC8C88D;
// Mono.CSharp.BlockContext
struct BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161;
// Mono.CSharp.CloneContext
struct CloneContext_t05693E643AAC9CB5A95947DEC8B8CA5EF1F49B2A;
// Mono.CSharp.CompilerContext
struct CompilerContext_t4F0AFBF2AD3D5DF37C68BFD853D437A152C9B5A0;
// Mono.CSharp.ConditionalAccessContext
struct ConditionalAccessContext_t61229EFEB30A9688E063A536301C999D99DEACB7;
// Wenzil.Console.ConsoleCommandCallback
struct ConsoleCommandCallback_t747E2C2159A300D81D5755E1385DA036AF8D2056;
// Mono.CSharp.DefiniteAssignmentBitSet
struct DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// Mono.CSharp.DynamicSiteClass
struct DynamicSiteClass_t3E76BED87F5AA7A05B78E4EBB5AD7961370C4BB0;
// Mono.CSharp.EmitContext
struct EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B;
// Mono.CSharp.ExceptionStatement
struct ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453;
// Mono.CSharp.Expression
struct Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31;
// Mono.CSharp.FlowAnalysisContext
struct FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// Mono.CSharp.IMemberContext
struct IMemberContext_tDE944F94F601E225FA106ECD23E90DF5F49E9751;
// Mono.CSharp.LocalVariable
struct LocalVariable_tA8C36B52BF086714723D452FC309A1C1B696E36A;
// Mono.CSharp.LoopStatement
struct LoopStatement_t14901229726AB5B6E193B0D683690CE7166AF884;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// Mono.CSharp.ParametersBlock
struct ParametersBlock_t30562D7F0452D25CD1537D1FE2F9DF8F32130D5A;
// Mono.CSharp.ResolveContext
struct ResolveContext_t40849BA9E46B26BF7120B15AEA68FFD5E185A7EB;
// Mono.CSharp.ResumableStatement
struct ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// Mono.CompilerServices.SymbolWriter.SourceMethodBuilder
struct SourceMethodBuilder_t20E41F27F7E03856F8C88E880BD1628D2BC8BED8;
// Mono.CSharp.StateMachine
struct StateMachine_tD8A15D3F0A9D4D7727FFA8404DA0BA2BA8035B62;
// Mono.CSharp.StateMachineInitializer
struct StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48;
// Mono.CSharp.Statement
struct Statement_t412FB2CAA6DCE73C11AD18A3F3E56815675E7D79;
// Mono.CSharp.Switch
struct Switch_t2CEA4337B1222A5A9B26B32936CCD53241AAE027;
// Mono.CSharp.TryFinally
struct TryFinally_t9B9CAAEE26F075D9241144D094E26B5631AE68F3;
// Mono.CSharp.TypeDefinition
struct TypeDefinition_t356551DFC70A14243EF40A9AAE75333D1A547B02;
// Mono.CSharp.TypeSpec
struct TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// FullSerializer.fsBaseConverter
struct fsBaseConverter_tF4DC7258DCD56EA3FF50C3FD608C8F31631BF8CC;
// FullSerializer.fsData
struct fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16;
// FullSerializer.fsDirectConverter
struct fsDirectConverter_t41638BCC8EA7FFAC4B7A9C0797FC3E57D1ECF024;
// FullSerializer.fsSerializer
struct fsSerializer_tB6ACF6E73C607ABBD52D5C5964AE409C78994710;
// System.Collections.Generic.Dictionary`2<Mono.CSharp.Block,Mono.CSharp.Block>
struct Dictionary_2_t188E3037A86CB72A8BB65E4A73A2BB5FC43E37AF;
// System.Collections.Generic.Dictionary`2<Mono.CSharp.Statement,System.Collections.Generic.List`1<Mono.CSharp.DefiniteAssignmentBitSet>>
struct Dictionary_2_t0FF34CAAFD5B474F756CC87E236600B006A9F5FA;
// System.Collections.Generic.Dictionary`2<Mono.CSharp.TypeSpec,System.Object>
struct Dictionary_2_t6210FBAFCE098ADD4453912B9A98F5B5CE7884CB;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D;
// System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>
struct Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732;
// System.Xml.Linq.XHashtable`1/ExtractKeyDelegate<System.Object>
struct ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99;
// System.Func`2<System.Char,System.Int32>
struct Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3;
// System.Func`2<System.Char,System.Object>
struct Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E;
// System.Func`2<System.Char,System.Single>
struct Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F;
// System.Func`2<System.Char,System.Boolean>
struct Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A;
// System.Func`2<Wenzil.Console.ConsoleCommand,System.Boolean>
struct Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F;
// System.Func`2<System.Int32,System.Int32>
struct Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA;
// System.Func`2<System.Int32,System.Object>
struct Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123;
// System.Func`2<System.Int32,System.Single>
struct Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6;
// System.Func`2<System.Int32,System.Boolean>
struct Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274;
// System.Func`2<System.Int32Enum,System.Int32>
struct Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F;
// System.Func`2<System.Int32Enum,System.Object>
struct Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB;
// System.Func`2<System.Int32Enum,System.Single>
struct Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A;
// System.Func`2<System.Int32Enum,System.Boolean>
struct Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E;
// System.Func`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Int32>
struct Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD;
// System.Func`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Single>
struct Func_2_tE234817DD56F8D228C80039231C328502ABCF601;
// System.Func`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Boolean>
struct Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A;
// System.Func`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Object>
struct Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA;
// System.Func`2<System.Object,System.Int32>
struct Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C;
// System.Func`2<System.Object,System.Single>
struct Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1;
// System.Func`2<System.Object,System.Boolean>
struct Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8;
// System.Func`2<UnityEngine.Resolution,System.Int32>
struct Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894;
// System.Func`2<UnityEngine.Resolution,System.Object>
struct Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98;
// System.Func`2<UnityEngine.Resolution,System.Single>
struct Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB;
// System.Func`2<UnityEngine.Resolution,System.Boolean>
struct Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B;
// System.Func`2<System.Single,System.Int32>
struct Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988;
// System.Func`2<System.Single,System.Object>
struct Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0;
// System.Func`2<System.Single,System.Single>
struct Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149;
// System.Func`2<System.Single,System.Boolean>
struct Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>
struct Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>
struct Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Boolean>
struct Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>
struct Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>
struct Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,System.Boolean>
struct Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>
struct Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>
struct Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>
struct Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>
struct Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7;
// System.Func`2<System.Object,System.Object>
struct Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436;
// System.Collections.Generic.IEnumerable`1<Wenzil.Console.ConsoleCommand>
struct IEnumerable_1_t6E4C8442CCE7BF4163DB73942535D17DDD52B21B;
// System.Collections.Generic.IEnumerable`1<System.Int32>
struct IEnumerable_1_t60929E1AA80B46746F987B99A4EBD004FD72D370;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Resolution>
struct IEnumerable_1_t6C00CEA5A06125C94ABA52C332306AF43C3D1BB0;
// System.Collections.Generic.IEnumerable`1<System.Single>
struct IEnumerable_1_t673DFF64E51C18A8B1287F4BD988966CE3B15A45;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct IEnumerable_1_t254DD0CAF887398174AE3DB4C2FF15F27F85E84F;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>>
struct IEnumerable_1_tC051C5D337193A7BD028891A65E2F22B81E4CC36;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t22A35158F9E40077A7147A082319C0D1DFFBE2FD;
// System.Collections.Generic.IEnumerator`1<Wenzil.Console.ConsoleCommand>
struct IEnumerator_1_t3315B7D76C93A7A9C77E64922B9EB30505E6CDBD;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t72AB4B40AF5290B386215B0BFADC8919D394DCAB;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Resolution>
struct IEnumerator_1_tB1FDBF90CBBB3494971D658FF4467633C43B51A3;
// System.Collections.Generic.IEnumerator`1<System.Single>
struct IEnumerator_1_t5918C99D6FA69C530D0287467B91ADE56FA3D7AF;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct IEnumerator_1_tDAF514E77572EC170A11F75AEB9612CC3816D919;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>>
struct IEnumerator_1_t2290F7FDB809558AB23EDA7F708A50EDEFB08866;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_tF437149CAED78D4A68294D431DE692A78F7D67B3;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2DC97C7D486BF9E077C2BC2E517E434F393AA76E;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_tE6A65C5E45E33FD7D9849FD0914DE3AD32B68050;
// System.Linq.Enumerable/Iterator`1<Wenzil.Console.ConsoleCommand>
struct Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2;
// System.Linq.Enumerable/Iterator`1<System.Int32>
struct Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379;
// System.Linq.Enumerable/Iterator`1<System.Object>
struct Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279;
// System.Linq.Enumerable/Iterator`1<System.Single>
struct Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.String,FullSerializer.fsData>
struct KeyCollection_t9C27345F3624A9ADE514DD0418B90BD992CD57A9;
// System.Collections.Generic.List`1<System.Char>
struct List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE;
// System.Collections.Generic.List`1<Mono.CSharp.IExpressionCleanup>
struct List_1_t0661DCFA1F47C9C1003923A2C7CD37916EA51D2B;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Int32Enum>
struct List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A;
// System.Collections.Generic.List`1<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency>
struct List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.Resolution>
struct List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9;
// System.Collections.Generic.List`1<Mono.CSharp.ResumableStatement>
struct List_1_t8614223ADB0F2D5A6C300250D505CE621C7CC277;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA;
// System.Collections.Generic.List`1<Mono.CSharp.SourceFile>
struct List_1_t223BD2C007E5133386D16B06104524B17BAF0FDC;
// System.Collections.Generic.List`1<Mono.CSharp.TryFinally>
struct List_1_tF2B1CABE3E8F4D589ACEB22D7E980DFECD8B99BF;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>>
struct List_1_t9B9E3A64698A414C03953277E8E6D511E145491F;
// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E;
// System.Collections.Generic.List`1<System.String>
struct List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.String,FullSerializer.fsData>
struct ValueCollection_t19F1D0DD58E5CB4CE34943813A0CFF639AAA7AC4;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Object>
struct WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Single>
struct WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Int32>
struct WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Object>
struct WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Single,System.Single>
struct WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>
struct WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>
struct WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>
struct WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>
struct WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>
struct WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>
struct WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>
struct WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303;
// System.Linq.Enumerable/WhereSelectEnumerableIterator`2<System.Object,System.Object>
struct WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Char,System.Int32>
struct WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Char,System.Object>
struct WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Char,System.Single>
struct WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Int32>
struct WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Object>
struct WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32,System.Single>
struct WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,System.Int32>
struct WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,System.Object>
struct WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Int32Enum,System.Single>
struct WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7;
// System.Linq.Enumerable/WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Int32>
struct WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353;
// System.Linq.Enumerable/WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Single>
struct WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F;
// System.Linq.Enumerable/WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Object>
struct WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Int32>
struct WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Single>
struct WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Resolution,System.Int32>
struct WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Resolution,System.Object>
struct WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE;
// System.Linq.Enumerable/WhereSelectListIterator`2<UnityEngine.Resolution,System.Single>
struct WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,System.Int32>
struct WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,System.Object>
struct WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Single,System.Single>
struct WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>
struct WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>
struct WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>
struct WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>
struct WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>
struct WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>
struct WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>
struct WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077;
// System.Linq.Enumerable/WhereSelectListIterator`2<System.Object,System.Object>
struct WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2;
// System.Xml.Linq.XHashtable`1/XHashtableState<System.Object>
struct XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE;
// System.Xml.Linq.XHashtable`1<System.Object>
struct XHashtable_1_tDCC8A8B3022A89A8902F5A8091A444BD7922AB37;
// Mono.CSharp.YieldStatement`1<System.Object>
struct YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5;
// FullSerializer.fsDirectConverter`1<DaggerfallConnect.DFBlock/RmbFldGroundData>
struct fsDirectConverter_1_tB5CD42BC188F3390971F3B024C91AA214F6979BB;
// FullSerializer.fsDirectConverter`1<System.Object>
struct fsDirectConverter_1_t4BF52AD38E426E1318FC9B7EA41EE9AE7D096B81;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// Mono.CSharp.Location/Checkpoint[]
struct CheckpointU5BU5D_t3F07D2BC7C10773FB53214893F6B88196DC7EFCD;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Collections.Generic.Dictionary`2/Entry<System.String,FullSerializer.fsData>[]
struct EntryU5BU5D_t4AD5062630B4BE21BD0F344FB547FCD336883843;
// System.Xml.Linq.XHashtable`1/XHashtableState/Entry<System.Object>[]
struct EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E;
// System.Reflection.Emit.ILGenerator
struct ILGenerator_tCB47F61B7259CF97E8239F921A474B2BEEF84F8F;
// System.Int32Enum[]
struct Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// System.InvalidOperationException
struct InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB;
// System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>[]
struct KeyValuePair_2U5BU5D_t5FCF4CDE71C93AE134C148C0B3CF24627C714462;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_tA780E964000F617CC6335A0DEC92B09FE0085E1C;
// System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>[]
struct KeyValuePair_2U5BU5D_tE5F173470DCC4B3AD82018FF5FA1EDCC277EA78F;
// System.Reflection.Emit.LocalBuilder
struct LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16;
// DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency[]
struct ModDependencyU5BU5D_tD66380C512C3DF4EF4136EF7FADA98A3A34E0004;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// System.OverflowException
struct OverflowException_tD1FBF4E54D81EC98EEF386B69344D336D1EC1AB9;
// UnityEngine.Resolution[]
struct ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597;
// DaggerfallConnect.DFBlock/RmbGroundScenery[0...,0...]
struct RmbGroundSceneryU5BU2CU5D_t14746C06BF70A5F821EE3DC6EA2E4D79655E89DD;
// DaggerfallConnect.DFBlock/RmbGroundTiles[0...,0...]
struct RmbGroundTilesU5BU2CU5D_tDA7F1928E558C6667D20D291B171656B453EDC29;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// FullSerializer.Internal.fsVersionedType[]
struct fsVersionedTypeU5BU5D_tC366477451B99788E59FE1CCF4AF6C3AC985AD72;

IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OverflowException_tD1FBF4E54D81EC98EEF386B69344D336D1EC1AB9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0A7D3B18DF4F9238A50A156FF06A5A7E794C1C7F;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_mB6E331DE1E5FCDA60F4997990B21B6BDA65AC4E6_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* XHashtableState_Resize_m2F60ACE78E15F4F50EAD11B7DA897A2BB7B7362A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* fsOption_1_get_Value_m3D1611CEBDB9440CC8285C9814801A410042CD4A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* fsOption_1_get_Value_m61B93A2F2EDD9AB4AB9A94DE75103A39EC833F66_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_m03C13B47EFC2336E91F60AAEC480568F289F373A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_m0B450345EA19C2C7D67B7997473865A140E2AA0D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_m169091704DEF5029F433F02CC8F0B026518A3856_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_m5529937A5FB75E1505AA643FB485895B87A1438F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_m7E83098ACC9FD3D3AD5EB79A9B291A0C7C2AAF45_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_m96716ABDF1C57CFB7D425C5C2E29D43895175F98_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_mAA70577DEF67CEC98FE677984AE2175B7D4E4D00_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_mAB2AD9EAF60076FC567705357B1A87DCC0FDC6F7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_mD57DB85A1263CB1310548DB5CEA8C4ADBE0CC22B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_mD8FC2FAD9ABE52E2A164F6804FC6A5B88DEA246B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_mE8ED82A82D2D3C5700EFE62D6BDB8195AEFEFA3C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_mF7369CBD265AEF953AE723D80982F581FB2FD45F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_Dispose_mF7B907536C0BA14EF57C92CF190037065BFD02F3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_m03576C66B9D242B193B03F095D352EE362D9777B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_m075453CF074406B30F0C205C8FDD6FAD7A91081D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_m2B4B1B4542AA0029E6A8EEDEA52E10C0B9A05DA9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_m60986EAF84DA044041E9F5AF74F5A1F2D9D287B6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_m8102780893AF990BA69A9F746059ADECF5AAC5FA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_m902032CD1F689E6733927A473F43F1495920D802_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_m95AEE737A22EFFFE6557F448BF5AFCC6241D0BD7_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_m9AED0076EB03B4E46908FB05BEB74852F542382E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_mAD7BCF14B4CD4C276B3D022E0EE2E390E2672FB6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_mC0C5F211EC8D720A808897439D1B86D279BF5616_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_mC137E9D24D354B452D9B70E9968CA8DE3E9AB373_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_mC788302A5CB47DB612B108BB2B0D4FF71E0534AA_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhereSelectEnumerableIterator_2_MoveNext_mFECC995DE8883D132C0C8817C1E52F0D2A0EE794_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XHashtableState_Resize_m2F60ACE78E15F4F50EAD11B7DA897A2BB7B7362A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t XHashtableState__ctor_mE68E63173C17EF13FA0F533F0AA34F4FA753674F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t fsDirectConverter_1_TryDeserialize_m71932F844FD53E71380641D6E13BB6AC92BD9B08_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t fsDirectConverter_1_TryDeserialize_m97AC4DED02F467623B8104D7ABAF8728EE844B90_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t fsDirectConverter_1_TrySerialize_mAB61D2E0C70DC296480D4DB72C12882A06F89443_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t fsDirectConverter_1_TrySerialize_mEEC3432A88B41A12BFF8D2A628E1DFA97C688871_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t fsDirectConverter_1_get_ModelType_m45E6E9FCD79B3081F985100AC97EBA45259E125B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t fsDirectConverter_1_get_ModelType_mB29327C4E1AD39E1409ACA4775B7871027298CBC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t fsOption_1_get_Value_m3D1611CEBDB9440CC8285C9814801A410042CD4A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t fsOption_1_get_Value_m61B93A2F2EDD9AB4AB9A94DE75103A39EC833F66_MetadataUsageId;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;
struct fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23_marshaled_com;
struct fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23_marshaled_pinvoke;

struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// FullSerializer.fsBaseConverter
struct  fsBaseConverter_tF4DC7258DCD56EA3FF50C3FD608C8F31631BF8CC  : public RuntimeObject
{
public:
	// FullSerializer.fsSerializer FullSerializer.fsBaseConverter::Serializer
	fsSerializer_tB6ACF6E73C607ABBD52D5C5964AE409C78994710 * ___Serializer_0;

public:
	inline static int32_t get_offset_of_Serializer_0() { return static_cast<int32_t>(offsetof(fsBaseConverter_tF4DC7258DCD56EA3FF50C3FD608C8F31631BF8CC, ___Serializer_0)); }
	inline fsSerializer_tB6ACF6E73C607ABBD52D5C5964AE409C78994710 * get_Serializer_0() const { return ___Serializer_0; }
	inline fsSerializer_tB6ACF6E73C607ABBD52D5C5964AE409C78994710 ** get_address_of_Serializer_0() { return &___Serializer_0; }
	inline void set_Serializer_0(fsSerializer_tB6ACF6E73C607ABBD52D5C5964AE409C78994710 * value)
	{
		___Serializer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Serializer_0), (void*)value);
	}
};


// FullSerializer.fsData
struct  fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16  : public RuntimeObject
{
public:
	// System.Object FullSerializer.fsData::_value
	RuntimeObject * ____value_0;

public:
	inline static int32_t get_offset_of__value_0() { return static_cast<int32_t>(offsetof(fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16, ____value_0)); }
	inline RuntimeObject * get__value_0() const { return ____value_0; }
	inline RuntimeObject ** get_address_of__value_0() { return &____value_0; }
	inline void set__value_0(RuntimeObject * value)
	{
		____value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____value_0), (void*)value);
	}
};

struct fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16_StaticFields
{
public:
	// FullSerializer.fsData FullSerializer.fsData::True
	fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * ___True_1;
	// FullSerializer.fsData FullSerializer.fsData::False
	fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * ___False_2;
	// FullSerializer.fsData FullSerializer.fsData::Null
	fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * ___Null_3;

public:
	inline static int32_t get_offset_of_True_1() { return static_cast<int32_t>(offsetof(fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16_StaticFields, ___True_1)); }
	inline fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * get_True_1() const { return ___True_1; }
	inline fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 ** get_address_of_True_1() { return &___True_1; }
	inline void set_True_1(fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * value)
	{
		___True_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___True_1), (void*)value);
	}

	inline static int32_t get_offset_of_False_2() { return static_cast<int32_t>(offsetof(fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16_StaticFields, ___False_2)); }
	inline fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * get_False_2() const { return ___False_2; }
	inline fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 ** get_address_of_False_2() { return &___False_2; }
	inline void set_False_2(fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * value)
	{
		___False_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___False_2), (void*)value);
	}

	inline static int32_t get_offset_of_Null_3() { return static_cast<int32_t>(offsetof(fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16_StaticFields, ___Null_3)); }
	inline fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * get_Null_3() const { return ___Null_3; }
	inline fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 ** get_address_of_Null_3() { return &___Null_3; }
	inline void set_Null_3(fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * value)
	{
		___Null_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Null_3), (void*)value);
	}
};


// Mono.CSharp.CloneContext
struct  CloneContext_t05693E643AAC9CB5A95947DEC8B8CA5EF1F49B2A  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Mono.CSharp.Block,Mono.CSharp.Block> Mono.CSharp.CloneContext::block_map
	Dictionary_2_t188E3037A86CB72A8BB65E4A73A2BB5FC43E37AF * ___block_map_0;

public:
	inline static int32_t get_offset_of_block_map_0() { return static_cast<int32_t>(offsetof(CloneContext_t05693E643AAC9CB5A95947DEC8B8CA5EF1F49B2A, ___block_map_0)); }
	inline Dictionary_2_t188E3037A86CB72A8BB65E4A73A2BB5FC43E37AF * get_block_map_0() const { return ___block_map_0; }
	inline Dictionary_2_t188E3037A86CB72A8BB65E4A73A2BB5FC43E37AF ** get_address_of_block_map_0() { return &___block_map_0; }
	inline void set_block_map_0(Dictionary_2_t188E3037A86CB72A8BB65E4A73A2BB5FC43E37AF * value)
	{
		___block_map_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___block_map_0), (void*)value);
	}
};


// Mono.CSharp.FlowAnalysisContext
struct  FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3  : public RuntimeObject
{
public:
	// Mono.CSharp.CompilerContext Mono.CSharp.FlowAnalysisContext::ctx
	CompilerContext_t4F0AFBF2AD3D5DF37C68BFD853D437A152C9B5A0 * ___ctx_0;
	// Mono.CSharp.DefiniteAssignmentBitSet Mono.CSharp.FlowAnalysisContext::conditional_access
	DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * ___conditional_access_1;
	// Mono.CSharp.DefiniteAssignmentBitSet Mono.CSharp.FlowAnalysisContext::<DefiniteAssignment>k__BackingField
	DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * ___U3CDefiniteAssignmentU3Ek__BackingField_2;
	// Mono.CSharp.DefiniteAssignmentBitSet Mono.CSharp.FlowAnalysisContext::<DefiniteAssignmentOnTrue>k__BackingField
	DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * ___U3CDefiniteAssignmentOnTrueU3Ek__BackingField_3;
	// Mono.CSharp.DefiniteAssignmentBitSet Mono.CSharp.FlowAnalysisContext::<DefiniteAssignmentOnFalse>k__BackingField
	DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * ___U3CDefiniteAssignmentOnFalseU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<Mono.CSharp.Statement,System.Collections.Generic.List`1<Mono.CSharp.DefiniteAssignmentBitSet>> Mono.CSharp.FlowAnalysisContext::<LabelStack>k__BackingField
	Dictionary_2_t0FF34CAAFD5B474F756CC87E236600B006A9F5FA * ___U3CLabelStackU3Ek__BackingField_5;
	// Mono.CSharp.ParametersBlock Mono.CSharp.FlowAnalysisContext::<ParametersBlock>k__BackingField
	ParametersBlock_t30562D7F0452D25CD1537D1FE2F9DF8F32130D5A * ___U3CParametersBlockU3Ek__BackingField_6;
	// Mono.CSharp.DefiniteAssignmentBitSet Mono.CSharp.FlowAnalysisContext::<SwitchInitialDefinitiveAssignment>k__BackingField
	DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * ___U3CSwitchInitialDefinitiveAssignmentU3Ek__BackingField_7;
	// Mono.CSharp.TryFinally Mono.CSharp.FlowAnalysisContext::<TryFinally>k__BackingField
	TryFinally_t9B9CAAEE26F075D9241144D094E26B5631AE68F3 * ___U3CTryFinallyU3Ek__BackingField_8;
	// System.Boolean Mono.CSharp.FlowAnalysisContext::<UnreachableReported>k__BackingField
	bool ___U3CUnreachableReportedU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_ctx_0() { return static_cast<int32_t>(offsetof(FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3, ___ctx_0)); }
	inline CompilerContext_t4F0AFBF2AD3D5DF37C68BFD853D437A152C9B5A0 * get_ctx_0() const { return ___ctx_0; }
	inline CompilerContext_t4F0AFBF2AD3D5DF37C68BFD853D437A152C9B5A0 ** get_address_of_ctx_0() { return &___ctx_0; }
	inline void set_ctx_0(CompilerContext_t4F0AFBF2AD3D5DF37C68BFD853D437A152C9B5A0 * value)
	{
		___ctx_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ctx_0), (void*)value);
	}

	inline static int32_t get_offset_of_conditional_access_1() { return static_cast<int32_t>(offsetof(FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3, ___conditional_access_1)); }
	inline DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * get_conditional_access_1() const { return ___conditional_access_1; }
	inline DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 ** get_address_of_conditional_access_1() { return &___conditional_access_1; }
	inline void set_conditional_access_1(DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * value)
	{
		___conditional_access_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___conditional_access_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDefiniteAssignmentU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3, ___U3CDefiniteAssignmentU3Ek__BackingField_2)); }
	inline DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * get_U3CDefiniteAssignmentU3Ek__BackingField_2() const { return ___U3CDefiniteAssignmentU3Ek__BackingField_2; }
	inline DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 ** get_address_of_U3CDefiniteAssignmentU3Ek__BackingField_2() { return &___U3CDefiniteAssignmentU3Ek__BackingField_2; }
	inline void set_U3CDefiniteAssignmentU3Ek__BackingField_2(DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * value)
	{
		___U3CDefiniteAssignmentU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDefiniteAssignmentU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDefiniteAssignmentOnTrueU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3, ___U3CDefiniteAssignmentOnTrueU3Ek__BackingField_3)); }
	inline DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * get_U3CDefiniteAssignmentOnTrueU3Ek__BackingField_3() const { return ___U3CDefiniteAssignmentOnTrueU3Ek__BackingField_3; }
	inline DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 ** get_address_of_U3CDefiniteAssignmentOnTrueU3Ek__BackingField_3() { return &___U3CDefiniteAssignmentOnTrueU3Ek__BackingField_3; }
	inline void set_U3CDefiniteAssignmentOnTrueU3Ek__BackingField_3(DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * value)
	{
		___U3CDefiniteAssignmentOnTrueU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDefiniteAssignmentOnTrueU3Ek__BackingField_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CDefiniteAssignmentOnFalseU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3, ___U3CDefiniteAssignmentOnFalseU3Ek__BackingField_4)); }
	inline DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * get_U3CDefiniteAssignmentOnFalseU3Ek__BackingField_4() const { return ___U3CDefiniteAssignmentOnFalseU3Ek__BackingField_4; }
	inline DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 ** get_address_of_U3CDefiniteAssignmentOnFalseU3Ek__BackingField_4() { return &___U3CDefiniteAssignmentOnFalseU3Ek__BackingField_4; }
	inline void set_U3CDefiniteAssignmentOnFalseU3Ek__BackingField_4(DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * value)
	{
		___U3CDefiniteAssignmentOnFalseU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDefiniteAssignmentOnFalseU3Ek__BackingField_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CLabelStackU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3, ___U3CLabelStackU3Ek__BackingField_5)); }
	inline Dictionary_2_t0FF34CAAFD5B474F756CC87E236600B006A9F5FA * get_U3CLabelStackU3Ek__BackingField_5() const { return ___U3CLabelStackU3Ek__BackingField_5; }
	inline Dictionary_2_t0FF34CAAFD5B474F756CC87E236600B006A9F5FA ** get_address_of_U3CLabelStackU3Ek__BackingField_5() { return &___U3CLabelStackU3Ek__BackingField_5; }
	inline void set_U3CLabelStackU3Ek__BackingField_5(Dictionary_2_t0FF34CAAFD5B474F756CC87E236600B006A9F5FA * value)
	{
		___U3CLabelStackU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CLabelStackU3Ek__BackingField_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CParametersBlockU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3, ___U3CParametersBlockU3Ek__BackingField_6)); }
	inline ParametersBlock_t30562D7F0452D25CD1537D1FE2F9DF8F32130D5A * get_U3CParametersBlockU3Ek__BackingField_6() const { return ___U3CParametersBlockU3Ek__BackingField_6; }
	inline ParametersBlock_t30562D7F0452D25CD1537D1FE2F9DF8F32130D5A ** get_address_of_U3CParametersBlockU3Ek__BackingField_6() { return &___U3CParametersBlockU3Ek__BackingField_6; }
	inline void set_U3CParametersBlockU3Ek__BackingField_6(ParametersBlock_t30562D7F0452D25CD1537D1FE2F9DF8F32130D5A * value)
	{
		___U3CParametersBlockU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CParametersBlockU3Ek__BackingField_6), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSwitchInitialDefinitiveAssignmentU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3, ___U3CSwitchInitialDefinitiveAssignmentU3Ek__BackingField_7)); }
	inline DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * get_U3CSwitchInitialDefinitiveAssignmentU3Ek__BackingField_7() const { return ___U3CSwitchInitialDefinitiveAssignmentU3Ek__BackingField_7; }
	inline DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 ** get_address_of_U3CSwitchInitialDefinitiveAssignmentU3Ek__BackingField_7() { return &___U3CSwitchInitialDefinitiveAssignmentU3Ek__BackingField_7; }
	inline void set_U3CSwitchInitialDefinitiveAssignmentU3Ek__BackingField_7(DefiniteAssignmentBitSet_t490BC01A20E645332593E16251F1D889701C49E5 * value)
	{
		___U3CSwitchInitialDefinitiveAssignmentU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSwitchInitialDefinitiveAssignmentU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTryFinallyU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3, ___U3CTryFinallyU3Ek__BackingField_8)); }
	inline TryFinally_t9B9CAAEE26F075D9241144D094E26B5631AE68F3 * get_U3CTryFinallyU3Ek__BackingField_8() const { return ___U3CTryFinallyU3Ek__BackingField_8; }
	inline TryFinally_t9B9CAAEE26F075D9241144D094E26B5631AE68F3 ** get_address_of_U3CTryFinallyU3Ek__BackingField_8() { return &___U3CTryFinallyU3Ek__BackingField_8; }
	inline void set_U3CTryFinallyU3Ek__BackingField_8(TryFinally_t9B9CAAEE26F075D9241144D094E26B5631AE68F3 * value)
	{
		___U3CTryFinallyU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTryFinallyU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CUnreachableReportedU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3, ___U3CUnreachableReportedU3Ek__BackingField_9)); }
	inline bool get_U3CUnreachableReportedU3Ek__BackingField_9() const { return ___U3CUnreachableReportedU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CUnreachableReportedU3Ek__BackingField_9() { return &___U3CUnreachableReportedU3Ek__BackingField_9; }
	inline void set_U3CUnreachableReportedU3Ek__BackingField_9(bool value)
	{
		___U3CUnreachableReportedU3Ek__BackingField_9 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>
struct  Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Collections.Generic.Dictionary`2_Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t4AD5062630B4BE21BD0F344FB547FCD336883843* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2_KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t9C27345F3624A9ADE514DD0418B90BD992CD57A9 * ___keys_7;
	// System.Collections.Generic.Dictionary`2_ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t19F1D0DD58E5CB4CE34943813A0CFF639AAA7AC4 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732, ___entries_1)); }
	inline EntryU5BU5D_t4AD5062630B4BE21BD0F344FB547FCD336883843* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t4AD5062630B4BE21BD0F344FB547FCD336883843** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t4AD5062630B4BE21BD0F344FB547FCD336883843* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732, ___keys_7)); }
	inline KeyCollection_t9C27345F3624A9ADE514DD0418B90BD992CD57A9 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t9C27345F3624A9ADE514DD0418B90BD992CD57A9 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t9C27345F3624A9ADE514DD0418B90BD992CD57A9 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732, ___values_8)); }
	inline ValueCollection_t19F1D0DD58E5CB4CE34943813A0CFF639AAA7AC4 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t19F1D0DD58E5CB4CE34943813A0CFF639AAA7AC4 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t19F1D0DD58E5CB4CE34943813A0CFF639AAA7AC4 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.List`1<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency>
struct  List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ModDependencyU5BU5D_tD66380C512C3DF4EF4136EF7FADA98A3A34E0004* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46, ____items_1)); }
	inline ModDependencyU5BU5D_tD66380C512C3DF4EF4136EF7FADA98A3A34E0004* get__items_1() const { return ____items_1; }
	inline ModDependencyU5BU5D_tD66380C512C3DF4EF4136EF7FADA98A3A34E0004** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ModDependencyU5BU5D_tD66380C512C3DF4EF4136EF7FADA98A3A34E0004* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ModDependencyU5BU5D_tD66380C512C3DF4EF4136EF7FADA98A3A34E0004* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46_StaticFields, ____emptyArray_5)); }
	inline ModDependencyU5BU5D_tD66380C512C3DF4EF4136EF7FADA98A3A34E0004* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ModDependencyU5BU5D_tD66380C512C3DF4EF4136EF7FADA98A3A34E0004** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ModDependencyU5BU5D_tD66380C512C3DF4EF4136EF7FADA98A3A34E0004* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Char>
struct  List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE, ____items_1)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get__items_1() const { return ____items_1; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE_StaticFields, ____emptyArray_5)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get__emptyArray_5() const { return ____emptyArray_5; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct  List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_t5FCF4CDE71C93AE134C148C0B3CF24627C714462* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7, ____items_1)); }
	inline KeyValuePair_2U5BU5D_t5FCF4CDE71C93AE134C148C0B3CF24627C714462* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_t5FCF4CDE71C93AE134C148C0B3CF24627C714462** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_t5FCF4CDE71C93AE134C148C0B3CF24627C714462* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	KeyValuePair_2U5BU5D_t5FCF4CDE71C93AE134C148C0B3CF24627C714462* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7_StaticFields, ____emptyArray_5)); }
	inline KeyValuePair_2U5BU5D_t5FCF4CDE71C93AE134C148C0B3CF24627C714462* get__emptyArray_5() const { return ____emptyArray_5; }
	inline KeyValuePair_2U5BU5D_t5FCF4CDE71C93AE134C148C0B3CF24627C714462** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(KeyValuePair_2U5BU5D_t5FCF4CDE71C93AE134C148C0B3CF24627C714462* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_tA780E964000F617CC6335A0DEC92B09FE0085E1C* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E, ____items_1)); }
	inline KeyValuePair_2U5BU5D_tA780E964000F617CC6335A0DEC92B09FE0085E1C* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_tA780E964000F617CC6335A0DEC92B09FE0085E1C** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_tA780E964000F617CC6335A0DEC92B09FE0085E1C* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	KeyValuePair_2U5BU5D_tA780E964000F617CC6335A0DEC92B09FE0085E1C* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E_StaticFields, ____emptyArray_5)); }
	inline KeyValuePair_2U5BU5D_tA780E964000F617CC6335A0DEC92B09FE0085E1C* get__emptyArray_5() const { return ____emptyArray_5; }
	inline KeyValuePair_2U5BU5D_tA780E964000F617CC6335A0DEC92B09FE0085E1C** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(KeyValuePair_2U5BU5D_tA780E964000F617CC6335A0DEC92B09FE0085E1C* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>>
struct  List_1_t9B9E3A64698A414C03953277E8E6D511E145491F  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	KeyValuePair_2U5BU5D_tE5F173470DCC4B3AD82018FF5FA1EDCC277EA78F* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t9B9E3A64698A414C03953277E8E6D511E145491F, ____items_1)); }
	inline KeyValuePair_2U5BU5D_tE5F173470DCC4B3AD82018FF5FA1EDCC277EA78F* get__items_1() const { return ____items_1; }
	inline KeyValuePair_2U5BU5D_tE5F173470DCC4B3AD82018FF5FA1EDCC277EA78F** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(KeyValuePair_2U5BU5D_tE5F173470DCC4B3AD82018FF5FA1EDCC277EA78F* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t9B9E3A64698A414C03953277E8E6D511E145491F, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t9B9E3A64698A414C03953277E8E6D511E145491F, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t9B9E3A64698A414C03953277E8E6D511E145491F, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t9B9E3A64698A414C03953277E8E6D511E145491F_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	KeyValuePair_2U5BU5D_tE5F173470DCC4B3AD82018FF5FA1EDCC277EA78F* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t9B9E3A64698A414C03953277E8E6D511E145491F_StaticFields, ____emptyArray_5)); }
	inline KeyValuePair_2U5BU5D_tE5F173470DCC4B3AD82018FF5FA1EDCC277EA78F* get__emptyArray_5() const { return ____emptyArray_5; }
	inline KeyValuePair_2U5BU5D_tE5F173470DCC4B3AD82018FF5FA1EDCC277EA78F** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(KeyValuePair_2U5BU5D_tE5F173470DCC4B3AD82018FF5FA1EDCC277EA78F* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32>
struct  List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____items_1)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__items_1() const { return ____items_1; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7_StaticFields, ____emptyArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Int32Enum>
struct  List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A, ____items_1)); }
	inline Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* get__items_1() const { return ____items_1; }
	inline Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A_StaticFields, ____emptyArray_5)); }
	inline Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* get__emptyArray_5() const { return ____emptyArray_5; }
	inline Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(Int32EnumU5BU5D_t9327F857579EE00EB201E1913599094BF837D3CD* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Object>
struct  List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<System.Single>
struct  List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____items_1)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get__items_1() const { return ____items_1; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA_StaticFields, ____emptyArray_5)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Resolution>
struct  List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9, ____items_1)); }
	inline ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597* get__items_1() const { return ____items_1; }
	inline ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9_StaticFields, ____emptyArray_5)); }
	inline ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ResolutionU5BU5D_t06BC9930CBEA8A2A4EEBA9534C2498E7CD0B5597* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.ComponentModel.TypeDescriptionProvider
struct  TypeDescriptionProvider_t122A1EB346B118B569F333CECF3DCB613CD793D0  : public RuntimeObject
{
public:

public:
};


// System.Linq.Enumerable_Iterator`1<System.Int32>
struct  Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable_Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable_Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable_Iterator`1::current
	int32_t ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379, ___current_2)); }
	inline int32_t get_current_2() const { return ___current_2; }
	inline int32_t* get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(int32_t value)
	{
		___current_2 = value;
	}
};


// System.Linq.Enumerable_Iterator`1<System.Object>
struct  Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable_Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable_Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable_Iterator`1::current
	RuntimeObject * ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279, ___current_2)); }
	inline RuntimeObject * get_current_2() const { return ___current_2; }
	inline RuntimeObject ** get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(RuntimeObject * value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_2), (void*)value);
	}
};


// System.Linq.Enumerable_Iterator`1<System.Single>
struct  Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable_Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable_Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable_Iterator`1::current
	float ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199, ___current_2)); }
	inline float get_current_2() const { return ___current_2; }
	inline float* get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(float value)
	{
		___current_2 = value;
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Xml.Linq.XHashtable`1_XHashtableState<System.Object>
struct  XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE  : public RuntimeObject
{
public:
	// System.Int32[] System.Xml.Linq.XHashtable`1_XHashtableState::buckets
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___buckets_0;
	// System.Xml.Linq.XHashtable`1_XHashtableState_Entry<TValue>[] System.Xml.Linq.XHashtable`1_XHashtableState::entries
	EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* ___entries_1;
	// System.Int32 System.Xml.Linq.XHashtable`1_XHashtableState::numEntries
	int32_t ___numEntries_2;
	// System.Xml.Linq.XHashtable`1_ExtractKeyDelegate<TValue> System.Xml.Linq.XHashtable`1_XHashtableState::extractKey
	ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 * ___extractKey_3;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE, ___buckets_0)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE, ___entries_1)); }
	inline EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_numEntries_2() { return static_cast<int32_t>(offsetof(XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE, ___numEntries_2)); }
	inline int32_t get_numEntries_2() const { return ___numEntries_2; }
	inline int32_t* get_address_of_numEntries_2() { return &___numEntries_2; }
	inline void set_numEntries_2(int32_t value)
	{
		___numEntries_2 = value;
	}

	inline static int32_t get_offset_of_extractKey_3() { return static_cast<int32_t>(offsetof(XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE, ___extractKey_3)); }
	inline ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 * get_extractKey_3() const { return ___extractKey_3; }
	inline ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 ** get_address_of_extractKey_3() { return &___extractKey_3; }
	inline void set_extractKey_3(ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 * value)
	{
		___extractKey_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___extractKey_3), (void*)value);
	}
};


// System.Xml.Linq.XHashtable`1<System.Object>
struct  XHashtable_1_tDCC8A8B3022A89A8902F5A8091A444BD7922AB37  : public RuntimeObject
{
public:
	// System.Xml.Linq.XHashtable`1_XHashtableState<TValue> System.Xml.Linq.XHashtable`1::state
	XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * ___state_0;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(XHashtable_1_tDCC8A8B3022A89A8902F5A8091A444BD7922AB37, ___state_0)); }
	inline XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * get_state_0() const { return ___state_0; }
	inline XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE ** get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * value)
	{
		___state_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___state_0), (void*)value);
	}
};


// DaggerfallConnect.DFBlock_RmbFldGroundData
struct  RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939 
{
public:
	// System.Byte[] DaggerfallConnect.DFBlock_RmbFldGroundData::Header
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___Header_0;
	// DaggerfallConnect.DFBlock_RmbGroundTiles[0...,0...] DaggerfallConnect.DFBlock_RmbFldGroundData::GroundTiles
	RmbGroundTilesU5BU2CU5D_tDA7F1928E558C6667D20D291B171656B453EDC29* ___GroundTiles_1;
	// DaggerfallConnect.DFBlock_RmbGroundScenery[0...,0...] DaggerfallConnect.DFBlock_RmbFldGroundData::GroundScenery
	RmbGroundSceneryU5BU2CU5D_t14746C06BF70A5F821EE3DC6EA2E4D79655E89DD* ___GroundScenery_2;

public:
	inline static int32_t get_offset_of_Header_0() { return static_cast<int32_t>(offsetof(RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939, ___Header_0)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_Header_0() const { return ___Header_0; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_Header_0() { return &___Header_0; }
	inline void set_Header_0(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___Header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Header_0), (void*)value);
	}

	inline static int32_t get_offset_of_GroundTiles_1() { return static_cast<int32_t>(offsetof(RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939, ___GroundTiles_1)); }
	inline RmbGroundTilesU5BU2CU5D_tDA7F1928E558C6667D20D291B171656B453EDC29* get_GroundTiles_1() const { return ___GroundTiles_1; }
	inline RmbGroundTilesU5BU2CU5D_tDA7F1928E558C6667D20D291B171656B453EDC29** get_address_of_GroundTiles_1() { return &___GroundTiles_1; }
	inline void set_GroundTiles_1(RmbGroundTilesU5BU2CU5D_tDA7F1928E558C6667D20D291B171656B453EDC29* value)
	{
		___GroundTiles_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GroundTiles_1), (void*)value);
	}

	inline static int32_t get_offset_of_GroundScenery_2() { return static_cast<int32_t>(offsetof(RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939, ___GroundScenery_2)); }
	inline RmbGroundSceneryU5BU2CU5D_t14746C06BF70A5F821EE3DC6EA2E4D79655E89DD* get_GroundScenery_2() const { return ___GroundScenery_2; }
	inline RmbGroundSceneryU5BU2CU5D_t14746C06BF70A5F821EE3DC6EA2E4D79655E89DD** get_address_of_GroundScenery_2() { return &___GroundScenery_2; }
	inline void set_GroundScenery_2(RmbGroundSceneryU5BU2CU5D_t14746C06BF70A5F821EE3DC6EA2E4D79655E89DD* value)
	{
		___GroundScenery_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GroundScenery_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of DaggerfallConnect.DFBlock/RmbFldGroundData
struct RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939_marshaled_pinvoke
{
	Il2CppSafeArray/*NONE*/* ___Header_0;
	RmbGroundTilesU5BU2CU5D_tDA7F1928E558C6667D20D291B171656B453EDC29* ___GroundTiles_1;
	RmbGroundSceneryU5BU2CU5D_t14746C06BF70A5F821EE3DC6EA2E4D79655E89DD* ___GroundScenery_2;
};
// Native definition for COM marshalling of DaggerfallConnect.DFBlock/RmbFldGroundData
struct RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939_marshaled_com
{
	Il2CppSafeArray/*NONE*/* ___Header_0;
	RmbGroundTilesU5BU2CU5D_tDA7F1928E558C6667D20D291B171656B453EDC29* ___GroundTiles_1;
	RmbGroundSceneryU5BU2CU5D_t14746C06BF70A5F821EE3DC6EA2E4D79655E89DD* ___GroundScenery_2;
};

// DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency
struct  ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D 
{
public:
	// System.String DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency::Name
	String_t* ___Name_0;
	// System.Boolean DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency::IsOptional
	bool ___IsOptional_1;
	// System.Boolean DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency::IsPeer
	bool ___IsPeer_2;
	// System.String DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency::Version
	String_t* ___Version_3;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Name_0), (void*)value);
	}

	inline static int32_t get_offset_of_IsOptional_1() { return static_cast<int32_t>(offsetof(ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D, ___IsOptional_1)); }
	inline bool get_IsOptional_1() const { return ___IsOptional_1; }
	inline bool* get_address_of_IsOptional_1() { return &___IsOptional_1; }
	inline void set_IsOptional_1(bool value)
	{
		___IsOptional_1 = value;
	}

	inline static int32_t get_offset_of_IsPeer_2() { return static_cast<int32_t>(offsetof(ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D, ___IsPeer_2)); }
	inline bool get_IsPeer_2() const { return ___IsPeer_2; }
	inline bool* get_address_of_IsPeer_2() { return &___IsPeer_2; }
	inline void set_IsPeer_2(bool value)
	{
		___IsPeer_2 = value;
	}

	inline static int32_t get_offset_of_Version_3() { return static_cast<int32_t>(offsetof(ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D, ___Version_3)); }
	inline String_t* get_Version_3() const { return ___Version_3; }
	inline String_t** get_address_of_Version_3() { return &___Version_3; }
	inline void set_Version_3(String_t* value)
	{
		___Version_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Version_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency
struct ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D_marshaled_pinvoke
{
	char* ___Name_0;
	int32_t ___IsOptional_1;
	int32_t ___IsPeer_2;
	char* ___Version_3;
};
// Native definition for COM marshalling of DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency
struct ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D_marshaled_com
{
	Il2CppChar* ___Name_0;
	int32_t ___IsOptional_1;
	int32_t ___IsPeer_2;
	Il2CppChar* ___Version_3;
};

// FullSerializer.Internal.fsOption`1<System.Object>
struct  fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 
{
public:
	// System.Boolean FullSerializer.Internal.fsOption`1::_hasValue
	bool ____hasValue_0;
	// T FullSerializer.Internal.fsOption`1::_value
	RuntimeObject * ____value_1;

public:
	inline static int32_t get_offset_of__hasValue_0() { return static_cast<int32_t>(offsetof(fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5, ____hasValue_0)); }
	inline bool get__hasValue_0() const { return ____hasValue_0; }
	inline bool* get_address_of__hasValue_0() { return &____hasValue_0; }
	inline void set__hasValue_0(bool value)
	{
		____hasValue_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____value_1), (void*)value);
	}
};


// FullSerializer.Internal.fsVersionedType
struct  fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23 
{
public:
	// FullSerializer.Internal.fsVersionedType[] FullSerializer.Internal.fsVersionedType::Ancestors
	fsVersionedTypeU5BU5D_tC366477451B99788E59FE1CCF4AF6C3AC985AD72* ___Ancestors_0;
	// System.String FullSerializer.Internal.fsVersionedType::VersionString
	String_t* ___VersionString_1;
	// System.Type FullSerializer.Internal.fsVersionedType::ModelType
	Type_t * ___ModelType_2;

public:
	inline static int32_t get_offset_of_Ancestors_0() { return static_cast<int32_t>(offsetof(fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23, ___Ancestors_0)); }
	inline fsVersionedTypeU5BU5D_tC366477451B99788E59FE1CCF4AF6C3AC985AD72* get_Ancestors_0() const { return ___Ancestors_0; }
	inline fsVersionedTypeU5BU5D_tC366477451B99788E59FE1CCF4AF6C3AC985AD72** get_address_of_Ancestors_0() { return &___Ancestors_0; }
	inline void set_Ancestors_0(fsVersionedTypeU5BU5D_tC366477451B99788E59FE1CCF4AF6C3AC985AD72* value)
	{
		___Ancestors_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Ancestors_0), (void*)value);
	}

	inline static int32_t get_offset_of_VersionString_1() { return static_cast<int32_t>(offsetof(fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23, ___VersionString_1)); }
	inline String_t* get_VersionString_1() const { return ___VersionString_1; }
	inline String_t** get_address_of_VersionString_1() { return &___VersionString_1; }
	inline void set_VersionString_1(String_t* value)
	{
		___VersionString_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VersionString_1), (void*)value);
	}

	inline static int32_t get_offset_of_ModelType_2() { return static_cast<int32_t>(offsetof(fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23, ___ModelType_2)); }
	inline Type_t * get_ModelType_2() const { return ___ModelType_2; }
	inline Type_t ** get_address_of_ModelType_2() { return &___ModelType_2; }
	inline void set_ModelType_2(Type_t * value)
	{
		___ModelType_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ModelType_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of FullSerializer.Internal.fsVersionedType
struct fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23_marshaled_pinvoke
{
	fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23_marshaled_pinvoke* ___Ancestors_0;
	char* ___VersionString_1;
	Type_t * ___ModelType_2;
};
// Native definition for COM marshalling of FullSerializer.Internal.fsVersionedType
struct fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23_marshaled_com
{
	fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23_marshaled_com* ___Ancestors_0;
	Il2CppChar* ___VersionString_1;
	Type_t * ___ModelType_2;
};

// FullSerializer.fsDirectConverter
struct  fsDirectConverter_t41638BCC8EA7FFAC4B7A9C0797FC3E57D1ECF024  : public fsBaseConverter_tF4DC7258DCD56EA3FF50C3FD608C8F31631BF8CC
{
public:

public:
};


// FullSerializer.fsResult
struct  fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA 
{
public:
	// System.Boolean FullSerializer.fsResult::_success
	bool ____success_1;
	// System.Collections.Generic.List`1<System.String> FullSerializer.fsResult::_messages
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ____messages_2;

public:
	inline static int32_t get_offset_of__success_1() { return static_cast<int32_t>(offsetof(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA, ____success_1)); }
	inline bool get__success_1() const { return ____success_1; }
	inline bool* get_address_of__success_1() { return &____success_1; }
	inline void set__success_1(bool value)
	{
		____success_1 = value;
	}

	inline static int32_t get_offset_of__messages_2() { return static_cast<int32_t>(offsetof(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA, ____messages_2)); }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * get__messages_2() const { return ____messages_2; }
	inline List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 ** get_address_of__messages_2() { return &____messages_2; }
	inline void set__messages_2(List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * value)
	{
		____messages_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____messages_2), (void*)value);
	}
};

struct fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_StaticFields
{
public:
	// System.String[] FullSerializer.fsResult::EmptyStringArray
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* ___EmptyStringArray_0;
	// FullSerializer.fsResult FullSerializer.fsResult::Success
	fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  ___Success_3;

public:
	inline static int32_t get_offset_of_EmptyStringArray_0() { return static_cast<int32_t>(offsetof(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_StaticFields, ___EmptyStringArray_0)); }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* get_EmptyStringArray_0() const { return ___EmptyStringArray_0; }
	inline StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A** get_address_of_EmptyStringArray_0() { return &___EmptyStringArray_0; }
	inline void set_EmptyStringArray_0(StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* value)
	{
		___EmptyStringArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyStringArray_0), (void*)value);
	}

	inline static int32_t get_offset_of_Success_3() { return static_cast<int32_t>(offsetof(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_StaticFields, ___Success_3)); }
	inline fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  get_Success_3() const { return ___Success_3; }
	inline fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA * get_address_of_Success_3() { return &___Success_3; }
	inline void set_Success_3(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  value)
	{
		___Success_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___Success_3))->____messages_2), (void*)NULL);
	}
};

// Native definition for P/Invoke marshalling of FullSerializer.fsResult
struct fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_marshaled_pinvoke
{
	int32_t ____success_1;
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ____messages_2;
};
// Native definition for COM marshalling of FullSerializer.fsResult
struct fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_marshaled_com
{
	int32_t ____success_1;
	List_1_t6C9F81EDBF0F4A31A9B0DA372D2EF34BDA3A1AF3 * ____messages_2;
};

// MS.Internal.Xml.Linq.ComponentModel.XTypeDescriptionProvider`1<System.Object>
struct  XTypeDescriptionProvider_1_t3E70768CA58B1E0422A52E2D980397207AEC99DB  : public TypeDescriptionProvider_t122A1EB346B118B569F333CECF3DCB613CD793D0
{
public:

public:
};


// Mono.CSharp.Location
struct  Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2 
{
public:
	// System.Int32 Mono.CSharp.Location::token
	int32_t ___token_0;

public:
	inline static int32_t get_offset_of_token_0() { return static_cast<int32_t>(offsetof(Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2, ___token_0)); }
	inline int32_t get_token_0() const { return ___token_0; }
	inline int32_t* get_address_of_token_0() { return &___token_0; }
	inline void set_token_0(int32_t value)
	{
		___token_0 = value;
	}
};

struct Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2_StaticFields
{
public:
	// System.Collections.Generic.List`1<Mono.CSharp.SourceFile> Mono.CSharp.Location::source_list
	List_1_t223BD2C007E5133386D16B06104524B17BAF0FDC * ___source_list_1;
	// Mono.CSharp.Location_Checkpoint[] Mono.CSharp.Location::checkpoints
	CheckpointU5BU5D_t3F07D2BC7C10773FB53214893F6B88196DC7EFCD* ___checkpoints_2;
	// System.Int32 Mono.CSharp.Location::checkpoint_index
	int32_t ___checkpoint_index_3;
	// Mono.CSharp.Location Mono.CSharp.Location::Null
	Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2  ___Null_4;
	// System.Boolean Mono.CSharp.Location::InEmacs
	bool ___InEmacs_5;

public:
	inline static int32_t get_offset_of_source_list_1() { return static_cast<int32_t>(offsetof(Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2_StaticFields, ___source_list_1)); }
	inline List_1_t223BD2C007E5133386D16B06104524B17BAF0FDC * get_source_list_1() const { return ___source_list_1; }
	inline List_1_t223BD2C007E5133386D16B06104524B17BAF0FDC ** get_address_of_source_list_1() { return &___source_list_1; }
	inline void set_source_list_1(List_1_t223BD2C007E5133386D16B06104524B17BAF0FDC * value)
	{
		___source_list_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_list_1), (void*)value);
	}

	inline static int32_t get_offset_of_checkpoints_2() { return static_cast<int32_t>(offsetof(Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2_StaticFields, ___checkpoints_2)); }
	inline CheckpointU5BU5D_t3F07D2BC7C10773FB53214893F6B88196DC7EFCD* get_checkpoints_2() const { return ___checkpoints_2; }
	inline CheckpointU5BU5D_t3F07D2BC7C10773FB53214893F6B88196DC7EFCD** get_address_of_checkpoints_2() { return &___checkpoints_2; }
	inline void set_checkpoints_2(CheckpointU5BU5D_t3F07D2BC7C10773FB53214893F6B88196DC7EFCD* value)
	{
		___checkpoints_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___checkpoints_2), (void*)value);
	}

	inline static int32_t get_offset_of_checkpoint_index_3() { return static_cast<int32_t>(offsetof(Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2_StaticFields, ___checkpoint_index_3)); }
	inline int32_t get_checkpoint_index_3() const { return ___checkpoint_index_3; }
	inline int32_t* get_address_of_checkpoint_index_3() { return &___checkpoint_index_3; }
	inline void set_checkpoint_index_3(int32_t value)
	{
		___checkpoint_index_3 = value;
	}

	inline static int32_t get_offset_of_Null_4() { return static_cast<int32_t>(offsetof(Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2_StaticFields, ___Null_4)); }
	inline Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2  get_Null_4() const { return ___Null_4; }
	inline Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2 * get_address_of_Null_4() { return &___Null_4; }
	inline void set_Null_4(Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2  value)
	{
		___Null_4 = value;
	}

	inline static int32_t get_offset_of_InEmacs_5() { return static_cast<int32_t>(offsetof(Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2_StaticFields, ___InEmacs_5)); }
	inline bool get_InEmacs_5() const { return ___InEmacs_5; }
	inline bool* get_address_of_InEmacs_5() { return &___InEmacs_5; }
	inline void set_InEmacs_5(bool value)
	{
		___InEmacs_5 = value;
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Char
struct  Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14 
{
public:
	// System.Char System.Char::m_value
	Il2CppChar ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14, ___m_value_0)); }
	inline Il2CppChar get_m_value_0() const { return ___m_value_0; }
	inline Il2CppChar* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(Il2CppChar value)
	{
		___m_value_0 = value;
	}
};

struct Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields
{
public:
	// System.Byte[] System.Char::categoryForLatin1
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___categoryForLatin1_3;

public:
	inline static int32_t get_offset_of_categoryForLatin1_3() { return static_cast<int32_t>(offsetof(Char_tFF60D8E7E89A20BE2294A003734341BD1DF43E14_StaticFields, ___categoryForLatin1_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_categoryForLatin1_3() const { return ___categoryForLatin1_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_categoryForLatin1_3() { return &___categoryForLatin1_3; }
	inline void set_categoryForLatin1_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___categoryForLatin1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___categoryForLatin1_3), (void*)value);
	}
};


// System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>
struct  KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	int64_t ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2, ___key_0)); }
	inline int64_t get_key_0() const { return ___key_0; }
	inline int64_t* get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(int64_t value)
	{
		___key_0 = value;
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}
};


// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>
struct  KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625, ___value_1)); }
	inline RuntimeObject * get_value_1() const { return ___value_1; }
	inline RuntimeObject ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___value_1), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Char>
struct  Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	Il2CppChar ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F, ___list_0)); }
	inline List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * get_list_0() const { return ___list_0; }
	inline List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F, ___current_3)); }
	inline Il2CppChar get_current_3() const { return ___current_3; }
	inline Il2CppChar* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Il2CppChar value)
	{
		___current_3 = value;
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Int32>
struct  Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___list_0)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_list_0() const { return ___list_0; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Object>
struct  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	RuntimeObject * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___list_0)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_list_0() const { return ___list_0; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6, ___current_3)); }
	inline RuntimeObject * get_current_3() const { return ___current_3; }
	inline RuntimeObject ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(RuntimeObject * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_3), (void*)value);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Single>
struct  Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	float ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783, ___list_0)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_list_0() const { return ___list_0; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783, ___current_3)); }
	inline float get_current_3() const { return ___current_3; }
	inline float* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(float value)
	{
		___current_3 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Linq.Enumerable_WhereEnumerableIterator`1<System.Int32>
struct  WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereEnumerableIterator`1::predicate
	Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA, ___predicate_4)); }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable_WhereEnumerableIterator`1<System.Object>
struct  WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereEnumerableIterator`1::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable_WhereEnumerableIterator`1<System.Single>
struct  WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereEnumerableIterator`1::predicate
	Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F, ___predicate_4)); }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>
struct  WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734, ___predicate_4)); }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734, ___selector_5)); }
	inline Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * get_selector_5() const { return ___selector_5; }
	inline Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>
struct  WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442, ___predicate_4)); }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442, ___selector_5)); }
	inline Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>
struct  WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08, ___predicate_4)); }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08, ___selector_5)); }
	inline Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>
struct  WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F, ___predicate_4)); }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F, ___selector_5)); }
	inline Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>
struct  WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505, ___predicate_4)); }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505, ___selector_5)); }
	inline Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * get_selector_5() const { return ___selector_5; }
	inline Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>
struct  WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303, ___predicate_4)); }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303, ___selector_5)); }
	inline Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * get_selector_5() const { return ___selector_5; }
	inline Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Object,System.Object>
struct  WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___selector_5)); }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Int32>
struct  WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6, ___predicate_4)); }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6, ___selector_5)); }
	inline Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Object>
struct  WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A, ___predicate_4)); }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A, ___selector_5)); }
	inline Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Single>
struct  WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296, ___predicate_4)); }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296, ___selector_5)); }
	inline Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Object>
struct  WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5, ___predicate_4)); }
	inline Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5, ___selector_5)); }
	inline Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Single>
struct  WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF, ___predicate_4)); }
	inline Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF, ___selector_5)); }
	inline Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Reflection.Emit.Label
struct  Label_t15CF7D602807306CF6D6340B09720178CE278550 
{
public:
	// System.Int32 System.Reflection.Emit.Label::label
	int32_t ___label_0;

public:
	inline static int32_t get_offset_of_label_0() { return static_cast<int32_t>(offsetof(Label_t15CF7D602807306CF6D6340B09720178CE278550, ___label_0)); }
	inline int32_t get_label_0() const { return ___label_0; }
	inline int32_t* get_address_of_label_0() { return &___label_0; }
	inline void set_label_0(int32_t value)
	{
		___label_0 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Xml.Linq.XHashtable`1_XHashtableState_Entry<System.Object>
struct  Entry_t07D1CC404E65C6AB3CD8F93EC6546722B73EBF94 
{
public:
	// TValue System.Xml.Linq.XHashtable`1_XHashtableState_Entry::Value
	RuntimeObject * ___Value_0;
	// System.Int32 System.Xml.Linq.XHashtable`1_XHashtableState_Entry::HashCode
	int32_t ___HashCode_1;
	// System.Int32 System.Xml.Linq.XHashtable`1_XHashtableState_Entry::Next
	int32_t ___Next_2;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Entry_t07D1CC404E65C6AB3CD8F93EC6546722B73EBF94, ___Value_0)); }
	inline RuntimeObject * get_Value_0() const { return ___Value_0; }
	inline RuntimeObject ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(RuntimeObject * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}

	inline static int32_t get_offset_of_HashCode_1() { return static_cast<int32_t>(offsetof(Entry_t07D1CC404E65C6AB3CD8F93EC6546722B73EBF94, ___HashCode_1)); }
	inline int32_t get_HashCode_1() const { return ___HashCode_1; }
	inline int32_t* get_address_of_HashCode_1() { return &___HashCode_1; }
	inline void set_HashCode_1(int32_t value)
	{
		___HashCode_1 = value;
	}

	inline static int32_t get_offset_of_Next_2() { return static_cast<int32_t>(offsetof(Entry_t07D1CC404E65C6AB3CD8F93EC6546722B73EBF94, ___Next_2)); }
	inline int32_t get_Next_2() const { return ___Next_2; }
	inline int32_t* get_address_of_Next_2() { return &___Next_2; }
	inline void set_Next_2(int32_t value)
	{
		___Next_2 = value;
	}
};


// UnityEngine.Resolution
struct  Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 
{
public:
	// System.Int32 UnityEngine.Resolution::m_Width
	int32_t ___m_Width_0;
	// System.Int32 UnityEngine.Resolution::m_Height
	int32_t ___m_Height_1;
	// System.Int32 UnityEngine.Resolution::m_RefreshRate
	int32_t ___m_RefreshRate_2;

public:
	inline static int32_t get_offset_of_m_Width_0() { return static_cast<int32_t>(offsetof(Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767, ___m_Width_0)); }
	inline int32_t get_m_Width_0() const { return ___m_Width_0; }
	inline int32_t* get_address_of_m_Width_0() { return &___m_Width_0; }
	inline void set_m_Width_0(int32_t value)
	{
		___m_Width_0 = value;
	}

	inline static int32_t get_offset_of_m_Height_1() { return static_cast<int32_t>(offsetof(Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767, ___m_Height_1)); }
	inline int32_t get_m_Height_1() const { return ___m_Height_1; }
	inline int32_t* get_address_of_m_Height_1() { return &___m_Height_1; }
	inline void set_m_Height_1(int32_t value)
	{
		___m_Height_1 = value;
	}

	inline static int32_t get_offset_of_m_RefreshRate_2() { return static_cast<int32_t>(offsetof(Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767, ___m_RefreshRate_2)); }
	inline int32_t get_m_RefreshRate_2() const { return ___m_RefreshRate_2; }
	inline int32_t* get_address_of_m_RefreshRate_2() { return &___m_RefreshRate_2; }
	inline void set_m_RefreshRate_2(int32_t value)
	{
		___m_RefreshRate_2 = value;
	}
};


// Wenzil.Console.ConsoleCommand
struct  ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE 
{
public:
	// System.String Wenzil.Console.ConsoleCommand::<name>k__BackingField
	String_t* ___U3CnameU3Ek__BackingField_0;
	// System.String Wenzil.Console.ConsoleCommand::<description>k__BackingField
	String_t* ___U3CdescriptionU3Ek__BackingField_1;
	// System.String Wenzil.Console.ConsoleCommand::<usage>k__BackingField
	String_t* ___U3CusageU3Ek__BackingField_2;
	// Wenzil.Console.ConsoleCommandCallback Wenzil.Console.ConsoleCommand::<callback>k__BackingField
	ConsoleCommandCallback_t747E2C2159A300D81D5755E1385DA036AF8D2056 * ___U3CcallbackU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CnameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE, ___U3CnameU3Ek__BackingField_0)); }
	inline String_t* get_U3CnameU3Ek__BackingField_0() const { return ___U3CnameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CnameU3Ek__BackingField_0() { return &___U3CnameU3Ek__BackingField_0; }
	inline void set_U3CnameU3Ek__BackingField_0(String_t* value)
	{
		___U3CnameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CnameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CdescriptionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE, ___U3CdescriptionU3Ek__BackingField_1)); }
	inline String_t* get_U3CdescriptionU3Ek__BackingField_1() const { return ___U3CdescriptionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CdescriptionU3Ek__BackingField_1() { return &___U3CdescriptionU3Ek__BackingField_1; }
	inline void set_U3CdescriptionU3Ek__BackingField_1(String_t* value)
	{
		___U3CdescriptionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CdescriptionU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CusageU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE, ___U3CusageU3Ek__BackingField_2)); }
	inline String_t* get_U3CusageU3Ek__BackingField_2() const { return ___U3CusageU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CusageU3Ek__BackingField_2() { return &___U3CusageU3Ek__BackingField_2; }
	inline void set_U3CusageU3Ek__BackingField_2(String_t* value)
	{
		___U3CusageU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CusageU3Ek__BackingField_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcallbackU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE, ___U3CcallbackU3Ek__BackingField_3)); }
	inline ConsoleCommandCallback_t747E2C2159A300D81D5755E1385DA036AF8D2056 * get_U3CcallbackU3Ek__BackingField_3() const { return ___U3CcallbackU3Ek__BackingField_3; }
	inline ConsoleCommandCallback_t747E2C2159A300D81D5755E1385DA036AF8D2056 ** get_address_of_U3CcallbackU3Ek__BackingField_3() { return &___U3CcallbackU3Ek__BackingField_3; }
	inline void set_U3CcallbackU3Ek__BackingField_3(ConsoleCommandCallback_t747E2C2159A300D81D5755E1385DA036AF8D2056 * value)
	{
		___U3CcallbackU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcallbackU3Ek__BackingField_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Wenzil.Console.ConsoleCommand
struct ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE_marshaled_pinvoke
{
	char* ___U3CnameU3Ek__BackingField_0;
	char* ___U3CdescriptionU3Ek__BackingField_1;
	char* ___U3CusageU3Ek__BackingField_2;
	Il2CppMethodPointer ___U3CcallbackU3Ek__BackingField_3;
};
// Native definition for COM marshalling of Wenzil.Console.ConsoleCommand
struct ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE_marshaled_com
{
	Il2CppChar* ___U3CnameU3Ek__BackingField_0;
	Il2CppChar* ___U3CdescriptionU3Ek__BackingField_1;
	Il2CppChar* ___U3CusageU3Ek__BackingField_2;
	Il2CppMethodPointer ___U3CcallbackU3Ek__BackingField_3;
};

// FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>
struct  fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 
{
public:
	// System.Boolean FullSerializer.Internal.fsOption`1::_hasValue
	bool ____hasValue_0;
	// T FullSerializer.Internal.fsOption`1::_value
	fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  ____value_1;

public:
	inline static int32_t get_offset_of__hasValue_0() { return static_cast<int32_t>(offsetof(fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5, ____hasValue_0)); }
	inline bool get__hasValue_0() const { return ____hasValue_0; }
	inline bool* get_address_of__hasValue_0() { return &____hasValue_0; }
	inline void set__hasValue_0(bool value)
	{
		____hasValue_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5, ____value_1)); }
	inline fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  get__value_1() const { return ____value_1; }
	inline fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23 * get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&____value_1))->___Ancestors_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&____value_1))->___VersionString_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&____value_1))->___ModelType_2), (void*)NULL);
		#endif
	}
};


// FullSerializer.fsDataType
struct  fsDataType_t695AF95B41F96B4909DC5E4A42CA5A16B3C650E7 
{
public:
	// System.Int32 FullSerializer.fsDataType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(fsDataType_t695AF95B41F96B4909DC5E4A42CA5A16B3C650E7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FullSerializer.fsDirectConverter`1<DaggerfallConnect.DFBlock_RmbFldGroundData>
struct  fsDirectConverter_1_tB5CD42BC188F3390971F3B024C91AA214F6979BB  : public fsDirectConverter_t41638BCC8EA7FFAC4B7A9C0797FC3E57D1ECF024
{
public:

public:
};


// FullSerializer.fsDirectConverter`1<System.Object>
struct  fsDirectConverter_1_t4BF52AD38E426E1318FC9B7EA41EE9AE7D096B81  : public fsDirectConverter_t41638BCC8EA7FFAC4B7A9C0797FC3E57D1ECF024
{
public:

public:
};


// Mono.CSharp.BuilderContext_Options
struct  Options_t3D42AEF3FDC0CB793A1D47926990F37FB36BEE94 
{
public:
	// System.Int32 Mono.CSharp.BuilderContext_Options::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Options_t3D42AEF3FDC0CB793A1D47926990F37FB36BEE94, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Mono.CSharp.ExprClass
struct  ExprClass_t1E4B0D55C2AD936A774B9BABD8E2962D9F50B909 
{
public:
	// System.Byte Mono.CSharp.ExprClass::value__
	uint8_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ExprClass_t1E4B0D55C2AD936A774B9BABD8E2962D9F50B909, ___value___2)); }
	inline uint8_t get_value___2() const { return ___value___2; }
	inline uint8_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(uint8_t value)
	{
		___value___2 = value;
	}
};


// Mono.CSharp.ResolveContext_Options
struct  Options_t9EE45DF168706B425F310C407433CD4B79B2D491 
{
public:
	// System.Int32 Mono.CSharp.ResolveContext_Options::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Options_t9EE45DF168706B425F310C407433CD4B79B2D491, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Mono.CSharp.Statement
struct  Statement_t412FB2CAA6DCE73C11AD18A3F3E56815675E7D79  : public RuntimeObject
{
public:
	// Mono.CSharp.Location Mono.CSharp.Statement::loc
	Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2  ___loc_0;
	// System.Boolean Mono.CSharp.Statement::reachable
	bool ___reachable_1;

public:
	inline static int32_t get_offset_of_loc_0() { return static_cast<int32_t>(offsetof(Statement_t412FB2CAA6DCE73C11AD18A3F3E56815675E7D79, ___loc_0)); }
	inline Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2  get_loc_0() const { return ___loc_0; }
	inline Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2 * get_address_of_loc_0() { return &___loc_0; }
	inline void set_loc_0(Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2  value)
	{
		___loc_0 = value;
	}

	inline static int32_t get_offset_of_reachable_1() { return static_cast<int32_t>(offsetof(Statement_t412FB2CAA6DCE73C11AD18A3F3E56815675E7D79, ___reachable_1)); }
	inline bool get_reachable_1() const { return ___reachable_1; }
	inline bool* get_address_of_reachable_1() { return &___reachable_1; }
	inline void set_reachable_1(bool value)
	{
		___reachable_1 = value;
	}
};


// System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>
struct  KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	RuntimeObject * ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE  ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A, ___key_0)); }
	inline RuntimeObject * get_key_0() const { return ___key_0; }
	inline RuntimeObject ** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(RuntimeObject * value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___key_0), (void*)value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A, ___value_1)); }
	inline ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE  get_value_1() const { return ___value_1; }
	inline ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE * get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE  value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___U3CdescriptionU3Ek__BackingField_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___U3CusageU3Ek__BackingField_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___value_1))->___U3CcallbackU3Ek__BackingField_3), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1_Enumerator<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency>
struct  Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD, ___list_0)); }
	inline List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * get_list_0() const { return ___list_0; }
	inline List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD, ___current_3)); }
	inline ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  get_current_3() const { return ___current_3; }
	inline ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___Name_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___Version_3), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>
struct  Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6, ___list_0)); }
	inline List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * get_list_0() const { return ___list_0; }
	inline List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6, ___current_3)); }
	inline KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___value_1), (void*)NULL);
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct  Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04, ___list_0)); }
	inline List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * get_list_0() const { return ___list_0; }
	inline List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04, ___current_3)); }
	inline KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___value_1), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1_Enumerator<UnityEngine.Resolution>
struct  Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B, ___list_0)); }
	inline List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * get_list_0() const { return ___list_0; }
	inline List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B, ___current_3)); }
	inline Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  get_current_3() const { return ___current_3; }
	inline Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  value)
	{
		___current_3 = value;
	}
};


// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Int32Enum
struct  Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t9B63F771913F2B6D586F1173B44A41FBE26F6B5C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Linq.Enumerable_Iterator`1<Wenzil.Console.ConsoleCommand>
struct  Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2  : public RuntimeObject
{
public:
	// System.Int32 System.Linq.Enumerable_Iterator`1::threadId
	int32_t ___threadId_0;
	// System.Int32 System.Linq.Enumerable_Iterator`1::state
	int32_t ___state_1;
	// TSource System.Linq.Enumerable_Iterator`1::current
	ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE  ___current_2;

public:
	inline static int32_t get_offset_of_threadId_0() { return static_cast<int32_t>(offsetof(Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2, ___threadId_0)); }
	inline int32_t get_threadId_0() const { return ___threadId_0; }
	inline int32_t* get_address_of_threadId_0() { return &___threadId_0; }
	inline void set_threadId_0(int32_t value)
	{
		___threadId_0 = value;
	}

	inline static int32_t get_offset_of_state_1() { return static_cast<int32_t>(offsetof(Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2, ___state_1)); }
	inline int32_t get_state_1() const { return ___state_1; }
	inline int32_t* get_address_of_state_1() { return &___state_1; }
	inline void set_state_1(int32_t value)
	{
		___state_1 = value;
	}

	inline static int32_t get_offset_of_current_2() { return static_cast<int32_t>(offsetof(Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2, ___current_2)); }
	inline ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE  get_current_2() const { return ___current_2; }
	inline ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE * get_address_of_current_2() { return &___current_2; }
	inline void set_current_2(ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE  value)
	{
		___current_2 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___U3CdescriptionU3Ek__BackingField_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___U3CusageU3Ek__BackingField_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_2))->___U3CcallbackU3Ek__BackingField_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Int32>
struct  WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D, ___source_3)); }
	inline List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * get_source_3() const { return ___source_3; }
	inline List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D, ___predicate_4)); }
	inline Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D, ___selector_5)); }
	inline Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D, ___enumerator_6)); }
	inline Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Object>
struct  WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9, ___source_3)); }
	inline List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * get_source_3() const { return ___source_3; }
	inline List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9, ___predicate_4)); }
	inline Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9, ___selector_5)); }
	inline Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E * get_selector_5() const { return ___selector_5; }
	inline Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9, ___enumerator_6)); }
	inline Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Single>
struct  WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7, ___source_3)); }
	inline List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * get_source_3() const { return ___source_3; }
	inline List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7, ___predicate_4)); }
	inline Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7, ___selector_5)); }
	inline Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F * get_selector_5() const { return ___selector_5; }
	inline Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7, ___enumerator_6)); }
	inline Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Int32>
struct  WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325, ___source_3)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_source_3() const { return ___source_3; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325, ___predicate_4)); }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325, ___selector_5)); }
	inline Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325, ___enumerator_6)); }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Object>
struct  WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4, ___source_3)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_source_3() const { return ___source_3; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4, ___predicate_4)); }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4, ___selector_5)); }
	inline Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4, ___enumerator_6)); }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Single>
struct  WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06, ___source_3)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_source_3() const { return ___source_3; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06, ___predicate_4)); }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06, ___selector_5)); }
	inline Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06, ___enumerator_6)); }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Int32>
struct  WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6, ___selector_5)); }
	inline Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * get_selector_5() const { return ___selector_5; }
	inline Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Object>
struct  WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___selector_5)); }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Single>
struct  WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F, ___source_3)); }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * get_source_3() const { return ___source_3; }
	inline List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F, ___predicate_4)); }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F, ___selector_5)); }
	inline Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F, ___enumerator_6)); }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___current_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Int32>
struct  WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775, ___source_3)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_source_3() const { return ___source_3; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775, ___predicate_4)); }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775, ___selector_5)); }
	inline Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775, ___enumerator_6)); }
	inline Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Object>
struct  WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259, ___source_3)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_source_3() const { return ___source_3; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259, ___predicate_4)); }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259, ___selector_5)); }
	inline Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259, ___enumerator_6)); }
	inline Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Single>
struct  WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB, ___source_3)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_source_3() const { return ___source_3; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB, ___predicate_4)); }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB, ___selector_5)); }
	inline Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB, ___enumerator_6)); }
	inline Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Nullable`1<System.Reflection.Emit.Label>
struct  Nullable_1_t187DA0097AAD049050BE4EE2A751D749D971B43B 
{
public:
	// T System.Nullable`1::value
	Label_t15CF7D602807306CF6D6340B09720178CE278550  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t187DA0097AAD049050BE4EE2A751D749D971B43B, ___value_0)); }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550  get_value_0() const { return ___value_0; }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Label_t15CF7D602807306CF6D6340B09720178CE278550  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t187DA0097AAD049050BE4EE2A751D749D971B43B, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};


// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// Mono.CSharp.BuilderContext
struct  BuilderContext_t1A35BCA7F6FD35BA145731728D9F17DC32A46990  : public RuntimeObject
{
public:
	// Mono.CSharp.BuilderContext_Options Mono.CSharp.BuilderContext::flags
	int32_t ___flags_0;

public:
	inline static int32_t get_offset_of_flags_0() { return static_cast<int32_t>(offsetof(BuilderContext_t1A35BCA7F6FD35BA145731728D9F17DC32A46990, ___flags_0)); }
	inline int32_t get_flags_0() const { return ___flags_0; }
	inline int32_t* get_address_of_flags_0() { return &___flags_0; }
	inline void set_flags_0(int32_t value)
	{
		___flags_0 = value;
	}
};


// Mono.CSharp.Expression
struct  Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31  : public RuntimeObject
{
public:
	// Mono.CSharp.ExprClass Mono.CSharp.Expression::eclass
	uint8_t ___eclass_0;
	// Mono.CSharp.TypeSpec Mono.CSharp.Expression::type
	TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * ___type_1;
	// Mono.CSharp.Location Mono.CSharp.Expression::loc
	Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2  ___loc_2;

public:
	inline static int32_t get_offset_of_eclass_0() { return static_cast<int32_t>(offsetof(Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31, ___eclass_0)); }
	inline uint8_t get_eclass_0() const { return ___eclass_0; }
	inline uint8_t* get_address_of_eclass_0() { return &___eclass_0; }
	inline void set_eclass_0(uint8_t value)
	{
		___eclass_0 = value;
	}

	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31, ___type_1)); }
	inline TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * get_type_1() const { return ___type_1; }
	inline TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F ** get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * value)
	{
		___type_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___type_1), (void*)value);
	}

	inline static int32_t get_offset_of_loc_2() { return static_cast<int32_t>(offsetof(Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31, ___loc_2)); }
	inline Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2  get_loc_2() const { return ___loc_2; }
	inline Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2 * get_address_of_loc_2() { return &___loc_2; }
	inline void set_loc_2(Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2  value)
	{
		___loc_2 = value;
	}
};


// Mono.CSharp.ResolveContext
struct  ResolveContext_t40849BA9E46B26BF7120B15AEA68FFD5E185A7EB  : public RuntimeObject
{
public:
	// Mono.CSharp.ResolveContext_Options Mono.CSharp.ResolveContext::flags
	int32_t ___flags_0;
	// Mono.CSharp.AnonymousExpression Mono.CSharp.ResolveContext::CurrentAnonymousMethod
	AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6 * ___CurrentAnonymousMethod_1;
	// Mono.CSharp.Expression Mono.CSharp.ResolveContext::CurrentInitializerVariable
	Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * ___CurrentInitializerVariable_2;
	// Mono.CSharp.Block Mono.CSharp.ResolveContext::CurrentBlock
	Block_t6084BBFB00E424695AF02B74D74E6060CEC8C88D * ___CurrentBlock_3;
	// Mono.CSharp.IMemberContext Mono.CSharp.ResolveContext::MemberContext
	RuntimeObject* ___MemberContext_4;

public:
	inline static int32_t get_offset_of_flags_0() { return static_cast<int32_t>(offsetof(ResolveContext_t40849BA9E46B26BF7120B15AEA68FFD5E185A7EB, ___flags_0)); }
	inline int32_t get_flags_0() const { return ___flags_0; }
	inline int32_t* get_address_of_flags_0() { return &___flags_0; }
	inline void set_flags_0(int32_t value)
	{
		___flags_0 = value;
	}

	inline static int32_t get_offset_of_CurrentAnonymousMethod_1() { return static_cast<int32_t>(offsetof(ResolveContext_t40849BA9E46B26BF7120B15AEA68FFD5E185A7EB, ___CurrentAnonymousMethod_1)); }
	inline AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6 * get_CurrentAnonymousMethod_1() const { return ___CurrentAnonymousMethod_1; }
	inline AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6 ** get_address_of_CurrentAnonymousMethod_1() { return &___CurrentAnonymousMethod_1; }
	inline void set_CurrentAnonymousMethod_1(AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6 * value)
	{
		___CurrentAnonymousMethod_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CurrentAnonymousMethod_1), (void*)value);
	}

	inline static int32_t get_offset_of_CurrentInitializerVariable_2() { return static_cast<int32_t>(offsetof(ResolveContext_t40849BA9E46B26BF7120B15AEA68FFD5E185A7EB, ___CurrentInitializerVariable_2)); }
	inline Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * get_CurrentInitializerVariable_2() const { return ___CurrentInitializerVariable_2; }
	inline Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 ** get_address_of_CurrentInitializerVariable_2() { return &___CurrentInitializerVariable_2; }
	inline void set_CurrentInitializerVariable_2(Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * value)
	{
		___CurrentInitializerVariable_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CurrentInitializerVariable_2), (void*)value);
	}

	inline static int32_t get_offset_of_CurrentBlock_3() { return static_cast<int32_t>(offsetof(ResolveContext_t40849BA9E46B26BF7120B15AEA68FFD5E185A7EB, ___CurrentBlock_3)); }
	inline Block_t6084BBFB00E424695AF02B74D74E6060CEC8C88D * get_CurrentBlock_3() const { return ___CurrentBlock_3; }
	inline Block_t6084BBFB00E424695AF02B74D74E6060CEC8C88D ** get_address_of_CurrentBlock_3() { return &___CurrentBlock_3; }
	inline void set_CurrentBlock_3(Block_t6084BBFB00E424695AF02B74D74E6060CEC8C88D * value)
	{
		___CurrentBlock_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CurrentBlock_3), (void*)value);
	}

	inline static int32_t get_offset_of_MemberContext_4() { return static_cast<int32_t>(offsetof(ResolveContext_t40849BA9E46B26BF7120B15AEA68FFD5E185A7EB, ___MemberContext_4)); }
	inline RuntimeObject* get_MemberContext_4() const { return ___MemberContext_4; }
	inline RuntimeObject** get_address_of_MemberContext_4() { return &___MemberContext_4; }
	inline void set_MemberContext_4(RuntimeObject* value)
	{
		___MemberContext_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MemberContext_4), (void*)value);
	}
};


// Mono.CSharp.ResumableStatement
struct  ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC  : public Statement_t412FB2CAA6DCE73C11AD18A3F3E56815675E7D79
{
public:
	// System.Boolean Mono.CSharp.ResumableStatement::prepared
	bool ___prepared_2;
	// System.Reflection.Emit.Label Mono.CSharp.ResumableStatement::resume_point
	Label_t15CF7D602807306CF6D6340B09720178CE278550  ___resume_point_3;

public:
	inline static int32_t get_offset_of_prepared_2() { return static_cast<int32_t>(offsetof(ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC, ___prepared_2)); }
	inline bool get_prepared_2() const { return ___prepared_2; }
	inline bool* get_address_of_prepared_2() { return &___prepared_2; }
	inline void set_prepared_2(bool value)
	{
		___prepared_2 = value;
	}

	inline static int32_t get_offset_of_resume_point_3() { return static_cast<int32_t>(offsetof(ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC, ___resume_point_3)); }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550  get_resume_point_3() const { return ___resume_point_3; }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550 * get_address_of_resume_point_3() { return &___resume_point_3; }
	inline void set_resume_point_3(Label_t15CF7D602807306CF6D6340B09720178CE278550  value)
	{
		___resume_point_3 = value;
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>>
struct  Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_t9B9E3A64698A414C03953277E8E6D511E145491F * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D, ___list_0)); }
	inline List_1_t9B9E3A64698A414C03953277E8E6D511E145491F * get_list_0() const { return ___list_0; }
	inline List_1_t9B9E3A64698A414C03953277E8E6D511E145491F ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t9B9E3A64698A414C03953277E8E6D511E145491F * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D, ___current_3)); }
	inline KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___current_3))->___key_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___U3CdescriptionU3Ek__BackingField_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___U3CusageU3Ek__BackingField_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___current_3))->___value_1))->___U3CcallbackU3Ek__BackingField_3), (void*)NULL);
		#endif
	}
};


// System.Collections.Generic.List`1_Enumerator<System.Int32Enum>
struct  Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1_Enumerator::list
	List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___list_0;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1_Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1_Enumerator::current
	int32_t ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984, ___list_0)); }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * get_list_0() const { return ___list_0; }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984, ___current_3)); }
	inline int32_t get_current_3() const { return ___current_3; }
	inline int32_t* get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(int32_t value)
	{
		___current_3 = value;
	}
};


// System.Linq.Enumerable_WhereEnumerableIterator`1<Wenzil.Console.ConsoleCommand>
struct  WhereEnumerableIterator_1_t1D8672A800AD4D70F00916ACF7506ECA8A6BAF8A  : public Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereEnumerableIterator`1::predicate
	Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F * ___predicate_4;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::enumerator
	RuntimeObject* ___enumerator_5;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1D8672A800AD4D70F00916ACF7506ECA8A6BAF8A, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1D8672A800AD4D70F00916ACF7506ECA8A6BAF8A, ___predicate_4)); }
	inline Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_5() { return static_cast<int32_t>(offsetof(WhereEnumerableIterator_1_t1D8672A800AD4D70F00916ACF7506ECA8A6BAF8A, ___enumerator_5)); }
	inline RuntimeObject* get_enumerator_5() const { return ___enumerator_5; }
	inline RuntimeObject** get_address_of_enumerator_5() { return &___enumerator_5; }
	inline void set_enumerator_5(RuntimeObject* value)
	{
		___enumerator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_5), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>
struct  WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0  : public Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2
{
public:
	// System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::source
	RuntimeObject* ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::predicate
	Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::selector
	Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * ___selector_5;
	// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::enumerator
	RuntimeObject* ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0, ___source_3)); }
	inline RuntimeObject* get_source_3() const { return ___source_3; }
	inline RuntimeObject** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(RuntimeObject* value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0, ___predicate_4)); }
	inline Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0, ___selector_5)); }
	inline Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0, ___enumerator_6)); }
	inline RuntimeObject* get_enumerator_6() const { return ___enumerator_6; }
	inline RuntimeObject** get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(RuntimeObject* value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumerator_6), (void*)value);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Int32>
struct  WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353, ___source_3)); }
	inline List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * get_source_3() const { return ___source_3; }
	inline List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353, ___predicate_4)); }
	inline Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353, ___selector_5)); }
	inline Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD * get_selector_5() const { return ___selector_5; }
	inline Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353, ___enumerator_6)); }
	inline Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___Name_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___Version_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Object>
struct  WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0, ___source_3)); }
	inline List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * get_source_3() const { return ___source_3; }
	inline List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0, ___predicate_4)); }
	inline Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0, ___selector_5)); }
	inline Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA * get_selector_5() const { return ___selector_5; }
	inline Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0, ___enumerator_6)); }
	inline Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___Name_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___Version_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Single>
struct  WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_tE234817DD56F8D228C80039231C328502ABCF601 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F, ___source_3)); }
	inline List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * get_source_3() const { return ___source_3; }
	inline List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F, ___predicate_4)); }
	inline Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F, ___selector_5)); }
	inline Func_2_tE234817DD56F8D228C80039231C328502ABCF601 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE234817DD56F8D228C80039231C328502ABCF601 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE234817DD56F8D228C80039231C328502ABCF601 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F, ___enumerator_6)); }
	inline Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___Name_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___Version_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>
struct  WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184, ___source_3)); }
	inline List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * get_source_3() const { return ___source_3; }
	inline List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184, ___predicate_4)); }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184, ___selector_5)); }
	inline Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * get_selector_5() const { return ___selector_5; }
	inline Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184, ___enumerator_6)); }
	inline Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___value_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>
struct  WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559, ___source_3)); }
	inline List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * get_source_3() const { return ___source_3; }
	inline List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559, ___predicate_4)); }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559, ___selector_5)); }
	inline Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559, ___enumerator_6)); }
	inline Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___value_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>
struct  WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE, ___source_3)); }
	inline List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * get_source_3() const { return ___source_3; }
	inline List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE, ___predicate_4)); }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE, ___selector_5)); }
	inline Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE, ___enumerator_6)); }
	inline Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___value_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>
struct  WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238, ___source_3)); }
	inline List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * get_source_3() const { return ___source_3; }
	inline List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238, ___predicate_4)); }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238, ___selector_5)); }
	inline Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * get_selector_5() const { return ___selector_5; }
	inline Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238, ___enumerator_6)); }
	inline Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___value_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>
struct  WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529, ___source_3)); }
	inline List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * get_source_3() const { return ___source_3; }
	inline List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529, ___predicate_4)); }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529, ___selector_5)); }
	inline Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * get_selector_5() const { return ___selector_5; }
	inline Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529, ___enumerator_6)); }
	inline Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___value_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>
struct  WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077, ___source_3)); }
	inline List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * get_source_3() const { return ___source_3; }
	inline List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077, ___predicate_4)); }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077, ___selector_5)); }
	inline Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * get_selector_5() const { return ___selector_5; }
	inline Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077, ___enumerator_6)); }
	inline Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___value_1), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Int32>
struct  WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F, ___source_3)); }
	inline List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * get_source_3() const { return ___source_3; }
	inline List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F, ___predicate_4)); }
	inline Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F, ___selector_5)); }
	inline Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F, ___enumerator_6)); }
	inline Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Object>
struct  WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE, ___source_3)); }
	inline List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * get_source_3() const { return ___source_3; }
	inline List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE, ___predicate_4)); }
	inline Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE, ___selector_5)); }
	inline Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE, ___enumerator_6)); }
	inline Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Single>
struct  WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546, ___source_3)); }
	inline List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * get_source_3() const { return ___source_3; }
	inline List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546, ___predicate_4)); }
	inline Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546, ___selector_5)); }
	inline Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * get_selector_5() const { return ___selector_5; }
	inline Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546, ___enumerator_6)); }
	inline Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// Mono.CSharp.BlockContext
struct  BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161  : public ResolveContext_t40849BA9E46B26BF7120B15AEA68FFD5E185A7EB
{
public:
	// Mono.CSharp.TypeSpec Mono.CSharp.BlockContext::return_type
	TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * ___return_type_5;
	// System.Int32 Mono.CSharp.BlockContext::AssignmentInfoOffset
	int32_t ___AssignmentInfoOffset_6;
	// Mono.CSharp.ExceptionStatement Mono.CSharp.BlockContext::<CurrentTryBlock>k__BackingField
	ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * ___U3CCurrentTryBlockU3Ek__BackingField_7;
	// Mono.CSharp.LoopStatement Mono.CSharp.BlockContext::<EnclosingLoop>k__BackingField
	LoopStatement_t14901229726AB5B6E193B0D683690CE7166AF884 * ___U3CEnclosingLoopU3Ek__BackingField_8;
	// Mono.CSharp.LoopStatement Mono.CSharp.BlockContext::<EnclosingLoopOrSwitch>k__BackingField
	LoopStatement_t14901229726AB5B6E193B0D683690CE7166AF884 * ___U3CEnclosingLoopOrSwitchU3Ek__BackingField_9;
	// Mono.CSharp.Switch Mono.CSharp.BlockContext::<Switch>k__BackingField
	Switch_t2CEA4337B1222A5A9B26B32936CCD53241AAE027 * ___U3CSwitchU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_return_type_5() { return static_cast<int32_t>(offsetof(BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161, ___return_type_5)); }
	inline TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * get_return_type_5() const { return ___return_type_5; }
	inline TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F ** get_address_of_return_type_5() { return &___return_type_5; }
	inline void set_return_type_5(TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * value)
	{
		___return_type_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___return_type_5), (void*)value);
	}

	inline static int32_t get_offset_of_AssignmentInfoOffset_6() { return static_cast<int32_t>(offsetof(BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161, ___AssignmentInfoOffset_6)); }
	inline int32_t get_AssignmentInfoOffset_6() const { return ___AssignmentInfoOffset_6; }
	inline int32_t* get_address_of_AssignmentInfoOffset_6() { return &___AssignmentInfoOffset_6; }
	inline void set_AssignmentInfoOffset_6(int32_t value)
	{
		___AssignmentInfoOffset_6 = value;
	}

	inline static int32_t get_offset_of_U3CCurrentTryBlockU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161, ___U3CCurrentTryBlockU3Ek__BackingField_7)); }
	inline ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * get_U3CCurrentTryBlockU3Ek__BackingField_7() const { return ___U3CCurrentTryBlockU3Ek__BackingField_7; }
	inline ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 ** get_address_of_U3CCurrentTryBlockU3Ek__BackingField_7() { return &___U3CCurrentTryBlockU3Ek__BackingField_7; }
	inline void set_U3CCurrentTryBlockU3Ek__BackingField_7(ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * value)
	{
		___U3CCurrentTryBlockU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CCurrentTryBlockU3Ek__BackingField_7), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEnclosingLoopU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161, ___U3CEnclosingLoopU3Ek__BackingField_8)); }
	inline LoopStatement_t14901229726AB5B6E193B0D683690CE7166AF884 * get_U3CEnclosingLoopU3Ek__BackingField_8() const { return ___U3CEnclosingLoopU3Ek__BackingField_8; }
	inline LoopStatement_t14901229726AB5B6E193B0D683690CE7166AF884 ** get_address_of_U3CEnclosingLoopU3Ek__BackingField_8() { return &___U3CEnclosingLoopU3Ek__BackingField_8; }
	inline void set_U3CEnclosingLoopU3Ek__BackingField_8(LoopStatement_t14901229726AB5B6E193B0D683690CE7166AF884 * value)
	{
		___U3CEnclosingLoopU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CEnclosingLoopU3Ek__BackingField_8), (void*)value);
	}

	inline static int32_t get_offset_of_U3CEnclosingLoopOrSwitchU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161, ___U3CEnclosingLoopOrSwitchU3Ek__BackingField_9)); }
	inline LoopStatement_t14901229726AB5B6E193B0D683690CE7166AF884 * get_U3CEnclosingLoopOrSwitchU3Ek__BackingField_9() const { return ___U3CEnclosingLoopOrSwitchU3Ek__BackingField_9; }
	inline LoopStatement_t14901229726AB5B6E193B0D683690CE7166AF884 ** get_address_of_U3CEnclosingLoopOrSwitchU3Ek__BackingField_9() { return &___U3CEnclosingLoopOrSwitchU3Ek__BackingField_9; }
	inline void set_U3CEnclosingLoopOrSwitchU3Ek__BackingField_9(LoopStatement_t14901229726AB5B6E193B0D683690CE7166AF884 * value)
	{
		___U3CEnclosingLoopOrSwitchU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CEnclosingLoopOrSwitchU3Ek__BackingField_9), (void*)value);
	}

	inline static int32_t get_offset_of_U3CSwitchU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161, ___U3CSwitchU3Ek__BackingField_10)); }
	inline Switch_t2CEA4337B1222A5A9B26B32936CCD53241AAE027 * get_U3CSwitchU3Ek__BackingField_10() const { return ___U3CSwitchU3Ek__BackingField_10; }
	inline Switch_t2CEA4337B1222A5A9B26B32936CCD53241AAE027 ** get_address_of_U3CSwitchU3Ek__BackingField_10() { return &___U3CSwitchU3Ek__BackingField_10; }
	inline void set_U3CSwitchU3Ek__BackingField_10(Switch_t2CEA4337B1222A5A9B26B32936CCD53241AAE027 * value)
	{
		___U3CSwitchU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CSwitchU3Ek__BackingField_10), (void*)value);
	}
};


// Mono.CSharp.EmitContext
struct  EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B  : public BuilderContext_t1A35BCA7F6FD35BA145731728D9F17DC32A46990
{
public:
	// System.Reflection.Emit.ILGenerator Mono.CSharp.EmitContext::ig
	ILGenerator_tCB47F61B7259CF97E8239F921A474B2BEEF84F8F * ___ig_1;
	// Mono.CSharp.TypeSpec Mono.CSharp.EmitContext::return_type
	TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * ___return_type_2;
	// System.Collections.Generic.Dictionary`2<Mono.CSharp.TypeSpec,System.Object> Mono.CSharp.EmitContext::temporary_storage
	Dictionary_2_t6210FBAFCE098ADD4453912B9A98F5B5CE7884CB * ___temporary_storage_3;
	// System.Reflection.Emit.LocalBuilder Mono.CSharp.EmitContext::return_value
	LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 * ___return_value_4;
	// System.Reflection.Emit.Label Mono.CSharp.EmitContext::LoopBegin
	Label_t15CF7D602807306CF6D6340B09720178CE278550  ___LoopBegin_5;
	// System.Reflection.Emit.Label Mono.CSharp.EmitContext::LoopEnd
	Label_t15CF7D602807306CF6D6340B09720178CE278550  ___LoopEnd_6;
	// Mono.CSharp.Switch Mono.CSharp.EmitContext::Switch
	Switch_t2CEA4337B1222A5A9B26B32936CCD53241AAE027 * ___Switch_7;
	// Mono.CSharp.AnonymousExpression Mono.CSharp.EmitContext::CurrentAnonymousMethod
	AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6 * ___CurrentAnonymousMethod_8;
	// Mono.CSharp.IMemberContext Mono.CSharp.EmitContext::member_context
	RuntimeObject* ___member_context_9;
	// Mono.CompilerServices.SymbolWriter.SourceMethodBuilder Mono.CSharp.EmitContext::methodSymbols
	SourceMethodBuilder_t20E41F27F7E03856F8C88E880BD1628D2BC8BED8 * ___methodSymbols_10;
	// Mono.CSharp.DynamicSiteClass Mono.CSharp.EmitContext::dynamic_site_container
	DynamicSiteClass_t3E76BED87F5AA7A05B78E4EBB5AD7961370C4BB0 * ___dynamic_site_container_11;
	// System.Nullable`1<System.Reflection.Emit.Label> Mono.CSharp.EmitContext::return_label
	Nullable_1_t187DA0097AAD049050BE4EE2A751D749D971B43B  ___return_label_12;
	// System.Collections.Generic.List`1<Mono.CSharp.IExpressionCleanup> Mono.CSharp.EmitContext::epilogue_expressions
	List_1_t0661DCFA1F47C9C1003923A2C7CD37916EA51D2B * ___epilogue_expressions_13;
	// Mono.CSharp.ConditionalAccessContext Mono.CSharp.EmitContext::<ConditionalAccess>k__BackingField
	ConditionalAccessContext_t61229EFEB30A9688E063A536301C999D99DEACB7 * ___U3CConditionalAccessU3Ek__BackingField_14;
	// Mono.CSharp.LocalVariable Mono.CSharp.EmitContext::<AsyncThrowVariable>k__BackingField
	LocalVariable_tA8C36B52BF086714723D452FC309A1C1B696E36A * ___U3CAsyncThrowVariableU3Ek__BackingField_15;
	// System.Collections.Generic.List`1<Mono.CSharp.TryFinally> Mono.CSharp.EmitContext::<TryFinallyUnwind>k__BackingField
	List_1_tF2B1CABE3E8F4D589ACEB22D7E980DFECD8B99BF * ___U3CTryFinallyUnwindU3Ek__BackingField_16;
	// System.Reflection.Emit.Label Mono.CSharp.EmitContext::<RecursivePatternLabel>k__BackingField
	Label_t15CF7D602807306CF6D6340B09720178CE278550  ___U3CRecursivePatternLabelU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_ig_1() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___ig_1)); }
	inline ILGenerator_tCB47F61B7259CF97E8239F921A474B2BEEF84F8F * get_ig_1() const { return ___ig_1; }
	inline ILGenerator_tCB47F61B7259CF97E8239F921A474B2BEEF84F8F ** get_address_of_ig_1() { return &___ig_1; }
	inline void set_ig_1(ILGenerator_tCB47F61B7259CF97E8239F921A474B2BEEF84F8F * value)
	{
		___ig_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ig_1), (void*)value);
	}

	inline static int32_t get_offset_of_return_type_2() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___return_type_2)); }
	inline TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * get_return_type_2() const { return ___return_type_2; }
	inline TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F ** get_address_of_return_type_2() { return &___return_type_2; }
	inline void set_return_type_2(TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * value)
	{
		___return_type_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___return_type_2), (void*)value);
	}

	inline static int32_t get_offset_of_temporary_storage_3() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___temporary_storage_3)); }
	inline Dictionary_2_t6210FBAFCE098ADD4453912B9A98F5B5CE7884CB * get_temporary_storage_3() const { return ___temporary_storage_3; }
	inline Dictionary_2_t6210FBAFCE098ADD4453912B9A98F5B5CE7884CB ** get_address_of_temporary_storage_3() { return &___temporary_storage_3; }
	inline void set_temporary_storage_3(Dictionary_2_t6210FBAFCE098ADD4453912B9A98F5B5CE7884CB * value)
	{
		___temporary_storage_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___temporary_storage_3), (void*)value);
	}

	inline static int32_t get_offset_of_return_value_4() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___return_value_4)); }
	inline LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 * get_return_value_4() const { return ___return_value_4; }
	inline LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 ** get_address_of_return_value_4() { return &___return_value_4; }
	inline void set_return_value_4(LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 * value)
	{
		___return_value_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___return_value_4), (void*)value);
	}

	inline static int32_t get_offset_of_LoopBegin_5() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___LoopBegin_5)); }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550  get_LoopBegin_5() const { return ___LoopBegin_5; }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550 * get_address_of_LoopBegin_5() { return &___LoopBegin_5; }
	inline void set_LoopBegin_5(Label_t15CF7D602807306CF6D6340B09720178CE278550  value)
	{
		___LoopBegin_5 = value;
	}

	inline static int32_t get_offset_of_LoopEnd_6() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___LoopEnd_6)); }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550  get_LoopEnd_6() const { return ___LoopEnd_6; }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550 * get_address_of_LoopEnd_6() { return &___LoopEnd_6; }
	inline void set_LoopEnd_6(Label_t15CF7D602807306CF6D6340B09720178CE278550  value)
	{
		___LoopEnd_6 = value;
	}

	inline static int32_t get_offset_of_Switch_7() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___Switch_7)); }
	inline Switch_t2CEA4337B1222A5A9B26B32936CCD53241AAE027 * get_Switch_7() const { return ___Switch_7; }
	inline Switch_t2CEA4337B1222A5A9B26B32936CCD53241AAE027 ** get_address_of_Switch_7() { return &___Switch_7; }
	inline void set_Switch_7(Switch_t2CEA4337B1222A5A9B26B32936CCD53241AAE027 * value)
	{
		___Switch_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Switch_7), (void*)value);
	}

	inline static int32_t get_offset_of_CurrentAnonymousMethod_8() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___CurrentAnonymousMethod_8)); }
	inline AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6 * get_CurrentAnonymousMethod_8() const { return ___CurrentAnonymousMethod_8; }
	inline AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6 ** get_address_of_CurrentAnonymousMethod_8() { return &___CurrentAnonymousMethod_8; }
	inline void set_CurrentAnonymousMethod_8(AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6 * value)
	{
		___CurrentAnonymousMethod_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CurrentAnonymousMethod_8), (void*)value);
	}

	inline static int32_t get_offset_of_member_context_9() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___member_context_9)); }
	inline RuntimeObject* get_member_context_9() const { return ___member_context_9; }
	inline RuntimeObject** get_address_of_member_context_9() { return &___member_context_9; }
	inline void set_member_context_9(RuntimeObject* value)
	{
		___member_context_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___member_context_9), (void*)value);
	}

	inline static int32_t get_offset_of_methodSymbols_10() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___methodSymbols_10)); }
	inline SourceMethodBuilder_t20E41F27F7E03856F8C88E880BD1628D2BC8BED8 * get_methodSymbols_10() const { return ___methodSymbols_10; }
	inline SourceMethodBuilder_t20E41F27F7E03856F8C88E880BD1628D2BC8BED8 ** get_address_of_methodSymbols_10() { return &___methodSymbols_10; }
	inline void set_methodSymbols_10(SourceMethodBuilder_t20E41F27F7E03856F8C88E880BD1628D2BC8BED8 * value)
	{
		___methodSymbols_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___methodSymbols_10), (void*)value);
	}

	inline static int32_t get_offset_of_dynamic_site_container_11() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___dynamic_site_container_11)); }
	inline DynamicSiteClass_t3E76BED87F5AA7A05B78E4EBB5AD7961370C4BB0 * get_dynamic_site_container_11() const { return ___dynamic_site_container_11; }
	inline DynamicSiteClass_t3E76BED87F5AA7A05B78E4EBB5AD7961370C4BB0 ** get_address_of_dynamic_site_container_11() { return &___dynamic_site_container_11; }
	inline void set_dynamic_site_container_11(DynamicSiteClass_t3E76BED87F5AA7A05B78E4EBB5AD7961370C4BB0 * value)
	{
		___dynamic_site_container_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dynamic_site_container_11), (void*)value);
	}

	inline static int32_t get_offset_of_return_label_12() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___return_label_12)); }
	inline Nullable_1_t187DA0097AAD049050BE4EE2A751D749D971B43B  get_return_label_12() const { return ___return_label_12; }
	inline Nullable_1_t187DA0097AAD049050BE4EE2A751D749D971B43B * get_address_of_return_label_12() { return &___return_label_12; }
	inline void set_return_label_12(Nullable_1_t187DA0097AAD049050BE4EE2A751D749D971B43B  value)
	{
		___return_label_12 = value;
	}

	inline static int32_t get_offset_of_epilogue_expressions_13() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___epilogue_expressions_13)); }
	inline List_1_t0661DCFA1F47C9C1003923A2C7CD37916EA51D2B * get_epilogue_expressions_13() const { return ___epilogue_expressions_13; }
	inline List_1_t0661DCFA1F47C9C1003923A2C7CD37916EA51D2B ** get_address_of_epilogue_expressions_13() { return &___epilogue_expressions_13; }
	inline void set_epilogue_expressions_13(List_1_t0661DCFA1F47C9C1003923A2C7CD37916EA51D2B * value)
	{
		___epilogue_expressions_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___epilogue_expressions_13), (void*)value);
	}

	inline static int32_t get_offset_of_U3CConditionalAccessU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___U3CConditionalAccessU3Ek__BackingField_14)); }
	inline ConditionalAccessContext_t61229EFEB30A9688E063A536301C999D99DEACB7 * get_U3CConditionalAccessU3Ek__BackingField_14() const { return ___U3CConditionalAccessU3Ek__BackingField_14; }
	inline ConditionalAccessContext_t61229EFEB30A9688E063A536301C999D99DEACB7 ** get_address_of_U3CConditionalAccessU3Ek__BackingField_14() { return &___U3CConditionalAccessU3Ek__BackingField_14; }
	inline void set_U3CConditionalAccessU3Ek__BackingField_14(ConditionalAccessContext_t61229EFEB30A9688E063A536301C999D99DEACB7 * value)
	{
		___U3CConditionalAccessU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CConditionalAccessU3Ek__BackingField_14), (void*)value);
	}

	inline static int32_t get_offset_of_U3CAsyncThrowVariableU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___U3CAsyncThrowVariableU3Ek__BackingField_15)); }
	inline LocalVariable_tA8C36B52BF086714723D452FC309A1C1B696E36A * get_U3CAsyncThrowVariableU3Ek__BackingField_15() const { return ___U3CAsyncThrowVariableU3Ek__BackingField_15; }
	inline LocalVariable_tA8C36B52BF086714723D452FC309A1C1B696E36A ** get_address_of_U3CAsyncThrowVariableU3Ek__BackingField_15() { return &___U3CAsyncThrowVariableU3Ek__BackingField_15; }
	inline void set_U3CAsyncThrowVariableU3Ek__BackingField_15(LocalVariable_tA8C36B52BF086714723D452FC309A1C1B696E36A * value)
	{
		___U3CAsyncThrowVariableU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CAsyncThrowVariableU3Ek__BackingField_15), (void*)value);
	}

	inline static int32_t get_offset_of_U3CTryFinallyUnwindU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___U3CTryFinallyUnwindU3Ek__BackingField_16)); }
	inline List_1_tF2B1CABE3E8F4D589ACEB22D7E980DFECD8B99BF * get_U3CTryFinallyUnwindU3Ek__BackingField_16() const { return ___U3CTryFinallyUnwindU3Ek__BackingField_16; }
	inline List_1_tF2B1CABE3E8F4D589ACEB22D7E980DFECD8B99BF ** get_address_of_U3CTryFinallyUnwindU3Ek__BackingField_16() { return &___U3CTryFinallyUnwindU3Ek__BackingField_16; }
	inline void set_U3CTryFinallyUnwindU3Ek__BackingField_16(List_1_tF2B1CABE3E8F4D589ACEB22D7E980DFECD8B99BF * value)
	{
		___U3CTryFinallyUnwindU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CTryFinallyUnwindU3Ek__BackingField_16), (void*)value);
	}

	inline static int32_t get_offset_of_U3CRecursivePatternLabelU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B, ___U3CRecursivePatternLabelU3Ek__BackingField_17)); }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550  get_U3CRecursivePatternLabelU3Ek__BackingField_17() const { return ___U3CRecursivePatternLabelU3Ek__BackingField_17; }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550 * get_address_of_U3CRecursivePatternLabelU3Ek__BackingField_17() { return &___U3CRecursivePatternLabelU3Ek__BackingField_17; }
	inline void set_U3CRecursivePatternLabelU3Ek__BackingField_17(Label_t15CF7D602807306CF6D6340B09720178CE278550  value)
	{
		___U3CRecursivePatternLabelU3Ek__BackingField_17 = value;
	}
};


// Mono.CSharp.ExceptionStatement
struct  ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453  : public ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC
{
public:
	// System.Collections.Generic.List`1<Mono.CSharp.ResumableStatement> Mono.CSharp.ExceptionStatement::resume_points
	List_1_t8614223ADB0F2D5A6C300250D505CE621C7CC277 * ___resume_points_4;
	// System.Int32 Mono.CSharp.ExceptionStatement::first_resume_pc
	int32_t ___first_resume_pc_5;
	// Mono.CSharp.ExceptionStatement Mono.CSharp.ExceptionStatement::parent
	ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * ___parent_6;

public:
	inline static int32_t get_offset_of_resume_points_4() { return static_cast<int32_t>(offsetof(ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453, ___resume_points_4)); }
	inline List_1_t8614223ADB0F2D5A6C300250D505CE621C7CC277 * get_resume_points_4() const { return ___resume_points_4; }
	inline List_1_t8614223ADB0F2D5A6C300250D505CE621C7CC277 ** get_address_of_resume_points_4() { return &___resume_points_4; }
	inline void set_resume_points_4(List_1_t8614223ADB0F2D5A6C300250D505CE621C7CC277 * value)
	{
		___resume_points_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resume_points_4), (void*)value);
	}

	inline static int32_t get_offset_of_first_resume_pc_5() { return static_cast<int32_t>(offsetof(ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453, ___first_resume_pc_5)); }
	inline int32_t get_first_resume_pc_5() const { return ___first_resume_pc_5; }
	inline int32_t* get_address_of_first_resume_pc_5() { return &___first_resume_pc_5; }
	inline void set_first_resume_pc_5(int32_t value)
	{
		___first_resume_pc_5 = value;
	}

	inline static int32_t get_offset_of_parent_6() { return static_cast<int32_t>(offsetof(ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453, ___parent_6)); }
	inline ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * get_parent_6() const { return ___parent_6; }
	inline ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 ** get_address_of_parent_6() { return &___parent_6; }
	inline void set_parent_6(ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * value)
	{
		___parent_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___parent_6), (void*)value);
	}
};


// Mono.CSharp.ExpressionStatement
struct  ExpressionStatement_tA6FB6D5618A88C1542784A2BD9826189D66A2F60  : public Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31
{
public:

public:
};


// Mono.CSharp.YieldStatement`1<System.Object>
struct  YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5  : public ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC
{
public:
	// Mono.CSharp.Expression Mono.CSharp.YieldStatement`1::expr
	Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * ___expr_4;
	// System.Boolean Mono.CSharp.YieldStatement`1::unwind_protect
	bool ___unwind_protect_5;
	// T Mono.CSharp.YieldStatement`1::machine_initializer
	RuntimeObject * ___machine_initializer_6;
	// System.Int32 Mono.CSharp.YieldStatement`1::resume_pc
	int32_t ___resume_pc_7;
	// Mono.CSharp.ExceptionStatement Mono.CSharp.YieldStatement`1::inside_try_block
	ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * ___inside_try_block_8;

public:
	inline static int32_t get_offset_of_expr_4() { return static_cast<int32_t>(offsetof(YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5, ___expr_4)); }
	inline Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * get_expr_4() const { return ___expr_4; }
	inline Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 ** get_address_of_expr_4() { return &___expr_4; }
	inline void set_expr_4(Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * value)
	{
		___expr_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___expr_4), (void*)value);
	}

	inline static int32_t get_offset_of_unwind_protect_5() { return static_cast<int32_t>(offsetof(YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5, ___unwind_protect_5)); }
	inline bool get_unwind_protect_5() const { return ___unwind_protect_5; }
	inline bool* get_address_of_unwind_protect_5() { return &___unwind_protect_5; }
	inline void set_unwind_protect_5(bool value)
	{
		___unwind_protect_5 = value;
	}

	inline static int32_t get_offset_of_machine_initializer_6() { return static_cast<int32_t>(offsetof(YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5, ___machine_initializer_6)); }
	inline RuntimeObject * get_machine_initializer_6() const { return ___machine_initializer_6; }
	inline RuntimeObject ** get_address_of_machine_initializer_6() { return &___machine_initializer_6; }
	inline void set_machine_initializer_6(RuntimeObject * value)
	{
		___machine_initializer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___machine_initializer_6), (void*)value);
	}

	inline static int32_t get_offset_of_resume_pc_7() { return static_cast<int32_t>(offsetof(YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5, ___resume_pc_7)); }
	inline int32_t get_resume_pc_7() const { return ___resume_pc_7; }
	inline int32_t* get_address_of_resume_pc_7() { return &___resume_pc_7; }
	inline void set_resume_pc_7(int32_t value)
	{
		___resume_pc_7 = value;
	}

	inline static int32_t get_offset_of_inside_try_block_8() { return static_cast<int32_t>(offsetof(YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5, ___inside_try_block_8)); }
	inline ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * get_inside_try_block_8() const { return ___inside_try_block_8; }
	inline ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 ** get_address_of_inside_try_block_8() { return &___inside_try_block_8; }
	inline void set_inside_try_block_8(ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * value)
	{
		___inside_try_block_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inside_try_block_8), (void*)value);
	}
};


// System.ArithmeticException
struct  ArithmeticException_t8E5F44FABC7FAE0966CBA6DE9BFD545F2660ED47  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.Func`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Boolean>
struct  Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Int32>
struct  Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Object>
struct  Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Single>
struct  Func_2_tE234817DD56F8D228C80039231C328502ABCF601  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Char,System.Boolean>
struct  Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Char,System.Int32>
struct  Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Char,System.Object>
struct  Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Char,System.Single>
struct  Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Boolean>
struct  Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>
struct  Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>
struct  Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>
struct  Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Boolean>
struct  Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>
struct  Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>
struct  Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>
struct  Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,System.Boolean>
struct  Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>
struct  Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32,System.Boolean>
struct  Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32,System.Int32>
struct  Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32,System.Object>
struct  Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32,System.Single>
struct  Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32Enum,System.Boolean>
struct  Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32Enum,System.Int32>
struct  Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32Enum,System.Object>
struct  Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Int32Enum,System.Single>
struct  Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Boolean>
struct  Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Int32>
struct  Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Object>
struct  Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Object,System.Single>
struct  Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Single,System.Boolean>
struct  Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Single,System.Int32>
struct  Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Single,System.Object>
struct  Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<System.Single,System.Single>
struct  Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Resolution,System.Boolean>
struct  Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Resolution,System.Int32>
struct  Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Resolution,System.Object>
struct  Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<UnityEngine.Resolution,System.Single>
struct  Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB  : public MulticastDelegate_t
{
public:

public:
};


// System.Func`2<Wenzil.Console.ConsoleCommand,System.Boolean>
struct  Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F  : public MulticastDelegate_t
{
public:

public:
};


// System.InvalidOperationException
struct  InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>
struct  WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592  : public Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_t9B9E3A64698A414C03953277E8E6D511E145491F * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592, ___source_3)); }
	inline List_1_t9B9E3A64698A414C03953277E8E6D511E145491F * get_source_3() const { return ___source_3; }
	inline List_1_t9B9E3A64698A414C03953277E8E6D511E145491F ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_t9B9E3A64698A414C03953277E8E6D511E145491F * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592, ___predicate_4)); }
	inline Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592, ___selector_5)); }
	inline Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * get_selector_5() const { return ___selector_5; }
	inline Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592, ___enumerator_6)); }
	inline Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___enumerator_6))->___current_3))->___key_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___U3CnameU3Ek__BackingField_0), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___U3CdescriptionU3Ek__BackingField_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___U3CusageU3Ek__BackingField_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___enumerator_6))->___current_3))->___value_1))->___U3CcallbackU3Ek__BackingField_3), (void*)NULL);
		#endif
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Int32>
struct  WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB  : public Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB, ___source_3)); }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * get_source_3() const { return ___source_3; }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB, ___predicate_4)); }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB, ___selector_5)); }
	inline Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F * get_selector_5() const { return ___selector_5; }
	inline Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB, ___enumerator_6)); }
	inline Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Object>
struct  WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8  : public Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8, ___source_3)); }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * get_source_3() const { return ___source_3; }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8, ___predicate_4)); }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8, ___selector_5)); }
	inline Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * get_selector_5() const { return ___selector_5; }
	inline Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8, ___enumerator_6)); }
	inline Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Single>
struct  WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7  : public Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199
{
public:
	// System.Collections.Generic.List`1<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::source
	List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___source_3;
	// System.Func`2<TSource,System.Boolean> System.Linq.Enumerable_WhereSelectListIterator`2::predicate
	Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate_4;
	// System.Func`2<TSource,TResult> System.Linq.Enumerable_WhereSelectListIterator`2::selector
	Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A * ___selector_5;
	// System.Collections.Generic.List`1_Enumerator<TSource> System.Linq.Enumerable_WhereSelectListIterator`2::enumerator
	Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  ___enumerator_6;

public:
	inline static int32_t get_offset_of_source_3() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7, ___source_3)); }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * get_source_3() const { return ___source_3; }
	inline List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A ** get_address_of_source_3() { return &___source_3; }
	inline void set_source_3(List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * value)
	{
		___source_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___source_3), (void*)value);
	}

	inline static int32_t get_offset_of_predicate_4() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7, ___predicate_4)); }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * get_predicate_4() const { return ___predicate_4; }
	inline Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E ** get_address_of_predicate_4() { return &___predicate_4; }
	inline void set_predicate_4(Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * value)
	{
		___predicate_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___predicate_4), (void*)value);
	}

	inline static int32_t get_offset_of_selector_5() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7, ___selector_5)); }
	inline Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A * get_selector_5() const { return ___selector_5; }
	inline Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A ** get_address_of_selector_5() { return &___selector_5; }
	inline void set_selector_5(Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A * value)
	{
		___selector_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___selector_5), (void*)value);
	}

	inline static int32_t get_offset_of_enumerator_6() { return static_cast<int32_t>(offsetof(WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7, ___enumerator_6)); }
	inline Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  get_enumerator_6() const { return ___enumerator_6; }
	inline Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * get_address_of_enumerator_6() { return &___enumerator_6; }
	inline void set_enumerator_6(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  value)
	{
		___enumerator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___enumerator_6))->___list_0), (void*)NULL);
	}
};


// System.Xml.Linq.XHashtable`1_ExtractKeyDelegate<System.Object>
struct  ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99  : public MulticastDelegate_t
{
public:

public:
};


// Mono.CSharp.AnonymousExpression
struct  AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6  : public ExpressionStatement_tA6FB6D5618A88C1542784A2BD9826189D66A2F60
{
public:
	// Mono.CSharp.ParametersBlock Mono.CSharp.AnonymousExpression::block
	ParametersBlock_t30562D7F0452D25CD1537D1FE2F9DF8F32130D5A * ___block_3;
	// Mono.CSharp.TypeSpec Mono.CSharp.AnonymousExpression::ReturnType
	TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * ___ReturnType_4;

public:
	inline static int32_t get_offset_of_block_3() { return static_cast<int32_t>(offsetof(AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6, ___block_3)); }
	inline ParametersBlock_t30562D7F0452D25CD1537D1FE2F9DF8F32130D5A * get_block_3() const { return ___block_3; }
	inline ParametersBlock_t30562D7F0452D25CD1537D1FE2F9DF8F32130D5A ** get_address_of_block_3() { return &___block_3; }
	inline void set_block_3(ParametersBlock_t30562D7F0452D25CD1537D1FE2F9DF8F32130D5A * value)
	{
		___block_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___block_3), (void*)value);
	}

	inline static int32_t get_offset_of_ReturnType_4() { return static_cast<int32_t>(offsetof(AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6, ___ReturnType_4)); }
	inline TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * get_ReturnType_4() const { return ___ReturnType_4; }
	inline TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F ** get_address_of_ReturnType_4() { return &___ReturnType_4; }
	inline void set_ReturnType_4(TypeSpec_t62DA042B2FBB591F545D1F08D6059CDE8355FF3F * value)
	{
		___ReturnType_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ReturnType_4), (void*)value);
	}
};


// System.OverflowException
struct  OverflowException_tD1FBF4E54D81EC98EEF386B69344D336D1EC1AB9  : public ArithmeticException_t8E5F44FABC7FAE0966CBA6DE9BFD545F2660ED47
{
public:

public:
};


// Mono.CSharp.StateMachineInitializer
struct  StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48  : public AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6
{
public:
	// Mono.CSharp.TypeDefinition Mono.CSharp.StateMachineInitializer::Host
	TypeDefinition_t356551DFC70A14243EF40A9AAE75333D1A547B02 * ___Host_5;
	// Mono.CSharp.StateMachine Mono.CSharp.StateMachineInitializer::storey
	StateMachine_tD8A15D3F0A9D4D7727FFA8404DA0BA2BA8035B62 * ___storey_6;
	// System.Reflection.Emit.Label Mono.CSharp.StateMachineInitializer::move_next_ok
	Label_t15CF7D602807306CF6D6340B09720178CE278550  ___move_next_ok_7;
	// System.Reflection.Emit.Label Mono.CSharp.StateMachineInitializer::move_next_error
	Label_t15CF7D602807306CF6D6340B09720178CE278550  ___move_next_error_8;
	// System.Reflection.Emit.LocalBuilder Mono.CSharp.StateMachineInitializer::skip_finally
	LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 * ___skip_finally_9;
	// System.Reflection.Emit.LocalBuilder Mono.CSharp.StateMachineInitializer::current_pc
	LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 * ___current_pc_10;
	// System.Collections.Generic.List`1<Mono.CSharp.ResumableStatement> Mono.CSharp.StateMachineInitializer::resume_points
	List_1_t8614223ADB0F2D5A6C300250D505CE621C7CC277 * ___resume_points_11;
	// System.Reflection.Emit.Label Mono.CSharp.StateMachineInitializer::<BodyEnd>k__BackingField
	Label_t15CF7D602807306CF6D6340B09720178CE278550  ___U3CBodyEndU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_Host_5() { return static_cast<int32_t>(offsetof(StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48, ___Host_5)); }
	inline TypeDefinition_t356551DFC70A14243EF40A9AAE75333D1A547B02 * get_Host_5() const { return ___Host_5; }
	inline TypeDefinition_t356551DFC70A14243EF40A9AAE75333D1A547B02 ** get_address_of_Host_5() { return &___Host_5; }
	inline void set_Host_5(TypeDefinition_t356551DFC70A14243EF40A9AAE75333D1A547B02 * value)
	{
		___Host_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Host_5), (void*)value);
	}

	inline static int32_t get_offset_of_storey_6() { return static_cast<int32_t>(offsetof(StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48, ___storey_6)); }
	inline StateMachine_tD8A15D3F0A9D4D7727FFA8404DA0BA2BA8035B62 * get_storey_6() const { return ___storey_6; }
	inline StateMachine_tD8A15D3F0A9D4D7727FFA8404DA0BA2BA8035B62 ** get_address_of_storey_6() { return &___storey_6; }
	inline void set_storey_6(StateMachine_tD8A15D3F0A9D4D7727FFA8404DA0BA2BA8035B62 * value)
	{
		___storey_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___storey_6), (void*)value);
	}

	inline static int32_t get_offset_of_move_next_ok_7() { return static_cast<int32_t>(offsetof(StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48, ___move_next_ok_7)); }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550  get_move_next_ok_7() const { return ___move_next_ok_7; }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550 * get_address_of_move_next_ok_7() { return &___move_next_ok_7; }
	inline void set_move_next_ok_7(Label_t15CF7D602807306CF6D6340B09720178CE278550  value)
	{
		___move_next_ok_7 = value;
	}

	inline static int32_t get_offset_of_move_next_error_8() { return static_cast<int32_t>(offsetof(StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48, ___move_next_error_8)); }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550  get_move_next_error_8() const { return ___move_next_error_8; }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550 * get_address_of_move_next_error_8() { return &___move_next_error_8; }
	inline void set_move_next_error_8(Label_t15CF7D602807306CF6D6340B09720178CE278550  value)
	{
		___move_next_error_8 = value;
	}

	inline static int32_t get_offset_of_skip_finally_9() { return static_cast<int32_t>(offsetof(StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48, ___skip_finally_9)); }
	inline LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 * get_skip_finally_9() const { return ___skip_finally_9; }
	inline LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 ** get_address_of_skip_finally_9() { return &___skip_finally_9; }
	inline void set_skip_finally_9(LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 * value)
	{
		___skip_finally_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___skip_finally_9), (void*)value);
	}

	inline static int32_t get_offset_of_current_pc_10() { return static_cast<int32_t>(offsetof(StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48, ___current_pc_10)); }
	inline LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 * get_current_pc_10() const { return ___current_pc_10; }
	inline LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 ** get_address_of_current_pc_10() { return &___current_pc_10; }
	inline void set_current_pc_10(LocalBuilder_t7D66C7BAA00271B00F8FDBE1F3D85A6223E99E16 * value)
	{
		___current_pc_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___current_pc_10), (void*)value);
	}

	inline static int32_t get_offset_of_resume_points_11() { return static_cast<int32_t>(offsetof(StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48, ___resume_points_11)); }
	inline List_1_t8614223ADB0F2D5A6C300250D505CE621C7CC277 * get_resume_points_11() const { return ___resume_points_11; }
	inline List_1_t8614223ADB0F2D5A6C300250D505CE621C7CC277 ** get_address_of_resume_points_11() { return &___resume_points_11; }
	inline void set_resume_points_11(List_1_t8614223ADB0F2D5A6C300250D505CE621C7CC277 * value)
	{
		___resume_points_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___resume_points_11), (void*)value);
	}

	inline static int32_t get_offset_of_U3CBodyEndU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48, ___U3CBodyEndU3Ek__BackingField_12)); }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550  get_U3CBodyEndU3Ek__BackingField_12() const { return ___U3CBodyEndU3Ek__BackingField_12; }
	inline Label_t15CF7D602807306CF6D6340B09720178CE278550 * get_address_of_U3CBodyEndU3Ek__BackingField_12() { return &___U3CBodyEndU3Ek__BackingField_12; }
	inline void set_U3CBodyEndU3Ek__BackingField_12(Label_t15CF7D602807306CF6D6340B09720178CE278550  value)
	{
		___U3CBodyEndU3Ek__BackingField_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// System.Xml.Linq.XHashtable`1_XHashtableState_Entry<System.Object>[]
struct EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Entry_t07D1CC404E65C6AB3CD8F93EC6546722B73EBF94  m_Items[1];

public:
	inline Entry_t07D1CC404E65C6AB3CD8F93EC6546722B73EBF94  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Entry_t07D1CC404E65C6AB3CD8F93EC6546722B73EBF94 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Entry_t07D1CC404E65C6AB3CD8F93EC6546722B73EBF94  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Value_0), (void*)NULL);
	}
	inline Entry_t07D1CC404E65C6AB3CD8F93EC6546722B73EBF94  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Entry_t07D1CC404E65C6AB3CD8F93EC6546722B73EBF94 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Entry_t07D1CC404E65C6AB3CD8F93EC6546722B73EBF94  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___Value_0), (void*)NULL);
	}
};


// !0 System.Collections.Generic.List`1/Enumerator<System.Char>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Il2CppChar Enumerator_get_Current_mBEE48434132CD10DC7A6BD2C69B50B69206DF14C_gshared_inline (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Char>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m0AA15D29D509CAD91436A961E5D89B05F2EC97BA_gshared (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_gshared_inline (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6_gshared (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32Enum>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_gshared_inline (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32Enum>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mDECBD2FBFA44578D8E050CECB33BF350152E111A_gshared (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  Enumerator_get_Current_m57C04D97C5B60AAC24531BE8204334F187B1007D_gshared_inline (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mB0383B4B0018664482A70C59D1CB886213E34304_gshared (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Resolution>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  Enumerator_get_Current_m1EB734EC70AB008E22FBDA4FCE5FF70CED4BC7D5_gshared_inline (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Resolution>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m5C01A3C015421F6AF08BB3674A3C1D4212E1CFE6_gshared (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Single>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_gshared_inline (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mF6D031AEDDDEEAF750E0BFE7866FBBA9C9752C7E_gshared (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  Enumerator_get_Current_m290640F097AF113C466EEBA5107E3850AB8FBA65_gshared_inline (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_m7A815733AA2D57CE50C0579CD9463138FC8122C8_gshared (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  Enumerator_get_Current_mD3498F86E6123F926AA4E50358C9F6515C73EECC_gshared_inline (Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mB734D19B11DC29D53835D8BA81B44955AB0FA62E_gshared (Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  Enumerator_get_Current_m36A49F353C175C557E3490621F4489D7D95A646B_gshared_inline (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mC851DE6441AF295DB85CFCE9C6F9242152172F67_gshared (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared (Dictionary_2_tBD1E3221EBD04CEBDA49B84779912E91F56B958D * __this, const RuntimeMethod* method);
// System.Boolean FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::get_HasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool fsOption_1_get_HasValue_m00C0772621EA752A0EE2ED47E4FB3AE3CA4076FF_gshared (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, const RuntimeMethod* method);
// System.Boolean FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::get_IsEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool fsOption_1_get_IsEmpty_m79159C98271291CF3C76D956B12B69736A4D7FBB_gshared (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, const RuntimeMethod* method);
// T FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  fsOption_1_get_Value_m61B93A2F2EDD9AB4AB9A94DE75103A39EC833F66_gshared (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, const RuntimeMethod* method);
// System.Void FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::.ctor(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void fsOption_1__ctor_mB59DF4B8063F319B6D2423AD6ED5BB982FDDDE13_gshared (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  ___value0, const RuntimeMethod* method);
// System.Boolean FullSerializer.Internal.fsOption`1<System.Object>::get_HasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool fsOption_1_get_HasValue_mCC291617445169FE15CD25E16F6153F2ABEF7421_gshared (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, const RuntimeMethod* method);
// System.Boolean FullSerializer.Internal.fsOption`1<System.Object>::get_IsEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool fsOption_1_get_IsEmpty_mD138FFBFB08368962CC8D7FD6BE3C9A2753FF92E_gshared (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, const RuntimeMethod* method);
// T FullSerializer.Internal.fsOption`1<System.Object>::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * fsOption_1_get_Value_m3D1611CEBDB9440CC8285C9814801A410042CD4A_gshared (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, const RuntimeMethod* method);
// System.Void FullSerializer.Internal.fsOption`1<System.Object>::.ctor(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void fsOption_1__ctor_m501783AB74D015496072C03602E16A08B949B870_gshared (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, RuntimeObject * ___value0, const RuntimeMethod* method);

// !0 System.Collections.Generic.List`1/Enumerator<System.Char>::get_Current()
inline Il2CppChar Enumerator_get_Current_mBEE48434132CD10DC7A6BD2C69B50B69206DF14C_inline (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * __this, const RuntimeMethod* method)
{
	return ((  Il2CppChar (*) (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *, const RuntimeMethod*))Enumerator_get_Current_mBEE48434132CD10DC7A6BD2C69B50B69206DF14C_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Char>::MoveNext()
inline bool Enumerator_MoveNext_m0AA15D29D509CAD91436A961E5D89B05F2EC97BA (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *, const RuntimeMethod*))Enumerator_MoveNext_m0AA15D29D509CAD91436A961E5D89B05F2EC97BA_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
inline int32_t Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *, const RuntimeMethod*))Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
inline bool Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6 (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *, const RuntimeMethod*))Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Int32Enum>::get_Current()
inline int32_t Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_inline (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *, const RuntimeMethod*))Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32Enum>::MoveNext()
inline bool Enumerator_MoveNext_mDECBD2FBFA44578D8E050CECB33BF350152E111A (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *, const RuntimeMethod*))Enumerator_MoveNext_mDECBD2FBFA44578D8E050CECB33BF350152E111A_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency>::get_Current()
inline ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  Enumerator_get_Current_m57C04D97C5B60AAC24531BE8204334F187B1007D_inline (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * __this, const RuntimeMethod* method)
{
	return ((  ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  (*) (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *, const RuntimeMethod*))Enumerator_get_Current_m57C04D97C5B60AAC24531BE8204334F187B1007D_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency>::MoveNext()
inline bool Enumerator_MoveNext_mB0383B4B0018664482A70C59D1CB886213E34304 (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *, const RuntimeMethod*))Enumerator_MoveNext_mB0383B4B0018664482A70C59D1CB886213E34304_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
inline RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject * (*) (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *, const RuntimeMethod*))Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
inline bool Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0 (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *, const RuntimeMethod*))Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<UnityEngine.Resolution>::get_Current()
inline Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  Enumerator_get_Current_m1EB734EC70AB008E22FBDA4FCE5FF70CED4BC7D5_inline (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * __this, const RuntimeMethod* method)
{
	return ((  Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  (*) (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *, const RuntimeMethod*))Enumerator_get_Current_m1EB734EC70AB008E22FBDA4FCE5FF70CED4BC7D5_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Resolution>::MoveNext()
inline bool Enumerator_MoveNext_m5C01A3C015421F6AF08BB3674A3C1D4212E1CFE6 (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *, const RuntimeMethod*))Enumerator_MoveNext_m5C01A3C015421F6AF08BB3674A3C1D4212E1CFE6_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Single>::get_Current()
inline float Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_inline (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * __this, const RuntimeMethod* method)
{
	return ((  float (*) (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *, const RuntimeMethod*))Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Single>::MoveNext()
inline bool Enumerator_MoveNext_mF6D031AEDDDEEAF750E0BFE7866FBBA9C9752C7E (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *, const RuntimeMethod*))Enumerator_MoveNext_mF6D031AEDDDEEAF750E0BFE7866FBBA9C9752C7E_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::get_Current()
inline KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  Enumerator_get_Current_m290640F097AF113C466EEBA5107E3850AB8FBA65_inline (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  (*) (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *, const RuntimeMethod*))Enumerator_get_Current_m290640F097AF113C466EEBA5107E3850AB8FBA65_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::MoveNext()
inline bool Enumerator_MoveNext_m7A815733AA2D57CE50C0579CD9463138FC8122C8 (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *, const RuntimeMethod*))Enumerator_MoveNext_m7A815733AA2D57CE50C0579CD9463138FC8122C8_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>>::get_Current()
inline KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  Enumerator_get_Current_mD3498F86E6123F926AA4E50358C9F6515C73EECC_inline (Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D * __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  (*) (Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D *, const RuntimeMethod*))Enumerator_get_Current_mD3498F86E6123F926AA4E50358C9F6515C73EECC_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>>::MoveNext()
inline bool Enumerator_MoveNext_mB734D19B11DC29D53835D8BA81B44955AB0FA62E (Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D *, const RuntimeMethod*))Enumerator_MoveNext_mB734D19B11DC29D53835D8BA81B44955AB0FA62E_gshared)(__this, method);
}
// !0 System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
inline KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  Enumerator_get_Current_m36A49F353C175C557E3490621F4489D7D95A646B_inline (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * __this, const RuntimeMethod* method)
{
	return ((  KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  (*) (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *, const RuntimeMethod*))Enumerator_get_Current_m36A49F353C175C557E3490621F4489D7D95A646B_gshared_inline)(__this, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
inline bool Enumerator_MoveNext_mC851DE6441AF295DB85CFCE9C6F9242152172F67 (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *, const RuntimeMethod*))Enumerator_MoveNext_mC851DE6441AF295DB85CFCE9C6F9242152172F67_gshared)(__this, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Int32 System.Threading.Interlocked::CompareExchange(System.Int32&,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Interlocked_CompareExchange_m317AD9524376B7BE74DD9069346E345F2B131382 (int32_t* ___location10, int32_t ___value1, int32_t ___comparand2, const RuntimeMethod* method);
// System.Void System.OverflowException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OverflowException__ctor_m9D5C4C7E08BE06B4D72424590FB4365733FC351D (OverflowException_tD1FBF4E54D81EC98EEF386B69344D336D1EC1AB9 * __this, const RuntimeMethod* method);
// System.Int32 System.String::get_Length()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method);
// System.Int32 System.Threading.Interlocked::Increment(System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Interlocked_Increment_mEF7FA106280D9E57DA8A97887389A961B65E47D8 (int32_t* ___location0, const RuntimeMethod* method);
// System.Void System.Threading.Thread::MemoryBarrier()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Thread_MemoryBarrier_m9E2B68F7889D5D3AD76126930EE120D51C1B3402 (const RuntimeMethod* method);
// System.Int32 System.String::CompareOrdinal(System.String,System.Int32,System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t String_CompareOrdinal_m080D376EC2E7A0C528A440094A0DB97DFB34CD41 (String_t* ___strA0, int32_t ___indexA1, String_t* ___strB2, int32_t ___indexB3, int32_t ___length4, const RuntimeMethod* method);
// System.Char System.String::get_Chars(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Il2CppChar String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70 (String_t* __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object,System.Boolean&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4 (RuntimeObject * ___obj0, bool* ___lockTaken1, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void Mono.CSharp.ResumableStatement::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResumableStatement__ctor_mAEB3654FD6AC1AB4004985FC092B1E19A32A4FA5 (ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC * __this, const RuntimeMethod* method);
// Mono.CSharp.Expression Mono.CSharp.Expression::Resolve(Mono.CSharp.ResolveContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * Expression_Resolve_mE9C7188F0D2FD3E2CD01051E851CC3B52A7CC12F (Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * __this, ResolveContext_t40849BA9E46B26BF7120B15AEA68FFD5E185A7EB * ___rc0, const RuntimeMethod* method);
// Mono.CSharp.ExceptionStatement Mono.CSharp.BlockContext::get_CurrentTryBlock()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * BlockContext_get_CurrentTryBlock_m599BADFEE226F520C77FC87162728E15ABF16FAB_inline (BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161 * __this, const RuntimeMethod* method);
// System.Int32 Mono.CSharp.StateMachineInitializer::AddResumePoint(Mono.CSharp.ResumableStatement)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t StateMachineInitializer_AddResumePoint_m63C7CDA60F3BF7F1989F3A3740BEFDBE58EE52F6 (StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48 * __this, ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC * ___stmt0, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>::.ctor()
inline void Dictionary_2__ctor_mB6E331DE1E5FCDA60F4997990B21B6BDA65AC4E6 (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *, const RuntimeMethod*))Dictionary_2__ctor_m2C8EE5C13636D67F6C451C4935049F534AEC658F_gshared)(__this, method);
}
// System.Void FullSerializer.fsData::.ctor(System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void fsData__ctor_mD629E2C45F8DFD34B8555B3F2725E84662101BF4 (fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * __this, Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * ___dict0, const RuntimeMethod* method);
// FullSerializer.fsResult FullSerializer.fsBaseConverter::CheckType(FullSerializer.fsData,FullSerializer.fsDataType)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  fsBaseConverter_CheckType_mACFF822768709E985EA98A2C7AB8A2707C3049D9 (fsBaseConverter_tF4DC7258DCD56EA3FF50C3FD608C8F31631BF8CC * __this, fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * ___data0, int32_t ___type1, const RuntimeMethod* method);
// FullSerializer.fsResult FullSerializer.fsResult::op_Addition(FullSerializer.fsResult,FullSerializer.fsResult)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  fsResult_op_Addition_m4D6866C260A2A17083DF96D3BA88B172E279A8EA (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  ___a0, fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  ___b1, const RuntimeMethod* method);
// System.Boolean FullSerializer.fsResult::get_Failed()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool fsResult_get_Failed_m9C792FBD2658DA77759BE224A949470679F99074 (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA * __this, const RuntimeMethod* method);
// System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData> FullSerializer.fsData::get_AsDictionary()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * fsData_get_AsDictionary_mDF9877487F2278358C4B94556633D11843715CA1 (fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * __this, const RuntimeMethod* method);
// System.Void FullSerializer.fsDirectConverter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void fsDirectConverter__ctor_m97790897C194D24C858E342216E248590112F770 (fsDirectConverter_t41638BCC8EA7FFAC4B7A9C0797FC3E57D1ECF024 * __this, const RuntimeMethod* method);
// System.Boolean FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::get_HasValue()
inline bool fsOption_1_get_HasValue_m00C0772621EA752A0EE2ED47E4FB3AE3CA4076FF (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 *, const RuntimeMethod*))fsOption_1_get_HasValue_m00C0772621EA752A0EE2ED47E4FB3AE3CA4076FF_gshared)(__this, method);
}
// System.Boolean FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::get_IsEmpty()
inline bool fsOption_1_get_IsEmpty_m79159C98271291CF3C76D956B12B69736A4D7FBB (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 *, const RuntimeMethod*))fsOption_1_get_IsEmpty_m79159C98271291CF3C76D956B12B69736A4D7FBB_gshared)(__this, method);
}
// System.Void System.InvalidOperationException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * __this, String_t* ___message0, const RuntimeMethod* method);
// T FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::get_Value()
inline fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  fsOption_1_get_Value_m61B93A2F2EDD9AB4AB9A94DE75103A39EC833F66 (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, const RuntimeMethod* method)
{
	return ((  fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  (*) (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 *, const RuntimeMethod*))fsOption_1_get_Value_m61B93A2F2EDD9AB4AB9A94DE75103A39EC833F66_gshared)(__this, method);
}
// System.Void FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::.ctor(T)
inline void fsOption_1__ctor_mB59DF4B8063F319B6D2423AD6ED5BB982FDDDE13 (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  ___value0, const RuntimeMethod* method)
{
	((  void (*) (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 *, fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23 , const RuntimeMethod*))fsOption_1__ctor_mB59DF4B8063F319B6D2423AD6ED5BB982FDDDE13_gshared)(__this, ___value0, method);
}
// System.Boolean FullSerializer.Internal.fsOption`1<System.Object>::get_HasValue()
inline bool fsOption_1_get_HasValue_mCC291617445169FE15CD25E16F6153F2ABEF7421 (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 *, const RuntimeMethod*))fsOption_1_get_HasValue_mCC291617445169FE15CD25E16F6153F2ABEF7421_gshared)(__this, method);
}
// System.Boolean FullSerializer.Internal.fsOption`1<System.Object>::get_IsEmpty()
inline bool fsOption_1_get_IsEmpty_mD138FFBFB08368962CC8D7FD6BE3C9A2753FF92E (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 *, const RuntimeMethod*))fsOption_1_get_IsEmpty_mD138FFBFB08368962CC8D7FD6BE3C9A2753FF92E_gshared)(__this, method);
}
// T FullSerializer.Internal.fsOption`1<System.Object>::get_Value()
inline RuntimeObject * fsOption_1_get_Value_m3D1611CEBDB9440CC8285C9814801A410042CD4A (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, const RuntimeMethod* method)
{
	return ((  RuntimeObject * (*) (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 *, const RuntimeMethod*))fsOption_1_get_Value_m3D1611CEBDB9440CC8285C9814801A410042CD4A_gshared)(__this, method);
}
// System.Void FullSerializer.Internal.fsOption`1<System.Object>::.ctor(T)
inline void fsOption_1__ctor_m501783AB74D015496072C03602E16A08B949B870 (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	((  void (*) (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 *, RuntimeObject *, const RuntimeMethod*))fsOption_1__ctor_m501783AB74D015496072C03602E16A08B949B870_gshared)(__this, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mCBFFBD86096AE99FD94374348115968DA181FEC5_gshared (WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5 * __this, RuntimeObject* ___source0, Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * ___predicate1, Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mB00F5FD602CFDBCAE7C5C655AF35C78BF02E868E_gshared (WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_1 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * L_2 = (Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5 * L_3 = (WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5 *, RuntimeObject*, Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *, Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_1, (Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m0B450345EA19C2C7D67B7997473865A140E2AA0D_gshared (WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_m0B450345EA19C2C7D67B7997473865A140E2AA0D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m2B4B1B4542AA0029E6A8EEDEA52E10C0B9A05DA9_gshared (WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_m2B4B1B4542AA0029E6A8EEDEA52E10C0B9A05DA9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Resolution>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_6 = InterfaceFuncInvoker0< Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Resolution>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_6;
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_7 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_8 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_9 = V_1;
		NullCheck((Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_8);
		bool L_10 = ((  bool (*) (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *, Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_8, (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * L_11 = (Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *)__this->get_selector_5();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_12 = V_1;
		NullCheck((Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *, Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *)L_11, (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mB24941CF75C72B2BE292DB1EFC41E1B4FFF4723F_gshared (WhereSelectEnumerableIterator_2_t600AAE11E568495A75E3BCC352F51CAD3E7F94E5 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Single>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m140BB536DF76E3B5261367EFD61C58C1B7C0F28A_gshared (WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF * __this, RuntimeObject* ___source0, Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * ___predicate1, Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectEnumerableIterator_2_Clone_m8975623FD354CB264BA7654D3ACBFBB8E42BB586_gshared (WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_1 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * L_2 = (Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF * L_3 = (WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF *, RuntimeObject*, Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *, Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_1, (Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Single>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m5529937A5FB75E1505AA643FB485895B87A1438F_gshared (WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_m5529937A5FB75E1505AA643FB485895B87A1438F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m8102780893AF990BA69A9F746059ADECF5AAC5FA_gshared (WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_m8102780893AF990BA69A9F746059ADECF5AAC5FA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<UnityEngine.Resolution>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_6 = InterfaceFuncInvoker0< Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<UnityEngine.Resolution>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_6;
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_7 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_8 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_9 = V_1;
		NullCheck((Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_8);
		bool L_10 = ((  bool (*) (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *, Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_8, (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * L_11 = (Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *)__this->get_selector_5();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_12 = V_1;
		NullCheck((Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *)L_11);
		float L_13 = ((  float (*) (Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *, Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *)L_11, (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<UnityEngine.Resolution,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mC604C60498A9FDAFC0EDF3EAED068E34246AAE60_gshared (WhereSelectEnumerableIterator_2_t293CA73EC95873414DC79C1C41B48B06690CDEEF * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m78AFB48B5665CD2A00F101101A1510BE9566E0EC_gshared (WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6 * __this, RuntimeObject* ___source0, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate1, Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectEnumerableIterator_2_Clone_mC542EAA1EE9C456B6F3091269F4632E5C825CDBC_gshared (WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * L_2 = (Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6 * L_3 = (WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6 *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_1, (Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Int32>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mE8ED82A82D2D3C5700EFE62D6BDB8195AEFEFA3C_gshared (WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_mE8ED82A82D2D3C5700EFE62D6BDB8195AEFEFA3C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mC788302A5CB47DB612B108BB2B0D4FF71E0534AA_gshared (WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_mC788302A5CB47DB612B108BB2B0D4FF71E0534AA_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Single>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		float L_6 = InterfaceFuncInvoker0< float >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (float)L_6;
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_7 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_8 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		float L_9 = V_1;
		NullCheck((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * L_11 = (Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *)__this->get_selector_5();
		float L_12 = V_1;
		NullCheck((Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *)L_11, (float)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m1057E1A8BC59504D63383777CF4D0BEEFA89F512_gshared (WhereSelectEnumerableIterator_2_tF8C65C5401C365C6E1C33E4EBD17F10CB6C6C0F6 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m965328D2ECA16C2BE75E3E223BDDFA35731CC5BB_gshared (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * __this, RuntimeObject* ___source0, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate1, Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m34512788F517D64AF88551951B2D75A4CA0DD066_gshared (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_2 = (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * L_3 = (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_1, (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m03C13B47EFC2336E91F60AAEC480568F289F373A_gshared (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_m03C13B47EFC2336E91F60AAEC480568F289F373A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mAD7BCF14B4CD4C276B3D022E0EE2E390E2672FB6_gshared (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_mAD7BCF14B4CD4C276B3D022E0EE2E390E2672FB6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Single>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		float L_6 = InterfaceFuncInvoker0< float >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (float)L_6;
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_7 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_8 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		float L_9 = V_1;
		NullCheck((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_11 = (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)__this->get_selector_5();
		float L_12 = V_1;
		NullCheck((Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_11, (float)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m78EC6F7DCD8C5E50C5FF4CCD1F89CF89491CB627_gshared (WhereSelectEnumerableIterator_2_t8330901F0811E7C54CAD7107B2350ACFCBF3859A * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Single>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m6F27C34DB9DBAAE0A94AC2FDC07D7B79899FFFEE_gshared (WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296 * __this, RuntimeObject* ___source0, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate1, Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectEnumerableIterator_2_Clone_mAD162C74F6AD4C26CAF09A09DB95B827BB01FCFD_gshared (WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_2 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296 * L_3 = (WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296 *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_1, (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Single>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mD8FC2FAD9ABE52E2A164F6804FC6A5B88DEA246B_gshared (WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_mD8FC2FAD9ABE52E2A164F6804FC6A5B88DEA246B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mFECC995DE8883D132C0C8817C1E52F0D2A0EE794_gshared (WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_mFECC995DE8883D132C0C8817C1E52F0D2A0EE794_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Single>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		float L_6 = InterfaceFuncInvoker0< float >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Single>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (float)L_6;
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_7 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_8 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		float L_9 = V_1;
		NullCheck((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_11 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)__this->get_selector_5();
		float L_12 = V_1;
		NullCheck((Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)L_11);
		float L_13 = ((  float (*) (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)L_11, (float)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Single,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mD917B452C0AD13E8919B30724346490D1F2042F4_gshared (WhereSelectEnumerableIterator_2_t641ED124137C2860DB7E5576678416B87DC7E296 * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mE667E2F927AD732993B2FE1C0B08A01F29940963_gshared (WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734 * __this, RuntimeObject* ___source0, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate1, Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectEnumerableIterator_2_Clone_m596B465B04233E092955640F3B9906E3300249D3_gshared (WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * L_2 = (Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734 * L_3 = (WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734 *, RuntimeObject*, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_1, (Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m96716ABDF1C57CFB7D425C5C2E29D43895175F98_gshared (WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_m96716ABDF1C57CFB7D425C5C2E29D43895175F98_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m075453CF074406B30F0C205C8FDD6FAD7A91081D_gshared (WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_m075453CF074406B30F0C205C8FDD6FAD7A91081D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_6;
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_7 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_8 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_9 = V_1;
		NullCheck((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * L_11 = (Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *)__this->get_selector_5();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_12 = V_1;
		NullCheck((Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *)L_11, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m097CF59940699E8E354FB4BB07F0862E95AA0F42_gshared (WhereSelectEnumerableIterator_2_t3FD9A0AED922A6599A690E44B5B4706E04A31734 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m3E069F61BD38B75E771F3587B1F20CA5964BE387_gshared (WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08 * __this, RuntimeObject* ___source0, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate1, Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectEnumerableIterator_2_Clone_mF222743968C6C0B9745D03B7A5BF489D21257494_gshared (WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * L_2 = (Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08 * L_3 = (WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08 *, RuntimeObject*, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_1, (Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m7E83098ACC9FD3D3AD5EB79A9B291A0C7C2AAF45_gshared (WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_m7E83098ACC9FD3D3AD5EB79A9B291A0C7C2AAF45_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mC0C5F211EC8D720A808897439D1B86D279BF5616_gshared (WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_mC0C5F211EC8D720A808897439D1B86D279BF5616_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_6;
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_7 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_8 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_9 = V_1;
		NullCheck((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * L_11 = (Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *)__this->get_selector_5();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_12 = V_1;
		NullCheck((Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *)L_11);
		float L_13 = ((  float (*) (Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *)L_11, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mD88BC1FE971B957F7AD731624B6C3E7804FCFA20_gshared (WhereSelectEnumerableIterator_2_tAB2C98717B4C5A0C8309259C5BD3B0F2114C1C08 * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mB8050F9D9890E1236148F707F5EBE31F6D79AC91_gshared (WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442 * __this, RuntimeObject* ___source0, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate1, Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m23EEADE930298DC5E0AAC6D8A7749CE939BA2DCF_gshared (WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * L_2 = (Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442 * L_3 = (WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442 *, RuntimeObject*, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_1, (Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mF7B907536C0BA14EF57C92CF190037065BFD02F3_gshared (WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_mF7B907536C0BA14EF57C92CF190037065BFD02F3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_mC137E9D24D354B452D9B70E9968CA8DE3E9AB373_gshared (WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_mC137E9D24D354B452D9B70E9968CA8DE3E9AB373_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_6;
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_7 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_8 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_9 = V_1;
		NullCheck((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * L_11 = (Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *)__this->get_selector_5();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_12 = V_1;
		NullCheck((Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *)L_11, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mE70AF6DDFE682E908778FB198AEEE60177010E4E_gshared (WhereSelectEnumerableIterator_2_t09921236AC29DB3764E6CE5A7131ACB946AFA442 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_mE717A1A4ED9DEAAEC43BA98C20D41B3895DD5BB7_gshared (WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0 * __this, RuntimeObject* ___source0, Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * ___predicate1, Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this);
		((  void (*) (Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 * WhereSelectEnumerableIterator_2_Clone_mDE11EF0FC2E2F50578A516678C7A22F76F1473F7_gshared (WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * L_1 = (Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)__this->get_predicate_4();
		Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * L_2 = (Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0 * L_3 = (WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0 *, RuntimeObject*, Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *, Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)L_1, (Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_m169091704DEF5029F433F02CC8F0B026518A3856_gshared (WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_m169091704DEF5029F433F02CC8F0B026518A3856_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this);
		((  void (*) (Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m60986EAF84DA044041E9F5AF74F5A1F2D9D287B6_gshared (WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_m60986EAF84DA044041E9F5AF74F5A1F2D9D287B6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A )L_6;
		Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * L_7 = (Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * L_8 = (Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)__this->get_predicate_4();
		KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  L_9 = V_1;
		NullCheck((Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *, KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)L_8, (KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * L_11 = (Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *)__this->get_selector_5();
		KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  L_12 = V_1;
		NullCheck((Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *)L_11);
		ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE  L_13 = ((  ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE  (*) (Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *, KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *)L_11, (KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Wenzil.Console.ConsoleCommand>::Dispose() */, (Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m4DBDCC202688B7D2BD6EACDE1B00B5A432E40891_gshared (WhereSelectEnumerableIterator_2_tB6EA5401CD43CDDE168B296715A38443232D26A0 * __this, Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1D8672A800AD4D70F00916ACF7506ECA8A6BAF8A * L_1 = (WhereEnumerableIterator_1_t1D8672A800AD4D70F00916ACF7506ECA8A6BAF8A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1D8672A800AD4D70F00916ACF7506ECA8A6BAF8A *, RuntimeObject*, Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m5D17F6FDC61EF5BAAC6022B71257146D2AA70043_gshared (WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F * __this, RuntimeObject* ___source0, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate1, Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectEnumerableIterator_2_Clone_m6CAEFFA1C102891960A1ECBF22DA543E31FE57C0_gshared (WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * L_2 = (Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F * L_3 = (WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F *, RuntimeObject*, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_1, (Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mF7369CBD265AEF953AE723D80982F581FB2FD45F_gshared (WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_mF7369CBD265AEF953AE723D80982F581FB2FD45F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m902032CD1F689E6733927A473F43F1495920D802_gshared (WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_m902032CD1F689E6733927A473F43F1495920D802_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_6;
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_7 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_8 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_9 = V_1;
		NullCheck((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * L_11 = (Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *)__this->get_selector_5();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_12 = V_1;
		NullCheck((Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *)L_11, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m5B2829A836944DB4B0E1A66D4236460F3F509ED5_gshared (WhereSelectEnumerableIterator_2_t950DB65ABCDAF653E6DCAE7E1A1FEC62D9AE236F * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m634CE03BF281A9966A650B2BA6ED44AD72E5879F_gshared (WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505 * __this, RuntimeObject* ___source0, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate1, Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_m82474F2DE866D303C5767C7B58EE3213342A29F0_gshared (WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * L_2 = (Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505 * L_3 = (WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505 *, RuntimeObject*, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_1, (Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mAB2AD9EAF60076FC567705357B1A87DCC0FDC6F7_gshared (WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_mAB2AD9EAF60076FC567705357B1A87DCC0FDC6F7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m9AED0076EB03B4E46908FB05BEB74852F542382E_gshared (WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_m9AED0076EB03B4E46908FB05BEB74852F542382E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_6;
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_7 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_8 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_9 = V_1;
		NullCheck((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * L_11 = (Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *)__this->get_selector_5();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_12 = V_1;
		NullCheck((Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *)L_11, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_mDF3B69D0756C062EA752AA0B60FA20A65396313F_gshared (WhereSelectEnumerableIterator_2_t1F3033734D19B5ED4B101CB6D02F411359552505 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m80999DE5E008AC3C0F7DB0ACB141B2E1C1F5158E_gshared (WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303 * __this, RuntimeObject* ___source0, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate1, Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectEnumerableIterator_2_Clone_m0B569BB4B3D0B7D23ADEC1CB407A9AEFD1FA424D_gshared (WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * L_2 = (Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303 * L_3 = (WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303 *, RuntimeObject*, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_1, (Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mD57DB85A1263CB1310548DB5CEA8C4ADBE0CC22B_gshared (WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_mD57DB85A1263CB1310548DB5CEA8C4ADBE0CC22B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m03576C66B9D242B193B03F095D352EE362D9777B_gshared (WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_m03576C66B9D242B193B03F095D352EE362D9777B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_6 = InterfaceFuncInvoker0< KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_6;
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_7 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_8 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_9 = V_1;
		NullCheck((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * L_11 = (Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *)__this->get_selector_5();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_12 = V_1;
		NullCheck((Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *)L_11);
		float L_13 = ((  float (*) (Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *)L_11, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m3EE0FA253783F686546375C80A9FAE9833D34487_gshared (WhereSelectEnumerableIterator_2_tFC2813FCEA0888E0383FF24D31EF87C95C46E303 * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2__ctor_m6DFD3E949A8FA5121F520D501B78C17E84EBDFAC_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, RuntimeObject* ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		RuntimeObject* L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Object,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectEnumerableIterator_2_Clone_mD5F17A02281E6D1529D117CFF2E0F8C347D1B13F_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * L_3 = (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (RuntimeObject*)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Object,System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectEnumerableIterator_2_Dispose_mAA70577DEF67CEC98FE677984AE2175B7D4E4D00_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_Dispose_mAA70577DEF67CEC98FE677984AE2175B7D4E4D00_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = (RuntimeObject*)__this->get_enumerator_6();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		RuntimeObject* L_1 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_1);
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, (RuntimeObject*)L_1);
	}

IL_0013:
	{
		__this->set_enumerator_6((RuntimeObject*)NULL);
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectEnumerableIterator_2_MoveNext_m95AEE737A22EFFFE6557F448BF5AFCC6241D0BD7_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhereSelectEnumerableIterator_2_MoveNext_m95AEE737A22EFFFE6557F448BF5AFCC6241D0BD7_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		RuntimeObject* L_3 = (RuntimeObject*)__this->get_source_3();
		NullCheck((RuntimeObject*)L_3);
		RuntimeObject* L_4 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Object>::GetEnumerator() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 5), (RuntimeObject*)L_3);
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		RuntimeObject* L_5 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_5);
		RuntimeObject * L_6 = InterfaceFuncInvoker0< RuntimeObject * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Object>::get_Current() */, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6), (RuntimeObject*)L_5);
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_11 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		RuntimeObject* L_14 = (RuntimeObject*)__this->get_enumerator_6();
		NullCheck((RuntimeObject*)L_14);
		bool L_15 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, (RuntimeObject*)L_14);
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2<System.Object,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectEnumerableIterator_2_Where_m72A38A8170E8B837F5C090642BE08E3845A8EB37_gshared (WhereSelectEnumerableIterator_2_tDAA8BFBEA68F81670F3F51C6200EBD26D7A8FBAB * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 9));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 10));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m9AD31F7E3582145F0B8AA326DABF32A48E2BA65B_gshared (WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D * __this, List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * ___source0, Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * ___predicate1, Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_mA13048871CE4B55695C7640EF42C89E92DA079D7_gshared (WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D * __this, const RuntimeMethod* method)
{
	{
		List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * L_0 = (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)__this->get_source_3();
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_1 = (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)__this->get_predicate_4();
		Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 * L_2 = (Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 *)__this->get_selector_5();
		WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D * L_3 = (WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D *, List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *, Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *, Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)L_0, (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)L_1, (Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m17A52F81638376815A1E7875F0DABD6D8254F803_gshared (WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Il2CppChar V_1 = 0x0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * L_3 = (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)__this->get_source_3();
		NullCheck((List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)L_3);
		Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  L_4 = ((  Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  (*) (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * L_5 = (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)__this->get_address_of_enumerator_6();
		Il2CppChar L_6 = Enumerator_get_Current_mBEE48434132CD10DC7A6BD2C69B50B69206DF14C_inline((Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Il2CppChar)L_6;
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_7 = (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_8 = (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)__this->get_predicate_4();
		Il2CppChar L_9 = V_1;
		NullCheck((Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *, Il2CppChar, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)L_8, (Il2CppChar)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 * L_11 = (Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 *)__this->get_selector_5();
		Il2CppChar L_12 = V_1;
		NullCheck((Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 *, Il2CppChar, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t3D22400F82E02B1457EDE2BDEC35B894B5DBF1D3 *)L_11, (Il2CppChar)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * L_14 = (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m0AA15D29D509CAD91436A961E5D89B05F2EC97BA((Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m43EA012A13CBCA1BD4F97047F46BFBE3BAFFFC30_gshared (WhereSelectListIterator_2_tEB0F135192CA44E6AE4CF01F125128C969178E5D * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m1273F1E44D53282B24ABCBCF873DBD2BF00796FF_gshared (WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9 * __this, List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * ___source0, Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * ___predicate1, Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m0135D89F1EFA969707DCEC88006A729B658F5B99_gshared (WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9 * __this, const RuntimeMethod* method)
{
	{
		List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * L_0 = (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)__this->get_source_3();
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_1 = (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)__this->get_predicate_4();
		Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E * L_2 = (Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E *)__this->get_selector_5();
		WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9 * L_3 = (WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9 *, List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *, Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *, Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)L_0, (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)L_1, (Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mD12DCEAB5903A24D379014F0B5CEEA5D4D30E697_gshared (WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Il2CppChar V_1 = 0x0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * L_3 = (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)__this->get_source_3();
		NullCheck((List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)L_3);
		Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  L_4 = ((  Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  (*) (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * L_5 = (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)__this->get_address_of_enumerator_6();
		Il2CppChar L_6 = Enumerator_get_Current_mBEE48434132CD10DC7A6BD2C69B50B69206DF14C_inline((Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Il2CppChar)L_6;
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_7 = (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_8 = (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)__this->get_predicate_4();
		Il2CppChar L_9 = V_1;
		NullCheck((Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *, Il2CppChar, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)L_8, (Il2CppChar)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E * L_11 = (Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E *)__this->get_selector_5();
		Il2CppChar L_12 = V_1;
		NullCheck((Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E *, Il2CppChar, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t225B855E9BC31283EA4F08379810F7AF97E56D2E *)L_11, (Il2CppChar)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * L_14 = (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m0AA15D29D509CAD91436A961E5D89B05F2EC97BA((Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m90069D8DB056FF93CE83AB46A9BB4A5174EE47E7_gshared (WhereSelectListIterator_2_t20893F717570DB661B6C6C28F4092C58D74C6BE9 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Single>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m5365F20B28744A506B61A5CFAB31C1B1107A61C1_gshared (WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7 * __this, List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * ___source0, Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * ___predicate1, Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectListIterator_2_Clone_m6E0527013DB575A14AF5F8C063E0F843B4A5545F_gshared (WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7 * __this, const RuntimeMethod* method)
{
	{
		List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * L_0 = (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)__this->get_source_3();
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_1 = (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)__this->get_predicate_4();
		Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F * L_2 = (Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F *)__this->get_selector_5();
		WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7 * L_3 = (WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7 *, List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *, Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *, Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)L_0, (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)L_1, (Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mFEA2844BD9E69B56F9F4BF5EE791EAA648CF658B_gshared (WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Il2CppChar V_1 = 0x0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE * L_3 = (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)__this->get_source_3();
		NullCheck((List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)L_3);
		Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  L_4 = ((  Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F  (*) (List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tC466EC97F6208520EFC214F520D3CB9E72FD1EAE *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * L_5 = (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)__this->get_address_of_enumerator_6();
		Il2CppChar L_6 = Enumerator_get_Current_mBEE48434132CD10DC7A6BD2C69B50B69206DF14C_inline((Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Il2CppChar)L_6;
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_7 = (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A * L_8 = (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)__this->get_predicate_4();
		Il2CppChar L_9 = V_1;
		NullCheck((Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *, Il2CppChar, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t12237805D7B3E966E36DB4327BA1F80B724C4B9A *)L_8, (Il2CppChar)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F * L_11 = (Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F *)__this->get_selector_5();
		Il2CppChar L_12 = V_1;
		NullCheck((Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F *)L_11);
		float L_13 = ((  float (*) (Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F *, Il2CppChar, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t5A9842BD33DE1DB78A27E8F657AFF1E521BD202F *)L_11, (Il2CppChar)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * L_14 = (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m0AA15D29D509CAD91436A961E5D89B05F2EC97BA((Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)(Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Char,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mE07708D3B3367E37D771E2E7570432C246EAA659_gshared (WhereSelectListIterator_2_t674A97ECAD57DDA4CAA6CDA37FBEEB8166694DD7 * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m895E21AE9AB1E3F19B3147EDC913BB567B1A65C7_gshared (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 * __this, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source0, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate1, Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_mEE6375B2C79172E13732CA49AAF389493C1C7100_gshared (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 * __this, const RuntimeMethod* method)
{
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * L_2 = (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *)__this->get_selector_5();
		WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 * L_3 = (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 *, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_0, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_1, (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m637B802A50BA94CD511636CAF5D912C6B96B18A1_gshared (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		NullCheck((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3);
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  L_4 = ((  Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_5 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		int32_t L_6 = Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_7 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_8 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA * L_11 = (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tFF6AE79EFD0857556AD37A1A1594C43F76012FEA *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_14 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m7F75FF628D2E99D2BA127B84FDD08DD88048ADB0_gshared (WhereSelectListIterator_2_t4CC3FE3A35610DC6F761EE7DB863B845957AD325 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mA0D61B688D5EE4E84300EA96C87ED9F3E10D5EA9_gshared (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 * __this, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source0, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate1, Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_mE40F022902D030D86E465678E5DD79B3058FE2CB_gshared (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 * __this, const RuntimeMethod* method)
{
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * L_2 = (Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *)__this->get_selector_5();
		WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 * L_3 = (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 *, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_0, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_1, (Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mDEB78D7AB98D0FDC13661615FDBC0C01A621E84F_gshared (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		NullCheck((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3);
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  L_4 = ((  Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_5 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		int32_t L_6 = Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_7 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_8 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 * L_11 = (Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t401E8A228CE43E56CCE9280AD9C6D87CC73A0123 *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_14 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m9633A851E09C546940C2D5C7EF8CB7C501784EB3_gshared (WhereSelectListIterator_2_tA41D93FF12E41BB5A5BEA27AEED367695ADACEA4 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Single>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mD0F22E680E4B429387D727945B0B06AF6B95820D_gshared (WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06 * __this, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___source0, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate1, Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectListIterator_2_Clone_mAE64D0D473635D8F2970A87ACAC846D2E9A986AF_gshared (WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06 * __this, const RuntimeMethod* method)
{
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_0 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_1 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 * L_2 = (Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 *)__this->get_selector_5();
		WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06 * L_3 = (WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06 *, List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_0, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_1, (Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m29234E904F32B85A74F888428F60230FF7AE5ED7_gshared (WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * L_3 = (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)__this->get_source_3();
		NullCheck((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3);
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  L_4 = ((  Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C  (*) (List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_5 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		int32_t L_6 = Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_inline((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_7 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_8 = (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 * L_11 = (Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 *)L_11);
		float L_13 = ((  float (*) (Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t1452DFFA61F60F54CF3A78C987C4F98906EFD2B6 *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * L_14 = (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m40FD166B6757334A2BBCF67238EFDF70D727A4A6((Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)(Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m1737FB827F03C5E8C7CA71544B9FD803C25A6315_gshared (WhereSelectListIterator_2_tBCA9468991E9C8B9F4178D0FA0C99545886B7F06 * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m60E22D0AE9E4C880251E5D9732C3ABBE88ADC4EF_gshared (WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB * __this, List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___source0, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate1, Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_m5EEF76EB1A2DC2EF4CE1E3A635C2A07B7EF78B99_gshared (WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB * __this, const RuntimeMethod* method)
{
	{
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_0 = (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)__this->get_source_3();
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F * L_2 = (Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F *)__this->get_selector_5();
		WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB * L_3 = (WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB *, List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_0, (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_1, (Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m9EB7197F911B69B0306609231368CD52824585CB_gshared (WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_3 = (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)__this->get_source_3();
		NullCheck((List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_3);
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  L_4 = ((  Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  (*) (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * L_5 = (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)__this->get_address_of_enumerator_6();
		int32_t L_6 = Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_inline((Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_7 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_8 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F * L_11 = (Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tD04FB815631FDCAD604C98A824D9EAB26A1EDD6F *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * L_14 = (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mDECBD2FBFA44578D8E050CECB33BF350152E111A((Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m33BCEE32D02458DD13AA2C6391815C58B7948438_gshared (WhereSelectListIterator_2_tDE3E77E7CCECD8BBCA59A7362558E7375B8BD9EB * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mC542F38EC17CA13C41DB738F0D825E697982CA4E_gshared (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 * __this, List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___source0, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate1, Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m9A196A73CDF63D17862E2CA0BFF6FDCA3D0E8E47_gshared (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 * __this, const RuntimeMethod* method)
{
	{
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_0 = (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)__this->get_source_3();
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * L_2 = (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)__this->get_selector_5();
		WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 * L_3 = (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 *, List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_0, (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_1, (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m10D83660FAC86C1CBD7ED4E1368F14B5478499A5_gshared (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_3 = (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)__this->get_source_3();
		NullCheck((List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_3);
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  L_4 = ((  Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  (*) (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * L_5 = (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)__this->get_address_of_enumerator_6();
		int32_t L_6 = Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_inline((Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_7 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_8 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB * L_11 = (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tF916A06E36F9340A8A2E0053D6A8F0737C5FD2EB *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * L_14 = (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mDECBD2FBFA44578D8E050CECB33BF350152E111A((Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m1E339E047A89DE7D5B31A0AE1A48944B1C933300_gshared (WhereSelectListIterator_2_t5F082584C1A03211ACA908D2D073C1D6149B14A8 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Single>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mBA779B90B4C3F48552710FE9E88BDDC85CD80C66_gshared (WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7 * __this, List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * ___source0, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * ___predicate1, Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectListIterator_2_Clone_m9F1FFCBCCFAAE4455CE5961B1285B7EE5A148507_gshared (WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7 * __this, const RuntimeMethod* method)
{
	{
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_0 = (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)__this->get_source_3();
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_1 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A * L_2 = (Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A *)__this->get_selector_5();
		WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7 * L_3 = (WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7 *, List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *, Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_0, (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_1, (Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mDC374C9B32AB6285FEC1BD4963522B0B87D939E0_gshared (WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A * L_3 = (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)__this->get_source_3();
		NullCheck((List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_3);
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  L_4 = ((  Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984  (*) (List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tD9A0DAE3CF1F9450036B041168264AE0CEF1737A *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * L_5 = (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)__this->get_address_of_enumerator_6();
		int32_t L_6 = Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_inline((Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (int32_t)L_6;
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_7 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E * L_8 = (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)__this->get_predicate_4();
		int32_t L_9 = V_1;
		NullCheck((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t8D00E17E9D1413C71B5B12D3538C754C4F141A2E *)L_8, (int32_t)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A * L_11 = (Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A *)__this->get_selector_5();
		int32_t L_12 = V_1;
		NullCheck((Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A *)L_11);
		float L_13 = ((  float (*) (Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t385F771F7BAFF61E67C5427F718CE4092890D97A *)L_11, (int32_t)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * L_14 = (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mDECBD2FBFA44578D8E050CECB33BF350152E111A((Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)(Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Int32Enum,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m8399115DC06FF23B1DE8803590418F73E97648DD_gshared (WhereSelectListIterator_2_t49CAFB3B79C8154EA992CB384C309E77F7475DB7 * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mFB39C6F27784C4CC97BCD7438772858ADF2670A8_gshared (WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353 * __this, List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * ___source0, Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * ___predicate1, Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_m3E44DAE239FF810D736BA15E2FE983F5F852D50C_gshared (WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353 * __this, const RuntimeMethod* method)
{
	{
		List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * L_0 = (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)__this->get_source_3();
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_1 = (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)__this->get_predicate_4();
		Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD * L_2 = (Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD *)__this->get_selector_5();
		WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353 * L_3 = (WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353 *, List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *, Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *, Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)L_0, (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)L_1, (Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m1BF50A46163F2F1D088D13205D9EED5D7B34D6F6_gshared (WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * L_3 = (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)__this->get_source_3();
		NullCheck((List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)L_3);
		Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  L_4 = ((  Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  (*) (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * L_5 = (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)__this->get_address_of_enumerator_6();
		ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  L_6 = Enumerator_get_Current_m57C04D97C5B60AAC24531BE8204334F187B1007D_inline((Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D )L_6;
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_7 = (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_8 = (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)__this->get_predicate_4();
		ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  L_9 = V_1;
		NullCheck((Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *, ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)L_8, (ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD * L_11 = (Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD *)__this->get_selector_5();
		ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  L_12 = V_1;
		NullCheck((Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD *, ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tAA9B9F578D63DEB8EACA8DE7D7F26CEB4F6B23CD *)L_11, (ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * L_14 = (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mB0383B4B0018664482A70C59D1CB886213E34304((Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mF0F80A5F3FC2966032FA453DD940C323648AF6A3_gshared (WhereSelectListIterator_2_tDE0022E089A5D6331146D572EF17DC3ED1764353 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Single>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mDE3C5380E245E71C22781B43C022E3294D14E351_gshared (WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F * __this, List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * ___source0, Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * ___predicate1, Func_2_tE234817DD56F8D228C80039231C328502ABCF601 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE234817DD56F8D228C80039231C328502ABCF601 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectListIterator_2_Clone_m2810A560541EE8F2F9F404FF1AD269211E3C319C_gshared (WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F * __this, const RuntimeMethod* method)
{
	{
		List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * L_0 = (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)__this->get_source_3();
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_1 = (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)__this->get_predicate_4();
		Func_2_tE234817DD56F8D228C80039231C328502ABCF601 * L_2 = (Func_2_tE234817DD56F8D228C80039231C328502ABCF601 *)__this->get_selector_5();
		WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F * L_3 = (WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F *, List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *, Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *, Func_2_tE234817DD56F8D228C80039231C328502ABCF601 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)L_0, (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)L_1, (Func_2_tE234817DD56F8D228C80039231C328502ABCF601 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m849F3DFAD5FC50BF5F67B2BF493F4DE75327EAB2_gshared (WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * L_3 = (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)__this->get_source_3();
		NullCheck((List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)L_3);
		Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  L_4 = ((  Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  (*) (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * L_5 = (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)__this->get_address_of_enumerator_6();
		ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  L_6 = Enumerator_get_Current_m57C04D97C5B60AAC24531BE8204334F187B1007D_inline((Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D )L_6;
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_7 = (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_8 = (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)__this->get_predicate_4();
		ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  L_9 = V_1;
		NullCheck((Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *, ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)L_8, (ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE234817DD56F8D228C80039231C328502ABCF601 * L_11 = (Func_2_tE234817DD56F8D228C80039231C328502ABCF601 *)__this->get_selector_5();
		ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  L_12 = V_1;
		NullCheck((Func_2_tE234817DD56F8D228C80039231C328502ABCF601 *)L_11);
		float L_13 = ((  float (*) (Func_2_tE234817DD56F8D228C80039231C328502ABCF601 *, ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tE234817DD56F8D228C80039231C328502ABCF601 *)L_11, (ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * L_14 = (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mB0383B4B0018664482A70C59D1CB886213E34304((Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m95FF14B23A47021D67F1C17B5D60498B51E04828_gshared (WhereSelectListIterator_2_t487396B4D582FB799F8B70C3B77D738F77F5E58F * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m5240A511F07BDA5ED6275B852B29A0C140091D82_gshared (WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0 * __this, List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * ___source0, Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * ___predicate1, Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m80985F0A11BCDB4BF8EB459533D5C10A2B301C77_gshared (WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0 * __this, const RuntimeMethod* method)
{
	{
		List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * L_0 = (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)__this->get_source_3();
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_1 = (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)__this->get_predicate_4();
		Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA * L_2 = (Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA *)__this->get_selector_5();
		WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0 * L_3 = (WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0 *, List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *, Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *, Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)L_0, (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)L_1, (Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m724FFA80D4459F04EB61D57E205A31109511DC0A_gshared (WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 * L_3 = (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)__this->get_source_3();
		NullCheck((List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)L_3);
		Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  L_4 = ((  Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD  (*) (List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tC71A58C2AF6C235C3A1A55AA5CAA720D5F5C5C46 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * L_5 = (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)__this->get_address_of_enumerator_6();
		ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  L_6 = Enumerator_get_Current_m57C04D97C5B60AAC24531BE8204334F187B1007D_inline((Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D )L_6;
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_7 = (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A * L_8 = (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)__this->get_predicate_4();
		ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  L_9 = V_1;
		NullCheck((Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *, ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t42A89A4370445ECB53A947078230ECA9BBDCBA8A *)L_8, (ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA * L_11 = (Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA *)__this->get_selector_5();
		ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  L_12 = V_1;
		NullCheck((Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA *, ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t32F547A79E4EAEB728D1A611400C5623B4D892AA *)L_11, (ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * L_14 = (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mB0383B4B0018664482A70C59D1CB886213E34304((Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)(Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<DaggerfallWorkshop.Game.Utility.ModSupport.ModDependency,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m14D5AC76EBDFADB314371A38C67F0DFA15094F95_gshared (WhereSelectListIterator_2_tE728E618E90D1EDC1E38F36A4F9C05C800A82DE0 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mA18E5B755DF06EAE14007C295ACE48C7E0615BAE_gshared (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_mFEFF616AB589FF68AA4BCFE920814C287AC50268_gshared (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * L_2 = (Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *)__this->get_selector_5();
		WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 * L_3 = (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m758147C75B5A6CB8C86D4FEA039965940D0BDD6C_gshared (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C * L_11 = (Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t0CEE9D1C856153BA9C23BB9D7E929D577AF37A2C *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mB03479084DC9989DEAC9938DC78329DC769EFA14_gshared (WhereSelectListIterator_2_tA7C52B3E46CAC7800298BB868DD54565FDCB75B6 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Single>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m5B584B94A6D099E4BDD69EA2797218B8827C5F0F_gshared (WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectListIterator_2_Clone_m5B14105AF5C08F50EEDA1BC3839CD2C0FC35393F_gshared (WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 * L_2 = (Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 *)__this->get_selector_5();
		WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F * L_3 = (WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mF1AD1CD66F1D89CBD4853848B121B6C5D28353EE_gshared (WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 * L_11 = (Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 *)L_11);
		float L_13 = ((  float (*) (Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t78F21BB7B6C7D754A8C4D71ACB39668A8F967BA1 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mB1F2866EF1FAB0E8292A272C59D73236A0091839_gshared (WhereSelectListIterator_2_t20DB17268C20F9E95977E2485885C2547B573A2F * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m3A87EAD1F5F97AC055D753CCE2349285D70A01E0_gshared (WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F * __this, List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * ___source0, Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * ___predicate1, Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_m68B545558095E8B409121B72044416307E10550E_gshared (WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F * __this, const RuntimeMethod* method)
{
	{
		List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * L_0 = (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)__this->get_source_3();
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_1 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 * L_2 = (Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 *)__this->get_selector_5();
		WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F * L_3 = (WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F *, List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *, Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *, Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)L_0, (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_1, (Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mFFE60DA371E097A52E5D4178F0487CAAB73DA41F_gshared (WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * L_3 = (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)__this->get_source_3();
		NullCheck((List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)L_3);
		Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  L_4 = ((  Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  (*) (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * L_5 = (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)__this->get_address_of_enumerator_6();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_6 = Enumerator_get_Current_m1EB734EC70AB008E22FBDA4FCE5FF70CED4BC7D5_inline((Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_6;
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_7 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_8 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_9 = V_1;
		NullCheck((Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_8);
		bool L_10 = ((  bool (*) (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *, Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_8, (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 * L_11 = (Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 *)__this->get_selector_5();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_12 = V_1;
		NullCheck((Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 *, Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t4B545306240B3BDEFEA30C08A7D8623B196B5894 *)L_11, (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * L_14 = (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m5C01A3C015421F6AF08BB3674A3C1D4212E1CFE6((Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mCBA72722CCF2D41BBE3D42B135F6CBF84DDE3C60_gshared (WhereSelectListIterator_2_t0163693CC0D06C640C4F9D28CD8B06AC366F840F * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m9FC2A42BE6251B5AEB3F12C7069E123760EAFD69_gshared (WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE * __this, List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * ___source0, Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * ___predicate1, Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m7A81EAE160B15BA87070008FC480301E8AD754A7_gshared (WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE * __this, const RuntimeMethod* method)
{
	{
		List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * L_0 = (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)__this->get_source_3();
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_1 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * L_2 = (Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *)__this->get_selector_5();
		WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE * L_3 = (WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE *, List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *, Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *, Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)L_0, (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_1, (Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m14A7A0F611F3778E0277762EBE06A072395C594E_gshared (WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * L_3 = (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)__this->get_source_3();
		NullCheck((List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)L_3);
		Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  L_4 = ((  Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  (*) (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * L_5 = (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)__this->get_address_of_enumerator_6();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_6 = Enumerator_get_Current_m1EB734EC70AB008E22FBDA4FCE5FF70CED4BC7D5_inline((Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_6;
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_7 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_8 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_9 = V_1;
		NullCheck((Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_8);
		bool L_10 = ((  bool (*) (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *, Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_8, (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 * L_11 = (Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *)__this->get_selector_5();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_12 = V_1;
		NullCheck((Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *, Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t3738E60217584FDD65D4E05397EF3C71D8E19C98 *)L_11, (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * L_14 = (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m5C01A3C015421F6AF08BB3674A3C1D4212E1CFE6((Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mE99DFB30D65222A61DEF16F59446E414FE9E660A_gshared (WhereSelectListIterator_2_tC0F6A5ADB2AA6E5838B94847FE5F46272ABD07AE * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Single>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mA9589312A581CED0F9DF8EAC23D2B72A065E0E49_gshared (WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546 * __this, List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * ___source0, Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * ___predicate1, Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectListIterator_2_Clone_mB0BC2F105822B912778EB419D0BF81ED920B1AE7_gshared (WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546 * __this, const RuntimeMethod* method)
{
	{
		List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * L_0 = (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)__this->get_source_3();
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_1 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * L_2 = (Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *)__this->get_selector_5();
		WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546 * L_3 = (WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546 *, List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *, Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *, Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)L_0, (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_1, (Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m779E614F1DAD760477A15BA3E33BB39DE4655984_gshared (WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 * L_3 = (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)__this->get_source_3();
		NullCheck((List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)L_3);
		Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  L_4 = ((  Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B  (*) (List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t7EDE35B06DC5574B13F97159B5979551B4770DF9 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * L_5 = (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)__this->get_address_of_enumerator_6();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_6 = Enumerator_get_Current_m1EB734EC70AB008E22FBDA4FCE5FF70CED4BC7D5_inline((Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_6;
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_7 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B * L_8 = (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)__this->get_predicate_4();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_9 = V_1;
		NullCheck((Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_8);
		bool L_10 = ((  bool (*) (Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *, Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tC6FA67CDD1081CE820287AE499D58240D6BA3F0B *)L_8, (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB * L_11 = (Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *)__this->get_selector_5();
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_12 = V_1;
		NullCheck((Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *)L_11);
		float L_13 = ((  float (*) (Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *, Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tA4519303C518BBBDAC46D3F4E4144BC6FE627BFB *)L_11, (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * L_14 = (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m5C01A3C015421F6AF08BB3674A3C1D4212E1CFE6((Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)(Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<UnityEngine.Resolution,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m0BB0201851A59B6A10C11B38F8C5158D6D25361A_gshared (WhereSelectListIterator_2_t9AA0221208459721559DF01DBECDFC7149992546 * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mBCF873FA551316BFD3D4E9E710454FA6DABEFDA9_gshared (WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775 * __this, List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___source0, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate1, Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_mE044F97DC5DCCFA99F8D703834C233F46B64B8DA_gshared (WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775 * __this, const RuntimeMethod* method)
{
	{
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_0 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)__this->get_source_3();
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * L_2 = (Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *)__this->get_selector_5();
		WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775 * L_3 = (WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775 *, List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_0, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_1, (Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m46CE0D176C857CD7E9A931A702065878A0B5D8CE_gshared (WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_3 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)__this->get_source_3();
		NullCheck((List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_3);
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  L_4 = ((  Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  (*) (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * L_5 = (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)__this->get_address_of_enumerator_6();
		float L_6 = Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_inline((Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (float)L_6;
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_7 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_8 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		float L_9 = V_1;
		NullCheck((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 * L_11 = (Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *)__this->get_selector_5();
		float L_12 = V_1;
		NullCheck((Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t3603394E973C6DE8FD574D2385C276B1E479D988 *)L_11, (float)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * L_14 = (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mF6D031AEDDDEEAF750E0BFE7866FBBA9C9752C7E((Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m179CBA2B471F091571660A559CCAF4EDC6A791D9_gshared (WhereSelectListIterator_2_t8B81B69BCA4CF445D036F00C98CD34BD6C556775 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mE72A3E981B94D2502A4AFDA08A6E9867C64ED651_gshared (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 * __this, List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___source0, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate1, Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m932F0C8E73F1307B320D9321DECE070701BB780C_gshared (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 * __this, const RuntimeMethod* method)
{
	{
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_0 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)__this->get_source_3();
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_2 = (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)__this->get_selector_5();
		WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 * L_3 = (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 *, List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_0, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_1, (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m786886FF98DCF38B8D4705E8E73EDA2EFC9FCF46_gshared (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_3 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)__this->get_source_3();
		NullCheck((List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_3);
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  L_4 = ((  Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  (*) (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * L_5 = (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)__this->get_address_of_enumerator_6();
		float L_6 = Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_inline((Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (float)L_6;
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_7 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_8 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		float L_9 = V_1;
		NullCheck((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 * L_11 = (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)__this->get_selector_5();
		float L_12 = V_1;
		NullCheck((Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tE11BDF71BCB314E58F1A9CFC9DAE08A5F717E2A0 *)L_11, (float)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * L_14 = (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mF6D031AEDDDEEAF750E0BFE7866FBBA9C9752C7E((Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m0CA9E839CED94CB32A63000C2B9318E221D5C580_gshared (WhereSelectListIterator_2_t77C04C3B7AD0F11B52AC864B3B1635FBFC5D2259 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Single>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m0C1261002AE29944E298C7F0C6C1CE6F33B66663_gshared (WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB * __this, List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___source0, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate1, Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectListIterator_2_Clone_m0C2D273D61C436B3B16DAADBC8C6BCD603A95155_gshared (WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB * __this, const RuntimeMethod* method)
{
	{
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_0 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)__this->get_source_3();
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_1 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_2 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)__this->get_selector_5();
		WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB * L_3 = (WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB *, List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_0, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_1, (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mA9B825FC933029FE3C11340D1861EABB030F8455_gshared (WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * L_3 = (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)__this->get_source_3();
		NullCheck((List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_3);
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  L_4 = ((  Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783  (*) (List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * L_5 = (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)__this->get_address_of_enumerator_6();
		float L_6 = Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_inline((Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (float)L_6;
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_7 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_8 = (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)__this->get_predicate_4();
		float L_9 = V_1;
		NullCheck((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_8, (float)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 * L_11 = (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)__this->get_selector_5();
		float L_12 = V_1;
		NullCheck((Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)L_11);
		float L_13 = ((  float (*) (Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *, float, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t10AC82CE8886E423467ACF3C9F68E7DA50E26149 *)L_11, (float)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * L_14 = (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mF6D031AEDDDEEAF750E0BFE7866FBBA9C9752C7E((Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)(Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Single,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mDA6BAE198F731052D20CA77532E473206822E739_gshared (WhereSelectListIterator_2_t8D95D48F020ABEF07AF96D3D786CAAB959419DBB * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m3B1A70EE67F3AB9C4D5979C5A84AABD0E6AA9FE3_gshared (WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184 * __this, List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * ___source0, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate1, Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_mC973C2076B5B9C1A8D023DC77EC09AD34B9C36F7_gshared (WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184 * __this, const RuntimeMethod* method)
{
	{
		List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * L_0 = (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)__this->get_source_3();
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * L_2 = (Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *)__this->get_selector_5();
		WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184 * L_3 = (WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184 *, List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)L_0, (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_1, (Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m4FE071BB8C22BC9E16A566C18E71D652D3CA7B31_gshared (WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * L_3 = (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)__this->get_source_3();
		NullCheck((List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)L_3);
		Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  L_4 = ((  Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  (*) (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * L_5 = (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_6 = Enumerator_get_Current_m290640F097AF113C466EEBA5107E3850AB8FBA65_inline((Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_6;
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_7 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_8 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_9 = V_1;
		NullCheck((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A * L_11 = (Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *)__this->get_selector_5();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_12 = V_1;
		NullCheck((Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t6CD6AD2729CA2026CA2720DB004DA5F20A66256A *)L_11, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * L_14 = (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m7A815733AA2D57CE50C0579CD9463138FC8122C8((Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mF41BE3188849E2081F72C317C5853D47FF67DDF0_gshared (WhereSelectListIterator_2_t25017F35116B34219E3139A17C37E89430A2D184 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mAB6565916C93B042C1AB6F734BEF92895B6F754C_gshared (WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE * __this, List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * ___source0, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate1, Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectListIterator_2_Clone_m4176923AE0AA0DB0FCA11AA4C75EADCAA0344F22_gshared (WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE * __this, const RuntimeMethod* method)
{
	{
		List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * L_0 = (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)__this->get_source_3();
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * L_2 = (Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *)__this->get_selector_5();
		WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE * L_3 = (WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE *, List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)L_0, (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_1, (Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m2F5DB412EFB541E9B38652B1643B020056E8ACEF_gshared (WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * L_3 = (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)__this->get_source_3();
		NullCheck((List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)L_3);
		Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  L_4 = ((  Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  (*) (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * L_5 = (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_6 = Enumerator_get_Current_m290640F097AF113C466EEBA5107E3850AB8FBA65_inline((Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_6;
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_7 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_8 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_9 = V_1;
		NullCheck((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 * L_11 = (Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *)__this->get_selector_5();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_12 = V_1;
		NullCheck((Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *)L_11);
		float L_13 = ((  float (*) (Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tE2DBFCC3777D68930230B86E77640894D3FC6D04 *)L_11, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * L_14 = (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m7A815733AA2D57CE50C0579CD9463138FC8122C8((Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mDFD9C50F207C68F5660D971B9C339AB3BC23587A_gshared (WhereSelectListIterator_2_t3E44A60D3A01FDFD2B40B1206565008E95BB0AFE * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mB1DE3D19DF743B6881529C03F3BE46C03BB83907_gshared (WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559 * __this, List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * ___source0, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * ___predicate1, Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m4E481882F338496B5C33880176DF199D54DDA135_gshared (WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559 * __this, const RuntimeMethod* method)
{
	{
		List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * L_0 = (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)__this->get_source_3();
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_1 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * L_2 = (Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *)__this->get_selector_5();
		WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559 * L_3 = (WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559 *, List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *, Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)L_0, (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_1, (Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m930CC3D995C60B9C5C12026219746100193916D1_gshared (WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 * L_3 = (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)__this->get_source_3();
		NullCheck((List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)L_3);
		Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  L_4 = ((  Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6  (*) (List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t27E3D1BDE7648E380336F5E74D8620E40AF86EF7 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * L_5 = (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_6 = Enumerator_get_Current_m290640F097AF113C466EEBA5107E3850AB8FBA65_inline((Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_6;
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_7 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F * L_8 = (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)__this->get_predicate_4();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_9 = V_1;
		NullCheck((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t407F79CF5CAA56C94E6B18F57DC13D72839D606F *)L_8, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 * L_11 = (Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *)__this->get_selector_5();
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_12 = V_1;
		NullCheck((Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *, KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t6C7DC32D47A1098C559E0D6209E9F094B1F00B18 *)L_11, (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * L_14 = (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m7A815733AA2D57CE50C0579CD9463138FC8122C8((Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)(Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Int64,System.Object>,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m34156B9A9BD103514019A4E5F9A2CFAF62F8FF44_gshared (WhereSelectListIterator_2_t3D7D7ABDF507F1CC27E9882931FCA98C32F51559 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m3D7597A02A6BDFAB9531AC298A94244BD779EF93_gshared (WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592 * __this, List_1_t9B9E3A64698A414C03953277E8E6D511E145491F * ___source0, Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * ___predicate1, Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this);
		((  void (*) (Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t9B9E3A64698A414C03953277E8E6D511E145491F * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 * WhereSelectListIterator_2_Clone_m9921069373E3DBB06BB7FC02094F4BF8D9779638_gshared (WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592 * __this, const RuntimeMethod* method)
{
	{
		List_1_t9B9E3A64698A414C03953277E8E6D511E145491F * L_0 = (List_1_t9B9E3A64698A414C03953277E8E6D511E145491F *)__this->get_source_3();
		Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * L_1 = (Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)__this->get_predicate_4();
		Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * L_2 = (Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *)__this->get_selector_5();
		WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592 * L_3 = (WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592 *, List_1_t9B9E3A64698A414C03953277E8E6D511E145491F *, Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *, Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t9B9E3A64698A414C03953277E8E6D511E145491F *)L_0, (Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)L_1, (Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m67BFDD1CFC06634BB2748F95E3FE87FE8823B661_gshared (WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t9B9E3A64698A414C03953277E8E6D511E145491F * L_3 = (List_1_t9B9E3A64698A414C03953277E8E6D511E145491F *)__this->get_source_3();
		NullCheck((List_1_t9B9E3A64698A414C03953277E8E6D511E145491F *)L_3);
		Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D  L_4 = ((  Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D  (*) (List_1_t9B9E3A64698A414C03953277E8E6D511E145491F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t9B9E3A64698A414C03953277E8E6D511E145491F *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D * L_5 = (Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  L_6 = Enumerator_get_Current_mD3498F86E6123F926AA4E50358C9F6515C73EECC_inline((Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D *)(Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A )L_6;
		Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * L_7 = (Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 * L_8 = (Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)__this->get_predicate_4();
		KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  L_9 = V_1;
		NullCheck((Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *, KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t78B6CBEE8A642B098421AC130BE747E9CDB7D614 *)L_8, (KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 * L_11 = (Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *)__this->get_selector_5();
		KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  L_12 = V_1;
		NullCheck((Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *)L_11);
		ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE  L_13 = ((  ConsoleCommand_t7F8796EBA09CC329082D67307997C5791850A0AE  (*) (Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *, KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t0A455FBD2C654FE4C689B5441E04441BC59D54B2 *)L_11, (KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D * L_14 = (Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mB734D19B11DC29D53835D8BA81B44955AB0FA62E((Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D *)(Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<Wenzil.Console.ConsoleCommand>::Dispose() */, (Iterator_1_tFF423718A163022BA293D3AB9EEBB540394141E2 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,Wenzil.Console.ConsoleCommand>,Wenzil.Console.ConsoleCommand>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m74DB256540A4631B5863D110F084C9586681D7BD_gshared (WhereSelectListIterator_2_t42D5EA7BAB28E746FC9AB9AFE92713594BF63592 * __this, Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1D8672A800AD4D70F00916ACF7506ECA8A6BAF8A * L_1 = (WhereEnumerableIterator_1_t1D8672A800AD4D70F00916ACF7506ECA8A6BAF8A *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1D8672A800AD4D70F00916ACF7506ECA8A6BAF8A *, RuntimeObject*, Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t222AEBA0622CC2A3D9186DB9913E065FFAF8AD5F *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m46C9D846839C09BE2917636F0C57D8DE49C3157E_gshared (WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238 * __this, List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * ___source0, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate1, Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		((  void (*) (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 * WhereSelectListIterator_2_Clone_mA3B8485062030924299BA0E2DAC49675361F02D6_gshared (WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238 * __this, const RuntimeMethod* method)
{
	{
		List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * L_0 = (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)__this->get_source_3();
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * L_2 = (Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *)__this->get_selector_5();
		WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238 * L_3 = (WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238 *, List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)L_0, (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_1, (Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_m0C64B51508852AC4EB6D41D8178AC3E0E715CB3A_gshared (WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * L_3 = (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)__this->get_source_3();
		NullCheck((List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)L_3);
		Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  L_4 = ((  Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  (*) (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * L_5 = (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_6 = Enumerator_get_Current_m36A49F353C175C557E3490621F4489D7D95A646B_inline((Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_6;
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_7 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_8 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_9 = V_1;
		NullCheck((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E * L_11 = (Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *)__this->get_selector_5();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_12 = V_1;
		NullCheck((Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *)L_11);
		int32_t L_13 = ((  int32_t (*) (Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tE11D275E92E41C7F0153BCC1A05E2657AECFED9E *)L_11, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * L_14 = (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mC851DE6441AF295DB85CFCE9C6F9242152172F67((Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Int32>::Dispose() */, (Iterator_1_tCFFC952B03DBE4E956DE317DB9704D936AEA2379 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Int32>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mA0D06E15DAC9CE66E1D7D032733A299D0EC817B0_gshared (WhereSelectListIterator_2_t464A050BB0132CEACEB1D9578DA9AA5AC98B5238 * __this, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA * L_1 = (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t9F4DDC70173BABD72AEC7AA00D62F4FAE2613CEA *, RuntimeObject*, Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t2EBF98B0BA555D9F0633C9BCCBE3DF332B9C1274 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m530D4471452DA4089C130C0D9FC5910907BF6007_gshared (WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529 * __this, List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * ___source0, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate1, Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m72EFE228727E1A3BF476F5CF995391D549ED4C1E_gshared (WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529 * __this, const RuntimeMethod* method)
{
	{
		List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * L_0 = (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)__this->get_source_3();
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * L_2 = (Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *)__this->get_selector_5();
		WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529 * L_3 = (WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529 *, List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)L_0, (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_1, (Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mB979C50B4E1833CA8C6A8394B5503C7FEB4A608A_gshared (WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * L_3 = (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)__this->get_source_3();
		NullCheck((List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)L_3);
		Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  L_4 = ((  Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  (*) (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * L_5 = (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_6 = Enumerator_get_Current_m36A49F353C175C557E3490621F4489D7D95A646B_inline((Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_6;
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_7 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_8 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_9 = V_1;
		NullCheck((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD * L_11 = (Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *)__this->get_selector_5();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_12 = V_1;
		NullCheck((Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t10CDF2C6D18EB51CBA3E607199613B198E2CFDFD *)L_11, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * L_14 = (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mC851DE6441AF295DB85CFCE9C6F9242152172F67((Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mB12D7743450CEE6D221C22755DDB9F022B7E24B1_gshared (WhereSelectListIterator_2_tE552775A10E0B597E67A5A9B0010974038D5D529 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_m17AAC16CF5ED042BCFB841BC686D60C7A30ED704_gshared (WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077 * __this, List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * ___source0, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * ___predicate1, Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		((  void (*) (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 * WhereSelectListIterator_2_Clone_mD1AE56F42BB9C5073A2D462749609FFB30011676_gshared (WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077 * __this, const RuntimeMethod* method)
{
	{
		List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * L_0 = (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)__this->get_source_3();
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_1 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * L_2 = (Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *)__this->get_selector_5();
		WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077 * L_3 = (WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077 *, List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *, Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)L_0, (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_1, (Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mE04DB7761BBC12A0B66433ED846DE0CDDEF6772A_gshared (WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		int32_t L_0 = (int32_t)((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E * L_3 = (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)__this->get_source_3();
		NullCheck((List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)L_3);
		Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  L_4 = ((  Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04  (*) (List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_tFBF6A022293416BA5AD7B89F3A150C81EF05606E *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * L_5 = (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)__this->get_address_of_enumerator_6();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_6 = Enumerator_get_Current_m36A49F353C175C557E3490621F4489D7D95A646B_inline((Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_6;
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_7 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 * L_8 = (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)__this->get_predicate_4();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_9 = V_1;
		NullCheck((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_tD570D29C5027C04365E3BD2965A2B25DE326F8D7 *)L_8, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B * L_11 = (Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *)__this->get_selector_5();
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_12 = V_1;
		NullCheck((Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *)L_11);
		float L_13 = ((  float (*) (Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *, KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 , const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_t3EA169DF6053DFF60BE8BDD1E1460D9AD57D770B *)L_11, (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * L_14 = (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_mC851DE6441AF295DB85CFCE9C6F9242152172F67((Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)(Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Single>::Dispose() */, (Iterator_1_t859CAAB8DD3B63D3758E13D2802ED7523B331199 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>,System.Single>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_m66F5C93EBB9406D15476C427C5C71538A8EDD01E_gshared (WhereSelectListIterator_2_t1AC39668E18FC077E93788F15F41ADB23264A077 * __this, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A * L_0 = ___predicate0;
		WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F * L_1 = (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_tC5339F8E75587CBA511FF3EDFBA6D5A81E54057F *, RuntimeObject*, Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t7214BC9C9A47482C751067B9A197D164F4041A3A *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Object>::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhereSelectListIterator_2__ctor_mCF313A191371C8CCC2E79D89A3BF21714EFDB20E_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * ___source0, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate1, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * ___selector2, const RuntimeMethod* method)
{
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		((  void (*) (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = ___source0;
		__this->set_source_3(L_0);
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = ___predicate1;
		__this->set_predicate_4(L_1);
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = ___selector2;
		__this->set_selector_5(L_2);
		return;
	}
}
// System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Object>::Clone()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 * WhereSelectListIterator_2_Clone_m667BCD94E83BB3A02AF2D66E07B089FA86971342_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, const RuntimeMethod* method)
{
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_0 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_1 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_2 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * L_3 = (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 *, List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_3, (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_0, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_1, (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return L_3;
	}
}
// System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool WhereSelectListIterator_2_MoveNext_mEE0E8B173345B059100E0736D106FFAE0C2D29CA_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	RuntimeObject * V_1 = NULL;
	{
		int32_t L_0 = (int32_t)((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->get_state_1();
		V_0 = (int32_t)L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)1)))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)2)))
		{
			goto IL_0061;
		}
	}
	{
		goto IL_0074;
	}

IL_0011:
	{
		List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * L_3 = (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)__this->get_source_3();
		NullCheck((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3);
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  L_4 = ((  Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6  (*) (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		__this->set_enumerator_6(L_4);
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_state_1(2);
		goto IL_0061;
	}

IL_002b:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_5 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		RuntimeObject * L_6 = Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_inline((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_1 = (RuntimeObject *)L_6;
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_7 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_8 = (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)__this->get_predicate_4();
		RuntimeObject * L_9 = V_1;
		NullCheck((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8);
		bool L_10 = ((  bool (*) (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_8, (RuntimeObject *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		if (!L_10)
		{
			goto IL_0061;
		}
	}

IL_004d:
	{
		Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 * L_11 = (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)__this->get_selector_5();
		RuntimeObject * L_12 = V_1;
		NullCheck((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11);
		RuntimeObject * L_13 = ((  RuntimeObject * (*) (Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((Func_2_tFF5BB8F40A35B1BEA00D4EBBC6CBE7184A584436 *)L_11, (RuntimeObject *)L_12, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this)->set_current_2(L_13);
		return (bool)1;
	}

IL_0061:
	{
		Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * L_14 = (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)__this->get_address_of_enumerator_6();
		bool L_15 = Enumerator_MoveNext_m2E56233762839CE55C67E00AC8DD3D4D3F6C0DF0((Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)(Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		if (L_15)
		{
			goto IL_002b;
		}
	}
	{
		NullCheck((Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
		VirtActionInvoker0::Invoke(12 /* System.Void System.Linq.Enumerable/Iterator`1<System.Object>::Dispose() */, (Iterator_1_t674ABE41CF4096D4BE4D51E21FEBDADBF74CC279 *)__this);
	}

IL_0074:
	{
		return (bool)0;
	}
}
// System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2<System.Object,System.Object>::Where(System.Func`2<TResult,System.Boolean>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* WhereSelectListIterator_2_Where_mAC87184664F73DD7F3EC4AB4CE2BDE71BE76249D_gshared (WhereSelectListIterator_2_t85B78DFF0573BC95A62C79D6088FA39DFEBE1AF2 * __this, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * ___predicate0, const RuntimeMethod* method)
{
	{
		Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 * L_0 = ___predicate0;
		WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 * L_1 = (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 10));
		((  void (*) (WhereEnumerableIterator_1_t1E9FDCFD8F8136C6A5A5740C1E093EF03F0B5CE0 *, RuntimeObject*, Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11)->methodPointer)(L_1, (RuntimeObject*)__this, (Func_2_t99409DECFF50F0FA9B427C863AC6C99C66E6F9F8 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 11));
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.Linq.XHashtable`1_XHashtableState<System.Object>::.ctor(System.Xml.Linq.XHashtable`1_ExtractKeyDelegate<TValue>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XHashtableState__ctor_mE68E63173C17EF13FA0F533F0AA34F4FA753674F_gshared (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * __this, ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 * ___extractKey0, int32_t ___capacity1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XHashtableState__ctor_mE68E63173C17EF13FA0F533F0AA34F4FA753674F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405((RuntimeObject *)__this, /*hidden argument*/NULL);
		int32_t L_0 = ___capacity1;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)L_0);
		__this->set_buckets_0(L_1);
		int32_t L_2 = ___capacity1;
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_3 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)(EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)SZArrayNew(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0), (uint32_t)L_2);
		__this->set_entries_1(L_3);
		ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 * L_4 = ___extractKey0;
		__this->set_extractKey_3(L_4);
		return;
	}
}
// System.Xml.Linq.XHashtable`1_XHashtableState<TValue> System.Xml.Linq.XHashtable`1_XHashtableState<System.Object>::Resize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * XHashtableState_Resize_m2F60ACE78E15F4F50EAD11B7DA897A2BB7B7362A_gshared (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (XHashtableState_Resize_m2F60ACE78E15F4F50EAD11B7DA897A2BB7B7362A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * V_1 = NULL;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	RuntimeObject * V_6 = NULL;
	{
		int32_t L_0 = (int32_t)__this->get_numEntries_2();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_1 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		NullCheck(L_1);
		if ((((int32_t)L_0) >= ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))))))
		{
			goto IL_0012;
		}
	}
	{
		return __this;
	}

IL_0012:
	{
		V_0 = (int32_t)0;
		V_2 = (int32_t)0;
		goto IL_00a7;
	}

IL_001b:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_2 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		int32_t L_3 = V_2;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		int32_t L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_3 = (int32_t)L_5;
		int32_t L_6 = V_3;
		if (L_6)
		{
			goto IL_009f;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_7 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		int32_t L_8 = V_2;
		NullCheck(L_7);
		int32_t L_9 = Interlocked_CompareExchange_m317AD9524376B7BE74DD9069346E345F2B131382((int32_t*)(int32_t*)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_8))), (int32_t)(-1), (int32_t)0, /*hidden argument*/NULL);
		V_3 = (int32_t)L_9;
		goto IL_009f;
	}

IL_003d:
	{
		ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 * L_10 = (ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)__this->get_extractKey_3();
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_11 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_12 = V_3;
		NullCheck(L_11);
		RuntimeObject * L_13 = (RuntimeObject *)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_12)))->get_Value_0();
		NullCheck((ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)L_10);
		String_t* L_14 = ((  String_t* (*) (ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)L_10, (RuntimeObject *)L_13, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		if (!L_14)
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_15 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_15, (int32_t)1));
	}

IL_005f:
	{
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_16 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_17 = V_3;
		NullCheck(L_16);
		int32_t L_18 = (int32_t)((L_16)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_17)))->get_Next_2();
		if (L_18)
		{
			goto IL_008d;
		}
	}
	{
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_19 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_20 = V_3;
		NullCheck(L_19);
		int32_t* L_21 = (int32_t*)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20)))->get_address_of_Next_2();
		int32_t L_22 = Interlocked_CompareExchange_m317AD9524376B7BE74DD9069346E345F2B131382((int32_t*)(int32_t*)L_21, (int32_t)(-1), (int32_t)0, /*hidden argument*/NULL);
		V_3 = (int32_t)L_22;
		goto IL_009f;
	}

IL_008d:
	{
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_23 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_24 = V_3;
		NullCheck(L_23);
		int32_t L_25 = (int32_t)((L_23)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_24)))->get_Next_2();
		V_3 = (int32_t)L_25;
	}

IL_009f:
	{
		int32_t L_26 = V_3;
		if ((((int32_t)L_26) > ((int32_t)0)))
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_27 = V_2;
		V_2 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_27, (int32_t)1));
	}

IL_00a7:
	{
		int32_t L_28 = V_2;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_29 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_29)->max_length)))))))
		{
			goto IL_001b;
		}
	}
	{
		int32_t L_30 = V_0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_31 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		NullCheck(L_31);
		if ((((int32_t)L_30) >= ((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_31)->max_length))))/(int32_t)2)))))
		{
			goto IL_00cd;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_32 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		NullCheck(L_32);
		V_0 = (int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_32)->max_length))));
		goto IL_00e2;
	}

IL_00cd:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_33 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		NullCheck(L_33);
		V_0 = (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_33)->max_length)))), (int32_t)2));
		int32_t L_34 = V_0;
		if ((((int32_t)L_34) >= ((int32_t)0)))
		{
			goto IL_00e2;
		}
	}
	{
		OverflowException_tD1FBF4E54D81EC98EEF386B69344D336D1EC1AB9 * L_35 = (OverflowException_tD1FBF4E54D81EC98EEF386B69344D336D1EC1AB9 *)il2cpp_codegen_object_new(OverflowException_tD1FBF4E54D81EC98EEF386B69344D336D1EC1AB9_il2cpp_TypeInfo_var);
		OverflowException__ctor_m9D5C4C7E08BE06B4D72424590FB4365733FC351D(L_35, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_35, XHashtableState_Resize_m2F60ACE78E15F4F50EAD11B7DA897A2BB7B7362A_RuntimeMethod_var);
	}

IL_00e2:
	{
		ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 * L_36 = (ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)__this->get_extractKey_3();
		int32_t L_37 = V_0;
		XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * L_38 = (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2));
		((  void (*) (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *, ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)(L_38, (ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)L_36, (int32_t)L_37, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		V_1 = (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)L_38;
		V_4 = (int32_t)0;
		goto IL_013b;
	}

IL_00f4:
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_39 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		int32_t L_40 = V_4;
		NullCheck(L_39);
		int32_t L_41 = L_40;
		int32_t L_42 = (L_39)->GetAt(static_cast<il2cpp_array_size_t>(L_41));
		V_5 = (int32_t)L_42;
		goto IL_0130;
	}

IL_0101:
	{
		XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * L_43 = V_1;
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_44 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_45 = V_5;
		NullCheck(L_44);
		RuntimeObject * L_46 = (RuntimeObject *)((L_44)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_45)))->get_Value_0();
		NullCheck((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)L_43);
		((  bool (*) (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *, RuntimeObject *, RuntimeObject **, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)L_43, (RuntimeObject *)L_46, (RuntimeObject **)(RuntimeObject **)(&V_6), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_47 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_48 = V_5;
		NullCheck(L_47);
		int32_t L_49 = (int32_t)((L_47)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48)))->get_Next_2();
		V_5 = (int32_t)L_49;
	}

IL_0130:
	{
		int32_t L_50 = V_5;
		if ((((int32_t)L_50) > ((int32_t)0)))
		{
			goto IL_0101;
		}
	}
	{
		int32_t L_51 = V_4;
		V_4 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_51, (int32_t)1));
	}

IL_013b:
	{
		int32_t L_52 = V_4;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_53 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		NullCheck(L_53);
		if ((((int32_t)L_52) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_53)->max_length)))))))
		{
			goto IL_00f4;
		}
	}
	{
		XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * L_54 = V_1;
		return L_54;
	}
}
// System.Boolean System.Xml.Linq.XHashtable`1_XHashtableState<System.Object>::TryGetValue(System.String,System.Int32,System.Int32,TValue&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XHashtableState_TryGetValue_mA9B06BC8E0F95FB0FC63CC3D765A4F22887EB93A_gshared (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * __this, String_t* ___key0, int32_t ___index1, int32_t ___count2, RuntimeObject ** ___value3, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		String_t* L_0 = ___key0;
		int32_t L_1 = ___index1;
		int32_t L_2 = ___count2;
		int32_t L_3 = ((  int32_t (*) (String_t*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((String_t*)L_0, (int32_t)L_1, (int32_t)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_0 = (int32_t)L_3;
		V_1 = (int32_t)0;
		int32_t L_4 = V_0;
		String_t* L_5 = ___key0;
		int32_t L_6 = ___index1;
		int32_t L_7 = ___count2;
		NullCheck((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)__this);
		bool L_8 = ((  bool (*) (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *, int32_t, String_t*, int32_t, int32_t, int32_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)__this, (int32_t)L_4, (String_t*)L_5, (int32_t)L_6, (int32_t)L_7, (int32_t*)(int32_t*)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_8)
		{
			goto IL_0033;
		}
	}
	{
		RuntimeObject ** L_9 = ___value3;
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_10 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_11 = V_1;
		NullCheck(L_10);
		RuntimeObject * L_12 = (RuntimeObject *)((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)))->get_Value_0();
		*(RuntimeObject **)L_9 = L_12;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_9, (void*)L_12);
		return (bool)1;
	}

IL_0033:
	{
		RuntimeObject ** L_13 = ___value3;
		il2cpp_codegen_initobj(L_13, sizeof(RuntimeObject *));
		return (bool)0;
	}
}
// System.Boolean System.Xml.Linq.XHashtable`1_XHashtableState<System.Object>::TryAdd(TValue,TValue&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XHashtableState_TryAdd_m66B188F6F86501E916DC5D9C3C13AC115C08F977_gshared (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * __this, RuntimeObject * ___value0, RuntimeObject ** ___newValue1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	int32_t V_3 = 0;
	{
		RuntimeObject ** L_0 = ___newValue1;
		RuntimeObject * L_1 = ___value0;
		*(RuntimeObject **)L_0 = L_1;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_0, (void*)L_1);
		ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 * L_2 = (ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)__this->get_extractKey_3();
		RuntimeObject * L_3 = ___value0;
		NullCheck((ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)L_2);
		String_t* L_4 = ((  String_t* (*) (ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)L_2, (RuntimeObject *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_2 = (String_t*)L_4;
		String_t* L_5 = V_2;
		if (L_5)
		{
			goto IL_0019;
		}
	}
	{
		return (bool)1;
	}

IL_0019:
	{
		String_t* L_6 = V_2;
		String_t* L_7 = V_2;
		NullCheck((String_t*)L_7);
		int32_t L_8 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline((String_t*)L_7, /*hidden argument*/NULL);
		int32_t L_9 = ((  int32_t (*) (String_t*, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((String_t*)L_6, (int32_t)0, (int32_t)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		V_3 = (int32_t)L_9;
		int32_t* L_10 = (int32_t*)__this->get_address_of_numEntries_2();
		int32_t L_11 = Interlocked_Increment_mEF7FA106280D9E57DA8A97887389A961B65E47D8((int32_t*)(int32_t*)L_10, /*hidden argument*/NULL);
		V_0 = (int32_t)L_11;
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) < ((int32_t)0)))
		{
			goto IL_0042;
		}
	}
	{
		int32_t L_13 = V_0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_14 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length)))))))
		{
			goto IL_0044;
		}
	}

IL_0042:
	{
		return (bool)0;
	}

IL_0044:
	{
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_15 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_16 = V_0;
		NullCheck(L_15);
		RuntimeObject * L_17 = ___value0;
		((L_15)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_16)))->set_Value_0(L_17);
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_18 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_19 = V_0;
		NullCheck(L_18);
		int32_t L_20 = V_3;
		((L_18)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_19)))->set_HashCode_1(L_20);
		Thread_MemoryBarrier_m9E2B68F7889D5D3AD76126930EE120D51C1B3402(/*hidden argument*/NULL);
		V_1 = (int32_t)0;
		goto IL_00b7;
	}

IL_0071:
	{
		int32_t L_21 = V_1;
		if (L_21)
		{
			goto IL_0095;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_22 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		int32_t L_23 = V_3;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_24 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		NullCheck(L_24);
		NullCheck(L_22);
		int32_t L_25 = V_0;
		int32_t L_26 = Interlocked_CompareExchange_m317AD9524376B7BE74DD9069346E345F2B131382((int32_t*)(int32_t*)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_23&(int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length)))), (int32_t)1))))))), (int32_t)L_25, (int32_t)0, /*hidden argument*/NULL);
		V_1 = (int32_t)L_26;
		goto IL_00ae;
	}

IL_0095:
	{
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_27 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_28 = V_1;
		NullCheck(L_27);
		int32_t* L_29 = (int32_t*)((L_27)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_28)))->get_address_of_Next_2();
		int32_t L_30 = V_0;
		int32_t L_31 = Interlocked_CompareExchange_m317AD9524376B7BE74DD9069346E345F2B131382((int32_t*)(int32_t*)L_29, (int32_t)L_30, (int32_t)0, /*hidden argument*/NULL);
		V_1 = (int32_t)L_31;
	}

IL_00ae:
	{
		int32_t L_32 = V_1;
		if ((((int32_t)L_32) > ((int32_t)0)))
		{
			goto IL_00b7;
		}
	}
	{
		int32_t L_33 = V_1;
		return (bool)((((int32_t)L_33) == ((int32_t)0))? 1 : 0);
	}

IL_00b7:
	{
		int32_t L_34 = V_3;
		String_t* L_35 = V_2;
		String_t* L_36 = V_2;
		NullCheck((String_t*)L_36);
		int32_t L_37 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline((String_t*)L_36, /*hidden argument*/NULL);
		NullCheck((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)__this);
		bool L_38 = ((  bool (*) (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *, int32_t, String_t*, int32_t, int32_t, int32_t*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)__this, (int32_t)L_34, (String_t*)L_35, (int32_t)0, (int32_t)L_37, (int32_t*)(int32_t*)(&V_1), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		if (!L_38)
		{
			goto IL_0071;
		}
	}
	{
		RuntimeObject ** L_39 = ___newValue1;
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_40 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_41 = V_1;
		NullCheck(L_40);
		RuntimeObject * L_42 = (RuntimeObject *)((L_40)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_41)))->get_Value_0();
		*(RuntimeObject **)L_39 = L_42;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_39, (void*)L_42);
		return (bool)1;
	}
}
// System.Boolean System.Xml.Linq.XHashtable`1_XHashtableState<System.Object>::FindEntry(System.Int32,System.String,System.Int32,System.Int32,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XHashtableState_FindEntry_mE4A022366877BF9D180DEA774989355DA126E01B_gshared (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * __this, int32_t ___hashCode0, String_t* ___key1, int32_t ___index2, int32_t ___count3, int32_t* ___entryIndex4, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	{
		int32_t* L_0 = ___entryIndex4;
		int32_t L_1 = *((int32_t*)L_0);
		V_0 = (int32_t)L_1;
		int32_t L_2 = V_0;
		if (L_2)
		{
			goto IL_0020;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_3 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		int32_t L_4 = ___hashCode0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_5 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		NullCheck(L_5);
		NullCheck(L_3);
		int32_t L_6 = ((int32_t)((int32_t)L_4&(int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length)))), (int32_t)1))));
		int32_t L_7 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		V_1 = (int32_t)L_7;
		goto IL_00f9;
	}

IL_0020:
	{
		int32_t L_8 = V_0;
		V_1 = (int32_t)L_8;
		goto IL_00f9;
	}

IL_0027:
	{
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_9 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_10 = V_1;
		NullCheck(L_9);
		int32_t L_11 = (int32_t)((L_9)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_10)))->get_HashCode_1();
		int32_t L_12 = ___hashCode0;
		if ((!(((uint32_t)L_11) == ((uint32_t)L_12))))
		{
			goto IL_00e5;
		}
	}
	{
		ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 * L_13 = (ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)__this->get_extractKey_3();
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_14 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_15 = V_1;
		NullCheck(L_14);
		RuntimeObject * L_16 = (RuntimeObject *)((L_14)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_15)))->get_Value_0();
		NullCheck((ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)L_13);
		String_t* L_17 = ((  String_t* (*) (ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)L_13, (RuntimeObject *)L_16, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_2 = (String_t*)L_17;
		String_t* L_18 = V_2;
		if (L_18)
		{
			goto IL_00c8;
		}
	}
	{
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_19 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_20 = V_1;
		NullCheck(L_19);
		int32_t L_21 = (int32_t)((L_19)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_20)))->get_Next_2();
		if ((((int32_t)L_21) <= ((int32_t)0)))
		{
			goto IL_00e5;
		}
	}
	{
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_22 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_23 = V_1;
		NullCheck(L_22);
		RuntimeObject ** L_24 = (RuntimeObject **)((L_22)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_23)))->get_address_of_Value_0();
		il2cpp_codegen_initobj(L_24, sizeof(RuntimeObject *));
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_25 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_26 = V_1;
		NullCheck(L_25);
		int32_t L_27 = (int32_t)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))->get_Next_2();
		V_1 = (int32_t)L_27;
		int32_t L_28 = V_0;
		if (L_28)
		{
			goto IL_00b4;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_29 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		int32_t L_30 = ___hashCode0;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_31 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)__this->get_buckets_0();
		NullCheck(L_31);
		int32_t L_32 = V_1;
		NullCheck(L_29);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_30&(int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_31)->max_length)))), (int32_t)1))))), (int32_t)L_32);
		goto IL_00f9;
	}

IL_00b4:
	{
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_33 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_34 = V_0;
		NullCheck(L_33);
		int32_t L_35 = V_1;
		((L_33)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_34)))->set_Next_2(L_35);
		goto IL_00f9;
	}

IL_00c8:
	{
		int32_t L_36 = ___count3;
		String_t* L_37 = V_2;
		NullCheck((String_t*)L_37);
		int32_t L_38 = String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline((String_t*)L_37, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_36) == ((uint32_t)L_38))))
		{
			goto IL_00e5;
		}
	}
	{
		String_t* L_39 = ___key1;
		int32_t L_40 = ___index2;
		String_t* L_41 = V_2;
		int32_t L_42 = ___count3;
		int32_t L_43 = String_CompareOrdinal_m080D376EC2E7A0C528A440094A0DB97DFB34CD41((String_t*)L_39, (int32_t)L_40, (String_t*)L_41, (int32_t)0, (int32_t)L_42, /*hidden argument*/NULL);
		if (L_43)
		{
			goto IL_00e5;
		}
	}
	{
		int32_t* L_44 = ___entryIndex4;
		int32_t L_45 = V_1;
		*((int32_t*)L_44) = (int32_t)L_45;
		return (bool)1;
	}

IL_00e5:
	{
		int32_t L_46 = V_1;
		V_0 = (int32_t)L_46;
		EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E* L_47 = (EntryU5BU5D_tD96158975B5E90990947D08F791911D27646C20E*)__this->get_entries_1();
		int32_t L_48 = V_1;
		NullCheck(L_47);
		int32_t L_49 = (int32_t)((L_47)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_48)))->get_Next_2();
		V_1 = (int32_t)L_49;
	}

IL_00f9:
	{
		int32_t L_50 = V_1;
		if ((((int32_t)L_50) > ((int32_t)0)))
		{
			goto IL_0027;
		}
	}
	{
		int32_t* L_51 = ___entryIndex4;
		int32_t L_52 = V_0;
		*((int32_t*)L_51) = (int32_t)L_52;
		return (bool)0;
	}
}
// System.Int32 System.Xml.Linq.XHashtable`1_XHashtableState<System.Object>::ComputeHashCode(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t XHashtableState_ComputeHashCode_m7AF1650B3BEC69788C368FAC0BFFAEAFF075E13B_gshared (String_t* ___key0, int32_t ___index1, int32_t ___count2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = (int32_t)((int32_t)352654597);
		int32_t L_0 = ___index1;
		int32_t L_1 = ___count2;
		V_1 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)L_1));
		int32_t L_2 = ___index1;
		V_2 = (int32_t)L_2;
		goto IL_0020;
	}

IL_000e:
	{
		int32_t L_3 = V_0;
		int32_t L_4 = V_0;
		String_t* L_5 = ___key0;
		int32_t L_6 = V_2;
		NullCheck((String_t*)L_5);
		Il2CppChar L_7 = String_get_Chars_m9B1A5E4C8D70AA33A60F03735AF7B77AB9DBBA70((String_t*)L_5, (int32_t)L_6, /*hidden argument*/NULL);
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_3, (int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_4<<(int32_t)7))^(int32_t)L_7))));
		int32_t L_8 = V_2;
		V_2 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0020:
	{
		int32_t L_9 = V_2;
		int32_t L_10 = V_1;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_000e;
		}
	}
	{
		int32_t L_11 = V_0;
		int32_t L_12 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_11, (int32_t)((int32_t)((int32_t)L_12>>(int32_t)((int32_t)17)))));
		int32_t L_13 = V_0;
		int32_t L_14 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_13, (int32_t)((int32_t)((int32_t)L_14>>(int32_t)((int32_t)11)))));
		int32_t L_15 = V_0;
		int32_t L_16 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_15, (int32_t)((int32_t)((int32_t)L_16>>(int32_t)5))));
		int32_t L_17 = V_0;
		return ((int32_t)((int32_t)L_17&(int32_t)((int32_t)2147483647LL)));
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Xml.Linq.XHashtable`1<System.Object>::.ctor(System.Xml.Linq.XHashtable`1_ExtractKeyDelegate<TValue>,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void XHashtable_1__ctor_mB4BD2615B8F54F8589333762FAF1C4A8807725FF_gshared (XHashtable_1_tDCC8A8B3022A89A8902F5A8091A444BD7922AB37 * __this, ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 * ___extractKey0, int32_t ___capacity1, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405((RuntimeObject *)__this, /*hidden argument*/NULL);
		ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 * L_0 = ___extractKey0;
		int32_t L_1 = ___capacity1;
		XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * L_2 = (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *, ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_2, (ExtractKeyDelegate_t9679484F2DC398593CBBE5E1C03BB37C3D304A99 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		__this->set_state_0(L_2);
		return;
	}
}
// System.Boolean System.Xml.Linq.XHashtable`1<System.Object>::TryGetValue(System.String,System.Int32,System.Int32,TValue&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool XHashtable_1_TryGetValue_m7A823EC488B0CF6F3FFE7A1DD5D82997343784F0_gshared (XHashtable_1_tDCC8A8B3022A89A8902F5A8091A444BD7922AB37 * __this, String_t* ___key0, int32_t ___index1, int32_t ___count2, RuntimeObject ** ___value3, const RuntimeMethod* method)
{
	{
		XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * L_0 = (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)__this->get_state_0();
		String_t* L_1 = ___key0;
		int32_t L_2 = ___index1;
		int32_t L_3 = ___count2;
		RuntimeObject ** L_4 = ___value3;
		NullCheck((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)L_0);
		bool L_5 = ((  bool (*) (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *, String_t*, int32_t, int32_t, RuntimeObject **, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)L_0, (String_t*)L_1, (int32_t)L_2, (int32_t)L_3, (RuntimeObject **)(RuntimeObject **)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		return L_5;
	}
}
// TValue System.Xml.Linq.XHashtable`1<System.Object>::Add(TValue)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * XHashtable_1_Add_m675BFA5CB9B9AFF50836CB517F1C4C7E054629E1_gshared (XHashtable_1_tDCC8A8B3022A89A8902F5A8091A444BD7922AB37 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	RuntimeObject * V_0 = NULL;
	XHashtable_1_tDCC8A8B3022A89A8902F5A8091A444BD7922AB37 * V_1 = NULL;
	bool V_2 = false;
	XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * V_3 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	{
		XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * L_0 = (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)__this->get_state_0();
		RuntimeObject * L_1 = ___value0;
		NullCheck((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)L_0);
		bool L_2 = ((  bool (*) (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *, RuntimeObject *, RuntimeObject **, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)L_0, (RuntimeObject *)L_1, (RuntimeObject **)(RuntimeObject **)(&V_0), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		if (!L_2)
		{
			goto IL_0012;
		}
	}
	{
		RuntimeObject * L_3 = V_0;
		return L_3;
	}

IL_0012:
	{
		V_1 = (XHashtable_1_tDCC8A8B3022A89A8902F5A8091A444BD7922AB37 *)__this;
		V_2 = (bool)0;
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		XHashtable_1_tDCC8A8B3022A89A8902F5A8091A444BD7922AB37 * L_4 = V_1;
		Monitor_Enter_mBEB6CC84184B46F26375EC3FC8921D16E48EA4C4((RuntimeObject *)L_4, (bool*)(bool*)(&V_2), /*hidden argument*/NULL);
		XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * L_5 = (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)__this->get_state_0();
		NullCheck((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)L_5);
		XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * L_6 = ((  XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * (*) (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		V_3 = (XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE *)L_6;
		Thread_MemoryBarrier_m9E2B68F7889D5D3AD76126930EE120D51C1B3402(/*hidden argument*/NULL);
		XHashtableState_t62E24AC7DF88CDB79D00D2633A0BE0DFC8C554FE * L_7 = V_3;
		__this->set_state_0(L_7);
		IL2CPP_LEAVE(0x0, FINALLY_0038);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0038;
	}

FINALLY_0038:
	{ // begin finally (depth: 1)
		{
			bool L_8 = V_2;
			if (!L_8)
			{
				goto IL_0041;
			}
		}

IL_003b:
		{
			XHashtable_1_tDCC8A8B3022A89A8902F5A8091A444BD7922AB37 * L_9 = V_1;
			Monitor_Exit_mA776B403DA88AC77CDEEF67AB9F0D0E77ABD254A((RuntimeObject *)L_9, /*hidden argument*/NULL);
		}

IL_0041:
		{
			IL2CPP_END_FINALLY(56)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(56)
	{
		IL2CPP_JUMP_TBL(0x0, IL_0000)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}
	{
		return NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Mono.CSharp.YieldStatement`1<System.Object>::.ctor(Mono.CSharp.Expression,Mono.CSharp.Location)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void YieldStatement_1__ctor_m2C62D7D174A893ECC2BEEAE04CFA97ACC00525A8_gshared (YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 * __this, Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * ___expr0, Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2  ___l1, const RuntimeMethod* method)
{
	{
		NullCheck((ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC *)__this);
		ResumableStatement__ctor_mAEB3654FD6AC1AB4004985FC092B1E19A32A4FA5((ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC *)__this, /*hidden argument*/NULL);
		Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * L_0 = ___expr0;
		__this->set_expr_4(L_0);
		Location_t3AD35D334EBE4B3F73BE5B439F6A08C1B25070B2  L_1 = ___l1;
		((Statement_t412FB2CAA6DCE73C11AD18A3F3E56815675E7D79 *)__this)->set_loc_0(L_1);
		return;
	}
}
// Mono.CSharp.Expression Mono.CSharp.YieldStatement`1<System.Object>::get_Expr()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * YieldStatement_1_get_Expr_mFE35E4CAD64B579CD3FAADD13600FA8081DE929E_gshared (YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 * __this, const RuntimeMethod* method)
{
	{
		Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * L_0 = (Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)__this->get_expr_4();
		return L_0;
	}
}
// System.Void Mono.CSharp.YieldStatement`1<System.Object>::CloneTo(Mono.CSharp.CloneContext,Mono.CSharp.Statement)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void YieldStatement_1_CloneTo_mB2C136ABFB1B76AE95AA8899D2B3B8C757F23719_gshared (YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 * __this, CloneContext_t05693E643AAC9CB5A95947DEC8B8CA5EF1F49B2A * ___clonectx0, Statement_t412FB2CAA6DCE73C11AD18A3F3E56815675E7D79 * ___t1, const RuntimeMethod* method)
{
	{
		Statement_t412FB2CAA6DCE73C11AD18A3F3E56815675E7D79 * L_0 = ___t1;
		Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * L_1 = (Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)__this->get_expr_4();
		CloneContext_t05693E643AAC9CB5A95947DEC8B8CA5EF1F49B2A * L_2 = ___clonectx0;
		NullCheck((Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)L_1);
		Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * L_3 = VirtFuncInvoker1< Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *, CloneContext_t05693E643AAC9CB5A95947DEC8B8CA5EF1F49B2A * >::Invoke(31 /* Mono.CSharp.Expression Mono.CSharp.Expression::Clone(Mono.CSharp.CloneContext) */, (Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)L_1, (CloneContext_t05693E643AAC9CB5A95947DEC8B8CA5EF1F49B2A *)L_2);
		NullCheck(((YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))));
		((YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 *)Castclass((RuntimeObject*)L_0, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0)))->set_expr_4(L_3);
		return;
	}
}
// System.Void Mono.CSharp.YieldStatement`1<System.Object>::DoEmit(Mono.CSharp.EmitContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void YieldStatement_1_DoEmit_mE083045EA61639BFCA5E723AF3F6141D95AF1AFA_gshared (YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 * __this, EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B * ___ec0, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_machine_initializer_6();
		EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B * L_1 = ___ec0;
		Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * L_2 = (Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)__this->get_expr_4();
		int32_t L_3 = (int32_t)__this->get_resume_pc_7();
		bool L_4 = (bool)__this->get_unwind_protect_5();
		Label_t15CF7D602807306CF6D6340B09720178CE278550  L_5 = (Label_t15CF7D602807306CF6D6340B09720178CE278550 )((ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC *)__this)->get_resume_point_3();
		NullCheck((StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48 *)L_0);
		VirtActionInvoker5< EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B *, Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *, int32_t, bool, Label_t15CF7D602807306CF6D6340B09720178CE278550  >::Invoke(40 /* System.Void Mono.CSharp.StateMachineInitializer::InjectYield(Mono.CSharp.EmitContext,Mono.CSharp.Expression,System.Int32,System.Boolean,System.Reflection.Emit.Label) */, (StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48 *)L_0, (EmitContext_tB8DB1B9B44A1A66237AEA98EDD12D7E9296A097B *)L_1, (Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)L_2, (int32_t)L_3, (bool)L_4, (Label_t15CF7D602807306CF6D6340B09720178CE278550 )L_5);
		return;
	}
}
// System.Boolean Mono.CSharp.YieldStatement`1<System.Object>::DoFlowAnalysis(Mono.CSharp.FlowAnalysisContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool YieldStatement_1_DoFlowAnalysis_mD58E20E82D4A2D7E3293EE4C14B347B835C385C6_gshared (YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 * __this, FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3 * ___fc0, const RuntimeMethod* method)
{
	{
		Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * L_0 = (Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)__this->get_expr_4();
		FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3 * L_1 = ___fc0;
		NullCheck((Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)L_0);
		VirtActionInvoker1< FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3 * >::Invoke(26 /* System.Void Mono.CSharp.Expression::FlowAnalysis(Mono.CSharp.FlowAnalysisContext) */, (Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)L_0, (FlowAnalysisContext_tE7A0415BDADB1544A561851779F688FC677CA8A3 *)L_1);
		NullCheck((YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 *)__this);
		((  void (*) (YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		return (bool)0;
	}
}
// System.Boolean Mono.CSharp.YieldStatement`1<System.Object>::Resolve(Mono.CSharp.BlockContext)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool YieldStatement_1_Resolve_m0D713E0D24341494688C84520826467505B31B60_gshared (YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 * __this, BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161 * ___bc0, const RuntimeMethod* method)
{
	{
		Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * L_0 = (Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)__this->get_expr_4();
		BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161 * L_1 = ___bc0;
		NullCheck((Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)L_0);
		Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * L_2 = Expression_Resolve_mE9C7188F0D2FD3E2CD01051E851CC3B52A7CC12F((Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)L_0, (ResolveContext_t40849BA9E46B26BF7120B15AEA68FFD5E185A7EB *)L_1, /*hidden argument*/NULL);
		__this->set_expr_4(L_2);
		Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 * L_3 = (Expression_t60DD7A67F08B9A21D6B5781C0B21DD9E78531C31 *)__this->get_expr_4();
		if (L_3)
		{
			goto IL_001c;
		}
	}
	{
		return (bool)0;
	}

IL_001c:
	{
		BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161 * L_4 = ___bc0;
		NullCheck(L_4);
		AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6 * L_5 = (AnonymousExpression_t668D33E15351168838690ECFA1D97C45709FB1A6 *)((ResolveContext_t40849BA9E46B26BF7120B15AEA68FFD5E185A7EB *)L_4)->get_CurrentAnonymousMethod_1();
		__this->set_machine_initializer_6(((RuntimeObject *)Castclass((RuntimeObject*)((RuntimeObject *)IsInst((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))));
		BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161 * L_6 = ___bc0;
		NullCheck((BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161 *)L_6);
		ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * L_7 = BlockContext_get_CurrentTryBlock_m599BADFEE226F520C77FC87162728E15ABF16FAB_inline((BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161 *)L_6, /*hidden argument*/NULL);
		__this->set_inside_try_block_8(L_7);
		return (bool)1;
	}
}
// System.Void Mono.CSharp.YieldStatement`1<System.Object>::RegisterResumePoint()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void YieldStatement_1_RegisterResumePoint_mDECE04223A4329AD498E1F891BDE2B8D06FEA9A4_gshared (YieldStatement_1_tF55C53E341B6FD4E6517DDA00C5D42A2364971E5 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_resume_pc_7();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * L_1 = (ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 *)__this->get_inside_try_block_8();
		if (L_1)
		{
			goto IL_0029;
		}
	}
	{
		RuntimeObject * L_2 = (RuntimeObject *)__this->get_machine_initializer_6();
		NullCheck((StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48 *)L_2);
		int32_t L_3 = StateMachineInitializer_AddResumePoint_m63C7CDA60F3BF7F1989F3A3740BEFDBE58EE52F6((StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48 *)L_2, (ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC *)__this, /*hidden argument*/NULL);
		__this->set_resume_pc_7(L_3);
		return;
	}

IL_0029:
	{
		ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * L_4 = (ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 *)__this->get_inside_try_block_8();
		int32_t L_5 = (int32_t)__this->get_resume_pc_7();
		RuntimeObject * L_6 = (RuntimeObject *)__this->get_machine_initializer_6();
		NullCheck((ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 *)L_4);
		int32_t L_7 = VirtFuncInvoker3< int32_t, ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC *, int32_t, StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48 * >::Invoke(15 /* System.Int32 Mono.CSharp.ExceptionStatement::AddResumePoint(Mono.CSharp.ResumableStatement,System.Int32,Mono.CSharp.StateMachineInitializer) */, (ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 *)L_4, (ResumableStatement_tE33A50DE444D51C291FC4A8F74EFED50CAB45FEC *)__this, (int32_t)L_5, (StateMachineInitializer_tA35E139BAC55B3F6F9499D4E3B6E4071F5A47C48 *)L_6);
		__this->set_resume_pc_7(L_7);
		__this->set_unwind_protect_5((bool)1);
		__this->set_inside_try_block_8((ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 *)NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Type FullSerializer.fsDirectConverter`1<DaggerfallConnect.DFBlock_RmbFldGroundData>::get_ModelType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * fsDirectConverter_1_get_ModelType_mB29327C4E1AD39E1409ACA4775B7871027298CBC_gshared (fsDirectConverter_1_tB5CD42BC188F3390971F3B024C91AA214F6979BB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (fsDirectConverter_1_get_ModelType_mB29327C4E1AD39E1409ACA4775B7871027298CBC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->klass->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_0, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Type_t * L_2 = V_0;
		return L_2;
	}
}
// FullSerializer.fsResult FullSerializer.fsDirectConverter`1<DaggerfallConnect.DFBlock_RmbFldGroundData>::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  fsDirectConverter_1_TrySerialize_mAB61D2E0C70DC296480D4DB72C12882A06F89443_gshared (fsDirectConverter_1_tB5CD42BC188F3390971F3B024C91AA214F6979BB * __this, RuntimeObject * ___instance0, fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 ** ___serialized1, Type_t * ___storageType2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (fsDirectConverter_1_TrySerialize_mAB61D2E0C70DC296480D4DB72C12882A06F89443_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * V_0 = NULL;
	fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  V_1;
	memset((&V_1), 0, sizeof(V_1));
	fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * L_0 = (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *)il2cpp_codegen_object_new(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mB6E331DE1E5FCDA60F4997990B21B6BDA65AC4E6(L_0, /*hidden argument*/Dictionary_2__ctor_mB6E331DE1E5FCDA60F4997990B21B6BDA65AC4E6_RuntimeMethod_var);
		V_0 = (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *)L_0;
		RuntimeObject * L_1 = ___instance0;
		Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * L_2 = V_0;
		NullCheck((fsDirectConverter_1_tB5CD42BC188F3390971F3B024C91AA214F6979BB *)__this);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_3 = VirtFuncInvoker2< fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA , RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939 , Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * >::Invoke(10 /* FullSerializer.fsResult FullSerializer.fsDirectConverter`1<DaggerfallConnect.DFBlock/RmbFldGroundData>::DoSerialize(TModel,System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>) */, (fsDirectConverter_1_tB5CD42BC188F3390971F3B024C91AA214F6979BB *)__this, (RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939 )((*(RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939 *)((RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939 *)UnBox(L_1, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))))), (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *)L_2);
		V_1 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_3;
		fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 ** L_4 = ___serialized1;
		Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * L_5 = V_0;
		fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * L_6 = (fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 *)il2cpp_codegen_object_new(fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16_il2cpp_TypeInfo_var);
		fsData__ctor_mD629E2C45F8DFD34B8555B3F2725E84662101BF4(L_6, (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *)L_5, /*hidden argument*/NULL);
		*((RuntimeObject **)L_4) = (RuntimeObject *)L_6;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_4, (void*)(RuntimeObject *)L_6);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_7 = V_1;
		V_2 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_7;
		goto IL_0021;
	}

IL_0021:
	{
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_8 = V_2;
		return L_8;
	}
}
// FullSerializer.fsResult FullSerializer.fsDirectConverter`1<DaggerfallConnect.DFBlock_RmbFldGroundData>::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  fsDirectConverter_1_TryDeserialize_m97AC4DED02F467623B8104D7ABAF8728EE844B90_gshared (fsDirectConverter_1_tB5CD42BC188F3390971F3B024C91AA214F6979BB * __this, fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * ___data0, RuntimeObject ** ___instance1, Type_t * ___storageType2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (fsDirectConverter_1_TryDeserialize_m97AC4DED02F467623B8104D7ABAF8728EE844B90_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  V_0;
	memset((&V_0), 0, sizeof(V_0));
	RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939  V_1;
	memset((&V_1), 0, sizeof(V_1));
	bool V_2 = false;
	fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  V_3;
	memset((&V_3), 0, sizeof(V_3));
	fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		IL2CPP_RUNTIME_CLASS_INIT(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_il2cpp_TypeInfo_var);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_0 = ((fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_StaticFields*)il2cpp_codegen_static_fields_for(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_il2cpp_TypeInfo_var))->get_Success_3();
		V_0 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_0;
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_1 = V_0;
		fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * L_2 = ___data0;
		NullCheck((fsBaseConverter_tF4DC7258DCD56EA3FF50C3FD608C8F31631BF8CC *)__this);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_3 = fsBaseConverter_CheckType_mACFF822768709E985EA98A2C7AB8A2707C3049D9((fsBaseConverter_tF4DC7258DCD56EA3FF50C3FD608C8F31631BF8CC *)__this, (fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 *)L_2, (int32_t)1, /*hidden argument*/NULL);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_4 = fsResult_op_Addition_m4D6866C260A2A17083DF96D3BA88B172E279A8EA((fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_1, (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_3, /*hidden argument*/NULL);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_5 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_4;
		V_0 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_5;
		V_3 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_5;
		bool L_6 = fsResult_get_Failed_m9C792FBD2658DA77759BE224A949470679F99074((fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA *)(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA *)(&V_3), /*hidden argument*/NULL);
		V_2 = (bool)L_6;
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0028;
		}
	}
	{
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_8 = V_0;
		V_4 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_8;
		goto IL_0052;
	}

IL_0028:
	{
		RuntimeObject ** L_9 = ___instance1;
		RuntimeObject * L_10 = *((RuntimeObject **)L_9);
		V_1 = (RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939 )((*(RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939 *)((RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939 *)UnBox(L_10, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1)))));
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_11 = V_0;
		fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * L_12 = ___data0;
		NullCheck((fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 *)L_12);
		Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * L_13 = fsData_get_AsDictionary_mDF9877487F2278358C4B94556633D11843715CA1((fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 *)L_12, /*hidden argument*/NULL);
		NullCheck((fsDirectConverter_1_tB5CD42BC188F3390971F3B024C91AA214F6979BB *)__this);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_14 = VirtFuncInvoker2< fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA , Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *, RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939 * >::Invoke(11 /* FullSerializer.fsResult FullSerializer.fsDirectConverter`1<DaggerfallConnect.DFBlock/RmbFldGroundData>::DoDeserialize(System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>,TModel&) */, (fsDirectConverter_1_tB5CD42BC188F3390971F3B024C91AA214F6979BB *)__this, (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *)L_13, (RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939 *)(RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939 *)(&V_1));
		IL2CPP_RUNTIME_CLASS_INIT(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_il2cpp_TypeInfo_var);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_15 = fsResult_op_Addition_m4D6866C260A2A17083DF96D3BA88B172E279A8EA((fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_11, (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_14, /*hidden argument*/NULL);
		V_0 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_15;
		RuntimeObject ** L_16 = ___instance1;
		RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939  L_17 = V_1;
		RmbFldGroundData_t73B66DFF3D80A0CD642F3C98B0B5CCA45EE09939  L_18 = L_17;
		RuntimeObject * L_19 = Box(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1), &L_18);
		*((RuntimeObject **)L_16) = (RuntimeObject *)L_19;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_16, (void*)(RuntimeObject *)L_19);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_20 = V_0;
		V_4 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_20;
		goto IL_0052;
	}

IL_0052:
	{
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_21 = V_4;
		return L_21;
	}
}
// System.Void FullSerializer.fsDirectConverter`1<DaggerfallConnect.DFBlock_RmbFldGroundData>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void fsDirectConverter_1__ctor_mEC8293A3B20593C6C3A35A4FCF33B8C3AEE288DF_gshared (fsDirectConverter_1_tB5CD42BC188F3390971F3B024C91AA214F6979BB * __this, const RuntimeMethod* method)
{
	{
		NullCheck((fsDirectConverter_t41638BCC8EA7FFAC4B7A9C0797FC3E57D1ECF024 *)__this);
		fsDirectConverter__ctor_m97790897C194D24C858E342216E248590112F770((fsDirectConverter_t41638BCC8EA7FFAC4B7A9C0797FC3E57D1ECF024 *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Type FullSerializer.fsDirectConverter`1<System.Object>::get_ModelType()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * fsDirectConverter_1_get_ModelType_m45E6E9FCD79B3081F985100AC97EBA45259E125B_gshared (fsDirectConverter_1_t4BF52AD38E426E1318FC9B7EA41EE9AE7D096B81 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (fsDirectConverter_1_get_ModelType_m45E6E9FCD79B3081F985100AC97EBA45259E125B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Type_t * V_0 = NULL;
	{
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (IL2CPP_RGCTX_TYPE(method->klass->rgctx_data, 0)) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E((RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 )L_0, /*hidden argument*/NULL);
		V_0 = (Type_t *)L_1;
		goto IL_000e;
	}

IL_000e:
	{
		Type_t * L_2 = V_0;
		return L_2;
	}
}
// FullSerializer.fsResult FullSerializer.fsDirectConverter`1<System.Object>::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  fsDirectConverter_1_TrySerialize_mEEC3432A88B41A12BFF8D2A628E1DFA97C688871_gshared (fsDirectConverter_1_t4BF52AD38E426E1318FC9B7EA41EE9AE7D096B81 * __this, RuntimeObject * ___instance0, fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 ** ___serialized1, Type_t * ___storageType2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (fsDirectConverter_1_TrySerialize_mEEC3432A88B41A12BFF8D2A628E1DFA97C688871_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * V_0 = NULL;
	fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  V_1;
	memset((&V_1), 0, sizeof(V_1));
	fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * L_0 = (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *)il2cpp_codegen_object_new(Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_mB6E331DE1E5FCDA60F4997990B21B6BDA65AC4E6(L_0, /*hidden argument*/Dictionary_2__ctor_mB6E331DE1E5FCDA60F4997990B21B6BDA65AC4E6_RuntimeMethod_var);
		V_0 = (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *)L_0;
		RuntimeObject * L_1 = ___instance0;
		Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * L_2 = V_0;
		NullCheck((fsDirectConverter_1_t4BF52AD38E426E1318FC9B7EA41EE9AE7D096B81 *)__this);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_3 = VirtFuncInvoker2< fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA , RuntimeObject *, Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * >::Invoke(10 /* FullSerializer.fsResult FullSerializer.fsDirectConverter`1<System.Object>::DoSerialize(TModel,System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>) */, (fsDirectConverter_1_t4BF52AD38E426E1318FC9B7EA41EE9AE7D096B81 *)__this, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_1, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *)L_2);
		V_1 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_3;
		fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 ** L_4 = ___serialized1;
		Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * L_5 = V_0;
		fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * L_6 = (fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 *)il2cpp_codegen_object_new(fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16_il2cpp_TypeInfo_var);
		fsData__ctor_mD629E2C45F8DFD34B8555B3F2725E84662101BF4(L_6, (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *)L_5, /*hidden argument*/NULL);
		*((RuntimeObject **)L_4) = (RuntimeObject *)L_6;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_4, (void*)(RuntimeObject *)L_6);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_7 = V_1;
		V_2 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_7;
		goto IL_0021;
	}

IL_0021:
	{
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_8 = V_2;
		return L_8;
	}
}
// FullSerializer.fsResult FullSerializer.fsDirectConverter`1<System.Object>::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  fsDirectConverter_1_TryDeserialize_m71932F844FD53E71380641D6E13BB6AC92BD9B08_gshared (fsDirectConverter_1_t4BF52AD38E426E1318FC9B7EA41EE9AE7D096B81 * __this, fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * ___data0, RuntimeObject ** ___instance1, Type_t * ___storageType2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (fsDirectConverter_1_TryDeserialize_m71932F844FD53E71380641D6E13BB6AC92BD9B08_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  V_0;
	memset((&V_0), 0, sizeof(V_0));
	RuntimeObject * V_1 = NULL;
	bool V_2 = false;
	fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  V_3;
	memset((&V_3), 0, sizeof(V_3));
	fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  V_4;
	memset((&V_4), 0, sizeof(V_4));
	{
		IL2CPP_RUNTIME_CLASS_INIT(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_il2cpp_TypeInfo_var);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_0 = ((fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_StaticFields*)il2cpp_codegen_static_fields_for(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_il2cpp_TypeInfo_var))->get_Success_3();
		V_0 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_0;
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_1 = V_0;
		fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * L_2 = ___data0;
		NullCheck((fsBaseConverter_tF4DC7258DCD56EA3FF50C3FD608C8F31631BF8CC *)__this);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_3 = fsBaseConverter_CheckType_mACFF822768709E985EA98A2C7AB8A2707C3049D9((fsBaseConverter_tF4DC7258DCD56EA3FF50C3FD608C8F31631BF8CC *)__this, (fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 *)L_2, (int32_t)1, /*hidden argument*/NULL);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_4 = fsResult_op_Addition_m4D6866C260A2A17083DF96D3BA88B172E279A8EA((fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_1, (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_3, /*hidden argument*/NULL);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_5 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_4;
		V_0 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_5;
		V_3 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_5;
		bool L_6 = fsResult_get_Failed_m9C792FBD2658DA77759BE224A949470679F99074((fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA *)(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA *)(&V_3), /*hidden argument*/NULL);
		V_2 = (bool)L_6;
		bool L_7 = V_2;
		if (!L_7)
		{
			goto IL_0028;
		}
	}
	{
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_8 = V_0;
		V_4 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_8;
		goto IL_0052;
	}

IL_0028:
	{
		RuntimeObject ** L_9 = ___instance1;
		RuntimeObject * L_10 = *((RuntimeObject **)L_9);
		V_1 = (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_10, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1)));
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_11 = V_0;
		fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 * L_12 = ___data0;
		NullCheck((fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 *)L_12);
		Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 * L_13 = fsData_get_AsDictionary_mDF9877487F2278358C4B94556633D11843715CA1((fsData_tB08E0516CBB6440EA15A5E77FB11C82BAF3DAA16 *)L_12, /*hidden argument*/NULL);
		NullCheck((fsDirectConverter_1_t4BF52AD38E426E1318FC9B7EA41EE9AE7D096B81 *)__this);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_14 = VirtFuncInvoker2< fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA , Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *, RuntimeObject ** >::Invoke(11 /* FullSerializer.fsResult FullSerializer.fsDirectConverter`1<System.Object>::DoDeserialize(System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>,TModel&) */, (fsDirectConverter_1_t4BF52AD38E426E1318FC9B7EA41EE9AE7D096B81 *)__this, (Dictionary_2_t6ECC7736C343B934B6095D1384FC92A44FFC0732 *)L_13, (RuntimeObject **)(RuntimeObject **)(&V_1));
		IL2CPP_RUNTIME_CLASS_INIT(fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA_il2cpp_TypeInfo_var);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_15 = fsResult_op_Addition_m4D6866C260A2A17083DF96D3BA88B172E279A8EA((fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_11, (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_14, /*hidden argument*/NULL);
		V_0 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_15;
		RuntimeObject ** L_16 = ___instance1;
		RuntimeObject * L_17 = V_1;
		*((RuntimeObject **)L_16) = (RuntimeObject *)L_17;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_16, (void*)(RuntimeObject *)L_17);
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_18 = V_0;
		V_4 = (fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA )L_18;
		goto IL_0052;
	}

IL_0052:
	{
		fsResult_t7F63FB9FF5D7240CCC485CFA59E54F18E3EDE1CA  L_19 = V_4;
		return L_19;
	}
}
// System.Void FullSerializer.fsDirectConverter`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void fsDirectConverter_1__ctor_mC538C25E7E5E1DD2F4127D363CBA9EEAFCA1414D_gshared (fsDirectConverter_1_t4BF52AD38E426E1318FC9B7EA41EE9AE7D096B81 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((fsDirectConverter_t41638BCC8EA7FFAC4B7A9C0797FC3E57D1ECF024 *)__this);
		fsDirectConverter__ctor_m97790897C194D24C858E342216E248590112F770((fsDirectConverter_t41638BCC8EA7FFAC4B7A9C0797FC3E57D1ECF024 *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::get_HasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool fsOption_1_get_HasValue_m00C0772621EA752A0EE2ED47E4FB3AE3CA4076FF_gshared (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = (bool)__this->get__hasValue_0();
		V_0 = (bool)L_0;
		goto IL_000a;
	}

IL_000a:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool fsOption_1_get_HasValue_m00C0772621EA752A0EE2ED47E4FB3AE3CA4076FF_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * _thisAdjusted = reinterpret_cast<fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 *>(__this + _offset);
	return fsOption_1_get_HasValue_m00C0772621EA752A0EE2ED47E4FB3AE3CA4076FF(_thisAdjusted, method);
}
// System.Boolean FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::get_IsEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool fsOption_1_get_IsEmpty_m79159C98271291CF3C76D956B12B69736A4D7FBB_gshared (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = (bool)__this->get__hasValue_0();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool fsOption_1_get_IsEmpty_m79159C98271291CF3C76D956B12B69736A4D7FBB_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * _thisAdjusted = reinterpret_cast<fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 *>(__this + _offset);
	return fsOption_1_get_IsEmpty_m79159C98271291CF3C76D956B12B69736A4D7FBB(_thisAdjusted, method);
}
// T FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  fsOption_1_get_Value_m61B93A2F2EDD9AB4AB9A94DE75103A39EC833F66_gshared (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (fsOption_1_get_Value_m61B93A2F2EDD9AB4AB9A94DE75103A39EC833F66_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		bool L_0 = fsOption_1_get_IsEmpty_m79159C98271291CF3C76D956B12B69736A4D7FBB((fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 *)(fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		V_0 = (bool)L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_2 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_2, (String_t*)_stringLiteral0A7D3B18DF4F9238A50A156FF06A5A7E794C1C7F, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, fsOption_1_get_Value_m61B93A2F2EDD9AB4AB9A94DE75103A39EC833F66_RuntimeMethod_var);
	}

IL_0016:
	{
		fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  L_3 = (fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23 )__this->get__value_1();
		V_1 = (fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23 )L_3;
		goto IL_001f;
	}

IL_001f:
	{
		fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  L_4 = V_1;
		return L_4;
	}
}
IL2CPP_EXTERN_C  fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  fsOption_1_get_Value_m61B93A2F2EDD9AB4AB9A94DE75103A39EC833F66_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * _thisAdjusted = reinterpret_cast<fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 *>(__this + _offset);
	return fsOption_1_get_Value_m61B93A2F2EDD9AB4AB9A94DE75103A39EC833F66(_thisAdjusted, method);
}
// System.Void FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType>::.ctor(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void fsOption_1__ctor_mB59DF4B8063F319B6D2423AD6ED5BB982FDDDE13_gshared (fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * __this, fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  ___value0, const RuntimeMethod* method)
{
	{
		__this->set__hasValue_0((bool)1);
		fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  L_0 = ___value0;
		__this->set__value_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void fsOption_1__ctor_mB59DF4B8063F319B6D2423AD6ED5BB982FDDDE13_AdjustorThunk (RuntimeObject * __this, fsVersionedType_t393061A2450EE3A33C5C8BE6F988FE902569AD23  ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 * _thisAdjusted = reinterpret_cast<fsOption_1_t844F3720AC5A1DCA3D512B9D964389A7BDBA87F5 *>(__this + _offset);
	fsOption_1__ctor_mB59DF4B8063F319B6D2423AD6ED5BB982FDDDE13(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean FullSerializer.Internal.fsOption`1<System.Object>::get_HasValue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool fsOption_1_get_HasValue_mCC291617445169FE15CD25E16F6153F2ABEF7421_gshared (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = (bool)__this->get__hasValue_0();
		V_0 = (bool)L_0;
		goto IL_000a;
	}

IL_000a:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool fsOption_1_get_HasValue_mCC291617445169FE15CD25E16F6153F2ABEF7421_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * _thisAdjusted = reinterpret_cast<fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 *>(__this + _offset);
	return fsOption_1_get_HasValue_mCC291617445169FE15CD25E16F6153F2ABEF7421(_thisAdjusted, method);
}
// System.Boolean FullSerializer.Internal.fsOption`1<System.Object>::get_IsEmpty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool fsOption_1_get_IsEmpty_mD138FFBFB08368962CC8D7FD6BE3C9A2753FF92E_gshared (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		bool L_0 = (bool)__this->get__hasValue_0();
		V_0 = (bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0);
		goto IL_000d;
	}

IL_000d:
	{
		bool L_1 = V_0;
		return L_1;
	}
}
IL2CPP_EXTERN_C  bool fsOption_1_get_IsEmpty_mD138FFBFB08368962CC8D7FD6BE3C9A2753FF92E_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * _thisAdjusted = reinterpret_cast<fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 *>(__this + _offset);
	return fsOption_1_get_IsEmpty_mD138FFBFB08368962CC8D7FD6BE3C9A2753FF92E(_thisAdjusted, method);
}
// T FullSerializer.Internal.fsOption`1<System.Object>::get_Value()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * fsOption_1_get_Value_m3D1611CEBDB9440CC8285C9814801A410042CD4A_gshared (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (fsOption_1_get_Value_m3D1611CEBDB9440CC8285C9814801A410042CD4A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	RuntimeObject * V_1 = NULL;
	{
		bool L_0 = fsOption_1_get_IsEmpty_mD138FFBFB08368962CC8D7FD6BE3C9A2753FF92E((fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 *)(fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		V_0 = (bool)L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB * L_2 = (InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB *)il2cpp_codegen_object_new(InvalidOperationException_t10D3EE59AD28EC641ACEE05BCA4271A527E5ECAB_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_mC012CE552988309733C896F3FEA8249171E4402E(L_2, (String_t*)_stringLiteral0A7D3B18DF4F9238A50A156FF06A5A7E794C1C7F, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, fsOption_1_get_Value_m3D1611CEBDB9440CC8285C9814801A410042CD4A_RuntimeMethod_var);
	}

IL_0016:
	{
		RuntimeObject * L_3 = (RuntimeObject *)__this->get__value_1();
		V_1 = (RuntimeObject *)L_3;
		goto IL_001f;
	}

IL_001f:
	{
		RuntimeObject * L_4 = V_1;
		return L_4;
	}
}
IL2CPP_EXTERN_C  RuntimeObject * fsOption_1_get_Value_m3D1611CEBDB9440CC8285C9814801A410042CD4A_AdjustorThunk (RuntimeObject * __this, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * _thisAdjusted = reinterpret_cast<fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 *>(__this + _offset);
	return fsOption_1_get_Value_m3D1611CEBDB9440CC8285C9814801A410042CD4A(_thisAdjusted, method);
}
// System.Void FullSerializer.Internal.fsOption`1<System.Object>::.ctor(T)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void fsOption_1__ctor_m501783AB74D015496072C03602E16A08B949B870_gshared (fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	{
		__this->set__hasValue_0((bool)1);
		RuntimeObject * L_0 = ___value0;
		__this->set__value_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void fsOption_1__ctor_m501783AB74D015496072C03602E16A08B949B870_AdjustorThunk (RuntimeObject * __this, RuntimeObject * ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 * _thisAdjusted = reinterpret_cast<fsOption_1_t069934DF831182E3C700A8688AECCFA59F0C6FD5 *>(__this + _offset);
	fsOption_1__ctor_m501783AB74D015496072C03602E16A08B949B870(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t String_get_Length_m129FC0ADA02FECBED3C0B1A809AE84A5AEE1CF09_inline (String_t* __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_m_stringLength_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * BlockContext_get_CurrentTryBlock_m599BADFEE226F520C77FC87162728E15ABF16FAB_inline (BlockContext_tB638512B4057CA8E5323C5AFF8AA84456E38B161 * __this, const RuntimeMethod* method)
{
	{
		ExceptionStatement_t4194E79BE1DAFE1F6D09472FBE4EA1097E00E453 * L_0 = __this->get_U3CCurrentTryBlockU3Ek__BackingField_7();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Il2CppChar Enumerator_get_Current_mBEE48434132CD10DC7A6BD2C69B50B69206DF14C_gshared_inline (Enumerator_tA84BA090971D432C9648ACA7F1744157466B5A4F * __this, const RuntimeMethod* method)
{
	{
		Il2CppChar L_0 = (Il2CppChar)__this->get_current_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_m6BBD624C51F7E20D347FE5894A6ECA94B8011181_gshared_inline (Enumerator_t7BA00929E14A2F2A62CE085585044A3FEB2C5F3C * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Enumerator_get_Current_mDD503AFD786235D3B40842B0872AC17DC86EB040_gshared_inline (Enumerator_t3A873D53A96DA0AC1C030CB5A0BF2D0F57FCC984 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  Enumerator_get_Current_m57C04D97C5B60AAC24531BE8204334F187B1007D_gshared_inline (Enumerator_t009A71B4AED29005DA8CC3E2D87EE82EE2A9C7FD * __this, const RuntimeMethod* method)
{
	{
		ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D  L_0 = (ModDependency_t7219791B39BD87837E7E2D45B0A0FD62398A0D2D )__this->get_current_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m9C4EBBD2108B51885E750F927D7936290C8E20EE_gshared_inline (Enumerator_tB6009981BD4E3881E3EC83627255A24198F902D6 * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_current_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  Enumerator_get_Current_m1EB734EC70AB008E22FBDA4FCE5FF70CED4BC7D5_gshared_inline (Enumerator_tD7A9689A574F0003D92365490A48E5F35C66C03B * __this, const RuntimeMethod* method)
{
	{
		Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767  L_0 = (Resolution_t1906ED569E57B1BD0C7F7A8DBCEA1D584F5F1767 )__this->get_current_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR float Enumerator_get_Current_m1DC0B40110173B7E2D13319164F7657C3BE3536D_gshared_inline (Enumerator_tC1FD01C8BB5327D442D71FD022B4338ACD701783 * __this, const RuntimeMethod* method)
{
	{
		float L_0 = (float)__this->get_current_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  Enumerator_get_Current_m290640F097AF113C466EEBA5107E3850AB8FBA65_gshared_inline (Enumerator_tCB80A6245D6FAEBB56F66A5BDD75BD883EDF55F6 * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2  L_0 = (KeyValuePair_2_t8EB09BF4DD251CCCBB6F85C46B29153BF9822DA2 )__this->get_current_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  Enumerator_get_Current_mD3498F86E6123F926AA4E50358C9F6515C73EECC_gshared_inline (Enumerator_t249D8C36E885029E8F66BEF78189C8B180E6879D * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A  L_0 = (KeyValuePair_2_t90D1751B96C35ACA835CEA082EB7FADD19F3738A )__this->get_current_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  Enumerator_get_Current_m36A49F353C175C557E3490621F4489D7D95A646B_gshared_inline (Enumerator_tA73714A95511E4A3614246077E7C1FDAD5447D04 * __this, const RuntimeMethod* method)
{
	{
		KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625  L_0 = (KeyValuePair_2_tFB6A066C69E28C6ACA5FC5E24D969BFADC5FA625 )__this->get_current_3();
		return L_0;
	}
}
