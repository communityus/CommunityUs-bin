﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngineInternal.MathfInternal::.cctor()
extern void MathfInternal__cctor_mB7CF38BBE41ECBC62A4457C91FDFD0FEE8679895 (void);
// 0x00000002 System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(UnityEngineInternal.TypeInferenceRules)
extern void TypeInferenceRuleAttribute__ctor_m8B31AC5D44FB102217AB0308EE71EA00379B2ECF (void);
// 0x00000003 System.Void UnityEngineInternal.TypeInferenceRuleAttribute::.ctor(System.String)
extern void TypeInferenceRuleAttribute__ctor_mE01C01375335FB362405B8ADE483DB62E7DD7B8F (void);
// 0x00000004 System.String UnityEngineInternal.TypeInferenceRuleAttribute::ToString()
extern void TypeInferenceRuleAttribute_ToString_mD1488CF490AFA2A7F88481AD5766C6E6B865ABC4 (void);
// 0x00000005 System.Void UnityEngineInternal.GenericStack::.ctor()
extern void GenericStack__ctor_m42B668E8F293EE21F529A2679AA110C0877605DD (void);
// 0x00000006 System.Void Unity.Profiling.ProfilerMarker::.ctor(System.String)
extern void ProfilerMarker__ctor_mCE8D10CF2D2B2C4E51BF1BB66D75FDDE5BDA4A41_AdjustorThunk (void);
// 0x00000007 System.IntPtr Unity.Profiling.LowLevel.Unsafe.ProfilerUnsafeUtility::CreateMarker(System.String,System.UInt16,Unity.Profiling.LowLevel.MarkerFlags,System.Int32)
extern void ProfilerUnsafeUtility_CreateMarker_m419027084C68545B765B9345949D8BFCB96C51AD (void);
// 0x00000008 System.Void Unity.Jobs.IJob::Execute()
// 0x00000009 Unity.Jobs.JobHandle Unity.Jobs.IJobExtensions::Schedule(T,Unity.Jobs.JobHandle)
// 0x0000000A System.IntPtr Unity.Jobs.IJobExtensions_JobStruct`1::Initialize()
// 0x0000000B System.Void Unity.Jobs.IJobExtensions_JobStruct`1::Execute(T&,System.IntPtr,System.IntPtr,Unity.Jobs.LowLevel.Unsafe.JobRanges&,System.Int32)
// 0x0000000C System.Void Unity.Jobs.IJobExtensions_JobStruct`1_ExecuteJobFunction::.ctor(System.Object,System.IntPtr)
// 0x0000000D System.Void Unity.Jobs.IJobExtensions_JobStruct`1_ExecuteJobFunction::Invoke(T&,System.IntPtr,System.IntPtr,Unity.Jobs.LowLevel.Unsafe.JobRanges&,System.Int32)
// 0x0000000E System.IAsyncResult Unity.Jobs.IJobExtensions_JobStruct`1_ExecuteJobFunction::BeginInvoke(T&,System.IntPtr,System.IntPtr,Unity.Jobs.LowLevel.Unsafe.JobRanges&,System.Int32,System.AsyncCallback,System.Object)
// 0x0000000F System.Void Unity.Jobs.IJobExtensions_JobStruct`1_ExecuteJobFunction::EndInvoke(T&,Unity.Jobs.LowLevel.Unsafe.JobRanges&,System.IAsyncResult)
// 0x00000010 System.Void Unity.Jobs.IJobParallelFor::Execute(System.Int32)
// 0x00000011 Unity.Jobs.JobHandle Unity.Jobs.IJobParallelForExtensions::Schedule(T,System.Int32,System.Int32,Unity.Jobs.JobHandle)
// 0x00000012 System.IntPtr Unity.Jobs.IJobParallelForExtensions_ParallelForJobStruct`1::Initialize()
// 0x00000013 System.Void Unity.Jobs.IJobParallelForExtensions_ParallelForJobStruct`1::Execute(T&,System.IntPtr,System.IntPtr,Unity.Jobs.LowLevel.Unsafe.JobRanges&,System.Int32)
// 0x00000014 System.Void Unity.Jobs.IJobParallelForExtensions_ParallelForJobStruct`1_ExecuteJobFunction::.ctor(System.Object,System.IntPtr)
// 0x00000015 System.Void Unity.Jobs.IJobParallelForExtensions_ParallelForJobStruct`1_ExecuteJobFunction::Invoke(T&,System.IntPtr,System.IntPtr,Unity.Jobs.LowLevel.Unsafe.JobRanges&,System.Int32)
// 0x00000016 System.IAsyncResult Unity.Jobs.IJobParallelForExtensions_ParallelForJobStruct`1_ExecuteJobFunction::BeginInvoke(T&,System.IntPtr,System.IntPtr,Unity.Jobs.LowLevel.Unsafe.JobRanges&,System.Int32,System.AsyncCallback,System.Object)
// 0x00000017 System.Void Unity.Jobs.IJobParallelForExtensions_ParallelForJobStruct`1_ExecuteJobFunction::EndInvoke(T&,Unity.Jobs.LowLevel.Unsafe.JobRanges&,System.IAsyncResult)
// 0x00000018 System.Void Unity.Jobs.JobHandle::Complete()
extern void JobHandle_Complete_m947DF01E0F87C3B0A24AECEBF72D245A6CDBE148_AdjustorThunk (void);
// 0x00000019 System.Boolean Unity.Jobs.JobHandle::get_IsCompleted()
extern void JobHandle_get_IsCompleted_m2747303E2CD600A78AC8F3ED1EA40D3F507C75A2_AdjustorThunk (void);
// 0x0000001A System.Void Unity.Jobs.JobHandle::ScheduleBatchedJobs()
extern void JobHandle_ScheduleBatchedJobs_m31A19EE8C93D6BA7F2222001596EBEF313167916 (void);
// 0x0000001B System.Void Unity.Jobs.JobHandle::ScheduleBatchedJobsAndComplete(Unity.Jobs.JobHandle&)
extern void JobHandle_ScheduleBatchedJobsAndComplete_m9D762E10C5648909D15E56C8E099410300F691A0 (void);
// 0x0000001C System.Boolean Unity.Jobs.JobHandle::ScheduleBatchedJobsAndIsCompleted(Unity.Jobs.JobHandle&)
extern void JobHandle_ScheduleBatchedJobsAndIsCompleted_mCF40C24E56E7EFE034ED8588E9041F47A93BAEA6 (void);
// 0x0000001D System.Void Unity.Jobs.LowLevel.Unsafe.JobProducerTypeAttribute::.ctor(System.Type)
extern void JobProducerTypeAttribute__ctor_m6CF83F27400526E37ED6CEADFF330E49A310BA7C (void);
// 0x0000001E System.Boolean Unity.Jobs.LowLevel.Unsafe.JobsUtility::GetWorkStealingRange(Unity.Jobs.LowLevel.Unsafe.JobRanges&,System.Int32,System.Int32&,System.Int32&)
extern void JobsUtility_GetWorkStealingRange_m8E2276200A11FDF636F1C6092E786ACD0396435C (void);
// 0x0000001F Unity.Jobs.JobHandle Unity.Jobs.LowLevel.Unsafe.JobsUtility::Schedule(Unity.Jobs.LowLevel.Unsafe.JobsUtility_JobScheduleParameters&)
extern void JobsUtility_Schedule_mEE082ED2BA501FE2A656B0EEB4086A69A5C66D37 (void);
// 0x00000020 Unity.Jobs.JobHandle Unity.Jobs.LowLevel.Unsafe.JobsUtility::ScheduleParallelFor(Unity.Jobs.LowLevel.Unsafe.JobsUtility_JobScheduleParameters&,System.Int32,System.Int32)
extern void JobsUtility_ScheduleParallelFor_mB2883217C5BAE5ED43D89478B76849936EDDEE78 (void);
// 0x00000021 System.IntPtr Unity.Jobs.LowLevel.Unsafe.JobsUtility::CreateJobReflectionData(System.Type,System.Type,Unity.Jobs.LowLevel.Unsafe.JobType,System.Object,System.Object,System.Object)
extern void JobsUtility_CreateJobReflectionData_mCB06DEB035740C84B8729194E4941840C315D1C4 (void);
// 0x00000022 System.IntPtr Unity.Jobs.LowLevel.Unsafe.JobsUtility::CreateJobReflectionData(System.Type,Unity.Jobs.LowLevel.Unsafe.JobType,System.Object,System.Object,System.Object)
extern void JobsUtility_CreateJobReflectionData_m78D2E589323A7C22D6B577B4C6B08569147EAC05 (void);
// 0x00000023 System.Void Unity.Jobs.LowLevel.Unsafe.JobsUtility::Schedule_Injected(Unity.Jobs.LowLevel.Unsafe.JobsUtility_JobScheduleParameters&,Unity.Jobs.JobHandle&)
extern void JobsUtility_Schedule_Injected_mFDFBDD20D03C3D74C5D56206B6DC08E101ECF64C (void);
// 0x00000024 System.Void Unity.Jobs.LowLevel.Unsafe.JobsUtility::ScheduleParallelFor_Injected(Unity.Jobs.LowLevel.Unsafe.JobsUtility_JobScheduleParameters&,System.Int32,System.Int32,Unity.Jobs.JobHandle&)
extern void JobsUtility_ScheduleParallelFor_Injected_m0F7C3AB0415EF402D7242BE6FA82F2ACBC899360 (void);
// 0x00000025 System.Void Unity.Jobs.LowLevel.Unsafe.JobsUtility_JobScheduleParameters::.ctor(System.Void*,System.IntPtr,Unity.Jobs.JobHandle,Unity.Jobs.LowLevel.Unsafe.ScheduleMode)
extern void JobScheduleParameters__ctor_m7BAC662AF5FA4A96429644CC676D3179A5B143F8_AdjustorThunk (void);
// 0x00000026 System.Void Unity.Collections.ReadOnlyAttribute::.ctor()
extern void ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF (void);
// 0x00000027 System.Void Unity.Collections.WriteOnlyAttribute::.ctor()
extern void WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F (void);
// 0x00000028 System.Void Unity.Collections.NativeLeakDetection::Initialize()
extern void NativeLeakDetection_Initialize_m1A20F4DD5DD1EF32704F40F0B05B0DE021399936 (void);
// 0x00000029 System.Void Unity.Collections.NativeArray`1::.ctor(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArrayOptions)
// 0x0000002A System.Void Unity.Collections.NativeArray`1::.ctor(T[],Unity.Collections.Allocator)
// 0x0000002B System.Void Unity.Collections.NativeArray`1::Allocate(System.Int32,Unity.Collections.Allocator,Unity.Collections.NativeArray`1<T>&)
// 0x0000002C System.Int32 Unity.Collections.NativeArray`1::get_Length()
// 0x0000002D T Unity.Collections.NativeArray`1::get_Item(System.Int32)
// 0x0000002E System.Void Unity.Collections.NativeArray`1::set_Item(System.Int32,T)
// 0x0000002F System.Boolean Unity.Collections.NativeArray`1::get_IsCreated()
// 0x00000030 System.Void Unity.Collections.NativeArray`1::Dispose()
// 0x00000031 System.Void Unity.Collections.NativeArray`1::CopyTo(T[])
// 0x00000032 Unity.Collections.NativeArray`1_Enumerator<T> Unity.Collections.NativeArray`1::GetEnumerator()
// 0x00000033 System.Collections.Generic.IEnumerator`1<T> Unity.Collections.NativeArray`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000034 System.Collections.IEnumerator Unity.Collections.NativeArray`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000035 System.Boolean Unity.Collections.NativeArray`1::Equals(Unity.Collections.NativeArray`1<T>)
// 0x00000036 System.Boolean Unity.Collections.NativeArray`1::Equals(System.Object)
// 0x00000037 System.Int32 Unity.Collections.NativeArray`1::GetHashCode()
// 0x00000038 System.Void Unity.Collections.NativeArray`1::Copy(T[],Unity.Collections.NativeArray`1<T>)
// 0x00000039 System.Void Unity.Collections.NativeArray`1::Copy(Unity.Collections.NativeArray`1<T>,T[])
// 0x0000003A System.Void Unity.Collections.NativeArray`1::Copy(T[],System.Int32,Unity.Collections.NativeArray`1<T>,System.Int32,System.Int32)
// 0x0000003B System.Void Unity.Collections.NativeArray`1::Copy(Unity.Collections.NativeArray`1<T>,System.Int32,T[],System.Int32,System.Int32)
// 0x0000003C System.Void Unity.Collections.NativeArray`1_Enumerator::.ctor(Unity.Collections.NativeArray`1<T>&)
// 0x0000003D System.Void Unity.Collections.NativeArray`1_Enumerator::Dispose()
// 0x0000003E System.Boolean Unity.Collections.NativeArray`1_Enumerator::MoveNext()
// 0x0000003F System.Void Unity.Collections.NativeArray`1_Enumerator::Reset()
// 0x00000040 T Unity.Collections.NativeArray`1_Enumerator::get_Current()
// 0x00000041 System.Object Unity.Collections.NativeArray`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000042 System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerAttribute::.ctor()
extern void NativeContainerAttribute__ctor_m3863E2733AAF8A12491A6D448B14182C89864633 (void);
// 0x00000043 System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerSupportsMinMaxWriteRestrictionAttribute::.ctor()
extern void NativeContainerSupportsMinMaxWriteRestrictionAttribute__ctor_mBB573360E0CCDC3FEBD208460D9109485687F1C8 (void);
// 0x00000044 System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerSupportsDeallocateOnJobCompletionAttribute::.ctor()
extern void NativeContainerSupportsDeallocateOnJobCompletionAttribute__ctor_mFA8893231D6CAF247A130FA9A9950672757848E6 (void);
// 0x00000045 System.Void Unity.Collections.LowLevel.Unsafe.NativeContainerSupportsDeferredConvertListToArray::.ctor()
extern void NativeContainerSupportsDeferredConvertListToArray__ctor_m0C65F9A9FF8862741C56E758FB996C5708E2D975 (void);
// 0x00000046 System.Void Unity.Collections.LowLevel.Unsafe.WriteAccessRequiredAttribute::.ctor()
extern void WriteAccessRequiredAttribute__ctor_m832267CA67398C994C2B666B00253CD9959610B9 (void);
// 0x00000047 System.Void Unity.Collections.LowLevel.Unsafe.NativeDisableUnsafePtrRestrictionAttribute::.ctor()
extern void NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE (void);
// 0x00000048 Unity.Collections.NativeArray`1<T> Unity.Collections.LowLevel.Unsafe.NativeArrayUnsafeUtility::ConvertExistingDataToNativeArray(System.Void*,System.Int32,Unity.Collections.Allocator)
// 0x00000049 System.Void* Unity.Collections.LowLevel.Unsafe.UnsafeUtility::Malloc(System.Int64,System.Int32,Unity.Collections.Allocator)
extern void UnsafeUtility_Malloc_m18FCC67A056C48A4E0F939D08C43F9E876CA1CF6 (void);
// 0x0000004A System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::Free(System.Void*,Unity.Collections.Allocator)
extern void UnsafeUtility_Free_mA805168FF1B6728E7DF3AD1DE47400B37F3441F9 (void);
// 0x0000004B System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::MemCpy(System.Void*,System.Void*,System.Int64)
extern void UnsafeUtility_MemCpy_m8E335BAB1C2A8483AF8531CE8464C6A69BB98C1B (void);
// 0x0000004C System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::MemSet(System.Void*,System.Byte,System.Int64)
extern void UnsafeUtility_MemSet_m963D137A8DADF01067A68EFDEF736E7216411353 (void);
// 0x0000004D System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::MemClear(System.Void*,System.Int64)
extern void UnsafeUtility_MemClear_m9A2B75C85CB8B6637B1286A562A8E35C82772D09 (void);
// 0x0000004E System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::SizeOf(System.Type)
extern void UnsafeUtility_SizeOf_mE364EE2E10DCC0FF112874221FFD07C7C3E08676 (void);
// 0x0000004F System.Boolean Unity.Collections.LowLevel.Unsafe.UnsafeUtility::IsBlittable(System.Type)
extern void UnsafeUtility_IsBlittable_mC76E5617EC7CA98CC232702004D71915DDBF1D47 (void);
// 0x00000050 System.Boolean Unity.Collections.LowLevel.Unsafe.UnsafeUtility::IsBlittableValueType(System.Type)
extern void UnsafeUtility_IsBlittableValueType_mC548D28E3E1FF39148C8A2D745A30AB6122485B7 (void);
// 0x00000051 System.String Unity.Collections.LowLevel.Unsafe.UnsafeUtility::GetReasonForTypeNonBlittableImpl(System.Type,System.String)
extern void UnsafeUtility_GetReasonForTypeNonBlittableImpl_mCB0D7E72BB113F1628B2FEFDEB175FDC1A6ED5DF (void);
// 0x00000052 System.Boolean Unity.Collections.LowLevel.Unsafe.UnsafeUtility::IsArrayBlittable(System.Array)
extern void UnsafeUtility_IsArrayBlittable_m1F4CE4F21EF62AF1D4BC1F7A0C520745814BC959 (void);
// 0x00000053 System.String Unity.Collections.LowLevel.Unsafe.UnsafeUtility::GetReasonForArrayNonBlittable(System.Array)
extern void UnsafeUtility_GetReasonForArrayNonBlittable_m451743E99803EC89E4397135323169DF87D9F620 (void);
// 0x00000054 System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::AlignOf()
// 0x00000055 T Unity.Collections.LowLevel.Unsafe.UnsafeUtility::ReadArrayElement(System.Void*,System.Int32)
// 0x00000056 System.Void Unity.Collections.LowLevel.Unsafe.UnsafeUtility::WriteArrayElement(System.Void*,System.Int32,T)
// 0x00000057 System.Void* Unity.Collections.LowLevel.Unsafe.UnsafeUtility::AddressOf(T&)
// 0x00000058 System.Int32 Unity.Collections.LowLevel.Unsafe.UnsafeUtility::SizeOf()
// 0x00000059 System.Int32 UnityEngine.SortingLayer::GetLayerValueFromID(System.Int32)
extern void SortingLayer_GetLayerValueFromID_mA244A6AFF800BD8D27D9E402C01EC9B1D85421F3 (void);
// 0x0000005A System.Void UnityEngine.Keyframe::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Keyframe__ctor_m572CCEE06F612003F939F3FF439B15F89E8C1D54_AdjustorThunk (void);
// 0x0000005B System.Single UnityEngine.Keyframe::get_time()
extern void Keyframe_get_time_m75EBFDECA329315F6D41A60C0B1291F5BA4039E8_AdjustorThunk (void);
// 0x0000005C System.Void UnityEngine.Keyframe::set_time(System.Single)
extern void Keyframe_set_time_mB48C8B14B2346F46A0A4FE27CDD01D163F945CC4_AdjustorThunk (void);
// 0x0000005D System.Void UnityEngine.AnimationCurve::Internal_Destroy(System.IntPtr)
extern void AnimationCurve_Internal_Destroy_m29AC7F49DA0754B8C7A451FE946BD9A38B76E61F (void);
// 0x0000005E System.IntPtr UnityEngine.AnimationCurve::Internal_Create(UnityEngine.Keyframe[])
extern void AnimationCurve_Internal_Create_m876905D8C13846F935CA93C0C779368519D01D0C (void);
// 0x0000005F System.Boolean UnityEngine.AnimationCurve::Internal_Equals(System.IntPtr)
extern void AnimationCurve_Internal_Equals_m0D37631AC99BD190E2E753012C2F24A63DD78D05 (void);
// 0x00000060 System.Void UnityEngine.AnimationCurve::Finalize()
extern void AnimationCurve_Finalize_m4F8AF6E43E488439AB1022E7A97FEDE777655375 (void);
// 0x00000061 System.Single UnityEngine.AnimationCurve::Evaluate(System.Single)
extern void AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A (void);
// 0x00000062 UnityEngine.Keyframe[] UnityEngine.AnimationCurve::get_keys()
extern void AnimationCurve_get_keys_m64FA75C0B1F7BCDE123EFB903962B2BD9DD5F321 (void);
// 0x00000063 System.Void UnityEngine.AnimationCurve::set_keys(UnityEngine.Keyframe[])
extern void AnimationCurve_set_keys_m146468F0FF5228D829489E845C1193B1D83543BF (void);
// 0x00000064 System.Int32 UnityEngine.AnimationCurve::AddKey(UnityEngine.Keyframe)
extern void AnimationCurve_AddKey_m3AEE7259785540EF6A157BA99B3737AC60E30D9A (void);
// 0x00000065 System.Int32 UnityEngine.AnimationCurve::AddKey_Internal(UnityEngine.Keyframe)
extern void AnimationCurve_AddKey_Internal_mAC23772AFF804FE8E436AB21EE9522F698912DFD (void);
// 0x00000066 UnityEngine.Keyframe UnityEngine.AnimationCurve::get_Item(System.Int32)
extern void AnimationCurve_get_Item_mD5F6B4C79C432C9CEADB6D116C07969802D5611A (void);
// 0x00000067 System.Int32 UnityEngine.AnimationCurve::get_length()
extern void AnimationCurve_get_length_mB3D0734222EE55DB1389BCB98CCB1324AF8AA4E0 (void);
// 0x00000068 System.Void UnityEngine.AnimationCurve::SetKeys(UnityEngine.Keyframe[])
extern void AnimationCurve_SetKeys_mAD2913A53CA10840C9DA59A94210CD228499FAFE (void);
// 0x00000069 UnityEngine.Keyframe UnityEngine.AnimationCurve::GetKey(System.Int32)
extern void AnimationCurve_GetKey_m4B24EFBA502C3FE5F3E93BDF7ADAC10C44EB3168 (void);
// 0x0000006A UnityEngine.Keyframe[] UnityEngine.AnimationCurve::GetKeys()
extern void AnimationCurve_GetKeys_mC0CB4482DA2A68FC0B1359DC6D3FCAA0873A9941 (void);
// 0x0000006B System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern void AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0 (void);
// 0x0000006C System.Void UnityEngine.AnimationCurve::.ctor()
extern void AnimationCurve__ctor_m68D6F819242C539EC522FEAFFEB6F1579767043E (void);
// 0x0000006D System.Boolean UnityEngine.AnimationCurve::Equals(System.Object)
extern void AnimationCurve_Equals_mE1B90C79209D2E006B96751B48A2F0BA6F60A5B8 (void);
// 0x0000006E System.Boolean UnityEngine.AnimationCurve::Equals(UnityEngine.AnimationCurve)
extern void AnimationCurve_Equals_mFB50636B9BE34BBD83BE401186BC1EB7267C5416 (void);
// 0x0000006F System.Int32 UnityEngine.AnimationCurve::GetHashCode()
extern void AnimationCurve_GetHashCode_mCF18923053E945F39386CE8F1FD149D4020BC4DD (void);
// 0x00000070 System.Int32 UnityEngine.AnimationCurve::AddKey_Internal_Injected(UnityEngine.Keyframe&)
extern void AnimationCurve_AddKey_Internal_Injected_m3D52627A04B7A4D84F0DBE43E9EF2483D0CE53BC (void);
// 0x00000071 System.Void UnityEngine.AnimationCurve::GetKey_Injected(System.Int32,UnityEngine.Keyframe&)
extern void AnimationCurve_GetKey_Injected_m6621C42E9C97A7F85A63C06DFA0A11DC3D9C5DD4 (void);
// 0x00000072 System.Void UnityEngine.Application::Quit(System.Int32)
extern void Application_Quit_m0B016F5CC91420F53C8844350ECD7FCA673226B2 (void);
// 0x00000073 System.Void UnityEngine.Application::Quit()
extern void Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A (void);
// 0x00000074 System.Boolean UnityEngine.Application::get_isPlaying()
extern void Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567 (void);
// 0x00000075 System.String UnityEngine.Application::get_dataPath()
extern void Application_get_dataPath_m026509C15A1E58FC6461EE7EC336EC18C9C2271E (void);
// 0x00000076 System.String UnityEngine.Application::get_streamingAssetsPath()
extern void Application_get_streamingAssetsPath_mA1FBABB08D7A4590A468C7CD940CD442B58C91E1 (void);
// 0x00000077 System.String UnityEngine.Application::get_persistentDataPath()
extern void Application_get_persistentDataPath_mBD9C84D06693A9DEF2D9D2206B59D4BCF8A03463 (void);
// 0x00000078 System.String UnityEngine.Application::get_temporaryCachePath()
extern void Application_get_temporaryCachePath_mB0F2F6D4D8FD2E082F7E0831A90FC6F1D18C23DF (void);
// 0x00000079 System.Void UnityEngine.Application::set_targetFrameRate(System.Int32)
extern void Application_set_targetFrameRate_m0F44C8D07060E17D9D44D176888D14DBABE0CBFC (void);
// 0x0000007A UnityEngine.RuntimePlatform UnityEngine.Application::get_platform()
extern void Application_get_platform_mB22F7F39CDD46667C3EF64507E55BB7DA18F66C4 (void);
// 0x0000007B System.Boolean UnityEngine.Application::get_isMobilePlatform()
extern void Application_get_isMobilePlatform_m86CE0D4224E9F3E4DC8DFF848C5E559F4B3E622E (void);
// 0x0000007C UnityEngine.SystemLanguage UnityEngine.Application::get_systemLanguage()
extern void Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045 (void);
// 0x0000007D System.Void UnityEngine.Application::CallLowMemory()
extern void Application_CallLowMemory_m508B1899F8865EC715FE37ACB500C98B370F7329 (void);
// 0x0000007E System.Void UnityEngine.Application::CallLogCallback(System.String,System.String,UnityEngine.LogType,System.Boolean)
extern void Application_CallLogCallback_m42BBBDDFC6BAD182D6D574F22C0B73F3F881D681 (void);
// 0x0000007F System.Boolean UnityEngine.Application::Internal_ApplicationWantsToQuit()
extern void Application_Internal_ApplicationWantsToQuit_mF91858E5F03D57EDCCB80F470ABBA60B0FBCC5BE (void);
// 0x00000080 System.Void UnityEngine.Application::Internal_ApplicationQuit()
extern void Application_Internal_ApplicationQuit_mDFB4E615433A0A568182698ADE0E316287EDAFE3 (void);
// 0x00000081 System.Void UnityEngine.Application::Internal_ApplicationUnload()
extern void Application_Internal_ApplicationUnload_m28EA03E5E3B12350E6C4147213DE14297C50BEC7 (void);
// 0x00000082 System.Void UnityEngine.Application::InvokeOnBeforeRender()
extern void Application_InvokeOnBeforeRender_mB7267D4900392FD7E09E15DF3F6CE1C15E6598F9 (void);
// 0x00000083 System.Void UnityEngine.Application::InvokeFocusChanged(System.Boolean)
extern void Application_InvokeFocusChanged_m1A6F45FF1CA2B03A5FB1FA864BFC37248486AC2F (void);
// 0x00000084 System.Void UnityEngine.Application::InvokeDeepLinkActivated(System.String)
extern void Application_InvokeDeepLinkActivated_mCE514E3BC1F10AF34A58A40C95C4CD5ACED1396B (void);
// 0x00000085 System.Boolean UnityEngine.Application::get_isEditor()
extern void Application_get_isEditor_m7367DDB72F13E4846E8EB76FFAAACA84840BE921 (void);
// 0x00000086 System.Void UnityEngine.Application_LowMemoryCallback::.ctor(System.Object,System.IntPtr)
extern void LowMemoryCallback__ctor_m91C04CE7D7A323B50CA6A73B23949639E1EA53C3 (void);
// 0x00000087 System.Void UnityEngine.Application_LowMemoryCallback::Invoke()
extern void LowMemoryCallback_Invoke_mDF9C72A7F7CD34CC8FAED88642864AE193742437 (void);
// 0x00000088 System.IAsyncResult UnityEngine.Application_LowMemoryCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void LowMemoryCallback_BeginInvoke_mF446F2B2F047BC877D3819B38D1CB0AFC4C4D953 (void);
// 0x00000089 System.Void UnityEngine.Application_LowMemoryCallback::EndInvoke(System.IAsyncResult)
extern void LowMemoryCallback_EndInvoke_mB653B34015F110168720FED3201FDD6D2B235212 (void);
// 0x0000008A System.Void UnityEngine.Application_LogCallback::.ctor(System.Object,System.IntPtr)
extern void LogCallback__ctor_mB5F165ECC180A20258EF4EFF589D88FB9F9E9C57 (void);
// 0x0000008B System.Void UnityEngine.Application_LogCallback::Invoke(System.String,System.String,UnityEngine.LogType)
extern void LogCallback_Invoke_m5503AB0FDB4D9D1A8FFE13283321AF278B7F6C4E (void);
// 0x0000008C System.IAsyncResult UnityEngine.Application_LogCallback::BeginInvoke(System.String,System.String,UnityEngine.LogType,System.AsyncCallback,System.Object)
extern void LogCallback_BeginInvoke_m346CFB69BAB75EF96F9EBA2B3C312A1AD1D4147F (void);
// 0x0000008D System.Void UnityEngine.Application_LogCallback::EndInvoke(System.IAsyncResult)
extern void LogCallback_EndInvoke_m940C8D1C936259607F04EA27DA8FBB2828FC9280 (void);
// 0x0000008E UnityEngine.BootConfigData UnityEngine.BootConfigData::WrapBootConfigData(System.IntPtr)
extern void BootConfigData_WrapBootConfigData_mB8682F80DBE83934FFEAFE08A08B376979105E48 (void);
// 0x0000008F System.Void UnityEngine.BootConfigData::.ctor(System.IntPtr)
extern void BootConfigData__ctor_m4BF11252A4EA57BE0B82E6C1657B621D163B8068 (void);
// 0x00000090 System.Single UnityEngine.Camera::get_nearClipPlane()
extern void Camera_get_nearClipPlane_m75A7270074A35D95B05F25EBF8CE392ECA6517DC (void);
// 0x00000091 System.Void UnityEngine.Camera::set_nearClipPlane(System.Single)
extern void Camera_set_nearClipPlane_m4EA1D92F6E1D17E423EC036561E115F434DC2263 (void);
// 0x00000092 System.Single UnityEngine.Camera::get_farClipPlane()
extern void Camera_get_farClipPlane_m0FA1B9E2E815BECE2EA40023302EB942B52D9596 (void);
// 0x00000093 System.Void UnityEngine.Camera::set_farClipPlane(System.Single)
extern void Camera_set_farClipPlane_m63F1302068433A72A5103281327C68CD8F67AD41 (void);
// 0x00000094 System.Single UnityEngine.Camera::get_fieldOfView()
extern void Camera_get_fieldOfView_mA9BA910800B2E33B572929CDA9A12CE596353920 (void);
// 0x00000095 System.Void UnityEngine.Camera::set_fieldOfView(System.Single)
extern void Camera_set_fieldOfView_m138FE103CAC4B803F39E4CF579609A5C3FEB5E49 (void);
// 0x00000096 UnityEngine.RenderingPath UnityEngine.Camera::get_renderingPath()
extern void Camera_get_renderingPath_m9E4313D0F05EDEA75B1154CCE05EB8F9FC0474CD (void);
// 0x00000097 System.Void UnityEngine.Camera::set_renderingPath(UnityEngine.RenderingPath)
extern void Camera_set_renderingPath_mCB2BAB2434EEAE5C3B44083403B8D620414DFD86 (void);
// 0x00000098 UnityEngine.RenderingPath UnityEngine.Camera::get_actualRenderingPath()
extern void Camera_get_actualRenderingPath_m8BA9E6EE548FA2EDB981948271E81D81006DC9BE (void);
// 0x00000099 System.Boolean UnityEngine.Camera::get_allowHDR()
extern void Camera_get_allowHDR_mD961A362F2C8BBA6FF0B316488FC566D64CB6222 (void);
// 0x0000009A System.Single UnityEngine.Camera::get_orthographicSize()
extern void Camera_get_orthographicSize_m970DC87D428A71EDF30F9ED7D0E76E08B1BE4EFE (void);
// 0x0000009B System.Void UnityEngine.Camera::set_orthographicSize(System.Single)
extern void Camera_set_orthographicSize_mFC4BBB0BB0097A5FE13E99D8388DF3008971085F (void);
// 0x0000009C System.Boolean UnityEngine.Camera::get_orthographic()
extern void Camera_get_orthographic_m3DE9A7705E2B4926BBFD2D6798D97B931B5B5550 (void);
// 0x0000009D System.Void UnityEngine.Camera::set_orthographic(System.Boolean)
extern void Camera_set_orthographic_m38A872FC2D448915EE94C4FB72FB02D4C3F56C52 (void);
// 0x0000009E System.Single UnityEngine.Camera::get_depth()
extern void Camera_get_depth_m063B48665DB9226949AC3A615362EA20193B823D (void);
// 0x0000009F System.Void UnityEngine.Camera::set_depth(System.Single)
extern void Camera_set_depth_m33DBE382C6A293EDFF50FF459CBAB2FD3B7F469C (void);
// 0x000000A0 System.Single UnityEngine.Camera::get_aspect()
extern void Camera_get_aspect_mD0A1FC8F998473DA08866FF9CD61C02E6D5F4987 (void);
// 0x000000A1 System.Void UnityEngine.Camera::set_aspect(System.Single)
extern void Camera_set_aspect_mF0D72E1250A4408B6A7C32700EE3D80C75C3879C (void);
// 0x000000A2 System.Int32 UnityEngine.Camera::get_cullingMask()
extern void Camera_get_cullingMask_m63492ED3AFA8F571FBED0B1729264A2E3BB64236 (void);
// 0x000000A3 System.Void UnityEngine.Camera::set_cullingMask(System.Int32)
extern void Camera_set_cullingMask_mB4B1EE4C6CC355625EDE08EAF17E99DAB242E0CC (void);
// 0x000000A4 System.Int32 UnityEngine.Camera::get_eventMask()
extern void Camera_get_eventMask_m69507E71D5281F902A304A8BDDE7D23A3C501292 (void);
// 0x000000A5 System.Void UnityEngine.Camera::set_cullingMatrix(UnityEngine.Matrix4x4)
extern void Camera_set_cullingMatrix_m74474597B2F1D09F4802A40D595E96B9EDEC7F05 (void);
// 0x000000A6 UnityEngine.Color UnityEngine.Camera::get_backgroundColor()
extern void Camera_get_backgroundColor_m556B0BCFA01DC59AA6A3A4B27C9408C72C144FB5 (void);
// 0x000000A7 System.Void UnityEngine.Camera::set_backgroundColor(UnityEngine.Color)
extern void Camera_set_backgroundColor_m7083574094F4031F3289444E1AF4CBC4FEDACFCF (void);
// 0x000000A8 UnityEngine.CameraClearFlags UnityEngine.Camera::get_clearFlags()
extern void Camera_get_clearFlags_m7D0E7A0DBAB6A84B680EC09835AA2F081A17E0D7 (void);
// 0x000000A9 System.Void UnityEngine.Camera::set_clearFlags(UnityEngine.CameraClearFlags)
extern void Camera_set_clearFlags_mE79A756CD7C9C84C86B6676F5C6342A45AE5F373 (void);
// 0x000000AA UnityEngine.DepthTextureMode UnityEngine.Camera::get_depthTextureMode()
extern void Camera_get_depthTextureMode_mA4B03C6B552FB03DF8A8C20DC9F4EB8FD978A7DD (void);
// 0x000000AB System.Void UnityEngine.Camera::set_depthTextureMode(UnityEngine.DepthTextureMode)
extern void Camera_set_depthTextureMode_m2D4631800947438BE9A7697778E2CB0E38083CF1 (void);
// 0x000000AC UnityEngine.Rect UnityEngine.Camera::get_rect()
extern void Camera_get_rect_m6B59AEFF7465FA7B605D25E04E99B6E555D59DB7 (void);
// 0x000000AD UnityEngine.Rect UnityEngine.Camera::get_pixelRect()
extern void Camera_get_pixelRect_m58284153875DDE6470D4BDCAF2DFC9F5C9DE3D3A (void);
// 0x000000AE System.Int32 UnityEngine.Camera::get_pixelWidth()
extern void Camera_get_pixelWidth_m7DC2EF816FA7AB52EEE991E054FC7B1F31982802 (void);
// 0x000000AF System.Int32 UnityEngine.Camera::get_pixelHeight()
extern void Camera_get_pixelHeight_m7A18CEE2D95835EB95156E88D4D27EA018404201 (void);
// 0x000000B0 UnityEngine.RenderTexture UnityEngine.Camera::get_targetTexture()
extern void Camera_get_targetTexture_m1DF637F05FF945625231DED8F3071795755DD4BF (void);
// 0x000000B1 System.Void UnityEngine.Camera::set_targetTexture(UnityEngine.RenderTexture)
extern void Camera_set_targetTexture_m200B05665D1F5C62D9016C2DD20955755BAB4596 (void);
// 0x000000B2 System.Int32 UnityEngine.Camera::get_targetDisplay()
extern void Camera_get_targetDisplay_mED770420CB57E500C60BE15B9F7F5ED424F0BA3D (void);
// 0x000000B3 UnityEngine.Matrix4x4 UnityEngine.Camera::get_worldToCameraMatrix()
extern void Camera_get_worldToCameraMatrix_m7E2B63F64437E2C91C07F7FC819C79BE2152C5F6 (void);
// 0x000000B4 System.Void UnityEngine.Camera::set_worldToCameraMatrix(UnityEngine.Matrix4x4)
extern void Camera_set_worldToCameraMatrix_mD9E982CC4F356AC310C5B3145FC59FD73C420BE3 (void);
// 0x000000B5 UnityEngine.Matrix4x4 UnityEngine.Camera::get_projectionMatrix()
extern void Camera_get_projectionMatrix_mDB77E3A7F71CEF085797BCE58FAC78058C5D6756 (void);
// 0x000000B6 System.Void UnityEngine.Camera::set_projectionMatrix(UnityEngine.Matrix4x4)
extern void Camera_set_projectionMatrix_m3645AC49FC94726BDA07191C80299D8361D5C328 (void);
// 0x000000B7 System.Void UnityEngine.Camera::set_nonJitteredProjectionMatrix(UnityEngine.Matrix4x4)
extern void Camera_set_nonJitteredProjectionMatrix_mBD4086F0CE187C0E68619606FA3F06AB9113A7D0 (void);
// 0x000000B8 System.Void UnityEngine.Camera::set_useJitteredProjectionMatrixForTransparentRendering(System.Boolean)
extern void Camera_set_useJitteredProjectionMatrixForTransparentRendering_mC534E01407398A73886F2E5796824120B50461EA (void);
// 0x000000B9 System.Void UnityEngine.Camera::ResetProjectionMatrix()
extern void Camera_ResetProjectionMatrix_m3010D24D6ACC1880F046CAB995A1EF69B4D3C2BE (void);
// 0x000000BA UnityEngine.Matrix4x4 UnityEngine.Camera::CalculateObliqueMatrix(UnityEngine.Vector4)
extern void Camera_CalculateObliqueMatrix_m8DCA87B4563A3515995B189E4EBEAE548563B94C (void);
// 0x000000BB UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3,UnityEngine.Camera_MonoOrStereoscopicEye)
extern void Camera_WorldToScreenPoint_m205044769D9BC639283F3B929D964E1982067637 (void);
// 0x000000BC UnityEngine.Vector3 UnityEngine.Camera::ViewportToWorldPoint(UnityEngine.Vector3,UnityEngine.Camera_MonoOrStereoscopicEye)
extern void Camera_ViewportToWorldPoint_m65A69A57F4259FE074749BF3DFED7418DDAF99AA (void);
// 0x000000BD UnityEngine.Vector3 UnityEngine.Camera::WorldToScreenPoint(UnityEngine.Vector3)
extern void Camera_WorldToScreenPoint_m44710195E7736CE9DE5A9B05E32059A9A950F95C (void);
// 0x000000BE UnityEngine.Vector3 UnityEngine.Camera::ViewportToWorldPoint(UnityEngine.Vector3)
extern void Camera_ViewportToWorldPoint_m1273EE3868551C6FF551ABA9A76DC7D66E883116 (void);
// 0x000000BF UnityEngine.Vector3 UnityEngine.Camera::ScreenToViewportPoint(UnityEngine.Vector3)
extern void Camera_ScreenToViewportPoint_m0300D4845234BDBE1A1D08CF493966C57F6D4D8A (void);
// 0x000000C0 UnityEngine.Ray UnityEngine.Camera::ViewportPointToRay(UnityEngine.Vector2,UnityEngine.Camera_MonoOrStereoscopicEye)
extern void Camera_ViewportPointToRay_mCD14AD107A561268CBC5E578BB336AC5AA047365 (void);
// 0x000000C1 UnityEngine.Ray UnityEngine.Camera::ViewportPointToRay(UnityEngine.Vector3,UnityEngine.Camera_MonoOrStereoscopicEye)
extern void Camera_ViewportPointToRay_mB5148E07D2D84822653EFC04A76D819113A06781 (void);
// 0x000000C2 UnityEngine.Ray UnityEngine.Camera::ViewportPointToRay(UnityEngine.Vector3)
extern void Camera_ViewportPointToRay_mA5F17F1603768A23286A637F706B612BCEF8D2C8 (void);
// 0x000000C3 UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector2,UnityEngine.Camera_MonoOrStereoscopicEye)
extern void Camera_ScreenPointToRay_mE590E18429611C84F551E9FA8ED67391ED32D437 (void);
// 0x000000C4 UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3,UnityEngine.Camera_MonoOrStereoscopicEye)
extern void Camera_ScreenPointToRay_m217923426BB5E72835726B1ED792BFFFF8B498C1 (void);
// 0x000000C5 UnityEngine.Ray UnityEngine.Camera::ScreenPointToRay(UnityEngine.Vector3)
extern void Camera_ScreenPointToRay_mD385213935A81030EDC604A39FD64761077CFBAB (void);
// 0x000000C6 UnityEngine.Camera UnityEngine.Camera::get_main()
extern void Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (void);
// 0x000000C7 UnityEngine.Camera UnityEngine.Camera::get_current()
extern void Camera_get_current_m4E5A6D19F422F8DD2CFF4EE80C65B033F24C45D6 (void);
// 0x000000C8 System.Int32 UnityEngine.Camera::GetAllCamerasCount()
extern void Camera_GetAllCamerasCount_m3B036EBA99A0F6E8FF30C9B14DDD6C3A93C63529 (void);
// 0x000000C9 System.Int32 UnityEngine.Camera::GetAllCamerasImpl(UnityEngine.Camera[])
extern void Camera_GetAllCamerasImpl_m220999F1251B5F6846B56F6F63D0D0820AF51F0B (void);
// 0x000000CA System.Int32 UnityEngine.Camera::get_allCamerasCount()
extern void Camera_get_allCamerasCount_mFDB01BDA0C2C796020756A9DD53C38E32046B801 (void);
// 0x000000CB System.Int32 UnityEngine.Camera::GetAllCameras(UnityEngine.Camera[])
extern void Camera_GetAllCameras_mC94EF7B11A5BA31CED54B106E68D64ED82D0CEF4 (void);
// 0x000000CC System.Void UnityEngine.Camera::Render()
extern void Camera_Render_m2D9749799AAC91A3725044A6CF5594E1F5D68D61 (void);
// 0x000000CD System.Void UnityEngine.Camera::AddCommandBufferImpl(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern void Camera_AddCommandBufferImpl_mE730232060696749068B95BB44E8F766A1E6DE7F (void);
// 0x000000CE System.Void UnityEngine.Camera::RemoveCommandBufferImpl(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern void Camera_RemoveCommandBufferImpl_mD35499AF3AA5F880EAF57ADB9B3605C30311BD94 (void);
// 0x000000CF System.Void UnityEngine.Camera::AddCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern void Camera_AddCommandBuffer_m8F88C5009AC9676BCD0AD1AE4AA655D5BF71CB2F (void);
// 0x000000D0 System.Void UnityEngine.Camera::RemoveCommandBuffer(UnityEngine.Rendering.CameraEvent,UnityEngine.Rendering.CommandBuffer)
extern void Camera_RemoveCommandBuffer_m6F4BE369C87C0DB3913AE4A8DA31B2838B1DA3E6 (void);
// 0x000000D1 System.Void UnityEngine.Camera::FireOnPreCull(UnityEngine.Camera)
extern void Camera_FireOnPreCull_mAA81BB789FC8C7516B71ED4E1452C4D8E99334C9 (void);
// 0x000000D2 System.Void UnityEngine.Camera::FireOnPreRender(UnityEngine.Camera)
extern void Camera_FireOnPreRender_mB282AD49DFA0C9036D92BAE0E28F2567C713099E (void);
// 0x000000D3 System.Void UnityEngine.Camera::FireOnPostRender(UnityEngine.Camera)
extern void Camera_FireOnPostRender_mC07CDF605EEABC89BD814E09F2542E9EE16AE0DA (void);
// 0x000000D4 System.Void UnityEngine.Camera::.ctor()
extern void Camera__ctor_m30D37AB91648C862FCB8E69805DC4DF728A2804C (void);
// 0x000000D5 System.Void UnityEngine.Camera::set_cullingMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Camera_set_cullingMatrix_Injected_mAB1C10742D9D80FA70AC950323FBBFAA41D6F945 (void);
// 0x000000D6 System.Void UnityEngine.Camera::get_backgroundColor_Injected(UnityEngine.Color&)
extern void Camera_get_backgroundColor_Injected_mF1A0E8DF67F470A21F4D1A4930B978C971C5E1FC (void);
// 0x000000D7 System.Void UnityEngine.Camera::set_backgroundColor_Injected(UnityEngine.Color&)
extern void Camera_set_backgroundColor_Injected_mB5B121018E6FEF0BC6265F7EEAA8B8B77EFC6E9B (void);
// 0x000000D8 System.Void UnityEngine.Camera::get_rect_Injected(UnityEngine.Rect&)
extern void Camera_get_rect_Injected_mB6D9AB2842F87749A8AA7350C8433DE5CC6E2E9D (void);
// 0x000000D9 System.Void UnityEngine.Camera::get_pixelRect_Injected(UnityEngine.Rect&)
extern void Camera_get_pixelRect_Injected_mE01A6F5B5892953CD25DECDBC12B5CBF15CD84E2 (void);
// 0x000000DA System.Void UnityEngine.Camera::get_worldToCameraMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Camera_get_worldToCameraMatrix_Injected_mB4EE64122499915852BB85AC9383872B075AF2DA (void);
// 0x000000DB System.Void UnityEngine.Camera::set_worldToCameraMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Camera_set_worldToCameraMatrix_Injected_m1C04D82A042ADA2F5ADC85002FD65F6415B85CEB (void);
// 0x000000DC System.Void UnityEngine.Camera::get_projectionMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Camera_get_projectionMatrix_Injected_m6CC44F0D6BF337883510093EFFAC51ECE0E9BDAD (void);
// 0x000000DD System.Void UnityEngine.Camera::set_projectionMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Camera_set_projectionMatrix_Injected_m1DF5C1D3A0CB6B574F04A5EDA656CFF9B066A763 (void);
// 0x000000DE System.Void UnityEngine.Camera::set_nonJitteredProjectionMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Camera_set_nonJitteredProjectionMatrix_Injected_m1AEE58E49B64826640BA82AE15ECE7416BE9BD5A (void);
// 0x000000DF System.Void UnityEngine.Camera::CalculateObliqueMatrix_Injected(UnityEngine.Vector4&,UnityEngine.Matrix4x4&)
extern void Camera_CalculateObliqueMatrix_Injected_m41578914E8138AB55C51C55ADA2D1E79703AB0A3 (void);
// 0x000000E0 System.Void UnityEngine.Camera::WorldToScreenPoint_Injected(UnityEngine.Vector3&,UnityEngine.Camera_MonoOrStereoscopicEye,UnityEngine.Vector3&)
extern void Camera_WorldToScreenPoint_Injected_mE1001A12AC94E57CE91F40D345F0DBC3C42C188A (void);
// 0x000000E1 System.Void UnityEngine.Camera::ViewportToWorldPoint_Injected(UnityEngine.Vector3&,UnityEngine.Camera_MonoOrStereoscopicEye,UnityEngine.Vector3&)
extern void Camera_ViewportToWorldPoint_Injected_m6071E9158AF48984C9C5516462BD8D2EDF38F724 (void);
// 0x000000E2 System.Void UnityEngine.Camera::ScreenToViewportPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Camera_ScreenToViewportPoint_Injected_m313B8AF34F729447B818935A20A7EBE6724AC473 (void);
// 0x000000E3 System.Void UnityEngine.Camera::ViewportPointToRay_Injected(UnityEngine.Vector2&,UnityEngine.Camera_MonoOrStereoscopicEye,UnityEngine.Ray&)
extern void Camera_ViewportPointToRay_Injected_m908348B88DD53BB24B8CEB6A912E15FDBDEE7563 (void);
// 0x000000E4 System.Void UnityEngine.Camera::ScreenPointToRay_Injected(UnityEngine.Vector2&,UnityEngine.Camera_MonoOrStereoscopicEye,UnityEngine.Ray&)
extern void Camera_ScreenPointToRay_Injected_mB548944EF4CBF628CB55E7A729DA28A9BFFB4006 (void);
// 0x000000E5 System.Void UnityEngine.Camera_CameraCallback::.ctor(System.Object,System.IntPtr)
extern void CameraCallback__ctor_m6E26A220911F56F3E8936DDD217ED76A15B1915E (void);
// 0x000000E6 System.Void UnityEngine.Camera_CameraCallback::Invoke(UnityEngine.Camera)
extern void CameraCallback_Invoke_m52ACC6D0C6BA5A247A24DB9C63D4340B2EF97B24 (void);
// 0x000000E7 System.IAsyncResult UnityEngine.Camera_CameraCallback::BeginInvoke(UnityEngine.Camera,System.AsyncCallback,System.Object)
extern void CameraCallback_BeginInvoke_m7DD0D3DB7F6ACCBFE090C1E2EEE9BBD065A0925D (void);
// 0x000000E8 System.Void UnityEngine.Camera_CameraCallback::EndInvoke(System.IAsyncResult)
extern void CameraCallback_EndInvoke_m2CF9596F172FF401AAF5A606BE966E3D08F62DB9 (void);
// 0x000000E9 System.Void UnityEngine.CullingGroup::SendEvents(UnityEngine.CullingGroup,System.IntPtr,System.Int32)
extern void CullingGroup_SendEvents_m01D14A887DFA90F3ED208073A2AE283ADF2C8B22 (void);
// 0x000000EA System.Void UnityEngine.CullingGroup_StateChanged::.ctor(System.Object,System.IntPtr)
extern void StateChanged__ctor_mBBB5FB739CB1D1206034FFDE998AE8342CC5C0C2 (void);
// 0x000000EB System.Void UnityEngine.CullingGroup_StateChanged::Invoke(UnityEngine.CullingGroupEvent)
extern void StateChanged_Invoke_m6CD13C3770E1EB709C4B125F69F7E4CE6539814D (void);
// 0x000000EC System.IAsyncResult UnityEngine.CullingGroup_StateChanged::BeginInvoke(UnityEngine.CullingGroupEvent,System.AsyncCallback,System.Object)
extern void StateChanged_BeginInvoke_m70DAA3720650BA61CA34446A5E19736213E82D4D (void);
// 0x000000ED System.Void UnityEngine.CullingGroup_StateChanged::EndInvoke(System.IAsyncResult)
extern void StateChanged_EndInvoke_m742AAC097C6A02605BF7FB6AA283CA24C021ED65 (void);
// 0x000000EE System.Void UnityEngine.ReflectionProbe::CallReflectionProbeEvent(UnityEngine.ReflectionProbe,UnityEngine.ReflectionProbe_ReflectionProbeEvent)
extern void ReflectionProbe_CallReflectionProbeEvent_mD705BC25F93FC786FA7E2B7E1025DF35366AF31D (void);
// 0x000000EF System.Void UnityEngine.ReflectionProbe::CallSetDefaultReflection(UnityEngine.Cubemap)
extern void ReflectionProbe_CallSetDefaultReflection_m88965097CBA94F6B21ED05F6770A1CAF1279486C (void);
// 0x000000F0 System.Void UnityEngine.DebugLogHandler::Internal_Log(UnityEngine.LogType,UnityEngine.LogOption,System.String,UnityEngine.Object)
extern void DebugLogHandler_Internal_Log_mA1D09B6E813ABFAB6358863B6C2D28E3ACA9E101 (void);
// 0x000000F1 System.Void UnityEngine.DebugLogHandler::Internal_LogException(System.Exception,UnityEngine.Object)
extern void DebugLogHandler_Internal_LogException_mD0D1F120433EB1009D23E64F3A221FA234718BFF (void);
// 0x000000F2 System.Void UnityEngine.DebugLogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern void DebugLogHandler_LogFormat_mB876FBE8959FC3D9E9950527A82936F779F7A00C (void);
// 0x000000F3 System.Void UnityEngine.DebugLogHandler::LogException(System.Exception,UnityEngine.Object)
extern void DebugLogHandler_LogException_m131BB407038398CCADB197F19BB4AA2435627386 (void);
// 0x000000F4 System.Void UnityEngine.DebugLogHandler::.ctor()
extern void DebugLogHandler__ctor_mA13DBA2C9834013BEC67A4DF3E414F0E970D47C8 (void);
// 0x000000F5 UnityEngine.ILogger UnityEngine.Debug::get_unityLogger()
extern void Debug_get_unityLogger_m70D38067C3055104F6C8D050AB7CE0FDFD05EE22 (void);
// 0x000000F6 System.Void UnityEngine.Debug::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern void Debug_DrawLine_m5384A47F25E5B174ABA7D6A7A8726F2F17A5241F (void);
// 0x000000F7 System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single)
extern void Debug_DrawRay_m3954B3FFA675C0660ED438E8B705B45EDE393C60 (void);
// 0x000000F8 System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color)
extern void Debug_DrawRay_m918D1131BACEBD7CCEA9D9BFDF3279F6CD56E121 (void);
// 0x000000F9 System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Debug_DrawRay_m8477619B612629DAC24B466874AA23B6DC005D8D (void);
// 0x000000FA System.Void UnityEngine.Debug::DrawRay(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Color,System.Single,System.Boolean)
extern void Debug_DrawRay_m9F3CB318085029EFD8FEBB224C84F78D84543A0E (void);
// 0x000000FB System.Void UnityEngine.Debug::Break()
extern void Debug_Break_m9F329399AFCF1871DD61583BD8EEB3F2E9B72C3A (void);
// 0x000000FC System.Void UnityEngine.Debug::Log(System.Object)
extern void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (void);
// 0x000000FD System.Void UnityEngine.Debug::Log(System.Object,UnityEngine.Object)
extern void Debug_Log_mF3A49A8FCD5EC1535CADCDFF57432813A8790DF6 (void);
// 0x000000FE System.Void UnityEngine.Debug::LogFormat(System.String,System.Object[])
extern void Debug_LogFormat_m3FD4ED373B54BC92F95360449859CD95D45D73C6 (void);
// 0x000000FF System.Void UnityEngine.Debug::LogError(System.Object)
extern void Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485 (void);
// 0x00000100 System.Void UnityEngine.Debug::LogError(System.Object,UnityEngine.Object)
extern void Debug_LogError_mEFF048E5541EE45362C0AAD829E3FA4C2CAB9199 (void);
// 0x00000101 System.Void UnityEngine.Debug::LogErrorFormat(System.String,System.Object[])
extern void Debug_LogErrorFormat_mDBF43684A22EAAB187285C9B4174C9555DB11E83 (void);
// 0x00000102 System.Void UnityEngine.Debug::LogErrorFormat(UnityEngine.Object,System.String,System.Object[])
extern void Debug_LogErrorFormat_m1807338EFAE61B3F6CF96FCB905D9B8E2DBAA0F7 (void);
// 0x00000103 System.Void UnityEngine.Debug::LogException(System.Exception)
extern void Debug_LogException_m1BE957624F4DD291B1B4265D4A55A34EFAA8D7BA (void);
// 0x00000104 System.Void UnityEngine.Debug::LogException(System.Exception,UnityEngine.Object)
extern void Debug_LogException_mE0C50EE1EE5F38196CABAF961EF7E43DD520C29B (void);
// 0x00000105 System.Void UnityEngine.Debug::LogWarning(System.Object)
extern void Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7 (void);
// 0x00000106 System.Void UnityEngine.Debug::LogWarning(System.Object,UnityEngine.Object)
extern void Debug_LogWarning_mE6AF3EFCF84F2296622CD42FBF9EEAF07244C0A8 (void);
// 0x00000107 System.Void UnityEngine.Debug::LogWarningFormat(System.String,System.Object[])
extern void Debug_LogWarningFormat_m405E9C0A631F815816F349D7591DD545932FF774 (void);
// 0x00000108 System.Void UnityEngine.Debug::LogWarningFormat(UnityEngine.Object,System.String,System.Object[])
extern void Debug_LogWarningFormat_m2DED8BDFB26AFA883EEDD701573B30B093270CA7 (void);
// 0x00000109 System.Boolean UnityEngine.Debug::CallOverridenDebugHandler(System.Exception,UnityEngine.Object)
extern void Debug_CallOverridenDebugHandler_mDE23EE8FA741C9568B37F4128B0DA7976F8612DC (void);
// 0x0000010A System.Void UnityEngine.Debug::.cctor()
extern void Debug__cctor_m8FC005AAA0C78F065A27FD1E48B12C5C5E38A486 (void);
// 0x0000010B System.Void UnityEngine.Debug::DrawLine_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Color&,System.Single,System.Boolean)
extern void Debug_DrawLine_Injected_m6AC14B6FE78DAD68F95C3E8826C36D46849983F9 (void);
// 0x0000010C System.Void UnityEngine.Bounds::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Bounds__ctor_m8356472A177F4B22FFCE8911EBC8547A65A07CA3_AdjustorThunk (void);
// 0x0000010D System.Int32 UnityEngine.Bounds::GetHashCode()
extern void Bounds_GetHashCode_m822D1DF4F57180495A4322AADD3FD173C6FC3A1B_AdjustorThunk (void);
// 0x0000010E System.Boolean UnityEngine.Bounds::Equals(System.Object)
extern void Bounds_Equals_mB759250EA283B06481746E9A247B024D273408C9_AdjustorThunk (void);
// 0x0000010F System.Boolean UnityEngine.Bounds::Equals(UnityEngine.Bounds)
extern void Bounds_Equals_mC558BE6FE3614F7B42F5E22D1F155194A0E3DD0F_AdjustorThunk (void);
// 0x00000110 UnityEngine.Vector3 UnityEngine.Bounds::get_center()
extern void Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485_AdjustorThunk (void);
// 0x00000111 System.Void UnityEngine.Bounds::set_center(UnityEngine.Vector3)
extern void Bounds_set_center_mAC54A53224BBEFE37A4387DCBD0EF3774751221E_AdjustorThunk (void);
// 0x00000112 UnityEngine.Vector3 UnityEngine.Bounds::get_size()
extern void Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14_AdjustorThunk (void);
// 0x00000113 System.Void UnityEngine.Bounds::set_size(UnityEngine.Vector3)
extern void Bounds_set_size_mEDB113237CED433C65294B534EAD30449277FD18_AdjustorThunk (void);
// 0x00000114 UnityEngine.Vector3 UnityEngine.Bounds::get_extents()
extern void Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02_AdjustorThunk (void);
// 0x00000115 System.Void UnityEngine.Bounds::set_extents(UnityEngine.Vector3)
extern void Bounds_set_extents_m9590B3BB7D59A4C35D9F0E381A20C6A96A4D9D89_AdjustorThunk (void);
// 0x00000116 UnityEngine.Vector3 UnityEngine.Bounds::get_min()
extern void Bounds_get_min_mEDCEC21FB04A8E7196EDE841F7BE9042E1908519_AdjustorThunk (void);
// 0x00000117 UnityEngine.Vector3 UnityEngine.Bounds::get_max()
extern void Bounds_get_max_m7562010AAD919B8449B8B90382BFBA4D83C669BD_AdjustorThunk (void);
// 0x00000118 System.Boolean UnityEngine.Bounds::op_Equality(UnityEngine.Bounds,UnityEngine.Bounds)
extern void Bounds_op_Equality_mE94F24EA5893A56978CAB03E5881AD8A0767208C (void);
// 0x00000119 System.Boolean UnityEngine.Bounds::op_Inequality(UnityEngine.Bounds,UnityEngine.Bounds)
extern void Bounds_op_Inequality_m7072FE6FA9E44024F1B71806364E9189792D00C6 (void);
// 0x0000011A System.Void UnityEngine.Bounds::SetMinMax(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Bounds_SetMinMax_m37B0AF472E857A51AC86463D1CA7DB161D5A880D_AdjustorThunk (void);
// 0x0000011B System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Vector3)
extern void Bounds_Encapsulate_mE74070861B19845E70A3C333A5206823ADC7DAD7_AdjustorThunk (void);
// 0x0000011C System.Void UnityEngine.Bounds::Encapsulate(UnityEngine.Bounds)
extern void Bounds_Encapsulate_mA1D113F56C635E1ACD53FCCB1C6C2560A295533B_AdjustorThunk (void);
// 0x0000011D System.String UnityEngine.Bounds::ToString()
extern void Bounds_ToString_mC2F42E6634E786CC9A1B847ECDD88226B7880E7B_AdjustorThunk (void);
// 0x0000011E System.String UnityEngine.Bounds::ToString(System.String,System.IFormatProvider)
extern void Bounds_ToString_m493BA40092851F60E7CE73A8BD58489DAE18B3AD_AdjustorThunk (void);
// 0x0000011F System.Boolean UnityEngine.Bounds::Contains(UnityEngine.Vector3)
extern void Bounds_Contains_m24154F7F564711846389EF6AEDABB4092A72FFA1_AdjustorThunk (void);
// 0x00000120 System.Boolean UnityEngine.Bounds::Contains_Injected(UnityEngine.Bounds&,UnityEngine.Vector3&)
extern void Bounds_Contains_Injected_mF76162C458967E6EE016D0F525B9DE13F678CEDC (void);
// 0x00000121 UnityEngine.Vector3 UnityEngine.Plane::get_normal()
extern void Plane_get_normal_m71CB4BAB04EE04CAEF9AD272B16766800F7D556B_AdjustorThunk (void);
// 0x00000122 System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Plane__ctor_m5B830C0E99AA5A47EF0D15767828D6E859867E67_AdjustorThunk (void);
// 0x00000123 System.Void UnityEngine.Plane::.ctor(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Plane__ctor_m9CFA0680D3935F859B6478E777A34168D4F1D19A_AdjustorThunk (void);
// 0x00000124 System.Boolean UnityEngine.Plane::Raycast(UnityEngine.Ray,System.Single&)
extern void Plane_Raycast_m8E3B0EF5B22DF336430373D4997155B647E99A24_AdjustorThunk (void);
// 0x00000125 System.String UnityEngine.Plane::ToString()
extern void Plane_ToString_mD0E5921B1DFC4E83443BA7267E1182ACDD65C5DD_AdjustorThunk (void);
// 0x00000126 System.String UnityEngine.Plane::ToString(System.String,System.IFormatProvider)
extern void Plane_ToString_m0AF5EF6354605B6F6698346081FBC1A8BE138C4B_AdjustorThunk (void);
// 0x00000127 System.Void UnityEngine.Ray::.ctor(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Ray__ctor_m75B1F651FF47EE6B887105101B7DA61CBF41F83C_AdjustorThunk (void);
// 0x00000128 UnityEngine.Vector3 UnityEngine.Ray::get_origin()
extern void Ray_get_origin_m0C1B2BFF99CDF5231AC29AC031C161F55B53C1D0_AdjustorThunk (void);
// 0x00000129 UnityEngine.Vector3 UnityEngine.Ray::get_direction()
extern void Ray_get_direction_m2B31F86F19B64474A901B28D3808011AE7A13EFC_AdjustorThunk (void);
// 0x0000012A System.Void UnityEngine.Ray::set_direction(UnityEngine.Vector3)
extern void Ray_set_direction_mDA9E058A81EA8D21BCD222A4729F66071FDDAFE3_AdjustorThunk (void);
// 0x0000012B UnityEngine.Vector3 UnityEngine.Ray::GetPoint(System.Single)
extern void Ray_GetPoint_mC92464E32E42603B7B3444938E8BB8ADA43AB240_AdjustorThunk (void);
// 0x0000012C System.String UnityEngine.Ray::ToString()
extern void Ray_ToString_mC923383E101007E445FB0229261813AD13FBA509_AdjustorThunk (void);
// 0x0000012D System.String UnityEngine.Ray::ToString(System.String,System.IFormatProvider)
extern void Ray_ToString_m9D764E4D9D0742160471AFD9D0AA21B13252C1EC_AdjustorThunk (void);
// 0x0000012E System.Void UnityEngine.Rect::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70_AdjustorThunk (void);
// 0x0000012F System.Void UnityEngine.Rect::.ctor(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Rect__ctor_m00C682F84642AE657D7EBB0D5BC6E8F3CA4D1E82_AdjustorThunk (void);
// 0x00000130 UnityEngine.Rect UnityEngine.Rect::get_zero()
extern void Rect_get_zero_m4F738804E40698120CC691AB45A6416C4FF52589 (void);
// 0x00000131 System.Single UnityEngine.Rect::get_x()
extern void Rect_get_x_mA61220F6F26ECD6951B779FFA7CAD7ECE11D6987_AdjustorThunk (void);
// 0x00000132 System.Void UnityEngine.Rect::set_x(System.Single)
extern void Rect_set_x_m1147A05B5046E1D4427E8EC99B9DFA4A32EEDEE6_AdjustorThunk (void);
// 0x00000133 System.Single UnityEngine.Rect::get_y()
extern void Rect_get_y_m4E1AAD20D167085FF4F9E9C86EF34689F5770A74_AdjustorThunk (void);
// 0x00000134 System.Void UnityEngine.Rect::set_y(System.Single)
extern void Rect_set_y_m015507262F8AC5AFF1B4E986B66307F31FB3A10E_AdjustorThunk (void);
// 0x00000135 UnityEngine.Vector2 UnityEngine.Rect::get_position()
extern void Rect_get_position_m4D98DEE21C60D7EA5E4A30869F4DBDE25DB93A86_AdjustorThunk (void);
// 0x00000136 System.Void UnityEngine.Rect::set_position(UnityEngine.Vector2)
extern void Rect_set_position_m25716C90100155ED807D2812E24D2880D7D89D0D_AdjustorThunk (void);
// 0x00000137 UnityEngine.Vector2 UnityEngine.Rect::get_center()
extern void Rect_get_center_mDFC7A4AE153DCDC1D6248803381C6F36C7883707_AdjustorThunk (void);
// 0x00000138 UnityEngine.Vector2 UnityEngine.Rect::get_min()
extern void Rect_get_min_mAB48143A34188D0C92C811E6BCE3684FC81F29B6_AdjustorThunk (void);
// 0x00000139 UnityEngine.Vector2 UnityEngine.Rect::get_max()
extern void Rect_get_max_mD553C13D7CBC8CD7DB0D7FD0D7D2B703734EAC78_AdjustorThunk (void);
// 0x0000013A System.Single UnityEngine.Rect::get_width()
extern void Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF_AdjustorThunk (void);
// 0x0000013B System.Void UnityEngine.Rect::set_width(System.Single)
extern void Rect_set_width_m07D84AD7C7093EDCCD94A7B93A9447CA9917DD9D_AdjustorThunk (void);
// 0x0000013C System.Single UnityEngine.Rect::get_height()
extern void Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A_AdjustorThunk (void);
// 0x0000013D System.Void UnityEngine.Rect::set_height(System.Single)
extern void Rect_set_height_m4A00B16C122F44FEF4BA074386F3DC11FF4B4D23_AdjustorThunk (void);
// 0x0000013E UnityEngine.Vector2 UnityEngine.Rect::get_size()
extern void Rect_get_size_m752B3BB45AE862F6EAE941ED5E5C1B01C0973A00_AdjustorThunk (void);
// 0x0000013F System.Void UnityEngine.Rect::set_size(UnityEngine.Vector2)
extern void Rect_set_size_mC7194C291CB6B00C39FEE55985FE0D2B6F4ACF14_AdjustorThunk (void);
// 0x00000140 System.Single UnityEngine.Rect::get_xMin()
extern void Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873_AdjustorThunk (void);
// 0x00000141 System.Void UnityEngine.Rect::set_xMin(System.Single)
extern void Rect_set_xMin_mC91AC74347F8E3D537E8C5D70015E9B8EA872A3F_AdjustorThunk (void);
// 0x00000142 System.Single UnityEngine.Rect::get_yMin()
extern void Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4_AdjustorThunk (void);
// 0x00000143 System.Void UnityEngine.Rect::set_yMin(System.Single)
extern void Rect_set_yMin_mA2FDFF7C8C2361A4CF3F446BAB9A861F923F763A_AdjustorThunk (void);
// 0x00000144 System.Single UnityEngine.Rect::get_xMax()
extern void Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5_AdjustorThunk (void);
// 0x00000145 System.Void UnityEngine.Rect::set_xMax(System.Single)
extern void Rect_set_xMax_m4E466ED07B11CC5457BD62517418C493C0DDF2E3_AdjustorThunk (void);
// 0x00000146 System.Single UnityEngine.Rect::get_yMax()
extern void Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3_AdjustorThunk (void);
// 0x00000147 System.Void UnityEngine.Rect::set_yMax(System.Single)
extern void Rect_set_yMax_m4E7A7C5E88FA369D6ED022C939427F4895F6635D_AdjustorThunk (void);
// 0x00000148 System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector2)
extern void Rect_Contains_m8438BA185AD95D6C50E7A2CF9DD30FBA044CE0B2_AdjustorThunk (void);
// 0x00000149 System.Boolean UnityEngine.Rect::Contains(UnityEngine.Vector3)
extern void Rect_Contains_m51C65159B1706EB00CC962D7CD1CEC2EBD85BC3A_AdjustorThunk (void);
// 0x0000014A UnityEngine.Rect UnityEngine.Rect::OrderMinMax(UnityEngine.Rect)
extern void Rect_OrderMinMax_mB0EAA3C5660D716D83556F42F7D87DDB8FF7F39C (void);
// 0x0000014B System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect)
extern void Rect_Overlaps_mFF91B379E163CE421F334C99C9F3E5A7D3C1591F_AdjustorThunk (void);
// 0x0000014C System.Boolean UnityEngine.Rect::Overlaps(UnityEngine.Rect,System.Boolean)
extern void Rect_Overlaps_m4B186F55121E25A8D498AEFECCE973AEE62E7EDD_AdjustorThunk (void);
// 0x0000014D System.Boolean UnityEngine.Rect::op_Inequality(UnityEngine.Rect,UnityEngine.Rect)
extern void Rect_op_Inequality_m6D87EE93EB6C68B78B8C044217EAFCE33EE12B66 (void);
// 0x0000014E System.Boolean UnityEngine.Rect::op_Equality(UnityEngine.Rect,UnityEngine.Rect)
extern void Rect_op_Equality_m17C955A4F85F01A7CF0B43EDE41463301E93F6C1 (void);
// 0x0000014F System.Int32 UnityEngine.Rect::GetHashCode()
extern void Rect_GetHashCode_mE5841C54528B29D5E85173AF1973326793AF9311_AdjustorThunk (void);
// 0x00000150 System.Boolean UnityEngine.Rect::Equals(System.Object)
extern void Rect_Equals_mF1F747B913CDB083ED803F5DD21E2005736AE4AB_AdjustorThunk (void);
// 0x00000151 System.Boolean UnityEngine.Rect::Equals(UnityEngine.Rect)
extern void Rect_Equals_mC9EE5E0C234DB174AC16E653ED8D32BB4D356BB8_AdjustorThunk (void);
// 0x00000152 System.String UnityEngine.Rect::ToString()
extern void Rect_ToString_mCB7EA3D9B51213304AB0175B2D5E4545AFBCF483_AdjustorThunk (void);
// 0x00000153 System.String UnityEngine.Rect::ToString(System.String,System.IFormatProvider)
extern void Rect_ToString_m3DFE65344E06224C23BB7500D069F908D5DDE8F5_AdjustorThunk (void);
// 0x00000154 System.Int32 UnityEngine.RectInt::get_x()
extern void RectInt_get_x_m2D7412BC4EE7871D19042F469D6C573016E39F79_AdjustorThunk (void);
// 0x00000155 System.Int32 UnityEngine.RectInt::get_y()
extern void RectInt_get_y_mAEB15FA1B7CCD08DA9FCB978EE0BA45D6BB9FE33_AdjustorThunk (void);
// 0x00000156 System.Int32 UnityEngine.RectInt::get_width()
extern void RectInt_get_width_m7C3DBF05BE2DC727635FC9D7C778EACF0A9E4085_AdjustorThunk (void);
// 0x00000157 System.Int32 UnityEngine.RectInt::get_height()
extern void RectInt_get_height_m5CCF18289CB154C1709AE02AA6375093ECA07DBC_AdjustorThunk (void);
// 0x00000158 System.String UnityEngine.RectInt::ToString()
extern void RectInt_ToString_mDDF5677F3BD7A81A75913503531D9AAB6E5127A8_AdjustorThunk (void);
// 0x00000159 System.String UnityEngine.RectInt::ToString(System.String,System.IFormatProvider)
extern void RectInt_ToString_m4166071DDA0606D66F0BD83538D000174A33EED9_AdjustorThunk (void);
// 0x0000015A System.Boolean UnityEngine.RectInt::Equals(UnityEngine.RectInt)
extern void RectInt_Equals_mAF3A442FB55544FF362006EDB9B2F225D32BDF6D_AdjustorThunk (void);
// 0x0000015B System.Void UnityEngine.RectOffset::.ctor()
extern void RectOffset__ctor_m83529BADBE62C2D61ABEE8EB774BAB2D38DCF2AD (void);
// 0x0000015C System.Void UnityEngine.RectOffset::.ctor(System.Object,System.IntPtr)
extern void RectOffset__ctor_mA519A9F678D6B88731C3F6A67E0DA9092A4596D4 (void);
// 0x0000015D System.Void UnityEngine.RectOffset::Finalize()
extern void RectOffset_Finalize_m640BD40EDCC5A2774B9501E75735A1D530AFED27 (void);
// 0x0000015E System.Void UnityEngine.RectOffset::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void RectOffset__ctor_m428BA3F4AE287FA7D5F4CED6394225951E5E507B (void);
// 0x0000015F System.String UnityEngine.RectOffset::ToString()
extern void RectOffset_ToString_mFD37DA306C2835C1C5CE0F1DFBE92654A27231C0 (void);
// 0x00000160 System.String UnityEngine.RectOffset::ToString(System.String,System.IFormatProvider)
extern void RectOffset_ToString_mA3FFA19BFCBA3A7DE7700B4C6C10E476C61B4ACE (void);
// 0x00000161 System.Void UnityEngine.RectOffset::Destroy()
extern void RectOffset_Destroy_m56862AB47C5C13956BA4DDE49CB01701069E2EFE (void);
// 0x00000162 System.IntPtr UnityEngine.RectOffset::InternalCreate()
extern void RectOffset_InternalCreate_m24E70055383879A2ECA568F0B6B57C026188E3A7 (void);
// 0x00000163 System.Void UnityEngine.RectOffset::InternalDestroy(System.IntPtr)
extern void RectOffset_InternalDestroy_m58D18EDE9295B31E2BCA824825FE08F9A1C1D42F (void);
// 0x00000164 System.Int32 UnityEngine.RectOffset::get_left()
extern void RectOffset_get_left_m3B3066D09D8C9C94826258386B609CDBFFF11910 (void);
// 0x00000165 System.Void UnityEngine.RectOffset::set_left(System.Int32)
extern void RectOffset_set_left_m1D87F31ED5C73CEC8F5C42D9013FBB1527A34E36 (void);
// 0x00000166 System.Int32 UnityEngine.RectOffset::get_right()
extern void RectOffset_get_right_m889468939F3F926FE21F8E81E23C0342D45615C6 (void);
// 0x00000167 System.Void UnityEngine.RectOffset::set_right(System.Int32)
extern void RectOffset_set_right_m63D81A7E7FF7060DA8E7C518105C7E2649A5AA1E (void);
// 0x00000168 System.Int32 UnityEngine.RectOffset::get_top()
extern void RectOffset_get_top_m42000C7682185F03F23E7E0C3E8EC026FDBAB9D1 (void);
// 0x00000169 System.Void UnityEngine.RectOffset::set_top(System.Int32)
extern void RectOffset_set_top_m647BE16B99A85B3FFEE89141EE084E9FBEBF0E86 (void);
// 0x0000016A System.Int32 UnityEngine.RectOffset::get_bottom()
extern void RectOffset_get_bottom_mDDEBF1389FC1B8DCD9FC15DF91E51D264925C00D (void);
// 0x0000016B System.Void UnityEngine.RectOffset::set_bottom(System.Int32)
extern void RectOffset_set_bottom_m9DD7D8F63F7ED6F22A3981E064F6D4DB0527A45E (void);
// 0x0000016C System.Int32 UnityEngine.RectOffset::get_horizontal()
extern void RectOffset_get_horizontal_m7B1D97260EF95BCEDB9A7AF7AC9FAF99D56E9177 (void);
// 0x0000016D System.Int32 UnityEngine.RectOffset::get_vertical()
extern void RectOffset_get_vertical_m589292AEF7A556D4FD0CED648DEED422C1CA36A4 (void);
// 0x0000016E System.Void UnityEngine.LightingSettings::LightingSettingsDontStripMe()
extern void LightingSettings_LightingSettingsDontStripMe_m6503DED1969BC7777F8CE2AA1CBBC7ACB229DDA8 (void);
// 0x0000016F System.Void UnityEngine.Gizmos::DrawLine(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Gizmos_DrawLine_m91F1AA0205C7D53D2AA8E2F1D7B338E601A30823 (void);
// 0x00000170 System.Void UnityEngine.Gizmos::set_color(UnityEngine.Color)
extern void Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F (void);
// 0x00000171 System.Void UnityEngine.Gizmos::DrawLine_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Gizmos_DrawLine_Injected_m6001318DA5AFD74C82E087BCFF6A8A23378BC186 (void);
// 0x00000172 System.Void UnityEngine.Gizmos::set_color_Injected(UnityEngine.Color&)
extern void Gizmos_set_color_Injected_m41CFCA3C458C9BC5F6F71278C59F97004CFB9B6B (void);
// 0x00000173 System.Void UnityEngine.BeforeRenderHelper::Invoke()
extern void BeforeRenderHelper_Invoke_m30EA54023BDAB65766E9B5FD6FC90E92D75A7026 (void);
// 0x00000174 System.Void UnityEngine.BeforeRenderHelper::.cctor()
extern void BeforeRenderHelper__cctor_mAB141E73B12E9FD55D3258F715472F6BA0872282 (void);
// 0x00000175 System.Void UnityEngine.Display::.ctor()
extern void Display__ctor_m12A59C1FBFC6F4BAFCB7ADDACB5BE4E6F61145F0 (void);
// 0x00000176 System.Void UnityEngine.Display::.ctor(System.IntPtr)
extern void Display__ctor_m3FB487510CB9197672FAE63EFF6FD0FEAA542B4F (void);
// 0x00000177 System.Int32 UnityEngine.Display::get_renderingWidth()
extern void Display_get_renderingWidth_m426E1CB184C1135E1EE83678FFF7EF6521B5DB64 (void);
// 0x00000178 System.Int32 UnityEngine.Display::get_renderingHeight()
extern void Display_get_renderingHeight_m18F083C41C0BB1646CB4C819E1266B9B51563F61 (void);
// 0x00000179 System.Int32 UnityEngine.Display::get_systemWidth()
extern void Display_get_systemWidth_m5FDF4465D7B1A0AD8A1A8C5B314BF71F4C8DCBB5 (void);
// 0x0000017A System.Int32 UnityEngine.Display::get_systemHeight()
extern void Display_get_systemHeight_mA296AFD545D00DF7FEB84E7C690FD56CC2C19D70 (void);
// 0x0000017B UnityEngine.Vector3 UnityEngine.Display::RelativeMouseAt(UnityEngine.Vector3)
extern void Display_RelativeMouseAt_m97B71A8A86DD2983B03E4816AE5C7B95484FB011 (void);
// 0x0000017C UnityEngine.Display UnityEngine.Display::get_main()
extern void Display_get_main_mAC219027538C9134DF8606B59B8249EE027E867B (void);
// 0x0000017D System.Void UnityEngine.Display::RecreateDisplayList(System.IntPtr[])
extern void Display_RecreateDisplayList_m86C6D207FEF8B77696B74ECF530002E260B1591E (void);
// 0x0000017E System.Void UnityEngine.Display::FireDisplaysUpdated()
extern void Display_FireDisplaysUpdated_mA8B70C50D286065B80D47D6D12D195A2FFB223DA (void);
// 0x0000017F System.Void UnityEngine.Display::GetSystemExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern void Display_GetSystemExtImpl_m81EED1E4A983CF9BF6646F46C19004756F57AD3C (void);
// 0x00000180 System.Void UnityEngine.Display::GetRenderingExtImpl(System.IntPtr,System.Int32&,System.Int32&)
extern void Display_GetRenderingExtImpl_m6D78AA779EA195FA7BD70C4EEED9DD2EB986841E (void);
// 0x00000181 System.Int32 UnityEngine.Display::RelativeMouseAtImpl(System.Int32,System.Int32,System.Int32&,System.Int32&)
extern void Display_RelativeMouseAtImpl_m7991D56921379FEC4AE3516FEDF046D0DCAFDBB9 (void);
// 0x00000182 System.Void UnityEngine.Display::.cctor()
extern void Display__cctor_m87EA9DCECCE11A1F161F7C451A5018180DE9EB06 (void);
// 0x00000183 System.Void UnityEngine.Display_DisplaysUpdatedDelegate::.ctor(System.Object,System.IntPtr)
extern void DisplaysUpdatedDelegate__ctor_mE01841FD1E938AA63EF9D1153CB40E44543A78BE (void);
// 0x00000184 System.Void UnityEngine.Display_DisplaysUpdatedDelegate::Invoke()
extern void DisplaysUpdatedDelegate_Invoke_mBABCF33A27A4E0A5FBC06AECECA79FBF4293E7F9 (void);
// 0x00000185 System.IAsyncResult UnityEngine.Display_DisplaysUpdatedDelegate::BeginInvoke(System.AsyncCallback,System.Object)
extern void DisplaysUpdatedDelegate_BeginInvoke_m42DDA570D68BFAD055E2FEBB75FBE79EFEE2752F (void);
// 0x00000186 System.Void UnityEngine.Display_DisplaysUpdatedDelegate::EndInvoke(System.IAsyncResult)
extern void DisplaysUpdatedDelegate_EndInvoke_mBE00DC8335AF5142275DCCE3C5CEBC4ED0F60937 (void);
// 0x00000187 System.Int32 UnityEngine.Screen::get_width()
extern void Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C (void);
// 0x00000188 System.Int32 UnityEngine.Screen::get_height()
extern void Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE (void);
// 0x00000189 System.Single UnityEngine.Screen::get_dpi()
extern void Screen_get_dpi_m37167A82DE896C738517BBF75BFD70C616CCCF55 (void);
// 0x0000018A UnityEngine.Resolution UnityEngine.Screen::get_currentResolution()
extern void Screen_get_currentResolution_mE1A3C7E9603FA56B539FDDA1F602C66732EFD17B (void);
// 0x0000018B System.Boolean UnityEngine.Screen::get_fullScreen()
extern void Screen_get_fullScreen_mE83864F0C9C773C03D3FBBAD981159238CA3565A (void);
// 0x0000018C UnityEngine.FullScreenMode UnityEngine.Screen::get_fullScreenMode()
extern void Screen_get_fullScreenMode_m282CC0BCBD3C02B7199D0606090D28BB328EC624 (void);
// 0x0000018D System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,UnityEngine.FullScreenMode,System.Int32)
extern void Screen_SetResolution_m239784140EEC26DC52AB5ED95DD1D9BDBC49ADB1 (void);
// 0x0000018E System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,UnityEngine.FullScreenMode)
extern void Screen_SetResolution_mF6E20000304F14EC25BA996512719D7742C29175 (void);
// 0x0000018F System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean,System.Int32)
extern void Screen_SetResolution_m9C34571700200E29E9FC2C8B1F096B58F3ACADB4 (void);
// 0x00000190 System.Void UnityEngine.Screen::SetResolution(System.Int32,System.Int32,System.Boolean)
extern void Screen_SetResolution_m1C17EB7EDF8CDA862338782353D53DFF6742DFC5 (void);
// 0x00000191 UnityEngine.Resolution[] UnityEngine.Screen::get_resolutions()
extern void Screen_get_resolutions_mA2968FF5FA4EA6A6AE76019C47338824E8610A81 (void);
// 0x00000192 System.Void UnityEngine.Screen::get_currentResolution_Injected(UnityEngine.Resolution&)
extern void Screen_get_currentResolution_Injected_m8B505219301BD3B52B1AA433F59A3C190C0C1BD7 (void);
// 0x00000193 System.Int32 UnityEngine.Graphics::Internal_GetMaxDrawMeshInstanceCount()
extern void Graphics_Internal_GetMaxDrawMeshInstanceCount_m24408CC79F6E8FECC6931357C34B2A645D6FED0D (void);
// 0x00000194 System.Void UnityEngine.Graphics::Internal_SetMRTSimple(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern void Graphics_Internal_SetMRTSimple_m4FD6245D60EBBFC2CF6EF5AFB8BD4F761CCF7F8F (void);
// 0x00000195 System.Void UnityEngine.Graphics::CopyTexture_Slice_AllMips(UnityEngine.Texture,System.Int32,UnityEngine.Texture,System.Int32)
extern void Graphics_CopyTexture_Slice_AllMips_m08934B607CA033F39B5D661D6365D8DD7D700D40 (void);
// 0x00000196 System.Void UnityEngine.Graphics::CopyTexture_Region(UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Graphics_CopyTexture_Region_m07F15543D2CE113DAC6E86735500022AB0203DC5 (void);
// 0x00000197 System.Void UnityEngine.Graphics::Internal_DrawTexture(UnityEngine.Internal_DrawTextureArguments&)
extern void Graphics_Internal_DrawTexture_mB6C15F9074A9148F0F8884C6806B9F136BE329D5 (void);
// 0x00000198 System.Void UnityEngine.Graphics::Internal_BlitMaterial5(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32,System.Boolean)
extern void Graphics_Internal_BlitMaterial5_mEE98BDDC0DED5652473E2AF67A2F9158EB190884 (void);
// 0x00000199 System.Void UnityEngine.Graphics::Blit2(UnityEngine.Texture,UnityEngine.RenderTexture)
extern void Graphics_Blit2_mCD1E51AF9FB9A59E6DC0D54E3E1703C780A7275E (void);
// 0x0000019A System.Void UnityEngine.Graphics::SetRenderTargetImpl(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern void Graphics_SetRenderTargetImpl_mB59467C438E6F9558B1993BAA18D480772F8D897 (void);
// 0x0000019B System.Void UnityEngine.Graphics::SetRenderTarget(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer)
extern void Graphics_SetRenderTarget_m1D2A179C2034AED00C0249C87C12379C2CB1E204 (void);
// 0x0000019C System.Void UnityEngine.Graphics::CopyTexture(UnityEngine.Texture,System.Int32,UnityEngine.Texture,System.Int32)
extern void Graphics_CopyTexture_m6DF56E49A9215CE66C7AC8884503E1A4B9DD54DD (void);
// 0x0000019D System.Void UnityEngine.Graphics::CopyTexture(UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Texture,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Graphics_CopyTexture_mE0CCF667E93CD48B65974B538ECCD6D7E98E1F44 (void);
// 0x0000019E System.Void UnityEngine.Graphics::DrawTextureImpl(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Rect,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color,UnityEngine.Material,System.Int32)
extern void Graphics_DrawTextureImpl_mDCC2E07E786CAB30B196108A5D11F99D0F14E194 (void);
// 0x0000019F System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Rect,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color,UnityEngine.Material,System.Int32)
extern void Graphics_DrawTexture_mAAFD6A4E5B5076DFFEB4CBD1C09B3C30146AD4C8 (void);
// 0x000001A0 System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Rect,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Material,System.Int32)
extern void Graphics_DrawTexture_mB6E04C3B2283B871D12389667871E56778A9455E (void);
// 0x000001A1 System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
extern void Graphics_Blit_m946B14CAE548AAFF3FC223AB54DC5D10AEF760F7 (void);
// 0x000001A2 System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
extern void Graphics_Blit_mF7AA319E24D980D0E560A721966B87186392C3DD (void);
// 0x000001A3 System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material)
extern void Graphics_Blit_m5E0E60EA4A4D351DCC314AB2C49C7DC5B8863D05 (void);
// 0x000001A4 System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Rect,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color,UnityEngine.Material)
extern void Graphics_DrawTexture_mC03C86CF3E8769E5C0E336B514F7FCAB7BE9E77F (void);
// 0x000001A5 System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Rect,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Material)
extern void Graphics_DrawTexture_m4E6020006EB011582C0D7F4E4E71D9220BBC64E5 (void);
// 0x000001A6 System.Void UnityEngine.Graphics::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.Rect,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Graphics_DrawTexture_mE17B8B05CA0F48ADE89BDA03BC15EA716BC861EF (void);
// 0x000001A7 System.Void UnityEngine.Graphics::.cctor()
extern void Graphics__cctor_m13A73AA242ED3CEFBCC84A86F5DEEDC20ADF7DBF (void);
// 0x000001A8 System.Void UnityEngine.Graphics::Internal_SetMRTSimple_Injected(UnityEngine.RenderBuffer[],UnityEngine.RenderBuffer&,System.Int32,UnityEngine.CubemapFace,System.Int32)
extern void Graphics_Internal_SetMRTSimple_Injected_m6FBFA6630343A05E22DCAEFDA7DCBFB354605863 (void);
// 0x000001A9 System.Void UnityEngine.GL::Vertex3(System.Single,System.Single,System.Single)
extern void GL_Vertex3_mA3D208C1AAEDFCF24364A88552204D8D1BE6AFE9 (void);
// 0x000001AA System.Void UnityEngine.GL::TexCoord3(System.Single,System.Single,System.Single)
extern void GL_TexCoord3_mF136075FA8821B85501C481A38F1F5186296A6ED (void);
// 0x000001AB System.Void UnityEngine.GL::TexCoord2(System.Single,System.Single)
extern void GL_TexCoord2_mC0F9C6145A4A03D24E23BA5C5696EE834F8BA5AE (void);
// 0x000001AC System.Boolean UnityEngine.GL::get_invertCulling()
extern void GL_get_invertCulling_mC894630CB76C03799A7FE8B28BA1DF8B2E174611 (void);
// 0x000001AD System.Void UnityEngine.GL::set_invertCulling(System.Boolean)
extern void GL_set_invertCulling_mECEDF71D7F35A9F99931F6975D415C9C6A8B8FF9 (void);
// 0x000001AE System.Void UnityEngine.GL::PushMatrix()
extern void GL_PushMatrix_mF1F26015AB75226AB048A7ABF700871C5A0AE0B3 (void);
// 0x000001AF System.Void UnityEngine.GL::PopMatrix()
extern void GL_PopMatrix_m06F4C60CA3B91C7F8B7EFF83D21563C9613B3738 (void);
// 0x000001B0 System.Void UnityEngine.GL::LoadOrtho()
extern void GL_LoadOrtho_mF9410BE33CE4CBB921240C428A10D4086D2D3174 (void);
// 0x000001B1 System.Void UnityEngine.GL::GLLoadPixelMatrixScript(System.Single,System.Single,System.Single,System.Single)
extern void GL_GLLoadPixelMatrixScript_m118F6BE15090C744E0DF6A3932C1ED55303650BA (void);
// 0x000001B2 System.Void UnityEngine.GL::LoadPixelMatrix(System.Single,System.Single,System.Single,System.Single)
extern void GL_LoadPixelMatrix_m93F54ED32CAD774AA81D4057F5839FA965BAD60D (void);
// 0x000001B3 System.Void UnityEngine.GL::Begin(System.Int32)
extern void GL_Begin_mD719B21F92DCF2D8F567C008AD21CA73CA762622 (void);
// 0x000001B4 System.Void UnityEngine.GL::End()
extern void GL_End_m4A164117D2D3CD3620938F2D29EFBA2D5CB900B7 (void);
// 0x000001B5 System.Void UnityEngine.GL::GLClear(System.Boolean,System.Boolean,UnityEngine.Color,System.Single)
extern void GL_GLClear_mE2DD1932E1F31479C9741ED51046DACF5C2309AB (void);
// 0x000001B6 System.Void UnityEngine.GL::Clear(System.Boolean,System.Boolean,UnityEngine.Color)
extern void GL_Clear_mF5FCE24D7F60731D6D88AA3DC98B0E88A644CD93 (void);
// 0x000001B7 System.Void UnityEngine.GL::GLClear_Injected(System.Boolean,System.Boolean,UnityEngine.Color&,System.Single)
extern void GL_GLClear_Injected_m701ED01F88E11E3BFEEFDF7D9D88C8B8851B2FF2 (void);
// 0x000001B8 System.Int32 UnityEngine.Resolution::get_width()
extern void Resolution_get_width_mDD9DCC89D65057B64C413AF15BEE2E37E9892065_AdjustorThunk (void);
// 0x000001B9 System.Int32 UnityEngine.Resolution::get_height()
extern void Resolution_get_height_mB90F24337D7B96A288F8BE1D0F2F3599B785AD27_AdjustorThunk (void);
// 0x000001BA System.String UnityEngine.Resolution::ToString()
extern void Resolution_ToString_m0F17D03CC087E67DAB7F8F383D86A9D5C3E2587B_AdjustorThunk (void);
// 0x000001BB System.Void UnityEngine.QualitySettings::SetQualityLevel(System.Int32)
extern void QualitySettings_SetQualityLevel_m7547DAEE60DDC96320AA52240F3DB2AD60B2AA01 (void);
// 0x000001BC System.Int32 UnityEngine.QualitySettings::get_pixelLightCount()
extern void QualitySettings_get_pixelLightCount_mEDF5C3C52253C8E0BAB76B46343228C49945A28B (void);
// 0x000001BD System.Void UnityEngine.QualitySettings::set_pixelLightCount(System.Int32)
extern void QualitySettings_set_pixelLightCount_m7D98C1D031DADEEA806AA1A9BF8BEF1AE86B6B3A (void);
// 0x000001BE System.Single UnityEngine.QualitySettings::get_shadowDistance()
extern void QualitySettings_get_shadowDistance_mF31D534F44DC8CE80694E69BAD6DC80EA3758BFB (void);
// 0x000001BF System.Void UnityEngine.QualitySettings::set_shadowDistance(System.Single)
extern void QualitySettings_set_shadowDistance_mA52D73656ECABD8F02258BEB16BA4D32C9BA0B37 (void);
// 0x000001C0 System.Void UnityEngine.QualitySettings::set_shadowResolution(UnityEngine.ShadowResolution)
extern void QualitySettings_set_shadowResolution_m466012C1FE6B3813D8E65790FB491DD26BBF4B29 (void);
// 0x000001C1 System.Int32 UnityEngine.QualitySettings::get_vSyncCount()
extern void QualitySettings_get_vSyncCount_m00DC32AF7052D3BFF7EB5C384D5EA45BE0B98520 (void);
// 0x000001C2 System.Void UnityEngine.QualitySettings::set_vSyncCount(System.Int32)
extern void QualitySettings_set_vSyncCount_mE91593C022E003EC0028C9F86751FDF160F32A9E (void);
// 0x000001C3 System.Int32 UnityEngine.QualitySettings::GetQualityLevel()
extern void QualitySettings_GetQualityLevel_m0C44BD36C023D6D00BA989E483AC40EA393AF72A (void);
// 0x000001C4 System.Void UnityEngine.QualitySettings::SetQualityLevel(System.Int32,System.Boolean)
extern void QualitySettings_SetQualityLevel_m7925838FC6013484091AB48C3E8FE7B09150A7C6 (void);
// 0x000001C5 System.String[] UnityEngine.QualitySettings::get_names()
extern void QualitySettings_get_names_mBE61BC830197E73E5E236B86AB1BDF8A9D300A52 (void);
// 0x000001C6 UnityEngine.ColorSpace UnityEngine.QualitySettings::get_activeColorSpace()
extern void QualitySettings_get_activeColorSpace_m65BE7300D1A12D2981B492329B32673199CCE7F4 (void);
// 0x000001C7 System.Void UnityEngine.ImageEffectAllowedInSceneView::.ctor()
extern void ImageEffectAllowedInSceneView__ctor_mD174DBEBB4721497940142A62DCB59429340D44A (void);
// 0x000001C8 System.Void UnityEngine.LineRenderer::set_startWidth(System.Single)
extern void LineRenderer_set_startWidth_mD88B562DD413EE9861FB254963C7EDCB5199C1DF (void);
// 0x000001C9 System.Void UnityEngine.LineRenderer::set_endWidth(System.Single)
extern void LineRenderer_set_endWidth_m3DCD4FFCC3944DD6DC0737C8AF8C7EDB5A6CF59A (void);
// 0x000001CA System.Void UnityEngine.LineRenderer::SetPosition(System.Int32,UnityEngine.Vector3)
extern void LineRenderer_SetPosition_mD37DBE4B3E13A838FFD09289BC77DEDB423D620F (void);
// 0x000001CB System.Void UnityEngine.LineRenderer::SetPosition_Injected(System.Int32,UnityEngine.Vector3&)
extern void LineRenderer_SetPosition_Injected_mBF66D678D9553A69040E6D697414109AAE5FF5BA (void);
// 0x000001CC System.Void UnityEngine.MaterialPropertyBlock::SetFloatImpl(System.Int32,System.Single)
extern void MaterialPropertyBlock_SetFloatImpl_mEBFDBFB9842E8EDCD9BA82CDB35582F44F327580 (void);
// 0x000001CD System.IntPtr UnityEngine.MaterialPropertyBlock::CreateImpl()
extern void MaterialPropertyBlock_CreateImpl_m2191D3625D35DDD79213E0C8B9115FBB419F9311 (void);
// 0x000001CE System.Void UnityEngine.MaterialPropertyBlock::DestroyImpl(System.IntPtr)
extern void MaterialPropertyBlock_DestroyImpl_mEB8A57D8CAEAE69F4A4C087782629FE03F873C7D (void);
// 0x000001CF System.Void UnityEngine.MaterialPropertyBlock::.ctor()
extern void MaterialPropertyBlock__ctor_m8EB29E415C68427B841A0C68A902A8368B9228E8 (void);
// 0x000001D0 System.Void UnityEngine.MaterialPropertyBlock::Finalize()
extern void MaterialPropertyBlock_Finalize_m07D5F69E6E31F260FE7A7181692D24519E1056B5 (void);
// 0x000001D1 System.Void UnityEngine.MaterialPropertyBlock::Dispose()
extern void MaterialPropertyBlock_Dispose_m9B0577FA8137A55BEE6ED1C8CE3714F0F87238B0 (void);
// 0x000001D2 System.Void UnityEngine.MaterialPropertyBlock::SetFloat(System.String,System.Single)
extern void MaterialPropertyBlock_SetFloat_m5AADD8A9BFF5C1DD5A93BF028A2F001681221942 (void);
// 0x000001D3 UnityEngine.Bounds UnityEngine.Renderer::get_bounds()
extern void Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097 (void);
// 0x000001D4 UnityEngine.Material UnityEngine.Renderer::GetMaterial()
extern void Renderer_GetMaterial_m1879C3A29E54692423F2030EFDC7CE13817978A3 (void);
// 0x000001D5 UnityEngine.Material UnityEngine.Renderer::GetSharedMaterial()
extern void Renderer_GetSharedMaterial_m273D9ED2135F4A6808C4E814BA13E346F4231DFD (void);
// 0x000001D6 System.Void UnityEngine.Renderer::SetMaterial(UnityEngine.Material)
extern void Renderer_SetMaterial_m45DB63437410875B21F714779FC6D68827BF9497 (void);
// 0x000001D7 UnityEngine.Material[] UnityEngine.Renderer::GetMaterialArray()
extern void Renderer_GetMaterialArray_m8B6C925CA174B6E97CED486A6492790D2925FDD9 (void);
// 0x000001D8 System.Void UnityEngine.Renderer::SetMaterialArray(UnityEngine.Material[])
extern void Renderer_SetMaterialArray_m7A76143B4B693BEF5EF7993FF13546852866519B (void);
// 0x000001D9 System.Void UnityEngine.Renderer::Internal_SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern void Renderer_Internal_SetPropertyBlock_mB753C110E8FFAF514088BC469CAFE4E3197645BB (void);
// 0x000001DA System.Void UnityEngine.Renderer::Internal_GetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern void Renderer_Internal_GetPropertyBlock_mFEDF8A808AD77D7E7D2F2C19F3317352ACB99340 (void);
// 0x000001DB System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern void Renderer_SetPropertyBlock_m3F0E4E98D8274A1396AEBA8456AFA4036DCA7B7A (void);
// 0x000001DC System.Void UnityEngine.Renderer::GetPropertyBlock(UnityEngine.MaterialPropertyBlock)
extern void Renderer_GetPropertyBlock_mE73E51E42ED4F800C6422F5461A4D9E1DB61AEDC (void);
// 0x000001DD System.Boolean UnityEngine.Renderer::get_enabled()
extern void Renderer_get_enabled_mEFB6D3FD1D9D251EBB6E6286491592C4A6ABD6DE (void);
// 0x000001DE System.Void UnityEngine.Renderer::set_enabled(System.Boolean)
extern void Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B (void);
// 0x000001DF System.Void UnityEngine.Renderer::set_shadowCastingMode(UnityEngine.Rendering.ShadowCastingMode)
extern void Renderer_set_shadowCastingMode_mDD2D9695A5F1B0D1FCACB8D87A3D1148C14D6F01 (void);
// 0x000001E0 System.Void UnityEngine.Renderer::set_receiveShadows(System.Boolean)
extern void Renderer_set_receiveShadows_mA7D75C6A0E4C9F3BEB8F9BB27829A5B749AAD875 (void);
// 0x000001E1 System.Int32 UnityEngine.Renderer::get_sortingLayerID()
extern void Renderer_get_sortingLayerID_m668C1AA36751AF6655BAAD42BE7627E7950E48E8 (void);
// 0x000001E2 System.Void UnityEngine.Renderer::set_sortingLayerID(System.Int32)
extern void Renderer_set_sortingLayerID_m8F4A87EB577D6314BF191E6227E139AEBE962BC8 (void);
// 0x000001E3 System.Int32 UnityEngine.Renderer::get_sortingOrder()
extern void Renderer_get_sortingOrder_m043173C955559C12E0A33BD7F7945DA12B755AE0 (void);
// 0x000001E4 System.Void UnityEngine.Renderer::set_sortingOrder(System.Int32)
extern void Renderer_set_sortingOrder_mAABE4F8F9B158068C8A1582ACE0BFEA3CF499139 (void);
// 0x000001E5 UnityEngine.Material[] UnityEngine.Renderer::GetSharedMaterialArray()
extern void Renderer_GetSharedMaterialArray_mD493BFDF52666F719582D0C3AC0F93B16D8D80CA (void);
// 0x000001E6 UnityEngine.Material[] UnityEngine.Renderer::get_materials()
extern void Renderer_get_materials_m96CCC6CDACF2D131E18C7E0E70DE9F3AEA9E9E44 (void);
// 0x000001E7 System.Void UnityEngine.Renderer::set_materials(UnityEngine.Material[])
extern void Renderer_set_materials_mF25CCE14D0F008A45DC73043BFF69113C485A629 (void);
// 0x000001E8 UnityEngine.Material UnityEngine.Renderer::get_material()
extern void Renderer_get_material_mE6B01125502D08EE0D6DFE2EAEC064AD9BB31804 (void);
// 0x000001E9 System.Void UnityEngine.Renderer::set_material(UnityEngine.Material)
extern void Renderer_set_material_m8DED7F4F7AF38755C3D7DAFDD613BBE1AAB941BA (void);
// 0x000001EA UnityEngine.Material UnityEngine.Renderer::get_sharedMaterial()
extern void Renderer_get_sharedMaterial_m42DF538F0C6EA249B1FB626485D45D083BA74FCC (void);
// 0x000001EB System.Void UnityEngine.Renderer::set_sharedMaterial(UnityEngine.Material)
extern void Renderer_set_sharedMaterial_m1E66766F93E95F692C3C9C2C09AFD795B156678B (void);
// 0x000001EC UnityEngine.Material[] UnityEngine.Renderer::get_sharedMaterials()
extern void Renderer_get_sharedMaterials_m9B2D432CA8AD8CEC4348E61789CC1BB0C3A00AFD (void);
// 0x000001ED System.Void UnityEngine.Renderer::set_sharedMaterials(UnityEngine.Material[])
extern void Renderer_set_sharedMaterials_m9838EC09412E988925C4670E8E355E5EEFE35A25 (void);
// 0x000001EE System.Void UnityEngine.Renderer::get_bounds_Injected(UnityEngine.Bounds&)
extern void Renderer_get_bounds_Injected_m37798C3D5E22FC1910B4ED4FEBDCD264BEF4C7AA (void);
// 0x000001EF System.Boolean UnityEngine.RenderSettings::get_fog()
extern void RenderSettings_get_fog_mAB01FC3FE552B153EB0D5AB467B29A896719B90F (void);
// 0x000001F0 System.Void UnityEngine.RenderSettings::set_fog(System.Boolean)
extern void RenderSettings_set_fog_mD367E22D3EF66CA9FAAD7AF7712E891B2F569275 (void);
// 0x000001F1 System.Single UnityEngine.RenderSettings::get_fogStartDistance()
extern void RenderSettings_get_fogStartDistance_m49AA3604575352B3B33A08540295045DDF00A53B (void);
// 0x000001F2 System.Void UnityEngine.RenderSettings::set_fogStartDistance(System.Single)
extern void RenderSettings_set_fogStartDistance_m75DB7FF50F338FEA287BD5409A8A172EE9A40615 (void);
// 0x000001F3 System.Single UnityEngine.RenderSettings::get_fogEndDistance()
extern void RenderSettings_get_fogEndDistance_mBE81E3EBEAE4F9360FBECFBC436176DCA5D1858E (void);
// 0x000001F4 System.Void UnityEngine.RenderSettings::set_fogEndDistance(System.Single)
extern void RenderSettings_set_fogEndDistance_m09D18D44027CBEDD3D0DBF9EC5DC5FA5FE9DFA0B (void);
// 0x000001F5 UnityEngine.FogMode UnityEngine.RenderSettings::get_fogMode()
extern void RenderSettings_get_fogMode_m7906B56E609BFBE526DFDE18FBD9FFB44C606B93 (void);
// 0x000001F6 System.Void UnityEngine.RenderSettings::set_fogMode(UnityEngine.FogMode)
extern void RenderSettings_set_fogMode_m791A6875CFF4A9C3A937CDFCA765AD6D6D00B703 (void);
// 0x000001F7 UnityEngine.Color UnityEngine.RenderSettings::get_fogColor()
extern void RenderSettings_get_fogColor_m0063E6F93BFEA19A22EA81BDDE0EBFD422731240 (void);
// 0x000001F8 System.Void UnityEngine.RenderSettings::set_fogColor(UnityEngine.Color)
extern void RenderSettings_set_fogColor_mC46154762710EFAA869A4E972C16D1FE9B0EA01F (void);
// 0x000001F9 System.Single UnityEngine.RenderSettings::get_fogDensity()
extern void RenderSettings_get_fogDensity_mDED6FA3F6CA0894141236FAB99EE5FF7DFDDC8B6 (void);
// 0x000001FA System.Void UnityEngine.RenderSettings::set_fogDensity(System.Single)
extern void RenderSettings_set_fogDensity_mC82D5FB8E350FF4A45D41B360FA1FC00384FAA94 (void);
// 0x000001FB UnityEngine.Color UnityEngine.RenderSettings::get_ambientLight()
extern void RenderSettings_get_ambientLight_m1879C965ADD513A1FC6732DC9BA6AC6C120B304B (void);
// 0x000001FC System.Void UnityEngine.RenderSettings::set_ambientLight(UnityEngine.Color)
extern void RenderSettings_set_ambientLight_m0BD3CBB34DDA694D6459FB5A68859A4C070670D2 (void);
// 0x000001FD System.Void UnityEngine.RenderSettings::get_fogColor_Injected(UnityEngine.Color&)
extern void RenderSettings_get_fogColor_Injected_m70851CFD9435A4E097679ECF38615E4AA6D02BEA (void);
// 0x000001FE System.Void UnityEngine.RenderSettings::set_fogColor_Injected(UnityEngine.Color&)
extern void RenderSettings_set_fogColor_Injected_m8EC222EF2B9667553BBA75981EB52CB90088A585 (void);
// 0x000001FF System.Void UnityEngine.RenderSettings::get_ambientLight_Injected(UnityEngine.Color&)
extern void RenderSettings_get_ambientLight_Injected_mC315F9B62DA59EC4DB3039DFE1C87BADF7AFAFCB (void);
// 0x00000200 System.Void UnityEngine.RenderSettings::set_ambientLight_Injected(UnityEngine.Color&)
extern void RenderSettings_set_ambientLight_Injected_mBC0B3DCF653CFBA1988C9FFC7DB6D264F5BE090D (void);
// 0x00000201 UnityEngine.Shader UnityEngine.Shader::Find(System.String)
extern void Shader_Find_m596EC6EBDCA8C9D5D86E2410A319928C1E8E6B5A (void);
// 0x00000202 System.Void UnityEngine.Shader::set_maximumLOD(System.Int32)
extern void Shader_set_maximumLOD_m0E42E3AC1D63AEBC9CE42A07AFC729F424365468 (void);
// 0x00000203 System.Void UnityEngine.Shader::EnableKeyword(System.String)
extern void Shader_EnableKeyword_m9AC58243D7FBD69DD73D13B73085E0B24E4ECFCE (void);
// 0x00000204 System.Void UnityEngine.Shader::DisableKeyword(System.String)
extern void Shader_DisableKeyword_mA6360809F3D85D7D0CAE3EE3C9A85987EC60DBCF (void);
// 0x00000205 System.Int32 UnityEngine.Shader::PropertyToID(System.String)
extern void Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F (void);
// 0x00000206 System.Void UnityEngine.Shader::SetGlobalFloatImpl(System.Int32,System.Single)
extern void Shader_SetGlobalFloatImpl_m02A206C366CB1EC84C3A579786F64397CC07EE18 (void);
// 0x00000207 System.Void UnityEngine.Shader::SetGlobalFloat(System.String,System.Single)
extern void Shader_SetGlobalFloat_mD653B388A2C2E91C323FDA07C9D9FA295738A420 (void);
// 0x00000208 System.Void UnityEngine.Shader::.ctor()
extern void Shader__ctor_m51131927C3747131EBA5F99732E86E5C9F176DC8 (void);
// 0x00000209 System.Void UnityEngine.Material::CreateWithShader(UnityEngine.Material,UnityEngine.Shader)
extern void Material_CreateWithShader_mD4E25791C111800AB1E98BEAD7D45CE72B912E62 (void);
// 0x0000020A System.Void UnityEngine.Material::CreateWithMaterial(UnityEngine.Material,UnityEngine.Material)
extern void Material_CreateWithMaterial_mD2035B551DB7CFA1296B91C0CCC3475C5706EE41 (void);
// 0x0000020B System.Void UnityEngine.Material::CreateWithString(UnityEngine.Material)
extern void Material_CreateWithString_m0C0F291654AFD2D5643A1A7826844F2416284F1F (void);
// 0x0000020C System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
extern void Material__ctor_mD2A3BCD3B4F17F5C6E95F3B34DAF4B497B67127E (void);
// 0x0000020D System.Void UnityEngine.Material::.ctor(UnityEngine.Material)
extern void Material__ctor_mD0C3D9CFAFE0FB858D864092467387D7FA178245 (void);
// 0x0000020E System.Void UnityEngine.Material::.ctor(System.String)
extern void Material__ctor_m71FEB02D66A71A0FF513ABC40339E1561A8192E0 (void);
// 0x0000020F UnityEngine.Shader UnityEngine.Material::get_shader()
extern void Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097 (void);
// 0x00000210 System.Void UnityEngine.Material::set_shader(UnityEngine.Shader)
extern void Material_set_shader_m21134B4BB30FB4978B4D583FA4A8AFF2A8A9410D (void);
// 0x00000211 UnityEngine.Color UnityEngine.Material::get_color()
extern void Material_get_color_m7926F7BE68B4D000306738C1EAABEB7ADFB97821 (void);
// 0x00000212 System.Void UnityEngine.Material::set_color(UnityEngine.Color)
extern void Material_set_color_mC3C88E2389B7132EBF3EB0D1F040545176B795C0 (void);
// 0x00000213 UnityEngine.Texture UnityEngine.Material::get_mainTexture()
extern void Material_get_mainTexture_mD1F98F8E09F68857D5408796A76A521925A04FAC (void);
// 0x00000214 System.Void UnityEngine.Material::set_mainTexture(UnityEngine.Texture)
extern void Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5 (void);
// 0x00000215 System.Int32 UnityEngine.Material::GetFirstPropertyNameIdByAttribute(UnityEngine.Rendering.ShaderPropertyFlags)
extern void Material_GetFirstPropertyNameIdByAttribute_m7192A1C1FCDB6F58A21C00571D2A67948E944CC0 (void);
// 0x00000216 System.Boolean UnityEngine.Material::HasProperty(System.Int32)
extern void Material_HasProperty_m699B4D99152E3A99733B8BD7D41EAE08BB8B1657 (void);
// 0x00000217 System.Boolean UnityEngine.Material::HasProperty(System.String)
extern void Material_HasProperty_mB6F155CD45C688DA232B56BD1A74474C224BE37E (void);
// 0x00000218 System.Void UnityEngine.Material::set_renderQueue(System.Int32)
extern void Material_set_renderQueue_m239F950307B3B71DC41AF02F9030DD0A80A3A201 (void);
// 0x00000219 System.Void UnityEngine.Material::EnableKeyword(System.String)
extern void Material_EnableKeyword_mBD03896F11814C3EF67F73A414DC66D5B577171D (void);
// 0x0000021A System.Void UnityEngine.Material::DisableKeyword(System.String)
extern void Material_DisableKeyword_mD43BE3ED8D792B7242F5487ADC074DF2A5A1BD18 (void);
// 0x0000021B System.Boolean UnityEngine.Material::IsKeywordEnabled(System.String)
extern void Material_IsKeywordEnabled_m21EB58B980BA61215B281A9C18BC861BF6CF126E (void);
// 0x0000021C System.Int32 UnityEngine.Material::get_passCount()
extern void Material_get_passCount_m8604F2400F17AC2524B95F1C4F39C785896EEE3A (void);
// 0x0000021D System.Void UnityEngine.Material::SetOverrideTag(System.String,System.String)
extern void Material_SetOverrideTag_m5786C05270AD48B5B0756D5BC367017965B880C4 (void);
// 0x0000021E System.String UnityEngine.Material::GetTagImpl(System.String,System.Boolean,System.String)
extern void Material_GetTagImpl_m33F2C64FC3861174F902B816B79EEC04884A18E0 (void);
// 0x0000021F System.String UnityEngine.Material::GetTag(System.String,System.Boolean)
extern void Material_GetTag_mEAF0285CDE4B5747603C684BB900CA34DCB8B5F9 (void);
// 0x00000220 System.Boolean UnityEngine.Material::SetPass(System.Int32)
extern void Material_SetPass_mC888245491A21488CFF2FD64CA45E8404CB9FF9C (void);
// 0x00000221 System.Void UnityEngine.Material::CopyPropertiesFromMaterial(UnityEngine.Material)
extern void Material_CopyPropertiesFromMaterial_m5A6DE308328EAB762EF5BE3253B728C8078773CF (void);
// 0x00000222 System.String[] UnityEngine.Material::GetShaderKeywords()
extern void Material_GetShaderKeywords_mC4CE7855606BC17C1CD3ABD4FC90F6E184DFE88B (void);
// 0x00000223 System.Void UnityEngine.Material::SetShaderKeywords(System.String[])
extern void Material_SetShaderKeywords_m73BF3A5DE699A06D214972A546644D37524D53CB (void);
// 0x00000224 System.String[] UnityEngine.Material::get_shaderKeywords()
extern void Material_get_shaderKeywords_mDAD743C090C3CEE0B2883140B244853D71C5E9E0 (void);
// 0x00000225 System.Void UnityEngine.Material::set_shaderKeywords(System.String[])
extern void Material_set_shaderKeywords_m9EC5EFA52BF30597B1692C623806E7167B1C7688 (void);
// 0x00000226 System.Void UnityEngine.Material::SetFloatImpl(System.Int32,System.Single)
extern void Material_SetFloatImpl_m07966D17C660588628A2ACDBBE2DD5FE0F830F1E (void);
// 0x00000227 System.Void UnityEngine.Material::SetColorImpl(System.Int32,UnityEngine.Color)
extern void Material_SetColorImpl_m0981AC6BCE99E2692B1859AC5A3A4334C5F52185 (void);
// 0x00000228 System.Void UnityEngine.Material::SetMatrixImpl(System.Int32,UnityEngine.Matrix4x4)
extern void Material_SetMatrixImpl_mF2A4040AEEC3AC166441A54E0ED04B7222AFD5D8 (void);
// 0x00000229 System.Void UnityEngine.Material::SetTextureImpl(System.Int32,UnityEngine.Texture)
extern void Material_SetTextureImpl_m671ED2C28CE1AE0E0E94E3E7633ABFA75186317C (void);
// 0x0000022A System.Void UnityEngine.Material::SetBufferImpl(System.Int32,UnityEngine.ComputeBuffer)
extern void Material_SetBufferImpl_m21218ADB88A54C2923032EDD633F786C44FE8FD6 (void);
// 0x0000022B System.Single UnityEngine.Material::GetFloatImpl(System.Int32)
extern void Material_GetFloatImpl_mB3186CDAD4244298ABA911133FB672182F465DB3 (void);
// 0x0000022C UnityEngine.Color UnityEngine.Material::GetColorImpl(System.Int32)
extern void Material_GetColorImpl_m317B54F3AB71AEE4527C34202EF69DA52DC7D313 (void);
// 0x0000022D UnityEngine.Texture UnityEngine.Material::GetTextureImpl(System.Int32)
extern void Material_GetTextureImpl_mD8BBF7EC67606802B01BF2BB55FEA981DBFFC1AE (void);
// 0x0000022E System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
extern void Material_SetFloat_mBE01E05D49E5C7045E010F49A38E96B101D82768 (void);
// 0x0000022F System.Void UnityEngine.Material::SetFloat(System.Int32,System.Single)
extern void Material_SetFloat_mAC7DC962B356565CF6743E358C7A19D0322EA060 (void);
// 0x00000230 System.Void UnityEngine.Material::SetInt(System.String,System.Int32)
extern void Material_SetInt_m15D944E498726C9BB3A60A41DAAA45000F570F87 (void);
// 0x00000231 System.Void UnityEngine.Material::SetInt(System.Int32,System.Int32)
extern void Material_SetInt_m3EB8D863E3CAE33B5EC0CB679D061CEBE174E44C (void);
// 0x00000232 System.Void UnityEngine.Material::SetColor(System.String,UnityEngine.Color)
extern void Material_SetColor_m5CAAF4A8D7F839597B4E14588E341462EEB81698 (void);
// 0x00000233 System.Void UnityEngine.Material::SetColor(System.Int32,UnityEngine.Color)
extern void Material_SetColor_m9DE63FCC5A31918F8A9A2E4FCED70C298677A7B4 (void);
// 0x00000234 System.Void UnityEngine.Material::SetVector(System.String,UnityEngine.Vector4)
extern void Material_SetVector_mCB22CD5FDA6D8C7C282D7998A9244E12CC293F0D (void);
// 0x00000235 System.Void UnityEngine.Material::SetVector(System.Int32,UnityEngine.Vector4)
extern void Material_SetVector_m47F7F5B5B21FA28885C4E747AF1C32F40C1022CB (void);
// 0x00000236 System.Void UnityEngine.Material::SetMatrix(System.Int32,UnityEngine.Matrix4x4)
extern void Material_SetMatrix_m7266FA4400474D08A30181EE08E01760CCAEBA0A (void);
// 0x00000237 System.Void UnityEngine.Material::SetTexture(System.String,UnityEngine.Texture)
extern void Material_SetTexture_m04A1CD55201841F85E475992931A210229C782CF (void);
// 0x00000238 System.Void UnityEngine.Material::SetTexture(System.Int32,UnityEngine.Texture)
extern void Material_SetTexture_mECB29488B89AB3E516331DA41409510D570E9B60 (void);
// 0x00000239 System.Void UnityEngine.Material::SetBuffer(System.String,UnityEngine.ComputeBuffer)
extern void Material_SetBuffer_m8E57EF9FF4D5A9C6F7C76A3C4EADC7058FFEA341 (void);
// 0x0000023A System.Single UnityEngine.Material::GetFloat(System.String)
extern void Material_GetFloat_mF2F48AFBDFC1E1E72A00F614EF20B656262EB167 (void);
// 0x0000023B System.Single UnityEngine.Material::GetFloat(System.Int32)
extern void Material_GetFloat_m508B992651DD512ECB2A51336C9A4E87AED82D27 (void);
// 0x0000023C UnityEngine.Color UnityEngine.Material::GetColor(System.String)
extern void Material_GetColor_m5B75B83FE5821381064306ECFEEF0CC44BE66688 (void);
// 0x0000023D UnityEngine.Color UnityEngine.Material::GetColor(System.Int32)
extern void Material_GetColor_m87CBA0F1030841DE18DED76EA658006A86060EA7 (void);
// 0x0000023E UnityEngine.Vector4 UnityEngine.Material::GetVector(System.String)
extern void Material_GetVector_m0E41ED876B69FCFC4B9EA715D0286EE714CD201F (void);
// 0x0000023F UnityEngine.Vector4 UnityEngine.Material::GetVector(System.Int32)
extern void Material_GetVector_m0F76C999BC936C571A3C20054D266DF122A85E88 (void);
// 0x00000240 UnityEngine.Texture UnityEngine.Material::GetTexture(System.String)
extern void Material_GetTexture_m559F9134FDF1311F3D39B8C22A90A50A9F80A5FB (void);
// 0x00000241 UnityEngine.Texture UnityEngine.Material::GetTexture(System.Int32)
extern void Material_GetTexture_m02A9C3BA6C1396C0F1AAA4C248B9A81D7ABED680 (void);
// 0x00000242 System.Void UnityEngine.Material::SetColorImpl_Injected(System.Int32,UnityEngine.Color&)
extern void Material_SetColorImpl_Injected_mB1B35D7949FB31533E2DF99F5A0C5DC3B798EC39 (void);
// 0x00000243 System.Void UnityEngine.Material::SetMatrixImpl_Injected(System.Int32,UnityEngine.Matrix4x4&)
extern void Material_SetMatrixImpl_Injected_m15ABD6B185339B78F89DE924539ABB632DEEC7D8 (void);
// 0x00000244 System.Void UnityEngine.Material::GetColorImpl_Injected(System.Int32,UnityEngine.Color&)
extern void Material_GetColorImpl_Injected_mFF3241EC1855296376026105A435E5B394D5AF38 (void);
// 0x00000245 UnityEngine.LightType UnityEngine.Light::get_type()
extern void Light_get_type_mDBBEC33D93952330EED5B02B15865C59D5C355A0 (void);
// 0x00000246 System.Void UnityEngine.Light::set_type(UnityEngine.LightType)
extern void Light_set_type_mD68B010AE47ECDDF9C13D9F99F13EB69E9A9F7F4 (void);
// 0x00000247 System.Single UnityEngine.Light::get_spotAngle()
extern void Light_get_spotAngle_m7BFB3B329103477AFFBB9F9E059718AB212D5FD7 (void);
// 0x00000248 UnityEngine.Color UnityEngine.Light::get_color()
extern void Light_get_color_mB587B97487FFA7F7E0415F270283E48D2D901F4B (void);
// 0x00000249 System.Void UnityEngine.Light::set_color(UnityEngine.Color)
extern void Light_set_color_mB33E961A7CF25D0EBE410EE22444B4B8D4317C6C (void);
// 0x0000024A System.Single UnityEngine.Light::get_intensity()
extern void Light_get_intensity_mFABC9E1EA23E954E1072233C33C2211D64262326 (void);
// 0x0000024B System.Void UnityEngine.Light::set_intensity(System.Single)
extern void Light_set_intensity_m372D5B9494809AFAD717B2707957DD1478C52DFC (void);
// 0x0000024C System.Single UnityEngine.Light::get_bounceIntensity()
extern void Light_get_bounceIntensity_m6B586C8D305CE352E537E4AC8E8F957E512C4D50 (void);
// 0x0000024D System.Single UnityEngine.Light::get_range()
extern void Light_get_range_m94D58A8FE80BC5B13571D9CC8EBF88336BA0F831 (void);
// 0x0000024E System.Void UnityEngine.Light::set_range(System.Single)
extern void Light_set_range_mEA722B268DA5244F52B50D44FFCC1354E8FC99B4 (void);
// 0x0000024F UnityEngine.LightBakingOutput UnityEngine.Light::get_bakingOutput()
extern void Light_get_bakingOutput_m3696BB20EFCAFCB3CB579E74A5FE00EAA0E71CB5 (void);
// 0x00000250 System.Void UnityEngine.Light::set_cullingMask(System.Int32)
extern void Light_set_cullingMask_m4C8DE4FCAA45EBDC4F5A29639D87A4B8BA5AF717 (void);
// 0x00000251 UnityEngine.LightShadows UnityEngine.Light::get_shadows()
extern void Light_get_shadows_mE77B8235C26E28A797CDDF283D167EE034226AD5 (void);
// 0x00000252 System.Void UnityEngine.Light::set_shadows(UnityEngine.LightShadows)
extern void Light_set_shadows_mC464AA4717DB55512DC06B3DFB95C8839F16C20B (void);
// 0x00000253 System.Void UnityEngine.Light::set_shadowStrength(System.Single)
extern void Light_set_shadowStrength_mB3632DDAD7DDDC29FEA4D3966201340C8846AC2D (void);
// 0x00000254 System.Single UnityEngine.Light::get_cookieSize()
extern void Light_get_cookieSize_mE1168D491F8BC5DB1BA10D6E9C3B39A46177DF2B (void);
// 0x00000255 UnityEngine.Texture UnityEngine.Light::get_cookie()
extern void Light_get_cookie_mC164223C67736F4E027DC020685D4C6D24E5A705 (void);
// 0x00000256 System.Void UnityEngine.Light::get_color_Injected(UnityEngine.Color&)
extern void Light_get_color_Injected_mFC80DFA291AB496FAE0BC39E82460F6653B3362D (void);
// 0x00000257 System.Void UnityEngine.Light::set_color_Injected(UnityEngine.Color&)
extern void Light_set_color_Injected_mD9A13B1B67C6B666BBCD27997B052C544018FD8A (void);
// 0x00000258 System.Void UnityEngine.Light::get_bakingOutput_Injected(UnityEngine.LightBakingOutput&)
extern void Light_get_bakingOutput_Injected_m7B026203BB40826D50299070138CF8F6A3519DED (void);
// 0x00000259 UnityEngine.Material UnityEngine.Skybox::get_material()
extern void Skybox_get_material_m9AAE5143C39D1E5E13CD41BE53E950B56E5AAD85 (void);
// 0x0000025A System.Void UnityEngine.Skybox::set_material(UnityEngine.Material)
extern void Skybox_set_material_m74316F6709A669D3181C4CB1EA1F40E745D0F88A (void);
// 0x0000025B System.Void UnityEngine.MeshFilter::DontStripMeshFilter()
extern void MeshFilter_DontStripMeshFilter_m8982FEABBD1847BE8B3E53E9DD2A15FBC7370DE2 (void);
// 0x0000025C UnityEngine.Mesh UnityEngine.MeshFilter::get_sharedMesh()
extern void MeshFilter_get_sharedMesh_mDCB12AB93E6E5F477F55A14990A7AB5F1B06F19E (void);
// 0x0000025D System.Void UnityEngine.MeshFilter::set_sharedMesh(UnityEngine.Mesh)
extern void MeshFilter_set_sharedMesh_mC96D5F9AE4BC1D186221F568A4C3CE23572EC958 (void);
// 0x0000025E UnityEngine.Mesh UnityEngine.MeshFilter::get_mesh()
extern void MeshFilter_get_mesh_mFC1DF5AFBC1E4269D08628DB83C954882FD2B417 (void);
// 0x0000025F System.Void UnityEngine.MeshFilter::set_mesh(UnityEngine.Mesh)
extern void MeshFilter_set_mesh_m13177C1A6C29D76DDCD858CEF2B28C2AA7CC46FC (void);
// 0x00000260 System.Void UnityEngine.MeshRenderer::DontStripMeshRenderer()
extern void MeshRenderer_DontStripMeshRenderer_m68A34690B98E3BF30C620117C323B48A31DE512F (void);
// 0x00000261 System.Void UnityEngine.Mesh::Internal_Create(UnityEngine.Mesh)
extern void Mesh_Internal_Create_m6802A5B262F48CF3D72E58C2C234EF063C2552B7 (void);
// 0x00000262 System.Void UnityEngine.Mesh::.ctor()
extern void Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6 (void);
// 0x00000263 System.Int32[] UnityEngine.Mesh::GetTrianglesImpl(System.Int32,System.Boolean)
extern void Mesh_GetTrianglesImpl_m5876580E004D6131F9D4635B640356171F624A05 (void);
// 0x00000264 System.Int32[] UnityEngine.Mesh::GetIndicesImpl(System.Int32,System.Boolean)
extern void Mesh_GetIndicesImpl_m4438ED268047265769FBD7D73F942C13293ECF9E (void);
// 0x00000265 System.Void UnityEngine.Mesh::SetIndicesImpl(System.Int32,UnityEngine.MeshTopology,UnityEngine.Rendering.IndexFormat,System.Array,System.Int32,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetIndicesImpl_mD313F66DDE73C2497530789D9EF7E2EEC9633479 (void);
// 0x00000266 System.Void UnityEngine.Mesh::PrintErrorCantAccessChannel(UnityEngine.Rendering.VertexAttribute)
extern void Mesh_PrintErrorCantAccessChannel_m39BB0FADC48525EAE52AA38AC8D7EE5BA650294C (void);
// 0x00000267 System.Boolean UnityEngine.Mesh::HasVertexAttribute(UnityEngine.Rendering.VertexAttribute)
extern void Mesh_HasVertexAttribute_m55371DBBBA8C77FBF6404F0D7989C0C7BDE3275C (void);
// 0x00000268 System.Void UnityEngine.Mesh::SetArrayForChannelImpl(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32,System.Array,System.Int32,System.Int32,System.Int32,UnityEngine.Rendering.MeshUpdateFlags)
extern void Mesh_SetArrayForChannelImpl_mBE2748A89C312EE0F77BBD88F086EB421AA64952 (void);
// 0x00000269 System.Array UnityEngine.Mesh::GetAllocArrayFromChannelImpl(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32)
extern void Mesh_GetAllocArrayFromChannelImpl_m9EC298F950FDC7F699CB02A265AAE1E1E580B541 (void);
// 0x0000026A System.Boolean UnityEngine.Mesh::get_canAccess()
extern void Mesh_get_canAccess_m991B64F0FA651459A7E0DD4526D7EF0384F1792F (void);
// 0x0000026B System.Int32 UnityEngine.Mesh::get_vertexCount()
extern void Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F (void);
// 0x0000026C System.Int32 UnityEngine.Mesh::get_subMeshCount()
extern void Mesh_get_subMeshCount_m60E2BCBFEEF21260C70D06EAEC3A2A51D80796FF (void);
// 0x0000026D System.Void UnityEngine.Mesh::set_subMeshCount(System.Int32)
extern void Mesh_set_subMeshCount_mF6F2199AE4FA096C1AE0CAD02E13B6FEA38C6283 (void);
// 0x0000026E UnityEngine.Bounds UnityEngine.Mesh::get_bounds()
extern void Mesh_get_bounds_m8704A23E8BA2D77C89FD4BF4379238062B1BE5DF (void);
// 0x0000026F System.Void UnityEngine.Mesh::set_bounds(UnityEngine.Bounds)
extern void Mesh_set_bounds_m9752E145EA6D719B417AA27555DDC2A388AB4E0A (void);
// 0x00000270 System.Void UnityEngine.Mesh::ClearImpl(System.Boolean)
extern void Mesh_ClearImpl_mD00C840FA60B68829F9D315B6888D60D0E3EB9E5 (void);
// 0x00000271 System.Void UnityEngine.Mesh::RecalculateBoundsImpl(UnityEngine.Rendering.MeshUpdateFlags)
extern void Mesh_RecalculateBoundsImpl_mB8C82BD506A5D075562538AE30E1569BA2DFA373 (void);
// 0x00000272 System.Void UnityEngine.Mesh::RecalculateNormalsImpl(UnityEngine.Rendering.MeshUpdateFlags)
extern void Mesh_RecalculateNormalsImpl_m9810DED3E3F7F230F0649CBB207110D1B59E24FC (void);
// 0x00000273 System.Void UnityEngine.Mesh::MarkDynamicImpl()
extern void Mesh_MarkDynamicImpl_m58203C57ECC338CBE9FD4561A48515386C7A0174 (void);
// 0x00000274 System.Void UnityEngine.Mesh::UploadMeshDataImpl(System.Boolean)
extern void Mesh_UploadMeshDataImpl_mEE786AC05A4FAFA95782AB8D08BAA95243B3146E (void);
// 0x00000275 UnityEngine.Rendering.VertexAttribute UnityEngine.Mesh::GetUVChannel(System.Int32)
extern void Mesh_GetUVChannel_m9566A8802F5B87D061A2812FEF94230F8EA1CBBF (void);
// 0x00000276 System.Int32 UnityEngine.Mesh::DefaultDimensionForChannel(UnityEngine.Rendering.VertexAttribute)
extern void Mesh_DefaultDimensionForChannel_m95062483A5D77AC517FE0F87EC41250CFDDEF8FD (void);
// 0x00000277 T[] UnityEngine.Mesh::GetAllocArrayFromChannel(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32)
// 0x00000278 T[] UnityEngine.Mesh::GetAllocArrayFromChannel(UnityEngine.Rendering.VertexAttribute)
// 0x00000279 System.Void UnityEngine.Mesh::SetSizedArrayForChannel(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32,System.Array,System.Int32,System.Int32,System.Int32,UnityEngine.Rendering.MeshUpdateFlags)
extern void Mesh_SetSizedArrayForChannel_m4E03A6A18D0C5BB49E89828AE7A0DD34BB20E7CC (void);
// 0x0000027A System.Void UnityEngine.Mesh::SetArrayForChannel(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32,T[],UnityEngine.Rendering.MeshUpdateFlags)
// 0x0000027B System.Void UnityEngine.Mesh::SetArrayForChannel(UnityEngine.Rendering.VertexAttribute,T[],UnityEngine.Rendering.MeshUpdateFlags)
// 0x0000027C System.Void UnityEngine.Mesh::SetListForChannel(UnityEngine.Rendering.VertexAttribute,UnityEngine.Rendering.VertexAttributeFormat,System.Int32,System.Collections.Generic.List`1<T>,System.Int32,System.Int32,UnityEngine.Rendering.MeshUpdateFlags)
// 0x0000027D System.Void UnityEngine.Mesh::SetListForChannel(UnityEngine.Rendering.VertexAttribute,System.Collections.Generic.List`1<T>,System.Int32,System.Int32,UnityEngine.Rendering.MeshUpdateFlags)
// 0x0000027E UnityEngine.Vector3[] UnityEngine.Mesh::get_vertices()
extern void Mesh_get_vertices_mB7A79698792B3CBA0E7E6EACDA6C031E496FB595 (void);
// 0x0000027F System.Void UnityEngine.Mesh::set_vertices(UnityEngine.Vector3[])
extern void Mesh_set_vertices_m38F0908D0FDFE484BE19E94BE9D6176667469AAD (void);
// 0x00000280 UnityEngine.Vector3[] UnityEngine.Mesh::get_normals()
extern void Mesh_get_normals_m5212279CEF7538618C8BA884C9A7B976B32352B0 (void);
// 0x00000281 System.Void UnityEngine.Mesh::set_normals(UnityEngine.Vector3[])
extern void Mesh_set_normals_m3D06E214B63B49788710672B71C99F2365A83130 (void);
// 0x00000282 UnityEngine.Vector4[] UnityEngine.Mesh::get_tangents()
extern void Mesh_get_tangents_m278A41721D47A627367F3F8E2B722B80A949A0F3 (void);
// 0x00000283 System.Void UnityEngine.Mesh::set_tangents(UnityEngine.Vector4[])
extern void Mesh_set_tangents_mFA4E0A26B52C1FCF80FA5DA642B28716249ACF67 (void);
// 0x00000284 UnityEngine.Vector2[] UnityEngine.Mesh::get_uv()
extern void Mesh_get_uv_m3FF0C231402D4106CDA3EEE381B16863B287D143 (void);
// 0x00000285 System.Void UnityEngine.Mesh::set_uv(UnityEngine.Vector2[])
extern void Mesh_set_uv_mF6FED6DDACBAE3EAF28BFBF257A0D5356FCF3AAC (void);
// 0x00000286 UnityEngine.Vector2[] UnityEngine.Mesh::get_uv2()
extern void Mesh_get_uv2_m03E41FCB77DA4290C9CA70E11E9C0125F68D505E (void);
// 0x00000287 System.Void UnityEngine.Mesh::set_uv2(UnityEngine.Vector2[])
extern void Mesh_set_uv2_mE60F42676D5CD294524617262BABEB81B9FB8F22 (void);
// 0x00000288 UnityEngine.Vector2[] UnityEngine.Mesh::get_uv3()
extern void Mesh_get_uv3_m59B9F0B319EEE7CCED6A15D3AF8A4C40C7280DF6 (void);
// 0x00000289 UnityEngine.Vector2[] UnityEngine.Mesh::get_uv4()
extern void Mesh_get_uv4_m5177A36A234F21CA1CEE7B30051B47B2FCB4A4A3 (void);
// 0x0000028A UnityEngine.Color32[] UnityEngine.Mesh::get_colors32()
extern void Mesh_get_colors32_m4BD048545AD6BC19E982926AB0C8A1948A82AD32 (void);
// 0x0000028B System.Void UnityEngine.Mesh::set_colors32(UnityEngine.Color32[])
extern void Mesh_set_colors32_m0EA838C15BBD01710A4EE1A475DB585EB9B5BD91 (void);
// 0x0000028C System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void Mesh_SetVertices_m08C90A1665735C09E15E17DE1A8CD9F196762BCD (void);
// 0x0000028D System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32,System.Int32)
extern void Mesh_SetVertices_m0603941E5ACFAEC45C95FE8658C41E196BE8164E (void);
// 0x0000028E System.Void UnityEngine.Mesh::SetVertices(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32,System.Int32,UnityEngine.Rendering.MeshUpdateFlags)
extern void Mesh_SetVertices_m26E8F8BCF660FBCFFC512FF683A7FBB8FEA32214 (void);
// 0x0000028F System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void Mesh_SetNormals_m10B6C93B59F4BC8F5D959CD79494F3FCDB67B168 (void);
// 0x00000290 System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32,System.Int32)
extern void Mesh_SetNormals_m8E810FA8F3EF65B047AE7C11B8E428FE9F1250BD (void);
// 0x00000291 System.Void UnityEngine.Mesh::SetNormals(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Int32,System.Int32,UnityEngine.Rendering.MeshUpdateFlags)
extern void Mesh_SetNormals_mF37082B29FFB07FB5FCBDD842C01620213924E53 (void);
// 0x00000292 System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>)
extern void Mesh_SetTangents_m728C5E61FD6656209686AE3F6734686A2F4E549E (void);
// 0x00000293 System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Int32,System.Int32)
extern void Mesh_SetTangents_m273D26B0AB58BD29EEAE0CA31D497430A8873D66 (void);
// 0x00000294 System.Void UnityEngine.Mesh::SetTangents(System.Collections.Generic.List`1<UnityEngine.Vector4>,System.Int32,System.Int32,UnityEngine.Rendering.MeshUpdateFlags)
extern void Mesh_SetTangents_mC172BCA3C194ADBF1B565DEC54CD884EB63B1D8C (void);
// 0x00000295 System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>)
extern void Mesh_SetColors_m3A1D5B4986EC06E3930617D45A88BA768072FA2F (void);
// 0x00000296 System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>,System.Int32,System.Int32)
extern void Mesh_SetColors_m1A9D32B21B621A3C246BEBCD842127123718303A (void);
// 0x00000297 System.Void UnityEngine.Mesh::SetColors(System.Collections.Generic.List`1<UnityEngine.Color32>,System.Int32,System.Int32,UnityEngine.Rendering.MeshUpdateFlags)
extern void Mesh_SetColors_mF89C9599491ACBAEBB2F3C8D92A8192B3F9B55CF (void);
// 0x00000298 System.Void UnityEngine.Mesh::SetUvsImpl(System.Int32,System.Int32,System.Collections.Generic.List`1<T>,System.Int32,System.Int32,UnityEngine.Rendering.MeshUpdateFlags)
// 0x00000299 System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>)
extern void Mesh_SetUVs_mDDD16F3EC3434233E34F18221FFD47F8AEA7BCCE (void);
// 0x0000029A System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Int32,System.Int32)
extern void Mesh_SetUVs_m5CE63A380341B88A588D73B4240A2F79B3A1776F (void);
// 0x0000029B System.Void UnityEngine.Mesh::SetUVs(System.Int32,System.Collections.Generic.List`1<UnityEngine.Vector2>,System.Int32,System.Int32,UnityEngine.Rendering.MeshUpdateFlags)
extern void Mesh_SetUVs_mC431E98259699C3524EEEF58E491F3053ECE14F3 (void);
// 0x0000029C System.Void UnityEngine.Mesh::PrintErrorCantAccessIndices()
extern void Mesh_PrintErrorCantAccessIndices_m2416235DD4E4CB1E5B5124480185157C9599A2B6 (void);
// 0x0000029D System.Boolean UnityEngine.Mesh::CheckCanAccessSubmesh(System.Int32,System.Boolean)
extern void Mesh_CheckCanAccessSubmesh_m639D3AFE8E9A6A8D6113897D6628BCB8D9851470 (void);
// 0x0000029E System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshTriangles(System.Int32)
extern void Mesh_CheckCanAccessSubmeshTriangles_m3FA6D53E009E173066F08968A34C810A968CD354 (void);
// 0x0000029F System.Boolean UnityEngine.Mesh::CheckCanAccessSubmeshIndices(System.Int32)
extern void Mesh_CheckCanAccessSubmeshIndices_m4C72E5C166B623591E599D0A5E93A7BF44E6DD52 (void);
// 0x000002A0 System.Int32[] UnityEngine.Mesh::get_triangles()
extern void Mesh_get_triangles_mC599119151146317136B1E4C40A9110373286D5A (void);
// 0x000002A1 System.Void UnityEngine.Mesh::set_triangles(System.Int32[])
extern void Mesh_set_triangles_mF1D92E67523CD5FDC66A4378FC4AD8D4AD0D5FEC (void);
// 0x000002A2 System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32)
extern void Mesh_GetIndices_m8C8D25ABFA9D8A7AE23DAEB6FD7142E6BB46C49D (void);
// 0x000002A3 System.Int32[] UnityEngine.Mesh::GetIndices(System.Int32,System.Boolean)
extern void Mesh_GetIndices_m716824BAD490CC3818631A2A4476DD174EF995C9 (void);
// 0x000002A4 System.Void UnityEngine.Mesh::CheckIndicesArrayRange(System.Int32,System.Int32,System.Int32)
extern void Mesh_CheckIndicesArrayRange_mEB6E1B2EE3E56B1D802F0C7EF2A7A9D15E32F5D6 (void);
// 0x000002A5 System.Void UnityEngine.Mesh::SetTrianglesImpl(System.Int32,UnityEngine.Rendering.IndexFormat,System.Array,System.Int32,System.Int32,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetTrianglesImpl_m996CAEDB2B197F72F41C865759FD21C3E9AB6964 (void);
// 0x000002A6 System.Void UnityEngine.Mesh::SetTriangles(System.Int32[],System.Int32)
extern void Mesh_SetTriangles_m63AC07691EC9F9AE0C85FB01A8DDD1A45FE9349F (void);
// 0x000002A7 System.Void UnityEngine.Mesh::SetTriangles(System.Int32[],System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetTriangles_m2097A43D5983A1A17C49BA59127EA59744B58541 (void);
// 0x000002A8 System.Void UnityEngine.Mesh::SetTriangles(System.Int32[],System.Int32,System.Int32,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetTriangles_m5D89367A7503882DED6AE15E5D2D665DC08D82D3 (void);
// 0x000002A9 System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32)
extern void Mesh_SetTriangles_mF74536E3A39AECF33809A7D23AFF54A1CFC37129 (void);
// 0x000002AA System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetTriangles_m9DA501E911C965F2FFFAF44F8956222E86DCB71E (void);
// 0x000002AB System.Void UnityEngine.Mesh::SetTriangles(System.Collections.Generic.List`1<System.Int32>,System.Int32,System.Int32,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetTriangles_m8F0B711A6E0FFCB6DE06943F24D12019B3100EB4 (void);
// 0x000002AC System.Void UnityEngine.Mesh::SetIndices(System.Int32[],UnityEngine.MeshTopology,System.Int32)
extern void Mesh_SetIndices_mCD0377083E978A3FF806CFCCD28410C042A77ECD (void);
// 0x000002AD System.Void UnityEngine.Mesh::SetIndices(System.Int32[],UnityEngine.MeshTopology,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetIndices_m6269D5A9E751E7DFA21248CAD177270989224770 (void);
// 0x000002AE System.Void UnityEngine.Mesh::SetIndices(System.Int32[],System.Int32,System.Int32,UnityEngine.MeshTopology,System.Int32,System.Boolean,System.Int32)
extern void Mesh_SetIndices_m87CA046F5A408FAE35E6C2645C86683D91D5D725 (void);
// 0x000002AF System.Void UnityEngine.Mesh::Clear(System.Boolean)
extern void Mesh_Clear_mD35FF3850B83B635DA849033E25D0D718E34D92B (void);
// 0x000002B0 System.Void UnityEngine.Mesh::Clear()
extern void Mesh_Clear_m7500ECE6209E14CC750CB16B48301B8D2A57ACCE (void);
// 0x000002B1 System.Void UnityEngine.Mesh::RecalculateBounds()
extern void Mesh_RecalculateBounds_mC39556595CFE3E4D8EFA777476ECD22B97FC2737 (void);
// 0x000002B2 System.Void UnityEngine.Mesh::RecalculateNormals()
extern void Mesh_RecalculateNormals_mEBF9ED932D0B463E4EF3947D232CC8BEECAE1A4A (void);
// 0x000002B3 System.Void UnityEngine.Mesh::RecalculateBounds(UnityEngine.Rendering.MeshUpdateFlags)
extern void Mesh_RecalculateBounds_m30A0A5900837569C4016A1FE2CEC0BB3E50CC43A (void);
// 0x000002B4 System.Void UnityEngine.Mesh::RecalculateNormals(UnityEngine.Rendering.MeshUpdateFlags)
extern void Mesh_RecalculateNormals_mACA7E88F9EE957C64005597F5E83F8EFAFA7A8D9 (void);
// 0x000002B5 System.Void UnityEngine.Mesh::MarkDynamic()
extern void Mesh_MarkDynamic_m11FFDC281C64F11C36EDDA47BC132EAC95082999 (void);
// 0x000002B6 System.Void UnityEngine.Mesh::UploadMeshData(System.Boolean)
extern void Mesh_UploadMeshData_m3745185BFF4D9B970DEB23EEB6FD93DCAFFA8C07 (void);
// 0x000002B7 System.Void UnityEngine.Mesh::get_bounds_Injected(UnityEngine.Bounds&)
extern void Mesh_get_bounds_Injected_m1EFC3F0977E82DD74B76F15661C562EE48695140 (void);
// 0x000002B8 System.Void UnityEngine.Mesh::set_bounds_Injected(UnityEngine.Bounds&)
extern void Mesh_set_bounds_Injected_m4DE5BB71883B1FC4CBD3299AF52FB299219FA6B7 (void);
// 0x000002B9 System.Void UnityEngine.Texture::.ctor()
extern void Texture__ctor_mA6FE9CC0AF05A99FADCEF0BED2FB0C95571AAF4A (void);
// 0x000002BA System.Int32 UnityEngine.Texture::get_mipmapCount()
extern void Texture_get_mipmapCount_mC54E39023B16EDF24FF5635CFC1D2C88507660AB (void);
// 0x000002BB System.Int32 UnityEngine.Texture::GetDataWidth()
extern void Texture_GetDataWidth_m5EE88F5417E01649909C3E11408491DB88AA9442 (void);
// 0x000002BC System.Int32 UnityEngine.Texture::GetDataHeight()
extern void Texture_GetDataHeight_m1DFF41FBC7542D2CDB0247CF02A0FE0ACB60FB99 (void);
// 0x000002BD UnityEngine.Rendering.TextureDimension UnityEngine.Texture::GetDimension()
extern void Texture_GetDimension_mFC3578580420BC64C4A877E0F77CBD3868B03668 (void);
// 0x000002BE System.Int32 UnityEngine.Texture::get_width()
extern void Texture_get_width_m98E7185116DB24A73E1647878013B023FF275B98 (void);
// 0x000002BF System.Void UnityEngine.Texture::set_width(System.Int32)
extern void Texture_set_width_m6BCD23D97A9883DE0FB34E6FF48883F6C09D9F8F (void);
// 0x000002C0 System.Int32 UnityEngine.Texture::get_height()
extern void Texture_get_height_m3D849F551F396027D4483C9B9FFF31F8AF4533B6 (void);
// 0x000002C1 System.Void UnityEngine.Texture::set_height(System.Int32)
extern void Texture_set_height_mAC3CA245CB260972C0537919C451DBF3BA1A4554 (void);
// 0x000002C2 UnityEngine.Rendering.TextureDimension UnityEngine.Texture::get_dimension()
extern void Texture_get_dimension_m32A86E516E1C47335027FF586B54DE992EF69EFD (void);
// 0x000002C3 System.Boolean UnityEngine.Texture::get_isReadable()
extern void Texture_get_isReadable_mF9C36F23F3632802946D4BCBA6FE3D27098407BC (void);
// 0x000002C4 UnityEngine.TextureWrapMode UnityEngine.Texture::get_wrapMode()
extern void Texture_get_wrapMode_mB442135F226C399108A5805A6B82845EC0362BA9 (void);
// 0x000002C5 System.Void UnityEngine.Texture::set_wrapMode(UnityEngine.TextureWrapMode)
extern void Texture_set_wrapMode_m1233D2DF48DC20996F8EE26E866D4BDD2AC8050D (void);
// 0x000002C6 UnityEngine.FilterMode UnityEngine.Texture::get_filterMode()
extern void Texture_get_filterMode_m66AAEC6C44B0A3A82477A27E5061AF6008501F9E (void);
// 0x000002C7 System.Void UnityEngine.Texture::set_filterMode(UnityEngine.FilterMode)
extern void Texture_set_filterMode_m045141DB0FEFE496885D45F5F23B15BC0E77C8D0 (void);
// 0x000002C8 System.Void UnityEngine.Texture::set_anisoLevel(System.Int32)
extern void Texture_set_anisoLevel_mE51360F6CD0562FD6355F8C0509B70A454CB33BE (void);
// 0x000002C9 System.Void UnityEngine.Texture::set_mipMapBias(System.Single)
extern void Texture_set_mipMapBias_m5057ADA9DBA8E8E30EC15EA5BABFA0734CE2821C (void);
// 0x000002CA UnityEngine.Vector2 UnityEngine.Texture::get_texelSize()
extern void Texture_get_texelSize_m804B471337C8AF2334FF12FA2CC6198EFD7EB5EB (void);
// 0x000002CB System.Int32 UnityEngine.Texture::Internal_GetActiveTextureColorSpace()
extern void Texture_Internal_GetActiveTextureColorSpace_m5D0FE0578B76D37F863DB9FDC8BD0608467EE59D (void);
// 0x000002CC UnityEngine.ColorSpace UnityEngine.Texture::get_activeTextureColorSpace()
extern void Texture_get_activeTextureColorSpace_m0553908E0813E6ABD035A2453AA073BADB5680F2 (void);
// 0x000002CD System.Boolean UnityEngine.Texture::ValidateFormat(UnityEngine.TextureFormat)
extern void Texture_ValidateFormat_mC3C7A3FE51CA18357ABE027958BF97D3C6675D39 (void);
// 0x000002CE System.Boolean UnityEngine.Texture::ValidateFormat(UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.FormatUsage)
extern void Texture_ValidateFormat_mB721DB544C78FC025FC3D3F85AD705D54F1B52CE (void);
// 0x000002CF UnityEngine.UnityException UnityEngine.Texture::CreateNonReadableException(UnityEngine.Texture)
extern void Texture_CreateNonReadableException_m5BFE30599C50688EEDE149FB1CEF834BE1633306 (void);
// 0x000002D0 System.Void UnityEngine.Texture::.cctor()
extern void Texture__cctor_m6474E45E076B9A176073F458843BAC631033F2B7 (void);
// 0x000002D1 System.Void UnityEngine.Texture::get_texelSize_Injected(UnityEngine.Vector2&)
extern void Texture_get_texelSize_Injected_mE4C2F32E9803126870079BDF7EB701CDD19910E2 (void);
// 0x000002D2 UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
extern void Texture2D_get_format_mCBCE13524A94042693822BDDE112990B25F4F8E4 (void);
// 0x000002D3 UnityEngine.Texture2D UnityEngine.Texture2D::get_whiteTexture()
extern void Texture2D_get_whiteTexture_m4ED96995BA1D42F7D2823BD9D18023CFE3C680A0 (void);
// 0x000002D4 System.Void UnityEngine.Texture2D::Compress(System.Boolean)
extern void Texture2D_Compress_m9EEAE939AF8538D56FA39D03FAA5AD6DA7EC5D60 (void);
// 0x000002D5 System.Boolean UnityEngine.Texture2D::Internal_CreateImpl(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Texture2D_Internal_CreateImpl_m48CD1B0F76E8671515956DFA43329604E29EB7B3 (void);
// 0x000002D6 System.Void UnityEngine.Texture2D::Internal_Create(UnityEngine.Texture2D,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Texture2D_Internal_Create_mEAA34D0081C646C185D322FDE203E5C6C7B05800 (void);
// 0x000002D7 System.Boolean UnityEngine.Texture2D::get_isReadable()
extern void Texture2D_get_isReadable_mD31C50788F7268E65EE9DA611B6F66199AA9D109 (void);
// 0x000002D8 System.Void UnityEngine.Texture2D::ApplyImpl(System.Boolean,System.Boolean)
extern void Texture2D_ApplyImpl_mC56607643B71223E3294F6BA352A5538FCC5915C (void);
// 0x000002D9 System.Boolean UnityEngine.Texture2D::ResizeImpl(System.Int32,System.Int32)
extern void Texture2D_ResizeImpl_m484CD2126E70AA80396A6F7C1539A72CC03BA71C (void);
// 0x000002DA System.Void UnityEngine.Texture2D::SetPixelImpl(System.Int32,System.Int32,System.Int32,UnityEngine.Color)
extern void Texture2D_SetPixelImpl_m9790950013B3DF46008381D971548B82C0378D91 (void);
// 0x000002DB UnityEngine.Color UnityEngine.Texture2D::GetPixelImpl(System.Int32,System.Int32,System.Int32)
extern void Texture2D_GetPixelImpl_m2224B987F48D881F71083B9472DE5DD11580977B (void);
// 0x000002DC UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinearImpl(System.Int32,System.Single,System.Single)
extern void Texture2D_GetPixelBilinearImpl_m688F5C550710DA1B1ECBE38C1354B0A15C89778E (void);
// 0x000002DD System.Boolean UnityEngine.Texture2D::ResizeWithFormatImpl(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Boolean)
extern void Texture2D_ResizeWithFormatImpl_m0A2C1EDF44282998B107D541509BD196AEBF65C6 (void);
// 0x000002DE System.Void UnityEngine.Texture2D::ReadPixelsImpl(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern void Texture2D_ReadPixelsImpl_m8C1FDBC0E9EA531BF107C9A60F931B15910BE260 (void);
// 0x000002DF System.Void UnityEngine.Texture2D::SetPixelsImpl(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32,System.Int32)
extern void Texture2D_SetPixelsImpl_m6F5B06278B956BFCCF360B135F73B19D4648AE92 (void);
// 0x000002E0 System.Void UnityEngine.Texture2D::SetAllPixels32(UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetAllPixels32_mC1C1E76040E72CAFB60D3CA3F2B95A92620F4A46 (void);
// 0x000002E1 System.Void UnityEngine.Texture2D::SetBlockOfPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetBlockOfPixels32_mFCEE9885A7D2E79C14F4840AF7FF03E9AACF468B (void);
// 0x000002E2 UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Texture2D_GetPixels_m63492D1225FC7E937BBF236538510E29B5866BEB (void);
// 0x000002E3 UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Texture2D_GetPixels_m677BB0ECBC49B9BAC6C3AE78A916AFF126C62CD3 (void);
// 0x000002E4 UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32(System.Int32)
extern void Texture2D_GetPixels32_mA4E2C09B4077716ECEFC0162ABEB8C3A66F623FA (void);
// 0x000002E5 UnityEngine.Color32[] UnityEngine.Texture2D::GetPixels32()
extern void Texture2D_GetPixels32_m419F7BF2D2D374C08247BE66838148DA485A6ECA (void);
// 0x000002E6 UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32,System.Boolean)
extern void Texture2D_PackTextures_m7946134DE42C42397876C7A67BA9FF755030DA1E (void);
// 0x000002E7 UnityEngine.Rect[] UnityEngine.Texture2D::PackTextures(UnityEngine.Texture2D[],System.Int32,System.Int32)
extern void Texture2D_PackTextures_m77163B065DB65C86DD0DC579D809EB3B67784791 (void);
// 0x000002E8 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean,System.IntPtr)
extern void Texture2D__ctor_mF706AD5FFC4EC2805E746C80630D1255A8867004 (void);
// 0x000002E9 System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern void Texture2D__ctor_m667452FB4794C77D283037E096FE0DC0AEB311F3 (void);
// 0x000002EA System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092 (void);
// 0x000002EB System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32)
extern void Texture2D__ctor_m7D64AB4C55A01F2EE57483FD9EF826607DF9E4B4 (void);
// 0x000002EC System.Void UnityEngine.Texture2D::SetPixel(System.Int32,System.Int32,UnityEngine.Color)
extern void Texture2D_SetPixel_m78878905E58C5DE9BCFED8D9262D025789E22F92 (void);
// 0x000002ED System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[],System.Int32)
extern void Texture2D_SetPixels_m39DFC67A52779E657C4B3AFA69FC586E2DB6AB50 (void);
// 0x000002EE System.Void UnityEngine.Texture2D::SetPixels(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color[])
extern void Texture2D_SetPixels_m802BA835119C0F93478BBA752BA23192013EA4F7 (void);
// 0x000002EF System.Void UnityEngine.Texture2D::SetPixels(UnityEngine.Color[])
extern void Texture2D_SetPixels_m5FBA81041D65F8641C3107195D390EE65467FB4F (void);
// 0x000002F0 UnityEngine.Color UnityEngine.Texture2D::GetPixel(System.Int32,System.Int32)
extern void Texture2D_GetPixel_m50474A401DE4CB3B567F1695546DF1D2C610A022 (void);
// 0x000002F1 UnityEngine.Color UnityEngine.Texture2D::GetPixelBilinear(System.Single,System.Single)
extern void Texture2D_GetPixelBilinear_mE25550DD7E9FD26DA7CB1E38FFCA2101F9D3D28D (void);
// 0x000002F2 System.Void UnityEngine.Texture2D::Apply(System.Boolean,System.Boolean)
extern void Texture2D_Apply_m83460E7B5610A6D85DD3CCA71CC5D4523390D660 (void);
// 0x000002F3 System.Void UnityEngine.Texture2D::Apply(System.Boolean)
extern void Texture2D_Apply_mA7D80A8D5DBA5A9334508F23EAEFC6E9C7019CB6 (void);
// 0x000002F4 System.Void UnityEngine.Texture2D::Apply()
extern void Texture2D_Apply_m3BB3975288119BA62ED9BE4243F7767EC2F88CA0 (void);
// 0x000002F5 System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32)
extern void Texture2D_Resize_m3B472A6ED37D683DC4162504F6DCF42E1FA2195C (void);
// 0x000002F6 System.Boolean UnityEngine.Texture2D::Resize(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture2D_Resize_m051609799A8F11F4E344C099DC81A50B69528A6B (void);
// 0x000002F7 System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32,System.Boolean)
extern void Texture2D_ReadPixels_m87ACCC9FDCD8FC8851AE8D3BE56A7C2CAF09C75E (void);
// 0x000002F8 System.Void UnityEngine.Texture2D::ReadPixels(UnityEngine.Rect,System.Int32,System.Int32)
extern void Texture2D_ReadPixels_m4C6FE8C2798C39C20A14DAFC963C720D17F4F987 (void);
// 0x000002F9 System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetPixels32_mBDD42381EB43E024214D81792B0A96D952911D4F (void);
// 0x000002FA System.Void UnityEngine.Texture2D::SetPixels32(UnityEngine.Color32[])
extern void Texture2D_SetPixels32_m6C2602EBE75F9C70DBC36D0B67EA4C12638518BB (void);
// 0x000002FB System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[],System.Int32)
extern void Texture2D_SetPixels32_m5C3B90CC4B9E104C5E278176E119945C353852D9 (void);
// 0x000002FC System.Void UnityEngine.Texture2D::SetPixels32(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Color32[])
extern void Texture2D_SetPixels32_m80C3D047272131066BA3B918855264376D2D1407 (void);
// 0x000002FD UnityEngine.Color[] UnityEngine.Texture2D::GetPixels(System.Int32)
extern void Texture2D_GetPixels_mDBE68956E50997CB02CB0419318E0D19493288A6 (void);
// 0x000002FE UnityEngine.Color[] UnityEngine.Texture2D::GetPixels()
extern void Texture2D_GetPixels_m702E1E59DE60A5A11197DA3F6474F9E6716D9699 (void);
// 0x000002FF System.Void UnityEngine.Texture2D::SetPixelImpl_Injected(System.Int32,System.Int32,System.Int32,UnityEngine.Color&)
extern void Texture2D_SetPixelImpl_Injected_mF0EB1B200D5BB3B2BC3BB50C708901785C1E1772 (void);
// 0x00000300 System.Void UnityEngine.Texture2D::GetPixelImpl_Injected(System.Int32,System.Int32,System.Int32,UnityEngine.Color&)
extern void Texture2D_GetPixelImpl_Injected_mFD898C97E14FC52C38BA6B42BE88C762F6BB2082 (void);
// 0x00000301 System.Void UnityEngine.Texture2D::GetPixelBilinearImpl_Injected(System.Int32,System.Single,System.Single,UnityEngine.Color&)
extern void Texture2D_GetPixelBilinearImpl_Injected_m378D7A9BC9E6079B59950C664419E04FB1E894FE (void);
// 0x00000302 System.Void UnityEngine.Texture2D::ReadPixelsImpl_Injected(UnityEngine.Rect&,System.Int32,System.Int32,System.Boolean)
extern void Texture2D_ReadPixelsImpl_Injected_mEC297796C9AA8F8A08551EEDBB144C95EA3F560C (void);
// 0x00000303 System.Boolean UnityEngine.Cubemap::Internal_CreateImpl(UnityEngine.Cubemap,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Cubemap_Internal_CreateImpl_m05F9BCB0CFD280E2121062B6F8DA9467789B27A2 (void);
// 0x00000304 System.Void UnityEngine.Cubemap::Internal_Create(UnityEngine.Cubemap,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.IntPtr)
extern void Cubemap_Internal_Create_m1647AD0EFBE0E383F5CDF6ABBC51842959E61931 (void);
// 0x00000305 System.Boolean UnityEngine.Cubemap::get_isReadable()
extern void Cubemap_get_isReadable_m169779CDA9E9AF98599E60A47C665E68B3256F5D (void);
// 0x00000306 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Cubemap__ctor_mBC1AD85DB40124D557615D0B391CACCDF76F1579 (void);
// 0x00000307 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Cubemap__ctor_mEC3B9661FA3DB1DFF54C7A3F4FEA41903EFE2C11 (void);
// 0x00000308 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Int32)
extern void Cubemap__ctor_mDEB5F10F08868EEBBF7891F00EAB793F4C5498FB (void);
// 0x00000309 System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void Cubemap__ctor_mF667EBD2A38E2D5DDBD46BC80ABF2DCE97A9411B (void);
// 0x0000030A System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Int32,System.IntPtr)
extern void Cubemap__ctor_mD760725AC038C20E54F8EC514DA075DFF97A8B56 (void);
// 0x0000030B System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Boolean,System.IntPtr)
extern void Cubemap__ctor_m766D71B1731BAD5C01FAEA35A73D1980C6CAE57B (void);
// 0x0000030C System.Void UnityEngine.Cubemap::.ctor(System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Cubemap__ctor_m3F121EC86FF615007F0BB6FD8162779EE7D19037 (void);
// 0x0000030D System.Boolean UnityEngine.Texture3D::get_isReadable()
extern void Texture3D_get_isReadable_mFE7B549E8E368B00CEAB4A297106AB135FA6E962 (void);
// 0x0000030E System.Boolean UnityEngine.Texture3D::Internal_CreateImpl(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture3D_Internal_CreateImpl_m41E3DE82535685BDE643C82AB65722CCB98E7C20 (void);
// 0x0000030F System.Void UnityEngine.Texture3D::Internal_Create(UnityEngine.Texture3D,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture3D_Internal_Create_m65B50261584E685D8375F5398311C549271ED1B6 (void);
// 0x00000310 System.Void UnityEngine.Texture3D::ApplyImpl(System.Boolean,System.Boolean)
extern void Texture3D_ApplyImpl_m53DBD3A0ACF34C4300F3200466F843F4E2331100 (void);
// 0x00000311 System.Void UnityEngine.Texture3D::SetPixels32(UnityEngine.Color32[],System.Int32)
extern void Texture3D_SetPixels32_m7BC6152A23FBF8D2D1D79FED26E76B4530404E00 (void);
// 0x00000312 System.Void UnityEngine.Texture3D::SetPixels32(UnityEngine.Color32[])
extern void Texture3D_SetPixels32_mEA63C4C5FA84F5CA5D7635DF9CDD987946088770 (void);
// 0x00000313 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture3D__ctor_mA422DEB7F88AA34806E6AA2D91258AA093F3C3AE (void);
// 0x00000314 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture3D__ctor_m666A8D01B0E3B7773C7CDAB624D69E16331CFA36 (void);
// 0x00000315 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void Texture3D__ctor_m6DC8372EBD1146127A4CE86F3E65930ABAB6539D (void);
// 0x00000316 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32)
extern void Texture3D__ctor_m7AE9A6F7E67FE89DEA087647FB3375343D997F4C (void);
// 0x00000317 System.Void UnityEngine.Texture3D::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture3D__ctor_m2B875ADAA935AC50C758ECEBA69F13172FD620FC (void);
// 0x00000318 System.Void UnityEngine.Texture3D::Apply(System.Boolean,System.Boolean)
extern void Texture3D_Apply_mA27E8CD29F3236BE67DA1E35298966976F3B27A5 (void);
// 0x00000319 System.Void UnityEngine.Texture3D::Apply()
extern void Texture3D_Apply_m83FB1F0D98CA327EF6263CF616BB05BBA0D45860 (void);
// 0x0000031A System.Int32 UnityEngine.Texture2DArray::get_allSlices()
extern void Texture2DArray_get_allSlices_mB49C66E3148B0002D15D4E6E908B1FD600263FDA (void);
// 0x0000031B System.Int32 UnityEngine.Texture2DArray::get_depth()
extern void Texture2DArray_get_depth_mE0A5F5C0CCA63B07B290EC0BA645A52C4C8FD272 (void);
// 0x0000031C UnityEngine.TextureFormat UnityEngine.Texture2DArray::get_format()
extern void Texture2DArray_get_format_m85F541FCF79D18F05F77DD792904F4D101706829 (void);
// 0x0000031D System.Boolean UnityEngine.Texture2DArray::get_isReadable()
extern void Texture2DArray_get_isReadable_m7676C4021DD3D435A3D2655A34C7A1FCCA00C042 (void);
// 0x0000031E System.Boolean UnityEngine.Texture2DArray::Internal_CreateImpl(UnityEngine.Texture2DArray,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray_Internal_CreateImpl_m5B8B806393D443E6F0CB49AB019C8E9A1C8644B1 (void);
// 0x0000031F System.Void UnityEngine.Texture2DArray::Internal_Create(UnityEngine.Texture2DArray,System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray_Internal_Create_m5DD4264F3965FBE126FAA447C79876C22D36D39C (void);
// 0x00000320 System.Void UnityEngine.Texture2DArray::ApplyImpl(System.Boolean,System.Boolean)
extern void Texture2DArray_ApplyImpl_m7D9288584A5198386367A6158D05DCEEC3EC5D10 (void);
// 0x00000321 System.Void UnityEngine.Texture2DArray::SetPixels32(UnityEngine.Color32[],System.Int32,System.Int32)
extern void Texture2DArray_SetPixels32_mC7955F5E0B35805E7C805449582D4C59238674F6 (void);
// 0x00000322 System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray__ctor_mAE2D5B259CE352E6F4F10A28FDDCE52B771672B2 (void);
// 0x00000323 System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void Texture2DArray__ctor_m139056CD509EAC819F9713F6A2CAE801D49CA13F (void);
// 0x00000324 System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void Texture2DArray__ctor_mB19D8E34783F95713A23A0F06F63EF1B1613E9C5 (void);
// 0x00000325 System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean)
extern void Texture2DArray__ctor_mEE6D4AD1D7469894FA16627A222EDC34647F6DB3 (void);
// 0x00000326 System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern void Texture2DArray__ctor_m4772A79C577E6E246301F31D86FE6F150B1B68E2 (void);
// 0x00000327 System.Void UnityEngine.Texture2DArray::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void Texture2DArray__ctor_mED6E22E57F51628D68F219E5C01FF01A265CE386 (void);
// 0x00000328 System.Void UnityEngine.Texture2DArray::Apply(System.Boolean,System.Boolean)
extern void Texture2DArray_Apply_mA7130B96298181F5B22A93692509D7BF1A575C2F (void);
// 0x00000329 System.Boolean UnityEngine.CubemapArray::get_isReadable()
extern void CubemapArray_get_isReadable_m8948D737E2D417F489BCFF3C5CA87B4D536A8F44 (void);
// 0x0000032A System.Boolean UnityEngine.CubemapArray::Internal_CreateImpl(UnityEngine.CubemapArray,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray_Internal_CreateImpl_mC6544EE7BDDE76EC9B3B8703CB13B08497921994 (void);
// 0x0000032B System.Void UnityEngine.CubemapArray::Internal_Create(UnityEngine.CubemapArray,System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray_Internal_Create_mC44055052CD006718B5C1964110B692B30DE1F1F (void);
// 0x0000032C System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray__ctor_mAEA6A6E06CDE3F825976EFA242AAE00AE41C0247 (void);
// 0x0000032D System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags)
extern void CubemapArray__ctor_mB5CEC4BD06765D072E96B663B4E9E09E0DCCEE17 (void);
// 0x0000032E System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.TextureCreationFlags,System.Int32)
extern void CubemapArray__ctor_mD891BB1565BECEEEDFB5F12EE3C64C34A8A93B78 (void);
// 0x0000032F System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Boolean)
extern void CubemapArray__ctor_m696D238938D8A70B273DE5D05F60C8C4834506D8 (void);
// 0x00000330 System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean)
extern void CubemapArray__ctor_m593EF9F31E1FC6357957584ACD550B434D4C9563 (void);
// 0x00000331 System.Void UnityEngine.CubemapArray::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
extern void CubemapArray__ctor_mCEB65D7A82E8C98530D970424F4B125E35A03205 (void);
// 0x00000332 System.Int32 UnityEngine.RenderTexture::get_width()
extern void RenderTexture_get_width_m7E2915C08E3B59DE86707FAF9990A73AD62609BA (void);
// 0x00000333 System.Void UnityEngine.RenderTexture::set_width(System.Int32)
extern void RenderTexture_set_width_m24E7C6AD852FA9DB089253755A450E1FC53F5CC5 (void);
// 0x00000334 System.Int32 UnityEngine.RenderTexture::get_height()
extern void RenderTexture_get_height_m2A29272DA6BAFE2051A228B15E3BC4AECD97473D (void);
// 0x00000335 System.Void UnityEngine.RenderTexture::set_height(System.Int32)
extern void RenderTexture_set_height_m131ECC892E6EA0CEA1E656C66862A272FF6F0376 (void);
// 0x00000336 UnityEngine.Rendering.TextureDimension UnityEngine.RenderTexture::get_dimension()
extern void RenderTexture_get_dimension_m597D6E558F9DE585F58AFA983A4AB64421949BF3 (void);
// 0x00000337 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTexture::get_graphicsFormat()
extern void RenderTexture_get_graphicsFormat_m1D7330403A886C0F7F43A0C82CFECFDC56DC6937 (void);
// 0x00000338 System.Void UnityEngine.RenderTexture::set_graphicsFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void RenderTexture_set_graphicsFormat_mA378AAD8BD876EE96637FF0A80A2AFEA4D852FAB (void);
// 0x00000339 System.Boolean UnityEngine.RenderTexture::get_sRGB()
extern void RenderTexture_get_sRGB_m53EE975EDE763A0CC5B2CF5B347D5FC6E1FE3B21 (void);
// 0x0000033A UnityEngine.RenderTextureFormat UnityEngine.RenderTexture::get_format()
extern void RenderTexture_get_format_mB9BBCACA0A809206FA73109ACF2A6976E19DB822 (void);
// 0x0000033B System.Void UnityEngine.RenderTexture::set_isPowerOfTwo(System.Boolean)
extern void RenderTexture_set_isPowerOfTwo_mA6D34F610DF27C40422DF5815892B45C656F1317 (void);
// 0x0000033C UnityEngine.RenderTexture UnityEngine.RenderTexture::GetActive()
extern void RenderTexture_GetActive_m198991C4EAA3A72357ABF3B16C677A4CB0995FFF (void);
// 0x0000033D System.Void UnityEngine.RenderTexture::SetActive(UnityEngine.RenderTexture)
extern void RenderTexture_SetActive_m044CDBE8E6AF70D0B7E03BCC814C02CFDF82FFC0 (void);
// 0x0000033E UnityEngine.RenderTexture UnityEngine.RenderTexture::get_active()
extern void RenderTexture_get_active_mB73718A56673D36F74B5338B310ED7FDFEB34AB7 (void);
// 0x0000033F System.Void UnityEngine.RenderTexture::set_active(UnityEngine.RenderTexture)
extern void RenderTexture_set_active_mA70AFD6D3CB54E9AEDDD45E48B8B6979FDB75ED9 (void);
// 0x00000340 UnityEngine.RenderBuffer UnityEngine.RenderTexture::GetColorBuffer()
extern void RenderTexture_GetColorBuffer_mEA9045FD8EA98B51E7AF4B313B4A82A26EDA68DE (void);
// 0x00000341 UnityEngine.RenderBuffer UnityEngine.RenderTexture::GetDepthBuffer()
extern void RenderTexture_GetDepthBuffer_m49B541537CA0852FFA3337E9012A2108EC3E22CF (void);
// 0x00000342 UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_colorBuffer()
extern void RenderTexture_get_colorBuffer_mB973BA4FAA3141D51E26D15473F5E6F8D2046675 (void);
// 0x00000343 UnityEngine.RenderBuffer UnityEngine.RenderTexture::get_depthBuffer()
extern void RenderTexture_get_depthBuffer_m474407A8697D75FAA2CD2D927DEB7F593C491A04 (void);
// 0x00000344 System.Boolean UnityEngine.RenderTexture::Create()
extern void RenderTexture_Create_m723CBB7B7543E9FAFEBC04E8FDCDF629DA31F411 (void);
// 0x00000345 System.Void UnityEngine.RenderTexture::Release()
extern void RenderTexture_Release_m533506E903688E798921C0D35F1B082522D88986 (void);
// 0x00000346 System.Boolean UnityEngine.RenderTexture::IsCreated()
extern void RenderTexture_IsCreated_m78F28FE94FDA5346E2E8B3AEC0863B8DCF116958 (void);
// 0x00000347 System.Void UnityEngine.RenderTexture::SetSRGBReadWrite(System.Boolean)
extern void RenderTexture_SetSRGBReadWrite_m1C0BEC5511CE36A91F44E1C40AEF2399BA347D14 (void);
// 0x00000348 System.Void UnityEngine.RenderTexture::Internal_Create(UnityEngine.RenderTexture)
extern void RenderTexture_Internal_Create_m6374BF6C59B7A2307975D6D1A70C82859EF0D8F3 (void);
// 0x00000349 System.Void UnityEngine.RenderTexture::SetRenderTextureDescriptor(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture_SetRenderTextureDescriptor_mD39CEA1EAF6810889EDB9D5CE08A84F14706F499 (void);
// 0x0000034A UnityEngine.RenderTextureDescriptor UnityEngine.RenderTexture::GetDescriptor()
extern void RenderTexture_GetDescriptor_mC6D87F96283042B76AA07994AC73E8131FA65F79 (void);
// 0x0000034B UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary_Internal(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture_GetTemporary_Internal_mF3F0B64948F5A615FFF8DD9A3263A2616B9B1D29 (void);
// 0x0000034C System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
extern void RenderTexture_ReleaseTemporary_m2BF2BDDC359A491C05C401B977878DAE1D0850D4 (void);
// 0x0000034D System.Int32 UnityEngine.RenderTexture::get_depth()
extern void RenderTexture_get_depth_m1E64CBB551FF92C496FDE4ED9A11A3937F93BC29 (void);
// 0x0000034E System.Void UnityEngine.RenderTexture::set_depth(System.Int32)
extern void RenderTexture_set_depth_mA57CEFEDDB45D0429FAC9532A631839F523944A3 (void);
// 0x0000034F System.Void UnityEngine.RenderTexture::.ctor()
extern void RenderTexture__ctor_m41C9973A79AF4CAD32E6C8987874BB20C9C87DF7 (void);
// 0x00000350 System.Void UnityEngine.RenderTexture::.ctor(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture__ctor_m96C4C4C7B41EE884420046EFE4B8EC528B10D8BD (void);
// 0x00000351 System.Void UnityEngine.RenderTexture::.ctor(UnityEngine.RenderTexture)
extern void RenderTexture__ctor_m26C29617F265AAA52563A260A5D2EDAAC22CA1C5 (void);
// 0x00000352 System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.DefaultFormat)
extern void RenderTexture__ctor_mE4898D07FB66535165C92D4AA6E37DAC7FF57D50 (void);
// 0x00000353 System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void RenderTexture__ctor_mBE300C716D0DD565F63442E58077515EC82E7BA8 (void);
// 0x00000354 System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32)
extern void RenderTexture__ctor_mBE459F2C0FB9B65A5201F7C646C7EC653408A3D6 (void);
// 0x00000355 System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern void RenderTexture__ctor_m2C35C7AD5162A6CFB7F6CF638B2DAC0DDC9282FD (void);
// 0x00000356 System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern void RenderTexture__ctor_m8E4220FDA652BA3CACE60FBA59D868B921C0F533 (void);
// 0x00000357 System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
extern void RenderTexture__ctor_m5D8D36B284951F95A048C6B9ACA24FC7509564FF (void);
// 0x00000358 System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,System.Int32)
extern void RenderTexture__ctor_m262905210EC882BA3F8B34B322848879561240F6 (void);
// 0x00000359 UnityEngine.RenderTextureDescriptor UnityEngine.RenderTexture::get_descriptor()
extern void RenderTexture_get_descriptor_mBD2530599DF6A24EB0C8F502718B862FC4BF1B9E (void);
// 0x0000035A System.Void UnityEngine.RenderTexture::set_descriptor(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture_set_descriptor_m3C8E31AE4644B23719A12345771D1B85EB6E5881 (void);
// 0x0000035B System.Void UnityEngine.RenderTexture::ValidateRenderTextureDesc(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture_ValidateRenderTextureDesc_m5D363CF342A8C617A326B982D209893F76E30404 (void);
// 0x0000035C UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTexture::GetCompatibleFormat(UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern void RenderTexture_GetCompatibleFormat_m21C46AD608AAA27D85641330E6F273AEF566FFB7 (void);
// 0x0000035D UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(UnityEngine.RenderTextureDescriptor)
extern void RenderTexture_GetTemporary_m7997BAA9A1DFE4D9D1B9F5047ECEE8464835B8DF (void);
// 0x0000035E UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporaryImpl(System.Int32,System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32,UnityEngine.RenderTextureMemoryless,UnityEngine.VRTextureUsage,System.Boolean)
extern void RenderTexture_GetTemporaryImpl_mEA42AB2F6F4F97F0ECA712E82CB2C6C05A515A58 (void);
// 0x0000035F UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern void RenderTexture_GetTemporary_mFDF23E91A85221C7EF61B0A5D46AAC858816E5F1 (void);
// 0x00000360 UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32,System.Int32,UnityEngine.RenderTextureFormat)
extern void RenderTexture_GetTemporary_mEA38E780ED9C1E065B97E85BBC63F8DE548C7B8E (void);
// 0x00000361 System.Void UnityEngine.RenderTexture::GetColorBuffer_Injected(UnityEngine.RenderBuffer&)
extern void RenderTexture_GetColorBuffer_Injected_m2A29CF16C78914B733AEE4C7A4E71ED236FEE0DC (void);
// 0x00000362 System.Void UnityEngine.RenderTexture::GetDepthBuffer_Injected(UnityEngine.RenderBuffer&)
extern void RenderTexture_GetDepthBuffer_Injected_mCA59890646B93E9D30E98708EAADE7444DAD2FF1 (void);
// 0x00000363 System.Void UnityEngine.RenderTexture::SetRenderTextureDescriptor_Injected(UnityEngine.RenderTextureDescriptor&)
extern void RenderTexture_SetRenderTextureDescriptor_Injected_m37024A53E72A10E7F192E51100E2224AA7D0169A (void);
// 0x00000364 System.Void UnityEngine.RenderTexture::GetDescriptor_Injected(UnityEngine.RenderTextureDescriptor&)
extern void RenderTexture_GetDescriptor_Injected_mF6F57BE0A174E81000F35E1E46A38311B661C2A3 (void);
// 0x00000365 UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary_Internal_Injected(UnityEngine.RenderTextureDescriptor&)
extern void RenderTexture_GetTemporary_Internal_Injected_m22A0AFBD90D2E2C8E4058A662EFA3ECC965FAF3C (void);
// 0x00000366 System.Int32 UnityEngine.RenderTextureDescriptor::get_width()
extern void RenderTextureDescriptor_get_width_m5DD56A0652453FDDB51FF030FC5ED914F83F5E31_AdjustorThunk (void);
// 0x00000367 System.Void UnityEngine.RenderTextureDescriptor::set_width(System.Int32)
extern void RenderTextureDescriptor_set_width_m8D4BAEBB8089FD77F4DC81088ACB511F2BCA41EA_AdjustorThunk (void);
// 0x00000368 System.Int32 UnityEngine.RenderTextureDescriptor::get_height()
extern void RenderTextureDescriptor_get_height_m661881AD8E078D6C1FD6C549207AACC2B179D201_AdjustorThunk (void);
// 0x00000369 System.Void UnityEngine.RenderTextureDescriptor::set_height(System.Int32)
extern void RenderTextureDescriptor_set_height_m1300AF31BCDCF2E14E86A598AFDC5569B682A46D_AdjustorThunk (void);
// 0x0000036A System.Int32 UnityEngine.RenderTextureDescriptor::get_msaaSamples()
extern void RenderTextureDescriptor_get_msaaSamples_m332912610A1FF2B7C05B0BA9939D733F2E7F0646_AdjustorThunk (void);
// 0x0000036B System.Void UnityEngine.RenderTextureDescriptor::set_msaaSamples(System.Int32)
extern void RenderTextureDescriptor_set_msaaSamples_m84320452D8BF3A8DD5662F6229FE666C299B5AEF_AdjustorThunk (void);
// 0x0000036C System.Int32 UnityEngine.RenderTextureDescriptor::get_volumeDepth()
extern void RenderTextureDescriptor_get_volumeDepth_m05E4A20A05286909E65D394D0BA5F6904D653688_AdjustorThunk (void);
// 0x0000036D System.Void UnityEngine.RenderTextureDescriptor::set_volumeDepth(System.Int32)
extern void RenderTextureDescriptor_set_volumeDepth_mC4D9C6B86B6799BA752855DE5C385CC24F6E3733_AdjustorThunk (void);
// 0x0000036E System.Void UnityEngine.RenderTextureDescriptor::set_mipCount(System.Int32)
extern void RenderTextureDescriptor_set_mipCount_mE713137D106256F44EF3E7B7CF33D5F146874659_AdjustorThunk (void);
// 0x0000036F UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.RenderTextureDescriptor::get_graphicsFormat()
extern void RenderTextureDescriptor_get_graphicsFormat_m9D77E42E017808FE3181673152A69CBC9A9B8B85_AdjustorThunk (void);
// 0x00000370 System.Void UnityEngine.RenderTextureDescriptor::set_graphicsFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void RenderTextureDescriptor_set_graphicsFormat_m946B6FE4422E8CD33EB13ADAFDB53669EBD361C4_AdjustorThunk (void);
// 0x00000371 System.Int32 UnityEngine.RenderTextureDescriptor::get_depthBufferBits()
extern void RenderTextureDescriptor_get_depthBufferBits_m92A95D5A1DCA7B844B3AC81AADCDFDD37D26333C_AdjustorThunk (void);
// 0x00000372 System.Void UnityEngine.RenderTextureDescriptor::set_depthBufferBits(System.Int32)
extern void RenderTextureDescriptor_set_depthBufferBits_m68BF4BF942828FF70442841A22D356E5D17BCF85_AdjustorThunk (void);
// 0x00000373 System.Void UnityEngine.RenderTextureDescriptor::set_dimension(UnityEngine.Rendering.TextureDimension)
extern void RenderTextureDescriptor_set_dimension_m4D3F1486F761F3C52308F00267B918BD7DB8137F_AdjustorThunk (void);
// 0x00000374 System.Void UnityEngine.RenderTextureDescriptor::set_shadowSamplingMode(UnityEngine.Rendering.ShadowSamplingMode)
extern void RenderTextureDescriptor_set_shadowSamplingMode_m92B77BB68CC465F38790F5865A7402C5DE77B8D1_AdjustorThunk (void);
// 0x00000375 System.Void UnityEngine.RenderTextureDescriptor::set_vrUsage(UnityEngine.VRTextureUsage)
extern void RenderTextureDescriptor_set_vrUsage_m5E4F43CB35EF142D55AC22996B641483566A2097_AdjustorThunk (void);
// 0x00000376 System.Void UnityEngine.RenderTextureDescriptor::set_memoryless(UnityEngine.RenderTextureMemoryless)
extern void RenderTextureDescriptor_set_memoryless_m6C34CD3938C6C92F98227E3864E665026C50BCE3_AdjustorThunk (void);
// 0x00000377 System.Void UnityEngine.RenderTextureDescriptor::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32)
extern void RenderTextureDescriptor__ctor_m25B4507361143C0DCCD40AC1D9B3D57F36DC83BE_AdjustorThunk (void);
// 0x00000378 System.Void UnityEngine.RenderTextureDescriptor::.ctor(System.Int32,System.Int32,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32,System.Int32)
extern void RenderTextureDescriptor__ctor_m320C821459C7856A088415334267C2963B270A9D_AdjustorThunk (void);
// 0x00000379 System.Void UnityEngine.RenderTextureDescriptor::SetOrClearRenderTextureCreationFlag(System.Boolean,UnityEngine.RenderTextureCreationFlags)
extern void RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m33FD234885342E9D0C6450C90C0F2E1B6B6A1044_AdjustorThunk (void);
// 0x0000037A System.Void UnityEngine.RenderTextureDescriptor::set_createdFromScript(System.Boolean)
extern void RenderTextureDescriptor_set_createdFromScript_mA73F313083556517070DB861089F9C8BD7C3FE31_AdjustorThunk (void);
// 0x0000037B System.Void UnityEngine.RenderTextureDescriptor::set_useDynamicScale(System.Boolean)
extern void RenderTextureDescriptor_set_useDynamicScale_mCD89818EC0E56C34DD71023F8C1821A5299621B4_AdjustorThunk (void);
// 0x0000037C System.Void UnityEngine.RenderTextureDescriptor::.cctor()
extern void RenderTextureDescriptor__cctor_mD4015195DE93496CA03515BD76A118751F6CB88F (void);
// 0x0000037D System.Int32 UnityEngine.Hash128::CompareTo(UnityEngine.Hash128)
extern void Hash128_CompareTo_m577A27A07658268AD07B09F4FFFF1D3216AD963B_AdjustorThunk (void);
// 0x0000037E System.String UnityEngine.Hash128::ToString()
extern void Hash128_ToString_mE6E0973B9B42A6AB9BEB8ACC679291CDAD2D03AC_AdjustorThunk (void);
// 0x0000037F UnityEngine.Hash128 UnityEngine.Hash128::Parse(System.String)
extern void Hash128_Parse_m717F9D24F3C0537AE4CE0534A8CEC34D721C1A89 (void);
// 0x00000380 System.String UnityEngine.Hash128::Hash128ToStringImpl(UnityEngine.Hash128)
extern void Hash128_Hash128ToStringImpl_mB227F7E22C9558FDDE51A8E9D839FFB708F331FE (void);
// 0x00000381 System.Void UnityEngine.Hash128::ComputeFromString(System.String,UnityEngine.Hash128&)
extern void Hash128_ComputeFromString_mAB2F22A4CD167B729C3A7CE56ACAC250C06FDF02 (void);
// 0x00000382 UnityEngine.Hash128 UnityEngine.Hash128::Compute(System.String)
extern void Hash128_Compute_m6A62DCDE72D17F89013487150F0D7B469AEDDC6A (void);
// 0x00000383 System.Boolean UnityEngine.Hash128::Equals(System.Object)
extern void Hash128_Equals_m2FEA62200ECEC6BA066924F3153C9FBA96B0E3FF_AdjustorThunk (void);
// 0x00000384 System.Boolean UnityEngine.Hash128::Equals(UnityEngine.Hash128)
extern void Hash128_Equals_mA77DD31AF975A04BF4BD9D1F5B2A143497C9F468_AdjustorThunk (void);
// 0x00000385 System.Int32 UnityEngine.Hash128::GetHashCode()
extern void Hash128_GetHashCode_mBEB470B9988886E4EB3FDA22903EBB699D1B7EA6_AdjustorThunk (void);
// 0x00000386 System.Int32 UnityEngine.Hash128::CompareTo(System.Object)
extern void Hash128_CompareTo_m28ACD34C28C044C2BEF2109446DAAEB53F4EC619_AdjustorThunk (void);
// 0x00000387 System.Boolean UnityEngine.Hash128::op_Equality(UnityEngine.Hash128,UnityEngine.Hash128)
extern void Hash128_op_Equality_m721DA198FB723CC694EE07A75766F1E173D5CD4F (void);
// 0x00000388 System.Boolean UnityEngine.Hash128::op_LessThan(UnityEngine.Hash128,UnityEngine.Hash128)
extern void Hash128_op_LessThan_m2BD510597CBD5898BF81AFBAB25D10AA696AFBE5 (void);
// 0x00000389 System.Boolean UnityEngine.Hash128::op_GreaterThan(UnityEngine.Hash128,UnityEngine.Hash128)
extern void Hash128_op_GreaterThan_m1F45666EBE8039F9187FF344CB86D0D8D0E367BB (void);
// 0x0000038A System.Void UnityEngine.Hash128::Parse_Injected(System.String,UnityEngine.Hash128&)
extern void Hash128_Parse_Injected_m0DB13A1F604BB3FC73E276279E0AC33F14531FEB (void);
// 0x0000038B System.String UnityEngine.Hash128::Hash128ToStringImpl_Injected(UnityEngine.Hash128&)
extern void Hash128_Hash128ToStringImpl_Injected_mDC1801343943BC014300C43AE3EC444D18A19F98 (void);
// 0x0000038C System.Void UnityEngine.Cursor::SetCursor(UnityEngine.Texture2D,UnityEngine.Vector2,UnityEngine.CursorMode)
extern void Cursor_SetCursor_m7EBC8C73DB36A950EFA4AC7DA1EC4E8CD10867FC (void);
// 0x0000038D System.Void UnityEngine.Cursor::set_visible(System.Boolean)
extern void Cursor_set_visible_m4747F0DC20D06D1932EC740C5CCC738C1664903D (void);
// 0x0000038E UnityEngine.CursorLockMode UnityEngine.Cursor::get_lockState()
extern void Cursor_get_lockState_mCE4888D80E92560908B4779FA38754B3864700C3 (void);
// 0x0000038F System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
extern void Cursor_set_lockState_mC0739186A04F4C278F02E8C1714D99B491E3A217 (void);
// 0x00000390 System.Void UnityEngine.Cursor::SetCursor_Injected(UnityEngine.Texture2D,UnityEngine.Vector2&,UnityEngine.CursorMode)
extern void Cursor_SetCursor_Injected_mE467159BD4155ADD3D62DF6ACE36DF51C707EF93 (void);
// 0x00000391 UnityEngine.ILogHandler UnityEngine.ILogger::get_logHandler()
// 0x00000392 System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object)
// 0x00000393 System.Void UnityEngine.ILogger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object)
// 0x00000394 System.Void UnityEngine.ILogger::LogFormat(UnityEngine.LogType,System.String,System.Object[])
// 0x00000395 System.Void UnityEngine.ILogHandler::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
// 0x00000396 System.Void UnityEngine.ILogHandler::LogException(System.Exception,UnityEngine.Object)
// 0x00000397 System.Void UnityEngine.Logger::.ctor(UnityEngine.ILogHandler)
extern void Logger__ctor_m01E91C7EFD28E110D491C1A6F316E5DD32616DE1 (void);
// 0x00000398 UnityEngine.ILogHandler UnityEngine.Logger::get_logHandler()
extern void Logger_get_logHandler_mE3531B52B745AAF6D304ED9CC5AC5D7BAF7E2024 (void);
// 0x00000399 System.Void UnityEngine.Logger::set_logHandler(UnityEngine.ILogHandler)
extern void Logger_set_logHandler_m07110CE12B6DC759DB4292CF11B0CC2288DA4114 (void);
// 0x0000039A System.Boolean UnityEngine.Logger::get_logEnabled()
extern void Logger_get_logEnabled_m12171AB0161FEDC83121C7A7ABA52AA3609F2D1F (void);
// 0x0000039B System.Void UnityEngine.Logger::set_logEnabled(System.Boolean)
extern void Logger_set_logEnabled_mE7274CE2DFF3669A88486479F7E2ED3DE208AA07 (void);
// 0x0000039C UnityEngine.LogType UnityEngine.Logger::get_filterLogType()
extern void Logger_get_filterLogType_mCD8726167BE9731AF85A23FE65AAFAD9353AE8A4 (void);
// 0x0000039D System.Void UnityEngine.Logger::set_filterLogType(UnityEngine.LogType)
extern void Logger_set_filterLogType_m5AFFB4C91E331F17DBEF4B85232EE07F73C040A2 (void);
// 0x0000039E System.Boolean UnityEngine.Logger::IsLogTypeAllowed(UnityEngine.LogType)
extern void Logger_IsLogTypeAllowed_m1AB436520161E88D0A7DDEF6F955BB88BE47A278 (void);
// 0x0000039F System.String UnityEngine.Logger::GetString(System.Object)
extern void Logger_GetString_mDC6359E20D3C69C29FAE80797B7CA0340506BA7B (void);
// 0x000003A0 System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object)
extern void Logger_Log_mBAF75BD87C8B66198F52DEFF72132C42CA369881 (void);
// 0x000003A1 System.Void UnityEngine.Logger::Log(UnityEngine.LogType,System.Object,UnityEngine.Object)
extern void Logger_Log_mD84CAE986DDEB614141DEDBDD023F7EB2EA511E7 (void);
// 0x000003A2 System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,System.String,System.Object[])
extern void Logger_LogFormat_mD6C153D96E0A869D48B2866E4D72D76A3E7CA2EF (void);
// 0x000003A3 System.Void UnityEngine.Logger::LogFormat(UnityEngine.LogType,UnityEngine.Object,System.String,System.Object[])
extern void Logger_LogFormat_m7D6FBEBB9C9708A50311878D7AF5A96E6E9A11F4 (void);
// 0x000003A4 System.Void UnityEngine.Logger::LogException(System.Exception,UnityEngine.Object)
extern void Logger_LogException_m207DC0A45A598148B848CF37BE3A20E6C3BB10F1 (void);
// 0x000003A5 System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLog(System.String)
extern void UnityLogWriter_WriteStringToUnityLog_m7DF2A8AB78591F20C87B8947A22D2C845F207A20 (void);
// 0x000003A6 System.Void UnityEngine.UnityLogWriter::WriteStringToUnityLogImpl(System.String)
extern void UnityLogWriter_WriteStringToUnityLogImpl_mBD8544A13E44C89FF1BCBD8685EDB0D1E760487F (void);
// 0x000003A7 System.Void UnityEngine.UnityLogWriter::Init()
extern void UnityLogWriter_Init_m84D1792BF114717225B36DD1AA45DC1201BA77FE (void);
// 0x000003A8 System.Text.Encoding UnityEngine.UnityLogWriter::get_Encoding()
extern void UnityLogWriter_get_Encoding_m6CFE536A5740F8E3F6547C4B89F40C49E5AEAE33 (void);
// 0x000003A9 System.Void UnityEngine.UnityLogWriter::Write(System.Char)
extern void UnityLogWriter_Write_m26CB2B40367CCA97725387637F0457998DED9230 (void);
// 0x000003AA System.Void UnityEngine.UnityLogWriter::Write(System.String)
extern void UnityLogWriter_Write_m256BEE6E2FB31EEFCD721BFEE676653E9CD00AD1 (void);
// 0x000003AB System.Void UnityEngine.UnityLogWriter::Write(System.Char[],System.Int32,System.Int32)
extern void UnityLogWriter_Write_m28FD8721A9896EE519A36770139213E697C57372 (void);
// 0x000003AC System.Void UnityEngine.UnityLogWriter::.ctor()
extern void UnityLogWriter__ctor_mB7AF0B7C8C546F210699D5F3AA23F370F1963A25 (void);
// 0x000003AD System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5_AdjustorThunk (void);
// 0x000003AE System.Void UnityEngine.Color::.ctor(System.Single,System.Single,System.Single)
extern void Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494_AdjustorThunk (void);
// 0x000003AF System.String UnityEngine.Color::ToString()
extern void Color_ToString_m2C9303D88F39CDAE35C613A3C816D8982C58B5FC_AdjustorThunk (void);
// 0x000003B0 System.String UnityEngine.Color::ToString(System.String,System.IFormatProvider)
extern void Color_ToString_m927424DFCE95E13D26A1F8BF91FA0AB3F6C2FC02_AdjustorThunk (void);
// 0x000003B1 System.Int32 UnityEngine.Color::GetHashCode()
extern void Color_GetHashCode_mAF5E7EE6AFA983D3FA5E3D316E672EE1511F97CF_AdjustorThunk (void);
// 0x000003B2 System.Boolean UnityEngine.Color::Equals(System.Object)
extern void Color_Equals_m90F8A5EF85416D809F5E3C0ACCADDD4F299AD8FC_AdjustorThunk (void);
// 0x000003B3 System.Boolean UnityEngine.Color::Equals(UnityEngine.Color)
extern void Color_Equals_mB531F532B5F7BE6168CFD4A6C89358C16F058D00_AdjustorThunk (void);
// 0x000003B4 UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,UnityEngine.Color)
extern void Color_op_Multiply_mFD03CB228034C2D37F326B7AFF27C861E95447B7 (void);
// 0x000003B5 UnityEngine.Color UnityEngine.Color::op_Multiply(UnityEngine.Color,System.Single)
extern void Color_op_Multiply_m1A1E7DECD013FBEB99018CEDDC30B8A7CF99941D (void);
// 0x000003B6 System.Boolean UnityEngine.Color::op_Equality(UnityEngine.Color,UnityEngine.Color)
extern void Color_op_Equality_m4975788CDFEF5571E3C51AE8363E6DF65C28A996 (void);
// 0x000003B7 System.Boolean UnityEngine.Color::op_Inequality(UnityEngine.Color,UnityEngine.Color)
extern void Color_op_Inequality_m6A9C7B9297D92024848ABFD305DDFE13DF40C86D (void);
// 0x000003B8 UnityEngine.Color UnityEngine.Color::Lerp(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void Color_Lerp_mC986D7F29103536908D76BD8FC59AA11DC33C197 (void);
// 0x000003B9 UnityEngine.Color UnityEngine.Color::RGBMultiplied(System.Single)
extern void Color_RGBMultiplied_mEE82A8761F22790ECD29CD8AE13B1184441CB6C8_AdjustorThunk (void);
// 0x000003BA UnityEngine.Color UnityEngine.Color::get_red()
extern void Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8 (void);
// 0x000003BB UnityEngine.Color UnityEngine.Color::get_green()
extern void Color_get_green_mFF9BD42534D385A0717B1EAD083ADF08712984B9 (void);
// 0x000003BC UnityEngine.Color UnityEngine.Color::get_blue()
extern void Color_get_blue_m6D62D515CA10A6E760848E1BFB997E27B90BD07B (void);
// 0x000003BD UnityEngine.Color UnityEngine.Color::get_white()
extern void Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E (void);
// 0x000003BE UnityEngine.Color UnityEngine.Color::get_black()
extern void Color_get_black_m67E91EB7017FC74D9AB5ADEF6B6929B7EFC9A982 (void);
// 0x000003BF UnityEngine.Color UnityEngine.Color::get_yellow()
extern void Color_get_yellow_m9FD4BDABA7E40E136BE57EE7872CEA6B1B2FA1D1 (void);
// 0x000003C0 UnityEngine.Color UnityEngine.Color::get_cyan()
extern void Color_get_cyan_m0C608BC083FD98C45C1F4F15AE803D288C647686 (void);
// 0x000003C1 UnityEngine.Color UnityEngine.Color::get_magenta()
extern void Color_get_magenta_m46B928AB3005B062069E5DF9CB271E1373A29FE0 (void);
// 0x000003C2 UnityEngine.Color UnityEngine.Color::get_gray()
extern void Color_get_gray_m34BEEC2EEF30819B433125EF748CE22BE17C9B6E (void);
// 0x000003C3 UnityEngine.Color UnityEngine.Color::get_grey()
extern void Color_get_grey_mB2E29B47327F20233856F933DC00ACADEBFDBDFA (void);
// 0x000003C4 UnityEngine.Color UnityEngine.Color::get_clear()
extern void Color_get_clear_m9F8076CEFE7B8119A9903212DCBF2BFED114E97F (void);
// 0x000003C5 UnityEngine.Color UnityEngine.Color::get_linear()
extern void Color_get_linear_m56FB2709C862D1A8E2B16B646FCD2E5FDF3CA904_AdjustorThunk (void);
// 0x000003C6 System.Single UnityEngine.Color::get_maxColorComponent()
extern void Color_get_maxColorComponent_mAB6964B3523DC9FDDF312F3329EB224DBFECE761_AdjustorThunk (void);
// 0x000003C7 UnityEngine.Vector4 UnityEngine.Color::op_Implicit(UnityEngine.Color)
extern void Color_op_Implicit_mECB4D0C812888ADAEE478E633B2ECF8F8FDB96C5 (void);
// 0x000003C8 UnityEngine.Color UnityEngine.Color::op_Implicit(UnityEngine.Vector4)
extern void Color_op_Implicit_m9B1A4B721726FCDA1844A0DC505C2FF8F8C50FC0 (void);
// 0x000003C9 System.Void UnityEngine.Color::RGBToHSV(UnityEngine.Color,System.Single&,System.Single&,System.Single&)
extern void Color_RGBToHSV_mDC3A14DCF9D4A898AF97613CD07D94BFF8402194 (void);
// 0x000003CA System.Void UnityEngine.Color::RGBToHSVHelper(System.Single,System.Single,System.Single,System.Single,System.Single&,System.Single&,System.Single&)
extern void Color_RGBToHSVHelper_m45E49F91ACB621F0705F8FB42C75F4BEC3958E89 (void);
// 0x000003CB UnityEngine.Color UnityEngine.Color::HSVToRGB(System.Single,System.Single,System.Single)
extern void Color_HSVToRGB_m8B61783B65A70BC889424B9A64FD40D48E735FEF (void);
// 0x000003CC UnityEngine.Color UnityEngine.Color::HSVToRGB(System.Single,System.Single,System.Single,System.Boolean)
extern void Color_HSVToRGB_m48EC4738BCDB7B4846D0B2815AE901E9BA4122F1 (void);
// 0x000003CD System.Void UnityEngine.Color32::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern void Color32__ctor_m9D07EC69256BB7ED2784E543848DE7B8484A5C94_AdjustorThunk (void);
// 0x000003CE UnityEngine.Color32 UnityEngine.Color32::op_Implicit(UnityEngine.Color)
extern void Color32_op_Implicit_mD17E8145D2D32EF369EFE349C4D32E839F7D7AA4 (void);
// 0x000003CF UnityEngine.Color UnityEngine.Color32::op_Implicit(UnityEngine.Color32)
extern void Color32_op_Implicit_m63F14F1A14B1A9A3EE4D154413EE229D3E001623 (void);
// 0x000003D0 UnityEngine.Color32 UnityEngine.Color32::Lerp(UnityEngine.Color32,UnityEngine.Color32,System.Single)
extern void Color32_Lerp_m15C150E00B311BD21CFA7660595C42574AA07269 (void);
// 0x000003D1 System.String UnityEngine.Color32::ToString()
extern void Color32_ToString_m11295D5492D1FB41F25635A83B87C20058DEA256_AdjustorThunk (void);
// 0x000003D2 System.String UnityEngine.Color32::ToString(System.String,System.IFormatProvider)
extern void Color32_ToString_m5BB9D04F00C5B22C5B295F6253C99972767102F5_AdjustorThunk (void);
// 0x000003D3 System.Boolean UnityEngine.ColorUtility::DoTryParseHtmlColor(System.String,UnityEngine.Color32&)
extern void ColorUtility_DoTryParseHtmlColor_mC35ED0130470E7096A6A2BD349321E989CBA17A2 (void);
// 0x000003D4 System.Boolean UnityEngine.ColorUtility::TryParseHtmlString(System.String,UnityEngine.Color&)
extern void ColorUtility_TryParseHtmlString_m69BEFAF655920930399471B79CF668FC3BAD4069 (void);
// 0x000003D5 System.String UnityEngine.ColorUtility::ToHtmlStringRGBA(UnityEngine.Color)
extern void ColorUtility_ToHtmlStringRGBA_mA60155FDB13ABB8720CFD3024EB8F5EDF23D79D4 (void);
// 0x000003D6 System.IntPtr UnityEngine.Gradient::Init()
extern void Gradient_Init_mF271EE940AEEA629E2646BADD07DF0BFFDC5EBA1 (void);
// 0x000003D7 System.Void UnityEngine.Gradient::Cleanup()
extern void Gradient_Cleanup_m0F4C6F0E90C86E8A5855170AA5B3FC6EC80DEF0C (void);
// 0x000003D8 System.Boolean UnityEngine.Gradient::Internal_Equals(System.IntPtr)
extern void Gradient_Internal_Equals_mA15F4C17B0910C9C9B0BAE3825F673C9F46B2054 (void);
// 0x000003D9 System.Void UnityEngine.Gradient::.ctor()
extern void Gradient__ctor_m4B95822B3C5187566CE4FA66E283600DCC916C5A (void);
// 0x000003DA System.Void UnityEngine.Gradient::Finalize()
extern void Gradient_Finalize_m2E940A5D5AE433B43D83B8E676FB9844E86F8CD0 (void);
// 0x000003DB System.Boolean UnityEngine.Gradient::Equals(System.Object)
extern void Gradient_Equals_m75D0B1625C55AAAEC024A951456300FEF4546EFF (void);
// 0x000003DC System.Boolean UnityEngine.Gradient::Equals(UnityEngine.Gradient)
extern void Gradient_Equals_m2F4EB14CAD1222F30E7DA925696DB1AF41CAF691 (void);
// 0x000003DD System.Int32 UnityEngine.Gradient::GetHashCode()
extern void Gradient_GetHashCode_m31528AF94CBACB9F6C453FD35BCDFABB77C9AED5 (void);
// 0x000003DE UnityEngine.Quaternion UnityEngine.Matrix4x4::GetRotation()
extern void Matrix4x4_GetRotation_m2613C86699F582B97CA1802BF47FA81A9BD6BF1C_AdjustorThunk (void);
// 0x000003DF UnityEngine.Vector3 UnityEngine.Matrix4x4::GetLossyScale()
extern void Matrix4x4_GetLossyScale_m4358084CE88EF575BCBF48D1CB77963F45FA15B4_AdjustorThunk (void);
// 0x000003E0 UnityEngine.Quaternion UnityEngine.Matrix4x4::get_rotation()
extern void Matrix4x4_get_rotation_m3F80DDCCBDC01EBF36D61F382749AE704603C379_AdjustorThunk (void);
// 0x000003E1 UnityEngine.Vector3 UnityEngine.Matrix4x4::get_lossyScale()
extern void Matrix4x4_get_lossyScale_m540B8C7CAA4F2FFF4B8C1DBED639B49F5EFB81CF_AdjustorThunk (void);
// 0x000003E2 UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::TRS(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void Matrix4x4_TRS_m0CBC696D0BDF58DCEC40B99BC32C716FAD024CE5 (void);
// 0x000003E3 UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Inverse(UnityEngine.Matrix4x4)
extern void Matrix4x4_Inverse_m2A60D822437B96567202296F39BFBD617D49E72F (void);
// 0x000003E4 UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_inverse()
extern void Matrix4x4_get_inverse_mFA34ECC790B269522F60FC32370D628DAFCAE225_AdjustorThunk (void);
// 0x000003E5 UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::Ortho(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single)
extern void Matrix4x4_Ortho_m9EBA11F0B670FC5563E88E4D98BAF072FFC549F1 (void);
// 0x000003E6 System.Void UnityEngine.Matrix4x4::.ctor(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Vector4)
extern void Matrix4x4__ctor_mFDDCE13D7171353ED7BA9A9B6885212DFC9E1076_AdjustorThunk (void);
// 0x000003E7 System.Single UnityEngine.Matrix4x4::get_Item(System.Int32,System.Int32)
extern void Matrix4x4_get_Item_mCE077E076AEE65232C5602DBBB5CC0D236184D0F_AdjustorThunk (void);
// 0x000003E8 System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Int32,System.Single)
extern void Matrix4x4_set_Item_mE286A89718710DDF166DF6ACF8A480D15FE06B2F_AdjustorThunk (void);
// 0x000003E9 System.Single UnityEngine.Matrix4x4::get_Item(System.Int32)
extern void Matrix4x4_get_Item_mA0E634EF5A723EA9DD824391D4C62F4100C64813_AdjustorThunk (void);
// 0x000003EA System.Void UnityEngine.Matrix4x4::set_Item(System.Int32,System.Single)
extern void Matrix4x4_set_Item_m27BC97EE3093FDADFE0804FB80AFF5CDB03D355B_AdjustorThunk (void);
// 0x000003EB System.Int32 UnityEngine.Matrix4x4::GetHashCode()
extern void Matrix4x4_GetHashCode_m102B903082CD1C786C221268A19679820E365B59_AdjustorThunk (void);
// 0x000003EC System.Boolean UnityEngine.Matrix4x4::Equals(System.Object)
extern void Matrix4x4_Equals_mF6EB7A6D466F5AE1D1A872451359645D1C69843D_AdjustorThunk (void);
// 0x000003ED System.Boolean UnityEngine.Matrix4x4::Equals(UnityEngine.Matrix4x4)
extern void Matrix4x4_Equals_mAE7AC284A922B094E4ACCC04A1C48B247E9A7997_AdjustorThunk (void);
// 0x000003EE UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern void Matrix4x4_op_Multiply_mC2B30D333D4399C1693414F1A73D87FB3450F39F (void);
// 0x000003EF UnityEngine.Vector4 UnityEngine.Matrix4x4::op_Multiply(UnityEngine.Matrix4x4,UnityEngine.Vector4)
extern void Matrix4x4_op_Multiply_m6967C4B4CC9F36FE461F5420DF7175B4201E2585 (void);
// 0x000003F0 UnityEngine.Vector4 UnityEngine.Matrix4x4::GetColumn(System.Int32)
extern void Matrix4x4_GetColumn_m5CAA237D7FD65AA772B84A1134E8B0551F9F8480_AdjustorThunk (void);
// 0x000003F1 System.Void UnityEngine.Matrix4x4::SetRow(System.Int32,UnityEngine.Vector4)
extern void Matrix4x4_SetRow_m86E7E23D71224106549DC25CEDFB992218C52187_AdjustorThunk (void);
// 0x000003F2 UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint(UnityEngine.Vector3)
extern void Matrix4x4_MultiplyPoint_mE92BEE4DED3B602983C2BBE06C44AD29564EDA83_AdjustorThunk (void);
// 0x000003F3 UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyPoint3x4(UnityEngine.Vector3)
extern void Matrix4x4_MultiplyPoint3x4_mA0A34C5FD162DA8E5421596F1F921436F3E7B2FC_AdjustorThunk (void);
// 0x000003F4 UnityEngine.Vector3 UnityEngine.Matrix4x4::MultiplyVector(UnityEngine.Vector3)
extern void Matrix4x4_MultiplyVector_m88C4BE23EB0B45BB701514AF3E1CA5A857F8212C_AdjustorThunk (void);
// 0x000003F5 UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_zero()
extern void Matrix4x4_get_zero_m653FAA37D26B00352B992A4B87CE534251FB91CC (void);
// 0x000003F6 UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::get_identity()
extern void Matrix4x4_get_identity_mC91289718DDD3DDBE0A10551BDA59A446414A596 (void);
// 0x000003F7 System.String UnityEngine.Matrix4x4::ToString()
extern void Matrix4x4_ToString_mF45C45AD892B0707C34BEE0C716C91C0B5FD2831_AdjustorThunk (void);
// 0x000003F8 System.String UnityEngine.Matrix4x4::ToString(System.String,System.IFormatProvider)
extern void Matrix4x4_ToString_mC2CC8C3C358C9C982F25F633BC21105D2C3BCEFB_AdjustorThunk (void);
// 0x000003F9 System.Void UnityEngine.Matrix4x4::.cctor()
extern void Matrix4x4__cctor_m98C56DA6312BFD0230C12C0BABB7CF6627A9CC87 (void);
// 0x000003FA System.Void UnityEngine.Matrix4x4::GetRotation_Injected(UnityEngine.Matrix4x4&,UnityEngine.Quaternion&)
extern void Matrix4x4_GetRotation_Injected_m370347C2A20639ACAAF078CD5E0093A1D932B49A (void);
// 0x000003FB System.Void UnityEngine.Matrix4x4::GetLossyScale_Injected(UnityEngine.Matrix4x4&,UnityEngine.Vector3&)
extern void Matrix4x4_GetLossyScale_Injected_mDD5A46BA5733BF1E74CD3F8E703DBFD9FC938D90 (void);
// 0x000003FC System.Void UnityEngine.Matrix4x4::TRS_Injected(UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern void Matrix4x4_TRS_Injected_m1595DD592EF50D0F0F33217285FB2884A81DF308 (void);
// 0x000003FD System.Void UnityEngine.Matrix4x4::Inverse_Injected(UnityEngine.Matrix4x4&,UnityEngine.Matrix4x4&)
extern void Matrix4x4_Inverse_Injected_m6DB86474590576CF7E1401A213812E05B41ABC37 (void);
// 0x000003FE System.Void UnityEngine.Matrix4x4::Ortho_Injected(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,UnityEngine.Matrix4x4&)
extern void Matrix4x4_Ortho_Injected_mCD0A8F3D5A71423D806A246D50D6ABD0B59E263A (void);
// 0x000003FF System.Void UnityEngine.Vector3::OrthoNormalize2(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Vector3_OrthoNormalize2_m569EE92448DA705CD5E7B45A51E95FF8A7EBB6A6 (void);
// 0x00000400 System.Void UnityEngine.Vector3::OrthoNormalize(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Vector3_OrthoNormalize_m4D0DA9DFEE300B3A4965FAB1F8CB77468BF9F2A1 (void);
// 0x00000401 UnityEngine.Vector3 UnityEngine.Vector3::RotateTowards(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single)
extern void Vector3_RotateTowards_mCE2B2820B2483A44056A74E9C2C22359ED7D1AD5 (void);
// 0x00000402 UnityEngine.Vector3 UnityEngine.Vector3::Lerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524 (void);
// 0x00000403 System.Single UnityEngine.Vector3::get_Item(System.Int32)
extern void Vector3_get_Item_m7E5B57E02F6873804F40DD48F8BEA00247AFF5AC_AdjustorThunk (void);
// 0x00000404 System.Void UnityEngine.Vector3::set_Item(System.Int32,System.Single)
extern void Vector3_set_Item_mF3E5D7FFAD5F81973283AE6C1D15C9B238AEE346_AdjustorThunk (void);
// 0x00000405 System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
extern void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_AdjustorThunk (void);
// 0x00000406 System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single)
extern void Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_AdjustorThunk (void);
// 0x00000407 UnityEngine.Vector3 UnityEngine.Vector3::Scale(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Scale_m8805EE8D2586DE7B6143FA35819B3D5CF1981FB3 (void);
// 0x00000408 System.Void UnityEngine.Vector3::Scale(UnityEngine.Vector3)
extern void Vector3_Scale_m52F0C3F26DBEE84B5E292F2E084DA2F7329B6EFF_AdjustorThunk (void);
// 0x00000409 UnityEngine.Vector3 UnityEngine.Vector3::Cross(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Cross_m63414F0C545EBB616F339FF8830D37F9230736A4 (void);
// 0x0000040A System.Int32 UnityEngine.Vector3::GetHashCode()
extern void Vector3_GetHashCode_m9F18401DA6025110A012F55BBB5ACABD36FA9A0A_AdjustorThunk (void);
// 0x0000040B System.Boolean UnityEngine.Vector3::Equals(System.Object)
extern void Vector3_Equals_m210CB160B594355581D44D4B87CF3D3994ABFED0_AdjustorThunk (void);
// 0x0000040C System.Boolean UnityEngine.Vector3::Equals(UnityEngine.Vector3)
extern void Vector3_Equals_mA92800CD98ED6A42DD7C55C5DB22DAB4DEAA6397_AdjustorThunk (void);
// 0x0000040D UnityEngine.Vector3 UnityEngine.Vector3::Reflect(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Reflect_m46273855FA396DF91C2EE4780B8F227C96C8A024 (void);
// 0x0000040E UnityEngine.Vector3 UnityEngine.Vector3::Normalize(UnityEngine.Vector3)
extern void Vector3_Normalize_m7C9B0E84BCB84D54A16D1212F3DE5AB2A386FCD9 (void);
// 0x0000040F System.Void UnityEngine.Vector3::Normalize()
extern void Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780_AdjustorThunk (void);
// 0x00000410 UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
extern void Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5_AdjustorThunk (void);
// 0x00000411 System.Single UnityEngine.Vector3::Dot(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F (void);
// 0x00000412 UnityEngine.Vector3 UnityEngine.Vector3::ProjectOnPlane(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_ProjectOnPlane_m066BDEFD60B2828C4B531CD96C4DBFADF6B0EF3B (void);
// 0x00000413 System.Single UnityEngine.Vector3::Angle(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Angle_m3715AB03A36C59D8CF08F8D71E2F46454EB884C1 (void);
// 0x00000414 System.Single UnityEngine.Vector3::SignedAngle(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_SignedAngle_m816C32A674665A4C3C9D850FB0A107E69A4D3E0A (void);
// 0x00000415 System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677 (void);
// 0x00000416 System.Single UnityEngine.Vector3::Magnitude(UnityEngine.Vector3)
extern void Vector3_Magnitude_mFBD4702FB2F35452191EC918B9B09766A5761854 (void);
// 0x00000417 System.Single UnityEngine.Vector3::get_magnitude()
extern void Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991_AdjustorThunk (void);
// 0x00000418 System.Single UnityEngine.Vector3::get_sqrMagnitude()
extern void Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249_AdjustorThunk (void);
// 0x00000419 UnityEngine.Vector3 UnityEngine.Vector3::Min(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Min_m400631CF8796AD247ABBAC2E40FD5BED64FA9BD0 (void);
// 0x0000041A UnityEngine.Vector3 UnityEngine.Vector3::Max(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_Max_m1BEB59C24069DAAE250E28C903B047431DC53155 (void);
// 0x0000041B UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
extern void Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (void);
// 0x0000041C UnityEngine.Vector3 UnityEngine.Vector3::get_one()
extern void Vector3_get_one_m9CDE5C456038B133ED94402673859EC37B1C1CCB (void);
// 0x0000041D UnityEngine.Vector3 UnityEngine.Vector3::get_forward()
extern void Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90 (void);
// 0x0000041E UnityEngine.Vector3 UnityEngine.Vector3::get_back()
extern void Vector3_get_back_mD521DF1A2C26E145578E07D618E1E4D08A1C6220 (void);
// 0x0000041F UnityEngine.Vector3 UnityEngine.Vector3::get_up()
extern void Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50 (void);
// 0x00000420 UnityEngine.Vector3 UnityEngine.Vector3::get_down()
extern void Vector3_get_down_mFA85B870E184121D30F66395BB183ECAB9DD8629 (void);
// 0x00000421 UnityEngine.Vector3 UnityEngine.Vector3::get_left()
extern void Vector3_get_left_mDAB848C352B9D30E2DDDA7F56308ABC32A4315A5 (void);
// 0x00000422 UnityEngine.Vector3 UnityEngine.Vector3::get_right()
extern void Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44 (void);
// 0x00000423 UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0 (void);
// 0x00000424 UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58 (void);
// 0x00000425 UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
extern void Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7 (void);
// 0x00000426 UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
extern void Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A (void);
// 0x00000427 UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(System.Single,UnityEngine.Vector3)
extern void Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F (void);
// 0x00000428 UnityEngine.Vector3 UnityEngine.Vector3::op_Division(UnityEngine.Vector3,System.Single)
extern void Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05 (void);
// 0x00000429 System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296 (void);
// 0x0000042A System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710 (void);
// 0x0000042B System.String UnityEngine.Vector3::ToString()
extern void Vector3_ToString_mD5085501F9A0483542E9F7B18CD09C0AB977E318_AdjustorThunk (void);
// 0x0000042C System.String UnityEngine.Vector3::ToString(System.String,System.IFormatProvider)
extern void Vector3_ToString_m8E771CC90555B06B8BDBA5F691EC5D8D87D68414_AdjustorThunk (void);
// 0x0000042D System.Void UnityEngine.Vector3::.cctor()
extern void Vector3__cctor_m1630C6F57B6D41EFCDFC7A10F52A4D2448BFB2E7 (void);
// 0x0000042E System.Void UnityEngine.Vector3::RotateTowards_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Single,UnityEngine.Vector3&)
extern void Vector3_RotateTowards_Injected_mE5549382553BABB025BCE9985C5B5A7EACA8D1F2 (void);
// 0x0000042F UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Quaternion_FromToRotation_mD0EBB9993FC7C6A45724D0365B09F11F1CEADB80 (void);
// 0x00000430 UnityEngine.Quaternion UnityEngine.Quaternion::Inverse(UnityEngine.Quaternion)
extern void Quaternion_Inverse_mE2A449C7AC8A40350AAC3761AE1AFC170062CAC9 (void);
// 0x00000431 UnityEngine.Quaternion UnityEngine.Quaternion::Internal_FromEulerRad(UnityEngine.Vector3)
extern void Quaternion_Internal_FromEulerRad_m3D0312F9F199A8ADD7463C450B24081061C884A7 (void);
// 0x00000432 UnityEngine.Vector3 UnityEngine.Quaternion::Internal_ToEulerRad(UnityEngine.Quaternion)
extern void Quaternion_Internal_ToEulerRad_m442827358B6C9EB81ADCC01F316AA2201453001E (void);
// 0x00000433 UnityEngine.Quaternion UnityEngine.Quaternion::AngleAxis(System.Single,UnityEngine.Vector3)
extern void Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398 (void);
// 0x00000434 UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Quaternion_LookRotation_m8A7DB5BDBC361586191AB67ACF857F46160EE3F1 (void);
// 0x00000435 UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
extern void Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF (void);
// 0x00000436 System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Quaternion__ctor_m564FA9302F5B9DA8BAB97B0A2D86FFE83ACAA421_AdjustorThunk (void);
// 0x00000437 UnityEngine.Quaternion UnityEngine.Quaternion::get_identity()
extern void Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702 (void);
// 0x00000438 UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0 (void);
// 0x00000439 UnityEngine.Vector3 UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Vector3)
extern void Quaternion_op_Multiply_mDC5F913E6B21FEC72AB2CF737D34CC6C7A69803D (void);
// 0x0000043A System.Boolean UnityEngine.Quaternion::IsEqualUsingDot(System.Single)
extern void Quaternion_IsEqualUsingDot_mC57C44978B13AD1592750B1D523AAB4549BD5643 (void);
// 0x0000043B System.Boolean UnityEngine.Quaternion::op_Equality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void Quaternion_op_Equality_m7EC909C253064DBECF7DB83BCF7C2E42163685BE (void);
// 0x0000043C System.Boolean UnityEngine.Quaternion::op_Inequality(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void Quaternion_op_Inequality_m37169F3E8ADDA24A5A221AD7397835B437B71439 (void);
// 0x0000043D System.Single UnityEngine.Quaternion::Dot(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void Quaternion_Dot_m7F12C5843352AB2EA687923444CC987D51515F9A (void);
// 0x0000043E UnityEngine.Vector3 UnityEngine.Quaternion::Internal_MakePositive(UnityEngine.Vector3)
extern void Quaternion_Internal_MakePositive_mD0A2B4D1439FC9AC4B487C2392836C8436323857 (void);
// 0x0000043F UnityEngine.Vector3 UnityEngine.Quaternion::get_eulerAngles()
extern void Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3_AdjustorThunk (void);
// 0x00000440 UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
extern void Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3 (void);
// 0x00000441 UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
extern void Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB (void);
// 0x00000442 System.Int32 UnityEngine.Quaternion::GetHashCode()
extern void Quaternion_GetHashCode_mFCEA4CA542544DC9BD222C66F524C2F3CFE60744_AdjustorThunk (void);
// 0x00000443 System.Boolean UnityEngine.Quaternion::Equals(System.Object)
extern void Quaternion_Equals_m3EDD7DBA22F59A5797B91820AE4BBA64576D240F_AdjustorThunk (void);
// 0x00000444 System.Boolean UnityEngine.Quaternion::Equals(UnityEngine.Quaternion)
extern void Quaternion_Equals_m02CE0D27C1DA0C037D8721750E30BB1FAF1A7DAD_AdjustorThunk (void);
// 0x00000445 System.String UnityEngine.Quaternion::ToString()
extern void Quaternion_ToString_mD3D4C66907C994D30D99E76063623F7000F6998E_AdjustorThunk (void);
// 0x00000446 System.String UnityEngine.Quaternion::ToString(System.String,System.IFormatProvider)
extern void Quaternion_ToString_mF10FE18AAC385F9CFE721ECD8B66F14346774F31_AdjustorThunk (void);
// 0x00000447 System.Void UnityEngine.Quaternion::.cctor()
extern void Quaternion__cctor_m580F8269E5FCCBE5C27222B87E7726823CEEC5E0 (void);
// 0x00000448 System.Void UnityEngine.Quaternion::FromToRotation_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Quaternion_FromToRotation_Injected_mF0D47B601B2A983EF001C4BDDA1819DB1CAAC68E (void);
// 0x00000449 System.Void UnityEngine.Quaternion::Inverse_Injected(UnityEngine.Quaternion&,UnityEngine.Quaternion&)
extern void Quaternion_Inverse_Injected_m709B75EDEB9B03431C31D5D5A100237FAB9F34D6 (void);
// 0x0000044A System.Void UnityEngine.Quaternion::Internal_FromEulerRad_Injected(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Quaternion_Internal_FromEulerRad_Injected_m5220AD64F37DB56C6DFF9DAE8B78F4E3F7185110 (void);
// 0x0000044B System.Void UnityEngine.Quaternion::Internal_ToEulerRad_Injected(UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void Quaternion_Internal_ToEulerRad_Injected_m4626950CCFC82D6D08FEDC4EA7F3C594C9C3DED0 (void);
// 0x0000044C System.Void UnityEngine.Quaternion::AngleAxis_Injected(System.Single,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Quaternion_AngleAxis_Injected_m651561C71005DEB6C7A0BF672B631DBF121B5B61 (void);
// 0x0000044D System.Void UnityEngine.Quaternion::LookRotation_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Quaternion_LookRotation_Injected_m5D096FB4F030FA09FACCB550040173DE7E3F9356 (void);
// 0x0000044E System.Int32 UnityEngine.Mathf::NextPowerOfTwo(System.Int32)
extern void Mathf_NextPowerOfTwo_m89DB0674631948FE00FD5660B18D9E62CE85CAF5 (void);
// 0x0000044F System.Single UnityEngine.Mathf::GammaToLinearSpace(System.Single)
extern void Mathf_GammaToLinearSpace_mD7A738810039778B4592535A1DB5767C4CAD68FB (void);
// 0x00000450 System.Single UnityEngine.Mathf::LinearToGammaSpace(System.Single)
extern void Mathf_LinearToGammaSpace_m5F74DEC2B3A7B45A75AC199284A22D92839118F3 (void);
// 0x00000451 System.Single UnityEngine.Mathf::PerlinNoise(System.Single,System.Single)
extern void Mathf_PerlinNoise_mBCF172821FEB8FAD7E7CF7F7982018846E702519 (void);
// 0x00000452 System.Single UnityEngine.Mathf::Sin(System.Single)
extern void Mathf_Sin_m530E5197B1E4441946DB8A12412A0C51730F6BA1 (void);
// 0x00000453 System.Single UnityEngine.Mathf::Cos(System.Single)
extern void Mathf_Cos_mF57C9B0E2B45B8A25619309BBAD6C201FF73AB98 (void);
// 0x00000454 System.Single UnityEngine.Mathf::Tan(System.Single)
extern void Mathf_Tan_mC7E6A6883BF16BBF77F15A1A0C35AB06DDAF48DC (void);
// 0x00000455 System.Single UnityEngine.Mathf::Asin(System.Single)
extern void Mathf_Asin_mABB8C82CAE370E54952561960E17198441719187 (void);
// 0x00000456 System.Single UnityEngine.Mathf::Acos(System.Single)
extern void Mathf_Acos_m199FA3A1E39D8E025CAC43BD3AF8BEE56027393C (void);
// 0x00000457 System.Single UnityEngine.Mathf::Atan(System.Single)
extern void Mathf_Atan_m12592B989E7F2CF919DF4223769CC480F888ADE5 (void);
// 0x00000458 System.Single UnityEngine.Mathf::Atan2(System.Single,System.Single)
extern void Mathf_Atan2_mF79B8F61C6089913A04B7FB6ED5A27171051A005 (void);
// 0x00000459 System.Single UnityEngine.Mathf::Sqrt(System.Single)
extern void Mathf_Sqrt_m7C3ABF6CADBBF2F97E316B025542BD0B85E87372 (void);
// 0x0000045A System.Single UnityEngine.Mathf::Abs(System.Single)
extern void Mathf_Abs_m0F02EBECA39438E5A996BE2379431B6F5E8A353D (void);
// 0x0000045B System.Int32 UnityEngine.Mathf::Abs(System.Int32)
extern void Mathf_Abs_mE46B08A540F26741910760E84ACB6AACD996C3C0 (void);
// 0x0000045C System.Single UnityEngine.Mathf::Min(System.Single,System.Single)
extern void Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14 (void);
// 0x0000045D System.Int32 UnityEngine.Mathf::Min(System.Int32,System.Int32)
extern void Mathf_Min_m8038BC2CE141C9AF3ECA2E31B88A9768423B1519 (void);
// 0x0000045E System.Single UnityEngine.Mathf::Max(System.Single,System.Single)
extern void Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775 (void);
// 0x0000045F System.Int32 UnityEngine.Mathf::Max(System.Int32,System.Int32)
extern void Mathf_Max_mAB2544BF70651EC36982F5F4EBD250AEE283335A (void);
// 0x00000460 System.Single UnityEngine.Mathf::Pow(System.Single,System.Single)
extern void Mathf_Pow_mA38993C7C8CCFCB94EE3F61236C79C45E878673E (void);
// 0x00000461 System.Single UnityEngine.Mathf::Exp(System.Single)
extern void Mathf_Exp_m78561917BA839F2FACFE60985B1B98E2A0135201 (void);
// 0x00000462 System.Single UnityEngine.Mathf::Log(System.Single,System.Single)
extern void Mathf_Log_mF7F3624FA030AB57AD8C1F4CAF084B2DCC99897A (void);
// 0x00000463 System.Single UnityEngine.Mathf::Ceil(System.Single)
extern void Mathf_Ceil_mD48EF2CA69275216AB32C63A47C8AF9428651D7D (void);
// 0x00000464 System.Single UnityEngine.Mathf::Floor(System.Single)
extern void Mathf_Floor_mB05B375ED9442CAB4ADEB7C442DB20C700F46BFB (void);
// 0x00000465 System.Single UnityEngine.Mathf::Round(System.Single)
extern void Mathf_Round_mE2DE38D81FFB085CBC073D939CDF5CBB5D57E9C9 (void);
// 0x00000466 System.Int32 UnityEngine.Mathf::CeilToInt(System.Single)
extern void Mathf_CeilToInt_m3A3E7C0F6A3CF731411BB90F264F989D8311CC6F (void);
// 0x00000467 System.Int32 UnityEngine.Mathf::FloorToInt(System.Single)
extern void Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E (void);
// 0x00000468 System.Int32 UnityEngine.Mathf::RoundToInt(System.Single)
extern void Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD (void);
// 0x00000469 System.Single UnityEngine.Mathf::Sign(System.Single)
extern void Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB (void);
// 0x0000046A System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
extern void Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87 (void);
// 0x0000046B System.Int32 UnityEngine.Mathf::Clamp(System.Int32,System.Int32,System.Int32)
extern void Mathf_Clamp_mAD0781EB7470594CD4482DD64A0D739E4E539C3C (void);
// 0x0000046C System.Single UnityEngine.Mathf::Clamp01(System.Single)
extern void Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (void);
// 0x0000046D System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
extern void Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616 (void);
// 0x0000046E System.Single UnityEngine.Mathf::LerpUnclamped(System.Single,System.Single,System.Single)
extern void Mathf_LerpUnclamped_mF68548D1AA22018863B6EBE911A5F7A959F94C1E (void);
// 0x0000046F System.Boolean UnityEngine.Mathf::Approximately(System.Single,System.Single)
extern void Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55 (void);
// 0x00000470 System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single)
extern void Mathf_SmoothDamp_m0B29D964FCB8460976BBE6BF56CBFDDC98EB5652 (void);
// 0x00000471 System.Single UnityEngine.Mathf::SmoothDamp(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern void Mathf_SmoothDamp_mBA32FC6CC8693F9446A2CF974AA44DC9801099C8 (void);
// 0x00000472 System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single)
extern void Mathf_SmoothDampAngle_mB876CA3D4A313CF29074CCDDB784CAF5533EBA46 (void);
// 0x00000473 System.Single UnityEngine.Mathf::SmoothDampAngle(System.Single,System.Single,System.Single&,System.Single,System.Single,System.Single)
extern void Mathf_SmoothDampAngle_m49AC737896E42840A706C2C895B0A93786149BF5 (void);
// 0x00000474 System.Single UnityEngine.Mathf::Repeat(System.Single,System.Single)
extern void Mathf_Repeat_mBAB712BA039DF58DBB1B31B669E502C54F3F13CE (void);
// 0x00000475 System.Single UnityEngine.Mathf::InverseLerp(System.Single,System.Single,System.Single)
extern void Mathf_InverseLerp_mCD2E6F9ADCFFB40EB7D3086E444DF2C702F9C29B (void);
// 0x00000476 System.Single UnityEngine.Mathf::DeltaAngle(System.Single,System.Single)
extern void Mathf_DeltaAngle_mB1BD0E139ACCAE694968F7D9CB096C60F69CE9FE (void);
// 0x00000477 System.Void UnityEngine.Mathf::.cctor()
extern void Mathf__cctor_mFC5862C195961EAE43A5C7BB96E07648084C1525 (void);
// 0x00000478 System.Single UnityEngine.Vector2::get_Item(System.Int32)
extern void Vector2_get_Item_m0F685FCCDE8FEFF108591D73A6D9F048CCEC5926_AdjustorThunk (void);
// 0x00000479 System.Void UnityEngine.Vector2::set_Item(System.Int32,System.Single)
extern void Vector2_set_Item_m817FDD0709F52F09ECBB949C29DEE88E73889CAD_AdjustorThunk (void);
// 0x0000047A System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
extern void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_AdjustorThunk (void);
// 0x0000047B UnityEngine.Vector2 UnityEngine.Vector2::Scale(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_Scale_m54AA203304585B8BB6ECA4936A90F408BD880916 (void);
// 0x0000047C System.Void UnityEngine.Vector2::Normalize()
extern void Vector2_Normalize_m0F1341493AD5F0B7DA4D504A44F52A1E2FFCCEC3_AdjustorThunk (void);
// 0x0000047D UnityEngine.Vector2 UnityEngine.Vector2::get_normalized()
extern void Vector2_get_normalized_m1F7F7AA3B7AC2414F245395C3785880B847BF7F5_AdjustorThunk (void);
// 0x0000047E System.String UnityEngine.Vector2::ToString()
extern void Vector2_ToString_mBD48EFCDB703ACCDC29E86AEB0D4D62FBA50F840_AdjustorThunk (void);
// 0x0000047F System.String UnityEngine.Vector2::ToString(System.String,System.IFormatProvider)
extern void Vector2_ToString_m503AFEA3F57B8529C047FF93C2E72126C5591C23_AdjustorThunk (void);
// 0x00000480 System.Int32 UnityEngine.Vector2::GetHashCode()
extern void Vector2_GetHashCode_m9A5DD8406289F38806CC42C394E324C1C2AB3732_AdjustorThunk (void);
// 0x00000481 System.Boolean UnityEngine.Vector2::Equals(System.Object)
extern void Vector2_Equals_m67A842D914AA5A4DCC076E9EA20019925E6A85A0_AdjustorThunk (void);
// 0x00000482 System.Boolean UnityEngine.Vector2::Equals(UnityEngine.Vector2)
extern void Vector2_Equals_m6E08A16717F2B9EE8B24EBA6B234A03098D5F05D_AdjustorThunk (void);
// 0x00000483 System.Single UnityEngine.Vector2::Dot(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_Dot_mB2DFFDDA2881BA755F0B75CB530A39E8EBE70B48 (void);
// 0x00000484 System.Single UnityEngine.Vector2::get_magnitude()
extern void Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85_AdjustorThunk (void);
// 0x00000485 System.Single UnityEngine.Vector2::get_sqrMagnitude()
extern void Vector2_get_sqrMagnitude_mF489F0EF7E88FF046BA36767ECC50B89674C925A_AdjustorThunk (void);
// 0x00000486 System.Single UnityEngine.Vector2::Distance(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_Distance_m7DFAD110E57AF0E903DDC47BDBD99D1CC62EA03F (void);
// 0x00000487 UnityEngine.Vector2 UnityEngine.Vector2::op_Addition(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC (void);
// 0x00000488 UnityEngine.Vector2 UnityEngine.Vector2::op_Subtraction(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A (void);
// 0x00000489 UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Multiply_m98AA5AF174918812B6E0C201C850FABB4A526813 (void);
// 0x0000048A UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Division_m63A593A281BC0B6C36FCFF399056E1DE9F4C01E0 (void);
// 0x0000048B UnityEngine.Vector2 UnityEngine.Vector2::op_UnaryNegation(UnityEngine.Vector2)
extern void Vector2_op_UnaryNegation_mA3AA3A53CD43237B0BA7AC57C09A0468A940D7C5 (void);
// 0x0000048C UnityEngine.Vector2 UnityEngine.Vector2::op_Multiply(UnityEngine.Vector2,System.Single)
extern void Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB (void);
// 0x0000048D UnityEngine.Vector2 UnityEngine.Vector2::op_Division(UnityEngine.Vector2,System.Single)
extern void Vector2_op_Division_m9E0ABD4CB731137B84249278B80D4C2624E58AC6 (void);
// 0x0000048E System.Boolean UnityEngine.Vector2::op_Equality(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0 (void);
// 0x0000048F System.Boolean UnityEngine.Vector2::op_Inequality(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Vector2_op_Inequality_mA9E4245E487F3051F0EBF086646A1C341213D24E (void);
// 0x00000490 UnityEngine.Vector2 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector3)
extern void Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A (void);
// 0x00000491 UnityEngine.Vector3 UnityEngine.Vector2::op_Implicit(UnityEngine.Vector2)
extern void Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294 (void);
// 0x00000492 UnityEngine.Vector2 UnityEngine.Vector2::get_zero()
extern void Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828 (void);
// 0x00000493 UnityEngine.Vector2 UnityEngine.Vector2::get_one()
extern void Vector2_get_one_m9B2AFD26404B6DD0F520D19FC7F79371C5C18B42 (void);
// 0x00000494 UnityEngine.Vector2 UnityEngine.Vector2::get_up()
extern void Vector2_get_up_mCEC23A0CF0FC3A2070C557AFD9F84F3D9991866C (void);
// 0x00000495 UnityEngine.Vector2 UnityEngine.Vector2::get_down()
extern void Vector2_get_down_m38F16950B2C1FAFBE218C3E62DA95D498443650F (void);
// 0x00000496 UnityEngine.Vector2 UnityEngine.Vector2::get_right()
extern void Vector2_get_right_m42ED15112D219375D2B6879E62ED925D002F15AF (void);
// 0x00000497 System.Void UnityEngine.Vector2::.cctor()
extern void Vector2__cctor_m64DC76912D71BE91E6A3B2D15D63452E2B3AD6EC (void);
// 0x00000498 System.Int32 UnityEngine.Vector2Int::get_x()
extern void Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_AdjustorThunk (void);
// 0x00000499 System.Void UnityEngine.Vector2Int::set_x(System.Int32)
extern void Vector2Int_set_x_m58F3B1895453A0A4BC964CA331D56B7C3D873B7C_AdjustorThunk (void);
// 0x0000049A System.Int32 UnityEngine.Vector2Int::get_y()
extern void Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_AdjustorThunk (void);
// 0x0000049B System.Void UnityEngine.Vector2Int::set_y(System.Int32)
extern void Vector2Int_set_y_m55A40AE7AF833E31D968E0C515A5C773F251C21A_AdjustorThunk (void);
// 0x0000049C System.Void UnityEngine.Vector2Int::.ctor(System.Int32,System.Int32)
extern void Vector2Int__ctor_mB2B73108B6DD3ADC1B515D7DD9116ECAC6833726_AdjustorThunk (void);
// 0x0000049D UnityEngine.Vector2 UnityEngine.Vector2Int::op_Implicit(UnityEngine.Vector2Int)
extern void Vector2Int_op_Implicit_m74C29CAFE091CE873934FAF6300CD01461D7FE6B (void);
// 0x0000049E System.Boolean UnityEngine.Vector2Int::Equals(System.Object)
extern void Vector2Int_Equals_m7EB52A67AE3584E8A1E8CAC550708DF13520F529_AdjustorThunk (void);
// 0x0000049F System.Boolean UnityEngine.Vector2Int::Equals(UnityEngine.Vector2Int)
extern void Vector2Int_Equals_m96F4F602CE85AFD675A8096AB9D5E2D4544382FF_AdjustorThunk (void);
// 0x000004A0 System.Int32 UnityEngine.Vector2Int::GetHashCode()
extern void Vector2Int_GetHashCode_mB963D0B9A29E161BC4B73F97AEAF2F843FC8EF71_AdjustorThunk (void);
// 0x000004A1 System.String UnityEngine.Vector2Int::ToString()
extern void Vector2Int_ToString_m7928A3CC56D9CAAB370F6B3EE797CED4BE9B9B20_AdjustorThunk (void);
// 0x000004A2 System.String UnityEngine.Vector2Int::ToString(System.String,System.IFormatProvider)
extern void Vector2Int_ToString_m608D5CEF9835892DD989B0891D7AE6F2FC0FBE02_AdjustorThunk (void);
// 0x000004A3 System.Void UnityEngine.Vector2Int::.cctor()
extern void Vector2Int__cctor_mB461BECA877E11B4B65206DFAA8A8C66170861F2 (void);
// 0x000004A4 System.Int32 UnityEngine.Vector3Int::get_x()
extern void Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594_AdjustorThunk (void);
// 0x000004A5 System.Int32 UnityEngine.Vector3Int::get_y()
extern void Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3_AdjustorThunk (void);
// 0x000004A6 System.Int32 UnityEngine.Vector3Int::get_z()
extern void Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6_AdjustorThunk (void);
// 0x000004A7 System.Void UnityEngine.Vector3Int::.ctor(System.Int32,System.Int32,System.Int32)
extern void Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_AdjustorThunk (void);
// 0x000004A8 System.Boolean UnityEngine.Vector3Int::op_Equality(UnityEngine.Vector3Int,UnityEngine.Vector3Int)
extern void Vector3Int_op_Equality_m724847B7E7A484A1E6F598CEC2D77CDE8ECE49E7 (void);
// 0x000004A9 System.Boolean UnityEngine.Vector3Int::Equals(System.Object)
extern void Vector3Int_Equals_mE2CEEF2FE79B510472A930F80DDB56E23B39CC11_AdjustorThunk (void);
// 0x000004AA System.Boolean UnityEngine.Vector3Int::Equals(UnityEngine.Vector3Int)
extern void Vector3Int_Equals_m8BE683205BACD053B7EB560AB5B7EDE78B779C5F_AdjustorThunk (void);
// 0x000004AB System.Int32 UnityEngine.Vector3Int::GetHashCode()
extern void Vector3Int_GetHashCode_mE3102EAE8E76A05DD7344009C2617BA34CDD3C08_AdjustorThunk (void);
// 0x000004AC System.String UnityEngine.Vector3Int::ToString()
extern void Vector3Int_ToString_mF13025AB50754BB9C9E98659216EC0B79F1CAA61_AdjustorThunk (void);
// 0x000004AD System.String UnityEngine.Vector3Int::ToString(System.String,System.IFormatProvider)
extern void Vector3Int_ToString_m8C32AEB68C6D46DA1F2546378067687007FCF6A5_AdjustorThunk (void);
// 0x000004AE System.Void UnityEngine.Vector3Int::.cctor()
extern void Vector3Int__cctor_m2EC94FD622152EE49C25C960DE1DEB83A3212283 (void);
// 0x000004AF System.Single UnityEngine.Vector4::get_Item(System.Int32)
extern void Vector4_get_Item_m469B9D88498D0F7CD14B71A9512915BAA0B9B3B7_AdjustorThunk (void);
// 0x000004B0 System.Void UnityEngine.Vector4::set_Item(System.Int32,System.Single)
extern void Vector4_set_Item_m7552B288FF218CA023F0DFB971BBA30D0362006A_AdjustorThunk (void);
// 0x000004B1 System.Void UnityEngine.Vector4::.ctor(System.Single,System.Single,System.Single,System.Single)
extern void Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2_AdjustorThunk (void);
// 0x000004B2 System.Int32 UnityEngine.Vector4::GetHashCode()
extern void Vector4_GetHashCode_mCA7B312F8CA141F6F25BABDDF406F3D2BDD5E895_AdjustorThunk (void);
// 0x000004B3 System.Boolean UnityEngine.Vector4::Equals(System.Object)
extern void Vector4_Equals_m71D14F39651C3FBEDE17214455DFA727921F07AA_AdjustorThunk (void);
// 0x000004B4 System.Boolean UnityEngine.Vector4::Equals(UnityEngine.Vector4)
extern void Vector4_Equals_m0919D35807550372D1748193EB31E4C5E406AE61_AdjustorThunk (void);
// 0x000004B5 System.Single UnityEngine.Vector4::Dot(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Vector4_Dot_m3373C73B23A0BC07DDE8B9C99AA2FC933CD1143F (void);
// 0x000004B6 System.Single UnityEngine.Vector4::get_sqrMagnitude()
extern void Vector4_get_sqrMagnitude_m1450744F6AAD57773CE0208B6F51DDEEE9A48E07_AdjustorThunk (void);
// 0x000004B7 UnityEngine.Vector4 UnityEngine.Vector4::get_zero()
extern void Vector4_get_zero_m9E807FEBC8B638914DF4A0BA87C0BD95A19F5200 (void);
// 0x000004B8 UnityEngine.Vector4 UnityEngine.Vector4::get_one()
extern void Vector4_get_one_m59B707729B52E58A045A6DE2ACDDE1D1600F48A4 (void);
// 0x000004B9 UnityEngine.Vector4 UnityEngine.Vector4::op_Multiply(UnityEngine.Vector4,System.Single)
extern void Vector4_op_Multiply_m4B615DCAD6D77FE276AC56F17EA3ED0DCD942111 (void);
// 0x000004BA UnityEngine.Vector4 UnityEngine.Vector4::op_Division(UnityEngine.Vector4,System.Single)
extern void Vector4_op_Division_m8AF7C92DD640CE3275F975E9BCD62F04E29DEDB6 (void);
// 0x000004BB System.Boolean UnityEngine.Vector4::op_Equality(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Vector4_op_Equality_mAC86329F5E0AF56A4A1067AB4299C291221720AE (void);
// 0x000004BC System.Boolean UnityEngine.Vector4::op_Inequality(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Vector4_op_Inequality_m2B976EE8649EE1AE9DE1771CEE183AC70E3A75B7 (void);
// 0x000004BD UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector3)
extern void Vector4_op_Implicit_mDCFA56E9D34979E1E2BFE6C2D61F1768D934A8EB (void);
// 0x000004BE UnityEngine.Vector3 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector4)
extern void Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B (void);
// 0x000004BF UnityEngine.Vector4 UnityEngine.Vector4::op_Implicit(UnityEngine.Vector2)
extern void Vector4_op_Implicit_mFFF2D39354FC98FDEDA761EDB4326E4F11B87504 (void);
// 0x000004C0 System.String UnityEngine.Vector4::ToString()
extern void Vector4_ToString_mF2D17142EBD75E91BC718B3E347F614AC45E9040_AdjustorThunk (void);
// 0x000004C1 System.String UnityEngine.Vector4::ToString(System.String,System.IFormatProvider)
extern void Vector4_ToString_m0EC6AA83CD606E3EB5BE60108A1D9AC4ECB5517A_AdjustorThunk (void);
// 0x000004C2 System.Void UnityEngine.Vector4::.cctor()
extern void Vector4__cctor_m35F167F24C48A767EAD837754896B5A5178C078A (void);
// 0x000004C3 System.Void UnityEngine.IPlayerEditorConnectionNative::Initialize()
// 0x000004C4 System.Void UnityEngine.IPlayerEditorConnectionNative::DisconnectAll()
// 0x000004C5 System.Void UnityEngine.IPlayerEditorConnectionNative::SendMessage(System.Guid,System.Byte[],System.Int32)
// 0x000004C6 System.Boolean UnityEngine.IPlayerEditorConnectionNative::TrySendMessage(System.Guid,System.Byte[],System.Int32)
// 0x000004C7 System.Void UnityEngine.IPlayerEditorConnectionNative::Poll()
// 0x000004C8 System.Void UnityEngine.IPlayerEditorConnectionNative::RegisterInternal(System.Guid)
// 0x000004C9 System.Void UnityEngine.IPlayerEditorConnectionNative::UnregisterInternal(System.Guid)
// 0x000004CA System.Boolean UnityEngine.IPlayerEditorConnectionNative::IsConnected()
// 0x000004CB System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.SendMessage(System.Guid,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_mE7A0F096977E9FEF4139A4D66DA05DC07C67399A (void);
// 0x000004CC System.Boolean UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.TrySendMessage(System.Guid,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_TrySendMessage_m9573E63723FDEBB68978AD4A14253DC648071582 (void);
// 0x000004CD System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.Poll()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Poll_mA33C0EA45F7DFF196710206BD472896DD02BB0BF (void);
// 0x000004CE System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.RegisterInternal(System.Guid)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_mC10E40AF3DE9AC1E1DAC42BF2F4F738E42F1131E (void);
// 0x000004CF System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.UnregisterInternal(System.Guid)
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_UnregisterInternal_m5E7A02A5A9569D111F9197ED07D5E822357FADBF (void);
// 0x000004D0 System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.Initialize()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Initialize_mC461084E67159FF60B78A2B995A0D6C6A5F05847 (void);
// 0x000004D1 System.Boolean UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.IsConnected()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_IsConnected_mFBCF58F025DCD0E95E1077019F30A915436221C8 (void);
// 0x000004D2 System.Void UnityEngine.PlayerConnectionInternal::UnityEngine.IPlayerEditorConnectionNative.DisconnectAll()
extern void PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_DisconnectAll_m39E3D0780D7BA1BBBAA514AB02C8B7121E6F1607 (void);
// 0x000004D3 System.Boolean UnityEngine.PlayerConnectionInternal::IsConnected()
extern void PlayerConnectionInternal_IsConnected_m6E50FFCA993DB110A440A175417542F19360878C (void);
// 0x000004D4 System.Void UnityEngine.PlayerConnectionInternal::Initialize()
extern void PlayerConnectionInternal_Initialize_m0E2E758B321FDCBA8598FE99E453F371E8544676 (void);
// 0x000004D5 System.Void UnityEngine.PlayerConnectionInternal::RegisterInternal(System.String)
extern void PlayerConnectionInternal_RegisterInternal_mEA39746E226DE13CDA2AD91050A7B49BE6CEDFD6 (void);
// 0x000004D6 System.Void UnityEngine.PlayerConnectionInternal::UnregisterInternal(System.String)
extern void PlayerConnectionInternal_UnregisterInternal_m22AB7635F9B4EE0EA1F23F61E212763375479EB0 (void);
// 0x000004D7 System.Void UnityEngine.PlayerConnectionInternal::SendMessage(System.String,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_SendMessage_m28075B14E3C74180BC8B54C013D22F7B0DDEFA8E (void);
// 0x000004D8 System.Boolean UnityEngine.PlayerConnectionInternal::TrySendMessage(System.String,System.Byte[],System.Int32)
extern void PlayerConnectionInternal_TrySendMessage_m654FD63C3F37CA5381FA3C956B4C6F68EFF8A202 (void);
// 0x000004D9 System.Void UnityEngine.PlayerConnectionInternal::PollInternal()
extern void PlayerConnectionInternal_PollInternal_m51A00BEDBF9EA39C42023981C0A1CA8B38116D86 (void);
// 0x000004DA System.Void UnityEngine.PlayerConnectionInternal::DisconnectAll()
extern void PlayerConnectionInternal_DisconnectAll_mBE50046CA1DD3A969860F0D25B5FD3381907881D (void);
// 0x000004DB System.Void UnityEngine.PlayerConnectionInternal::.ctor()
extern void PlayerConnectionInternal__ctor_m220EE8E01600348418FFBC1B83BF824C39A4441B (void);
// 0x000004DC System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern void PlayerPrefsException__ctor_m8AC8F1E3590787B418BEDCAC022B2945C43928CB (void);
// 0x000004DD System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
extern void PlayerPrefs_TrySetSetString_mC34C1AEA6A6E7BE93645BEB527BF1E6BF8D3A0F9 (void);
// 0x000004DE System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern void PlayerPrefs_SetString_m94CD8FF45692553A5726DFADF74935F7E1D1C633 (void);
// 0x000004DF System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern void PlayerPrefs_GetString_m5709C9DC233D10A7E9AF4BCC9639E3F18FE36831 (void);
// 0x000004E0 System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern void PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159 (void);
// 0x000004E1 System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern void PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92 (void);
// 0x000004E2 System.Void UnityEngine.PropertyAttribute::.ctor()
extern void PropertyAttribute__ctor_mA13181D93341AEAE429F0615989CB4647F2EB8A7 (void);
// 0x000004E3 System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
extern void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (void);
// 0x000004E4 System.Void UnityEngine.SpaceAttribute::.ctor()
extern void SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D (void);
// 0x000004E5 System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
extern void SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44 (void);
// 0x000004E6 System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
extern void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (void);
// 0x000004E7 System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
extern void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (void);
// 0x000004E8 System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
extern void TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042 (void);
// 0x000004E9 System.Void UnityEngine.ColorUsageAttribute::.ctor(System.Boolean)
extern void ColorUsageAttribute__ctor_mB764B17A7C17E6199BFC147BFFE72D34FD55D76E (void);
// 0x000004EA System.Void UnityEngine.ColorUsageAttribute::.ctor(System.Boolean,System.Boolean)
extern void ColorUsageAttribute__ctor_mA53A82E036DBF33A6DEC0AD2F00144C52DCD3B09 (void);
// 0x000004EB System.Void UnityEngine.Random::InitState(System.Int32)
extern void Random_InitState_m9030E6387803E8EBAD0A5B0150254A89F8286A9C (void);
// 0x000004EC UnityEngine.Random_State UnityEngine.Random::get_state()
extern void Random_get_state_mB5BB303DE4D0FD30AB8C8060EC0925C38A4A9020 (void);
// 0x000004ED System.Void UnityEngine.Random::set_state(UnityEngine.Random_State)
extern void Random_set_state_m10870B08C03C9B4058B410F1ABD056946D392E3A (void);
// 0x000004EE System.Single UnityEngine.Random::Range(System.Single,System.Single)
extern void Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2 (void);
// 0x000004EF System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern void Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A (void);
// 0x000004F0 System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern void Random_RandomRangeInt_mC1A726071BB9634DCA04EF5DCC1E59360CE392DE (void);
// 0x000004F1 System.Single UnityEngine.Random::get_value()
extern void Random_get_value_m9AEBC7DF0BB6C57C928B0798349A7D3C0B3FB872 (void);
// 0x000004F2 UnityEngine.Vector3 UnityEngine.Random::get_insideUnitSphere()
extern void Random_get_insideUnitSphere_m43E5AE1F6A6CFA892BAE6E3ED71BEBFCE308CE90 (void);
// 0x000004F3 System.Void UnityEngine.Random::GetRandomUnitCircle(UnityEngine.Vector2&)
extern void Random_GetRandomUnitCircle_m2FB0AE6E31D08D017D1104DE2411A3F252982A0C (void);
// 0x000004F4 UnityEngine.Vector2 UnityEngine.Random::get_insideUnitCircle()
extern void Random_get_insideUnitCircle_m0A73BD3F48690731663D2425D98687F5AD446190 (void);
// 0x000004F5 UnityEngine.Vector3 UnityEngine.Random::get_onUnitSphere()
extern void Random_get_onUnitSphere_m909066B4EAF5EE0A05D3C1DCBDADC55A2D6A4EED (void);
// 0x000004F6 System.Void UnityEngine.Random::get_state_Injected(UnityEngine.Random_State&)
extern void Random_get_state_Injected_m079170976C10F92A3036FE8BA5EB8C7589A11EC3 (void);
// 0x000004F7 System.Void UnityEngine.Random::set_state_Injected(UnityEngine.Random_State&)
extern void Random_set_state_Injected_mAE6755C8E67030DE2D11496C81D8EEF7918515B0 (void);
// 0x000004F8 System.Void UnityEngine.Random::get_insideUnitSphere_Injected(UnityEngine.Vector3&)
extern void Random_get_insideUnitSphere_Injected_m79570880ED45493E9DF45AB745483B18512C180F (void);
// 0x000004F9 System.Void UnityEngine.Random::get_onUnitSphere_Injected(UnityEngine.Vector3&)
extern void Random_get_onUnitSphere_Injected_m8B7EF1DF4AD0AB35507E1DFD6581EC0796F9C0E9 (void);
// 0x000004FA T[] UnityEngine.Resources::ConvertObjects(UnityEngine.Object[])
// 0x000004FB UnityEngine.Object[] UnityEngine.Resources::FindObjectsOfTypeAll(System.Type)
extern void Resources_FindObjectsOfTypeAll_mAC4233995ECE942E2C0250607C5CFD9E805F2215 (void);
// 0x000004FC T[] UnityEngine.Resources::FindObjectsOfTypeAll()
// 0x000004FD UnityEngine.Object UnityEngine.Resources::Load(System.String)
extern void Resources_Load_m011631B3740AFD38D496838F10D3DA635A061120 (void);
// 0x000004FE T UnityEngine.Resources::Load(System.String)
// 0x000004FF UnityEngine.Object UnityEngine.Resources::Load(System.String,System.Type)
extern void Resources_Load_m6E8E5EA02A03F3AFC8FD2D775263DBBC64BF205C (void);
// 0x00000500 UnityEngine.Object UnityEngine.Resources::GetBuiltinResource(System.Type,System.String)
extern void Resources_GetBuiltinResource_m59A7993A48D44A0002E532B7DD79BDA426E0C8A6 (void);
// 0x00000501 T UnityEngine.Resources::GetBuiltinResource(System.String)
// 0x00000502 UnityEngine.AsyncOperation UnityEngine.Resources::UnloadUnusedAssets()
extern void Resources_UnloadUnusedAssets_m5BF7EFD195EFFE171CB47FF88CA880D76A751C70 (void);
// 0x00000503 System.Void UnityEngine.AsyncOperation::InternalDestroy(System.IntPtr)
extern void AsyncOperation_InternalDestroy_mB659E46A7DE3337448BACCD77F5B64927877F482 (void);
// 0x00000504 System.Boolean UnityEngine.AsyncOperation::get_isDone()
extern void AsyncOperation_get_isDone_m4592F121393149E539D2107239639A049493D877 (void);
// 0x00000505 System.Single UnityEngine.AsyncOperation::get_progress()
extern void AsyncOperation_get_progress_m2471A0564D5C2207116737619E2CED05FBBC2D19 (void);
// 0x00000506 System.Void UnityEngine.AsyncOperation::Finalize()
extern void AsyncOperation_Finalize_m1A989E71664DD802015D858E7A336A1158BD474C (void);
// 0x00000507 System.Void UnityEngine.AsyncOperation::InvokeCompletionEvent()
extern void AsyncOperation_InvokeCompletionEvent_m2BFBB3DD63950957EDE38AE0A8D2587B900CB8F5 (void);
// 0x00000508 System.Void UnityEngine.AsyncOperation::add_completed(System.Action`1<UnityEngine.AsyncOperation>)
extern void AsyncOperation_add_completed_m44D28A82BB10C85AED56A43BB666850D2E9E59E8 (void);
// 0x00000509 System.Void UnityEngine.AsyncOperation::remove_completed(System.Action`1<UnityEngine.AsyncOperation>)
extern void AsyncOperation_remove_completed_m5B81CC486905074B13D3784BD0A061F2B4BCC3F3 (void);
// 0x0000050A System.Void UnityEngine.AsyncOperation::.ctor()
extern void AsyncOperation__ctor_mFC0E13622A23CD19A631B9ABBA506683B71A2E4A (void);
// 0x0000050B System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern void AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m9BA10D3F7DEEE7FB825187CF60338BBECA83F4B2 (void);
// 0x0000050C System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern void AttributeHelperEngine_GetRequiredComponents_mE25F6E1B6C8E4BB44FDE3E418DD442A12AA24063 (void);
// 0x0000050D System.Int32 UnityEngine.AttributeHelperEngine::GetExecuteMode(System.Type)
extern void AttributeHelperEngine_GetExecuteMode_mB33B8C49CC1EE05341F8ADFBF9B0EEB4894C0864 (void);
// 0x0000050E System.Int32 UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern void AttributeHelperEngine_CheckIsEditorScript_m109EDB093200EAAA9DA7203E58385429F791CA24 (void);
// 0x0000050F System.Int32 UnityEngine.AttributeHelperEngine::GetDefaultExecutionOrderFor(System.Type)
extern void AttributeHelperEngine_GetDefaultExecutionOrderFor_m4425CE70A70DB0716D3A5BF8C51C53C6A7891131 (void);
// 0x00000510 T UnityEngine.AttributeHelperEngine::GetCustomAttributeOfType(System.Type)
// 0x00000511 System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern void AttributeHelperEngine__cctor_m00AE154DE9BE8D99EBFBBE005F9FC40109E8FB19 (void);
// 0x00000512 System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern void DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D (void);
// 0x00000513 System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (void);
// 0x00000514 System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (void);
// 0x00000515 System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern void AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56 (void);
// 0x00000516 System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
extern void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1 (void);
// 0x00000517 System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
extern void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6 (void);
// 0x00000518 System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
extern void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (void);
// 0x00000519 System.Void UnityEngine.ContextMenu::.ctor(System.String)
extern void ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B (void);
// 0x0000051A System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean)
extern void ContextMenu__ctor_m2A623D0967274884B6D6BBB1E6242CBE60130D90 (void);
// 0x0000051B System.Void UnityEngine.ContextMenu::.ctor(System.String,System.Boolean,System.Int32)
extern void ContextMenu__ctor_mEF09B48BBF72E9A910B6DA5FB59721B15C6DAD36 (void);
// 0x0000051C System.Void UnityEngine.ExecuteInEditMode::.ctor()
extern void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (void);
// 0x0000051D System.Void UnityEngine.ExecuteAlways::.ctor()
extern void ExecuteAlways__ctor_mDB73D23637E65E57DE87C7BAAFE4CE694AE9BEE0 (void);
// 0x0000051E System.Void UnityEngine.HideInInspector::.ctor()
extern void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (void);
// 0x0000051F System.Void UnityEngine.HelpURLAttribute::.ctor(System.String)
extern void HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215 (void);
// 0x00000520 System.Int32 UnityEngine.DefaultExecutionOrder::get_order()
extern void DefaultExecutionOrder_get_order_m1C25A6D0F7F67A70D1B6B99D45C5A242DF92A4D2 (void);
// 0x00000521 System.Void UnityEngine.ExcludeFromPresetAttribute::.ctor()
extern void ExcludeFromPresetAttribute__ctor_mE4AF4E74678A39848AD81121936DEDDA1F89AB8B (void);
// 0x00000522 System.Boolean UnityEngine.Behaviour::get_enabled()
extern void Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800 (void);
// 0x00000523 System.Void UnityEngine.Behaviour::set_enabled(System.Boolean)
extern void Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32 (void);
// 0x00000524 System.Boolean UnityEngine.Behaviour::get_isActiveAndEnabled()
extern void Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA (void);
// 0x00000525 System.Void UnityEngine.Behaviour::.ctor()
extern void Behaviour__ctor_mCACD3614226521EA607B0F3640C0FAC7EACCBCE0 (void);
// 0x00000526 System.Void UnityEngine.ClassLibraryInitializer::Init()
extern void ClassLibraryInitializer_Init_m462E6CE3B1A6017AB1ADA0ACCEACBBB0B614F564 (void);
// 0x00000527 UnityEngine.Transform UnityEngine.Component::get_transform()
extern void Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (void);
// 0x00000528 UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern void Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (void);
// 0x00000529 UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern void Component_GetComponent_m4DE64B46F790BD785FDDDAD364E0364CDDE05BDB (void);
// 0x0000052A System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern void Component_GetComponentFastPath_m738F6C78381ECFB419C1B130FD8B968253F8186C (void);
// 0x0000052B T UnityEngine.Component::GetComponent()
// 0x0000052C UnityEngine.Component UnityEngine.Component::GetComponentInChildren(System.Type,System.Boolean)
extern void Component_GetComponentInChildren_m79A4BF58C839520872C6FB854CB11E6CFC57D50F (void);
// 0x0000052D T UnityEngine.Component::GetComponentInChildren()
// 0x0000052E T[] UnityEngine.Component::GetComponentsInChildren(System.Boolean)
// 0x0000052F System.Void UnityEngine.Component::GetComponentsInChildren(System.Boolean,System.Collections.Generic.List`1<T>)
// 0x00000530 T[] UnityEngine.Component::GetComponentsInChildren()
// 0x00000531 System.Void UnityEngine.Component::GetComponentsInChildren(System.Collections.Generic.List`1<T>)
// 0x00000532 UnityEngine.Component UnityEngine.Component::GetComponentInParent(System.Type)
extern void Component_GetComponentInParent_mC299BF144B9602E6B60C707B1BE695693381F264 (void);
// 0x00000533 T UnityEngine.Component::GetComponentInParent()
// 0x00000534 T[] UnityEngine.Component::GetComponentsInParent(System.Boolean)
// 0x00000535 System.Void UnityEngine.Component::GetComponentsInParent(System.Boolean,System.Collections.Generic.List`1<T>)
// 0x00000536 T[] UnityEngine.Component::GetComponentsInParent()
// 0x00000537 System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern void Component_GetComponentsForListInternal_mEC716300A7894D57F9AF6C98FA1A69C12AB4F036 (void);
// 0x00000538 System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern void Component_GetComponents_m0268D42CD0215CD9247CF74AA881BAACE10357FC (void);
// 0x00000539 System.Void UnityEngine.Component::GetComponents(System.Collections.Generic.List`1<T>)
// 0x0000053A System.String UnityEngine.Component::get_tag()
extern void Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E (void);
// 0x0000053B System.Void UnityEngine.Component::set_tag(System.String)
extern void Component_set_tag_m6E921BD86BD4A0B5114725FFF0CCD62F26BD7E81 (void);
// 0x0000053C T[] UnityEngine.Component::GetComponents()
// 0x0000053D System.Void UnityEngine.Component::SendMessage(System.String)
extern void Component_SendMessage_m3B002D579E5933EEFCA4024A1845CACB4FBBC208 (void);
// 0x0000053E System.Void UnityEngine.Component::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern void Component_SendMessage_m24896D34C08CE9336E8998CB942339604F4F822A (void);
// 0x0000053F System.Void UnityEngine.Component::SendMessage(System.String,UnityEngine.SendMessageOptions)
extern void Component_SendMessage_m4DEDB29112224E363C9B1093004C6530FFE12792 (void);
// 0x00000540 System.Void UnityEngine.Component::.ctor()
extern void Component__ctor_m0B00FA207EB3E560B78938D8AD877DB2BC1E3722 (void);
// 0x00000541 System.Void UnityEngine.Coroutine::.ctor()
extern void Coroutine__ctor_mCB658B6AD0EABDAB709A53D2B111955E06CE3C61 (void);
// 0x00000542 System.Void UnityEngine.Coroutine::Finalize()
extern void Coroutine_Finalize_m7248D49A93F72CA5E84EAD20A32ADE028905C536 (void);
// 0x00000543 System.Void UnityEngine.Coroutine::ReleaseCoroutine(System.IntPtr)
extern void Coroutine_ReleaseCoroutine_m2C46DD57BDB3BB7B64204EA229F4C5CDE342B18B (void);
// 0x00000544 System.Void UnityEngine.SetupCoroutine::InvokeMoveNext(System.Collections.IEnumerator,System.IntPtr)
extern void SetupCoroutine_InvokeMoveNext_m036E6EE8C2A4D2DAA957D5702F1A3CA51313F2C7 (void);
// 0x00000545 System.Object UnityEngine.SetupCoroutine::InvokeMember(System.Object,System.String,System.Object)
extern void SetupCoroutine_InvokeMember_m953E000F2B95EA72D6B1BC2330F0C844A2C0C680 (void);
// 0x00000546 System.Boolean UnityEngine.CustomYieldInstruction::get_keepWaiting()
// 0x00000547 System.Object UnityEngine.CustomYieldInstruction::get_Current()
extern void CustomYieldInstruction_get_Current_m719832E0EFC6B408579FCAB3B9D7A9C72A3EF80A (void);
// 0x00000548 System.Boolean UnityEngine.CustomYieldInstruction::MoveNext()
extern void CustomYieldInstruction_MoveNext_m987F30FB5F8A82F8FA62C9E3BF5DCD7D8A8FDAA6 (void);
// 0x00000549 System.Void UnityEngine.CustomYieldInstruction::Reset()
extern void CustomYieldInstruction_Reset_m9EE8C2D060FD26FE95EA2578AEF89F7012466332 (void);
// 0x0000054A System.Void UnityEngine.CustomYieldInstruction::.ctor()
extern void CustomYieldInstruction__ctor_m01929E3EEB78B751510038B32D889061960DA1BE (void);
// 0x0000054B System.Void UnityEngine.ExcludeFromObjectFactoryAttribute::.ctor()
extern void ExcludeFromObjectFactoryAttribute__ctor_mAF8163E246AD4F05E98775F7E0904F296770B06C (void);
// 0x0000054C System.Void UnityEngine.ExtensionOfNativeClassAttribute::.ctor()
extern void ExtensionOfNativeClassAttribute__ctor_mC11B09D547C215DC6E298F78CA6F05C2B28B8043 (void);
// 0x0000054D UnityEngine.GameObject UnityEngine.GameObject::CreatePrimitive(UnityEngine.PrimitiveType)
extern void GameObject_CreatePrimitive_mB1E03B8D373EBECCD93444A277316A53EC7812AC (void);
// 0x0000054E T UnityEngine.GameObject::GetComponent()
// 0x0000054F UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern void GameObject_GetComponent_mDF0C55D6EE63B6CA0DD45D627AD267004D6EC473 (void);
// 0x00000550 System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern void GameObject_GetComponentFastPath_mAC58DB8AC26576ED2A87C843A68A13C325E3C944 (void);
// 0x00000551 UnityEngine.Component UnityEngine.GameObject::GetComponentInChildren(System.Type,System.Boolean)
extern void GameObject_GetComponentInChildren_m56CAFD886686C8F6025B5CDF016E8BC684A20EED (void);
// 0x00000552 T UnityEngine.GameObject::GetComponentInChildren()
// 0x00000553 T UnityEngine.GameObject::GetComponentInChildren(System.Boolean)
// 0x00000554 UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type,System.Boolean)
extern void GameObject_GetComponentInParent_m5C1C3266DE9C2EE078C905787DDCD666AB157C2B (void);
// 0x00000555 UnityEngine.Component UnityEngine.GameObject::GetComponentInParent(System.Type)
extern void GameObject_GetComponentInParent_m84A8233060CF7F5043960E30EBC1FC715DDF9862 (void);
// 0x00000556 System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern void GameObject_GetComponentsInternal_mF491A337A167109189E2AB839584697EB2672E7D (void);
// 0x00000557 T[] UnityEngine.GameObject::GetComponents()
// 0x00000558 System.Void UnityEngine.GameObject::GetComponents(System.Collections.Generic.List`1<T>)
// 0x00000559 T[] UnityEngine.GameObject::GetComponentsInChildren(System.Boolean)
// 0x0000055A System.Void UnityEngine.GameObject::GetComponentsInChildren(System.Boolean,System.Collections.Generic.List`1<T>)
// 0x0000055B T[] UnityEngine.GameObject::GetComponentsInChildren()
// 0x0000055C System.Void UnityEngine.GameObject::GetComponentsInParent(System.Boolean,System.Collections.Generic.List`1<T>)
// 0x0000055D T[] UnityEngine.GameObject::GetComponentsInParent(System.Boolean)
// 0x0000055E UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern void GameObject_Internal_AddComponentWithType_mAD63AAF65D0603B157D8CC6C27F3EC73C108ADB4 (void);
// 0x0000055F UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern void GameObject_AddComponent_mD183856CB5A1CCF1576221D7D6CEBC4092E734B8 (void);
// 0x00000560 T UnityEngine.GameObject::AddComponent()
// 0x00000561 UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern void GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (void);
// 0x00000562 System.Int32 UnityEngine.GameObject::get_layer()
extern void GameObject_get_layer_m9D4C23A2FD105AF9964445BF18A77E8A49012F9F (void);
// 0x00000563 System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern void GameObject_set_layer_m2F946916ACB41A59C46346F5243F2BAC235A36A6 (void);
// 0x00000564 System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (void);
// 0x00000565 System.Boolean UnityEngine.GameObject::get_activeSelf()
extern void GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE (void);
// 0x00000566 System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern void GameObject_get_activeInHierarchy_mA3990AC5F61BB35283188E925C2BE7F7BF67734B (void);
// 0x00000567 System.String UnityEngine.GameObject::get_tag()
extern void GameObject_get_tag_mC21F33D368C18A631040F2887036C678B96ABC33 (void);
// 0x00000568 System.Void UnityEngine.GameObject::set_tag(System.String)
extern void GameObject_set_tag_m0EBA46574304C71E047A33BDD5F5D49E9D9A25BE (void);
// 0x00000569 System.Boolean UnityEngine.GameObject::CompareTag(System.String)
extern void GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001 (void);
// 0x0000056A UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
extern void GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F (void);
// 0x0000056B System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern void GameObject_SendMessage_mD49CCADA51268480B585733DD7C6540CCCC6EF5C (void);
// 0x0000056C System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object)
extern void GameObject_SendMessage_m9727C08D6F0A5E8F309FA9FFF389ADF8130D7BE7 (void);
// 0x0000056D System.Void UnityEngine.GameObject::SendMessage(System.String)
extern void GameObject_SendMessage_m592E9A21D51BE9E1D9E23A85750548E8CC8DB00D (void);
// 0x0000056E System.Void UnityEngine.GameObject::.ctor(System.String)
extern void GameObject__ctor_mDF8BF31EAE3E03F24421531B25FB4BEDB7C87144 (void);
// 0x0000056F System.Void UnityEngine.GameObject::.ctor()
extern void GameObject__ctor_mACDBD7A1F25B33D006A60F67EF901B33DD3D52E9 (void);
// 0x00000570 System.Void UnityEngine.GameObject::.ctor(System.String,System.Type[])
extern void GameObject__ctor_m9829583AE3BF1285861C580895202F760F3A82E8 (void);
// 0x00000571 System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern void GameObject_Internal_CreateGameObject_mA5BCF00F09243D45B7E9A1A421D8357610AE8633 (void);
// 0x00000572 UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern void GameObject_Find_m20157C941F1A9DA0E33E0ACA1324FAA41C2B199B (void);
// 0x00000573 UnityEngine.SceneManagement.Scene UnityEngine.GameObject::get_scene()
extern void GameObject_get_scene_m7EBF95ABB5037FEE6811928F2E83C769C53F86C2 (void);
// 0x00000574 UnityEngine.GameObject UnityEngine.GameObject::get_gameObject()
extern void GameObject_get_gameObject_mD5FFECF7C3AC5039E847DF7A8842478539B701D6 (void);
// 0x00000575 System.Void UnityEngine.GameObject::get_scene_Injected(UnityEngine.SceneManagement.Scene&)
extern void GameObject_get_scene_Injected_mE08525424E75EFD26BD152B4AC37563B0D4053F2 (void);
// 0x00000576 System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
extern void LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0 (void);
// 0x00000577 UnityEngine.LayerMask UnityEngine.LayerMask::op_Implicit(System.Int32)
extern void LayerMask_op_Implicit_mC7EE32122D2A4786D3C00B93E41604B71BF1397C (void);
// 0x00000578 System.Int32 UnityEngine.LayerMask::get_value()
extern void LayerMask_get_value_m6380C7449537F99361797225E179A9448A53DDF9_AdjustorThunk (void);
// 0x00000579 System.String UnityEngine.LayerMask::LayerToName(System.Int32)
extern void LayerMask_LayerToName_mDD0D3E5920B89D1CB4585B9295E93631AD2EF3B7 (void);
// 0x0000057A System.Int32 UnityEngine.LayerMask::NameToLayer(System.String)
extern void LayerMask_NameToLayer_m6A2BBB60EC90F7EC48A6A91CA98149C547A04C0A (void);
// 0x0000057B System.Void UnityEngine.ManagedStreamHelpers::ValidateLoadFromStream(System.IO.Stream)
extern void ManagedStreamHelpers_ValidateLoadFromStream_mDE750EE2AF2986BB8E11941D8513AD18597F3B13 (void);
// 0x0000057C System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamRead(System.Byte[],System.Int32,System.Int32,System.IO.Stream,System.IntPtr)
extern void ManagedStreamHelpers_ManagedStreamRead_m5E2F163F844AE93A058C5B4E31A011938FAE236B (void);
// 0x0000057D System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamSeek(System.Int64,System.UInt32,System.IO.Stream,System.IntPtr)
extern void ManagedStreamHelpers_ManagedStreamSeek_mD7B16AF1079F3F11EE782A32F851ABD761BD2E9E (void);
// 0x0000057E System.Void UnityEngine.ManagedStreamHelpers::ManagedStreamLength(System.IO.Stream,System.IntPtr)
extern void ManagedStreamHelpers_ManagedStreamLength_m352520A9914611ADA61F089CCA8D996F62F9857F (void);
// 0x0000057F System.Boolean UnityEngine.MonoBehaviour::IsInvoking()
extern void MonoBehaviour_IsInvoking_m7967B0E7FEE6F3FAD9E7565D37B009A8C43FE9A8 (void);
// 0x00000580 System.Void UnityEngine.MonoBehaviour::CancelInvoke()
extern void MonoBehaviour_CancelInvoke_mAF87B47704B16B114F82AC6914E4DA9AE034095D (void);
// 0x00000581 System.Void UnityEngine.MonoBehaviour::Invoke(System.String,System.Single)
extern void MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4 (void);
// 0x00000582 System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern void MonoBehaviour_InvokeRepeating_mB77F4276826FBA696A150831D190875CB5802C70 (void);
// 0x00000583 System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
extern void MonoBehaviour_CancelInvoke_mAD4E486A74AF79DC1AFA880691EF839CDDE630A9 (void);
// 0x00000584 System.Boolean UnityEngine.MonoBehaviour::IsInvoking(System.String)
extern void MonoBehaviour_IsInvoking_m6BF9433789CE7CE12E12069DBF043D620DFB0B4F (void);
// 0x00000585 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern void MonoBehaviour_StartCoroutine_m338FBDDDEBF67D9FC1F9E5CDEE50E66726454E2E (void);
// 0x00000586 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern void MonoBehaviour_StartCoroutine_m81C113F45C28D065C9E31DD6D7566D1BF10CF851 (void);
// 0x00000587 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern void MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (void);
// 0x00000588 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern void MonoBehaviour_StartCoroutine_Auto_m78A5B32C9E51A3C64C19BB73ED4A9A9B7536677A (void);
// 0x00000589 System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern void MonoBehaviour_StopCoroutine_m3AB89AE7770E06BDB33BF39104BE5C57DF90616B (void);
// 0x0000058A System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern void MonoBehaviour_StopCoroutine_m5FF0476C9886FD8A3E6BA82BBE34B896CA279413 (void);
// 0x0000058B System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.String)
extern void MonoBehaviour_StopCoroutine_m4DB2A899F9BDF8CA3264DD8C4130E767702B626B (void);
// 0x0000058C System.Void UnityEngine.MonoBehaviour::StopAllCoroutines()
extern void MonoBehaviour_StopAllCoroutines_m6CFEADAA0266A99176A33B47129392DF954962B4 (void);
// 0x0000058D System.Boolean UnityEngine.MonoBehaviour::get_useGUILayout()
extern void MonoBehaviour_get_useGUILayout_m42BE255035467866EB989932C62E99D5BC347524 (void);
// 0x0000058E System.Void UnityEngine.MonoBehaviour::set_useGUILayout(System.Boolean)
extern void MonoBehaviour_set_useGUILayout_m2FEF8B91090540BBED30EE904B11F0FB5F7C1E27 (void);
// 0x0000058F System.Void UnityEngine.MonoBehaviour::print(System.Object)
extern void MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090 (void);
// 0x00000590 System.Void UnityEngine.MonoBehaviour::Internal_CancelInvokeAll(UnityEngine.MonoBehaviour)
extern void MonoBehaviour_Internal_CancelInvokeAll_mE619220C0A18AE2657DFABDBCC54E54F53C60D83 (void);
// 0x00000591 System.Boolean UnityEngine.MonoBehaviour::Internal_IsInvokingAll(UnityEngine.MonoBehaviour)
extern void MonoBehaviour_Internal_IsInvokingAll_m898D1B5B37BBFC74C98EA5F86544987A5B41C49B (void);
// 0x00000592 System.Void UnityEngine.MonoBehaviour::InvokeDelayed(UnityEngine.MonoBehaviour,System.String,System.Single,System.Single)
extern void MonoBehaviour_InvokeDelayed_mD3EE47F65885A45357A5822A82F48E17D24C62C7 (void);
// 0x00000593 System.Void UnityEngine.MonoBehaviour::CancelInvoke(UnityEngine.MonoBehaviour,System.String)
extern void MonoBehaviour_CancelInvoke_m6BBDCBE18EEBE2584EED4F8706DA3BC6771E2529 (void);
// 0x00000594 System.Boolean UnityEngine.MonoBehaviour::IsInvoking(UnityEngine.MonoBehaviour,System.String)
extern void MonoBehaviour_IsInvoking_m2584CADBA55F43D3A1D9955F1E9EAA579B8FD206 (void);
// 0x00000595 System.Boolean UnityEngine.MonoBehaviour::IsObjectMonoBehaviour(UnityEngine.Object)
extern void MonoBehaviour_IsObjectMonoBehaviour_mE580D905186AD91FD74214B643B26F55D54A46AF (void);
// 0x00000596 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutineManaged(System.String,System.Object)
extern void MonoBehaviour_StartCoroutineManaged_mAA34ABF1564BF65264FF972AF41CF8C0F6A6B6F4 (void);
// 0x00000597 UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutineManaged2(System.Collections.IEnumerator)
extern void MonoBehaviour_StartCoroutineManaged2_m46EFC2DA4428D3CC64BA3F1394D24351E316577D (void);
// 0x00000598 System.Void UnityEngine.MonoBehaviour::StopCoroutineManaged(UnityEngine.Coroutine)
extern void MonoBehaviour_StopCoroutineManaged_m8214F615A10BC1C8C78CEE92DF921C892BE3603D (void);
// 0x00000599 System.Void UnityEngine.MonoBehaviour::StopCoroutineFromEnumeratorManaged(System.Collections.IEnumerator)
extern void MonoBehaviour_StopCoroutineFromEnumeratorManaged_m17DB11886ABA10DF9749F5E1DDCCD14AD4B5E31E (void);
// 0x0000059A System.String UnityEngine.MonoBehaviour::GetScriptClassName()
extern void MonoBehaviour_GetScriptClassName_m2968E4771004FC58819BF2D9391DED766D1213E3 (void);
// 0x0000059B System.Void UnityEngine.MonoBehaviour::.ctor()
extern void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (void);
// 0x0000059C System.Int32 UnityEngine.NoAllocHelpers::SafeLength(System.Array)
extern void NoAllocHelpers_SafeLength_m02DFDBC654748FCB895D2ADDC5E5C92415C3A4B2 (void);
// 0x0000059D System.Int32 UnityEngine.NoAllocHelpers::SafeLength(System.Collections.Generic.List`1<T>)
// 0x0000059E System.Array UnityEngine.NoAllocHelpers::ExtractArrayFromList(System.Object)
extern void NoAllocHelpers_ExtractArrayFromList_m097334749C829402A9634BF025A54F3918D238DD (void);
// 0x0000059F System.Int32 UnityEngine.RangeInt::get_end()
extern void RangeInt_get_end_m6F8F3C6EA01F7A99BF3A094827F5A0D612AA179E_AdjustorThunk (void);
// 0x000005A0 System.Void UnityEngine.RangeInt::.ctor(System.Int32,System.Int32)
extern void RangeInt__ctor_m61527D982CDE91D896757816896BE6BDB366B9E0_AdjustorThunk (void);
// 0x000005A1 System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor()
extern void RuntimeInitializeOnLoadMethodAttribute__ctor_mAEDC96FCA281601682E7207BD386A1553C1B6081 (void);
// 0x000005A2 System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::.ctor(UnityEngine.RuntimeInitializeLoadType)
extern void RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516 (void);
// 0x000005A3 System.Void UnityEngine.RuntimeInitializeOnLoadMethodAttribute::set_loadType(UnityEngine.RuntimeInitializeLoadType)
extern void RuntimeInitializeOnLoadMethodAttribute_set_loadType_m5C045AAF89A8C1541871F7F9090B3C0A289E32C6 (void);
// 0x000005A4 System.Void UnityEngine.ScriptableObject::.ctor()
extern void ScriptableObject__ctor_m8DAE6CDCFA34E16F2543B02CC3669669FF203063 (void);
// 0x000005A5 UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateInstance(System.Type)
extern void ScriptableObject_CreateInstance_m5371BDC0B4F60FE15914A7BB3FBE07D0ACA0A8D4 (void);
// 0x000005A6 T UnityEngine.ScriptableObject::CreateInstance()
// 0x000005A7 System.Void UnityEngine.ScriptableObject::CreateScriptableObject(UnityEngine.ScriptableObject)
extern void ScriptableObject_CreateScriptableObject_m9627DCBB805911280823940601D996E008021D6B (void);
// 0x000005A8 UnityEngine.ScriptableObject UnityEngine.ScriptableObject::CreateScriptableObjectInstanceFromType(System.Type,System.Boolean)
extern void ScriptableObject_CreateScriptableObjectInstanceFromType_mA2EB72F4D5FC5643D7CFFD07A29DD726CAB1B9AD (void);
// 0x000005A9 System.Boolean UnityEngine.ScriptingUtility::IsManagedCodeWorking()
extern void ScriptingUtility_IsManagedCodeWorking_m176E49DF0BCA69480A3D9360DAED8DDDB8732F68 (void);
// 0x000005AA System.Void UnityEngine.SelectionBaseAttribute::.ctor()
extern void SelectionBaseAttribute__ctor_mDCDA943585A570BA4243FEFB022DABA360910E11 (void);
// 0x000005AB System.Void UnityEngine.StackTraceUtility::SetProjectFolder(System.String)
extern void StackTraceUtility_SetProjectFolder_m4CF077574CDE9A65A3546973395B4A228C1F957C (void);
// 0x000005AC System.String UnityEngine.StackTraceUtility::ExtractStackTrace()
extern void StackTraceUtility_ExtractStackTrace_mDD5E4AEC754FEB52C9FFA3B0675E57088B425EC6 (void);
// 0x000005AD System.Void UnityEngine.StackTraceUtility::ExtractStringFromExceptionInternal(System.Object,System.String&,System.String&)
extern void StackTraceUtility_ExtractStringFromExceptionInternal_mE6192186E0D4CA0B148C602A5CDA6466EFA23D99 (void);
// 0x000005AE System.String UnityEngine.StackTraceUtility::ExtractFormattedStackTrace(System.Diagnostics.StackTrace)
extern void StackTraceUtility_ExtractFormattedStackTrace_m956907F6BE8EFF9BE9847275406FFBBB5FE7F093 (void);
// 0x000005AF System.Void UnityEngine.StackTraceUtility::.cctor()
extern void StackTraceUtility__cctor_m8AFBE529BA4A1737BAF53BB4F8028E18D2D84A5F (void);
// 0x000005B0 System.Void UnityEngine.UnityException::.ctor()
extern void UnityException__ctor_m8E5C592F5F76B6385D4EFDC2C28AA93B020279E7 (void);
// 0x000005B1 System.Void UnityEngine.UnityException::.ctor(System.String)
extern void UnityException__ctor_mB8EBFD7A68451D56285E7D51B42FBECFC8A141D8 (void);
// 0x000005B2 System.Void UnityEngine.UnityException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void UnityException__ctor_m00AA4E67E35F95220B5A1C46C9B59AF4ED0D6274 (void);
// 0x000005B3 System.Byte[] UnityEngine.TextAsset::get_bytes()
extern void TextAsset_get_bytes_m5F15438DABBBAAF7434D53B6778A97A498C1940F (void);
// 0x000005B4 System.String UnityEngine.TextAsset::get_text()
extern void TextAsset_get_text_m89A756483BA3218E173F5D62A582070714BC1218 (void);
// 0x000005B5 System.String UnityEngine.TextAsset::ToString()
extern void TextAsset_ToString_m55E5177185AA59560459F579E36C03CF1971EC57 (void);
// 0x000005B6 System.String UnityEngine.TextAsset::DecodeString(System.Byte[])
extern void TextAsset_DecodeString_m3CD8D6865DCE58592AE76360F4DB856A6962FE13 (void);
// 0x000005B7 System.Void UnityEngine.TextAsset_EncodingUtility::.cctor()
extern void EncodingUtility__cctor_m6BADEB7670563CC438C10AF259028A7FF06AD65F (void);
// 0x000005B8 System.Void UnityEngine.UnhandledExceptionHandler::RegisterUECatcher()
extern void UnhandledExceptionHandler_RegisterUECatcher_m3A92AB19701D52AE35F525E9518DAD6763D66A93 (void);
// 0x000005B9 System.Void UnityEngine.UnhandledExceptionHandler_<>c::.cctor()
extern void U3CU3Ec__cctor_m5B3F5B8A2DD74DC42F3E777C1FDD1C880EFA95BA (void);
// 0x000005BA System.Void UnityEngine.UnhandledExceptionHandler_<>c::.ctor()
extern void U3CU3Ec__ctor_mB628041C94E761F86F2A26819A038D6BC59E324D (void);
// 0x000005BB System.Void UnityEngine.UnhandledExceptionHandler_<>c::<RegisterUECatcher>b__0_0(System.Object,System.UnhandledExceptionEventArgs)
extern void U3CU3Ec_U3CRegisterUECatcherU3Eb__0_0_mB2E6DD6B9C72FA3D5DB8D311DB281F272A587278 (void);
// 0x000005BC System.Int32 UnityEngine.Object::GetInstanceID()
extern void Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501 (void);
// 0x000005BD System.Int32 UnityEngine.Object::GetHashCode()
extern void Object_GetHashCode_m7CC0B54570AA90E51ED2D2D6F6F078BEF9996538 (void);
// 0x000005BE System.Boolean UnityEngine.Object::Equals(System.Object)
extern void Object_Equals_m42425AB7E6C5B6E2BAB15B3184B23371AB0B3435 (void);
// 0x000005BF System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern void Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (void);
// 0x000005C0 System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern void Object_CompareBaseObjects_m412CC4C56457345D91A509C83A0B04E3AE5A0AED (void);
// 0x000005C1 System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern void Object_IsNativeObjectAlive_mE631050FBED7A72BC8A0394F26FF1984F546B6D4 (void);
// 0x000005C2 System.IntPtr UnityEngine.Object::GetCachedPtr()
extern void Object_GetCachedPtr_m9F3F26858655E3CE7D471324271E3E32C7AEFD7A (void);
// 0x000005C3 System.String UnityEngine.Object::get_name()
extern void Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (void);
// 0x000005C4 System.Void UnityEngine.Object::set_name(System.String)
extern void Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781 (void);
// 0x000005C5 UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Object_Instantiate_mADD3254CB5E6AB56FBB6964FE4B930F5187AB5AD (void);
// 0x000005C6 UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
extern void Object_Instantiate_m0B91F1876CDBE46242A9E5B32F9EE53FAF2BDD99 (void);
// 0x000005C7 UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object)
extern void Object_Instantiate_m565A02EA45AEA7442E19FDC8E82695491938CB5A (void);
// 0x000005C8 T UnityEngine.Object::Instantiate(T)
// 0x000005C9 T UnityEngine.Object::Instantiate(T,UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Transform)
// 0x000005CA System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern void Object_Destroy_mAAAA103F4911E9FA18634BF9605C28559F5E2AC7 (void);
// 0x000005CB System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern void Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30 (void);
// 0x000005CC System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern void Object_DestroyImmediate_m69AA5B06236FACFF316DDFAD131B2622B397AC49 (void);
// 0x000005CD System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern void Object_DestroyImmediate_mCCED69F4D4C9A4FA3AC30A142CF3D7F085F7C422 (void);
// 0x000005CE UnityEngine.Object[] UnityEngine.Object::FindObjectsOfType(System.Type,System.Boolean)
extern void Object_FindObjectsOfType_m0AEB81CC6F1D224A6F4DCC7D553482D54FC03C5A (void);
// 0x000005CF System.Void UnityEngine.Object::DontDestroyOnLoad(UnityEngine.Object)
extern void Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9 (void);
// 0x000005D0 UnityEngine.HideFlags UnityEngine.Object::get_hideFlags()
extern void Object_get_hideFlags_m0F6495E1FB440A08314B7D74CCA99C897FAC05D1 (void);
// 0x000005D1 System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern void Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC (void);
// 0x000005D2 T[] UnityEngine.Object::FindObjectsOfType()
// 0x000005D3 T UnityEngine.Object::FindObjectOfType()
// 0x000005D4 System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern void Object_CheckNullArgument_mFA979ED3433CACA46AC9AE0029A537B46E17D080 (void);
// 0x000005D5 UnityEngine.Object UnityEngine.Object::FindObjectOfType(System.Type,System.Boolean)
extern void Object_FindObjectOfType_mFDEFCE84CE9C644F2B51DCC26CDAC78AC7E89B1B (void);
// 0x000005D6 System.String UnityEngine.Object::ToString()
extern void Object_ToString_m2B1E5081A55425442324F437090FAD552931A7E9 (void);
// 0x000005D7 System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern void Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (void);
// 0x000005D8 System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern void Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (void);
// 0x000005D9 System.Int32 UnityEngine.Object::GetOffsetOfInstanceIDInCPlusPlusObject()
extern void Object_GetOffsetOfInstanceIDInCPlusPlusObject_m7749BCDBEBAE50378BE38DA313E2D854D77BB18C (void);
// 0x000005DA UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern void Object_Internal_CloneSingle_m6C669D602DFD7BC6C47ACA19B2F4D7C853F124BB (void);
// 0x000005DB UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Object_Internal_InstantiateSingle_mE1865EB98332FDFEDC294AD90748D28ED272021A (void);
// 0x000005DC UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Object_Internal_InstantiateSingleWithParent_m4702089B268A30D3A6FBE780B7E78833585054E4 (void);
// 0x000005DD System.String UnityEngine.Object::ToString(UnityEngine.Object)
extern void Object_ToString_m4E499FDF54B8B2727FCCE6EB7BEB976BAB670030 (void);
// 0x000005DE System.String UnityEngine.Object::GetName(UnityEngine.Object)
extern void Object_GetName_m6F0498EEECA37CD27B052F53ECDDA019129F3D7A (void);
// 0x000005DF System.Void UnityEngine.Object::SetName(UnityEngine.Object,System.String)
extern void Object_SetName_mD80C3B5E390937EF4885BE21FBAC3D91A1C6EC96 (void);
// 0x000005E0 UnityEngine.Object UnityEngine.Object::FindObjectFromInstanceID(System.Int32)
extern void Object_FindObjectFromInstanceID_m0AD731D3F583CBBDC7DC0E08B38F742B447FA44A (void);
// 0x000005E1 System.Void UnityEngine.Object::.ctor()
extern void Object__ctor_m4DCF5CDB32C2C69290894101A81F473865169279 (void);
// 0x000005E2 System.Void UnityEngine.Object::.cctor()
extern void Object__cctor_mD6AB403C7A4ED74F04167372E52C3C876EC422A1 (void);
// 0x000005E3 UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle_Injected(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Object_Internal_InstantiateSingle_Injected_mD475F2979EDD95363A9AC52EC1BB3E98E6D1D3C2 (void);
// 0x000005E4 UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingleWithParent_Injected(UnityEngine.Object,UnityEngine.Transform,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern void Object_Internal_InstantiateSingleWithParent_Injected_mC883CE3023EAF9F3336A534D5B698C71457ECD97 (void);
// 0x000005E5 System.Void UnityEngine.UnitySynchronizationContext::.ctor(System.Int32)
extern void UnitySynchronizationContext__ctor_mF71A02778FCC672CC2FE4F329D9D6C66C96F70CF (void);
// 0x000005E6 System.Void UnityEngine.UnitySynchronizationContext::.ctor(System.Collections.Generic.List`1<UnityEngine.UnitySynchronizationContext_WorkRequest>,System.Int32)
extern void UnitySynchronizationContext__ctor_m266FAB5B25698C86EC1AC37CE4BBA1FA244BB4FC (void);
// 0x000005E7 System.Void UnityEngine.UnitySynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Send_mEDA081A69B18358B9239E2A46E570466E5FA77F7 (void);
// 0x000005E8 System.Void UnityEngine.UnitySynchronizationContext::OperationStarted()
extern void UnitySynchronizationContext_OperationStarted_mEC5A306F40CB91350A64AAAF6B114B4A994B630A (void);
// 0x000005E9 System.Void UnityEngine.UnitySynchronizationContext::OperationCompleted()
extern void UnitySynchronizationContext_OperationCompleted_m069CD670B8D783A371D51E900B792EDCD71B69A7 (void);
// 0x000005EA System.Void UnityEngine.UnitySynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern void UnitySynchronizationContext_Post_mE3277DB5A0DCB461E497EC7B9C13850D4E7AB076 (void);
// 0x000005EB System.Threading.SynchronizationContext UnityEngine.UnitySynchronizationContext::CreateCopy()
extern void UnitySynchronizationContext_CreateCopy_mBB5F2E7947732680B0715AD92187E89211AE5E61 (void);
// 0x000005EC System.Void UnityEngine.UnitySynchronizationContext::Exec()
extern void UnitySynchronizationContext_Exec_mC89E49BFB922E69AAE753887480031A142016F81 (void);
// 0x000005ED System.Boolean UnityEngine.UnitySynchronizationContext::HasPendingTasks()
extern void UnitySynchronizationContext_HasPendingTasks_mBA93E72DC46200231B7ABCFB5F22FB0617A1B1EA (void);
// 0x000005EE System.Void UnityEngine.UnitySynchronizationContext::InitializeSynchronizationContext()
extern void UnitySynchronizationContext_InitializeSynchronizationContext_mB581D86F8675566DC3A885C15DC5B9A7B8D42CD4 (void);
// 0x000005EF System.Void UnityEngine.UnitySynchronizationContext::ExecuteTasks()
extern void UnitySynchronizationContext_ExecuteTasks_m323E27C0CD442B806D966D024725D9809563E0DD (void);
// 0x000005F0 System.Boolean UnityEngine.UnitySynchronizationContext::ExecutePendingTasks(System.Int64)
extern void UnitySynchronizationContext_ExecutePendingTasks_m7FD629962A8BA72CB2F51583DC393A8FDA672DBC (void);
// 0x000005F1 System.Void UnityEngine.UnitySynchronizationContext_WorkRequest::.ctor(System.Threading.SendOrPostCallback,System.Object,System.Threading.ManualResetEvent)
extern void WorkRequest__ctor_m13C7B4A89E47F4B97ED9B786DB99849DBC2B5603_AdjustorThunk (void);
// 0x000005F2 System.Void UnityEngine.UnitySynchronizationContext_WorkRequest::Invoke()
extern void WorkRequest_Invoke_m1C292B7297918C5F2DBE70971895FE8D5C33AA20_AdjustorThunk (void);
// 0x000005F3 System.Void UnityEngine.WaitForEndOfFrame::.ctor()
extern void WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA (void);
// 0x000005F4 System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (void);
// 0x000005F5 System.Single UnityEngine.WaitForSecondsRealtime::get_waitTime()
extern void WaitForSecondsRealtime_get_waitTime_m04ED4EACCB01E49DEC7E0E5A83789068A3525BC2 (void);
// 0x000005F6 System.Void UnityEngine.WaitForSecondsRealtime::set_waitTime(System.Single)
extern void WaitForSecondsRealtime_set_waitTime_m241120AEE2F1BDD0DC3077D865C7C3D878448268 (void);
// 0x000005F7 System.Boolean UnityEngine.WaitForSecondsRealtime::get_keepWaiting()
extern void WaitForSecondsRealtime_get_keepWaiting_m96E9697A6DA22E3537EE2ED3823523C05838844D (void);
// 0x000005F8 System.Void UnityEngine.WaitForSecondsRealtime::.ctor(System.Single)
extern void WaitForSecondsRealtime__ctor_m7A69DE38F96121145BE8108B5AA62C789059F225 (void);
// 0x000005F9 System.Void UnityEngine.WaitForSecondsRealtime::Reset()
extern void WaitForSecondsRealtime_Reset_m6CB6F149A3EC3BA4ADB6A78FD8A05F82246E9B01 (void);
// 0x000005FA System.Boolean UnityEngine.WaitUntil::get_keepWaiting()
extern void WaitUntil_get_keepWaiting_mA846E5325F23E950FC58FD1FD4FC8829E3C54D88 (void);
// 0x000005FB System.Void UnityEngine.WaitUntil::.ctor(System.Func`1<System.Boolean>)
extern void WaitUntil__ctor_m4D825CB1C0570631514BFF24AEF1FB17D646C76F (void);
// 0x000005FC System.Void UnityEngine.YieldInstruction::.ctor()
extern void YieldInstruction__ctor_mD8203310B47F2C36BED3EEC00CA1944C9D941AEF (void);
// 0x000005FD System.Void UnityEngine.SerializeField::.ctor()
extern void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (void);
// 0x000005FE System.Void UnityEngine.SerializeReference::.ctor()
extern void SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E (void);
// 0x000005FF System.Void UnityEngine.ISerializationCallbackReceiver::OnBeforeSerialize()
// 0x00000600 System.Void UnityEngine.ISerializationCallbackReceiver::OnAfterDeserialize()
// 0x00000601 System.Void UnityEngine.ComputeBuffer::Finalize()
extern void ComputeBuffer_Finalize_m0228E0E952755BECEF3790BA2BCD7F93AB3AFB03 (void);
// 0x00000602 System.Void UnityEngine.ComputeBuffer::Dispose()
extern void ComputeBuffer_Dispose_m2B87F7A44073E119999E0684414768E0F6B810D3 (void);
// 0x00000603 System.Void UnityEngine.ComputeBuffer::Dispose(System.Boolean)
extern void ComputeBuffer_Dispose_m7B78DD02830F996D5B943458CE3BF6BF9798338D (void);
// 0x00000604 System.IntPtr UnityEngine.ComputeBuffer::InitBuffer(System.Int32,System.Int32,UnityEngine.ComputeBufferType,UnityEngine.ComputeBufferMode)
extern void ComputeBuffer_InitBuffer_m898B1315B258816EAE378CA8BBA170CBA2A4D305 (void);
// 0x00000605 System.Void UnityEngine.ComputeBuffer::DestroyBuffer(UnityEngine.ComputeBuffer)
extern void ComputeBuffer_DestroyBuffer_mF4D63F7B14F4E1CF2EF115F95393CD3B6B6DE2C6 (void);
// 0x00000606 System.Void UnityEngine.ComputeBuffer::.ctor(System.Int32,System.Int32)
extern void ComputeBuffer__ctor_m66E68E9066197B06CF35FDDAFAFDC8C1DECC5436 (void);
// 0x00000607 System.Void UnityEngine.ComputeBuffer::.ctor(System.Int32,System.Int32,UnityEngine.ComputeBufferType,UnityEngine.ComputeBufferMode,System.Int32)
extern void ComputeBuffer__ctor_mDF3FA4318409FED145709358E0273A296802DB10 (void);
// 0x00000608 System.Void UnityEngine.ComputeBuffer::Release()
extern void ComputeBuffer_Release_m7F1D518D1542CD4B961B17E3A44376FA78E977EB (void);
// 0x00000609 System.Void UnityEngine.ComputeBuffer::SetData(System.Array)
extern void ComputeBuffer_SetData_m4A48A0AD92ADDE0C718BD077686C649BCF963EB8 (void);
// 0x0000060A System.Void UnityEngine.ComputeBuffer::InternalSetData(System.Array,System.Int32,System.Int32,System.Int32,System.Int32)
extern void ComputeBuffer_InternalSetData_mD66C53B1EBB6939FA9E8353F8FBE7507040E59D1 (void);
// 0x0000060B System.Int32 UnityEngine.ComputeShader::FindKernel(System.String)
extern void ComputeShader_FindKernel_mCA2683905A5DAB573D50389E2B24B48B18CD53D0 (void);
// 0x0000060C System.Void UnityEngine.ComputeShader::SetVector(System.Int32,UnityEngine.Vector4)
extern void ComputeShader_SetVector_mC514C61427474F1B0E29FB667B6F6D1CB7B4A8E9 (void);
// 0x0000060D System.Void UnityEngine.ComputeShader::SetTexture(System.Int32,System.Int32,UnityEngine.Texture,System.Int32)
extern void ComputeShader_SetTexture_mA023333A983918D3D846DD315C8FF59AA5EFC62C (void);
// 0x0000060E System.Void UnityEngine.ComputeShader::Internal_SetBuffer(System.Int32,System.Int32,UnityEngine.ComputeBuffer)
extern void ComputeShader_Internal_SetBuffer_m40C222A1E6FDF93B6ECD7FB1EDE78034FC30FD12 (void);
// 0x0000060F System.Void UnityEngine.ComputeShader::SetBuffer(System.Int32,System.Int32,UnityEngine.ComputeBuffer)
extern void ComputeShader_SetBuffer_m134F25C2DAD8D04F0A6EBBDECB56E8A17E461A3D (void);
// 0x00000610 System.Void UnityEngine.ComputeShader::Dispatch(System.Int32,System.Int32,System.Int32,System.Int32)
extern void ComputeShader_Dispatch_mCC2F94FF5EB215C5CC4824741C2CB8D94423CBA4 (void);
// 0x00000611 System.Void UnityEngine.ComputeShader::SetVector(System.String,UnityEngine.Vector4)
extern void ComputeShader_SetVector_mC2E2E16685ECDDB22D92A6AF19F057CF012D98F9 (void);
// 0x00000612 System.Void UnityEngine.ComputeShader::SetTexture(System.Int32,System.String,UnityEngine.Texture)
extern void ComputeShader_SetTexture_mE8EA0F8395891E76B8A27E19A1536D817F506E5B (void);
// 0x00000613 System.Void UnityEngine.ComputeShader::SetBuffer(System.Int32,System.String,UnityEngine.ComputeBuffer)
extern void ComputeShader_SetBuffer_m4EB505ABF717B7151A1EF0CD7364047432B3EC3C (void);
// 0x00000614 System.Void UnityEngine.ComputeShader::SetVector_Injected(System.Int32,UnityEngine.Vector4&)
extern void ComputeShader_SetVector_Injected_m50EB28FBAE8176695248910223A8496C0445B633 (void);
// 0x00000615 System.Void UnityEngine.LowerResBlitTexture::LowerResBlitTextureDontStripMe()
extern void LowerResBlitTexture_LowerResBlitTextureDontStripMe_mFF261E84D36BA5652A3EAB9B45159D6E2FE23A0E (void);
// 0x00000616 System.Void UnityEngine.PreloadData::PreloadDataDontStripMe()
extern void PreloadData_PreloadDataDontStripMe_mE542B732FBF4F1E9F01B1D1C2160C43E37E70B7A (void);
// 0x00000617 System.Single UnityEngine.SystemInfo::get_batteryLevel()
extern void SystemInfo_get_batteryLevel_m630A753F505B1B642D97439908F01E5E3F972F93 (void);
// 0x00000618 UnityEngine.BatteryStatus UnityEngine.SystemInfo::get_batteryStatus()
extern void SystemInfo_get_batteryStatus_mD6E9FA6A93D6D069E622A44B6DCBA5CC9E6E4F41 (void);
// 0x00000619 System.String UnityEngine.SystemInfo::get_operatingSystem()
extern void SystemInfo_get_operatingSystem_mF4A5701333A7EB228DDBEF6548200F204B58BEAF (void);
// 0x0000061A UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::get_operatingSystemFamily()
extern void SystemInfo_get_operatingSystemFamily_m797937E766B7FF87A5F1630263C49B814131DD95 (void);
// 0x0000061B System.String UnityEngine.SystemInfo::get_processorType()
extern void SystemInfo_get_processorType_mA84DD6B72682E1F078486D5D8CAF9CD6E4FB9C4D (void);
// 0x0000061C System.Int32 UnityEngine.SystemInfo::get_processorFrequency()
extern void SystemInfo_get_processorFrequency_m0310D22751362753B75552EBECAE2014D1D9E08D (void);
// 0x0000061D System.Int32 UnityEngine.SystemInfo::get_processorCount()
extern void SystemInfo_get_processorCount_mBAA6A01218CF7F03638A6016B6881466830FD0D5 (void);
// 0x0000061E System.Int32 UnityEngine.SystemInfo::get_systemMemorySize()
extern void SystemInfo_get_systemMemorySize_m40E0CFB6034F14C4FF249C0940CBEC3E7F167EF0 (void);
// 0x0000061F System.String UnityEngine.SystemInfo::get_deviceUniqueIdentifier()
extern void SystemInfo_get_deviceUniqueIdentifier_m12CA3C3D8C75E44FBFA73E2E34D9E743AF732B1E (void);
// 0x00000620 System.String UnityEngine.SystemInfo::get_deviceName()
extern void SystemInfo_get_deviceName_m6F221639891D4A635206A39DB2D65584BB708425 (void);
// 0x00000621 System.String UnityEngine.SystemInfo::get_deviceModel()
extern void SystemInfo_get_deviceModel_m99131C20271BDA64F3A537AA009B252DCEDC5977 (void);
// 0x00000622 System.Boolean UnityEngine.SystemInfo::get_supportsAccelerometer()
extern void SystemInfo_get_supportsAccelerometer_m69A84B0B73391B1A816B1459A721DB9A21252DC2 (void);
// 0x00000623 System.Boolean UnityEngine.SystemInfo::get_supportsGyroscope()
extern void SystemInfo_get_supportsGyroscope_m342A5CD283DB840C10B5CEC10C997E3E7086CD57 (void);
// 0x00000624 System.Boolean UnityEngine.SystemInfo::get_supportsLocationService()
extern void SystemInfo_get_supportsLocationService_mB42FEFD42C13D7E9E6BE870C688F0673D462617D (void);
// 0x00000625 System.Boolean UnityEngine.SystemInfo::get_supportsVibration()
extern void SystemInfo_get_supportsVibration_m08CB3483DC65D45284698CD059DAD1CB776FE4CC (void);
// 0x00000626 System.Boolean UnityEngine.SystemInfo::get_supportsAudio()
extern void SystemInfo_get_supportsAudio_mEAEA1E9AF746B866A21B04764497BEF9EA69E058 (void);
// 0x00000627 UnityEngine.DeviceType UnityEngine.SystemInfo::get_deviceType()
extern void SystemInfo_get_deviceType_mC7A6628167ECFF848FE509510A6E3E2FA0820100 (void);
// 0x00000628 System.Int32 UnityEngine.SystemInfo::get_graphicsMemorySize()
extern void SystemInfo_get_graphicsMemorySize_m774717D305DC07B669D39485DEB4895500714E40 (void);
// 0x00000629 System.String UnityEngine.SystemInfo::get_graphicsDeviceName()
extern void SystemInfo_get_graphicsDeviceName_m25563DB9012D2DB5EC4CB7A29BA4236926F93F33 (void);
// 0x0000062A System.String UnityEngine.SystemInfo::get_graphicsDeviceVendor()
extern void SystemInfo_get_graphicsDeviceVendor_mA70A62E48AF223EA26B119B5428A5B488A4CA136 (void);
// 0x0000062B System.Int32 UnityEngine.SystemInfo::get_graphicsDeviceID()
extern void SystemInfo_get_graphicsDeviceID_mF25513D7FC3A94705110FA00673BEC4D8F7E8218 (void);
// 0x0000062C System.Int32 UnityEngine.SystemInfo::get_graphicsDeviceVendorID()
extern void SystemInfo_get_graphicsDeviceVendorID_mC02AC12730B431B84FE0AA74A9F7FDDBA1EEEF40 (void);
// 0x0000062D UnityEngine.Rendering.GraphicsDeviceType UnityEngine.SystemInfo::get_graphicsDeviceType()
extern void SystemInfo_get_graphicsDeviceType_mC207E6B2221AD5AB39831C2412FF7FBD2F43CC02 (void);
// 0x0000062E System.Boolean UnityEngine.SystemInfo::get_graphicsUVStartsAtTop()
extern void SystemInfo_get_graphicsUVStartsAtTop_m8A76908C20DE5B8BEE70D7E21C16EE8CC7927501 (void);
// 0x0000062F System.String UnityEngine.SystemInfo::get_graphicsDeviceVersion()
extern void SystemInfo_get_graphicsDeviceVersion_mA74AFB5D881DB29389D2BB05EB37DE60779BABD9 (void);
// 0x00000630 System.Int32 UnityEngine.SystemInfo::get_graphicsShaderLevel()
extern void SystemInfo_get_graphicsShaderLevel_m2AB377CAE1D1A45C3E05A4ABF40383E3B4797A95 (void);
// 0x00000631 System.Boolean UnityEngine.SystemInfo::get_graphicsMultiThreaded()
extern void SystemInfo_get_graphicsMultiThreaded_m4F18D961FEC9604C0C866F884B99C9FD34C58354 (void);
// 0x00000632 UnityEngine.Rendering.RenderingThreadingMode UnityEngine.SystemInfo::get_renderingThreadingMode()
extern void SystemInfo_get_renderingThreadingMode_m29074A4856EC00C30886F74167F65895AB2533A2 (void);
// 0x00000633 System.Boolean UnityEngine.SystemInfo::get_hasHiddenSurfaceRemovalOnGPU()
extern void SystemInfo_get_hasHiddenSurfaceRemovalOnGPU_m00DD0BE87F19B47150817BECFADAB712911E8376 (void);
// 0x00000634 System.Boolean UnityEngine.SystemInfo::get_hasDynamicUniformArrayIndexingInFragmentShaders()
extern void SystemInfo_get_hasDynamicUniformArrayIndexingInFragmentShaders_m7FE923312B09BD0C756A32CE38F32A67C4F49AD0 (void);
// 0x00000635 System.Boolean UnityEngine.SystemInfo::get_supportsShadows()
extern void SystemInfo_get_supportsShadows_mEBD0AB3F30C63DF888C53C50E7690D1D27C05AA3 (void);
// 0x00000636 System.Boolean UnityEngine.SystemInfo::get_supportsRawShadowDepthSampling()
extern void SystemInfo_get_supportsRawShadowDepthSampling_mC563DFC3DF5B1D04C5F619F34C344A648446406F (void);
// 0x00000637 System.Boolean UnityEngine.SystemInfo::get_supportsRenderTextures()
extern void SystemInfo_get_supportsRenderTextures_mEF4C2D195EBF734268B24BAD3599E385FC6AD4B3 (void);
// 0x00000638 System.Boolean UnityEngine.SystemInfo::get_supportsMotionVectors()
extern void SystemInfo_get_supportsMotionVectors_mC53D082851F990A59130D6C1B462D709B85E98DC (void);
// 0x00000639 System.Boolean UnityEngine.SystemInfo::get_supportsRenderToCubemap()
extern void SystemInfo_get_supportsRenderToCubemap_m05B7F9253C53DFDC1C1ABE92EE2EC855A86E8FDE (void);
// 0x0000063A System.Boolean UnityEngine.SystemInfo::get_supportsImageEffects()
extern void SystemInfo_get_supportsImageEffects_m46EC93D9C657FF09E87B8853F8A42C5E91BAD224 (void);
// 0x0000063B System.Boolean UnityEngine.SystemInfo::get_supports3DTextures()
extern void SystemInfo_get_supports3DTextures_m1341FD9262C0E2D0BF9F14BFF0192BFABBE23B77 (void);
// 0x0000063C System.Boolean UnityEngine.SystemInfo::get_supportsCompressed3DTextures()
extern void SystemInfo_get_supportsCompressed3DTextures_m415A0933067C209C0423AE06AD9D5FA072F08C48 (void);
// 0x0000063D System.Boolean UnityEngine.SystemInfo::get_supports2DArrayTextures()
extern void SystemInfo_get_supports2DArrayTextures_m08D342F9FDE4826F5D9DF545F5F9059D8954CEEC (void);
// 0x0000063E System.Boolean UnityEngine.SystemInfo::get_supports3DRenderTextures()
extern void SystemInfo_get_supports3DRenderTextures_mB649E5BE0F25585DB3BC71B5B350494ECD2DA9A3 (void);
// 0x0000063F System.Boolean UnityEngine.SystemInfo::get_supportsCubemapArrayTextures()
extern void SystemInfo_get_supportsCubemapArrayTextures_mBA529111F8762D288E759C1F42463D1EFFB1C30E (void);
// 0x00000640 UnityEngine.Rendering.CopyTextureSupport UnityEngine.SystemInfo::get_copyTextureSupport()
extern void SystemInfo_get_copyTextureSupport_m3C154FFBD2130048D9A5553E063604CC0AADC366 (void);
// 0x00000641 System.Boolean UnityEngine.SystemInfo::get_supportsComputeShaders()
extern void SystemInfo_get_supportsComputeShaders_mBEBA178F780915D8BC2FF9ED04D75FBB361987C6 (void);
// 0x00000642 System.Boolean UnityEngine.SystemInfo::get_supportsGeometryShaders()
extern void SystemInfo_get_supportsGeometryShaders_m91A2CEC097D00CD1DB5ECD6067F7E935515003EC (void);
// 0x00000643 System.Boolean UnityEngine.SystemInfo::get_supportsTessellationShaders()
extern void SystemInfo_get_supportsTessellationShaders_mE06FE458554F2950E2CA7A4E006A6181680AD4D6 (void);
// 0x00000644 System.Boolean UnityEngine.SystemInfo::get_supportsRenderTargetArrayIndexFromVertexShader()
extern void SystemInfo_get_supportsRenderTargetArrayIndexFromVertexShader_m78D7FCC1DBA677E462AE084586333EE554D411CF (void);
// 0x00000645 System.Boolean UnityEngine.SystemInfo::get_supportsInstancing()
extern void SystemInfo_get_supportsInstancing_m754DA2DF0A412E7F36D1393F9BA1E9D75EFF48C3 (void);
// 0x00000646 System.Boolean UnityEngine.SystemInfo::get_supportsHardwareQuadTopology()
extern void SystemInfo_get_supportsHardwareQuadTopology_m337808DB50ED3BDF7FB67FD35771CC949628B7A9 (void);
// 0x00000647 System.Boolean UnityEngine.SystemInfo::get_supports32bitsIndexBuffer()
extern void SystemInfo_get_supports32bitsIndexBuffer_m4D5EF729115180BAB53C0C461C352ED0FBEC9705 (void);
// 0x00000648 System.Boolean UnityEngine.SystemInfo::get_supportsSparseTextures()
extern void SystemInfo_get_supportsSparseTextures_m704A277F55EF530C32A3297AE9D8689B2F4E6254 (void);
// 0x00000649 System.Int32 UnityEngine.SystemInfo::get_supportedRenderTargetCount()
extern void SystemInfo_get_supportedRenderTargetCount_mF1878047AAED7BE6839958B61C0CC7E2FB204B44 (void);
// 0x0000064A System.Boolean UnityEngine.SystemInfo::get_supportsSeparatedRenderTargetsBlend()
extern void SystemInfo_get_supportsSeparatedRenderTargetsBlend_mD03C5AA1EB335581BEE0481ED1441D9F972B29DF (void);
// 0x0000064B System.Int32 UnityEngine.SystemInfo::get_supportedRandomWriteTargetCount()
extern void SystemInfo_get_supportedRandomWriteTargetCount_mE1A7898F7A48F47BD751352C8E7A6D10212ECBC1 (void);
// 0x0000064C System.Int32 UnityEngine.SystemInfo::get_supportsMultisampledTextures()
extern void SystemInfo_get_supportsMultisampledTextures_mFCCDF5D310228864027C1961B1058C76D80F1901 (void);
// 0x0000064D System.Boolean UnityEngine.SystemInfo::get_supportsMultisampled2DArrayTextures()
extern void SystemInfo_get_supportsMultisampled2DArrayTextures_mB77D5417CE4F68DF92E54BF3F2E6A48262110006 (void);
// 0x0000064E System.Boolean UnityEngine.SystemInfo::get_supportsMultisampleAutoResolve()
extern void SystemInfo_get_supportsMultisampleAutoResolve_mFCC3837F6419EE26E16F3162E8DB8031EBE069A5 (void);
// 0x0000064F System.Int32 UnityEngine.SystemInfo::get_supportsTextureWrapMirrorOnce()
extern void SystemInfo_get_supportsTextureWrapMirrorOnce_m74AAE160A9692B1618BAA8441027D01FEAF83557 (void);
// 0x00000650 System.Boolean UnityEngine.SystemInfo::get_usesReversedZBuffer()
extern void SystemInfo_get_usesReversedZBuffer_m89513740D4B01E6954FA0EE01816723B451BD259 (void);
// 0x00000651 System.Int32 UnityEngine.SystemInfo::get_supportsStencil()
extern void SystemInfo_get_supportsStencil_m573648A39FD8CA327643D0B7D02095D0DD7AFDE1 (void);
// 0x00000652 System.Boolean UnityEngine.SystemInfo::IsValidEnumValue(System.Enum)
extern void SystemInfo_IsValidEnumValue_mDF4AFDCB30A42032742988AD9BC5E0E00EFA86C8 (void);
// 0x00000653 System.Boolean UnityEngine.SystemInfo::SupportsRenderTextureFormat(UnityEngine.RenderTextureFormat)
extern void SystemInfo_SupportsRenderTextureFormat_m243F66021A643C711FEDEA6B6D002B49ECEEFE1B (void);
// 0x00000654 System.Boolean UnityEngine.SystemInfo::SupportsTextureFormat(UnityEngine.TextureFormat)
extern void SystemInfo_SupportsTextureFormat_mE7DA9DC2B167CB7E9A864924C8772307F1A2F0B9 (void);
// 0x00000655 UnityEngine.NPOTSupport UnityEngine.SystemInfo::get_npotSupport()
extern void SystemInfo_get_npotSupport_mB5BA13B7465E9C3EA141BB79EBD79EDF4410C191 (void);
// 0x00000656 System.Int32 UnityEngine.SystemInfo::get_maxTextureSize()
extern void SystemInfo_get_maxTextureSize_m92A710AC08A38C8BAF96D95D796C073B1C900D40 (void);
// 0x00000657 System.Int32 UnityEngine.SystemInfo::get_maxCubemapSize()
extern void SystemInfo_get_maxCubemapSize_m163BBA5799F96A38A8D60026D84D7BE5C89BA2DC (void);
// 0x00000658 System.Int32 UnityEngine.SystemInfo::get_maxComputeBufferInputsVertex()
extern void SystemInfo_get_maxComputeBufferInputsVertex_m00265BD65B4A753AC2D47D8E0FC319248E0F4478 (void);
// 0x00000659 System.Int32 UnityEngine.SystemInfo::get_maxComputeBufferInputsFragment()
extern void SystemInfo_get_maxComputeBufferInputsFragment_mE1AE69D8A293459FBF5BA7799CC7B699881D7F4C (void);
// 0x0000065A System.Int32 UnityEngine.SystemInfo::get_maxComputeBufferInputsGeometry()
extern void SystemInfo_get_maxComputeBufferInputsGeometry_m4788CF5D5E5069820EC80803900C0CB8B9CC7CFA (void);
// 0x0000065B System.Int32 UnityEngine.SystemInfo::get_maxComputeBufferInputsDomain()
extern void SystemInfo_get_maxComputeBufferInputsDomain_m1A41A05DFC5EF0405A99EAE646A5136E210AA8DC (void);
// 0x0000065C System.Int32 UnityEngine.SystemInfo::get_maxComputeBufferInputsHull()
extern void SystemInfo_get_maxComputeBufferInputsHull_m79B731A61CA0166B9CDF7DA288F9DBC412CDDAFB (void);
// 0x0000065D System.Int32 UnityEngine.SystemInfo::get_maxComputeBufferInputsCompute()
extern void SystemInfo_get_maxComputeBufferInputsCompute_m946B192F671A841A5B7EB31398A4EA91F52A0245 (void);
// 0x0000065E System.Int32 UnityEngine.SystemInfo::get_maxComputeWorkGroupSize()
extern void SystemInfo_get_maxComputeWorkGroupSize_m6287FA4911F343A8C7D05147EC0B5DD06360735E (void);
// 0x0000065F System.Int32 UnityEngine.SystemInfo::get_maxComputeWorkGroupSizeX()
extern void SystemInfo_get_maxComputeWorkGroupSizeX_m7AFB3674CC79EBE0A4D3917E357129E4DCD95063 (void);
// 0x00000660 System.Int32 UnityEngine.SystemInfo::get_maxComputeWorkGroupSizeY()
extern void SystemInfo_get_maxComputeWorkGroupSizeY_m2ECD394008E42301F4B6AD4836FA7FA193189384 (void);
// 0x00000661 System.Int32 UnityEngine.SystemInfo::get_maxComputeWorkGroupSizeZ()
extern void SystemInfo_get_maxComputeWorkGroupSizeZ_m60E55E49A7D2C4B3CE8C431E52BA2EB74C527610 (void);
// 0x00000662 System.Boolean UnityEngine.SystemInfo::get_supportsAsyncCompute()
extern void SystemInfo_get_supportsAsyncCompute_m54699BD7BEB7AF7D195964E94F982B33F1C2AC6F (void);
// 0x00000663 System.Boolean UnityEngine.SystemInfo::get_supportsGpuRecorder()
extern void SystemInfo_get_supportsGpuRecorder_mBE41C4EE6F32832CB0F0C5CCA493A04C3FB759E4 (void);
// 0x00000664 System.Boolean UnityEngine.SystemInfo::get_supportsGraphicsFence()
extern void SystemInfo_get_supportsGraphicsFence_mB1F2A268F648FF77C72070B99FBA1FA00881907A (void);
// 0x00000665 System.Boolean UnityEngine.SystemInfo::get_supportsAsyncGPUReadback()
extern void SystemInfo_get_supportsAsyncGPUReadback_mC4DFC36451A1739A0DDA4614E434657CE6B196DF (void);
// 0x00000666 System.Boolean UnityEngine.SystemInfo::get_supportsRayTracing()
extern void SystemInfo_get_supportsRayTracing_m13DB5EB131598DB2FA563B333B7CD13A9B6390D0 (void);
// 0x00000667 System.Boolean UnityEngine.SystemInfo::get_supportsSetConstantBuffer()
extern void SystemInfo_get_supportsSetConstantBuffer_m87E9386FCBDF9E64AB73FD6A5F6D1CDFA42C0A94 (void);
// 0x00000668 System.Boolean UnityEngine.SystemInfo::get_minConstantBufferOffsetAlignment()
extern void SystemInfo_get_minConstantBufferOffsetAlignment_mCB41A7EADCAB74E469CDDA9AE841A253CCF865DE (void);
// 0x00000669 System.Boolean UnityEngine.SystemInfo::get_hasMipMaxLevel()
extern void SystemInfo_get_hasMipMaxLevel_mB1B7C8C620AB827C4ADA8BC4FE1672C51D2A13CD (void);
// 0x0000066A System.Boolean UnityEngine.SystemInfo::get_supportsMipStreaming()
extern void SystemInfo_get_supportsMipStreaming_m5AC48A0069CEA79C3E9DC896EF255336C5BAAD29 (void);
// 0x0000066B System.Int32 UnityEngine.SystemInfo::get_graphicsPixelFillrate()
extern void SystemInfo_get_graphicsPixelFillrate_mBDCFF1E3B471EB873760C493E1696E2670275236 (void);
// 0x0000066C System.Boolean UnityEngine.SystemInfo::get_usesLoadStoreActions()
extern void SystemInfo_get_usesLoadStoreActions_mC70423023F6787FB3C35255E4E3D42DF0155EC62 (void);
// 0x0000066D UnityEngine.HDRDisplaySupportFlags UnityEngine.SystemInfo::get_hdrDisplaySupportFlags()
extern void SystemInfo_get_hdrDisplaySupportFlags_mD29DC579D697DBA774D883341FCD60D63164F78C (void);
// 0x0000066E System.Boolean UnityEngine.SystemInfo::get_supportsConservativeRaster()
extern void SystemInfo_get_supportsConservativeRaster_m04DBEBD8EF6ACEE88D6C0C72EA5B686FBAF1448E (void);
// 0x0000066F System.Boolean UnityEngine.SystemInfo::get_supportsVertexPrograms()
extern void SystemInfo_get_supportsVertexPrograms_m39A944A131FDBF276D5FD951A4A10C8F27C04492 (void);
// 0x00000670 System.Single UnityEngine.SystemInfo::GetBatteryLevel()
extern void SystemInfo_GetBatteryLevel_mC7AD536404709DBCBE392E60D627F5C789ABB064 (void);
// 0x00000671 UnityEngine.BatteryStatus UnityEngine.SystemInfo::GetBatteryStatus()
extern void SystemInfo_GetBatteryStatus_mB9D79AD7FA05CA452714A2A468D1F804B4664BB5 (void);
// 0x00000672 System.String UnityEngine.SystemInfo::GetOperatingSystem()
extern void SystemInfo_GetOperatingSystem_m43505157C3545D360163565BB8E9EDDCDFA17062 (void);
// 0x00000673 UnityEngine.OperatingSystemFamily UnityEngine.SystemInfo::GetOperatingSystemFamily()
extern void SystemInfo_GetOperatingSystemFamily_mA28F08DC50049D25B1C1FB0E8F5C6EF00C7FEFCD (void);
// 0x00000674 System.String UnityEngine.SystemInfo::GetProcessorType()
extern void SystemInfo_GetProcessorType_mCE5AB6E00F496E308B5C8FC37E26B8A8EEEF4A75 (void);
// 0x00000675 System.Int32 UnityEngine.SystemInfo::GetProcessorFrequencyMHz()
extern void SystemInfo_GetProcessorFrequencyMHz_mABE320F475286FBDBCBFB7DA6271C32C905C117C (void);
// 0x00000676 System.Int32 UnityEngine.SystemInfo::GetProcessorCount()
extern void SystemInfo_GetProcessorCount_m068A033A6F098AC920AA05A54F136876A9F387D1 (void);
// 0x00000677 System.Int32 UnityEngine.SystemInfo::GetPhysicalMemoryMB()
extern void SystemInfo_GetPhysicalMemoryMB_m4131DA4696B7AC99F56545D91881E55D510E9659 (void);
// 0x00000678 System.String UnityEngine.SystemInfo::GetDeviceUniqueIdentifier()
extern void SystemInfo_GetDeviceUniqueIdentifier_mEFC98563366CED1C69EDDC6D5608D6BA8A4FC753 (void);
// 0x00000679 System.String UnityEngine.SystemInfo::GetDeviceName()
extern void SystemInfo_GetDeviceName_m8461B7A550FFF7711555938D487779B734F663C3 (void);
// 0x0000067A System.String UnityEngine.SystemInfo::GetDeviceModel()
extern void SystemInfo_GetDeviceModel_m72051C6D2BBAAEFB8AEE4212E175D3C85045E49E (void);
// 0x0000067B System.Boolean UnityEngine.SystemInfo::SupportsAccelerometer()
extern void SystemInfo_SupportsAccelerometer_m281D6BCE55B08C3B12035512F982A632FE2F3CB2 (void);
// 0x0000067C System.Boolean UnityEngine.SystemInfo::IsGyroAvailable()
extern void SystemInfo_IsGyroAvailable_m9F8DCB923F69916AB300C06E1A77E5885F47805F (void);
// 0x0000067D System.Boolean UnityEngine.SystemInfo::SupportsLocationService()
extern void SystemInfo_SupportsLocationService_m61FC0DE7CE79AF165DCF60679C49E2FDFBBF7242 (void);
// 0x0000067E System.Boolean UnityEngine.SystemInfo::SupportsVibration()
extern void SystemInfo_SupportsVibration_m7EAAA4B0741D8BA5C50AF39962E602F5250F7CB8 (void);
// 0x0000067F System.Boolean UnityEngine.SystemInfo::SupportsAudio()
extern void SystemInfo_SupportsAudio_m0B4FE034E202629F3F383CBF40E2EBF09EF32148 (void);
// 0x00000680 UnityEngine.DeviceType UnityEngine.SystemInfo::GetDeviceType()
extern void SystemInfo_GetDeviceType_m0FA185D60B7A50C01A3AFB22AAE47C585E0F6BAB (void);
// 0x00000681 System.Int32 UnityEngine.SystemInfo::GetGraphicsMemorySize()
extern void SystemInfo_GetGraphicsMemorySize_mA3DC96C958BD9F685C3A9A112A22AC4CACE7A2B3 (void);
// 0x00000682 System.String UnityEngine.SystemInfo::GetGraphicsDeviceName()
extern void SystemInfo_GetGraphicsDeviceName_mFD99DAEC5AF5CEF9E5BD5297DA94A0A79DF5EE9C (void);
// 0x00000683 System.String UnityEngine.SystemInfo::GetGraphicsDeviceVendor()
extern void SystemInfo_GetGraphicsDeviceVendor_mCE52C892914AD6FC791F88DD7AE2ACE28E641863 (void);
// 0x00000684 System.Int32 UnityEngine.SystemInfo::GetGraphicsDeviceID()
extern void SystemInfo_GetGraphicsDeviceID_m3E4C5B6836E823CAE2A53BDD34C61DF78ADC473A (void);
// 0x00000685 System.Int32 UnityEngine.SystemInfo::GetGraphicsDeviceVendorID()
extern void SystemInfo_GetGraphicsDeviceVendorID_mB3EC171381D1AE243B10C5BB0F6690E07E8F67F0 (void);
// 0x00000686 UnityEngine.Rendering.GraphicsDeviceType UnityEngine.SystemInfo::GetGraphicsDeviceType()
extern void SystemInfo_GetGraphicsDeviceType_m8E30DF86F5AE77EBB1752A55B183890C3ACA8443 (void);
// 0x00000687 System.Boolean UnityEngine.SystemInfo::GetGraphicsUVStartsAtTop()
extern void SystemInfo_GetGraphicsUVStartsAtTop_m05A4A723EEB7FBCB3E55C3A417AA03AF0B021E4D (void);
// 0x00000688 System.String UnityEngine.SystemInfo::GetGraphicsDeviceVersion()
extern void SystemInfo_GetGraphicsDeviceVersion_m0AB62B4AC54343A28B8CAA0AEE59B22E281F1D71 (void);
// 0x00000689 System.Int32 UnityEngine.SystemInfo::GetGraphicsShaderLevel()
extern void SystemInfo_GetGraphicsShaderLevel_m5FA0FBC7F9E2EB33F845D4481BBD2E703FDC9774 (void);
// 0x0000068A System.Boolean UnityEngine.SystemInfo::GetGraphicsMultiThreaded()
extern void SystemInfo_GetGraphicsMultiThreaded_m1D5B91AD64CB6959974C0900983F93C5F4B3EF22 (void);
// 0x0000068B UnityEngine.Rendering.RenderingThreadingMode UnityEngine.SystemInfo::GetRenderingThreadingMode()
extern void SystemInfo_GetRenderingThreadingMode_m682EEFF66E4F688ACA133E371FF891F8D30C3F85 (void);
// 0x0000068C System.Boolean UnityEngine.SystemInfo::HasHiddenSurfaceRemovalOnGPU()
extern void SystemInfo_HasHiddenSurfaceRemovalOnGPU_m1A886FAE9CA31DD2C7102C8ECCC715D04B01A2E3 (void);
// 0x0000068D System.Boolean UnityEngine.SystemInfo::HasDynamicUniformArrayIndexingInFragmentShaders()
extern void SystemInfo_HasDynamicUniformArrayIndexingInFragmentShaders_m3699977B68DBBEC76C54608D887B0B42EBC52B35 (void);
// 0x0000068E System.Boolean UnityEngine.SystemInfo::SupportsShadows()
extern void SystemInfo_SupportsShadows_m32C81C01678FE781DB83DE30138DD6CAC58A3436 (void);
// 0x0000068F System.Boolean UnityEngine.SystemInfo::SupportsRawShadowDepthSampling()
extern void SystemInfo_SupportsRawShadowDepthSampling_mF8A5C69333BDBE965E65BA0D0219E3D0B93EB5B9 (void);
// 0x00000690 System.Boolean UnityEngine.SystemInfo::SupportsMotionVectors()
extern void SystemInfo_SupportsMotionVectors_m1C2121EDB64B43A0EABDD42C933F127B5002FE8B (void);
// 0x00000691 System.Boolean UnityEngine.SystemInfo::Supports3DTextures()
extern void SystemInfo_Supports3DTextures_m592B058E2C3B1555B82F9CC819B7556D991B2B3A (void);
// 0x00000692 System.Boolean UnityEngine.SystemInfo::SupportsCompressed3DTextures()
extern void SystemInfo_SupportsCompressed3DTextures_m335562164806358C9F2A3646501FEEF3EA7F9800 (void);
// 0x00000693 System.Boolean UnityEngine.SystemInfo::Supports2DArrayTextures()
extern void SystemInfo_Supports2DArrayTextures_m4945FB9A31EC8ACD1AA625D81C10E994CFF183AB (void);
// 0x00000694 System.Boolean UnityEngine.SystemInfo::Supports3DRenderTextures()
extern void SystemInfo_Supports3DRenderTextures_m7833E0995D300986C7AF65B2B0BF4FC70E4DF68F (void);
// 0x00000695 System.Boolean UnityEngine.SystemInfo::SupportsCubemapArrayTextures()
extern void SystemInfo_SupportsCubemapArrayTextures_m76069C3C8035FA1FC2B17E8D881079E79BEF4E26 (void);
// 0x00000696 UnityEngine.Rendering.CopyTextureSupport UnityEngine.SystemInfo::GetCopyTextureSupport()
extern void SystemInfo_GetCopyTextureSupport_m65B2F7B818C998EF26B7E497B9EC307AB2C89FB0 (void);
// 0x00000697 System.Boolean UnityEngine.SystemInfo::SupportsComputeShaders()
extern void SystemInfo_SupportsComputeShaders_mCFF50395E4EC20377D30E391E412B434798B0B5A (void);
// 0x00000698 System.Boolean UnityEngine.SystemInfo::SupportsGeometryShaders()
extern void SystemInfo_SupportsGeometryShaders_mC7508CCCDF1C21C0F0A15E2059462CA030D9E7D6 (void);
// 0x00000699 System.Boolean UnityEngine.SystemInfo::SupportsTessellationShaders()
extern void SystemInfo_SupportsTessellationShaders_m399FD1050199877B83B1AFF6793D859E04FC087F (void);
// 0x0000069A System.Boolean UnityEngine.SystemInfo::SupportsRenderTargetArrayIndexFromVertexShader()
extern void SystemInfo_SupportsRenderTargetArrayIndexFromVertexShader_m281AE541D587F7EE4619FC377FE0AD804DDB0D5A (void);
// 0x0000069B System.Boolean UnityEngine.SystemInfo::SupportsInstancing()
extern void SystemInfo_SupportsInstancing_mBE34CD192E396397325D952C07B8BE73933173F2 (void);
// 0x0000069C System.Boolean UnityEngine.SystemInfo::SupportsHardwareQuadTopology()
extern void SystemInfo_SupportsHardwareQuadTopology_m4F53B514058B7D60B4F7F2B836CBD90BB25A8000 (void);
// 0x0000069D System.Boolean UnityEngine.SystemInfo::Supports32bitsIndexBuffer()
extern void SystemInfo_Supports32bitsIndexBuffer_mF7CA00A1DDC18C9EC8AA423FE8A29D78E548C5A8 (void);
// 0x0000069E System.Boolean UnityEngine.SystemInfo::SupportsSparseTextures()
extern void SystemInfo_SupportsSparseTextures_m5A896E23280539E34139FC148A8C9515129AA879 (void);
// 0x0000069F System.Int32 UnityEngine.SystemInfo::SupportedRenderTargetCount()
extern void SystemInfo_SupportedRenderTargetCount_mC0531AA9339B4BE7D7FEF57C4716560DD09A8F17 (void);
// 0x000006A0 System.Boolean UnityEngine.SystemInfo::SupportsSeparatedRenderTargetsBlend()
extern void SystemInfo_SupportsSeparatedRenderTargetsBlend_m49D9FD7844682EA0F445B0617C3D7518ED536B56 (void);
// 0x000006A1 System.Int32 UnityEngine.SystemInfo::SupportedRandomWriteTargetCount()
extern void SystemInfo_SupportedRandomWriteTargetCount_m0D8FEF1A53DA52747D072898F0A7FF02158DFDEA (void);
// 0x000006A2 System.Int32 UnityEngine.SystemInfo::MaxComputeBufferInputsVertex()
extern void SystemInfo_MaxComputeBufferInputsVertex_mAC4976451226A32E82E1703506A042B9AA89D0A8 (void);
// 0x000006A3 System.Int32 UnityEngine.SystemInfo::MaxComputeBufferInputsFragment()
extern void SystemInfo_MaxComputeBufferInputsFragment_m39AC4CB4FB9202A71F7247CFB220C216BEC498DA (void);
// 0x000006A4 System.Int32 UnityEngine.SystemInfo::MaxComputeBufferInputsGeometry()
extern void SystemInfo_MaxComputeBufferInputsGeometry_m98DED828662FE955597757899FA785EAC3924BC4 (void);
// 0x000006A5 System.Int32 UnityEngine.SystemInfo::MaxComputeBufferInputsDomain()
extern void SystemInfo_MaxComputeBufferInputsDomain_mDBACD7DC42E017D2C7273CDE441063881CEAF787 (void);
// 0x000006A6 System.Int32 UnityEngine.SystemInfo::MaxComputeBufferInputsHull()
extern void SystemInfo_MaxComputeBufferInputsHull_m1F241A825D0514C96839FFC4B0D51179F94E2EF9 (void);
// 0x000006A7 System.Int32 UnityEngine.SystemInfo::MaxComputeBufferInputsCompute()
extern void SystemInfo_MaxComputeBufferInputsCompute_mA76DDF21EC4973FC89A14B06B12B900871EA8A60 (void);
// 0x000006A8 System.Int32 UnityEngine.SystemInfo::SupportsMultisampledTextures()
extern void SystemInfo_SupportsMultisampledTextures_mDFF30091256075AE6D87F899521F454B45EF60B4 (void);
// 0x000006A9 System.Boolean UnityEngine.SystemInfo::SupportsMultisampled2DArrayTextures()
extern void SystemInfo_SupportsMultisampled2DArrayTextures_m0D8DB9C8B25F2288F7BAB8B14E2A00A1877372A0 (void);
// 0x000006AA System.Boolean UnityEngine.SystemInfo::SupportsMultisampleAutoResolve()
extern void SystemInfo_SupportsMultisampleAutoResolve_m68A929965B7FDFA3157309A78A13CB6DF9A4AD81 (void);
// 0x000006AB System.Int32 UnityEngine.SystemInfo::SupportsTextureWrapMirrorOnce()
extern void SystemInfo_SupportsTextureWrapMirrorOnce_m81A08C77E2955F724704F315B3902F8CBB37958C (void);
// 0x000006AC System.Boolean UnityEngine.SystemInfo::UsesReversedZBuffer()
extern void SystemInfo_UsesReversedZBuffer_m35DBEC617F6E8926ED483E06CC5CA4433606C4E0 (void);
// 0x000006AD System.Boolean UnityEngine.SystemInfo::HasRenderTextureNative(UnityEngine.RenderTextureFormat)
extern void SystemInfo_HasRenderTextureNative_mCAFDC38C167B083925D7F546A1AE1053AFE9626E (void);
// 0x000006AE System.Boolean UnityEngine.SystemInfo::SupportsTextureFormatNative(UnityEngine.TextureFormat)
extern void SystemInfo_SupportsTextureFormatNative_m1514BFE543D7EE39CEF43B429B52E2EC20AB8E75 (void);
// 0x000006AF UnityEngine.NPOTSupport UnityEngine.SystemInfo::GetNPOTSupport()
extern void SystemInfo_GetNPOTSupport_m039DB2B1D26432DF11CAC86D790EC6F5DCBBC12F (void);
// 0x000006B0 System.Int32 UnityEngine.SystemInfo::GetMaxTextureSize()
extern void SystemInfo_GetMaxTextureSize_m75E58620AC261FD77800B8D94FA3865F55A3A906 (void);
// 0x000006B1 System.Int32 UnityEngine.SystemInfo::GetMaxCubemapSize()
extern void SystemInfo_GetMaxCubemapSize_m28676F0ED1FFC3168EBD04FA1C782E14C7411C05 (void);
// 0x000006B2 System.Int32 UnityEngine.SystemInfo::GetMaxComputeWorkGroupSize()
extern void SystemInfo_GetMaxComputeWorkGroupSize_m96B34356E4F25BD5845BE11FCB11F9B755700C8C (void);
// 0x000006B3 System.Int32 UnityEngine.SystemInfo::GetMaxComputeWorkGroupSizeX()
extern void SystemInfo_GetMaxComputeWorkGroupSizeX_mF7C75E96C7AAFF457B18A383861BED022A25A550 (void);
// 0x000006B4 System.Int32 UnityEngine.SystemInfo::GetMaxComputeWorkGroupSizeY()
extern void SystemInfo_GetMaxComputeWorkGroupSizeY_m0BCDEECB257B654F928C72CA4AE512A8CFE53F08 (void);
// 0x000006B5 System.Int32 UnityEngine.SystemInfo::GetMaxComputeWorkGroupSizeZ()
extern void SystemInfo_GetMaxComputeWorkGroupSizeZ_mC3712B2429A16B5459EFA04EE9D3258B595F51B7 (void);
// 0x000006B6 System.Boolean UnityEngine.SystemInfo::SupportsAsyncCompute()
extern void SystemInfo_SupportsAsyncCompute_mD3C93816DFB853CF3E229CE3533FD68AA5514A8E (void);
// 0x000006B7 System.Boolean UnityEngine.SystemInfo::SupportsGpuRecorder()
extern void SystemInfo_SupportsGpuRecorder_m90284551360D6358431B6FAB466A389CC6EA2D0B (void);
// 0x000006B8 System.Boolean UnityEngine.SystemInfo::SupportsGPUFence()
extern void SystemInfo_SupportsGPUFence_mF2432380CAD096A5EAA1966BE656D2C2715BBD2A (void);
// 0x000006B9 System.Boolean UnityEngine.SystemInfo::SupportsAsyncGPUReadback()
extern void SystemInfo_SupportsAsyncGPUReadback_m1EC85AEF71882930BAE9149FEF51C753E8AC7400 (void);
// 0x000006BA System.Boolean UnityEngine.SystemInfo::SupportsRayTracing()
extern void SystemInfo_SupportsRayTracing_mBAC93D4F93BCA615F3C560A00A41F2949FDEDD86 (void);
// 0x000006BB System.Boolean UnityEngine.SystemInfo::SupportsSetConstantBuffer()
extern void SystemInfo_SupportsSetConstantBuffer_mBDD353A1F98DFD1A16D574B0E21C408973A9612C (void);
// 0x000006BC System.Boolean UnityEngine.SystemInfo::MinConstantBufferOffsetAlignment()
extern void SystemInfo_MinConstantBufferOffsetAlignment_mDF1F9AA4FD7FF4632750BF3F336A97B32BDFAB75 (void);
// 0x000006BD System.Boolean UnityEngine.SystemInfo::HasMipMaxLevel()
extern void SystemInfo_HasMipMaxLevel_m3E6616FF2CBCD69A497E10D624A2B9ED37E072FF (void);
// 0x000006BE System.Boolean UnityEngine.SystemInfo::SupportsMipStreaming()
extern void SystemInfo_SupportsMipStreaming_m7CAB18917327CC364D7EA0A754F6CEFABFF8F484 (void);
// 0x000006BF System.Boolean UnityEngine.SystemInfo::IsFormatSupported(UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.FormatUsage)
extern void SystemInfo_IsFormatSupported_m03EDA316B42377504BA47B256EB094F8A54BC75C (void);
// 0x000006C0 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.SystemInfo::GetCompatibleFormat(UnityEngine.Experimental.Rendering.GraphicsFormat,UnityEngine.Experimental.Rendering.FormatUsage)
extern void SystemInfo_GetCompatibleFormat_m02B5C5B1F3836661A92F3C83E44B66729C03B228 (void);
// 0x000006C1 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.SystemInfo::GetGraphicsFormat(UnityEngine.Experimental.Rendering.DefaultFormat)
extern void SystemInfo_GetGraphicsFormat_mE36FE85F87F085503FEAA34112E454E9F2AFEF55 (void);
// 0x000006C2 System.Boolean UnityEngine.SystemInfo::UsesLoadStoreActions()
extern void SystemInfo_UsesLoadStoreActions_mCA7356EC82B95D4C3897221CFDB8908C68553E4B (void);
// 0x000006C3 UnityEngine.HDRDisplaySupportFlags UnityEngine.SystemInfo::GetHDRDisplaySupportFlags()
extern void SystemInfo_GetHDRDisplaySupportFlags_m2F31E0179C6A8B9E6D6D16AF132FE4C507A77E9D (void);
// 0x000006C4 System.Boolean UnityEngine.SystemInfo::SupportsConservativeRaster()
extern void SystemInfo_SupportsConservativeRaster_m515C655351F5973A04F78917A09CE46F3154EE28 (void);
// 0x000006C5 System.Boolean UnityEngine.SystemInfo::get_supportsGPUFence()
extern void SystemInfo_get_supportsGPUFence_m785FB762A86266D6F2D869FED86105D2234DBC66 (void);
// 0x000006C6 System.Void UnityEngine.SystemInfo::.ctor()
extern void SystemInfo__ctor_m8F4A5CF956C47EFC15614BF2C14CD18CFFA5C111 (void);
// 0x000006C7 System.Single UnityEngine.Time::get_time()
extern void Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844 (void);
// 0x000006C8 System.Single UnityEngine.Time::get_timeSinceLevelLoad()
extern void Time_get_timeSinceLevelLoad_m47A90DE6CB3A3180D64F0049290BC72C186FC7FB (void);
// 0x000006C9 System.Single UnityEngine.Time::get_deltaTime()
extern void Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (void);
// 0x000006CA System.Single UnityEngine.Time::get_fixedTime()
extern void Time_get_fixedTime_mE4630B2127C67597CA450A81FFDF25E63A2CC7B5 (void);
// 0x000006CB System.Single UnityEngine.Time::get_unscaledTime()
extern void Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258 (void);
// 0x000006CC System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern void Time_get_unscaledDeltaTime_m2C153F1E5C77C6AF655054BC6C76D0C334C0DC84 (void);
// 0x000006CD System.Single UnityEngine.Time::get_fixedDeltaTime()
extern void Time_get_fixedDeltaTime_m8E94ECFF6A6A1D9B5D60BF82D116D540852484E5 (void);
// 0x000006CE System.Single UnityEngine.Time::get_smoothDeltaTime()
extern void Time_get_smoothDeltaTime_mA6EAFF9940EC75075565444A61A03895489CA96A (void);
// 0x000006CF System.Single UnityEngine.Time::get_timeScale()
extern void Time_get_timeScale_m082A05928ED5917AA986FAA6106E79D8446A26F4 (void);
// 0x000006D0 System.Void UnityEngine.Time::set_timeScale(System.Single)
extern void Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA (void);
// 0x000006D1 System.Int32 UnityEngine.Time::get_frameCount()
extern void Time_get_frameCount_m8601F5FB5B701680076B40D2F31405F304D963F0 (void);
// 0x000006D2 System.Single UnityEngine.Time::get_realtimeSinceStartup()
extern void Time_get_realtimeSinceStartup_m5228CC1C1E57213D4148E965499072EA70D8C02B (void);
// 0x000006D3 System.Void UnityEngine.TouchScreenKeyboard::Internal_Destroy(System.IntPtr)
extern void TouchScreenKeyboard_Internal_Destroy_m59FBAD63BC41007D106FA59C3378D547F67CA00D (void);
// 0x000006D4 System.Void UnityEngine.TouchScreenKeyboard::Destroy()
extern void TouchScreenKeyboard_Destroy_m2FFBCD2EF26EF68B394874335BA6DA21B95F65D2 (void);
// 0x000006D5 System.Void UnityEngine.TouchScreenKeyboard::Finalize()
extern void TouchScreenKeyboard_Finalize_m3C44228F58044B8132724CF9BD1A1B2354EBB76E (void);
// 0x000006D6 System.Void UnityEngine.TouchScreenKeyboard::.ctor(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.Int32)
extern void TouchScreenKeyboard__ctor_mA82A33DB603000BB9373F70744D0774BAD5714F4 (void);
// 0x000006D7 System.IntPtr UnityEngine.TouchScreenKeyboard::TouchScreenKeyboard_InternalConstructorHelper(UnityEngine.TouchScreenKeyboard_InternalConstructorHelperArguments&,System.String,System.String)
extern void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_mAAF0AC4D0E6D25AAFC9F71BF09447E053261EADB (void);
// 0x000006D8 System.Boolean UnityEngine.TouchScreenKeyboard::get_isSupported()
extern void TouchScreenKeyboard_get_isSupported_m0DB9F5600113241DD766588D28192A62185C158F (void);
// 0x000006D9 System.Boolean UnityEngine.TouchScreenKeyboard::get_isInPlaceEditingAllowed()
extern void TouchScreenKeyboard_get_isInPlaceEditingAllowed_m8364EE991616DCA6A1BDDA598F93D577B68491FC (void);
// 0x000006DA System.Boolean UnityEngine.TouchScreenKeyboard::IsInPlaceEditingAllowed()
extern void TouchScreenKeyboard_IsInPlaceEditingAllowed_m8328A436685488B94FDBE2E99F3E7992C3977ECC (void);
// 0x000006DB UnityEngine.TouchScreenKeyboard UnityEngine.TouchScreenKeyboard::Open(System.String,UnityEngine.TouchScreenKeyboardType,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.Int32)
extern void TouchScreenKeyboard_Open_mE7311250DC20FBA07392E4F61B71212437956B6E (void);
// 0x000006DC System.String UnityEngine.TouchScreenKeyboard::get_text()
extern void TouchScreenKeyboard_get_text_m46603E258E098841D53FE33A6D367A1169BDECA4 (void);
// 0x000006DD System.Void UnityEngine.TouchScreenKeyboard::set_text(System.String)
extern void TouchScreenKeyboard_set_text_m8BA9BBE790EA59FFE1E55FE25BD05E85CEEE7A27 (void);
// 0x000006DE System.Void UnityEngine.TouchScreenKeyboard::set_hideInput(System.Boolean)
extern void TouchScreenKeyboard_set_hideInput_m7A3F11FC569433CF00F71284991849E72E934D6F (void);
// 0x000006DF System.Boolean UnityEngine.TouchScreenKeyboard::get_active()
extern void TouchScreenKeyboard_get_active_m07DBA2A13D1062188AB6BE05BAA61C90197E55E2 (void);
// 0x000006E0 System.Void UnityEngine.TouchScreenKeyboard::set_active(System.Boolean)
extern void TouchScreenKeyboard_set_active_m506FA44E4FA49466735258D0257AC14AAC6AC245 (void);
// 0x000006E1 UnityEngine.TouchScreenKeyboard_Status UnityEngine.TouchScreenKeyboard::get_status()
extern void TouchScreenKeyboard_get_status_m05FBF0EF6E13308E24CDCD4259F0A532040F08D9 (void);
// 0x000006E2 System.Void UnityEngine.TouchScreenKeyboard::set_characterLimit(System.Int32)
extern void TouchScreenKeyboard_set_characterLimit_mE662ED65DD8BF31608A1E0C697053622893EC9DC (void);
// 0x000006E3 System.Boolean UnityEngine.TouchScreenKeyboard::get_canGetSelection()
extern void TouchScreenKeyboard_get_canGetSelection_m979FF4BC5D792F38CD9814DB2603EFA67C88EFF8 (void);
// 0x000006E4 System.Boolean UnityEngine.TouchScreenKeyboard::get_canSetSelection()
extern void TouchScreenKeyboard_get_canSetSelection_mC75BB2BE09235F3B8BD5805C5D8F1097C3AAD442 (void);
// 0x000006E5 UnityEngine.RangeInt UnityEngine.TouchScreenKeyboard::get_selection()
extern void TouchScreenKeyboard_get_selection_m3C092ED46B21E0C7BD694F5E9F2C7529F9D123E3 (void);
// 0x000006E6 System.Void UnityEngine.TouchScreenKeyboard::set_selection(UnityEngine.RangeInt)
extern void TouchScreenKeyboard_set_selection_mB53A2F70AAD20505589F58A61A086777BA8645AD (void);
// 0x000006E7 System.Void UnityEngine.TouchScreenKeyboard::GetSelection(System.Int32&,System.Int32&)
extern void TouchScreenKeyboard_GetSelection_mE5F74F635FED7B7E2CA492AEB5B83EC316EB4E0E (void);
// 0x000006E8 System.Void UnityEngine.TouchScreenKeyboard::SetSelection(System.Int32,System.Int32)
extern void TouchScreenKeyboard_SetSelection_mE48DEBFF4B65FD885A3A6C8009D61F086D758DC4 (void);
// 0x000006E9 System.Void UnityEngine.Pose::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void Pose__ctor_m57138889AE9BF5AFB50D31A007F6EE062991E8C9_AdjustorThunk (void);
// 0x000006EA System.String UnityEngine.Pose::ToString()
extern void Pose_ToString_m9958D8F32A381D4ADB8DEABDE63561F75A12962B_AdjustorThunk (void);
// 0x000006EB System.Boolean UnityEngine.Pose::Equals(System.Object)
extern void Pose_Equals_m93B87D733C2FBE5B400140040826521DCA4A2BB5_AdjustorThunk (void);
// 0x000006EC System.Boolean UnityEngine.Pose::Equals(UnityEngine.Pose)
extern void Pose_Equals_mFAE0041090F2032B8FCE63A0F289DD206134BF15_AdjustorThunk (void);
// 0x000006ED System.Int32 UnityEngine.Pose::GetHashCode()
extern void Pose_GetHashCode_mE1DD7FBCCB1C979B252F0B117276BA11CF4D2367_AdjustorThunk (void);
// 0x000006EE System.Void UnityEngine.Pose::.cctor()
extern void Pose__cctor_m4E5498207D9033E7DB4B1C255ABDBE8F746F89F8 (void);
// 0x000006EF System.Void UnityEngine.DrivenRectTransformTracker::Add(UnityEngine.Object,UnityEngine.RectTransform,UnityEngine.DrivenTransformProperties)
extern void DrivenRectTransformTracker_Add_m65814604ABCE8B9F81270F3C2E1632CCB9E9A5E7_AdjustorThunk (void);
// 0x000006F0 System.Void UnityEngine.DrivenRectTransformTracker::Clear()
extern void DrivenRectTransformTracker_Clear_m41F9B0AA2025AF5B76D38E68B08C111D7D8EB845_AdjustorThunk (void);
// 0x000006F1 System.Void UnityEngine.RectTransform::add_reapplyDrivenProperties(UnityEngine.RectTransform_ReapplyDrivenProperties)
extern void RectTransform_add_reapplyDrivenProperties_mCD8CB43C59C3C04528C842E4640AD1DC5B71F043 (void);
// 0x000006F2 System.Void UnityEngine.RectTransform::remove_reapplyDrivenProperties(UnityEngine.RectTransform_ReapplyDrivenProperties)
extern void RectTransform_remove_reapplyDrivenProperties_m2F771726CC09F7CF3E9194B72D87ABB3B61001F1 (void);
// 0x000006F3 UnityEngine.Rect UnityEngine.RectTransform::get_rect()
extern void RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2 (void);
// 0x000006F4 UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMin()
extern void RectTransform_get_anchorMin_m5CBB2E649A3D4234A7A5A16B1BBAADAC9C033319 (void);
// 0x000006F5 System.Void UnityEngine.RectTransform::set_anchorMin(UnityEngine.Vector2)
extern void RectTransform_set_anchorMin_mD9E6E95890B701A5190C12F5AE42E622246AF798 (void);
// 0x000006F6 UnityEngine.Vector2 UnityEngine.RectTransform::get_anchorMax()
extern void RectTransform_get_anchorMax_mC1577047A20870209C9A6801B75FE6930AE56F1E (void);
// 0x000006F7 System.Void UnityEngine.RectTransform::set_anchorMax(UnityEngine.Vector2)
extern void RectTransform_set_anchorMax_m67E04F54B5122804E32019D5FAE50C21CC67651D (void);
// 0x000006F8 UnityEngine.Vector2 UnityEngine.RectTransform::get_anchoredPosition()
extern void RectTransform_get_anchoredPosition_mFDC4F160F99634B2FBC73FE5FB1F4F4127CDD975 (void);
// 0x000006F9 System.Void UnityEngine.RectTransform::set_anchoredPosition(UnityEngine.Vector2)
extern void RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905 (void);
// 0x000006FA UnityEngine.Vector2 UnityEngine.RectTransform::get_sizeDelta()
extern void RectTransform_get_sizeDelta_mCFAE8C916280C173AB79BE32B910376E310D1C50 (void);
// 0x000006FB System.Void UnityEngine.RectTransform::set_sizeDelta(UnityEngine.Vector2)
extern void RectTransform_set_sizeDelta_m61943618442E31C6FF0556CDFC70940AE7AD04D0 (void);
// 0x000006FC UnityEngine.Vector2 UnityEngine.RectTransform::get_pivot()
extern void RectTransform_get_pivot_m146F0BB5D3873FCEF3606DAFB8994BFA978095EE (void);
// 0x000006FD System.Void UnityEngine.RectTransform::set_pivot(UnityEngine.Vector2)
extern void RectTransform_set_pivot_m94F32EF88DC4EC9CA96721F8EDD8BFBC4FD07335 (void);
// 0x000006FE UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMin()
extern void RectTransform_get_offsetMin_m6573EC7E31B25028A35C21D4195D96EA94645E5C (void);
// 0x000006FF System.Void UnityEngine.RectTransform::set_offsetMin(UnityEngine.Vector2)
extern void RectTransform_set_offsetMin_m86D7818770137C150B70A3842EBF03F494C34271 (void);
// 0x00000700 UnityEngine.Vector2 UnityEngine.RectTransform::get_offsetMax()
extern void RectTransform_get_offsetMax_m27465B00CD222399CC8B3BB53A4A68CF342319C6 (void);
// 0x00000701 System.Void UnityEngine.RectTransform::set_offsetMax(UnityEngine.Vector2)
extern void RectTransform_set_offsetMax_m5FDE1063C8BA1EC98D3C57C58DD2A1B9B721A8BF (void);
// 0x00000702 System.Void UnityEngine.RectTransform::GetLocalCorners(UnityEngine.Vector3[])
extern void RectTransform_GetLocalCorners_mA93C3DA0EF4915A399E111F23E8B0037FB0D897C (void);
// 0x00000703 System.Void UnityEngine.RectTransform::GetWorldCorners(UnityEngine.Vector3[])
extern void RectTransform_GetWorldCorners_m5351A825540654FFDBD0837AC37D2139F64A4FD8 (void);
// 0x00000704 System.Void UnityEngine.RectTransform::SetSizeWithCurrentAnchors(UnityEngine.RectTransform_Axis,System.Single)
extern void RectTransform_SetSizeWithCurrentAnchors_m69641A375B011EA52C69C5E2553406FFB819F44B (void);
// 0x00000705 System.Void UnityEngine.RectTransform::SendReapplyDrivenProperties(UnityEngine.RectTransform)
extern void RectTransform_SendReapplyDrivenProperties_m9139950FCE6E3C596110C5266174D8B2E56DC9CD (void);
// 0x00000706 UnityEngine.Vector2 UnityEngine.RectTransform::GetParentSize()
extern void RectTransform_GetParentSize_mB360151D47F306B0614F87B85402156C8FD949D5 (void);
// 0x00000707 System.Void UnityEngine.RectTransform::get_rect_Injected(UnityEngine.Rect&)
extern void RectTransform_get_rect_Injected_m9E2423A68A47664E62278AF461D5F5E8444E3E63 (void);
// 0x00000708 System.Void UnityEngine.RectTransform::get_anchorMin_Injected(UnityEngine.Vector2&)
extern void RectTransform_get_anchorMin_Injected_m591E30B205148C8EE6B40DEFF59C26578B269E86 (void);
// 0x00000709 System.Void UnityEngine.RectTransform::set_anchorMin_Injected(UnityEngine.Vector2&)
extern void RectTransform_set_anchorMin_Injected_mE7DC3F6291CD07ECE04F3E602395B1E8C841B9DB (void);
// 0x0000070A System.Void UnityEngine.RectTransform::get_anchorMax_Injected(UnityEngine.Vector2&)
extern void RectTransform_get_anchorMax_Injected_m66E954822B58B90A6C0BCF215BF8ADECF2AE82A5 (void);
// 0x0000070B System.Void UnityEngine.RectTransform::set_anchorMax_Injected(UnityEngine.Vector2&)
extern void RectTransform_set_anchorMax_Injected_m5C4650BC3A0CB3F5B9BB42020ED98310ED217D9F (void);
// 0x0000070C System.Void UnityEngine.RectTransform::get_anchoredPosition_Injected(UnityEngine.Vector2&)
extern void RectTransform_get_anchoredPosition_Injected_mEAE78E52E8C07DF7C3FD72FC31E675557B7D2D21 (void);
// 0x0000070D System.Void UnityEngine.RectTransform::set_anchoredPosition_Injected(UnityEngine.Vector2&)
extern void RectTransform_set_anchoredPosition_Injected_m5F082F2C7BECB268DD87C04857157E2C50C44FB9 (void);
// 0x0000070E System.Void UnityEngine.RectTransform::get_sizeDelta_Injected(UnityEngine.Vector2&)
extern void RectTransform_get_sizeDelta_Injected_mCDC20F4A6886D32FD2450EF690EA8B067769C093 (void);
// 0x0000070F System.Void UnityEngine.RectTransform::set_sizeDelta_Injected(UnityEngine.Vector2&)
extern void RectTransform_set_sizeDelta_Injected_m7693B136F6C2F35B06D21E813FE4D90007D0FCEA (void);
// 0x00000710 System.Void UnityEngine.RectTransform::get_pivot_Injected(UnityEngine.Vector2&)
extern void RectTransform_get_pivot_Injected_m37B7AD78DD72F2A181EC5B06AB9499EB11D20EB3 (void);
// 0x00000711 System.Void UnityEngine.RectTransform::set_pivot_Injected(UnityEngine.Vector2&)
extern void RectTransform_set_pivot_Injected_m9FE95D2C721B381940FCDA8D202B3A3AC5B03B56 (void);
// 0x00000712 System.Void UnityEngine.RectTransform_ReapplyDrivenProperties::.ctor(System.Object,System.IntPtr)
extern void ReapplyDrivenProperties__ctor_mD584B5E4A07E3D025352EA0BAE9B10FE5C13A583 (void);
// 0x00000713 System.Void UnityEngine.RectTransform_ReapplyDrivenProperties::Invoke(UnityEngine.RectTransform)
extern void ReapplyDrivenProperties_Invoke_m5B39EC5205C3910AC09DCF378EAA2D8E99391636 (void);
// 0x00000714 System.IAsyncResult UnityEngine.RectTransform_ReapplyDrivenProperties::BeginInvoke(UnityEngine.RectTransform,System.AsyncCallback,System.Object)
extern void ReapplyDrivenProperties_BeginInvoke_mC7625A8FDFF392D73C7828526490DCB88FD87232 (void);
// 0x00000715 System.Void UnityEngine.RectTransform_ReapplyDrivenProperties::EndInvoke(System.IAsyncResult)
extern void ReapplyDrivenProperties_EndInvoke_m89A593999C130CA23515BF8A9C02DDE5B39ECF67 (void);
// 0x00000716 System.Void UnityEngine.Transform::.ctor()
extern void Transform__ctor_m629D1F6D054AD8FA5BD74296A23FCA93BEB76803 (void);
// 0x00000717 UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern void Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (void);
// 0x00000718 System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (void);
// 0x00000719 UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern void Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2 (void);
// 0x0000071A System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern void Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC (void);
// 0x0000071B UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
extern void Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F (void);
// 0x0000071C System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
extern void Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E (void);
// 0x0000071D UnityEngine.Vector3 UnityEngine.Transform::get_localEulerAngles()
extern void Transform_get_localEulerAngles_m4C442107F523737ADAB54855FDC1777A9B71D545 (void);
// 0x0000071E System.Void UnityEngine.Transform::set_localEulerAngles(UnityEngine.Vector3)
extern void Transform_set_localEulerAngles_mB63076996124DC76E6902A81677A6E3C814C693B (void);
// 0x0000071F UnityEngine.Vector3 UnityEngine.Transform::get_right()
extern void Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE (void);
// 0x00000720 UnityEngine.Vector3 UnityEngine.Transform::get_up()
extern void Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31 (void);
// 0x00000721 UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern void Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053 (void);
// 0x00000722 System.Void UnityEngine.Transform::set_forward(UnityEngine.Vector3)
extern void Transform_set_forward_mAE46B156F55F2F90AB495B17F7C20BF59A5D7D4D (void);
// 0x00000723 UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern void Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (void);
// 0x00000724 System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
extern void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (void);
// 0x00000725 UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern void Transform_get_localRotation_mA6472AE7509D762965275D79B645A14A9CCF5BE5 (void);
// 0x00000726 System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern void Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C (void);
// 0x00000727 UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern void Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046 (void);
// 0x00000728 System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern void Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A (void);
// 0x00000729 UnityEngine.Transform UnityEngine.Transform::get_parent()
extern void Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9 (void);
// 0x0000072A System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern void Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13 (void);
// 0x0000072B UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern void Transform_get_parentInternal_m6477F21AD3A2B2F3FE2C365B1AF64BB1AFDA7B4C (void);
// 0x0000072C System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern void Transform_set_parentInternal_mED1BC58DB05A14DAC354E5A4B24C872A5D69D0C3 (void);
// 0x0000072D UnityEngine.Transform UnityEngine.Transform::GetParent()
extern void Transform_GetParent_mA53F6AE810935DDED00A9FEEE1830F4EF797F73B (void);
// 0x0000072E System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern void Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E (void);
// 0x0000072F System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern void Transform_SetParent_mA6A651EDE81F139E1D6C7BA894834AD71D07227A (void);
// 0x00000730 UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern void Transform_get_worldToLocalMatrix_mE22FDE24767E1DE402D3E7A1C9803379B2E8399D (void);
// 0x00000731 UnityEngine.Matrix4x4 UnityEngine.Transform::get_localToWorldMatrix()
extern void Transform_get_localToWorldMatrix_m6B810B0F20BA5DE48009461A4D662DD8BFF6A3CC (void);
// 0x00000732 System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3,UnityEngine.Space)
extern void Transform_Translate_mFB58CBF3FA00BD0EE09EC67457608F62564D0DDE (void);
// 0x00000733 System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single)
extern void Transform_Translate_mC9343E1E646DA8FD42BE37137ACCBB4B52093F5C (void);
// 0x00000734 System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,UnityEngine.Space)
extern void Transform_Rotate_m61816C8A09C86A5E157EA89965A9CC0510A1B378 (void);
// 0x00000735 System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3)
extern void Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7 (void);
// 0x00000736 System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single,UnityEngine.Space)
extern void Transform_Rotate_mE77655C011C18F49CAD740CED7940EF1C7000357 (void);
// 0x00000737 System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single)
extern void Transform_Rotate_mA3AE6D55AA9CC88A8F03C2B0B7CB3DB45ABA6A8E (void);
// 0x00000738 System.Void UnityEngine.Transform::RotateAroundInternal(UnityEngine.Vector3,System.Single)
extern void Transform_RotateAroundInternal_mEEC98EFC71E388DB313FE62D9FAB642724A38216 (void);
// 0x00000739 System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single,UnityEngine.Space)
extern void Transform_Rotate_m12614C5FABB1F4A9A6800EE65BBFDB433D6D804D (void);
// 0x0000073A System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single)
extern void Transform_Rotate_m2AA745C4A796363462642A13251E8971D5C7F4DC (void);
// 0x0000073B System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void Transform_RotateAround_m1F93A7A1807BE407BD23EC1BA49F03AD22FCE4BE (void);
// 0x0000073C System.Void UnityEngine.Transform::LookAt(UnityEngine.Transform,UnityEngine.Vector3)
extern void Transform_LookAt_m6037828F5E8329B052D62472060274F9A3261ECA (void);
// 0x0000073D System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Transform_LookAt_m6BB4B39BB829A451C2F63215361D27650AA24D8C (void);
// 0x0000073E System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
extern void Transform_LookAt_m996FADE2327B0A4412FF4A5179B8BABD9EB849BA (void);
// 0x0000073F System.Void UnityEngine.Transform::Internal_LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern void Transform_Internal_LookAt_m1A24125A99A766EDA6059424EA3B5FA9C5E8B61B (void);
// 0x00000740 UnityEngine.Vector3 UnityEngine.Transform::TransformDirection(UnityEngine.Vector3)
extern void Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91 (void);
// 0x00000741 UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern void Transform_TransformPoint_m68AF95765A9279192E601208A9C5170027A5F0D2 (void);
// 0x00000742 UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern void Transform_InverseTransformPoint_m476ABC8F3F14824D7D82FE2C54CEE5A151A669B8 (void);
// 0x00000743 System.Int32 UnityEngine.Transform::get_childCount()
extern void Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05 (void);
// 0x00000744 System.Void UnityEngine.Transform::SetAsFirstSibling()
extern void Transform_SetAsFirstSibling_mD5C02831BA6C7C3408CD491191EAF760ECB7E754 (void);
// 0x00000745 UnityEngine.Transform UnityEngine.Transform::FindRelativeTransformWithPath(UnityEngine.Transform,System.String,System.Boolean)
extern void Transform_FindRelativeTransformWithPath_m8B6DE13079DE11DCCDD2CA40CEC59319FD70A12D (void);
// 0x00000746 UnityEngine.Transform UnityEngine.Transform::Find(System.String)
extern void Transform_Find_mB1687901A4FB0D562C44A93CC67CD35DCFCAABA1 (void);
// 0x00000747 UnityEngine.Vector3 UnityEngine.Transform::get_lossyScale()
extern void Transform_get_lossyScale_m469A16F93F135C1E4D5955C7EBDB893D1892A331 (void);
// 0x00000748 System.Boolean UnityEngine.Transform::IsChildOf(UnityEngine.Transform)
extern void Transform_IsChildOf_m1783A88A490931E98F4D5E361595A518E09FD4BC (void);
// 0x00000749 System.Void UnityEngine.Transform::set_hasChanged(System.Boolean)
extern void Transform_set_hasChanged_mD1CDCAE366DB514FBECD9DAAED0F7834029E1304 (void);
// 0x0000074A System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern void Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E (void);
// 0x0000074B UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern void Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C (void);
// 0x0000074C System.Void UnityEngine.Transform::get_position_Injected(UnityEngine.Vector3&)
extern void Transform_get_position_Injected_m43CE3FC8FB3C52896D709B07EB77340407800C13 (void);
// 0x0000074D System.Void UnityEngine.Transform::set_position_Injected(UnityEngine.Vector3&)
extern void Transform_set_position_Injected_m634DE2302555154065001583E6080CC48D58A602 (void);
// 0x0000074E System.Void UnityEngine.Transform::get_localPosition_Injected(UnityEngine.Vector3&)
extern void Transform_get_localPosition_Injected_mBBD4D1AAD893D9B5DB40E9946A40E2B94E688782 (void);
// 0x0000074F System.Void UnityEngine.Transform::set_localPosition_Injected(UnityEngine.Vector3&)
extern void Transform_set_localPosition_Injected_m228521F584224C612AEF8ED500AABF31C8E87E02 (void);
// 0x00000750 System.Void UnityEngine.Transform::get_rotation_Injected(UnityEngine.Quaternion&)
extern void Transform_get_rotation_Injected_m1F756C98851F36F25BFBAC3401B67A4D2F176DF1 (void);
// 0x00000751 System.Void UnityEngine.Transform::set_rotation_Injected(UnityEngine.Quaternion&)
extern void Transform_set_rotation_Injected_m9ACF0891D219140A329411F33858C7B0A026407F (void);
// 0x00000752 System.Void UnityEngine.Transform::get_localRotation_Injected(UnityEngine.Quaternion&)
extern void Transform_get_localRotation_Injected_mCF48B92BAD51A015698EFE3973CD2F595048E74B (void);
// 0x00000753 System.Void UnityEngine.Transform::set_localRotation_Injected(UnityEngine.Quaternion&)
extern void Transform_set_localRotation_Injected_m19EF26CC5E0F8331297D3FB17EFFC7FD217A9FCA (void);
// 0x00000754 System.Void UnityEngine.Transform::get_localScale_Injected(UnityEngine.Vector3&)
extern void Transform_get_localScale_Injected_mC3D90F76FF1C9876761FBE40C5FF567213B86402 (void);
// 0x00000755 System.Void UnityEngine.Transform::set_localScale_Injected(UnityEngine.Vector3&)
extern void Transform_set_localScale_Injected_m7247850A81ED854FD10411376E0EF2C4F7C50B65 (void);
// 0x00000756 System.Void UnityEngine.Transform::get_worldToLocalMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Transform_get_worldToLocalMatrix_Injected_m8B625E30EDAC79587E1D73943D2486385C403BB1 (void);
// 0x00000757 System.Void UnityEngine.Transform::get_localToWorldMatrix_Injected(UnityEngine.Matrix4x4&)
extern void Transform_get_localToWorldMatrix_Injected_m990CE30D1A3D41A3247D4F9E73CA8B725466767B (void);
// 0x00000758 System.Void UnityEngine.Transform::RotateAroundInternal_Injected(UnityEngine.Vector3&,System.Single)
extern void Transform_RotateAroundInternal_Injected_m70D2B2635B5CB12008A8207829D178BF4970E9BD (void);
// 0x00000759 System.Void UnityEngine.Transform::Internal_LookAt_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_Internal_LookAt_Injected_mE9622F74A863FA172B36D3F453BE0AA4BDEAC092 (void);
// 0x0000075A System.Void UnityEngine.Transform::TransformDirection_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_TransformDirection_Injected_mB27ACC95D32503153AEBE93976553C0A41BF4D2E (void);
// 0x0000075B System.Void UnityEngine.Transform::TransformPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_TransformPoint_Injected_mFCDA82BF83E47142F6115E18D515FA0D0A0E5319 (void);
// 0x0000075C System.Void UnityEngine.Transform::InverseTransformPoint_Injected(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void Transform_InverseTransformPoint_Injected_mC6226F53D5631F42658A5CA83FEE16EC24670A36 (void);
// 0x0000075D System.Void UnityEngine.Transform::get_lossyScale_Injected(UnityEngine.Vector3&)
extern void Transform_get_lossyScale_Injected_m5D97E86A5663FF57E1977E5CA13EF00022B61648 (void);
// 0x0000075E System.Void UnityEngine.Transform_Enumerator::.ctor(UnityEngine.Transform)
extern void Enumerator__ctor_m052C22273F1D789E58A09606D5EE5E87ABC2C91B (void);
// 0x0000075F System.Object UnityEngine.Transform_Enumerator::get_Current()
extern void Enumerator_get_Current_m1CFECBB7AC3EACD05A11CC6848AE7A94A8123E9F (void);
// 0x00000760 System.Boolean UnityEngine.Transform_Enumerator::MoveNext()
extern void Enumerator_MoveNext_m346F9A121D9E89ADBA8296E6A7EF8763C5B58A14 (void);
// 0x00000761 System.Void UnityEngine.Transform_Enumerator::Reset()
extern void Enumerator_Reset_mFA289646E280C94D82CC223C024E0B615F811C8E (void);
// 0x00000762 System.Void UnityEngine.Sprite::.ctor()
extern void Sprite__ctor_m121D88C6A901A2A2FA602306D01FDB8D7A0206F0 (void);
// 0x00000763 System.Int32 UnityEngine.Sprite::GetPackingMode()
extern void Sprite_GetPackingMode_m398C471B7DDCCA1EA0355217EBB7568E851E597A (void);
// 0x00000764 System.Int32 UnityEngine.Sprite::GetPacked()
extern void Sprite_GetPacked_m6AC29F35C9ADE1B6394202132FB77DA9249DF5AE (void);
// 0x00000765 UnityEngine.Rect UnityEngine.Sprite::GetTextureRect()
extern void Sprite_GetTextureRect_m6E19823AEA9A3FC4C9FE76E53C30F19316F63954 (void);
// 0x00000766 UnityEngine.Vector4 UnityEngine.Sprite::GetInnerUVs()
extern void Sprite_GetInnerUVs_m394AF466930BBACE6F45425C418D0A8991600AD9 (void);
// 0x00000767 UnityEngine.Vector4 UnityEngine.Sprite::GetOuterUVs()
extern void Sprite_GetOuterUVs_mEB9D18CA03A78C02CAF4FAD386A7AF009187ACDD (void);
// 0x00000768 UnityEngine.Vector4 UnityEngine.Sprite::GetPadding()
extern void Sprite_GetPadding_mA039E911719B85FBB31F4C235B9EF9973F5E7FF3 (void);
// 0x00000769 UnityEngine.Bounds UnityEngine.Sprite::get_bounds()
extern void Sprite_get_bounds_m364F852DE78702F755D1414FF4465F61F3F238EF (void);
// 0x0000076A UnityEngine.Rect UnityEngine.Sprite::get_rect()
extern void Sprite_get_rect_m146D3624E5D8DD6DF5B1F39CE618D701B9008C70 (void);
// 0x0000076B UnityEngine.Vector4 UnityEngine.Sprite::get_border()
extern void Sprite_get_border_m6AEB051C1A675509BB786427883FC2EE957F60A7 (void);
// 0x0000076C UnityEngine.Texture2D UnityEngine.Sprite::get_texture()
extern void Sprite_get_texture_mD03E68058C9F727321FE643CBDB3A469F96E49FB (void);
// 0x0000076D System.Single UnityEngine.Sprite::get_pixelsPerUnit()
extern void Sprite_get_pixelsPerUnit_mEA3201EE604FB43CB93E3D309B19A5D0B44C739E (void);
// 0x0000076E UnityEngine.Texture2D UnityEngine.Sprite::get_associatedAlphaSplitTexture()
extern void Sprite_get_associatedAlphaSplitTexture_m212E3C39E4EE3385866E51194F5FC9AEDDEE4F00 (void);
// 0x0000076F UnityEngine.Vector2 UnityEngine.Sprite::get_pivot()
extern void Sprite_get_pivot_m39B1CFCDA5BB126D198CAEAB703EC39E763CC867 (void);
// 0x00000770 System.Boolean UnityEngine.Sprite::get_packed()
extern void Sprite_get_packed_m075910C79D785DC2572B171DA93918CF2793B133 (void);
// 0x00000771 UnityEngine.SpritePackingMode UnityEngine.Sprite::get_packingMode()
extern void Sprite_get_packingMode_m1BF2656F34C1C650D1634F0AE81727074BE85E5F (void);
// 0x00000772 UnityEngine.Rect UnityEngine.Sprite::get_textureRect()
extern void Sprite_get_textureRect_m5B350C2B122C85549960912CBD6343E4A5B02C35 (void);
// 0x00000773 UnityEngine.Vector2[] UnityEngine.Sprite::get_vertices()
extern void Sprite_get_vertices_m4A5EFBEDA14F12E5358C61831150AE368453F301 (void);
// 0x00000774 System.UInt16[] UnityEngine.Sprite::get_triangles()
extern void Sprite_get_triangles_mAE8C32A81703AEF45192E993E6B555AF659C5131 (void);
// 0x00000775 UnityEngine.Vector2[] UnityEngine.Sprite::get_uv()
extern void Sprite_get_uv_mBD902ADCF1FF8AE211C98881A6E3C310D73494B6 (void);
// 0x00000776 System.Void UnityEngine.Sprite::GetTextureRect_Injected(UnityEngine.Rect&)
extern void Sprite_GetTextureRect_Injected_m5D5B55E003133B5A537764AF7493BC094685F2BD (void);
// 0x00000777 System.Void UnityEngine.Sprite::GetInnerUVs_Injected(UnityEngine.Vector4&)
extern void Sprite_GetInnerUVs_Injected_m6BBD450F64FCAA0EE51E16034E239267E53BADB7 (void);
// 0x00000778 System.Void UnityEngine.Sprite::GetOuterUVs_Injected(UnityEngine.Vector4&)
extern void Sprite_GetOuterUVs_Injected_m386A7B21043ED228AE4BBAB93060AFBFE19C5BD7 (void);
// 0x00000779 System.Void UnityEngine.Sprite::GetPadding_Injected(UnityEngine.Vector4&)
extern void Sprite_GetPadding_Injected_m9C8743817FB7CD12F88DA90769BD653EA35273EE (void);
// 0x0000077A System.Void UnityEngine.Sprite::get_bounds_Injected(UnityEngine.Bounds&)
extern void Sprite_get_bounds_Injected_m4AE096B307AD9788AEDA44AF14C9605D5ABEEE1C (void);
// 0x0000077B System.Void UnityEngine.Sprite::get_rect_Injected(UnityEngine.Rect&)
extern void Sprite_get_rect_Injected_mE5951AA7D9D0CBBF4AF8263F8B77B8B3E203279D (void);
// 0x0000077C System.Void UnityEngine.Sprite::get_border_Injected(UnityEngine.Vector4&)
extern void Sprite_get_border_Injected_m7A2673F6D49E5085CA3CC2436763C7C7BE0F75C8 (void);
// 0x0000077D System.Void UnityEngine.Sprite::get_pivot_Injected(UnityEngine.Vector2&)
extern void Sprite_get_pivot_Injected_mAAE0A9705B766EB97C8732BE5541E800E8090809 (void);
// 0x0000077E System.Boolean UnityEngine._Scripting.APIUpdating.APIUpdaterRuntimeHelpers::GetMovedFromAttributeDataForType(System.Type,System.String&,System.String&,System.String&)
extern void APIUpdaterRuntimeHelpers_GetMovedFromAttributeDataForType_mEDA7447F4AEBCBDE3B6C5A04ED735FA9BA2E7B52 (void);
// 0x0000077F System.Boolean UnityEngine._Scripting.APIUpdating.APIUpdaterRuntimeHelpers::GetObsoleteTypeRedirection(System.Type,System.String&,System.String&,System.String&)
extern void APIUpdaterRuntimeHelpers_GetObsoleteTypeRedirection_mAD9DCC5AEEF51535CB9FCED2F1B38650C766D355 (void);
// 0x00000780 UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetInnerUV(UnityEngine.Sprite)
extern void DataUtility_GetInnerUV_mDAA53C8F613CBB89345EE978D14599F5EE04891C (void);
// 0x00000781 UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetOuterUV(UnityEngine.Sprite)
extern void DataUtility_GetOuterUV_mC6B306F20527EE5490505B8A5929C70C842AB966 (void);
// 0x00000782 UnityEngine.Vector4 UnityEngine.Sprites.DataUtility::GetPadding(UnityEngine.Sprite)
extern void DataUtility_GetPadding_m6300930863B61A94EDF09C10C88668AA94E4EBD4 (void);
// 0x00000783 UnityEngine.Vector2 UnityEngine.Sprites.DataUtility::GetMinSize(UnityEngine.Sprite)
extern void DataUtility_GetMinSize_mEDB6E71839C3EA17052EE74D2FEBDE1D2F7D0081 (void);
// 0x00000784 System.Boolean UnityEngine.U2D.SpriteAtlasManager::RequestAtlas(System.String)
extern void SpriteAtlasManager_RequestAtlas_m4EB540E080D8444FE4B53D8F3D44EA9C0C8C49A1 (void);
// 0x00000785 System.Void UnityEngine.U2D.SpriteAtlasManager::add_atlasRegistered(System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern void SpriteAtlasManager_add_atlasRegistered_mE6C9446A8FA30F4F4B317CFCFC5AE98EE060C3FE (void);
// 0x00000786 System.Void UnityEngine.U2D.SpriteAtlasManager::remove_atlasRegistered(System.Action`1<UnityEngine.U2D.SpriteAtlas>)
extern void SpriteAtlasManager_remove_atlasRegistered_m9B9CFC51E64BF35DFFCBC83EF2BBCDDA3870F0CE (void);
// 0x00000787 System.Void UnityEngine.U2D.SpriteAtlasManager::PostRegisteredAtlas(UnityEngine.U2D.SpriteAtlas)
extern void SpriteAtlasManager_PostRegisteredAtlas_m0F58C324E58E39D7B13803FBF7B1AE16CF6B4B7B (void);
// 0x00000788 System.Void UnityEngine.U2D.SpriteAtlasManager::Register(UnityEngine.U2D.SpriteAtlas)
extern void SpriteAtlasManager_Register_m48E996EAD9A5CF419B7738799EB99A78D7095C73 (void);
// 0x00000789 System.Void UnityEngine.U2D.SpriteAtlasManager::.cctor()
extern void SpriteAtlasManager__cctor_mDB99D76724E2DB007B46B61C2833878B624D5021 (void);
// 0x0000078A System.Boolean UnityEngine.U2D.SpriteAtlas::CanBindTo(UnityEngine.Sprite)
extern void SpriteAtlas_CanBindTo_m01D0066BE9609582194ADA0DA70E598530DACF03 (void);
// 0x0000078B UnityEngine.Sprite UnityEngine.U2D.SpriteAtlas::GetSprite(System.String)
extern void SpriteAtlas_GetSprite_mBDFF27666D4C361D5A5E6F55421A658A045C8DDB (void);
// 0x0000078C System.Int64 UnityEngine.Profiling.Profiler::GetMonoUsedSizeLong()
extern void Profiler_GetMonoUsedSizeLong_mFE5483CFA8A430C8BC7A44EB67A4C244DFF0CE02 (void);
// 0x0000078D System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_rawImageDataReference(Unity.Collections.NativeArray`1<System.Byte>)
extern void DebugScreenCapture_set_rawImageDataReference_m9FE7228E0FB4696A255B2F477B6B50C902D7786B_AdjustorThunk (void);
// 0x0000078E System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_imageFormat(UnityEngine.TextureFormat)
extern void DebugScreenCapture_set_imageFormat_m46E9D97376E844826DAE5C3C69E4AAF4B7A58ECB_AdjustorThunk (void);
// 0x0000078F System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_width(System.Int32)
extern void DebugScreenCapture_set_width_m6928DB2B2BCD54DE97BDA4116D69897921CCACF6_AdjustorThunk (void);
// 0x00000790 System.Void UnityEngine.Profiling.Experimental.DebugScreenCapture::set_height(System.Int32)
extern void DebugScreenCapture_set_height_m6CD0F6872F123966B784E6F356C51986B8B19F84_AdjustorThunk (void);
// 0x00000791 System.Void UnityEngine.Profiling.Memory.Experimental.MetaData::.ctor()
extern void MetaData__ctor_mD7868B95DDB386C2F8AC614F09A6760F420A44D5 (void);
// 0x00000792 System.Byte[] UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::PrepareMetadata()
extern void MemoryProfiler_PrepareMetadata_m571D85DE9BEAF3D3E0ED8269AE350960717D947E (void);
// 0x00000793 System.Int32 UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::WriteIntToByteArray(System.Byte[],System.Int32,System.Int32)
extern void MemoryProfiler_WriteIntToByteArray_mEC9056AEB48E7906BAA9FDA7FF538E7341233E8E (void);
// 0x00000794 System.Int32 UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::WriteStringToByteArray(System.Byte[],System.Int32,System.String)
extern void MemoryProfiler_WriteStringToByteArray_m48936038ADD56D3BF498870F4DA6AB12DE0CA9FC (void);
// 0x00000795 System.Void UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::FinalizeSnapshot(System.String,System.Boolean)
extern void MemoryProfiler_FinalizeSnapshot_m7DE2A0E49B6457B64D53255BE00F7F1C7EA30526 (void);
// 0x00000796 System.Void UnityEngine.Profiling.Memory.Experimental.MemoryProfiler::SaveScreenshotToDisk(System.String,System.Boolean,System.IntPtr,System.Int32,UnityEngine.TextureFormat,System.Int32,System.Int32)
extern void MemoryProfiler_SaveScreenshotToDisk_m9419808BAC900EAB213522E34F6268975722495A (void);
// 0x00000797 System.Void UnityEngine.WSA.WindowSizeChanged::.ctor(System.Object,System.IntPtr)
extern void WindowSizeChanged__ctor_mE184BC9192055FE6767E1E439A81B80A2FD5FCDF (void);
// 0x00000798 System.Void UnityEngine.WSA.WindowSizeChanged::Invoke(System.Int32,System.Int32)
extern void WindowSizeChanged_Invoke_m9C5F9E8BDC77F578888B3B35273D0814D20E824A (void);
// 0x00000799 System.IAsyncResult UnityEngine.WSA.WindowSizeChanged::BeginInvoke(System.Int32,System.Int32,System.AsyncCallback,System.Object)
extern void WindowSizeChanged_BeginInvoke_m39C9F6101934C25EC32001AA9EB4216974D564DC (void);
// 0x0000079A System.Void UnityEngine.WSA.WindowSizeChanged::EndInvoke(System.IAsyncResult)
extern void WindowSizeChanged_EndInvoke_mD6C472713445F18BD17E54BCACDD415D81921164 (void);
// 0x0000079B System.Void UnityEngine.WSA.WindowActivated::.ctor(System.Object,System.IntPtr)
extern void WindowActivated__ctor_m2725FDD17B58A484E5B2B5353E7892ADED8B6B05 (void);
// 0x0000079C System.Void UnityEngine.WSA.WindowActivated::Invoke(UnityEngine.WSA.WindowActivationState)
extern void WindowActivated_Invoke_mF077F7624898BA59AA03F39842F630D37BEA963E (void);
// 0x0000079D System.IAsyncResult UnityEngine.WSA.WindowActivated::BeginInvoke(UnityEngine.WSA.WindowActivationState,System.AsyncCallback,System.Object)
extern void WindowActivated_BeginInvoke_m4D5F0E5480B306DDEE4F135DD6646F66086A62DB (void);
// 0x0000079E System.Void UnityEngine.WSA.WindowActivated::EndInvoke(System.IAsyncResult)
extern void WindowActivated_EndInvoke_m186D0562CF86D246C08B253113E3B3EC2B82D065 (void);
// 0x0000079F System.Void UnityEngine.WSA.Application::InvokeWindowSizeChangedEvent(System.Int32,System.Int32)
extern void Application_InvokeWindowSizeChangedEvent_mA7AC1CFF5F8477230890ADEE3408A28D75E78FC6 (void);
// 0x000007A0 System.Void UnityEngine.WSA.Application::InvokeWindowActivatedEvent(UnityEngine.WSA.WindowActivationState)
extern void Application_InvokeWindowActivatedEvent_mAFCB006888BBE4F8499B3016782F75B3F8B731F7 (void);
// 0x000007A1 System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem::PhraseRecognitionSystem_InvokeErrorEvent(UnityEngine.Windows.Speech.SpeechError)
extern void PhraseRecognitionSystem_PhraseRecognitionSystem_InvokeErrorEvent_mAFD4E8DC3319962E46ADBCD2BB85DCB8381D52CF (void);
// 0x000007A2 System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem::PhraseRecognitionSystem_InvokeStatusChangedEvent(UnityEngine.Windows.Speech.SpeechSystemStatus)
extern void PhraseRecognitionSystem_PhraseRecognitionSystem_InvokeStatusChangedEvent_m6BAFA3514D9614847F28720F792FF62F1E623577 (void);
// 0x000007A3 System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem_ErrorDelegate::.ctor(System.Object,System.IntPtr)
extern void ErrorDelegate__ctor_m2F691DE208DD6430077E3C17D4A5293AA9002FF4 (void);
// 0x000007A4 System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem_ErrorDelegate::Invoke(UnityEngine.Windows.Speech.SpeechError)
extern void ErrorDelegate_Invoke_m46CECD81317CD9717E11456CA73E6B14B88E6724 (void);
// 0x000007A5 System.IAsyncResult UnityEngine.Windows.Speech.PhraseRecognitionSystem_ErrorDelegate::BeginInvoke(UnityEngine.Windows.Speech.SpeechError,System.AsyncCallback,System.Object)
extern void ErrorDelegate_BeginInvoke_mD2A98805D514FF2FE986EAB635AECA18A6CB42F8 (void);
// 0x000007A6 System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem_ErrorDelegate::EndInvoke(System.IAsyncResult)
extern void ErrorDelegate_EndInvoke_mBA12D5FE7D6C9A5615AF473703969C011EDCCA3B (void);
// 0x000007A7 System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem_StatusDelegate::.ctor(System.Object,System.IntPtr)
extern void StatusDelegate__ctor_m8D463CDB5084EA78077004167A884277C11C8580 (void);
// 0x000007A8 System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem_StatusDelegate::Invoke(UnityEngine.Windows.Speech.SpeechSystemStatus)
extern void StatusDelegate_Invoke_m04916521B4B6FD2EAFF4A9DF96628F8C34E0C8E2 (void);
// 0x000007A9 System.IAsyncResult UnityEngine.Windows.Speech.PhraseRecognitionSystem_StatusDelegate::BeginInvoke(UnityEngine.Windows.Speech.SpeechSystemStatus,System.AsyncCallback,System.Object)
extern void StatusDelegate_BeginInvoke_m5E20320BB35F9113700B3DA0DFAEF01DEE14DAC8 (void);
// 0x000007AA System.Void UnityEngine.Windows.Speech.PhraseRecognitionSystem_StatusDelegate::EndInvoke(System.IAsyncResult)
extern void StatusDelegate_EndInvoke_mADE8C786A44A85CB58AEA7976CFC6E5B9E95078F (void);
// 0x000007AB System.Void UnityEngine.Windows.Speech.PhraseRecognizer::InvokePhraseRecognizedEvent(System.String,UnityEngine.Windows.Speech.ConfidenceLevel,UnityEngine.Windows.Speech.SemanticMeaning[],System.Int64,System.Int64)
extern void PhraseRecognizer_InvokePhraseRecognizedEvent_m71BC6E4E3474B530FB8060790335C73EA1A3949F (void);
// 0x000007AC UnityEngine.Windows.Speech.SemanticMeaning[] UnityEngine.Windows.Speech.PhraseRecognizer::MarshalSemanticMeaning(System.IntPtr,System.IntPtr,System.IntPtr,System.Int32)
extern void PhraseRecognizer_MarshalSemanticMeaning_mE499C2118292E6AB02370790295E913FD22A1C60 (void);
// 0x000007AD System.Void UnityEngine.Windows.Speech.PhraseRecognizer_PhraseRecognizedDelegate::.ctor(System.Object,System.IntPtr)
extern void PhraseRecognizedDelegate__ctor_m01A9360909C670C8869C24BB40DC38C2EFCAA03C (void);
// 0x000007AE System.Void UnityEngine.Windows.Speech.PhraseRecognizer_PhraseRecognizedDelegate::Invoke(UnityEngine.Windows.Speech.PhraseRecognizedEventArgs)
extern void PhraseRecognizedDelegate_Invoke_m492CAE6A50A50603EFFC20E55A5E600B3F44567E (void);
// 0x000007AF System.IAsyncResult UnityEngine.Windows.Speech.PhraseRecognizer_PhraseRecognizedDelegate::BeginInvoke(UnityEngine.Windows.Speech.PhraseRecognizedEventArgs,System.AsyncCallback,System.Object)
extern void PhraseRecognizedDelegate_BeginInvoke_m7AA286808D7E2C74F4ABA0C33D494837575D1056 (void);
// 0x000007B0 System.Void UnityEngine.Windows.Speech.PhraseRecognizer_PhraseRecognizedDelegate::EndInvoke(System.IAsyncResult)
extern void PhraseRecognizedDelegate_EndInvoke_m1807091CC1EADD5595504824A06518F6E93045DE (void);
// 0x000007B1 System.Void UnityEngine.Windows.Speech.DictationRecognizer::DictationRecognizer_InvokeHypothesisGeneratedEvent(System.String)
extern void DictationRecognizer_DictationRecognizer_InvokeHypothesisGeneratedEvent_mD73863E9B51235457513AEFCAF731D2CE5CD74A5 (void);
// 0x000007B2 System.Void UnityEngine.Windows.Speech.DictationRecognizer::DictationRecognizer_InvokeResultGeneratedEvent(System.String,UnityEngine.Windows.Speech.ConfidenceLevel)
extern void DictationRecognizer_DictationRecognizer_InvokeResultGeneratedEvent_m341798A02CC88FFBC30AD9D65A8B698CC26FF51C (void);
// 0x000007B3 System.Void UnityEngine.Windows.Speech.DictationRecognizer::DictationRecognizer_InvokeCompletedEvent(UnityEngine.Windows.Speech.DictationCompletionCause)
extern void DictationRecognizer_DictationRecognizer_InvokeCompletedEvent_mA57B1D8E190290090345F7F46769FCA3CB300381 (void);
// 0x000007B4 System.Void UnityEngine.Windows.Speech.DictationRecognizer::DictationRecognizer_InvokeErrorEvent(System.String,System.Int32)
extern void DictationRecognizer_DictationRecognizer_InvokeErrorEvent_mD903A358EDB5EC2F026503EFB541AA0784D21030 (void);
// 0x000007B5 System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationHypothesisDelegate::.ctor(System.Object,System.IntPtr)
extern void DictationHypothesisDelegate__ctor_mC0ABE67DC4DBB4FA1F9869C18419245544A3E597 (void);
// 0x000007B6 System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationHypothesisDelegate::Invoke(System.String)
extern void DictationHypothesisDelegate_Invoke_m172D4B7A778AF77B8DF601464D1C6F94323DA351 (void);
// 0x000007B7 System.IAsyncResult UnityEngine.Windows.Speech.DictationRecognizer_DictationHypothesisDelegate::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void DictationHypothesisDelegate_BeginInvoke_m3C59FFB81CDAE4A1B9E42587468BA85EA85FAC98 (void);
// 0x000007B8 System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationHypothesisDelegate::EndInvoke(System.IAsyncResult)
extern void DictationHypothesisDelegate_EndInvoke_m8DFC208965BBC072D5301AF8AA2A304134385826 (void);
// 0x000007B9 System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationResultDelegate::.ctor(System.Object,System.IntPtr)
extern void DictationResultDelegate__ctor_mA245D7423472C29BE33A7340495C2673E4FF3A65 (void);
// 0x000007BA System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationResultDelegate::Invoke(System.String,UnityEngine.Windows.Speech.ConfidenceLevel)
extern void DictationResultDelegate_Invoke_mD1040B15ECCA7D8178544845A76F6D8CE8F8D7EB (void);
// 0x000007BB System.IAsyncResult UnityEngine.Windows.Speech.DictationRecognizer_DictationResultDelegate::BeginInvoke(System.String,UnityEngine.Windows.Speech.ConfidenceLevel,System.AsyncCallback,System.Object)
extern void DictationResultDelegate_BeginInvoke_mFDC155E178AFBE15333E08205F633D6B429EC4F8 (void);
// 0x000007BC System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationResultDelegate::EndInvoke(System.IAsyncResult)
extern void DictationResultDelegate_EndInvoke_m2532D7823F4B3D7D6DB9A3E8193DF3A699FD5CB5 (void);
// 0x000007BD System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationCompletedDelegate::.ctor(System.Object,System.IntPtr)
extern void DictationCompletedDelegate__ctor_mC90654E14428DFAEF0ADC53BF45C64487DE4B028 (void);
// 0x000007BE System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationCompletedDelegate::Invoke(UnityEngine.Windows.Speech.DictationCompletionCause)
extern void DictationCompletedDelegate_Invoke_mFC2FDC432DD9C96C8185D6F4FE2CDB03934DB7E7 (void);
// 0x000007BF System.IAsyncResult UnityEngine.Windows.Speech.DictationRecognizer_DictationCompletedDelegate::BeginInvoke(UnityEngine.Windows.Speech.DictationCompletionCause,System.AsyncCallback,System.Object)
extern void DictationCompletedDelegate_BeginInvoke_m71523637F04F434C0B724BED605FA9D3801B9C06 (void);
// 0x000007C0 System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationCompletedDelegate::EndInvoke(System.IAsyncResult)
extern void DictationCompletedDelegate_EndInvoke_mC10AC3ECC5701357846DF46323A7230B746D3333 (void);
// 0x000007C1 System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationErrorHandler::.ctor(System.Object,System.IntPtr)
extern void DictationErrorHandler__ctor_m0D33C95E89B5B9F7361B33CBEFD9E12FAEBB7CA4 (void);
// 0x000007C2 System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationErrorHandler::Invoke(System.String,System.Int32)
extern void DictationErrorHandler_Invoke_m44F785E16BD6C75A458770FBD90081BBF9F92F82 (void);
// 0x000007C3 System.IAsyncResult UnityEngine.Windows.Speech.DictationRecognizer_DictationErrorHandler::BeginInvoke(System.String,System.Int32,System.AsyncCallback,System.Object)
extern void DictationErrorHandler_BeginInvoke_m427CE87CC2C182A9622CEF829F4DB47B8E32C809 (void);
// 0x000007C4 System.Void UnityEngine.Windows.Speech.DictationRecognizer_DictationErrorHandler::EndInvoke(System.IAsyncResult)
extern void DictationErrorHandler_EndInvoke_m81F3A3DB125A5756A3A399410EDE07061CB696E5 (void);
// 0x000007C5 System.Void UnityEngine.Windows.Speech.PhraseRecognizedEventArgs::.ctor(System.String,UnityEngine.Windows.Speech.ConfidenceLevel,UnityEngine.Windows.Speech.SemanticMeaning[],System.DateTime,System.TimeSpan)
extern void PhraseRecognizedEventArgs__ctor_mDE0C6DB83D9AEC9FFBBFDD6F16FFDB59122B91BD_AdjustorThunk (void);
// 0x000007C6 UnityEngine.Windows.WebCam.PhotoCapture_PhotoCaptureResult UnityEngine.Windows.WebCam.PhotoCapture::MakeCaptureResult(System.Int64)
extern void PhotoCapture_MakeCaptureResult_mDB24427838429555162DA5395DE31032C291139A (void);
// 0x000007C7 System.Void UnityEngine.Windows.WebCam.PhotoCapture::InvokeOnCreatedResourceDelegate(UnityEngine.Windows.WebCam.PhotoCapture_OnCaptureResourceCreatedCallback,System.IntPtr)
extern void PhotoCapture_InvokeOnCreatedResourceDelegate_m0590F8148BA223CF8FE089B9BEAF65181019894C (void);
// 0x000007C8 System.Void UnityEngine.Windows.WebCam.PhotoCapture::.ctor(System.IntPtr)
extern void PhotoCapture__ctor_m272ED75FFE8FC350EA0FB9689FDCCC5E5A357683 (void);
// 0x000007C9 System.Void UnityEngine.Windows.WebCam.PhotoCapture::InvokeOnPhotoModeStartedDelegate(UnityEngine.Windows.WebCam.PhotoCapture_OnPhotoModeStartedCallback,System.Int64)
extern void PhotoCapture_InvokeOnPhotoModeStartedDelegate_m1655D665DEAD3E00A64186B78CDC53E1D2894C98 (void);
// 0x000007CA System.Void UnityEngine.Windows.WebCam.PhotoCapture::InvokeOnPhotoModeStoppedDelegate(UnityEngine.Windows.WebCam.PhotoCapture_OnPhotoModeStoppedCallback,System.Int64)
extern void PhotoCapture_InvokeOnPhotoModeStoppedDelegate_mD86C8F3385AA2F0F8FC7E06C62C0E795DD8BA002 (void);
// 0x000007CB System.Void UnityEngine.Windows.WebCam.PhotoCapture::InvokeOnCapturedPhotoToDiskDelegate(UnityEngine.Windows.WebCam.PhotoCapture_OnCapturedToDiskCallback,System.Int64)
extern void PhotoCapture_InvokeOnCapturedPhotoToDiskDelegate_m2D3054020413D1E8F62B61DDBD39CC023C198D1F (void);
// 0x000007CC System.Void UnityEngine.Windows.WebCam.PhotoCapture::InvokeOnCapturedPhotoToMemoryDelegate(UnityEngine.Windows.WebCam.PhotoCapture_OnCapturedToMemoryCallback,System.Int64,System.IntPtr)
extern void PhotoCapture_InvokeOnCapturedPhotoToMemoryDelegate_m76E1AF14059053B8CFD5F85AB7108CF8DD7C6942 (void);
// 0x000007CD System.Void UnityEngine.Windows.WebCam.PhotoCapture::Dispose()
extern void PhotoCapture_Dispose_mC657D88FF9C469242501DC5E01C6EADA8DEE49CC (void);
// 0x000007CE System.Void UnityEngine.Windows.WebCam.PhotoCapture::Dispose_Internal()
extern void PhotoCapture_Dispose_Internal_mBAE794ED741E9B9D2E940AF80D91124A8FE03D83 (void);
// 0x000007CF System.Void UnityEngine.Windows.WebCam.PhotoCapture::Finalize()
extern void PhotoCapture_Finalize_m2ECA2885C2AF55BCE899E5F61C460BE45C3331F4 (void);
// 0x000007D0 System.Void UnityEngine.Windows.WebCam.PhotoCapture::DisposeThreaded_Internal()
extern void PhotoCapture_DisposeThreaded_Internal_m638F1D622FD8AE8801D4FC6A1CDFAE0A67F6FF4D (void);
// 0x000007D1 System.Void UnityEngine.Windows.WebCam.PhotoCapture::.cctor()
extern void PhotoCapture__cctor_m2D9F492A3F615EEB0C515F1AF1D7068D51085574 (void);
// 0x000007D2 System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnCaptureResourceCreatedCallback::.ctor(System.Object,System.IntPtr)
extern void OnCaptureResourceCreatedCallback__ctor_m18862383676D88F484C4F1D55DE53FD76A254BF6 (void);
// 0x000007D3 System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnCaptureResourceCreatedCallback::Invoke(UnityEngine.Windows.WebCam.PhotoCapture)
extern void OnCaptureResourceCreatedCallback_Invoke_m00E949BE932CDE02940DAC09D88AC338B44678E5 (void);
// 0x000007D4 System.IAsyncResult UnityEngine.Windows.WebCam.PhotoCapture_OnCaptureResourceCreatedCallback::BeginInvoke(UnityEngine.Windows.WebCam.PhotoCapture,System.AsyncCallback,System.Object)
extern void OnCaptureResourceCreatedCallback_BeginInvoke_m70F7B03FF31B53058D1B51F05B0D5BB3E1C785E6 (void);
// 0x000007D5 System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnCaptureResourceCreatedCallback::EndInvoke(System.IAsyncResult)
extern void OnCaptureResourceCreatedCallback_EndInvoke_mA868D0097EBE8FE44E88BB93932C52B79E12910B (void);
// 0x000007D6 System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnPhotoModeStartedCallback::.ctor(System.Object,System.IntPtr)
extern void OnPhotoModeStartedCallback__ctor_mD122FA9A03DF5DD7A950ADD2DE0E1061779415B8 (void);
// 0x000007D7 System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnPhotoModeStartedCallback::Invoke(UnityEngine.Windows.WebCam.PhotoCapture_PhotoCaptureResult)
extern void OnPhotoModeStartedCallback_Invoke_m243CF5632026C05A11143C2A52CEA80A8B46AAB3 (void);
// 0x000007D8 System.IAsyncResult UnityEngine.Windows.WebCam.PhotoCapture_OnPhotoModeStartedCallback::BeginInvoke(UnityEngine.Windows.WebCam.PhotoCapture_PhotoCaptureResult,System.AsyncCallback,System.Object)
extern void OnPhotoModeStartedCallback_BeginInvoke_m4AB861825C43FBA4BB652B26EF01085FEF25B0CD (void);
// 0x000007D9 System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnPhotoModeStartedCallback::EndInvoke(System.IAsyncResult)
extern void OnPhotoModeStartedCallback_EndInvoke_m0FA93D4397AEC6F2EA56B65863308AD72B017B0B (void);
// 0x000007DA System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnPhotoModeStoppedCallback::.ctor(System.Object,System.IntPtr)
extern void OnPhotoModeStoppedCallback__ctor_mF8240EE3B1A20732ED29A0CF0D1CCE30CD838E6B (void);
// 0x000007DB System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnPhotoModeStoppedCallback::Invoke(UnityEngine.Windows.WebCam.PhotoCapture_PhotoCaptureResult)
extern void OnPhotoModeStoppedCallback_Invoke_mC55D72686C2EE6D124859D426C76835DD3CDAAF6 (void);
// 0x000007DC System.IAsyncResult UnityEngine.Windows.WebCam.PhotoCapture_OnPhotoModeStoppedCallback::BeginInvoke(UnityEngine.Windows.WebCam.PhotoCapture_PhotoCaptureResult,System.AsyncCallback,System.Object)
extern void OnPhotoModeStoppedCallback_BeginInvoke_m70BAC41BE079FC59B70B141F732DCE97FA7BC5AA (void);
// 0x000007DD System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnPhotoModeStoppedCallback::EndInvoke(System.IAsyncResult)
extern void OnPhotoModeStoppedCallback_EndInvoke_m1DCAE195481843479432344016A8E634D6023E07 (void);
// 0x000007DE System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnCapturedToDiskCallback::.ctor(System.Object,System.IntPtr)
extern void OnCapturedToDiskCallback__ctor_mF1937339779CDF2601A14371D2033EC6DB9EC297 (void);
// 0x000007DF System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnCapturedToDiskCallback::Invoke(UnityEngine.Windows.WebCam.PhotoCapture_PhotoCaptureResult)
extern void OnCapturedToDiskCallback_Invoke_m1D853AAE852DF5DDBBC0815B0AE3F5854FCE5B70 (void);
// 0x000007E0 System.IAsyncResult UnityEngine.Windows.WebCam.PhotoCapture_OnCapturedToDiskCallback::BeginInvoke(UnityEngine.Windows.WebCam.PhotoCapture_PhotoCaptureResult,System.AsyncCallback,System.Object)
extern void OnCapturedToDiskCallback_BeginInvoke_m7470B5311AF8B8F32DA2E4025E242A0DDDA2CD89 (void);
// 0x000007E1 System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnCapturedToDiskCallback::EndInvoke(System.IAsyncResult)
extern void OnCapturedToDiskCallback_EndInvoke_m6BE154AD3F6595D9F72B54D55427559A4495A4DB (void);
// 0x000007E2 System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnCapturedToMemoryCallback::.ctor(System.Object,System.IntPtr)
extern void OnCapturedToMemoryCallback__ctor_mE6373867AD9BC33554D11BECDCF720FF7F41F431 (void);
// 0x000007E3 System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnCapturedToMemoryCallback::Invoke(UnityEngine.Windows.WebCam.PhotoCapture_PhotoCaptureResult,UnityEngine.Windows.WebCam.PhotoCaptureFrame)
extern void OnCapturedToMemoryCallback_Invoke_mA9753BDC67A357C072B167772505785858730753 (void);
// 0x000007E4 System.IAsyncResult UnityEngine.Windows.WebCam.PhotoCapture_OnCapturedToMemoryCallback::BeginInvoke(UnityEngine.Windows.WebCam.PhotoCapture_PhotoCaptureResult,UnityEngine.Windows.WebCam.PhotoCaptureFrame,System.AsyncCallback,System.Object)
extern void OnCapturedToMemoryCallback_BeginInvoke_m00F1E3F01755247DA91F4B991E45812052EFFD78 (void);
// 0x000007E5 System.Void UnityEngine.Windows.WebCam.PhotoCapture_OnCapturedToMemoryCallback::EndInvoke(System.IAsyncResult)
extern void OnCapturedToMemoryCallback_EndInvoke_mE71F50B61AA38C40151B2C124047B6B91866A90E (void);
// 0x000007E6 System.Int32 UnityEngine.Windows.WebCam.PhotoCaptureFrame::get_dataLength()
extern void PhotoCaptureFrame_get_dataLength_m47499791125C48A4495A585D80985EAFA73152BA (void);
// 0x000007E7 System.Void UnityEngine.Windows.WebCam.PhotoCaptureFrame::set_dataLength(System.Int32)
extern void PhotoCaptureFrame_set_dataLength_mF01C30F8CC0E6B038E233F22C5BAB1D4C0251817 (void);
// 0x000007E8 System.Void UnityEngine.Windows.WebCam.PhotoCaptureFrame::set_hasLocationData(System.Boolean)
extern void PhotoCaptureFrame_set_hasLocationData_mCFE5AFBA1529278C8D69648778A502F40B4AC814 (void);
// 0x000007E9 System.Void UnityEngine.Windows.WebCam.PhotoCaptureFrame::set_pixelFormat(UnityEngine.Windows.WebCam.CapturePixelFormat)
extern void PhotoCaptureFrame_set_pixelFormat_m5ED1DEF417CEE9A2693DCF1450541236B48D1832 (void);
// 0x000007EA System.Int32 UnityEngine.Windows.WebCam.PhotoCaptureFrame::GetDataLength()
extern void PhotoCaptureFrame_GetDataLength_m778C47B77CDFF4638647E53363366E1E16410CF2 (void);
// 0x000007EB System.Boolean UnityEngine.Windows.WebCam.PhotoCaptureFrame::GetHasLocationData()
extern void PhotoCaptureFrame_GetHasLocationData_m0582FE1D5C964F77C0208B92D1A55B7782BAA649 (void);
// 0x000007EC UnityEngine.Windows.WebCam.CapturePixelFormat UnityEngine.Windows.WebCam.PhotoCaptureFrame::GetCapturePixelFormat()
extern void PhotoCaptureFrame_GetCapturePixelFormat_m172C702F3A874A6FF2D93AA2FF7AEF955A2D2A7E (void);
// 0x000007ED System.Void UnityEngine.Windows.WebCam.PhotoCaptureFrame::.ctor(System.IntPtr)
extern void PhotoCaptureFrame__ctor_m54006C0D1C252AA254443BDDF659EA77D048F7B6 (void);
// 0x000007EE System.Void UnityEngine.Windows.WebCam.PhotoCaptureFrame::Cleanup()
extern void PhotoCaptureFrame_Cleanup_m3352FE56134490EA6AD718AA8D015F743C600069 (void);
// 0x000007EF System.Void UnityEngine.Windows.WebCam.PhotoCaptureFrame::Dispose_Internal()
extern void PhotoCaptureFrame_Dispose_Internal_m6481298FEA6C2A1B1E86AF93B98D9366D1D53398 (void);
// 0x000007F0 System.Void UnityEngine.Windows.WebCam.PhotoCaptureFrame::Dispose()
extern void PhotoCaptureFrame_Dispose_m721C009AD08134027E0B83802ABAE1A22E248E02 (void);
// 0x000007F1 System.Void UnityEngine.Windows.WebCam.PhotoCaptureFrame::Finalize()
extern void PhotoCaptureFrame_Finalize_m566F6494D65D7036D4E3870794D1BABD874242A6 (void);
// 0x000007F2 UnityEngine.Windows.WebCam.VideoCapture_VideoCaptureResult UnityEngine.Windows.WebCam.VideoCapture::MakeCaptureResult(System.Int64)
extern void VideoCapture_MakeCaptureResult_m1B18134E624C914E7864DF53C04469488C373A7B (void);
// 0x000007F3 System.Void UnityEngine.Windows.WebCam.VideoCapture::InvokeOnCreatedVideoCaptureResourceDelegate(UnityEngine.Windows.WebCam.VideoCapture_OnVideoCaptureResourceCreatedCallback,System.IntPtr)
extern void VideoCapture_InvokeOnCreatedVideoCaptureResourceDelegate_m8FDDFA04ADAB2F82C2E5724490C030F5CA0A9E15 (void);
// 0x000007F4 System.Void UnityEngine.Windows.WebCam.VideoCapture::.ctor(System.IntPtr)
extern void VideoCapture__ctor_m59BBCFE085D02230DF34ED48710A9FE1677879E2 (void);
// 0x000007F5 System.Void UnityEngine.Windows.WebCam.VideoCapture::InvokeOnVideoModeStartedDelegate(UnityEngine.Windows.WebCam.VideoCapture_OnVideoModeStartedCallback,System.Int64)
extern void VideoCapture_InvokeOnVideoModeStartedDelegate_m79DB3233AD2A8F18940D0A5FB711F14BB7F411E5 (void);
// 0x000007F6 System.Void UnityEngine.Windows.WebCam.VideoCapture::InvokeOnVideoModeStoppedDelegate(UnityEngine.Windows.WebCam.VideoCapture_OnVideoModeStoppedCallback,System.Int64)
extern void VideoCapture_InvokeOnVideoModeStoppedDelegate_m9016BC3B3312B5A739D443075BF7728E58215AB6 (void);
// 0x000007F7 System.Void UnityEngine.Windows.WebCam.VideoCapture::InvokeOnStartedRecordingVideoToDiskDelegate(UnityEngine.Windows.WebCam.VideoCapture_OnStartedRecordingVideoCallback,System.Int64)
extern void VideoCapture_InvokeOnStartedRecordingVideoToDiskDelegate_m5ACD5DFAD5008371A0CBFBAA4CEFCC17C606E4CA (void);
// 0x000007F8 System.Void UnityEngine.Windows.WebCam.VideoCapture::InvokeOnStoppedRecordingVideoToDiskDelegate(UnityEngine.Windows.WebCam.VideoCapture_OnStoppedRecordingVideoCallback,System.Int64)
extern void VideoCapture_InvokeOnStoppedRecordingVideoToDiskDelegate_m386C086EE16E072FF40964E31B87E617F0B6C07A (void);
// 0x000007F9 System.Void UnityEngine.Windows.WebCam.VideoCapture::Dispose()
extern void VideoCapture_Dispose_m7A39424EAE62A1BEAE17325F61B8A22C7F20EF2D (void);
// 0x000007FA System.Void UnityEngine.Windows.WebCam.VideoCapture::Dispose_Internal()
extern void VideoCapture_Dispose_Internal_mB13763260E23253359B6CAE0AF929DE13D9A42F7 (void);
// 0x000007FB System.Void UnityEngine.Windows.WebCam.VideoCapture::Finalize()
extern void VideoCapture_Finalize_m3789179E8CF060E0C2A746A1FDDFFD21577E8ADB (void);
// 0x000007FC System.Void UnityEngine.Windows.WebCam.VideoCapture::DisposeThreaded_Internal()
extern void VideoCapture_DisposeThreaded_Internal_mB47B014DFF05AFC026E5DD7D0FAF54106553B9DB (void);
// 0x000007FD System.Void UnityEngine.Windows.WebCam.VideoCapture::.cctor()
extern void VideoCapture__cctor_m20292EE7FCFF5E79FB26BD15B3A89EBE5408EBD5 (void);
// 0x000007FE System.Void UnityEngine.Windows.WebCam.VideoCapture_OnVideoCaptureResourceCreatedCallback::.ctor(System.Object,System.IntPtr)
extern void OnVideoCaptureResourceCreatedCallback__ctor_m367E31CF82FCBA5320647F05C0DA9CA047752B59 (void);
// 0x000007FF System.Void UnityEngine.Windows.WebCam.VideoCapture_OnVideoCaptureResourceCreatedCallback::Invoke(UnityEngine.Windows.WebCam.VideoCapture)
extern void OnVideoCaptureResourceCreatedCallback_Invoke_m57F41946F7DA79E150143DD5882473FA61C84E07 (void);
// 0x00000800 System.IAsyncResult UnityEngine.Windows.WebCam.VideoCapture_OnVideoCaptureResourceCreatedCallback::BeginInvoke(UnityEngine.Windows.WebCam.VideoCapture,System.AsyncCallback,System.Object)
extern void OnVideoCaptureResourceCreatedCallback_BeginInvoke_mE36CE2C8E021A4FF484DBC059B46F8DC1841483A (void);
// 0x00000801 System.Void UnityEngine.Windows.WebCam.VideoCapture_OnVideoCaptureResourceCreatedCallback::EndInvoke(System.IAsyncResult)
extern void OnVideoCaptureResourceCreatedCallback_EndInvoke_m91AF83FB7BD99EF34F87DF6CC07DDF0DB9CF815D (void);
// 0x00000802 System.Void UnityEngine.Windows.WebCam.VideoCapture_OnVideoModeStartedCallback::.ctor(System.Object,System.IntPtr)
extern void OnVideoModeStartedCallback__ctor_mD6DC7A8AD853556AD0D66C32339A735D72B09C47 (void);
// 0x00000803 System.Void UnityEngine.Windows.WebCam.VideoCapture_OnVideoModeStartedCallback::Invoke(UnityEngine.Windows.WebCam.VideoCapture_VideoCaptureResult)
extern void OnVideoModeStartedCallback_Invoke_m13337457016034DE08D05247F064B9EECC351B2E (void);
// 0x00000804 System.IAsyncResult UnityEngine.Windows.WebCam.VideoCapture_OnVideoModeStartedCallback::BeginInvoke(UnityEngine.Windows.WebCam.VideoCapture_VideoCaptureResult,System.AsyncCallback,System.Object)
extern void OnVideoModeStartedCallback_BeginInvoke_m4A3CEBB5158BB4DCB0F8DD884D88A53CE484CEFB (void);
// 0x00000805 System.Void UnityEngine.Windows.WebCam.VideoCapture_OnVideoModeStartedCallback::EndInvoke(System.IAsyncResult)
extern void OnVideoModeStartedCallback_EndInvoke_mF9B72E6808B95E4DC060B1401CDF180A4030B5A8 (void);
// 0x00000806 System.Void UnityEngine.Windows.WebCam.VideoCapture_OnVideoModeStoppedCallback::.ctor(System.Object,System.IntPtr)
extern void OnVideoModeStoppedCallback__ctor_m833B3FF3AD7E58E090CB6238684518693AAF1A97 (void);
// 0x00000807 System.Void UnityEngine.Windows.WebCam.VideoCapture_OnVideoModeStoppedCallback::Invoke(UnityEngine.Windows.WebCam.VideoCapture_VideoCaptureResult)
extern void OnVideoModeStoppedCallback_Invoke_mD5CACF758DA742BCDC3FB7B27A648BAB02340498 (void);
// 0x00000808 System.IAsyncResult UnityEngine.Windows.WebCam.VideoCapture_OnVideoModeStoppedCallback::BeginInvoke(UnityEngine.Windows.WebCam.VideoCapture_VideoCaptureResult,System.AsyncCallback,System.Object)
extern void OnVideoModeStoppedCallback_BeginInvoke_mDBF06C8C08B108AB38F91C075A660AFA1D40876C (void);
// 0x00000809 System.Void UnityEngine.Windows.WebCam.VideoCapture_OnVideoModeStoppedCallback::EndInvoke(System.IAsyncResult)
extern void OnVideoModeStoppedCallback_EndInvoke_m97E26F948C10C4E3C76FF735248FADEE7E3D8398 (void);
// 0x0000080A System.Void UnityEngine.Windows.WebCam.VideoCapture_OnStartedRecordingVideoCallback::.ctor(System.Object,System.IntPtr)
extern void OnStartedRecordingVideoCallback__ctor_m2742271349A83A30DE2116DE61A29174E4186C15 (void);
// 0x0000080B System.Void UnityEngine.Windows.WebCam.VideoCapture_OnStartedRecordingVideoCallback::Invoke(UnityEngine.Windows.WebCam.VideoCapture_VideoCaptureResult)
extern void OnStartedRecordingVideoCallback_Invoke_mA36E25A3C9AE663053A004D216FFCEF82AEF3790 (void);
// 0x0000080C System.IAsyncResult UnityEngine.Windows.WebCam.VideoCapture_OnStartedRecordingVideoCallback::BeginInvoke(UnityEngine.Windows.WebCam.VideoCapture_VideoCaptureResult,System.AsyncCallback,System.Object)
extern void OnStartedRecordingVideoCallback_BeginInvoke_m1E4D58E53791FAACD887CFB36445C5FEA8F7CD06 (void);
// 0x0000080D System.Void UnityEngine.Windows.WebCam.VideoCapture_OnStartedRecordingVideoCallback::EndInvoke(System.IAsyncResult)
extern void OnStartedRecordingVideoCallback_EndInvoke_m2941925C6B4AFEC5855E18BA32423C56EE0FA2A3 (void);
// 0x0000080E System.Void UnityEngine.Windows.WebCam.VideoCapture_OnStoppedRecordingVideoCallback::.ctor(System.Object,System.IntPtr)
extern void OnStoppedRecordingVideoCallback__ctor_mDED3B288194CBA9478D42A22F5FA3E20A8FC4D79 (void);
// 0x0000080F System.Void UnityEngine.Windows.WebCam.VideoCapture_OnStoppedRecordingVideoCallback::Invoke(UnityEngine.Windows.WebCam.VideoCapture_VideoCaptureResult)
extern void OnStoppedRecordingVideoCallback_Invoke_m0CB497020C4935AE90335928C7AB897789C91F16 (void);
// 0x00000810 System.IAsyncResult UnityEngine.Windows.WebCam.VideoCapture_OnStoppedRecordingVideoCallback::BeginInvoke(UnityEngine.Windows.WebCam.VideoCapture_VideoCaptureResult,System.AsyncCallback,System.Object)
extern void OnStoppedRecordingVideoCallback_BeginInvoke_mA92F3961C471FBD5E16F95840A8CB912E20C54BC (void);
// 0x00000811 System.Void UnityEngine.Windows.WebCam.VideoCapture_OnStoppedRecordingVideoCallback::EndInvoke(System.IAsyncResult)
extern void OnStoppedRecordingVideoCallback_EndInvoke_m01C378DD8A73D83471A084E63276028C5317D7E2 (void);
// 0x00000812 System.String UnityEngine.Events.UnityEventTools::TidyAssemblyTypeName(System.String)
extern void UnityEventTools_TidyAssemblyTypeName_m7ABD7C25EA8BF24C586CD9BD637421A12D8C5B44 (void);
// 0x00000813 UnityEngine.Object UnityEngine.Events.ArgumentCache::get_unityObjectArgument()
extern void ArgumentCache_get_unityObjectArgument_m546FEA2DAB93AD1222C0000A882FFAF66DF88B47 (void);
// 0x00000814 System.String UnityEngine.Events.ArgumentCache::get_unityObjectArgumentAssemblyTypeName()
extern void ArgumentCache_get_unityObjectArgumentAssemblyTypeName_mAA84D93F6FE66B3422F4A99D794BCCA11882DE35 (void);
// 0x00000815 System.Int32 UnityEngine.Events.ArgumentCache::get_intArgument()
extern void ArgumentCache_get_intArgument_m8BDE2E1DBF5870F5F91041540EC7F867F0D24CFF (void);
// 0x00000816 System.Single UnityEngine.Events.ArgumentCache::get_floatArgument()
extern void ArgumentCache_get_floatArgument_m941EC215EC34675CA224A311BEDBBEF647F02150 (void);
// 0x00000817 System.String UnityEngine.Events.ArgumentCache::get_stringArgument()
extern void ArgumentCache_get_stringArgument_mE71B434FCF1AA70BF2A922647F419BED0553E7FB (void);
// 0x00000818 System.Boolean UnityEngine.Events.ArgumentCache::get_boolArgument()
extern void ArgumentCache_get_boolArgument_mB66F6E9F16B1246A82A368E8B6AB94C4E05AF127 (void);
// 0x00000819 System.Void UnityEngine.Events.ArgumentCache::OnBeforeSerialize()
extern void ArgumentCache_OnBeforeSerialize_mF5B9D962D88C22BC20AE330FFBC33FB3042604D0 (void);
// 0x0000081A System.Void UnityEngine.Events.ArgumentCache::OnAfterDeserialize()
extern void ArgumentCache_OnAfterDeserialize_mB50D42B36BF948499C4927084E7589D0B1D9C6FB (void);
// 0x0000081B System.Void UnityEngine.Events.ArgumentCache::.ctor()
extern void ArgumentCache__ctor_mF667890D72F5C3C3AE197688DEF71A23310248A0 (void);
// 0x0000081C System.Void UnityEngine.Events.BaseInvokableCall::.ctor()
extern void BaseInvokableCall__ctor_mC60A356F5535F98996ADF5AF925032449DFAF2BB (void);
// 0x0000081D System.Void UnityEngine.Events.BaseInvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern void BaseInvokableCall__ctor_mB7F553CF006225E89AFD3C57820E8FF0E777C3FB (void);
// 0x0000081E System.Void UnityEngine.Events.BaseInvokableCall::Invoke(System.Object[])
// 0x0000081F System.Void UnityEngine.Events.BaseInvokableCall::ThrowOnInvalidArg(System.Object)
// 0x00000820 System.Boolean UnityEngine.Events.BaseInvokableCall::AllowInvoke(System.Delegate)
extern void BaseInvokableCall_AllowInvoke_m84704F31555A5C8AD726DAE1C80929D3F75A00DF (void);
// 0x00000821 System.Boolean UnityEngine.Events.BaseInvokableCall::Find(System.Object,System.Reflection.MethodInfo)
// 0x00000822 System.Void UnityEngine.Events.InvokableCall::add_Delegate(UnityEngine.Events.UnityAction)
extern void InvokableCall_add_Delegate_mA80FC3DDF9C96793161FED5B38577EC44E8ECCEC (void);
// 0x00000823 System.Void UnityEngine.Events.InvokableCall::remove_Delegate(UnityEngine.Events.UnityAction)
extern void InvokableCall_remove_Delegate_m32D6AD4353BD281EAAC1C319D211FF323E87FABF (void);
// 0x00000824 System.Void UnityEngine.Events.InvokableCall::.ctor(System.Object,System.Reflection.MethodInfo)
extern void InvokableCall__ctor_mB755E9394048D1920AA344EA3B3905810042E992 (void);
// 0x00000825 System.Void UnityEngine.Events.InvokableCall::.ctor(UnityEngine.Events.UnityAction)
extern void InvokableCall__ctor_m00346D35103888C24ED7915EC8E1081F0EAB477D (void);
// 0x00000826 System.Void UnityEngine.Events.InvokableCall::Invoke(System.Object[])
extern void InvokableCall_Invoke_mE87495638C5A778726A99872D041A22CA957159E (void);
// 0x00000827 System.Void UnityEngine.Events.InvokableCall::Invoke()
extern void InvokableCall_Invoke_mC0E0B4D35F0795DCF2626DC15E5E92417430AAD7 (void);
// 0x00000828 System.Boolean UnityEngine.Events.InvokableCall::Find(System.Object,System.Reflection.MethodInfo)
extern void InvokableCall_Find_mE1AF062AF0ADE337AEB2728F401CAB23E95D9360 (void);
// 0x00000829 System.Void UnityEngine.Events.InvokableCall`1::add_Delegate(UnityEngine.Events.UnityAction`1<T1>)
// 0x0000082A System.Void UnityEngine.Events.InvokableCall`1::remove_Delegate(UnityEngine.Events.UnityAction`1<T1>)
// 0x0000082B System.Void UnityEngine.Events.InvokableCall`1::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x0000082C System.Void UnityEngine.Events.InvokableCall`1::.ctor(UnityEngine.Events.UnityAction`1<T1>)
// 0x0000082D System.Void UnityEngine.Events.InvokableCall`1::Invoke(System.Object[])
// 0x0000082E System.Void UnityEngine.Events.InvokableCall`1::Invoke(T1)
// 0x0000082F System.Boolean UnityEngine.Events.InvokableCall`1::Find(System.Object,System.Reflection.MethodInfo)
// 0x00000830 System.Void UnityEngine.Events.InvokableCall`2::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x00000831 System.Void UnityEngine.Events.InvokableCall`2::Invoke(System.Object[])
// 0x00000832 System.Boolean UnityEngine.Events.InvokableCall`2::Find(System.Object,System.Reflection.MethodInfo)
// 0x00000833 System.Void UnityEngine.Events.InvokableCall`3::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x00000834 System.Void UnityEngine.Events.InvokableCall`3::Invoke(System.Object[])
// 0x00000835 System.Void UnityEngine.Events.InvokableCall`3::Invoke(T1,T2,T3)
// 0x00000836 System.Boolean UnityEngine.Events.InvokableCall`3::Find(System.Object,System.Reflection.MethodInfo)
// 0x00000837 System.Void UnityEngine.Events.InvokableCall`4::.ctor(System.Object,System.Reflection.MethodInfo)
// 0x00000838 System.Void UnityEngine.Events.InvokableCall`4::Invoke(System.Object[])
// 0x00000839 System.Boolean UnityEngine.Events.InvokableCall`4::Find(System.Object,System.Reflection.MethodInfo)
// 0x0000083A System.Void UnityEngine.Events.CachedInvokableCall`1::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
// 0x0000083B System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(System.Object[])
// 0x0000083C System.Void UnityEngine.Events.CachedInvokableCall`1::Invoke(T)
// 0x0000083D UnityEngine.Object UnityEngine.Events.PersistentCall::get_target()
extern void PersistentCall_get_target_mE1268D2B40A4618C5E318D439F37022B969A6115 (void);
// 0x0000083E System.String UnityEngine.Events.PersistentCall::get_targetAssemblyTypeName()
extern void PersistentCall_get_targetAssemblyTypeName_m2F53F996EA6BFFDFCDF1D10B6361DE2A98DD9113 (void);
// 0x0000083F System.String UnityEngine.Events.PersistentCall::get_methodName()
extern void PersistentCall_get_methodName_m249643D059927A05051B73309FCEAC6A6C9F9C88 (void);
// 0x00000840 UnityEngine.Events.PersistentListenerMode UnityEngine.Events.PersistentCall::get_mode()
extern void PersistentCall_get_mode_m7041C70D7CA9CC2D7FCB5D31C81E9C519AF1FEB8 (void);
// 0x00000841 UnityEngine.Events.ArgumentCache UnityEngine.Events.PersistentCall::get_arguments()
extern void PersistentCall_get_arguments_m9FDD073B5551520F0FF4A672C60E237CADBC3435 (void);
// 0x00000842 System.Boolean UnityEngine.Events.PersistentCall::IsValid()
extern void PersistentCall_IsValid_m3BDFC18A3D1AD8ECA8822EA4265127B35237E11B (void);
// 0x00000843 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetRuntimeCall(UnityEngine.Events.UnityEventBase)
extern void PersistentCall_GetRuntimeCall_mCD3A4A0287D8A9C2CF00D894F378BD4E4684287A (void);
// 0x00000844 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.PersistentCall::GetObjectCall(UnityEngine.Object,System.Reflection.MethodInfo,UnityEngine.Events.ArgumentCache)
extern void PersistentCall_GetObjectCall_m5CDF291A373C8FD34513EF1A1D26C9B36ED7A95D (void);
// 0x00000845 System.Void UnityEngine.Events.PersistentCall::OnBeforeSerialize()
extern void PersistentCall_OnBeforeSerialize_mD5109AA99B22304579296F4B6077647839456346 (void);
// 0x00000846 System.Void UnityEngine.Events.PersistentCall::OnAfterDeserialize()
extern void PersistentCall_OnAfterDeserialize_m73B5F8F782FC97AE4C4DCE3DB2C1D9F102455D9E (void);
// 0x00000847 System.Void UnityEngine.Events.PersistentCall::.ctor()
extern void PersistentCall__ctor_mCA080B989171E4FE13761074DD7DE76683969140 (void);
// 0x00000848 System.Void UnityEngine.Events.PersistentCallGroup::.ctor()
extern void PersistentCallGroup__ctor_m0F94D4591DB6C4D6C7B997FA45A39C680610B1E0 (void);
// 0x00000849 System.Void UnityEngine.Events.PersistentCallGroup::Initialize(UnityEngine.Events.InvokableCallList,UnityEngine.Events.UnityEventBase)
extern void PersistentCallGroup_Initialize_m162FC343BA2C41CBBB6358D624CBD2CED9468833 (void);
// 0x0000084A System.Void UnityEngine.Events.InvokableCallList::AddPersistentInvokableCall(UnityEngine.Events.BaseInvokableCall)
extern void InvokableCallList_AddPersistentInvokableCall_mE7A19335649958FBF5522D7468D6075EEC325D9C (void);
// 0x0000084B System.Void UnityEngine.Events.InvokableCallList::AddListener(UnityEngine.Events.BaseInvokableCall)
extern void InvokableCallList_AddListener_m07F4E145332E2059D570D8300CB422F56F6B0BF1 (void);
// 0x0000084C System.Void UnityEngine.Events.InvokableCallList::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern void InvokableCallList_RemoveListener_mDE659068D9590D54D00436CCBDB3D27DCD263549 (void);
// 0x0000084D System.Void UnityEngine.Events.InvokableCallList::ClearPersistent()
extern void InvokableCallList_ClearPersistent_m9A59E6161F8E42120439B76FE952A17DA742443C (void);
// 0x0000084E System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.InvokableCallList::PrepareInvoke()
extern void InvokableCallList_PrepareInvoke_mE340D329F5FC08EA77FC0F3662FF5E5BB85AE0B1 (void);
// 0x0000084F System.Void UnityEngine.Events.InvokableCallList::.ctor()
extern void InvokableCallList__ctor_m986F4056CA5480D44854152DB768E031AF95C5D8 (void);
// 0x00000850 System.Void UnityEngine.Events.UnityEventBase::.ctor()
extern void UnityEventBase__ctor_m8F423B800E573ED81DF59EB55CD88D48E817B101 (void);
// 0x00000851 System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m4E84FB82333E5AA5812095E536810C4BCAF55727 (void);
// 0x00000852 System.Void UnityEngine.Events.UnityEventBase::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mA1BAB2B28C0E4A62BA06FEF5FCAD8A67C3449472 (void);
// 0x00000853 System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod_Impl(System.String,System.Type)
// 0x00000854 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEventBase::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x00000855 System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(UnityEngine.Events.PersistentCall)
extern void UnityEventBase_FindMethod_m665F2360483EC2BE2D190C6EFA4A624FB8722089 (void);
// 0x00000856 System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::FindMethod(System.String,System.Type,UnityEngine.Events.PersistentListenerMode,System.Type)
extern void UnityEventBase_FindMethod_m9F887E46F769E2C1D0CFE3C71F98892F9C461025 (void);
// 0x00000857 System.Void UnityEngine.Events.UnityEventBase::DirtyPersistentCalls()
extern void UnityEventBase_DirtyPersistentCalls_m85DA5AEA84F8C32D2FDF5DD58D9B7EF704B7B238 (void);
// 0x00000858 System.Void UnityEngine.Events.UnityEventBase::RebuildPersistentCallsIfNeeded()
extern void UnityEventBase_RebuildPersistentCallsIfNeeded_m1FF3E4C46BB16B554103FAAAF3BBCE52C991D21F (void);
// 0x00000859 System.Void UnityEngine.Events.UnityEventBase::AddCall(UnityEngine.Events.BaseInvokableCall)
extern void UnityEventBase_AddCall_mB4825708CFE71BBF9B167EB16FC911CFF95D101C (void);
// 0x0000085A System.Void UnityEngine.Events.UnityEventBase::RemoveListener(System.Object,System.Reflection.MethodInfo)
extern void UnityEventBase_RemoveListener_m0B091A723044E4120D592AC65CBD152AC3DF929E (void);
// 0x0000085B System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall> UnityEngine.Events.UnityEventBase::PrepareInvoke()
extern void UnityEventBase_PrepareInvoke_m999D0F37E0D88375576323D30B67ED7FDC7A779D (void);
// 0x0000085C System.String UnityEngine.Events.UnityEventBase::ToString()
extern void UnityEventBase_ToString_m0A3BBFEC8C23E044D52502CE201FE82EEF60A8E7 (void);
// 0x0000085D System.Reflection.MethodInfo UnityEngine.Events.UnityEventBase::GetValidMethodInfo(System.Type,System.String,System.Type[])
extern void UnityEventBase_GetValidMethodInfo_mBA413E99550A6AD8B36F5BEB8682B1536E976C36 (void);
// 0x0000085E System.Void UnityEngine.Events.UnityAction::.ctor(System.Object,System.IntPtr)
extern void UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA (void);
// 0x0000085F System.Void UnityEngine.Events.UnityAction::Invoke()
extern void UnityAction_Invoke_mFFF1FFE59D8285F8A81350E6D5C4D791F44F6AC9 (void);
// 0x00000860 System.IAsyncResult UnityEngine.Events.UnityAction::BeginInvoke(System.AsyncCallback,System.Object)
extern void UnityAction_BeginInvoke_m67AAC6F5871162346CBCCA0CA51AA38307A4ABB8 (void);
// 0x00000861 System.Void UnityEngine.Events.UnityAction::EndInvoke(System.IAsyncResult)
extern void UnityAction_EndInvoke_mC02B54511B4220CF6D1B37F14BF07D522E91786B (void);
// 0x00000862 System.Void UnityEngine.Events.UnityEvent::.ctor()
extern void UnityEvent__ctor_m98D9C5A59898546B23A45388CFACA25F52A9E5A6 (void);
// 0x00000863 System.Void UnityEngine.Events.UnityEvent::AddListener(UnityEngine.Events.UnityAction)
extern void UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D (void);
// 0x00000864 System.Reflection.MethodInfo UnityEngine.Events.UnityEvent::FindMethod_Impl(System.String,System.Type)
extern void UnityEvent_FindMethod_Impl_m763FB43F24EF4A092E26FAE6F6DF561CF360475A (void);
// 0x00000865 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(System.Object,System.Reflection.MethodInfo)
extern void UnityEvent_GetDelegate_m7AD1450601F49B957A87161BE6D14020720A1A56 (void);
// 0x00000866 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent::GetDelegate(UnityEngine.Events.UnityAction)
extern void UnityEvent_GetDelegate_m86F8240BF539D38F90F6BE322CF5CA91C17B13B9 (void);
// 0x00000867 System.Void UnityEngine.Events.UnityEvent::Invoke()
extern void UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5 (void);
// 0x00000868 System.Void UnityEngine.Events.UnityAction`1::.ctor(System.Object,System.IntPtr)
// 0x00000869 System.Void UnityEngine.Events.UnityAction`1::Invoke(T0)
// 0x0000086A System.IAsyncResult UnityEngine.Events.UnityAction`1::BeginInvoke(T0,System.AsyncCallback,System.Object)
// 0x0000086B System.Void UnityEngine.Events.UnityAction`1::EndInvoke(System.IAsyncResult)
// 0x0000086C System.Void UnityEngine.Events.UnityEvent`1::.ctor()
// 0x0000086D System.Void UnityEngine.Events.UnityEvent`1::AddListener(UnityEngine.Events.UnityAction`1<T0>)
// 0x0000086E System.Void UnityEngine.Events.UnityEvent`1::RemoveListener(UnityEngine.Events.UnityAction`1<T0>)
// 0x0000086F System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`1::FindMethod_Impl(System.String,System.Type)
// 0x00000870 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x00000871 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`1::GetDelegate(UnityEngine.Events.UnityAction`1<T0>)
// 0x00000872 System.Void UnityEngine.Events.UnityEvent`1::Invoke(T0)
// 0x00000873 System.Void UnityEngine.Events.UnityAction`2::.ctor(System.Object,System.IntPtr)
// 0x00000874 System.Void UnityEngine.Events.UnityAction`2::Invoke(T0,T1)
// 0x00000875 System.IAsyncResult UnityEngine.Events.UnityAction`2::BeginInvoke(T0,T1,System.AsyncCallback,System.Object)
// 0x00000876 System.Void UnityEngine.Events.UnityAction`2::EndInvoke(System.IAsyncResult)
// 0x00000877 System.Void UnityEngine.Events.UnityEvent`2::.ctor()
// 0x00000878 System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`2::FindMethod_Impl(System.String,System.Type)
// 0x00000879 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`2::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x0000087A System.Void UnityEngine.Events.UnityAction`3::.ctor(System.Object,System.IntPtr)
// 0x0000087B System.Void UnityEngine.Events.UnityAction`3::Invoke(T0,T1,T2)
// 0x0000087C System.IAsyncResult UnityEngine.Events.UnityAction`3::BeginInvoke(T0,T1,T2,System.AsyncCallback,System.Object)
// 0x0000087D System.Void UnityEngine.Events.UnityAction`3::EndInvoke(System.IAsyncResult)
// 0x0000087E System.Void UnityEngine.Events.UnityEvent`3::.ctor()
// 0x0000087F System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`3::FindMethod_Impl(System.String,System.Type)
// 0x00000880 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`3::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x00000881 System.Void UnityEngine.Events.UnityEvent`3::Invoke(T0,T1,T2)
// 0x00000882 System.Void UnityEngine.Events.UnityAction`4::.ctor(System.Object,System.IntPtr)
// 0x00000883 System.Void UnityEngine.Events.UnityAction`4::Invoke(T0,T1,T2,T3)
// 0x00000884 System.IAsyncResult UnityEngine.Events.UnityAction`4::BeginInvoke(T0,T1,T2,T3,System.AsyncCallback,System.Object)
// 0x00000885 System.Void UnityEngine.Events.UnityAction`4::EndInvoke(System.IAsyncResult)
// 0x00000886 System.Void UnityEngine.Events.UnityEvent`4::.ctor()
// 0x00000887 System.Reflection.MethodInfo UnityEngine.Events.UnityEvent`4::FindMethod_Impl(System.String,System.Type)
// 0x00000888 UnityEngine.Events.BaseInvokableCall UnityEngine.Events.UnityEvent`4::GetDelegate(System.Object,System.Reflection.MethodInfo)
// 0x00000889 System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
extern void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (void);
// 0x0000088A System.Void UnityEngine.Scripting.PreserveAttribute::.ctor()
extern void PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2 (void);
// 0x0000088B System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttributeData::Set(System.Boolean,System.String,System.String,System.String)
extern void MovedFromAttributeData_Set_mC5572E6033FCE1C53B8C43A8D5280E71CE388640_AdjustorThunk (void);
// 0x0000088C System.Void UnityEngine.Scripting.APIUpdating.MovedFromAttribute::.ctor(System.String)
extern void MovedFromAttribute__ctor_mA4B2632CE3004A3E0EA8E1241736518320806568 (void);
// 0x0000088D System.String UnityEngine.SceneManagement.Scene::GetNameInternal(System.Int32)
extern void Scene_GetNameInternal_mE4879219C90C34EE00F46188C29AE74D74FD15D2 (void);
// 0x0000088E System.Int32 UnityEngine.SceneManagement.Scene::GetBuildIndexInternal(System.Int32)
extern void Scene_GetBuildIndexInternal_m143AF2C97F3B5E24FA90B8FBC709411976F3EE23 (void);
// 0x0000088F System.Int32 UnityEngine.SceneManagement.Scene::get_handle()
extern void Scene_get_handle_m57967C50E461CD48441CA60645AF8FA04F7B132C_AdjustorThunk (void);
// 0x00000890 System.String UnityEngine.SceneManagement.Scene::get_name()
extern void Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8_AdjustorThunk (void);
// 0x00000891 System.Int32 UnityEngine.SceneManagement.Scene::get_buildIndex()
extern void Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4_AdjustorThunk (void);
// 0x00000892 System.Boolean UnityEngine.SceneManagement.Scene::op_Equality(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void Scene_op_Equality_m5B02632BCD075BF2857A75AF21F5F82AE22C4E4D (void);
// 0x00000893 System.Int32 UnityEngine.SceneManagement.Scene::GetHashCode()
extern void Scene_GetHashCode_mFC620B8CA1EAA64BF0D7B33E8D3EAFAEDC9A6DCB_AdjustorThunk (void);
// 0x00000894 System.Boolean UnityEngine.SceneManagement.Scene::Equals(System.Object)
extern void Scene_Equals_m78D2F82F3133AD32F35C7981B65D0980A6C3006D_AdjustorThunk (void);
// 0x00000895 System.Int32 UnityEngine.SceneManagement.SceneManager::get_sceneCount()
extern void SceneManager_get_sceneCount_m57B8EB790D8B6673BA840442B4F125121CC5456E (void);
// 0x00000896 UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetActiveScene()
extern void SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4 (void);
// 0x00000897 UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::GetSceneAt(System.Int32)
extern void SceneManager_GetSceneAt_m46AF96028C6A3A09198ABB313E4206D93A8D1F3F (void);
// 0x00000898 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal(System.String,System.Int32,UnityEngine.SceneManagement.LoadSceneParameters,System.Boolean)
extern void SceneManager_LoadSceneAsyncNameIndexInternal_mD72656A5141151775F28886F59D2830B0EC1B659 (void);
// 0x00000899 System.Void UnityEngine.SceneManagement.SceneManager::add_sceneUnloaded(UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>)
extern void SceneManager_add_sceneUnloaded_m2CE4194528D7AAE67F494EB15C8BBE650FBA4C72 (void);
// 0x0000089A System.Void UnityEngine.SceneManagement.SceneManager::remove_sceneUnloaded(UnityEngine.Events.UnityAction`1<UnityEngine.SceneManagement.Scene>)
extern void SceneManager_remove_sceneUnloaded_mA7CD60918C6EE3B8D8623201A487C2CEC8D4EA5E (void);
// 0x0000089B System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.String)
extern void SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092 (void);
// 0x0000089C UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::LoadScene(System.String,UnityEngine.SceneManagement.LoadSceneParameters)
extern void SceneManager_LoadScene_m84A1D8B405E365CFC372040E311669E497D12591 (void);
// 0x0000089D System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
extern void SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5 (void);
// 0x0000089E UnityEngine.SceneManagement.Scene UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32,UnityEngine.SceneManagement.LoadSceneParameters)
extern void SceneManager_LoadScene_m26A2A5DA3CBD39F367587C89ADC7ACD3B2C223C7 (void);
// 0x0000089F System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneLoaded(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.LoadSceneMode)
extern void SceneManager_Internal_SceneLoaded_m3546B371F03BC8DC053FFF165AB25ADF44A183BE (void);
// 0x000008A0 System.Void UnityEngine.SceneManagement.SceneManager::Internal_SceneUnloaded(UnityEngine.SceneManagement.Scene)
extern void SceneManager_Internal_SceneUnloaded_m9A68F15B0FA483633F24E0E50574CFB6DD2D5520 (void);
// 0x000008A1 System.Void UnityEngine.SceneManagement.SceneManager::Internal_ActiveSceneChanged(UnityEngine.SceneManagement.Scene,UnityEngine.SceneManagement.Scene)
extern void SceneManager_Internal_ActiveSceneChanged_m4094F4307C6B3DE0BB87ED646EA9BD6640B831DC (void);
// 0x000008A2 System.Void UnityEngine.SceneManagement.SceneManager::GetActiveScene_Injected(UnityEngine.SceneManagement.Scene&)
extern void SceneManager_GetActiveScene_Injected_m7F05E2A355D8CA1E3496075D7E7CCC1327880ED6 (void);
// 0x000008A3 System.Void UnityEngine.SceneManagement.SceneManager::GetSceneAt_Injected(System.Int32,UnityEngine.SceneManagement.Scene&)
extern void SceneManager_GetSceneAt_Injected_m2649DB4E01B925CFDF17EBDFB48816447082771A (void);
// 0x000008A4 UnityEngine.AsyncOperation UnityEngine.SceneManagement.SceneManager::LoadSceneAsyncNameIndexInternal_Injected(System.String,System.Int32,UnityEngine.SceneManagement.LoadSceneParameters&,System.Boolean)
extern void SceneManager_LoadSceneAsyncNameIndexInternal_Injected_m719D102B0AF20644651BDC09DA76836B331A80FE (void);
// 0x000008A5 System.Void UnityEngine.SceneManagement.LoadSceneParameters::.ctor(UnityEngine.SceneManagement.LoadSceneMode)
extern void LoadSceneParameters__ctor_m6B4C0245743813570AE22B68A8F75332248929AC_AdjustorThunk (void);
// 0x000008A6 System.Void UnityEngine.LowLevel.PlayerLoopSystem_UpdateFunction::.ctor(System.Object,System.IntPtr)
extern void UpdateFunction__ctor_mB10AB83A3F547AC95FF726E8A7B5FF9C16EC1319 (void);
// 0x000008A7 System.Void UnityEngine.LowLevel.PlayerLoopSystem_UpdateFunction::Invoke()
extern void UpdateFunction_Invoke_mB17C55B92FAECE51078028F59A9F1EAC2016B1AD (void);
// 0x000008A8 System.IAsyncResult UnityEngine.LowLevel.PlayerLoopSystem_UpdateFunction::BeginInvoke(System.AsyncCallback,System.Object)
extern void UpdateFunction_BeginInvoke_m1EB0935AB72A286D153ACEBD0E5D66BB38BD6799 (void);
// 0x000008A9 System.Void UnityEngine.LowLevel.PlayerLoopSystem_UpdateFunction::EndInvoke(System.IAsyncResult)
extern void UpdateFunction_EndInvoke_mB4BC0AA40E9C83274116DF6467D72DD4902DA624 (void);
// 0x000008AA System.Void UnityEngine.Networking.PlayerConnection.MessageEventArgs::.ctor()
extern void MessageEventArgs__ctor_m18272C7358FAC22848ED84A952DCE17E8CD623BA (void);
// 0x000008AB UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::get_instance()
extern void PlayerConnection_get_instance_mA46C9194C4D2FE0BB06C135C4C0521DD516592F6 (void);
// 0x000008AC System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::get_isConnected()
extern void PlayerConnection_get_isConnected_m315E30E90CA2BBEF2C2C366EF68BF0380C2BCF28 (void);
// 0x000008AD UnityEngine.Networking.PlayerConnection.PlayerConnection UnityEngine.Networking.PlayerConnection.PlayerConnection::CreateInstance()
extern void PlayerConnection_CreateInstance_m761E97DB4F693FC1A92418088B90872AA559BFFE (void);
// 0x000008AE System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::OnEnable()
extern void PlayerConnection_OnEnable_m89048E6B2F1A820F9FD99C3237CB55FF7F56F558 (void);
// 0x000008AF UnityEngine.IPlayerEditorConnectionNative UnityEngine.Networking.PlayerConnection.PlayerConnection::GetConnectionNativeApi()
extern void PlayerConnection_GetConnectionNativeApi_m94F4E5D341869936857659D92591483D5F6EE95F (void);
// 0x000008B0 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Register(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern void PlayerConnection_Register_m97A0118B2F172AA0236D0D428F2C6F6E8326BF3D (void);
// 0x000008B1 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Unregister(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern void PlayerConnection_Unregister_m1D2F6BE95E82D60F6EA02139AA64D7D58B4B889B (void);
// 0x000008B2 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterConnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_RegisterConnection_m3DA7DB08B44854C6914F9A154C486EFFCC2B4E8D (void);
// 0x000008B3 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::RegisterDisconnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_RegisterDisconnection_mA0D4C45243C1244339CAF66E45C65A6130E8B87A (void);
// 0x000008B4 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::UnregisterConnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_UnregisterConnection_mC44D98524BD2E21B7658AB898B8283C34A5AD96E (void);
// 0x000008B5 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::UnregisterDisconnection(UnityEngine.Events.UnityAction`1<System.Int32>)
extern void PlayerConnection_UnregisterDisconnection_m1DB86C159F601428F59DAB3A4BB912C780F8EAFD (void);
// 0x000008B6 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::Send(System.Guid,System.Byte[])
extern void PlayerConnection_Send_m51137877D6E519903AEA4A506ABD48A30AF684F0 (void);
// 0x000008B7 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::TrySend(System.Guid,System.Byte[])
extern void PlayerConnection_TrySend_m9AD3046E615B6DECE991776EEFAB10EA95E478A6 (void);
// 0x000008B8 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection::BlockUntilRecvMsg(System.Guid,System.Int32)
extern void PlayerConnection_BlockUntilRecvMsg_m3C3CE1A885657B18DEE2C10066425B45F2B15FAC (void);
// 0x000008B9 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectAll()
extern void PlayerConnection_DisconnectAll_m2A55CA3DECCC5683315504EACB9311E565D1EFF5 (void);
// 0x000008BA System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::MessageCallbackInternal(System.IntPtr,System.UInt64,System.UInt64,System.String)
extern void PlayerConnection_MessageCallbackInternal_m223C558BDA58E11AE597D73CEC359718A336FD74 (void);
// 0x000008BB System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::ConnectedCallbackInternal(System.Int32)
extern void PlayerConnection_ConnectedCallbackInternal_m9143E59EA71734C96F877CFD685F0935CEEBA13E (void);
// 0x000008BC System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::DisconnectedCallback(System.Int32)
extern void PlayerConnection_DisconnectedCallback_mCC356CB2D6C8820A435950F630BA349B7CA5E267 (void);
// 0x000008BD System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection::.ctor()
extern void PlayerConnection__ctor_mBB02EDC32EB6458CB527F9703CFD4400C7CDCB92 (void);
// 0x000008BE System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m66E5B9C72A27F0645F9854522ACE087CE32A5EEE (void);
// 0x000008BF System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection_<>c__DisplayClass12_0::<Register>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass12_0_U3CRegisterU3Eb__0_m27C1416BAD7AF0A1BF83339C8A03865A6487B234 (void);
// 0x000008C0 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection_<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m75A8930EBA7C6BF81519358930656DAA2FBDDEDB (void);
// 0x000008C1 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerConnection_<>c__DisplayClass13_0::<Unregister>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass13_0_U3CUnregisterU3Eb__0_mBA34804D7BB1B4FFE45D077BBB07BFA81C8DCB00 (void);
// 0x000008C2 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection_<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m2B215926A23E33037D754DFAFDF8459D82243683 (void);
// 0x000008C3 System.Void UnityEngine.Networking.PlayerConnection.PlayerConnection_<>c__DisplayClass20_0::<BlockUntilRecvMsg>b__0(UnityEngine.Networking.PlayerConnection.MessageEventArgs)
extern void U3CU3Ec__DisplayClass20_0_U3CBlockUntilRecvMsgU3Eb__0_m1D6C3976419CFCE0B612D04C135A985707C215E2 (void);
// 0x000008C4 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::InvokeMessageIdSubscribers(System.Guid,System.Byte[],System.Int32)
extern void PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m157C5C69D6E22A8BCBDC40929BA67E1B6E586389 (void);
// 0x000008C5 UnityEngine.Events.UnityEvent`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs> UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::AddAndCreate(System.Guid)
extern void PlayerEditorConnectionEvents_AddAndCreate_m9722774BB976599BF8AD8745CA8EC3218828306B (void);
// 0x000008C6 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::UnregisterManagedCallback(System.Guid,UnityEngine.Events.UnityAction`1<UnityEngine.Networking.PlayerConnection.MessageEventArgs>)
extern void PlayerEditorConnectionEvents_UnregisterManagedCallback_mEB4C4FBCBA7A9527475914F61AFDB9676B8BEFB3 (void);
// 0x000008C7 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents::.ctor()
extern void PlayerEditorConnectionEvents__ctor_m40946A5B9D9FB40849BEA07DCD600891229249A0 (void);
// 0x000008C8 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_MessageEvent::.ctor()
extern void MessageEvent__ctor_mE4D70D8837C51E422C9A40C3F8F34ACB9AB572BB (void);
// 0x000008C9 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_ConnectionChangeEvent::.ctor()
extern void ConnectionChangeEvent__ctor_m95EBD8B5EA1C4A14A5F2B71CEE1A2C18C73DB0A1 (void);
// 0x000008CA System.Guid UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_MessageTypeSubscribers::get_MessageTypeId()
extern void MessageTypeSubscribers_get_MessageTypeId_m5A873C55E97728BD12BA52B5DB72FFA366992DD9 (void);
// 0x000008CB System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_MessageTypeSubscribers::set_MessageTypeId(System.Guid)
extern void MessageTypeSubscribers_set_MessageTypeId_m7125FF3E71F4E678644F056A4DC5C29EFB749762 (void);
// 0x000008CC System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_MessageTypeSubscribers::.ctor()
extern void MessageTypeSubscribers__ctor_mA51A6D3E38DBAA5B8237BAB1688F669F24982F29 (void);
// 0x000008CD System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_<>c__DisplayClass6_0::.ctor()
extern void U3CU3Ec__DisplayClass6_0__ctor_mEB8DB2BFDA9F6F083E77F1EC1CE3F4861CD1815A (void);
// 0x000008CE System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_<>c__DisplayClass6_0::<InvokeMessageIdSubscribers>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass6_0_U3CInvokeMessageIdSubscribersU3Eb__0_mEF5D5DFC8F7C2CEC86378AC3BBD40E80A1D9C615 (void);
// 0x000008CF System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m97B67DA8AA306A160A790CCDD869669878DDB7F3 (void);
// 0x000008D0 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_<>c__DisplayClass7_0::<AddAndCreate>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass7_0_U3CAddAndCreateU3Eb__0_mAA3D205F35F7BE200A5DDD14099F56312FBED909 (void);
// 0x000008D1 System.Void UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m18AC0B2825D7BB04F59DC800DFF74D45921A28FA (void);
// 0x000008D2 System.Boolean UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_<>c__DisplayClass8_0::<UnregisterManagedCallback>b__0(UnityEngine.Networking.PlayerConnection.PlayerEditorConnectionEvents_MessageTypeSubscribers)
extern void U3CU3Ec__DisplayClass8_0_U3CUnregisterManagedCallbackU3Eb__0_m70805C4CE0F8DD258CC3975A8C90313A0A609BE3 (void);
// 0x000008D3 System.Void UnityEngine.Internal.DefaultValueAttribute::.ctor(System.String)
extern void DefaultValueAttribute__ctor_m7A9877491C22E8CDCFDAD240D04156D4FAE7D6C6 (void);
// 0x000008D4 System.Object UnityEngine.Internal.DefaultValueAttribute::get_Value()
extern void DefaultValueAttribute_get_Value_m333925936EF155BACAEF24CE1ADE07085722606E (void);
// 0x000008D5 System.Boolean UnityEngine.Internal.DefaultValueAttribute::Equals(System.Object)
extern void DefaultValueAttribute_Equals_mD096E8E7C8C6051AD743AAB7CAADB27428871E51 (void);
// 0x000008D6 System.Int32 UnityEngine.Internal.DefaultValueAttribute::GetHashCode()
extern void DefaultValueAttribute_GetHashCode_mA74BBB26F6C0B49AA44C175DBFDB82F00E497595 (void);
// 0x000008D7 System.Void UnityEngine.Internal.ExcludeFromDocsAttribute::.ctor()
extern void ExcludeFromDocsAttribute__ctor_mFA14E76D8A30ED8CA3ADCDA83BE056E54753825D (void);
// 0x000008D8 System.Boolean UnityEngine.Rendering.CameraEventUtils::IsValid(UnityEngine.Rendering.CameraEvent)
extern void CameraEventUtils_IsValid_m979977DB6D2CF349B44CF0D1D4B409216DAA8155 (void);
// 0x000008D9 System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Rendering.BuiltinRenderTextureType)
extern void RenderTargetIdentifier__ctor_m8F174D7C9A3172B5C5D0C5C8933973801157CDBB_AdjustorThunk (void);
// 0x000008DA System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(System.Int32)
extern void RenderTargetIdentifier__ctor_m76845E674E8D1597F68C57F362F522E37CCFCD5D_AdjustorThunk (void);
// 0x000008DB System.Void UnityEngine.Rendering.RenderTargetIdentifier::.ctor(UnityEngine.Texture)
extern void RenderTargetIdentifier__ctor_m23C0BAC2C6EF09642BC468CF9CABDF0D2B0DB9EC_AdjustorThunk (void);
// 0x000008DC UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(UnityEngine.Rendering.BuiltinRenderTextureType)
extern void RenderTargetIdentifier_op_Implicit_mB7B58C1D295E2DAE3C76874D030D4878A825E359 (void);
// 0x000008DD UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(System.Int32)
extern void RenderTargetIdentifier_op_Implicit_m065F5C06D85EAF99A60942A197E4CB25FB38B20B (void);
// 0x000008DE UnityEngine.Rendering.RenderTargetIdentifier UnityEngine.Rendering.RenderTargetIdentifier::op_Implicit(UnityEngine.Texture)
extern void RenderTargetIdentifier_op_Implicit_mFF9B98B136B3AB4E9413777F931392D02AA7A8FA (void);
// 0x000008DF System.String UnityEngine.Rendering.RenderTargetIdentifier::ToString()
extern void RenderTargetIdentifier_ToString_m6A676BAE1AD75ADAE5134EE9E243325DBC23CC4D_AdjustorThunk (void);
// 0x000008E0 System.Int32 UnityEngine.Rendering.RenderTargetIdentifier::GetHashCode()
extern void RenderTargetIdentifier_GetHashCode_mB5C49CB1724A3086C19B504C1B659754D67AB967_AdjustorThunk (void);
// 0x000008E1 System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(UnityEngine.Rendering.RenderTargetIdentifier)
extern void RenderTargetIdentifier_Equals_mFDF40A5663B058DA6026AAA920C0CFE6BAD05D1A_AdjustorThunk (void);
// 0x000008E2 System.Boolean UnityEngine.Rendering.RenderTargetIdentifier::Equals(System.Object)
extern void RenderTargetIdentifier_Equals_m57C0226B891EA78C5A9387C7B2A225CE3E4A521A_AdjustorThunk (void);
// 0x000008E3 System.Boolean UnityEngine.Rendering.GraphicsSettings::get_lightsUseLinearIntensity()
extern void GraphicsSettings_get_lightsUseLinearIntensity_m02A37F59F36C77877FC86B93478BC5795F70FFB2 (void);
// 0x000008E4 System.Int32 UnityEngine.Rendering.OnDemandRendering::get_renderFrameInterval()
extern void OnDemandRendering_get_renderFrameInterval_m7B3F913B3C16E756BE5DF901D86FC7F869759367 (void);
// 0x000008E5 System.Void UnityEngine.Rendering.OnDemandRendering::GetRenderFrameInterval(System.Int32&)
extern void OnDemandRendering_GetRenderFrameInterval_mF254C740E4C04295AC2EF10D05D22BCA179D7685 (void);
// 0x000008E6 System.Void UnityEngine.Rendering.OnDemandRendering::.cctor()
extern void OnDemandRendering__cctor_m3C3EFF1AE188B9FACE3E94D9F333CF873D110FD0 (void);
// 0x000008E7 System.IntPtr UnityEngine.Rendering.CommandBuffer::InitBuffer()
extern void CommandBuffer_InitBuffer_mEF17DF124E9787290AF80B935FDE92C46E540F6D (void);
// 0x000008E8 System.Void UnityEngine.Rendering.CommandBuffer::ReleaseBuffer()
extern void CommandBuffer_ReleaseBuffer_m778F98EED78913829C13948F5942D3B68EDEC41C (void);
// 0x000008E9 System.Void UnityEngine.Rendering.CommandBuffer::set_name(System.String)
extern void CommandBuffer_set_name_m4217524F48F215BB404C2847176AA50360CA6CE8 (void);
// 0x000008EA System.Void UnityEngine.Rendering.CommandBuffer::Clear()
extern void CommandBuffer_Clear_mDE798ACE9294B43B9387A6B06E43B3D4A30A7092 (void);
// 0x000008EB System.Void UnityEngine.Rendering.CommandBuffer::Internal_DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
extern void CommandBuffer_Internal_DrawMesh_mE0A7DCCA8C72E6F9CB35D2AB2AC0C61452D2E9B6 (void);
// 0x000008EC System.Void UnityEngine.Rendering.CommandBuffer::Blit_Texture(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Int32)
extern void CommandBuffer_Blit_Texture_m1F9A84A7C34C401D3B94382E8059C3657B17E98C (void);
// 0x000008ED System.Void UnityEngine.Rendering.CommandBuffer::Blit_Identifier(UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Int32)
extern void CommandBuffer_Blit_Identifier_m5AF32AB0279B7FCFD1DFFE934A9551495C1ACDB0 (void);
// 0x000008EE System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32,System.Boolean,UnityEngine.RenderTextureMemoryless,System.Boolean)
extern void CommandBuffer_GetTemporaryRT_mF4C1B7600F2E8E0C292A718A6309A84ECD870C83 (void);
// 0x000008EF System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32,System.Boolean,UnityEngine.RenderTextureMemoryless)
extern void CommandBuffer_GetTemporaryRT_m6677EA531A6D98CE18EA2EFB2F425CE02E5B56DD (void);
// 0x000008F0 System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.Experimental.Rendering.GraphicsFormat,System.Int32)
extern void CommandBuffer_GetTemporaryRT_m7D44FEC654D1CC488530CC7D219BB4EBC9416F0C (void);
// 0x000008F1 System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void CommandBuffer_GetTemporaryRT_m425B035D860BFC45398E5AF258333647E8FACC2A (void);
// 0x000008F2 System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32,System.Boolean,UnityEngine.RenderTextureMemoryless,System.Boolean)
extern void CommandBuffer_GetTemporaryRT_m416F3104D78E2FBFFAB769F692C5A4DED3A334E9 (void);
// 0x000008F3 System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32,System.Boolean,UnityEngine.RenderTextureMemoryless)
extern void CommandBuffer_GetTemporaryRT_mED03DAECE8A01E78B4BAE23124A039240BF39EAA (void);
// 0x000008F4 System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32,System.Boolean)
extern void CommandBuffer_GetTemporaryRT_m1E03E47E671E9F017022CEDFB61B0C658E712FA7 (void);
// 0x000008F5 System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite,System.Int32)
extern void CommandBuffer_GetTemporaryRT_m2EE88CD577B803F6A5491DC52E604C5FDFF8CDBB (void);
// 0x000008F6 System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern void CommandBuffer_GetTemporaryRT_m010AFBFF063C2ADC5A27A3642E55B9000EB1B7A4 (void);
// 0x000008F7 System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode,UnityEngine.RenderTextureFormat)
extern void CommandBuffer_GetTemporaryRT_mD8089EAEA2133AA0D545CB525BC56C5762BD3BBF (void);
// 0x000008F8 System.Void UnityEngine.Rendering.CommandBuffer::GetTemporaryRT(System.Int32,System.Int32,System.Int32,System.Int32,UnityEngine.FilterMode)
extern void CommandBuffer_GetTemporaryRT_m57A18FCE25917C5F08FFCEC375E7D4D4AC78B389 (void);
// 0x000008F9 System.Void UnityEngine.Rendering.CommandBuffer::ReleaseTemporaryRT(System.Int32)
extern void CommandBuffer_ReleaseTemporaryRT_mE4A4A8DE6B315956BEAB66915FF720F1C61BCDC5 (void);
// 0x000008FA System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalFloat(System.Int32,System.Single)
extern void CommandBuffer_SetGlobalFloat_mE7B8587FABBF52E7DC1981E9676BBBD4D1E838BF (void);
// 0x000008FB System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalVector(System.Int32,UnityEngine.Vector4)
extern void CommandBuffer_SetGlobalVector_m26AD5F4AF0ABDD9F7DA61583606824526A58ABEF (void);
// 0x000008FC System.Boolean UnityEngine.Rendering.CommandBuffer::ValidateAgainstExecutionFlags(UnityEngine.Rendering.CommandBufferExecutionFlags,UnityEngine.Rendering.CommandBufferExecutionFlags)
extern void CommandBuffer_ValidateAgainstExecutionFlags_mF5464766DDBFD0B426A86EF3A828C2BDFA12272A (void);
// 0x000008FD System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalTexture_Impl(System.Int32,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderTextureSubElement)
extern void CommandBuffer_SetGlobalTexture_Impl_m58E72BA44C8BBB68770387C6AE87F9B06144B4B2 (void);
// 0x000008FE System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTarget(UnityEngine.Rendering.RenderTargetIdentifier[],UnityEngine.Rendering.RenderTargetIdentifier)
extern void CommandBuffer_SetRenderTarget_m4F4A1974C1BC9F3849D54B5141EF296F37B4E7E3 (void);
// 0x000008FF System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTargetMulti_Internal(UnityEngine.Rendering.RenderTargetIdentifier[],UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.RenderBufferLoadAction[],UnityEngine.Rendering.RenderBufferStoreAction[],UnityEngine.Rendering.RenderBufferLoadAction,UnityEngine.Rendering.RenderBufferStoreAction)
extern void CommandBuffer_SetRenderTargetMulti_Internal_mD0A3E932DB98F8071C378D721B5B50A1DE97B124 (void);
// 0x00000900 System.Void UnityEngine.Rendering.CommandBuffer::Finalize()
extern void CommandBuffer_Finalize_m3D39159AB1026981EF8E349FD87BA6FD78B8248C (void);
// 0x00000901 System.Void UnityEngine.Rendering.CommandBuffer::Dispose()
extern void CommandBuffer_Dispose_m594BAF51BD0154E0CBAA12D1537D36CCF82EA0E2 (void);
// 0x00000902 System.Void UnityEngine.Rendering.CommandBuffer::Dispose(System.Boolean)
extern void CommandBuffer_Dispose_m12BD69C6CA4A9D4543CBEE4A5B11348634F6E8DA (void);
// 0x00000903 System.Void UnityEngine.Rendering.CommandBuffer::.ctor()
extern void CommandBuffer__ctor_mB7E1174EB3B4E2E53BCC6532840AB55ECE6D06CF (void);
// 0x00000904 System.Void UnityEngine.Rendering.CommandBuffer::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
extern void CommandBuffer_DrawMesh_m8AE72FB8773642F0293697038A3262E84E269AB0 (void);
// 0x00000905 System.Void UnityEngine.Rendering.CommandBuffer::DrawMesh(UnityEngine.Mesh,UnityEngine.Matrix4x4,UnityEngine.Material,System.Int32,System.Int32)
extern void CommandBuffer_DrawMesh_m1045759BE3456DC0CE14E6E372E13FF6625D1A83 (void);
// 0x00000906 System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material,System.Int32)
extern void CommandBuffer_Blit_m57FADAE57CE78D6E45D78A212905332B70560A2B (void);
// 0x00000907 System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.RenderTargetIdentifier)
extern void CommandBuffer_Blit_mDDA4CCEA9460315B9CEA882E52B7734F4033C362 (void);
// 0x00000908 System.Void UnityEngine.Rendering.CommandBuffer::Blit(UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Rendering.RenderTargetIdentifier,UnityEngine.Material,System.Int32)
extern void CommandBuffer_Blit_m50AB7CE0D68B2B2DED83FFE9331504382FA2AB9A (void);
// 0x00000909 System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalTexture(System.Int32,UnityEngine.Rendering.RenderTargetIdentifier)
extern void CommandBuffer_SetGlobalTexture_mED69F96D6790798BFE82C22AFF2A065E5EDA41CF (void);
// 0x0000090A System.Void UnityEngine.Rendering.CommandBuffer::Internal_DrawMesh_Injected(UnityEngine.Mesh,UnityEngine.Matrix4x4&,UnityEngine.Material,System.Int32,System.Int32,UnityEngine.MaterialPropertyBlock)
extern void CommandBuffer_Internal_DrawMesh_Injected_mE52B829DE1FA132BC528038FBF1137423B287FAB (void);
// 0x0000090B System.Void UnityEngine.Rendering.CommandBuffer::Blit_Texture_Injected(UnityEngine.Texture,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Int32)
extern void CommandBuffer_Blit_Texture_Injected_m883660541CCF527552E2FD55686494441CCD1CE6 (void);
// 0x0000090C System.Void UnityEngine.Rendering.CommandBuffer::Blit_Identifier_Injected(UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Material,System.Int32,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Int32)
extern void CommandBuffer_Blit_Identifier_Injected_m8AE03F3A3DB74691420314F637DF9E5C98937B35 (void);
// 0x0000090D System.Void UnityEngine.Rendering.CommandBuffer::SetGlobalVector_Injected(System.Int32,UnityEngine.Vector4&)
extern void CommandBuffer_SetGlobalVector_Injected_m803DA6619D95F23EB054BEBE1E53C3D8C6D13767 (void);
// 0x0000090E System.Void UnityEngine.Rendering.CommandBuffer::SetRenderTargetMulti_Internal_Injected(UnityEngine.Rendering.RenderTargetIdentifier[],UnityEngine.Rendering.RenderTargetIdentifier&,UnityEngine.Rendering.RenderBufferLoadAction[],UnityEngine.Rendering.RenderBufferStoreAction[],UnityEngine.Rendering.RenderBufferLoadAction,UnityEngine.Rendering.RenderBufferStoreAction)
extern void CommandBuffer_SetRenderTargetMulti_Internal_Injected_m6FAD10C4CFF47AD7C5CE8701DA70038320982D98 (void);
// 0x0000090F System.Void UnityEngine.Rendering.BatchCullingContext::.ctor(Unity.Collections.NativeArray`1<UnityEngine.Plane>,Unity.Collections.NativeArray`1<UnityEngine.Rendering.BatchVisibility>,Unity.Collections.NativeArray`1<System.Int32>,UnityEngine.Rendering.LODParameters)
extern void BatchCullingContext__ctor_m9257DEBD02B88BE06D6ED9D3E9BF686100081F7F_AdjustorThunk (void);
// 0x00000910 System.Void UnityEngine.Rendering.BatchRendererGroup::InvokeOnPerformCulling(UnityEngine.Rendering.BatchRendererGroup,UnityEngine.Rendering.BatchRendererCullingOutput&,UnityEngine.Rendering.LODParameters&)
extern void BatchRendererGroup_InvokeOnPerformCulling_m164A637514875EBBFF4A12C55521BEA93474739C (void);
// 0x00000911 System.Void UnityEngine.Rendering.BatchRendererGroup_OnPerformCulling::.ctor(System.Object,System.IntPtr)
extern void OnPerformCulling__ctor_m1862F8A67E8CA14A19B80DE61BBC1881DF945E9F (void);
// 0x00000912 Unity.Jobs.JobHandle UnityEngine.Rendering.BatchRendererGroup_OnPerformCulling::Invoke(UnityEngine.Rendering.BatchRendererGroup,UnityEngine.Rendering.BatchCullingContext)
extern void OnPerformCulling_Invoke_m804E8422831E63707E5582FBBBFEF08E8A100525 (void);
// 0x00000913 System.IAsyncResult UnityEngine.Rendering.BatchRendererGroup_OnPerformCulling::BeginInvoke(UnityEngine.Rendering.BatchRendererGroup,UnityEngine.Rendering.BatchCullingContext,System.AsyncCallback,System.Object)
extern void OnPerformCulling_BeginInvoke_mAF6E29B6BAC52D4DCAD0C87B8C6939B94566EAA1 (void);
// 0x00000914 Unity.Jobs.JobHandle UnityEngine.Rendering.BatchRendererGroup_OnPerformCulling::EndInvoke(System.IAsyncResult)
extern void OnPerformCulling_EndInvoke_m643216ACF662C78BD6F98E09FA5AA45F92E2F9F0 (void);
// 0x00000915 System.Boolean UnityEngine.Rendering.LODParameters::Equals(UnityEngine.Rendering.LODParameters)
extern void LODParameters_Equals_mF803671C1F46ECEEE0B376EBF3AAF69D1236B4C0_AdjustorThunk (void);
// 0x00000916 System.Boolean UnityEngine.Rendering.LODParameters::Equals(System.Object)
extern void LODParameters_Equals_m8F8B356BCB62FAEAE0EFD8C51FFF9C5045A7DB5C_AdjustorThunk (void);
// 0x00000917 System.Int32 UnityEngine.Rendering.LODParameters::GetHashCode()
extern void LODParameters_GetHashCode_m5310697EE3BF4943F7358EF0EA2E7B3A9D7C1BAD_AdjustorThunk (void);
// 0x00000918 System.Void UnityEngine.Rendering.RenderPipeline::Render(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera[])
// 0x00000919 System.Void UnityEngine.Rendering.RenderPipeline::InternalRender(UnityEngine.Rendering.ScriptableRenderContext,UnityEngine.Camera[])
extern void RenderPipeline_InternalRender_mD6C2FEDA607A430F963066B487C02F4D2379FBDA (void);
// 0x0000091A System.Boolean UnityEngine.Rendering.RenderPipeline::get_disposed()
extern void RenderPipeline_get_disposed_m67EE9900399CE2E9783C5C69BFD1FF08DF521C34 (void);
// 0x0000091B System.Void UnityEngine.Rendering.RenderPipeline::set_disposed(System.Boolean)
extern void RenderPipeline_set_disposed_mB375F2860E0C96CA5E7124154610CB67CE3F80CA (void);
// 0x0000091C System.Void UnityEngine.Rendering.RenderPipeline::Dispose()
extern void RenderPipeline_Dispose_mE06FE618AE8AE1B563CAC05585ACBA8832849A7F (void);
// 0x0000091D System.Void UnityEngine.Rendering.RenderPipeline::Dispose(System.Boolean)
extern void RenderPipeline_Dispose_mC61059BB1A9F1CE9DB36C49FD01A1EB5BF4CFFE8 (void);
// 0x0000091E UnityEngine.Rendering.RenderPipeline UnityEngine.Rendering.RenderPipelineAsset::InternalCreatePipeline()
extern void RenderPipelineAsset_InternalCreatePipeline_mAE0E11E7E8D2D501F7F0F06D37DB484D37533406 (void);
// 0x0000091F System.String[] UnityEngine.Rendering.RenderPipelineAsset::get_renderingLayerMaskNames()
extern void RenderPipelineAsset_get_renderingLayerMaskNames_m099B8C66F13086B843E997A91D5F29DFA640BD5B (void);
// 0x00000920 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultMaterial()
extern void RenderPipelineAsset_get_defaultMaterial_mC04A65F5C07B7B186B734ACBDF5DA55DAFC88A4D (void);
// 0x00000921 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_autodeskInteractiveShader()
extern void RenderPipelineAsset_get_autodeskInteractiveShader_mA096E3713307598F4F0714348A137E3A0F450EAF (void);
// 0x00000922 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_autodeskInteractiveTransparentShader()
extern void RenderPipelineAsset_get_autodeskInteractiveTransparentShader_m09C4E71DE3D7BD8CDE7BB60CE379DAB150F309FF (void);
// 0x00000923 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_autodeskInteractiveMaskedShader()
extern void RenderPipelineAsset_get_autodeskInteractiveMaskedShader_mD5AB54521766DF6A0FF42971D598761BCF7BC74A (void);
// 0x00000924 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_terrainDetailLitShader()
extern void RenderPipelineAsset_get_terrainDetailLitShader_mB60D130908834F5E6585578DDEA4E175DECCA654 (void);
// 0x00000925 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_terrainDetailGrassShader()
extern void RenderPipelineAsset_get_terrainDetailGrassShader_m3F41ABB79E3672A99A77594D7516855906BB71F8 (void);
// 0x00000926 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_terrainDetailGrassBillboardShader()
extern void RenderPipelineAsset_get_terrainDetailGrassBillboardShader_mED61CD4E3CF38C120FBC20866F99C17C5FE35FBB (void);
// 0x00000927 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultParticleMaterial()
extern void RenderPipelineAsset_get_defaultParticleMaterial_mA67643D5E509468BC5C20EABAD895B9920636961 (void);
// 0x00000928 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultLineMaterial()
extern void RenderPipelineAsset_get_defaultLineMaterial_mA76E800387087F9C4FB510E1C9526D43D7A430F3 (void);
// 0x00000929 UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultTerrainMaterial()
extern void RenderPipelineAsset_get_defaultTerrainMaterial_m29231CDF74C3D74809F5C990F6881E17759CE298 (void);
// 0x0000092A UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultUIMaterial()
extern void RenderPipelineAsset_get_defaultUIMaterial_m73C804E2798E17A2452687AB46A5570DD813AEC7 (void);
// 0x0000092B UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultUIOverdrawMaterial()
extern void RenderPipelineAsset_get_defaultUIOverdrawMaterial_m75075F78B2FFC61DB9D05B09340D97B588FE6EB6 (void);
// 0x0000092C UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_defaultUIETC1SupportedMaterial()
extern void RenderPipelineAsset_get_defaultUIETC1SupportedMaterial_m530DC5B2F50F075DAB4CFA35A693293C8C98CA33 (void);
// 0x0000092D UnityEngine.Material UnityEngine.Rendering.RenderPipelineAsset::get_default2DMaterial()
extern void RenderPipelineAsset_get_default2DMaterial_mFC4CF7D4F8341D140CC0AB0B471B154AD580C4F0 (void);
// 0x0000092E UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_defaultShader()
extern void RenderPipelineAsset_get_defaultShader_mE3420265AB2F3629C6C86E88CC7FDB7744B177C1 (void);
// 0x0000092F UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_defaultSpeedTree7Shader()
extern void RenderPipelineAsset_get_defaultSpeedTree7Shader_m14E423504D20BC7D296EDD97045790DEA68DBE14 (void);
// 0x00000930 UnityEngine.Shader UnityEngine.Rendering.RenderPipelineAsset::get_defaultSpeedTree8Shader()
extern void RenderPipelineAsset_get_defaultSpeedTree8Shader_mDAB93B789B21F1D58A6FE11858F8C18527ABD50B (void);
// 0x00000931 UnityEngine.Rendering.RenderPipeline UnityEngine.Rendering.RenderPipelineAsset::CreatePipeline()
// 0x00000932 System.Void UnityEngine.Rendering.RenderPipelineAsset::OnValidate()
extern void RenderPipelineAsset_OnValidate_mEFE93B16F8AA5C809D95536A557FA9E29FF7B1DB (void);
// 0x00000933 System.Void UnityEngine.Rendering.RenderPipelineAsset::OnDisable()
extern void RenderPipelineAsset_OnDisable_m98A18DF5D40DBB36EF0A33C637CFA41A71FC03C1 (void);
// 0x00000934 System.Void UnityEngine.Rendering.RenderPipelineAsset::.ctor()
extern void RenderPipelineAsset__ctor_mC4E1451327751FBCB6F05982262D3B7ED8538DF4 (void);
// 0x00000935 UnityEngine.Rendering.RenderPipeline UnityEngine.Rendering.RenderPipelineManager::get_currentPipeline()
extern void RenderPipelineManager_get_currentPipeline_m096347F0BF45316A41A84B9344DB6B29F4D48611 (void);
// 0x00000936 System.Void UnityEngine.Rendering.RenderPipelineManager::set_currentPipeline(UnityEngine.Rendering.RenderPipeline)
extern void RenderPipelineManager_set_currentPipeline_m03D0E14C590848C8D5ABC2C22C026935119CE526 (void);
// 0x00000937 System.Void UnityEngine.Rendering.RenderPipelineManager::CleanupRenderPipeline()
extern void RenderPipelineManager_CleanupRenderPipeline_m85443E0BE0778DFA602405C8047DCAE5DCF6D54E (void);
// 0x00000938 System.Void UnityEngine.Rendering.RenderPipelineManager::GetCameras(UnityEngine.Rendering.ScriptableRenderContext)
extern void RenderPipelineManager_GetCameras_m8BB39469D10975B096634537AC44FB7CF6D52938 (void);
// 0x00000939 System.Void UnityEngine.Rendering.RenderPipelineManager::DoRenderLoop_Internal(UnityEngine.Rendering.RenderPipelineAsset,System.IntPtr)
extern void RenderPipelineManager_DoRenderLoop_Internal_mB2DEE6063284E87AF7296E76461A3CF84F637E8B (void);
// 0x0000093A System.Void UnityEngine.Rendering.RenderPipelineManager::PrepareRenderPipeline(UnityEngine.Rendering.RenderPipelineAsset)
extern void RenderPipelineManager_PrepareRenderPipeline_m4C5F3624BD68AFCAAD4A9D800ED906B9DF4DFB55 (void);
// 0x0000093B System.Void UnityEngine.Rendering.RenderPipelineManager::.cctor()
extern void RenderPipelineManager__cctor_mAF92ABD8F7586C49F9E8AAE720EBE21A2700CF6D (void);
// 0x0000093C System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetNumberOfCameras_Internal()
extern void ScriptableRenderContext_GetNumberOfCameras_Internal_mDD1E260788000AB57C94A2D3F8F02A2B91DA653F_AdjustorThunk (void);
// 0x0000093D UnityEngine.Camera UnityEngine.Rendering.ScriptableRenderContext::GetCamera_Internal(System.Int32)
extern void ScriptableRenderContext_GetCamera_Internal_m20F51E42E84B8E2BBF2CC72F8C8EBFDCE6737646_AdjustorThunk (void);
// 0x0000093E System.Void UnityEngine.Rendering.ScriptableRenderContext::.ctor(System.IntPtr)
extern void ScriptableRenderContext__ctor_mEA592FA995EF36C1F8F05EF2E51BC1089D7371CA_AdjustorThunk (void);
// 0x0000093F System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetNumberOfCameras()
extern void ScriptableRenderContext_GetNumberOfCameras_m43FCE097543E85E40FCA641C570A25E718E3D6F6_AdjustorThunk (void);
// 0x00000940 UnityEngine.Camera UnityEngine.Rendering.ScriptableRenderContext::GetCamera(System.Int32)
extern void ScriptableRenderContext_GetCamera_mF8D58C4C1BB5667F63A2DCFDB72CE4C3C7ADBBFA_AdjustorThunk (void);
// 0x00000941 System.Boolean UnityEngine.Rendering.ScriptableRenderContext::Equals(UnityEngine.Rendering.ScriptableRenderContext)
extern void ScriptableRenderContext_Equals_mDC10DFED8A46426E355FE7877624EC2D549EA7B7_AdjustorThunk (void);
// 0x00000942 System.Boolean UnityEngine.Rendering.ScriptableRenderContext::Equals(System.Object)
extern void ScriptableRenderContext_Equals_mB04CFBF55095DF6179ED2229F4C9FE907F95799D_AdjustorThunk (void);
// 0x00000943 System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetHashCode()
extern void ScriptableRenderContext_GetHashCode_mACDECBAC76686105322F409089D9A867DF4BD46D_AdjustorThunk (void);
// 0x00000944 System.Int32 UnityEngine.Rendering.ScriptableRenderContext::GetNumberOfCameras_Internal_Injected(UnityEngine.Rendering.ScriptableRenderContext&)
extern void ScriptableRenderContext_GetNumberOfCameras_Internal_Injected_m65EF6B7DE8AFA868E6D83B2D75791315D9841426 (void);
// 0x00000945 UnityEngine.Camera UnityEngine.Rendering.ScriptableRenderContext::GetCamera_Internal_Injected(UnityEngine.Rendering.ScriptableRenderContext&,System.Int32)
extern void ScriptableRenderContext_GetCamera_Internal_Injected_m17CA2AE7513419CA8FC464A270218F599D48C830 (void);
// 0x00000946 UnityEngine.Rendering.SupportedRenderingFeatures UnityEngine.Rendering.SupportedRenderingFeatures::get_active()
extern void SupportedRenderingFeatures_get_active_mE16AFBB742C413071F2DB87563826A90A93EA04F (void);
// 0x00000947 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::set_active(UnityEngine.Rendering.SupportedRenderingFeatures)
extern void SupportedRenderingFeatures_set_active_m3BC49234CD45C5EFAE64E319D5198CA159143F54 (void);
// 0x00000948 UnityEngine.Rendering.SupportedRenderingFeatures_LightmapMixedBakeModes UnityEngine.Rendering.SupportedRenderingFeatures::get_defaultMixedLightingModes()
extern void SupportedRenderingFeatures_get_defaultMixedLightingModes_m7B53835BDDAF009835F9A0907BC59E4E88C71D9C (void);
// 0x00000949 UnityEngine.Rendering.SupportedRenderingFeatures_LightmapMixedBakeModes UnityEngine.Rendering.SupportedRenderingFeatures::get_mixedLightingModes()
extern void SupportedRenderingFeatures_get_mixedLightingModes_mE4A171C47A4A685E49F2F382AA51C556B48EE58C (void);
// 0x0000094A UnityEngine.LightmapBakeType UnityEngine.Rendering.SupportedRenderingFeatures::get_lightmapBakeTypes()
extern void SupportedRenderingFeatures_get_lightmapBakeTypes_mAF3B22ACCE625D1C45F411D30C2874E8A847605E (void);
// 0x0000094B UnityEngine.LightmapsMode UnityEngine.Rendering.SupportedRenderingFeatures::get_lightmapsModes()
extern void SupportedRenderingFeatures_get_lightmapsModes_m69B5455BF172B258A0A2DB6F52846E8934AD048A (void);
// 0x0000094C System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::get_enlighten()
extern void SupportedRenderingFeatures_get_enlighten_mA4BDBEBFE0E8F1C161D7BE28B328E6D6E36720A9 (void);
// 0x0000094D System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::get_rendersUIOverlay()
extern void SupportedRenderingFeatures_get_rendersUIOverlay_m8E56255490C51999C7B14F30CD072E49E2C39B4E (void);
// 0x0000094E System.Void UnityEngine.Rendering.SupportedRenderingFeatures::FallbackMixedLightingModeByRef(System.IntPtr)
extern void SupportedRenderingFeatures_FallbackMixedLightingModeByRef_m517CBD3BBD91EEA5D20CD5979DF8841FE0DBEDAC (void);
// 0x0000094F System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::IsMixedLightingModeSupported(UnityEngine.MixedLightingMode)
extern void SupportedRenderingFeatures_IsMixedLightingModeSupported_m8AFE3C9D450B1A6E52A31EA84C1B8F626AAC0CD0 (void);
// 0x00000950 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsMixedLightingModeSupportedByRef(UnityEngine.MixedLightingMode,System.IntPtr)
extern void SupportedRenderingFeatures_IsMixedLightingModeSupportedByRef_mC13FADD4B8DCEB26F58C6F5DD9D7899A621F9828 (void);
// 0x00000951 System.Boolean UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapBakeTypeSupported(UnityEngine.LightmapBakeType)
extern void SupportedRenderingFeatures_IsLightmapBakeTypeSupported_m8BBA40E9CBAFFD8B176F3812C36DD31D9D60BBEA (void);
// 0x00000952 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapBakeTypeSupportedByRef(UnityEngine.LightmapBakeType,System.IntPtr)
extern void SupportedRenderingFeatures_IsLightmapBakeTypeSupportedByRef_mB6F953153B328DBFD1F7E444D155F8C53ABCD876 (void);
// 0x00000953 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapsModeSupportedByRef(UnityEngine.LightmapsMode,System.IntPtr)
extern void SupportedRenderingFeatures_IsLightmapsModeSupportedByRef_m3477F21B073DD73F183FAD40A8EEF53843A8A581 (void);
// 0x00000954 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsLightmapperSupportedByRef(System.Int32,System.IntPtr)
extern void SupportedRenderingFeatures_IsLightmapperSupportedByRef_mEAFB23042E154953C780501A57D605F76510BB11 (void);
// 0x00000955 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::IsUIOverlayRenderedBySRP(System.IntPtr)
extern void SupportedRenderingFeatures_IsUIOverlayRenderedBySRP_m829585DA31B1BECD1349A02B69657B1D38D2DDC3 (void);
// 0x00000956 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::FallbackLightmapperByRef(System.IntPtr)
extern void SupportedRenderingFeatures_FallbackLightmapperByRef_mD2BB8B58F329170010CB9B3BF72794755218046A (void);
// 0x00000957 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::.ctor()
extern void SupportedRenderingFeatures__ctor_m0612F2A9F55682A7255B3B276E9BAFCFC0CB4EF4 (void);
// 0x00000958 System.Void UnityEngine.Rendering.SupportedRenderingFeatures::.cctor()
extern void SupportedRenderingFeatures__cctor_mCC9E6E14A59B708435BCF2B25BE36943EC590E28 (void);
// 0x00000959 System.Void UnityEngine.Playables.INotificationReceiver::OnNotify(UnityEngine.Playables.Playable,UnityEngine.Playables.INotification,System.Object)
// 0x0000095A System.Void UnityEngine.Playables.IPlayableBehaviour::OnGraphStart(UnityEngine.Playables.Playable)
// 0x0000095B System.Void UnityEngine.Playables.IPlayableBehaviour::OnGraphStop(UnityEngine.Playables.Playable)
// 0x0000095C System.Void UnityEngine.Playables.IPlayableBehaviour::OnPlayableCreate(UnityEngine.Playables.Playable)
// 0x0000095D System.Void UnityEngine.Playables.IPlayableBehaviour::OnPlayableDestroy(UnityEngine.Playables.Playable)
// 0x0000095E System.Void UnityEngine.Playables.IPlayableBehaviour::OnBehaviourPlay(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
// 0x0000095F System.Void UnityEngine.Playables.IPlayableBehaviour::OnBehaviourPause(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
// 0x00000960 System.Void UnityEngine.Playables.IPlayableBehaviour::PrepareFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
// 0x00000961 System.Void UnityEngine.Playables.IPlayableBehaviour::ProcessFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData,System.Object)
// 0x00000962 UnityEngine.Playables.Playable UnityEngine.Playables.Playable::get_Null()
extern void Playable_get_Null_m008AFBC0B01AE584444CF8322CCCC038ED2B8B58 (void);
// 0x00000963 System.Void UnityEngine.Playables.Playable::.ctor(UnityEngine.Playables.PlayableHandle)
extern void Playable__ctor_m4B5AC727703A68C00773F99DE1C711EFC973DCA8_AdjustorThunk (void);
// 0x00000964 UnityEngine.Playables.PlayableHandle UnityEngine.Playables.Playable::GetHandle()
extern void Playable_GetHandle_mA646BB041702651694A643FDE1A835112F718351_AdjustorThunk (void);
// 0x00000965 System.Boolean UnityEngine.Playables.Playable::Equals(UnityEngine.Playables.Playable)
extern void Playable_Equals_m1BFA06EB851EEEB20705520563A2D504637CDA1C_AdjustorThunk (void);
// 0x00000966 System.Void UnityEngine.Playables.Playable::.cctor()
extern void Playable__cctor_m83EA9DB5D7F0DAE681E34D0899A2562795301888 (void);
// 0x00000967 UnityEngine.Playables.Playable UnityEngine.Playables.PlayableAsset::CreatePlayable(UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject)
// 0x00000968 System.Double UnityEngine.Playables.PlayableAsset::get_duration()
extern void PlayableAsset_get_duration_m7D57ED7F9E30E414F2981357A61C42CBF3D8367D (void);
// 0x00000969 System.Collections.Generic.IEnumerable`1<UnityEngine.Playables.PlayableBinding> UnityEngine.Playables.PlayableAsset::get_outputs()
extern void PlayableAsset_get_outputs_m5F941EB83243BB24A93C27A7583BB55FF18CB38C (void);
// 0x0000096A System.Void UnityEngine.Playables.PlayableAsset::Internal_CreatePlayable(UnityEngine.Playables.PlayableAsset,UnityEngine.Playables.PlayableGraph,UnityEngine.GameObject,System.IntPtr)
extern void PlayableAsset_Internal_CreatePlayable_m5E4984A4BF33518FBE69B43A29DB486AF6501537 (void);
// 0x0000096B System.Void UnityEngine.Playables.PlayableAsset::Internal_GetPlayableAssetDuration(UnityEngine.Playables.PlayableAsset,System.IntPtr)
extern void PlayableAsset_Internal_GetPlayableAssetDuration_mE27BD3B5B7D9E80D1C249D6430401BD81E4AFB4C (void);
// 0x0000096C System.Void UnityEngine.Playables.PlayableAsset::.ctor()
extern void PlayableAsset__ctor_mAE1FA54D280C75ADC9486656C5C36AC1917D5FE4 (void);
// 0x0000096D System.Void UnityEngine.Playables.PlayableBehaviour::.ctor()
extern void PlayableBehaviour__ctor_m1EA2FC6B8DE3503745344E7BBAA0B40FDCD50FC8 (void);
// 0x0000096E System.Void UnityEngine.Playables.PlayableBehaviour::OnGraphStart(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnGraphStart_mB302433EB8460D88FD2FD035D2BF9BF82BD82AA6 (void);
// 0x0000096F System.Void UnityEngine.Playables.PlayableBehaviour::OnGraphStop(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnGraphStop_m24F852A18B98173C2CBF1B96C0477A1F77BB9CFE (void);
// 0x00000970 System.Void UnityEngine.Playables.PlayableBehaviour::OnPlayableCreate(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnPlayableCreate_m62F834EDF37B809612E47CCC9C4848435A89CD12 (void);
// 0x00000971 System.Void UnityEngine.Playables.PlayableBehaviour::OnPlayableDestroy(UnityEngine.Playables.Playable)
extern void PlayableBehaviour_OnPlayableDestroy_m9F6E1A185DC7C98C312D9F3A0A7308DE3D32F745 (void);
// 0x00000972 System.Void UnityEngine.Playables.PlayableBehaviour::OnBehaviourPlay(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern void PlayableBehaviour_OnBehaviourPlay_m71A792D97CD840F5D6C73889D00C2887F41A0F41 (void);
// 0x00000973 System.Void UnityEngine.Playables.PlayableBehaviour::OnBehaviourPause(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern void PlayableBehaviour_OnBehaviourPause_mCC4EAD0766538FE39656D3EE04268239BB091AD2 (void);
// 0x00000974 System.Void UnityEngine.Playables.PlayableBehaviour::PrepareFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData)
extern void PlayableBehaviour_PrepareFrame_m45F96C7EDDA4ABF4C2271DB16163FBD873AA1432 (void);
// 0x00000975 System.Void UnityEngine.Playables.PlayableBehaviour::ProcessFrame(UnityEngine.Playables.Playable,UnityEngine.Playables.FrameData,System.Object)
extern void PlayableBehaviour_ProcessFrame_m5EB22A817FFF0662E0E3AFAB34C41D7B09D4326F (void);
// 0x00000976 System.Object UnityEngine.Playables.PlayableBehaviour::Clone()
extern void PlayableBehaviour_Clone_m3265FD7A4EA58C9A607F0F28EFF108EBBD826C3A (void);
// 0x00000977 System.Void UnityEngine.Playables.PlayableBinding::.cctor()
extern void PlayableBinding__cctor_m75475729474FE236198B967978A11DBA38CC6E47 (void);
// 0x00000978 System.Void UnityEngine.Playables.PlayableBinding_CreateOutputMethod::.ctor(System.Object,System.IntPtr)
extern void CreateOutputMethod__ctor_m944A1B790F35F52E108EF2CC13BA02BD8DE62EB3 (void);
// 0x00000979 UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableBinding_CreateOutputMethod::Invoke(UnityEngine.Playables.PlayableGraph,System.String)
extern void CreateOutputMethod_Invoke_mFD551E15D9A6FD8B2C70A2CC96AAD0D2D9D3D109 (void);
// 0x0000097A System.IAsyncResult UnityEngine.Playables.PlayableBinding_CreateOutputMethod::BeginInvoke(UnityEngine.Playables.PlayableGraph,System.String,System.AsyncCallback,System.Object)
extern void CreateOutputMethod_BeginInvoke_m082AE47F9DFBF0C1787081D2D628E0E5CECCBFF1 (void);
// 0x0000097B UnityEngine.Playables.PlayableOutput UnityEngine.Playables.PlayableBinding_CreateOutputMethod::EndInvoke(System.IAsyncResult)
extern void CreateOutputMethod_EndInvoke_mAA7AE5BF9E6A38C081A972827A16497ACA9FA9CE (void);
// 0x0000097C System.Boolean UnityEngine.Playables.PlayableHandle::IsPlayableOfType()
// 0x0000097D UnityEngine.Playables.PlayableHandle UnityEngine.Playables.PlayableHandle::get_Null()
extern void PlayableHandle_get_Null_mD1C6FC2D7F6A7A23955ACDD87BE934B75463E612 (void);
// 0x0000097E System.Boolean UnityEngine.Playables.PlayableHandle::op_Equality(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern void PlayableHandle_op_Equality_mFD26CFA8ECF2B622B1A3D4117066CAE965C9F704 (void);
// 0x0000097F System.Boolean UnityEngine.Playables.PlayableHandle::Equals(System.Object)
extern void PlayableHandle_Equals_mEF30690ECF60C980C207198C86E401CB4DA5EF3F_AdjustorThunk (void);
// 0x00000980 System.Boolean UnityEngine.Playables.PlayableHandle::Equals(UnityEngine.Playables.PlayableHandle)
extern void PlayableHandle_Equals_mB50663A8BC01BCED4A650EFADE88DCF47309E955_AdjustorThunk (void);
// 0x00000981 System.Int32 UnityEngine.Playables.PlayableHandle::GetHashCode()
extern void PlayableHandle_GetHashCode_m26AD05C2D6209A017CA6A079F026BC8D28600D72_AdjustorThunk (void);
// 0x00000982 System.Boolean UnityEngine.Playables.PlayableHandle::CompareVersion(UnityEngine.Playables.PlayableHandle,UnityEngine.Playables.PlayableHandle)
extern void PlayableHandle_CompareVersion_m7B2FDE7E81BCBB16D3E2667AEDAA879FA75EA1AA (void);
// 0x00000983 System.Boolean UnityEngine.Playables.PlayableHandle::IsValid()
extern void PlayableHandle_IsValid_m237A5E7818768641BAC928BD08EC0AB439E3DAF6_AdjustorThunk (void);
// 0x00000984 System.Type UnityEngine.Playables.PlayableHandle::GetPlayableType()
extern void PlayableHandle_GetPlayableType_m5E523E4DA00DF2FAB2E209B0B43FD5AA430AE84F_AdjustorThunk (void);
// 0x00000985 System.Void UnityEngine.Playables.PlayableHandle::.cctor()
extern void PlayableHandle__cctor_m14F0FDD932E8AB558039FFCD57C2E319AA25C989 (void);
// 0x00000986 System.Boolean UnityEngine.Playables.PlayableHandle::IsValid_Injected(UnityEngine.Playables.PlayableHandle&)
extern void PlayableHandle_IsValid_Injected_m177B41D5EF570AEA24146EE35628EFF2DE256ADF (void);
// 0x00000987 System.Type UnityEngine.Playables.PlayableHandle::GetPlayableType_Injected(UnityEngine.Playables.PlayableHandle&)
extern void PlayableHandle_GetPlayableType_Injected_m05EDC4CC2DC82043F97BC6BC97EA38B7AD537B9A (void);
// 0x00000988 System.Void UnityEngine.Playables.PlayableOutput::.ctor(UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutput__ctor_m69B5F29DF2D785051B714CFEAAAF09A237978297_AdjustorThunk (void);
// 0x00000989 UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutput::GetHandle()
extern void PlayableOutput_GetHandle_mC56A4F912A7AC8640DE78F95DD6C4F346E36820E_AdjustorThunk (void);
// 0x0000098A System.Boolean UnityEngine.Playables.PlayableOutput::Equals(UnityEngine.Playables.PlayableOutput)
extern void PlayableOutput_Equals_m5A66A7B50C52A8524BB807F439345D22E96FD5CE_AdjustorThunk (void);
// 0x0000098B System.Void UnityEngine.Playables.PlayableOutput::.cctor()
extern void PlayableOutput__cctor_m4F7AC7CBF2D9DD26FF45B6D521C4051CDC1A17D3 (void);
// 0x0000098C UnityEngine.Playables.PlayableOutputHandle UnityEngine.Playables.PlayableOutputHandle::get_Null()
extern void PlayableOutputHandle_get_Null_m33F7D36A76BFDC0B58633BF931E55FA5BBFFD93D (void);
// 0x0000098D System.Int32 UnityEngine.Playables.PlayableOutputHandle::GetHashCode()
extern void PlayableOutputHandle_GetHashCode_mB4BD207CB03FA499179FBEAF64CC66A91F49E59C_AdjustorThunk (void);
// 0x0000098E System.Boolean UnityEngine.Playables.PlayableOutputHandle::op_Equality(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutputHandle_op_Equality_mD24E3FB556D12A8D2B6EBDB6953F667B963F5DA9 (void);
// 0x0000098F System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(System.Object)
extern void PlayableOutputHandle_Equals_m06CE237BED975C0A3E9B19D071767EF6E7A3F83C_AdjustorThunk (void);
// 0x00000990 System.Boolean UnityEngine.Playables.PlayableOutputHandle::Equals(UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutputHandle_Equals_mA8CE0F7B36B440F067705FA90DC4A836E92542E0_AdjustorThunk (void);
// 0x00000991 System.Boolean UnityEngine.Playables.PlayableOutputHandle::CompareVersion(UnityEngine.Playables.PlayableOutputHandle,UnityEngine.Playables.PlayableOutputHandle)
extern void PlayableOutputHandle_CompareVersion_mB96AE424417CD9E5610906A1508B299679814D8E (void);
// 0x00000992 System.Void UnityEngine.Playables.PlayableOutputHandle::.cctor()
extern void PlayableOutputHandle__cctor_mFC855E625CBD628602F990B3A15AF0DB8C7E3AC1 (void);
// 0x00000993 System.Single UnityEngine.Experimental.GlobalIllumination.LinearColor::get_red()
extern void LinearColor_get_red_mA08BA9496EAFF59F4E1EAD61FDB5F57B0B0CC541_AdjustorThunk (void);
// 0x00000994 System.Void UnityEngine.Experimental.GlobalIllumination.LinearColor::set_red(System.Single)
extern void LinearColor_set_red_mC0D9E4D96C36785145EAF7B0DBA90359EE193928_AdjustorThunk (void);
// 0x00000995 System.Single UnityEngine.Experimental.GlobalIllumination.LinearColor::get_green()
extern void LinearColor_get_green_m03F2EEBC25E91E1102A2D3252725B2BEDA7D7931_AdjustorThunk (void);
// 0x00000996 System.Void UnityEngine.Experimental.GlobalIllumination.LinearColor::set_green(System.Single)
extern void LinearColor_set_green_m6E196AC12B067ED8AEF3B7C7E9DA1B80B9550B89_AdjustorThunk (void);
// 0x00000997 System.Single UnityEngine.Experimental.GlobalIllumination.LinearColor::get_blue()
extern void LinearColor_get_blue_m0B87E7A2A5A5B6A6F5FD515890F6C3C528FC98FE_AdjustorThunk (void);
// 0x00000998 System.Void UnityEngine.Experimental.GlobalIllumination.LinearColor::set_blue(System.Single)
extern void LinearColor_set_blue_mB65DC7FD20C551501BC181C457D010DCEECDEE7F_AdjustorThunk (void);
// 0x00000999 UnityEngine.Experimental.GlobalIllumination.LinearColor UnityEngine.Experimental.GlobalIllumination.LinearColor::Convert(UnityEngine.Color,System.Single)
extern void LinearColor_Convert_m7C35C63BFFDD93EBCC6E8050567B79562B82823A (void);
// 0x0000099A UnityEngine.Experimental.GlobalIllumination.LinearColor UnityEngine.Experimental.GlobalIllumination.LinearColor::Black()
extern void LinearColor_Black_m50DB4626285C618DFD9F561573AA77F84AE7E180 (void);
// 0x0000099B System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.DirectionalLight&,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightDataGI_Init_m135DF5CF6CBECA4CBA0AC71F9CDEEF8DE25606DB_AdjustorThunk (void);
// 0x0000099C System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.PointLight&,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightDataGI_Init_m4E8BEBD383D5F6F1A1EDFEC763A718A7271C22A6_AdjustorThunk (void);
// 0x0000099D System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.SpotLight&,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightDataGI_Init_mC9948FAC4A191C99E3E7D94677B7CFD241857C86_AdjustorThunk (void);
// 0x0000099E System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.RectangleLight&,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightDataGI_Init_mA0DF9747C6AD308EAAF2A9530B4225A3D65BB092_AdjustorThunk (void);
// 0x0000099F System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::Init(UnityEngine.Experimental.GlobalIllumination.DiscLight&,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightDataGI_Init_mB96C3F3E00F10DD0806BD3DB93B58C2299D59E94_AdjustorThunk (void);
// 0x000009A0 System.Void UnityEngine.Experimental.GlobalIllumination.LightDataGI::InitNoBake(System.Int32)
extern void LightDataGI_InitNoBake_mF600D612DE9A1CE4355C61317F6173E1AAEFBD57_AdjustorThunk (void);
// 0x000009A1 UnityEngine.Experimental.GlobalIllumination.LightMode UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.LightmapBakeType)
extern void LightmapperUtils_Extract_m32B54C9DC94AE03162E3896C5FA00FE9678F9081 (void);
// 0x000009A2 UnityEngine.Experimental.GlobalIllumination.LinearColor UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::ExtractIndirect(UnityEngine.Light)
extern void LightmapperUtils_ExtractIndirect_mC17A833A46BAAA01B55F8BA8A5821292AB104F54 (void);
// 0x000009A3 System.Single UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::ExtractInnerCone(UnityEngine.Light)
extern void LightmapperUtils_ExtractInnerCone_mEF618AE5A5D8EB690F3BA162196859FE6F662947 (void);
// 0x000009A4 UnityEngine.Color UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::ExtractColorTemperature(UnityEngine.Light)
extern void LightmapperUtils_ExtractColorTemperature_m5052DE4DC7630A7077EA2B8C6AA903257C95AFA5 (void);
// 0x000009A5 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::ApplyColorTemperature(UnityEngine.Color,UnityEngine.Experimental.GlobalIllumination.LinearColor&)
extern void LightmapperUtils_ApplyColorTemperature_mA49FB616EB2F9F31AF4CCB4C964592005DCEA346 (void);
// 0x000009A6 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.DirectionalLight&)
extern void LightmapperUtils_Extract_mCBEC26CC920C0D87DF6E25417533923E26CB6DFC (void);
// 0x000009A7 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.PointLight&)
extern void LightmapperUtils_Extract_mB11D8F3B35F96E176A89517A25CD1A54DCBD7D6E (void);
// 0x000009A8 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.SpotLight&)
extern void LightmapperUtils_Extract_mB1572C38F682F043180745B7A231FBE07CD205E4 (void);
// 0x000009A9 System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.RectangleLight&)
extern void LightmapperUtils_Extract_mEABF77895D51E1CA5FD380682539C3DD3FC8A91C (void);
// 0x000009AA System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.DiscLight&)
extern void LightmapperUtils_Extract_m93B350DDA0CB5B624054835BAF46C177472E9212 (void);
// 0x000009AB System.Void UnityEngine.Experimental.GlobalIllumination.LightmapperUtils::Extract(UnityEngine.Light,UnityEngine.Experimental.GlobalIllumination.Cookie&)
extern void LightmapperUtils_Extract_mACAC5E823E243A53EDD2A01CF857DD16C3FDBE2A (void);
// 0x000009AC System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::SetDelegate(UnityEngine.Experimental.GlobalIllumination.Lightmapping_RequestLightsDelegate)
extern void Lightmapping_SetDelegate_m3C7B041BEEBD50C1EF3C0D9D5BC2162E93E19979 (void);
// 0x000009AD UnityEngine.Experimental.GlobalIllumination.Lightmapping_RequestLightsDelegate UnityEngine.Experimental.GlobalIllumination.Lightmapping::GetDelegate()
extern void Lightmapping_GetDelegate_mD44EBB65CFD4038D77119A3F896B55905DF9A9DC (void);
// 0x000009AE System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::ResetDelegate()
extern void Lightmapping_ResetDelegate_m6EF8586283713477679876577D753EFDCA74A6F3 (void);
// 0x000009AF System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::RequestLights(UnityEngine.Light[],System.IntPtr,System.Int32)
extern void Lightmapping_RequestLights_m40C73984B1F2DB34C58965D1C4D321181C7E5976 (void);
// 0x000009B0 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping::.cctor()
extern void Lightmapping__cctor_m94640A0363C80E0E1438E4EE650AED85AB3E576C (void);
// 0x000009B1 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping_RequestLightsDelegate::.ctor(System.Object,System.IntPtr)
extern void RequestLightsDelegate__ctor_m47823FBD9D2EE4ABA5EE8D889BBBC0783FB10A42 (void);
// 0x000009B2 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping_RequestLightsDelegate::Invoke(UnityEngine.Light[],Unity.Collections.NativeArray`1<UnityEngine.Experimental.GlobalIllumination.LightDataGI>)
extern void RequestLightsDelegate_Invoke_m8BC0D55744A3FA2E75444AC72B3D3E4FB858BECB (void);
// 0x000009B3 System.IAsyncResult UnityEngine.Experimental.GlobalIllumination.Lightmapping_RequestLightsDelegate::BeginInvoke(UnityEngine.Light[],Unity.Collections.NativeArray`1<UnityEngine.Experimental.GlobalIllumination.LightDataGI>,System.AsyncCallback,System.Object)
extern void RequestLightsDelegate_BeginInvoke_m75482E3D4E28205DCB4A202F5C144CB7C95622A6 (void);
// 0x000009B4 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping_RequestLightsDelegate::EndInvoke(System.IAsyncResult)
extern void RequestLightsDelegate_EndInvoke_mA1FF787B6F3182ACF6157D13F9A84AFCBB66EE60 (void);
// 0x000009B5 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping_<>c::.cctor()
extern void U3CU3Ec__cctor_m2A00D547FF8F6F8A03666B4737BB9A0620719987 (void);
// 0x000009B6 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping_<>c::.ctor()
extern void U3CU3Ec__ctor_m8F825BEC75A119B6CDDA0985A3F7BA3DEA8AD5B2 (void);
// 0x000009B7 System.Void UnityEngine.Experimental.GlobalIllumination.Lightmapping_<>c::<.cctor>b__7_0(UnityEngine.Light[],Unity.Collections.NativeArray`1<UnityEngine.Experimental.GlobalIllumination.LightDataGI>)
extern void U3CU3Ec_U3C_cctorU3Eb__7_0_m84A19BB5BB12263A1E3C52F1B351E3A24BE546C7 (void);
// 0x000009B8 UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Playables.CameraPlayable::GetHandle()
extern void CameraPlayable_GetHandle_m12A0FB549E5257C9AEBCF76B6E2DC49FE5F8B7C5_AdjustorThunk (void);
// 0x000009B9 System.Boolean UnityEngine.Experimental.Playables.CameraPlayable::Equals(UnityEngine.Experimental.Playables.CameraPlayable)
extern void CameraPlayable_Equals_m82D036759943861CAB110D108F966C60216E6327_AdjustorThunk (void);
// 0x000009BA UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Playables.MaterialEffectPlayable::GetHandle()
extern void MaterialEffectPlayable_GetHandle_mC4AA4C850DFB33EBA45EDA3D0524294EC03044FC_AdjustorThunk (void);
// 0x000009BB System.Boolean UnityEngine.Experimental.Playables.MaterialEffectPlayable::Equals(UnityEngine.Experimental.Playables.MaterialEffectPlayable)
extern void MaterialEffectPlayable_Equals_mC805BF647B60EA8517040C2C0716CFCB7F04CAFB_AdjustorThunk (void);
// 0x000009BC UnityEngine.Playables.PlayableHandle UnityEngine.Experimental.Playables.TextureMixerPlayable::GetHandle()
extern void TextureMixerPlayable_GetHandle_m44A48E52180084F5A93942EA0AFA454C9DFBFD40_AdjustorThunk (void);
// 0x000009BD System.Boolean UnityEngine.Experimental.Playables.TextureMixerPlayable::Equals(UnityEngine.Experimental.Playables.TextureMixerPlayable)
extern void TextureMixerPlayable_Equals_m49E1B77DF4F13F35321494AC6B5330538D0A1980_AdjustorThunk (void);
// 0x000009BE System.Boolean UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::TickRealtimeProbes()
extern void BuiltinRuntimeReflectionSystem_TickRealtimeProbes_mDE45D3D3E07AB0FF8C10791544CB29FF6D44DFA2 (void);
// 0x000009BF System.Void UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::Dispose()
extern void BuiltinRuntimeReflectionSystem_Dispose_mFF4F5CDB37BB93A28975F7BFE970040964B48F5A (void);
// 0x000009C0 System.Void UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::Dispose(System.Boolean)
extern void BuiltinRuntimeReflectionSystem_Dispose_mFBDDD8FE2956E99DB34533F8C29621C3E938BD13 (void);
// 0x000009C1 System.Boolean UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::BuiltinUpdate()
extern void BuiltinRuntimeReflectionSystem_BuiltinUpdate_mEDD980F13F6200E5B89742D6868C4EF4858AE405 (void);
// 0x000009C2 UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::Internal_BuiltinRuntimeReflectionSystem_New()
extern void BuiltinRuntimeReflectionSystem_Internal_BuiltinRuntimeReflectionSystem_New_mA4A701BE60FC41AD01F7AC965DACA950B4C9560C (void);
// 0x000009C3 System.Void UnityEngine.Experimental.Rendering.BuiltinRuntimeReflectionSystem::.ctor()
extern void BuiltinRuntimeReflectionSystem__ctor_m418427E040351EC086975D95B409F2C6E3E41045 (void);
// 0x000009C4 System.Boolean UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem::TickRealtimeProbes()
// 0x000009C5 System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::set_Internal_ScriptableRuntimeReflectionSystemSettings_system(UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem)
extern void ScriptableRuntimeReflectionSystemSettings_set_Internal_ScriptableRuntimeReflectionSystemSettings_system_mE9EF71AD222FC661C616AC9687961D98946D8680 (void);
// 0x000009C6 UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::get_Internal_ScriptableRuntimeReflectionSystemSettings_instance()
extern void ScriptableRuntimeReflectionSystemSettings_get_Internal_ScriptableRuntimeReflectionSystemSettings_instance_mC8110BFC8188AAFC7D4EC56780617AB33CB2D71C (void);
// 0x000009C7 System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::ScriptingDirtyReflectionSystemInstance()
extern void ScriptableRuntimeReflectionSystemSettings_ScriptingDirtyReflectionSystemInstance_mE4FFB1863BE37B6E20388C15D2C48F4E01FCFEEF (void);
// 0x000009C8 System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemSettings::.cctor()
extern void ScriptableRuntimeReflectionSystemSettings__cctor_m24D01EC03C21F2E3A40CC9C0DC4A646C8690096A (void);
// 0x000009C9 UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::get_implementation()
extern void ScriptableRuntimeReflectionSystemWrapper_get_implementation_mD0D0BB589A80E0B0C53491CC916EE406378649D6 (void);
// 0x000009CA System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::set_implementation(UnityEngine.Experimental.Rendering.IScriptableRuntimeReflectionSystem)
extern void ScriptableRuntimeReflectionSystemWrapper_set_implementation_m95A62C63F5D1D50EDCD5351D74F73EEBC0A239B1 (void);
// 0x000009CB System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::Internal_ScriptableRuntimeReflectionSystemWrapper_TickRealtimeProbes(System.Boolean&)
extern void ScriptableRuntimeReflectionSystemWrapper_Internal_ScriptableRuntimeReflectionSystemWrapper_TickRealtimeProbes_mC04DACDD9BF402C3D12DE78F286A36B3A81BA547 (void);
// 0x000009CC System.Void UnityEngine.Experimental.Rendering.ScriptableRuntimeReflectionSystemWrapper::.ctor()
extern void ScriptableRuntimeReflectionSystemWrapper__ctor_m14586B1A430F0316A379C966D52BDE6410BA5FCC (void);
// 0x000009CD UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat(UnityEngine.TextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_mF9AFEB31DE7E63FC76D6A99AE31A108491A9F232 (void);
// 0x000009CE UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat_Native_TextureFormat(UnityEngine.TextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_Native_TextureFormat_mAE09EC30C3D01C3190767E2590DC0FD9358A1C5C (void);
// 0x000009CF UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat(UnityEngine.RenderTextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_mD73B7F075511D7D392D55B146711F19A76E5AF1C (void);
// 0x000009D0 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat_Native_RenderTextureFormat(UnityEngine.RenderTextureFormat,System.Boolean)
extern void GraphicsFormatUtility_GetGraphicsFormat_Native_RenderTextureFormat_m7F44D525B67F585D711628BC2981852A6DA7633B (void);
// 0x000009D1 UnityEngine.Experimental.Rendering.GraphicsFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetGraphicsFormat(UnityEngine.RenderTextureFormat,UnityEngine.RenderTextureReadWrite)
extern void GraphicsFormatUtility_GetGraphicsFormat_m5ED879E5A23929743CD65735DEDDF3BE701946D8 (void);
// 0x000009D2 System.Boolean UnityEngine.Experimental.Rendering.GraphicsFormatUtility::IsSRGBFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void GraphicsFormatUtility_IsSRGBFormat_mDA5982709BD21EE1163A90381100F6C7C6F40F1C (void);
// 0x000009D3 UnityEngine.RenderTextureFormat UnityEngine.Experimental.Rendering.GraphicsFormatUtility::GetRenderTextureFormat(UnityEngine.Experimental.Rendering.GraphicsFormat)
extern void GraphicsFormatUtility_GetRenderTextureFormat_m46863058423E437148320C07088F53250E781F95 (void);
// 0x000009D4 System.Boolean UnityEngine.Experimental.Rendering.GraphicsFormatUtility::IsCompressedTextureFormat(UnityEngine.TextureFormat)
extern void GraphicsFormatUtility_IsCompressedTextureFormat_m740C48D113EFDF97CD6EB48308B486F68C03CF82 (void);
// 0x000009D5 System.Boolean UnityEngine.Experimental.Rendering.GraphicsFormatUtility::IsCrunchFormat(UnityEngine.TextureFormat)
extern void GraphicsFormatUtility_IsCrunchFormat_mB31D5C0C0D337A3B00D1AED3A7E036CD52540F66 (void);
static Il2CppMethodPointer s_methodPointers[2517] = 
{
	MathfInternal__cctor_mB7CF38BBE41ECBC62A4457C91FDFD0FEE8679895,
	TypeInferenceRuleAttribute__ctor_m8B31AC5D44FB102217AB0308EE71EA00379B2ECF,
	TypeInferenceRuleAttribute__ctor_mE01C01375335FB362405B8ADE483DB62E7DD7B8F,
	TypeInferenceRuleAttribute_ToString_mD1488CF490AFA2A7F88481AD5766C6E6B865ABC4,
	GenericStack__ctor_m42B668E8F293EE21F529A2679AA110C0877605DD,
	ProfilerMarker__ctor_mCE8D10CF2D2B2C4E51BF1BB66D75FDDE5BDA4A41_AdjustorThunk,
	ProfilerUnsafeUtility_CreateMarker_m419027084C68545B765B9345949D8BFCB96C51AD,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	JobHandle_Complete_m947DF01E0F87C3B0A24AECEBF72D245A6CDBE148_AdjustorThunk,
	JobHandle_get_IsCompleted_m2747303E2CD600A78AC8F3ED1EA40D3F507C75A2_AdjustorThunk,
	JobHandle_ScheduleBatchedJobs_m31A19EE8C93D6BA7F2222001596EBEF313167916,
	JobHandle_ScheduleBatchedJobsAndComplete_m9D762E10C5648909D15E56C8E099410300F691A0,
	JobHandle_ScheduleBatchedJobsAndIsCompleted_mCF40C24E56E7EFE034ED8588E9041F47A93BAEA6,
	JobProducerTypeAttribute__ctor_m6CF83F27400526E37ED6CEADFF330E49A310BA7C,
	JobsUtility_GetWorkStealingRange_m8E2276200A11FDF636F1C6092E786ACD0396435C,
	JobsUtility_Schedule_mEE082ED2BA501FE2A656B0EEB4086A69A5C66D37,
	JobsUtility_ScheduleParallelFor_mB2883217C5BAE5ED43D89478B76849936EDDEE78,
	JobsUtility_CreateJobReflectionData_mCB06DEB035740C84B8729194E4941840C315D1C4,
	JobsUtility_CreateJobReflectionData_m78D2E589323A7C22D6B577B4C6B08569147EAC05,
	JobsUtility_Schedule_Injected_mFDFBDD20D03C3D74C5D56206B6DC08E101ECF64C,
	JobsUtility_ScheduleParallelFor_Injected_m0F7C3AB0415EF402D7242BE6FA82F2ACBC899360,
	JobScheduleParameters__ctor_m7BAC662AF5FA4A96429644CC676D3179A5B143F8_AdjustorThunk,
	ReadOnlyAttribute__ctor_mAD4CE17A9D08D1361B9D0E789E9B7DEA9A343ABF,
	WriteOnlyAttribute__ctor_m192B6FC3CA901E8D8F15B9ED0E548EEC6EC4DF2F,
	NativeLeakDetection_Initialize_m1A20F4DD5DD1EF32704F40F0B05B0DE021399936,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NativeContainerAttribute__ctor_m3863E2733AAF8A12491A6D448B14182C89864633,
	NativeContainerSupportsMinMaxWriteRestrictionAttribute__ctor_mBB573360E0CCDC3FEBD208460D9109485687F1C8,
	NativeContainerSupportsDeallocateOnJobCompletionAttribute__ctor_mFA8893231D6CAF247A130FA9A9950672757848E6,
	NativeContainerSupportsDeferredConvertListToArray__ctor_m0C65F9A9FF8862741C56E758FB996C5708E2D975,
	WriteAccessRequiredAttribute__ctor_m832267CA67398C994C2B666B00253CD9959610B9,
	NativeDisableUnsafePtrRestrictionAttribute__ctor_m649878F0E120D899AA0B7D2D096BC045218834AE,
	NULL,
	UnsafeUtility_Malloc_m18FCC67A056C48A4E0F939D08C43F9E876CA1CF6,
	UnsafeUtility_Free_mA805168FF1B6728E7DF3AD1DE47400B37F3441F9,
	UnsafeUtility_MemCpy_m8E335BAB1C2A8483AF8531CE8464C6A69BB98C1B,
	UnsafeUtility_MemSet_m963D137A8DADF01067A68EFDEF736E7216411353,
	UnsafeUtility_MemClear_m9A2B75C85CB8B6637B1286A562A8E35C82772D09,
	UnsafeUtility_SizeOf_mE364EE2E10DCC0FF112874221FFD07C7C3E08676,
	UnsafeUtility_IsBlittable_mC76E5617EC7CA98CC232702004D71915DDBF1D47,
	UnsafeUtility_IsBlittableValueType_mC548D28E3E1FF39148C8A2D745A30AB6122485B7,
	UnsafeUtility_GetReasonForTypeNonBlittableImpl_mCB0D7E72BB113F1628B2FEFDEB175FDC1A6ED5DF,
	UnsafeUtility_IsArrayBlittable_m1F4CE4F21EF62AF1D4BC1F7A0C520745814BC959,
	UnsafeUtility_GetReasonForArrayNonBlittable_m451743E99803EC89E4397135323169DF87D9F620,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SortingLayer_GetLayerValueFromID_mA244A6AFF800BD8D27D9E402C01EC9B1D85421F3,
	Keyframe__ctor_m572CCEE06F612003F939F3FF439B15F89E8C1D54_AdjustorThunk,
	Keyframe_get_time_m75EBFDECA329315F6D41A60C0B1291F5BA4039E8_AdjustorThunk,
	Keyframe_set_time_mB48C8B14B2346F46A0A4FE27CDD01D163F945CC4_AdjustorThunk,
	AnimationCurve_Internal_Destroy_m29AC7F49DA0754B8C7A451FE946BD9A38B76E61F,
	AnimationCurve_Internal_Create_m876905D8C13846F935CA93C0C779368519D01D0C,
	AnimationCurve_Internal_Equals_m0D37631AC99BD190E2E753012C2F24A63DD78D05,
	AnimationCurve_Finalize_m4F8AF6E43E488439AB1022E7A97FEDE777655375,
	AnimationCurve_Evaluate_m1248B5B167F1FFFDC847A08C56B7D63B32311E6A,
	AnimationCurve_get_keys_m64FA75C0B1F7BCDE123EFB903962B2BD9DD5F321,
	AnimationCurve_set_keys_m146468F0FF5228D829489E845C1193B1D83543BF,
	AnimationCurve_AddKey_m3AEE7259785540EF6A157BA99B3737AC60E30D9A,
	AnimationCurve_AddKey_Internal_mAC23772AFF804FE8E436AB21EE9522F698912DFD,
	AnimationCurve_get_Item_mD5F6B4C79C432C9CEADB6D116C07969802D5611A,
	AnimationCurve_get_length_mB3D0734222EE55DB1389BCB98CCB1324AF8AA4E0,
	AnimationCurve_SetKeys_mAD2913A53CA10840C9DA59A94210CD228499FAFE,
	AnimationCurve_GetKey_m4B24EFBA502C3FE5F3E93BDF7ADAC10C44EB3168,
	AnimationCurve_GetKeys_mC0CB4482DA2A68FC0B1359DC6D3FCAA0873A9941,
	AnimationCurve__ctor_mDF6C1314A61F0E6F286865DD8BEA991795C07AC0,
	AnimationCurve__ctor_m68D6F819242C539EC522FEAFFEB6F1579767043E,
	AnimationCurve_Equals_mE1B90C79209D2E006B96751B48A2F0BA6F60A5B8,
	AnimationCurve_Equals_mFB50636B9BE34BBD83BE401186BC1EB7267C5416,
	AnimationCurve_GetHashCode_mCF18923053E945F39386CE8F1FD149D4020BC4DD,
	AnimationCurve_AddKey_Internal_Injected_m3D52627A04B7A4D84F0DBE43E9EF2483D0CE53BC,
	AnimationCurve_GetKey_Injected_m6621C42E9C97A7F85A63C06DFA0A11DC3D9C5DD4,
	Application_Quit_m0B016F5CC91420F53C8844350ECD7FCA673226B2,
	Application_Quit_m8D720E5092786C2EE32310D85FE61C253D3B1F2A,
	Application_get_isPlaying_m7BB718D8E58B807184491F64AFF0649517E56567,
	Application_get_dataPath_m026509C15A1E58FC6461EE7EC336EC18C9C2271E,
	Application_get_streamingAssetsPath_mA1FBABB08D7A4590A468C7CD940CD442B58C91E1,
	Application_get_persistentDataPath_mBD9C84D06693A9DEF2D9D2206B59D4BCF8A03463,
	Application_get_temporaryCachePath_mB0F2F6D4D8FD2E082F7E0831A90FC6F1D18C23DF,
	Application_set_targetFrameRate_m0F44C8D07060E17D9D44D176888D14DBABE0CBFC,
	Application_get_platform_mB22F7F39CDD46667C3EF64507E55BB7DA18F66C4,
	Application_get_isMobilePlatform_m86CE0D4224E9F3E4DC8DFF848C5E559F4B3E622E,
	Application_get_systemLanguage_m97271242ECD614FD02DC6BEB912CDDB6DD2BD045,
	Application_CallLowMemory_m508B1899F8865EC715FE37ACB500C98B370F7329,
	Application_CallLogCallback_m42BBBDDFC6BAD182D6D574F22C0B73F3F881D681,
	Application_Internal_ApplicationWantsToQuit_mF91858E5F03D57EDCCB80F470ABBA60B0FBCC5BE,
	Application_Internal_ApplicationQuit_mDFB4E615433A0A568182698ADE0E316287EDAFE3,
	Application_Internal_ApplicationUnload_m28EA03E5E3B12350E6C4147213DE14297C50BEC7,
	Application_InvokeOnBeforeRender_mB7267D4900392FD7E09E15DF3F6CE1C15E6598F9,
	Application_InvokeFocusChanged_m1A6F45FF1CA2B03A5FB1FA864BFC37248486AC2F,
	Application_InvokeDeepLinkActivated_mCE514E3BC1F10AF34A58A40C95C4CD5ACED1396B,
	Application_get_isEditor_m7367DDB72F13E4846E8EB76FFAAACA84840BE921,
	LowMemoryCallback__ctor_m91C04CE7D7A323B50CA6A73B23949639E1EA53C3,
	LowMemoryCallback_Invoke_mDF9C72A7F7CD34CC8FAED88642864AE193742437,
	LowMemoryCallback_BeginInvoke_mF446F2B2F047BC877D3819B38D1CB0AFC4C4D953,
	LowMemoryCallback_EndInvoke_mB653B34015F110168720FED3201FDD6D2B235212,
	LogCallback__ctor_mB5F165ECC180A20258EF4EFF589D88FB9F9E9C57,
	LogCallback_Invoke_m5503AB0FDB4D9D1A8FFE13283321AF278B7F6C4E,
	LogCallback_BeginInvoke_m346CFB69BAB75EF96F9EBA2B3C312A1AD1D4147F,
	LogCallback_EndInvoke_m940C8D1C936259607F04EA27DA8FBB2828FC9280,
	BootConfigData_WrapBootConfigData_mB8682F80DBE83934FFEAFE08A08B376979105E48,
	BootConfigData__ctor_m4BF11252A4EA57BE0B82E6C1657B621D163B8068,
	Camera_get_nearClipPlane_m75A7270074A35D95B05F25EBF8CE392ECA6517DC,
	Camera_set_nearClipPlane_m4EA1D92F6E1D17E423EC036561E115F434DC2263,
	Camera_get_farClipPlane_m0FA1B9E2E815BECE2EA40023302EB942B52D9596,
	Camera_set_farClipPlane_m63F1302068433A72A5103281327C68CD8F67AD41,
	Camera_get_fieldOfView_mA9BA910800B2E33B572929CDA9A12CE596353920,
	Camera_set_fieldOfView_m138FE103CAC4B803F39E4CF579609A5C3FEB5E49,
	Camera_get_renderingPath_m9E4313D0F05EDEA75B1154CCE05EB8F9FC0474CD,
	Camera_set_renderingPath_mCB2BAB2434EEAE5C3B44083403B8D620414DFD86,
	Camera_get_actualRenderingPath_m8BA9E6EE548FA2EDB981948271E81D81006DC9BE,
	Camera_get_allowHDR_mD961A362F2C8BBA6FF0B316488FC566D64CB6222,
	Camera_get_orthographicSize_m970DC87D428A71EDF30F9ED7D0E76E08B1BE4EFE,
	Camera_set_orthographicSize_mFC4BBB0BB0097A5FE13E99D8388DF3008971085F,
	Camera_get_orthographic_m3DE9A7705E2B4926BBFD2D6798D97B931B5B5550,
	Camera_set_orthographic_m38A872FC2D448915EE94C4FB72FB02D4C3F56C52,
	Camera_get_depth_m063B48665DB9226949AC3A615362EA20193B823D,
	Camera_set_depth_m33DBE382C6A293EDFF50FF459CBAB2FD3B7F469C,
	Camera_get_aspect_mD0A1FC8F998473DA08866FF9CD61C02E6D5F4987,
	Camera_set_aspect_mF0D72E1250A4408B6A7C32700EE3D80C75C3879C,
	Camera_get_cullingMask_m63492ED3AFA8F571FBED0B1729264A2E3BB64236,
	Camera_set_cullingMask_mB4B1EE4C6CC355625EDE08EAF17E99DAB242E0CC,
	Camera_get_eventMask_m69507E71D5281F902A304A8BDDE7D23A3C501292,
	Camera_set_cullingMatrix_m74474597B2F1D09F4802A40D595E96B9EDEC7F05,
	Camera_get_backgroundColor_m556B0BCFA01DC59AA6A3A4B27C9408C72C144FB5,
	Camera_set_backgroundColor_m7083574094F4031F3289444E1AF4CBC4FEDACFCF,
	Camera_get_clearFlags_m7D0E7A0DBAB6A84B680EC09835AA2F081A17E0D7,
	Camera_set_clearFlags_mE79A756CD7C9C84C86B6676F5C6342A45AE5F373,
	Camera_get_depthTextureMode_mA4B03C6B552FB03DF8A8C20DC9F4EB8FD978A7DD,
	Camera_set_depthTextureMode_m2D4631800947438BE9A7697778E2CB0E38083CF1,
	Camera_get_rect_m6B59AEFF7465FA7B605D25E04E99B6E555D59DB7,
	Camera_get_pixelRect_m58284153875DDE6470D4BDCAF2DFC9F5C9DE3D3A,
	Camera_get_pixelWidth_m7DC2EF816FA7AB52EEE991E054FC7B1F31982802,
	Camera_get_pixelHeight_m7A18CEE2D95835EB95156E88D4D27EA018404201,
	Camera_get_targetTexture_m1DF637F05FF945625231DED8F3071795755DD4BF,
	Camera_set_targetTexture_m200B05665D1F5C62D9016C2DD20955755BAB4596,
	Camera_get_targetDisplay_mED770420CB57E500C60BE15B9F7F5ED424F0BA3D,
	Camera_get_worldToCameraMatrix_m7E2B63F64437E2C91C07F7FC819C79BE2152C5F6,
	Camera_set_worldToCameraMatrix_mD9E982CC4F356AC310C5B3145FC59FD73C420BE3,
	Camera_get_projectionMatrix_mDB77E3A7F71CEF085797BCE58FAC78058C5D6756,
	Camera_set_projectionMatrix_m3645AC49FC94726BDA07191C80299D8361D5C328,
	Camera_set_nonJitteredProjectionMatrix_mBD4086F0CE187C0E68619606FA3F06AB9113A7D0,
	Camera_set_useJitteredProjectionMatrixForTransparentRendering_mC534E01407398A73886F2E5796824120B50461EA,
	Camera_ResetProjectionMatrix_m3010D24D6ACC1880F046CAB995A1EF69B4D3C2BE,
	Camera_CalculateObliqueMatrix_m8DCA87B4563A3515995B189E4EBEAE548563B94C,
	Camera_WorldToScreenPoint_m205044769D9BC639283F3B929D964E1982067637,
	Camera_ViewportToWorldPoint_m65A69A57F4259FE074749BF3DFED7418DDAF99AA,
	Camera_WorldToScreenPoint_m44710195E7736CE9DE5A9B05E32059A9A950F95C,
	Camera_ViewportToWorldPoint_m1273EE3868551C6FF551ABA9A76DC7D66E883116,
	Camera_ScreenToViewportPoint_m0300D4845234BDBE1A1D08CF493966C57F6D4D8A,
	Camera_ViewportPointToRay_mCD14AD107A561268CBC5E578BB336AC5AA047365,
	Camera_ViewportPointToRay_mB5148E07D2D84822653EFC04A76D819113A06781,
	Camera_ViewportPointToRay_mA5F17F1603768A23286A637F706B612BCEF8D2C8,
	Camera_ScreenPointToRay_mE590E18429611C84F551E9FA8ED67391ED32D437,
	Camera_ScreenPointToRay_m217923426BB5E72835726B1ED792BFFFF8B498C1,
	Camera_ScreenPointToRay_mD385213935A81030EDC604A39FD64761077CFBAB,
	Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C,
	Camera_get_current_m4E5A6D19F422F8DD2CFF4EE80C65B033F24C45D6,
	Camera_GetAllCamerasCount_m3B036EBA99A0F6E8FF30C9B14DDD6C3A93C63529,
	Camera_GetAllCamerasImpl_m220999F1251B5F6846B56F6F63D0D0820AF51F0B,
	Camera_get_allCamerasCount_mFDB01BDA0C2C796020756A9DD53C38E32046B801,
	Camera_GetAllCameras_mC94EF7B11A5BA31CED54B106E68D64ED82D0CEF4,
	Camera_Render_m2D9749799AAC91A3725044A6CF5594E1F5D68D61,
	Camera_AddCommandBufferImpl_mE730232060696749068B95BB44E8F766A1E6DE7F,
	Camera_RemoveCommandBufferImpl_mD35499AF3AA5F880EAF57ADB9B3605C30311BD94,
	Camera_AddCommandBuffer_m8F88C5009AC9676BCD0AD1AE4AA655D5BF71CB2F,
	Camera_RemoveCommandBuffer_m6F4BE369C87C0DB3913AE4A8DA31B2838B1DA3E6,
	Camera_FireOnPreCull_mAA81BB789FC8C7516B71ED4E1452C4D8E99334C9,
	Camera_FireOnPreRender_mB282AD49DFA0C9036D92BAE0E28F2567C713099E,
	Camera_FireOnPostRender_mC07CDF605EEABC89BD814E09F2542E9EE16AE0DA,
	Camera__ctor_m30D37AB91648C862FCB8E69805DC4DF728A2804C,
	Camera_set_cullingMatrix_Injected_mAB1C10742D9D80FA70AC950323FBBFAA41D6F945,
	Camera_get_backgroundColor_Injected_mF1A0E8DF67F470A21F4D1A4930B978C971C5E1FC,
	Camera_set_backgroundColor_Injected_mB5B121018E6FEF0BC6265F7EEAA8B8B77EFC6E9B,
	Camera_get_rect_Injected_mB6D9AB2842F87749A8AA7350C8433DE5CC6E2E9D,
	Camera_get_pixelRect_Injected_mE01A6F5B5892953CD25DECDBC12B5CBF15CD84E2,
	Camera_get_worldToCameraMatrix_Injected_mB4EE64122499915852BB85AC9383872B075AF2DA,
	Camera_set_worldToCameraMatrix_Injected_m1C04D82A042ADA2F5ADC85002FD65F6415B85CEB,
	Camera_get_projectionMatrix_Injected_m6CC44F0D6BF337883510093EFFAC51ECE0E9BDAD,
	Camera_set_projectionMatrix_Injected_m1DF5C1D3A0CB6B574F04A5EDA656CFF9B066A763,
	Camera_set_nonJitteredProjectionMatrix_Injected_m1AEE58E49B64826640BA82AE15ECE7416BE9BD5A,
	Camera_CalculateObliqueMatrix_Injected_m41578914E8138AB55C51C55ADA2D1E79703AB0A3,
	Camera_WorldToScreenPoint_Injected_mE1001A12AC94E57CE91F40D345F0DBC3C42C188A,
	Camera_ViewportToWorldPoint_Injected_m6071E9158AF48984C9C5516462BD8D2EDF38F724,
	Camera_ScreenToViewportPoint_Injected_m313B8AF34F729447B818935A20A7EBE6724AC473,
	Camera_ViewportPointToRay_Injected_m908348B88DD53BB24B8CEB6A912E15FDBDEE7563,
	Camera_ScreenPointToRay_Injected_mB548944EF4CBF628CB55E7A729DA28A9BFFB4006,
	CameraCallback__ctor_m6E26A220911F56F3E8936DDD217ED76A15B1915E,
	CameraCallback_Invoke_m52ACC6D0C6BA5A247A24DB9C63D4340B2EF97B24,
	CameraCallback_BeginInvoke_m7DD0D3DB7F6ACCBFE090C1E2EEE9BBD065A0925D,
	CameraCallback_EndInvoke_m2CF9596F172FF401AAF5A606BE966E3D08F62DB9,
	CullingGroup_SendEvents_m01D14A887DFA90F3ED208073A2AE283ADF2C8B22,
	StateChanged__ctor_mBBB5FB739CB1D1206034FFDE998AE8342CC5C0C2,
	StateChanged_Invoke_m6CD13C3770E1EB709C4B125F69F7E4CE6539814D,
	StateChanged_BeginInvoke_m70DAA3720650BA61CA34446A5E19736213E82D4D,
	StateChanged_EndInvoke_m742AAC097C6A02605BF7FB6AA283CA24C021ED65,
	ReflectionProbe_CallReflectionProbeEvent_mD705BC25F93FC786FA7E2B7E1025DF35366AF31D,
	ReflectionProbe_CallSetDefaultReflection_m88965097CBA94F6B21ED05F6770A1CAF1279486C,
	DebugLogHandler_Internal_Log_mA1D09B6E813ABFAB6358863B6C2D28E3ACA9E101,
	DebugLogHandler_Internal_LogException_mD0D1F120433EB1009D23E64F3A221FA234718BFF,
	DebugLogHandler_LogFormat_mB876FBE8959FC3D9E9950527A82936F779F7A00C,
	DebugLogHandler_LogException_m131BB407038398CCADB197F19BB4AA2435627386,
	DebugLogHandler__ctor_mA13DBA2C9834013BEC67A4DF3E414F0E970D47C8,
	Debug_get_unityLogger_m70D38067C3055104F6C8D050AB7CE0FDFD05EE22,
	Debug_DrawLine_m5384A47F25E5B174ABA7D6A7A8726F2F17A5241F,
	Debug_DrawRay_m3954B3FFA675C0660ED438E8B705B45EDE393C60,
	Debug_DrawRay_m918D1131BACEBD7CCEA9D9BFDF3279F6CD56E121,
	Debug_DrawRay_m8477619B612629DAC24B466874AA23B6DC005D8D,
	Debug_DrawRay_m9F3CB318085029EFD8FEBB224C84F78D84543A0E,
	Debug_Break_m9F329399AFCF1871DD61583BD8EEB3F2E9B72C3A,
	Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8,
	Debug_Log_mF3A49A8FCD5EC1535CADCDFF57432813A8790DF6,
	Debug_LogFormat_m3FD4ED373B54BC92F95360449859CD95D45D73C6,
	Debug_LogError_m8850D65592770A364D494025FF3A73E8D4D70485,
	Debug_LogError_mEFF048E5541EE45362C0AAD829E3FA4C2CAB9199,
	Debug_LogErrorFormat_mDBF43684A22EAAB187285C9B4174C9555DB11E83,
	Debug_LogErrorFormat_m1807338EFAE61B3F6CF96FCB905D9B8E2DBAA0F7,
	Debug_LogException_m1BE957624F4DD291B1B4265D4A55A34EFAA8D7BA,
	Debug_LogException_mE0C50EE1EE5F38196CABAF961EF7E43DD520C29B,
	Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7,
	Debug_LogWarning_mE6AF3EFCF84F2296622CD42FBF9EEAF07244C0A8,
	Debug_LogWarningFormat_m405E9C0A631F815816F349D7591DD545932FF774,
	Debug_LogWarningFormat_m2DED8BDFB26AFA883EEDD701573B30B093270CA7,
	Debug_CallOverridenDebugHandler_mDE23EE8FA741C9568B37F4128B0DA7976F8612DC,
	Debug__cctor_m8FC005AAA0C78F065A27FD1E48B12C5C5E38A486,
	Debug_DrawLine_Injected_m6AC14B6FE78DAD68F95C3E8826C36D46849983F9,
	Bounds__ctor_m8356472A177F4B22FFCE8911EBC8547A65A07CA3_AdjustorThunk,
	Bounds_GetHashCode_m822D1DF4F57180495A4322AADD3FD173C6FC3A1B_AdjustorThunk,
	Bounds_Equals_mB759250EA283B06481746E9A247B024D273408C9_AdjustorThunk,
	Bounds_Equals_mC558BE6FE3614F7B42F5E22D1F155194A0E3DD0F_AdjustorThunk,
	Bounds_get_center_m78CD262996DD859F71DAFFF39228EBE0C422F485_AdjustorThunk,
	Bounds_set_center_mAC54A53224BBEFE37A4387DCBD0EF3774751221E_AdjustorThunk,
	Bounds_get_size_mB1C37E89879C7810BC9F4210033D9277DAFE2C14_AdjustorThunk,
	Bounds_set_size_mEDB113237CED433C65294B534EAD30449277FD18_AdjustorThunk,
	Bounds_get_extents_mA54D7497D65DCF21CA952FA754B9D1086305FF02_AdjustorThunk,
	Bounds_set_extents_m9590B3BB7D59A4C35D9F0E381A20C6A96A4D9D89_AdjustorThunk,
	Bounds_get_min_mEDCEC21FB04A8E7196EDE841F7BE9042E1908519_AdjustorThunk,
	Bounds_get_max_m7562010AAD919B8449B8B90382BFBA4D83C669BD_AdjustorThunk,
	Bounds_op_Equality_mE94F24EA5893A56978CAB03E5881AD8A0767208C,
	Bounds_op_Inequality_m7072FE6FA9E44024F1B71806364E9189792D00C6,
	Bounds_SetMinMax_m37B0AF472E857A51AC86463D1CA7DB161D5A880D_AdjustorThunk,
	Bounds_Encapsulate_mE74070861B19845E70A3C333A5206823ADC7DAD7_AdjustorThunk,
	Bounds_Encapsulate_mA1D113F56C635E1ACD53FCCB1C6C2560A295533B_AdjustorThunk,
	Bounds_ToString_mC2F42E6634E786CC9A1B847ECDD88226B7880E7B_AdjustorThunk,
	Bounds_ToString_m493BA40092851F60E7CE73A8BD58489DAE18B3AD_AdjustorThunk,
	Bounds_Contains_m24154F7F564711846389EF6AEDABB4092A72FFA1_AdjustorThunk,
	Bounds_Contains_Injected_mF76162C458967E6EE016D0F525B9DE13F678CEDC,
	Plane_get_normal_m71CB4BAB04EE04CAEF9AD272B16766800F7D556B_AdjustorThunk,
	Plane__ctor_m5B830C0E99AA5A47EF0D15767828D6E859867E67_AdjustorThunk,
	Plane__ctor_m9CFA0680D3935F859B6478E777A34168D4F1D19A_AdjustorThunk,
	Plane_Raycast_m8E3B0EF5B22DF336430373D4997155B647E99A24_AdjustorThunk,
	Plane_ToString_mD0E5921B1DFC4E83443BA7267E1182ACDD65C5DD_AdjustorThunk,
	Plane_ToString_m0AF5EF6354605B6F6698346081FBC1A8BE138C4B_AdjustorThunk,
	Ray__ctor_m75B1F651FF47EE6B887105101B7DA61CBF41F83C_AdjustorThunk,
	Ray_get_origin_m0C1B2BFF99CDF5231AC29AC031C161F55B53C1D0_AdjustorThunk,
	Ray_get_direction_m2B31F86F19B64474A901B28D3808011AE7A13EFC_AdjustorThunk,
	Ray_set_direction_mDA9E058A81EA8D21BCD222A4729F66071FDDAFE3_AdjustorThunk,
	Ray_GetPoint_mC92464E32E42603B7B3444938E8BB8ADA43AB240_AdjustorThunk,
	Ray_ToString_mC923383E101007E445FB0229261813AD13FBA509_AdjustorThunk,
	Ray_ToString_m9D764E4D9D0742160471AFD9D0AA21B13252C1EC_AdjustorThunk,
	Rect__ctor_m12075526A02B55B680716A34AD5287B223122B70_AdjustorThunk,
	Rect__ctor_m00C682F84642AE657D7EBB0D5BC6E8F3CA4D1E82_AdjustorThunk,
	Rect_get_zero_m4F738804E40698120CC691AB45A6416C4FF52589,
	Rect_get_x_mA61220F6F26ECD6951B779FFA7CAD7ECE11D6987_AdjustorThunk,
	Rect_set_x_m1147A05B5046E1D4427E8EC99B9DFA4A32EEDEE6_AdjustorThunk,
	Rect_get_y_m4E1AAD20D167085FF4F9E9C86EF34689F5770A74_AdjustorThunk,
	Rect_set_y_m015507262F8AC5AFF1B4E986B66307F31FB3A10E_AdjustorThunk,
	Rect_get_position_m4D98DEE21C60D7EA5E4A30869F4DBDE25DB93A86_AdjustorThunk,
	Rect_set_position_m25716C90100155ED807D2812E24D2880D7D89D0D_AdjustorThunk,
	Rect_get_center_mDFC7A4AE153DCDC1D6248803381C6F36C7883707_AdjustorThunk,
	Rect_get_min_mAB48143A34188D0C92C811E6BCE3684FC81F29B6_AdjustorThunk,
	Rect_get_max_mD553C13D7CBC8CD7DB0D7FD0D7D2B703734EAC78_AdjustorThunk,
	Rect_get_width_m4A0500D95CA84917787A8E90D26E66D49DFA90EF_AdjustorThunk,
	Rect_set_width_m07D84AD7C7093EDCCD94A7B93A9447CA9917DD9D_AdjustorThunk,
	Rect_get_height_m42FEF31015A269E6E2B7E6F62E72E5BF6602302A_AdjustorThunk,
	Rect_set_height_m4A00B16C122F44FEF4BA074386F3DC11FF4B4D23_AdjustorThunk,
	Rect_get_size_m752B3BB45AE862F6EAE941ED5E5C1B01C0973A00_AdjustorThunk,
	Rect_set_size_mC7194C291CB6B00C39FEE55985FE0D2B6F4ACF14_AdjustorThunk,
	Rect_get_xMin_m02EA330BE4C4A07A3F18F50F257832E9E3C2B873_AdjustorThunk,
	Rect_set_xMin_mC91AC74347F8E3D537E8C5D70015E9B8EA872A3F_AdjustorThunk,
	Rect_get_yMin_m2C91041817D410B32B80E338764109D75ACB01E4_AdjustorThunk,
	Rect_set_yMin_mA2FDFF7C8C2361A4CF3F446BAB9A861F923F763A_AdjustorThunk,
	Rect_get_xMax_m174FFAACE6F19A59AA793B3D507BE70116E27DE5_AdjustorThunk,
	Rect_set_xMax_m4E466ED07B11CC5457BD62517418C493C0DDF2E3_AdjustorThunk,
	Rect_get_yMax_m9685BF55B44C51FF9BA080F9995073E458E1CDC3_AdjustorThunk,
	Rect_set_yMax_m4E7A7C5E88FA369D6ED022C939427F4895F6635D_AdjustorThunk,
	Rect_Contains_m8438BA185AD95D6C50E7A2CF9DD30FBA044CE0B2_AdjustorThunk,
	Rect_Contains_m51C65159B1706EB00CC962D7CD1CEC2EBD85BC3A_AdjustorThunk,
	Rect_OrderMinMax_mB0EAA3C5660D716D83556F42F7D87DDB8FF7F39C,
	Rect_Overlaps_mFF91B379E163CE421F334C99C9F3E5A7D3C1591F_AdjustorThunk,
	Rect_Overlaps_m4B186F55121E25A8D498AEFECCE973AEE62E7EDD_AdjustorThunk,
	Rect_op_Inequality_m6D87EE93EB6C68B78B8C044217EAFCE33EE12B66,
	Rect_op_Equality_m17C955A4F85F01A7CF0B43EDE41463301E93F6C1,
	Rect_GetHashCode_mE5841C54528B29D5E85173AF1973326793AF9311_AdjustorThunk,
	Rect_Equals_mF1F747B913CDB083ED803F5DD21E2005736AE4AB_AdjustorThunk,
	Rect_Equals_mC9EE5E0C234DB174AC16E653ED8D32BB4D356BB8_AdjustorThunk,
	Rect_ToString_mCB7EA3D9B51213304AB0175B2D5E4545AFBCF483_AdjustorThunk,
	Rect_ToString_m3DFE65344E06224C23BB7500D069F908D5DDE8F5_AdjustorThunk,
	RectInt_get_x_m2D7412BC4EE7871D19042F469D6C573016E39F79_AdjustorThunk,
	RectInt_get_y_mAEB15FA1B7CCD08DA9FCB978EE0BA45D6BB9FE33_AdjustorThunk,
	RectInt_get_width_m7C3DBF05BE2DC727635FC9D7C778EACF0A9E4085_AdjustorThunk,
	RectInt_get_height_m5CCF18289CB154C1709AE02AA6375093ECA07DBC_AdjustorThunk,
	RectInt_ToString_mDDF5677F3BD7A81A75913503531D9AAB6E5127A8_AdjustorThunk,
	RectInt_ToString_m4166071DDA0606D66F0BD83538D000174A33EED9_AdjustorThunk,
	RectInt_Equals_mAF3A442FB55544FF362006EDB9B2F225D32BDF6D_AdjustorThunk,
	RectOffset__ctor_m83529BADBE62C2D61ABEE8EB774BAB2D38DCF2AD,
	RectOffset__ctor_mA519A9F678D6B88731C3F6A67E0DA9092A4596D4,
	RectOffset_Finalize_m640BD40EDCC5A2774B9501E75735A1D530AFED27,
	RectOffset__ctor_m428BA3F4AE287FA7D5F4CED6394225951E5E507B,
	RectOffset_ToString_mFD37DA306C2835C1C5CE0F1DFBE92654A27231C0,
	RectOffset_ToString_mA3FFA19BFCBA3A7DE7700B4C6C10E476C61B4ACE,
	RectOffset_Destroy_m56862AB47C5C13956BA4DDE49CB01701069E2EFE,
	RectOffset_InternalCreate_m24E70055383879A2ECA568F0B6B57C026188E3A7,
	RectOffset_InternalDestroy_m58D18EDE9295B31E2BCA824825FE08F9A1C1D42F,
	RectOffset_get_left_m3B3066D09D8C9C94826258386B609CDBFFF11910,
	RectOffset_set_left_m1D87F31ED5C73CEC8F5C42D9013FBB1527A34E36,
	RectOffset_get_right_m889468939F3F926FE21F8E81E23C0342D45615C6,
	RectOffset_set_right_m63D81A7E7FF7060DA8E7C518105C7E2649A5AA1E,
	RectOffset_get_top_m42000C7682185F03F23E7E0C3E8EC026FDBAB9D1,
	RectOffset_set_top_m647BE16B99A85B3FFEE89141EE084E9FBEBF0E86,
	RectOffset_get_bottom_mDDEBF1389FC1B8DCD9FC15DF91E51D264925C00D,
	RectOffset_set_bottom_m9DD7D8F63F7ED6F22A3981E064F6D4DB0527A45E,
	RectOffset_get_horizontal_m7B1D97260EF95BCEDB9A7AF7AC9FAF99D56E9177,
	RectOffset_get_vertical_m589292AEF7A556D4FD0CED648DEED422C1CA36A4,
	LightingSettings_LightingSettingsDontStripMe_m6503DED1969BC7777F8CE2AA1CBBC7ACB229DDA8,
	Gizmos_DrawLine_m91F1AA0205C7D53D2AA8E2F1D7B338E601A30823,
	Gizmos_set_color_m937ACC6288C81BAFFC3449FAA03BB4F680F4E74F,
	Gizmos_DrawLine_Injected_m6001318DA5AFD74C82E087BCFF6A8A23378BC186,
	Gizmos_set_color_Injected_m41CFCA3C458C9BC5F6F71278C59F97004CFB9B6B,
	BeforeRenderHelper_Invoke_m30EA54023BDAB65766E9B5FD6FC90E92D75A7026,
	BeforeRenderHelper__cctor_mAB141E73B12E9FD55D3258F715472F6BA0872282,
	Display__ctor_m12A59C1FBFC6F4BAFCB7ADDACB5BE4E6F61145F0,
	Display__ctor_m3FB487510CB9197672FAE63EFF6FD0FEAA542B4F,
	Display_get_renderingWidth_m426E1CB184C1135E1EE83678FFF7EF6521B5DB64,
	Display_get_renderingHeight_m18F083C41C0BB1646CB4C819E1266B9B51563F61,
	Display_get_systemWidth_m5FDF4465D7B1A0AD8A1A8C5B314BF71F4C8DCBB5,
	Display_get_systemHeight_mA296AFD545D00DF7FEB84E7C690FD56CC2C19D70,
	Display_RelativeMouseAt_m97B71A8A86DD2983B03E4816AE5C7B95484FB011,
	Display_get_main_mAC219027538C9134DF8606B59B8249EE027E867B,
	Display_RecreateDisplayList_m86C6D207FEF8B77696B74ECF530002E260B1591E,
	Display_FireDisplaysUpdated_mA8B70C50D286065B80D47D6D12D195A2FFB223DA,
	Display_GetSystemExtImpl_m81EED1E4A983CF9BF6646F46C19004756F57AD3C,
	Display_GetRenderingExtImpl_m6D78AA779EA195FA7BD70C4EEED9DD2EB986841E,
	Display_RelativeMouseAtImpl_m7991D56921379FEC4AE3516FEDF046D0DCAFDBB9,
	Display__cctor_m87EA9DCECCE11A1F161F7C451A5018180DE9EB06,
	DisplaysUpdatedDelegate__ctor_mE01841FD1E938AA63EF9D1153CB40E44543A78BE,
	DisplaysUpdatedDelegate_Invoke_mBABCF33A27A4E0A5FBC06AECECA79FBF4293E7F9,
	DisplaysUpdatedDelegate_BeginInvoke_m42DDA570D68BFAD055E2FEBB75FBE79EFEE2752F,
	DisplaysUpdatedDelegate_EndInvoke_mBE00DC8335AF5142275DCCE3C5CEBC4ED0F60937,
	Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C,
	Screen_get_height_m110C90A573EE67895DC4F59E9165235EA22039EE,
	Screen_get_dpi_m37167A82DE896C738517BBF75BFD70C616CCCF55,
	Screen_get_currentResolution_mE1A3C7E9603FA56B539FDDA1F602C66732EFD17B,
	Screen_get_fullScreen_mE83864F0C9C773C03D3FBBAD981159238CA3565A,
	Screen_get_fullScreenMode_m282CC0BCBD3C02B7199D0606090D28BB328EC624,
	Screen_SetResolution_m239784140EEC26DC52AB5ED95DD1D9BDBC49ADB1,
	Screen_SetResolution_mF6E20000304F14EC25BA996512719D7742C29175,
	Screen_SetResolution_m9C34571700200E29E9FC2C8B1F096B58F3ACADB4,
	Screen_SetResolution_m1C17EB7EDF8CDA862338782353D53DFF6742DFC5,
	Screen_get_resolutions_mA2968FF5FA4EA6A6AE76019C47338824E8610A81,
	Screen_get_currentResolution_Injected_m8B505219301BD3B52B1AA433F59A3C190C0C1BD7,
	Graphics_Internal_GetMaxDrawMeshInstanceCount_m24408CC79F6E8FECC6931357C34B2A645D6FED0D,
	Graphics_Internal_SetMRTSimple_m4FD6245D60EBBFC2CF6EF5AFB8BD4F761CCF7F8F,
	Graphics_CopyTexture_Slice_AllMips_m08934B607CA033F39B5D661D6365D8DD7D700D40,
	Graphics_CopyTexture_Region_m07F15543D2CE113DAC6E86735500022AB0203DC5,
	Graphics_Internal_DrawTexture_mB6C15F9074A9148F0F8884C6806B9F136BE329D5,
	Graphics_Internal_BlitMaterial5_mEE98BDDC0DED5652473E2AF67A2F9158EB190884,
	Graphics_Blit2_mCD1E51AF9FB9A59E6DC0D54E3E1703C780A7275E,
	Graphics_SetRenderTargetImpl_mB59467C438E6F9558B1993BAA18D480772F8D897,
	Graphics_SetRenderTarget_m1D2A179C2034AED00C0249C87C12379C2CB1E204,
	Graphics_CopyTexture_m6DF56E49A9215CE66C7AC8884503E1A4B9DD54DD,
	Graphics_CopyTexture_mE0CCF667E93CD48B65974B538ECCD6D7E98E1F44,
	Graphics_DrawTextureImpl_mDCC2E07E786CAB30B196108A5D11F99D0F14E194,
	Graphics_DrawTexture_mAAFD6A4E5B5076DFFEB4CBD1C09B3C30146AD4C8,
	Graphics_DrawTexture_mB6E04C3B2283B871D12389667871E56778A9455E,
	Graphics_Blit_m946B14CAE548AAFF3FC223AB54DC5D10AEF760F7,
	Graphics_Blit_mF7AA319E24D980D0E560A721966B87186392C3DD,
	Graphics_Blit_m5E0E60EA4A4D351DCC314AB2C49C7DC5B8863D05,
	Graphics_DrawTexture_mC03C86CF3E8769E5C0E336B514F7FCAB7BE9E77F,
	Graphics_DrawTexture_m4E6020006EB011582C0D7F4E4E71D9220BBC64E5,
	Graphics_DrawTexture_mE17B8B05CA0F48ADE89BDA03BC15EA716BC861EF,
	Graphics__cctor_m13A73AA242ED3CEFBCC84A86F5DEEDC20ADF7DBF,
	Graphics_Internal_SetMRTSimple_Injected_m6FBFA6630343A05E22DCAEFDA7DCBFB354605863,
	GL_Vertex3_mA3D208C1AAEDFCF24364A88552204D8D1BE6AFE9,
	GL_TexCoord3_mF136075FA8821B85501C481A38F1F5186296A6ED,
	GL_TexCoord2_mC0F9C6145A4A03D24E23BA5C5696EE834F8BA5AE,
	GL_get_invertCulling_mC894630CB76C03799A7FE8B28BA1DF8B2E174611,
	GL_set_invertCulling_mECEDF71D7F35A9F99931F6975D415C9C6A8B8FF9,
	GL_PushMatrix_mF1F26015AB75226AB048A7ABF700871C5A0AE0B3,
	GL_PopMatrix_m06F4C60CA3B91C7F8B7EFF83D21563C9613B3738,
	GL_LoadOrtho_mF9410BE33CE4CBB921240C428A10D4086D2D3174,
	GL_GLLoadPixelMatrixScript_m118F6BE15090C744E0DF6A3932C1ED55303650BA,
	GL_LoadPixelMatrix_m93F54ED32CAD774AA81D4057F5839FA965BAD60D,
	GL_Begin_mD719B21F92DCF2D8F567C008AD21CA73CA762622,
	GL_End_m4A164117D2D3CD3620938F2D29EFBA2D5CB900B7,
	GL_GLClear_mE2DD1932E1F31479C9741ED51046DACF5C2309AB,
	GL_Clear_mF5FCE24D7F60731D6D88AA3DC98B0E88A644CD93,
	GL_GLClear_Injected_m701ED01F88E11E3BFEEFDF7D9D88C8B8851B2FF2,
	Resolution_get_width_mDD9DCC89D65057B64C413AF15BEE2E37E9892065_AdjustorThunk,
	Resolution_get_height_mB90F24337D7B96A288F8BE1D0F2F3599B785AD27_AdjustorThunk,
	Resolution_ToString_m0F17D03CC087E67DAB7F8F383D86A9D5C3E2587B_AdjustorThunk,
	QualitySettings_SetQualityLevel_m7547DAEE60DDC96320AA52240F3DB2AD60B2AA01,
	QualitySettings_get_pixelLightCount_mEDF5C3C52253C8E0BAB76B46343228C49945A28B,
	QualitySettings_set_pixelLightCount_m7D98C1D031DADEEA806AA1A9BF8BEF1AE86B6B3A,
	QualitySettings_get_shadowDistance_mF31D534F44DC8CE80694E69BAD6DC80EA3758BFB,
	QualitySettings_set_shadowDistance_mA52D73656ECABD8F02258BEB16BA4D32C9BA0B37,
	QualitySettings_set_shadowResolution_m466012C1FE6B3813D8E65790FB491DD26BBF4B29,
	QualitySettings_get_vSyncCount_m00DC32AF7052D3BFF7EB5C384D5EA45BE0B98520,
	QualitySettings_set_vSyncCount_mE91593C022E003EC0028C9F86751FDF160F32A9E,
	QualitySettings_GetQualityLevel_m0C44BD36C023D6D00BA989E483AC40EA393AF72A,
	QualitySettings_SetQualityLevel_m7925838FC6013484091AB48C3E8FE7B09150A7C6,
	QualitySettings_get_names_mBE61BC830197E73E5E236B86AB1BDF8A9D300A52,
	QualitySettings_get_activeColorSpace_m65BE7300D1A12D2981B492329B32673199CCE7F4,
	ImageEffectAllowedInSceneView__ctor_mD174DBEBB4721497940142A62DCB59429340D44A,
	LineRenderer_set_startWidth_mD88B562DD413EE9861FB254963C7EDCB5199C1DF,
	LineRenderer_set_endWidth_m3DCD4FFCC3944DD6DC0737C8AF8C7EDB5A6CF59A,
	LineRenderer_SetPosition_mD37DBE4B3E13A838FFD09289BC77DEDB423D620F,
	LineRenderer_SetPosition_Injected_mBF66D678D9553A69040E6D697414109AAE5FF5BA,
	MaterialPropertyBlock_SetFloatImpl_mEBFDBFB9842E8EDCD9BA82CDB35582F44F327580,
	MaterialPropertyBlock_CreateImpl_m2191D3625D35DDD79213E0C8B9115FBB419F9311,
	MaterialPropertyBlock_DestroyImpl_mEB8A57D8CAEAE69F4A4C087782629FE03F873C7D,
	MaterialPropertyBlock__ctor_m8EB29E415C68427B841A0C68A902A8368B9228E8,
	MaterialPropertyBlock_Finalize_m07D5F69E6E31F260FE7A7181692D24519E1056B5,
	MaterialPropertyBlock_Dispose_m9B0577FA8137A55BEE6ED1C8CE3714F0F87238B0,
	MaterialPropertyBlock_SetFloat_m5AADD8A9BFF5C1DD5A93BF028A2F001681221942,
	Renderer_get_bounds_m296EE301CAC35DE15E295646CAD7911794825097,
	Renderer_GetMaterial_m1879C3A29E54692423F2030EFDC7CE13817978A3,
	Renderer_GetSharedMaterial_m273D9ED2135F4A6808C4E814BA13E346F4231DFD,
	Renderer_SetMaterial_m45DB63437410875B21F714779FC6D68827BF9497,
	Renderer_GetMaterialArray_m8B6C925CA174B6E97CED486A6492790D2925FDD9,
	Renderer_SetMaterialArray_m7A76143B4B693BEF5EF7993FF13546852866519B,
	Renderer_Internal_SetPropertyBlock_mB753C110E8FFAF514088BC469CAFE4E3197645BB,
	Renderer_Internal_GetPropertyBlock_mFEDF8A808AD77D7E7D2F2C19F3317352ACB99340,
	Renderer_SetPropertyBlock_m3F0E4E98D8274A1396AEBA8456AFA4036DCA7B7A,
	Renderer_GetPropertyBlock_mE73E51E42ED4F800C6422F5461A4D9E1DB61AEDC,
	Renderer_get_enabled_mEFB6D3FD1D9D251EBB6E6286491592C4A6ABD6DE,
	Renderer_set_enabled_mFFBA418C428C1B2B151C77B879DD10C393D9D95B,
	Renderer_set_shadowCastingMode_mDD2D9695A5F1B0D1FCACB8D87A3D1148C14D6F01,
	Renderer_set_receiveShadows_mA7D75C6A0E4C9F3BEB8F9BB27829A5B749AAD875,
	Renderer_get_sortingLayerID_m668C1AA36751AF6655BAAD42BE7627E7950E48E8,
	Renderer_set_sortingLayerID_m8F4A87EB577D6314BF191E6227E139AEBE962BC8,
	Renderer_get_sortingOrder_m043173C955559C12E0A33BD7F7945DA12B755AE0,
	Renderer_set_sortingOrder_mAABE4F8F9B158068C8A1582ACE0BFEA3CF499139,
	Renderer_GetSharedMaterialArray_mD493BFDF52666F719582D0C3AC0F93B16D8D80CA,
	Renderer_get_materials_m96CCC6CDACF2D131E18C7E0E70DE9F3AEA9E9E44,
	Renderer_set_materials_mF25CCE14D0F008A45DC73043BFF69113C485A629,
	Renderer_get_material_mE6B01125502D08EE0D6DFE2EAEC064AD9BB31804,
	Renderer_set_material_m8DED7F4F7AF38755C3D7DAFDD613BBE1AAB941BA,
	Renderer_get_sharedMaterial_m42DF538F0C6EA249B1FB626485D45D083BA74FCC,
	Renderer_set_sharedMaterial_m1E66766F93E95F692C3C9C2C09AFD795B156678B,
	Renderer_get_sharedMaterials_m9B2D432CA8AD8CEC4348E61789CC1BB0C3A00AFD,
	Renderer_set_sharedMaterials_m9838EC09412E988925C4670E8E355E5EEFE35A25,
	Renderer_get_bounds_Injected_m37798C3D5E22FC1910B4ED4FEBDCD264BEF4C7AA,
	RenderSettings_get_fog_mAB01FC3FE552B153EB0D5AB467B29A896719B90F,
	RenderSettings_set_fog_mD367E22D3EF66CA9FAAD7AF7712E891B2F569275,
	RenderSettings_get_fogStartDistance_m49AA3604575352B3B33A08540295045DDF00A53B,
	RenderSettings_set_fogStartDistance_m75DB7FF50F338FEA287BD5409A8A172EE9A40615,
	RenderSettings_get_fogEndDistance_mBE81E3EBEAE4F9360FBECFBC436176DCA5D1858E,
	RenderSettings_set_fogEndDistance_m09D18D44027CBEDD3D0DBF9EC5DC5FA5FE9DFA0B,
	RenderSettings_get_fogMode_m7906B56E609BFBE526DFDE18FBD9FFB44C606B93,
	RenderSettings_set_fogMode_m791A6875CFF4A9C3A937CDFCA765AD6D6D00B703,
	RenderSettings_get_fogColor_m0063E6F93BFEA19A22EA81BDDE0EBFD422731240,
	RenderSettings_set_fogColor_mC46154762710EFAA869A4E972C16D1FE9B0EA01F,
	RenderSettings_get_fogDensity_mDED6FA3F6CA0894141236FAB99EE5FF7DFDDC8B6,
	RenderSettings_set_fogDensity_mC82D5FB8E350FF4A45D41B360FA1FC00384FAA94,
	RenderSettings_get_ambientLight_m1879C965ADD513A1FC6732DC9BA6AC6C120B304B,
	RenderSettings_set_ambientLight_m0BD3CBB34DDA694D6459FB5A68859A4C070670D2,
	RenderSettings_get_fogColor_Injected_m70851CFD9435A4E097679ECF38615E4AA6D02BEA,
	RenderSettings_set_fogColor_Injected_m8EC222EF2B9667553BBA75981EB52CB90088A585,
	RenderSettings_get_ambientLight_Injected_mC315F9B62DA59EC4DB3039DFE1C87BADF7AFAFCB,
	RenderSettings_set_ambientLight_Injected_mBC0B3DCF653CFBA1988C9FFC7DB6D264F5BE090D,
	Shader_Find_m596EC6EBDCA8C9D5D86E2410A319928C1E8E6B5A,
	Shader_set_maximumLOD_m0E42E3AC1D63AEBC9CE42A07AFC729F424365468,
	Shader_EnableKeyword_m9AC58243D7FBD69DD73D13B73085E0B24E4ECFCE,
	Shader_DisableKeyword_mA6360809F3D85D7D0CAE3EE3C9A85987EC60DBCF,
	Shader_PropertyToID_m8C1BEBBAC0CC3015B142AF0FA856495D5D239F5F,
	Shader_SetGlobalFloatImpl_m02A206C366CB1EC84C3A579786F64397CC07EE18,
	Shader_SetGlobalFloat_mD653B388A2C2E91C323FDA07C9D9FA295738A420,
	Shader__ctor_m51131927C3747131EBA5F99732E86E5C9F176DC8,
	Material_CreateWithShader_mD4E25791C111800AB1E98BEAD7D45CE72B912E62,
	Material_CreateWithMaterial_mD2035B551DB7CFA1296B91C0CCC3475C5706EE41,
	Material_CreateWithString_m0C0F291654AFD2D5643A1A7826844F2416284F1F,
	Material__ctor_mD2A3BCD3B4F17F5C6E95F3B34DAF4B497B67127E,
	Material__ctor_mD0C3D9CFAFE0FB858D864092467387D7FA178245,
	Material__ctor_m71FEB02D66A71A0FF513ABC40339E1561A8192E0,
	Material_get_shader_mEB85A8B8CA57235C464C2CC255E77A4EFF7A6097,
	Material_set_shader_m21134B4BB30FB4978B4D583FA4A8AFF2A8A9410D,
	Material_get_color_m7926F7BE68B4D000306738C1EAABEB7ADFB97821,
	Material_set_color_mC3C88E2389B7132EBF3EB0D1F040545176B795C0,
	Material_get_mainTexture_mD1F98F8E09F68857D5408796A76A521925A04FAC,
	Material_set_mainTexture_m1F715422BE5C75B4A7AC951691F0DC16A8C294C5,
	Material_GetFirstPropertyNameIdByAttribute_m7192A1C1FCDB6F58A21C00571D2A67948E944CC0,
	Material_HasProperty_m699B4D99152E3A99733B8BD7D41EAE08BB8B1657,
	Material_HasProperty_mB6F155CD45C688DA232B56BD1A74474C224BE37E,
	Material_set_renderQueue_m239F950307B3B71DC41AF02F9030DD0A80A3A201,
	Material_EnableKeyword_mBD03896F11814C3EF67F73A414DC66D5B577171D,
	Material_DisableKeyword_mD43BE3ED8D792B7242F5487ADC074DF2A5A1BD18,
	Material_IsKeywordEnabled_m21EB58B980BA61215B281A9C18BC861BF6CF126E,
	Material_get_passCount_m8604F2400F17AC2524B95F1C4F39C785896EEE3A,
	Material_SetOverrideTag_m5786C05270AD48B5B0756D5BC367017965B880C4,
	Material_GetTagImpl_m33F2C64FC3861174F902B816B79EEC04884A18E0,
	Material_GetTag_mEAF0285CDE4B5747603C684BB900CA34DCB8B5F9,
	Material_SetPass_mC888245491A21488CFF2FD64CA45E8404CB9FF9C,
	Material_CopyPropertiesFromMaterial_m5A6DE308328EAB762EF5BE3253B728C8078773CF,
	Material_GetShaderKeywords_mC4CE7855606BC17C1CD3ABD4FC90F6E184DFE88B,
	Material_SetShaderKeywords_m73BF3A5DE699A06D214972A546644D37524D53CB,
	Material_get_shaderKeywords_mDAD743C090C3CEE0B2883140B244853D71C5E9E0,
	Material_set_shaderKeywords_m9EC5EFA52BF30597B1692C623806E7167B1C7688,
	Material_SetFloatImpl_m07966D17C660588628A2ACDBBE2DD5FE0F830F1E,
	Material_SetColorImpl_m0981AC6BCE99E2692B1859AC5A3A4334C5F52185,
	Material_SetMatrixImpl_mF2A4040AEEC3AC166441A54E0ED04B7222AFD5D8,
	Material_SetTextureImpl_m671ED2C28CE1AE0E0E94E3E7633ABFA75186317C,
	Material_SetBufferImpl_m21218ADB88A54C2923032EDD633F786C44FE8FD6,
	Material_GetFloatImpl_mB3186CDAD4244298ABA911133FB672182F465DB3,
	Material_GetColorImpl_m317B54F3AB71AEE4527C34202EF69DA52DC7D313,
	Material_GetTextureImpl_mD8BBF7EC67606802B01BF2BB55FEA981DBFFC1AE,
	Material_SetFloat_mBE01E05D49E5C7045E010F49A38E96B101D82768,
	Material_SetFloat_mAC7DC962B356565CF6743E358C7A19D0322EA060,
	Material_SetInt_m15D944E498726C9BB3A60A41DAAA45000F570F87,
	Material_SetInt_m3EB8D863E3CAE33B5EC0CB679D061CEBE174E44C,
	Material_SetColor_m5CAAF4A8D7F839597B4E14588E341462EEB81698,
	Material_SetColor_m9DE63FCC5A31918F8A9A2E4FCED70C298677A7B4,
	Material_SetVector_mCB22CD5FDA6D8C7C282D7998A9244E12CC293F0D,
	Material_SetVector_m47F7F5B5B21FA28885C4E747AF1C32F40C1022CB,
	Material_SetMatrix_m7266FA4400474D08A30181EE08E01760CCAEBA0A,
	Material_SetTexture_m04A1CD55201841F85E475992931A210229C782CF,
	Material_SetTexture_mECB29488B89AB3E516331DA41409510D570E9B60,
	Material_SetBuffer_m8E57EF9FF4D5A9C6F7C76A3C4EADC7058FFEA341,
	Material_GetFloat_mF2F48AFBDFC1E1E72A00F614EF20B656262EB167,
	Material_GetFloat_m508B992651DD512ECB2A51336C9A4E87AED82D27,
	Material_GetColor_m5B75B83FE5821381064306ECFEEF0CC44BE66688,
	Material_GetColor_m87CBA0F1030841DE18DED76EA658006A86060EA7,
	Material_GetVector_m0E41ED876B69FCFC4B9EA715D0286EE714CD201F,
	Material_GetVector_m0F76C999BC936C571A3C20054D266DF122A85E88,
	Material_GetTexture_m559F9134FDF1311F3D39B8C22A90A50A9F80A5FB,
	Material_GetTexture_m02A9C3BA6C1396C0F1AAA4C248B9A81D7ABED680,
	Material_SetColorImpl_Injected_mB1B35D7949FB31533E2DF99F5A0C5DC3B798EC39,
	Material_SetMatrixImpl_Injected_m15ABD6B185339B78F89DE924539ABB632DEEC7D8,
	Material_GetColorImpl_Injected_mFF3241EC1855296376026105A435E5B394D5AF38,
	Light_get_type_mDBBEC33D93952330EED5B02B15865C59D5C355A0,
	Light_set_type_mD68B010AE47ECDDF9C13D9F99F13EB69E9A9F7F4,
	Light_get_spotAngle_m7BFB3B329103477AFFBB9F9E059718AB212D5FD7,
	Light_get_color_mB587B97487FFA7F7E0415F270283E48D2D901F4B,
	Light_set_color_mB33E961A7CF25D0EBE410EE22444B4B8D4317C6C,
	Light_get_intensity_mFABC9E1EA23E954E1072233C33C2211D64262326,
	Light_set_intensity_m372D5B9494809AFAD717B2707957DD1478C52DFC,
	Light_get_bounceIntensity_m6B586C8D305CE352E537E4AC8E8F957E512C4D50,
	Light_get_range_m94D58A8FE80BC5B13571D9CC8EBF88336BA0F831,
	Light_set_range_mEA722B268DA5244F52B50D44FFCC1354E8FC99B4,
	Light_get_bakingOutput_m3696BB20EFCAFCB3CB579E74A5FE00EAA0E71CB5,
	Light_set_cullingMask_m4C8DE4FCAA45EBDC4F5A29639D87A4B8BA5AF717,
	Light_get_shadows_mE77B8235C26E28A797CDDF283D167EE034226AD5,
	Light_set_shadows_mC464AA4717DB55512DC06B3DFB95C8839F16C20B,
	Light_set_shadowStrength_mB3632DDAD7DDDC29FEA4D3966201340C8846AC2D,
	Light_get_cookieSize_mE1168D491F8BC5DB1BA10D6E9C3B39A46177DF2B,
	Light_get_cookie_mC164223C67736F4E027DC020685D4C6D24E5A705,
	Light_get_color_Injected_mFC80DFA291AB496FAE0BC39E82460F6653B3362D,
	Light_set_color_Injected_mD9A13B1B67C6B666BBCD27997B052C544018FD8A,
	Light_get_bakingOutput_Injected_m7B026203BB40826D50299070138CF8F6A3519DED,
	Skybox_get_material_m9AAE5143C39D1E5E13CD41BE53E950B56E5AAD85,
	Skybox_set_material_m74316F6709A669D3181C4CB1EA1F40E745D0F88A,
	MeshFilter_DontStripMeshFilter_m8982FEABBD1847BE8B3E53E9DD2A15FBC7370DE2,
	MeshFilter_get_sharedMesh_mDCB12AB93E6E5F477F55A14990A7AB5F1B06F19E,
	MeshFilter_set_sharedMesh_mC96D5F9AE4BC1D186221F568A4C3CE23572EC958,
	MeshFilter_get_mesh_mFC1DF5AFBC1E4269D08628DB83C954882FD2B417,
	MeshFilter_set_mesh_m13177C1A6C29D76DDCD858CEF2B28C2AA7CC46FC,
	MeshRenderer_DontStripMeshRenderer_m68A34690B98E3BF30C620117C323B48A31DE512F,
	Mesh_Internal_Create_m6802A5B262F48CF3D72E58C2C234EF063C2552B7,
	Mesh__ctor_mA3D8570373462201AD7B8C9586A7F9412E49C2F6,
	Mesh_GetTrianglesImpl_m5876580E004D6131F9D4635B640356171F624A05,
	Mesh_GetIndicesImpl_m4438ED268047265769FBD7D73F942C13293ECF9E,
	Mesh_SetIndicesImpl_mD313F66DDE73C2497530789D9EF7E2EEC9633479,
	Mesh_PrintErrorCantAccessChannel_m39BB0FADC48525EAE52AA38AC8D7EE5BA650294C,
	Mesh_HasVertexAttribute_m55371DBBBA8C77FBF6404F0D7989C0C7BDE3275C,
	Mesh_SetArrayForChannelImpl_mBE2748A89C312EE0F77BBD88F086EB421AA64952,
	Mesh_GetAllocArrayFromChannelImpl_m9EC298F950FDC7F699CB02A265AAE1E1E580B541,
	Mesh_get_canAccess_m991B64F0FA651459A7E0DD4526D7EF0384F1792F,
	Mesh_get_vertexCount_m1EF3DD16EE298B955311F53EA1CAF05007A7722F,
	Mesh_get_subMeshCount_m60E2BCBFEEF21260C70D06EAEC3A2A51D80796FF,
	Mesh_set_subMeshCount_mF6F2199AE4FA096C1AE0CAD02E13B6FEA38C6283,
	Mesh_get_bounds_m8704A23E8BA2D77C89FD4BF4379238062B1BE5DF,
	Mesh_set_bounds_m9752E145EA6D719B417AA27555DDC2A388AB4E0A,
	Mesh_ClearImpl_mD00C840FA60B68829F9D315B6888D60D0E3EB9E5,
	Mesh_RecalculateBoundsImpl_mB8C82BD506A5D075562538AE30E1569BA2DFA373,
	Mesh_RecalculateNormalsImpl_m9810DED3E3F7F230F0649CBB207110D1B59E24FC,
	Mesh_MarkDynamicImpl_m58203C57ECC338CBE9FD4561A48515386C7A0174,
	Mesh_UploadMeshDataImpl_mEE786AC05A4FAFA95782AB8D08BAA95243B3146E,
	Mesh_GetUVChannel_m9566A8802F5B87D061A2812FEF94230F8EA1CBBF,
	Mesh_DefaultDimensionForChannel_m95062483A5D77AC517FE0F87EC41250CFDDEF8FD,
	NULL,
	NULL,
	Mesh_SetSizedArrayForChannel_m4E03A6A18D0C5BB49E89828AE7A0DD34BB20E7CC,
	NULL,
	NULL,
	NULL,
	NULL,
	Mesh_get_vertices_mB7A79698792B3CBA0E7E6EACDA6C031E496FB595,
	Mesh_set_vertices_m38F0908D0FDFE484BE19E94BE9D6176667469AAD,
	Mesh_get_normals_m5212279CEF7538618C8BA884C9A7B976B32352B0,
	Mesh_set_normals_m3D06E214B63B49788710672B71C99F2365A83130,
	Mesh_get_tangents_m278A41721D47A627367F3F8E2B722B80A949A0F3,
	Mesh_set_tangents_mFA4E0A26B52C1FCF80FA5DA642B28716249ACF67,
	Mesh_get_uv_m3FF0C231402D4106CDA3EEE381B16863B287D143,
	Mesh_set_uv_mF6FED6DDACBAE3EAF28BFBF257A0D5356FCF3AAC,
	Mesh_get_uv2_m03E41FCB77DA4290C9CA70E11E9C0125F68D505E,
	Mesh_set_uv2_mE60F42676D5CD294524617262BABEB81B9FB8F22,
	Mesh_get_uv3_m59B9F0B319EEE7CCED6A15D3AF8A4C40C7280DF6,
	Mesh_get_uv4_m5177A36A234F21CA1CEE7B30051B47B2FCB4A4A3,
	Mesh_get_colors32_m4BD048545AD6BC19E982926AB0C8A1948A82AD32,
	Mesh_set_colors32_m0EA838C15BBD01710A4EE1A475DB585EB9B5BD91,
	Mesh_SetVertices_m08C90A1665735C09E15E17DE1A8CD9F196762BCD,
	Mesh_SetVertices_m0603941E5ACFAEC45C95FE8658C41E196BE8164E,
	Mesh_SetVertices_m26E8F8BCF660FBCFFC512FF683A7FBB8FEA32214,
	Mesh_SetNormals_m10B6C93B59F4BC8F5D959CD79494F3FCDB67B168,
	Mesh_SetNormals_m8E810FA8F3EF65B047AE7C11B8E428FE9F1250BD,
	Mesh_SetNormals_mF37082B29FFB07FB5FCBDD842C01620213924E53,
	Mesh_SetTangents_m728C5E61FD6656209686AE3F6734686A2F4E549E,
	Mesh_SetTangents_m273D26B0AB58BD29EEAE0CA31D497430A8873D66,
	Mesh_SetTangents_mC172BCA3C194ADBF1B565DEC54CD884EB63B1D8C,
	Mesh_SetColors_m3A1D5B4986EC06E3930617D45A88BA768072FA2F,
	Mesh_SetColors_m1A9D32B21B621A3C246BEBCD842127123718303A,
	Mesh_SetColors_mF89C9599491ACBAEBB2F3C8D92A8192B3F9B55CF,
	NULL,
	Mesh_SetUVs_mDDD16F3EC3434233E34F18221FFD47F8AEA7BCCE,
	Mesh_SetUVs_m5CE63A380341B88A588D73B4240A2F79B3A1776F,
	Mesh_SetUVs_mC431E98259699C3524EEEF58E491F3053ECE14F3,
	Mesh_PrintErrorCantAccessIndices_m2416235DD4E4CB1E5B5124480185157C9599A2B6,
	Mesh_CheckCanAccessSubmesh_m639D3AFE8E9A6A8D6113897D6628BCB8D9851470,
	Mesh_CheckCanAccessSubmeshTriangles_m3FA6D53E009E173066F08968A34C810A968CD354,
	Mesh_CheckCanAccessSubmeshIndices_m4C72E5C166B623591E599D0A5E93A7BF44E6DD52,
	Mesh_get_triangles_mC599119151146317136B1E4C40A9110373286D5A,
	Mesh_set_triangles_mF1D92E67523CD5FDC66A4378FC4AD8D4AD0D5FEC,
	Mesh_GetIndices_m8C8D25ABFA9D8A7AE23DAEB6FD7142E6BB46C49D,
	Mesh_GetIndices_m716824BAD490CC3818631A2A4476DD174EF995C9,
	Mesh_CheckIndicesArrayRange_mEB6E1B2EE3E56B1D802F0C7EF2A7A9D15E32F5D6,
	Mesh_SetTrianglesImpl_m996CAEDB2B197F72F41C865759FD21C3E9AB6964,
	Mesh_SetTriangles_m63AC07691EC9F9AE0C85FB01A8DDD1A45FE9349F,
	Mesh_SetTriangles_m2097A43D5983A1A17C49BA59127EA59744B58541,
	Mesh_SetTriangles_m5D89367A7503882DED6AE15E5D2D665DC08D82D3,
	Mesh_SetTriangles_mF74536E3A39AECF33809A7D23AFF54A1CFC37129,
	Mesh_SetTriangles_m9DA501E911C965F2FFFAF44F8956222E86DCB71E,
	Mesh_SetTriangles_m8F0B711A6E0FFCB6DE06943F24D12019B3100EB4,
	Mesh_SetIndices_mCD0377083E978A3FF806CFCCD28410C042A77ECD,
	Mesh_SetIndices_m6269D5A9E751E7DFA21248CAD177270989224770,
	Mesh_SetIndices_m87CA046F5A408FAE35E6C2645C86683D91D5D725,
	Mesh_Clear_mD35FF3850B83B635DA849033E25D0D718E34D92B,
	Mesh_Clear_m7500ECE6209E14CC750CB16B48301B8D2A57ACCE,
	Mesh_RecalculateBounds_mC39556595CFE3E4D8EFA777476ECD22B97FC2737,
	Mesh_RecalculateNormals_mEBF9ED932D0B463E4EF3947D232CC8BEECAE1A4A,
	Mesh_RecalculateBounds_m30A0A5900837569C4016A1FE2CEC0BB3E50CC43A,
	Mesh_RecalculateNormals_mACA7E88F9EE957C64005597F5E83F8EFAFA7A8D9,
	Mesh_MarkDynamic_m11FFDC281C64F11C36EDDA47BC132EAC95082999,
	Mesh_UploadMeshData_m3745185BFF4D9B970DEB23EEB6FD93DCAFFA8C07,
	Mesh_get_bounds_Injected_m1EFC3F0977E82DD74B76F15661C562EE48695140,
	Mesh_set_bounds_Injected_m4DE5BB71883B1FC4CBD3299AF52FB299219FA6B7,
	Texture__ctor_mA6FE9CC0AF05A99FADCEF0BED2FB0C95571AAF4A,
	Texture_get_mipmapCount_mC54E39023B16EDF24FF5635CFC1D2C88507660AB,
	Texture_GetDataWidth_m5EE88F5417E01649909C3E11408491DB88AA9442,
	Texture_GetDataHeight_m1DFF41FBC7542D2CDB0247CF02A0FE0ACB60FB99,
	Texture_GetDimension_mFC3578580420BC64C4A877E0F77CBD3868B03668,
	Texture_get_width_m98E7185116DB24A73E1647878013B023FF275B98,
	Texture_set_width_m6BCD23D97A9883DE0FB34E6FF48883F6C09D9F8F,
	Texture_get_height_m3D849F551F396027D4483C9B9FFF31F8AF4533B6,
	Texture_set_height_mAC3CA245CB260972C0537919C451DBF3BA1A4554,
	Texture_get_dimension_m32A86E516E1C47335027FF586B54DE992EF69EFD,
	Texture_get_isReadable_mF9C36F23F3632802946D4BCBA6FE3D27098407BC,
	Texture_get_wrapMode_mB442135F226C399108A5805A6B82845EC0362BA9,
	Texture_set_wrapMode_m1233D2DF48DC20996F8EE26E866D4BDD2AC8050D,
	Texture_get_filterMode_m66AAEC6C44B0A3A82477A27E5061AF6008501F9E,
	Texture_set_filterMode_m045141DB0FEFE496885D45F5F23B15BC0E77C8D0,
	Texture_set_anisoLevel_mE51360F6CD0562FD6355F8C0509B70A454CB33BE,
	Texture_set_mipMapBias_m5057ADA9DBA8E8E30EC15EA5BABFA0734CE2821C,
	Texture_get_texelSize_m804B471337C8AF2334FF12FA2CC6198EFD7EB5EB,
	Texture_Internal_GetActiveTextureColorSpace_m5D0FE0578B76D37F863DB9FDC8BD0608467EE59D,
	Texture_get_activeTextureColorSpace_m0553908E0813E6ABD035A2453AA073BADB5680F2,
	Texture_ValidateFormat_mC3C7A3FE51CA18357ABE027958BF97D3C6675D39,
	Texture_ValidateFormat_mB721DB544C78FC025FC3D3F85AD705D54F1B52CE,
	Texture_CreateNonReadableException_m5BFE30599C50688EEDE149FB1CEF834BE1633306,
	Texture__cctor_m6474E45E076B9A176073F458843BAC631033F2B7,
	Texture_get_texelSize_Injected_mE4C2F32E9803126870079BDF7EB701CDD19910E2,
	Texture2D_get_format_mCBCE13524A94042693822BDDE112990B25F4F8E4,
	Texture2D_get_whiteTexture_m4ED96995BA1D42F7D2823BD9D18023CFE3C680A0,
	Texture2D_Compress_m9EEAE939AF8538D56FA39D03FAA5AD6DA7EC5D60,
	Texture2D_Internal_CreateImpl_m48CD1B0F76E8671515956DFA43329604E29EB7B3,
	Texture2D_Internal_Create_mEAA34D0081C646C185D322FDE203E5C6C7B05800,
	Texture2D_get_isReadable_mD31C50788F7268E65EE9DA611B6F66199AA9D109,
	Texture2D_ApplyImpl_mC56607643B71223E3294F6BA352A5538FCC5915C,
	Texture2D_ResizeImpl_m484CD2126E70AA80396A6F7C1539A72CC03BA71C,
	Texture2D_SetPixelImpl_m9790950013B3DF46008381D971548B82C0378D91,
	Texture2D_GetPixelImpl_m2224B987F48D881F71083B9472DE5DD11580977B,
	Texture2D_GetPixelBilinearImpl_m688F5C550710DA1B1ECBE38C1354B0A15C89778E,
	Texture2D_ResizeWithFormatImpl_m0A2C1EDF44282998B107D541509BD196AEBF65C6,
	Texture2D_ReadPixelsImpl_m8C1FDBC0E9EA531BF107C9A60F931B15910BE260,
	Texture2D_SetPixelsImpl_m6F5B06278B956BFCCF360B135F73B19D4648AE92,
	Texture2D_SetAllPixels32_mC1C1E76040E72CAFB60D3CA3F2B95A92620F4A46,
	Texture2D_SetBlockOfPixels32_mFCEE9885A7D2E79C14F4840AF7FF03E9AACF468B,
	Texture2D_GetPixels_m63492D1225FC7E937BBF236538510E29B5866BEB,
	Texture2D_GetPixels_m677BB0ECBC49B9BAC6C3AE78A916AFF126C62CD3,
	Texture2D_GetPixels32_mA4E2C09B4077716ECEFC0162ABEB8C3A66F623FA,
	Texture2D_GetPixels32_m419F7BF2D2D374C08247BE66838148DA485A6ECA,
	Texture2D_PackTextures_m7946134DE42C42397876C7A67BA9FF755030DA1E,
	Texture2D_PackTextures_m77163B065DB65C86DD0DC579D809EB3B67784791,
	Texture2D__ctor_mF706AD5FFC4EC2805E746C80630D1255A8867004,
	Texture2D__ctor_m667452FB4794C77D283037E096FE0DC0AEB311F3,
	Texture2D__ctor_mF138386223A07CBD4CE94672757E39D0EF718092,
	Texture2D__ctor_m7D64AB4C55A01F2EE57483FD9EF826607DF9E4B4,
	Texture2D_SetPixel_m78878905E58C5DE9BCFED8D9262D025789E22F92,
	Texture2D_SetPixels_m39DFC67A52779E657C4B3AFA69FC586E2DB6AB50,
	Texture2D_SetPixels_m802BA835119C0F93478BBA752BA23192013EA4F7,
	Texture2D_SetPixels_m5FBA81041D65F8641C3107195D390EE65467FB4F,
	Texture2D_GetPixel_m50474A401DE4CB3B567F1695546DF1D2C610A022,
	Texture2D_GetPixelBilinear_mE25550DD7E9FD26DA7CB1E38FFCA2101F9D3D28D,
	Texture2D_Apply_m83460E7B5610A6D85DD3CCA71CC5D4523390D660,
	Texture2D_Apply_mA7D80A8D5DBA5A9334508F23EAEFC6E9C7019CB6,
	Texture2D_Apply_m3BB3975288119BA62ED9BE4243F7767EC2F88CA0,
	Texture2D_Resize_m3B472A6ED37D683DC4162504F6DCF42E1FA2195C,
	Texture2D_Resize_m051609799A8F11F4E344C099DC81A50B69528A6B,
	Texture2D_ReadPixels_m87ACCC9FDCD8FC8851AE8D3BE56A7C2CAF09C75E,
	Texture2D_ReadPixels_m4C6FE8C2798C39C20A14DAFC963C720D17F4F987,
	Texture2D_SetPixels32_mBDD42381EB43E024214D81792B0A96D952911D4F,
	Texture2D_SetPixels32_m6C2602EBE75F9C70DBC36D0B67EA4C12638518BB,
	Texture2D_SetPixels32_m5C3B90CC4B9E104C5E278176E119945C353852D9,
	Texture2D_SetPixels32_m80C3D047272131066BA3B918855264376D2D1407,
	Texture2D_GetPixels_mDBE68956E50997CB02CB0419318E0D19493288A6,
	Texture2D_GetPixels_m702E1E59DE60A5A11197DA3F6474F9E6716D9699,
	Texture2D_SetPixelImpl_Injected_mF0EB1B200D5BB3B2BC3BB50C708901785C1E1772,
	Texture2D_GetPixelImpl_Injected_mFD898C97E14FC52C38BA6B42BE88C762F6BB2082,
	Texture2D_GetPixelBilinearImpl_Injected_m378D7A9BC9E6079B59950C664419E04FB1E894FE,
	Texture2D_ReadPixelsImpl_Injected_mEC297796C9AA8F8A08551EEDBB144C95EA3F560C,
	Cubemap_Internal_CreateImpl_m05F9BCB0CFD280E2121062B6F8DA9467789B27A2,
	Cubemap_Internal_Create_m1647AD0EFBE0E383F5CDF6ABBC51842959E61931,
	Cubemap_get_isReadable_m169779CDA9E9AF98599E60A47C665E68B3256F5D,
	Cubemap__ctor_mBC1AD85DB40124D557615D0B391CACCDF76F1579,
	Cubemap__ctor_mEC3B9661FA3DB1DFF54C7A3F4FEA41903EFE2C11,
	Cubemap__ctor_mDEB5F10F08868EEBBF7891F00EAB793F4C5498FB,
	Cubemap__ctor_mF667EBD2A38E2D5DDBD46BC80ABF2DCE97A9411B,
	Cubemap__ctor_mD760725AC038C20E54F8EC514DA075DFF97A8B56,
	Cubemap__ctor_m766D71B1731BAD5C01FAEA35A73D1980C6CAE57B,
	Cubemap__ctor_m3F121EC86FF615007F0BB6FD8162779EE7D19037,
	Texture3D_get_isReadable_mFE7B549E8E368B00CEAB4A297106AB135FA6E962,
	Texture3D_Internal_CreateImpl_m41E3DE82535685BDE643C82AB65722CCB98E7C20,
	Texture3D_Internal_Create_m65B50261584E685D8375F5398311C549271ED1B6,
	Texture3D_ApplyImpl_m53DBD3A0ACF34C4300F3200466F843F4E2331100,
	Texture3D_SetPixels32_m7BC6152A23FBF8D2D1D79FED26E76B4530404E00,
	Texture3D_SetPixels32_mEA63C4C5FA84F5CA5D7635DF9CDD987946088770,
	Texture3D__ctor_mA422DEB7F88AA34806E6AA2D91258AA093F3C3AE,
	Texture3D__ctor_m666A8D01B0E3B7773C7CDAB624D69E16331CFA36,
	Texture3D__ctor_m6DC8372EBD1146127A4CE86F3E65930ABAB6539D,
	Texture3D__ctor_m7AE9A6F7E67FE89DEA087647FB3375343D997F4C,
	Texture3D__ctor_m2B875ADAA935AC50C758ECEBA69F13172FD620FC,
	Texture3D_Apply_mA27E8CD29F3236BE67DA1E35298966976F3B27A5,
	Texture3D_Apply_m83FB1F0D98CA327EF6263CF616BB05BBA0D45860,
	Texture2DArray_get_allSlices_mB49C66E3148B0002D15D4E6E908B1FD600263FDA,
	Texture2DArray_get_depth_mE0A5F5C0CCA63B07B290EC0BA645A52C4C8FD272,
	Texture2DArray_get_format_m85F541FCF79D18F05F77DD792904F4D101706829,
	Texture2DArray_get_isReadable_m7676C4021DD3D435A3D2655A34C7A1FCCA00C042,
	Texture2DArray_Internal_CreateImpl_m5B8B806393D443E6F0CB49AB019C8E9A1C8644B1,
	Texture2DArray_Internal_Create_m5DD4264F3965FBE126FAA447C79876C22D36D39C,
	Texture2DArray_ApplyImpl_m7D9288584A5198386367A6158D05DCEEC3EC5D10,
	Texture2DArray_SetPixels32_mC7955F5E0B35805E7C805449582D4C59238674F6,
	Texture2DArray__ctor_mAE2D5B259CE352E6F4F10A28FDDCE52B771672B2,
	Texture2DArray__ctor_m139056CD509EAC819F9713F6A2CAE801D49CA13F,
	Texture2DArray__ctor_mB19D8E34783F95713A23A0F06F63EF1B1613E9C5,
	Texture2DArray__ctor_mEE6D4AD1D7469894FA16627A222EDC34647F6DB3,
	Texture2DArray__ctor_m4772A79C577E6E246301F31D86FE6F150B1B68E2,
	Texture2DArray__ctor_mED6E22E57F51628D68F219E5C01FF01A265CE386,
	Texture2DArray_Apply_mA7130B96298181F5B22A93692509D7BF1A575C2F,
	CubemapArray_get_isReadable_m8948D737E2D417F489BCFF3C5CA87B4D536A8F44,
	CubemapArray_Internal_CreateImpl_mC6544EE7BDDE76EC9B3B8703CB13B08497921994,
	CubemapArray_Internal_Create_mC44055052CD006718B5C1964110B692B30DE1F1F,
	CubemapArray__ctor_mAEA6A6E06CDE3F825976EFA242AAE00AE41C0247,
	CubemapArray__ctor_mB5CEC4BD06765D072E96B663B4E9E09E0DCCEE17,
	CubemapArray__ctor_mD891BB1565BECEEEDFB5F12EE3C64C34A8A93B78,
	CubemapArray__ctor_m696D238938D8A70B273DE5D05F60C8C4834506D8,
	CubemapArray__ctor_m593EF9F31E1FC6357957584ACD550B434D4C9563,
	CubemapArray__ctor_mCEB65D7A82E8C98530D970424F4B125E35A03205,
	RenderTexture_get_width_m7E2915C08E3B59DE86707FAF9990A73AD62609BA,
	RenderTexture_set_width_m24E7C6AD852FA9DB089253755A450E1FC53F5CC5,
	RenderTexture_get_height_m2A29272DA6BAFE2051A228B15E3BC4AECD97473D,
	RenderTexture_set_height_m131ECC892E6EA0CEA1E656C66862A272FF6F0376,
	RenderTexture_get_dimension_m597D6E558F9DE585F58AFA983A4AB64421949BF3,
	RenderTexture_get_graphicsFormat_m1D7330403A886C0F7F43A0C82CFECFDC56DC6937,
	RenderTexture_set_graphicsFormat_mA378AAD8BD876EE96637FF0A80A2AFEA4D852FAB,
	RenderTexture_get_sRGB_m53EE975EDE763A0CC5B2CF5B347D5FC6E1FE3B21,
	RenderTexture_get_format_mB9BBCACA0A809206FA73109ACF2A6976E19DB822,
	RenderTexture_set_isPowerOfTwo_mA6D34F610DF27C40422DF5815892B45C656F1317,
	RenderTexture_GetActive_m198991C4EAA3A72357ABF3B16C677A4CB0995FFF,
	RenderTexture_SetActive_m044CDBE8E6AF70D0B7E03BCC814C02CFDF82FFC0,
	RenderTexture_get_active_mB73718A56673D36F74B5338B310ED7FDFEB34AB7,
	RenderTexture_set_active_mA70AFD6D3CB54E9AEDDD45E48B8B6979FDB75ED9,
	RenderTexture_GetColorBuffer_mEA9045FD8EA98B51E7AF4B313B4A82A26EDA68DE,
	RenderTexture_GetDepthBuffer_m49B541537CA0852FFA3337E9012A2108EC3E22CF,
	RenderTexture_get_colorBuffer_mB973BA4FAA3141D51E26D15473F5E6F8D2046675,
	RenderTexture_get_depthBuffer_m474407A8697D75FAA2CD2D927DEB7F593C491A04,
	RenderTexture_Create_m723CBB7B7543E9FAFEBC04E8FDCDF629DA31F411,
	RenderTexture_Release_m533506E903688E798921C0D35F1B082522D88986,
	RenderTexture_IsCreated_m78F28FE94FDA5346E2E8B3AEC0863B8DCF116958,
	RenderTexture_SetSRGBReadWrite_m1C0BEC5511CE36A91F44E1C40AEF2399BA347D14,
	RenderTexture_Internal_Create_m6374BF6C59B7A2307975D6D1A70C82859EF0D8F3,
	RenderTexture_SetRenderTextureDescriptor_mD39CEA1EAF6810889EDB9D5CE08A84F14706F499,
	RenderTexture_GetDescriptor_mC6D87F96283042B76AA07994AC73E8131FA65F79,
	RenderTexture_GetTemporary_Internal_mF3F0B64948F5A615FFF8DD9A3263A2616B9B1D29,
	RenderTexture_ReleaseTemporary_m2BF2BDDC359A491C05C401B977878DAE1D0850D4,
	RenderTexture_get_depth_m1E64CBB551FF92C496FDE4ED9A11A3937F93BC29,
	RenderTexture_set_depth_mA57CEFEDDB45D0429FAC9532A631839F523944A3,
	RenderTexture__ctor_m41C9973A79AF4CAD32E6C8987874BB20C9C87DF7,
	RenderTexture__ctor_m96C4C4C7B41EE884420046EFE4B8EC528B10D8BD,
	RenderTexture__ctor_m26C29617F265AAA52563A260A5D2EDAAC22CA1C5,
	RenderTexture__ctor_mE4898D07FB66535165C92D4AA6E37DAC7FF57D50,
	RenderTexture__ctor_mBE300C716D0DD565F63442E58077515EC82E7BA8,
	RenderTexture__ctor_mBE459F2C0FB9B65A5201F7C646C7EC653408A3D6,
	RenderTexture__ctor_m2C35C7AD5162A6CFB7F6CF638B2DAC0DDC9282FD,
	RenderTexture__ctor_m8E4220FDA652BA3CACE60FBA59D868B921C0F533,
	RenderTexture__ctor_m5D8D36B284951F95A048C6B9ACA24FC7509564FF,
	RenderTexture__ctor_m262905210EC882BA3F8B34B322848879561240F6,
	RenderTexture_get_descriptor_mBD2530599DF6A24EB0C8F502718B862FC4BF1B9E,
	RenderTexture_set_descriptor_m3C8E31AE4644B23719A12345771D1B85EB6E5881,
	RenderTexture_ValidateRenderTextureDesc_m5D363CF342A8C617A326B982D209893F76E30404,
	RenderTexture_GetCompatibleFormat_m21C46AD608AAA27D85641330E6F273AEF566FFB7,
	RenderTexture_GetTemporary_m7997BAA9A1DFE4D9D1B9F5047ECEE8464835B8DF,
	RenderTexture_GetTemporaryImpl_mEA42AB2F6F4F97F0ECA712E82CB2C6C05A515A58,
	RenderTexture_GetTemporary_mFDF23E91A85221C7EF61B0A5D46AAC858816E5F1,
	RenderTexture_GetTemporary_mEA38E780ED9C1E065B97E85BBC63F8DE548C7B8E,
	RenderTexture_GetColorBuffer_Injected_m2A29CF16C78914B733AEE4C7A4E71ED236FEE0DC,
	RenderTexture_GetDepthBuffer_Injected_mCA59890646B93E9D30E98708EAADE7444DAD2FF1,
	RenderTexture_SetRenderTextureDescriptor_Injected_m37024A53E72A10E7F192E51100E2224AA7D0169A,
	RenderTexture_GetDescriptor_Injected_mF6F57BE0A174E81000F35E1E46A38311B661C2A3,
	RenderTexture_GetTemporary_Internal_Injected_m22A0AFBD90D2E2C8E4058A662EFA3ECC965FAF3C,
	RenderTextureDescriptor_get_width_m5DD56A0652453FDDB51FF030FC5ED914F83F5E31_AdjustorThunk,
	RenderTextureDescriptor_set_width_m8D4BAEBB8089FD77F4DC81088ACB511F2BCA41EA_AdjustorThunk,
	RenderTextureDescriptor_get_height_m661881AD8E078D6C1FD6C549207AACC2B179D201_AdjustorThunk,
	RenderTextureDescriptor_set_height_m1300AF31BCDCF2E14E86A598AFDC5569B682A46D_AdjustorThunk,
	RenderTextureDescriptor_get_msaaSamples_m332912610A1FF2B7C05B0BA9939D733F2E7F0646_AdjustorThunk,
	RenderTextureDescriptor_set_msaaSamples_m84320452D8BF3A8DD5662F6229FE666C299B5AEF_AdjustorThunk,
	RenderTextureDescriptor_get_volumeDepth_m05E4A20A05286909E65D394D0BA5F6904D653688_AdjustorThunk,
	RenderTextureDescriptor_set_volumeDepth_mC4D9C6B86B6799BA752855DE5C385CC24F6E3733_AdjustorThunk,
	RenderTextureDescriptor_set_mipCount_mE713137D106256F44EF3E7B7CF33D5F146874659_AdjustorThunk,
	RenderTextureDescriptor_get_graphicsFormat_m9D77E42E017808FE3181673152A69CBC9A9B8B85_AdjustorThunk,
	RenderTextureDescriptor_set_graphicsFormat_m946B6FE4422E8CD33EB13ADAFDB53669EBD361C4_AdjustorThunk,
	RenderTextureDescriptor_get_depthBufferBits_m92A95D5A1DCA7B844B3AC81AADCDFDD37D26333C_AdjustorThunk,
	RenderTextureDescriptor_set_depthBufferBits_m68BF4BF942828FF70442841A22D356E5D17BCF85_AdjustorThunk,
	RenderTextureDescriptor_set_dimension_m4D3F1486F761F3C52308F00267B918BD7DB8137F_AdjustorThunk,
	RenderTextureDescriptor_set_shadowSamplingMode_m92B77BB68CC465F38790F5865A7402C5DE77B8D1_AdjustorThunk,
	RenderTextureDescriptor_set_vrUsage_m5E4F43CB35EF142D55AC22996B641483566A2097_AdjustorThunk,
	RenderTextureDescriptor_set_memoryless_m6C34CD3938C6C92F98227E3864E665026C50BCE3_AdjustorThunk,
	RenderTextureDescriptor__ctor_m25B4507361143C0DCCD40AC1D9B3D57F36DC83BE_AdjustorThunk,
	RenderTextureDescriptor__ctor_m320C821459C7856A088415334267C2963B270A9D_AdjustorThunk,
	RenderTextureDescriptor_SetOrClearRenderTextureCreationFlag_m33FD234885342E9D0C6450C90C0F2E1B6B6A1044_AdjustorThunk,
	RenderTextureDescriptor_set_createdFromScript_mA73F313083556517070DB861089F9C8BD7C3FE31_AdjustorThunk,
	RenderTextureDescriptor_set_useDynamicScale_mCD89818EC0E56C34DD71023F8C1821A5299621B4_AdjustorThunk,
	RenderTextureDescriptor__cctor_mD4015195DE93496CA03515BD76A118751F6CB88F,
	Hash128_CompareTo_m577A27A07658268AD07B09F4FFFF1D3216AD963B_AdjustorThunk,
	Hash128_ToString_mE6E0973B9B42A6AB9BEB8ACC679291CDAD2D03AC_AdjustorThunk,
	Hash128_Parse_m717F9D24F3C0537AE4CE0534A8CEC34D721C1A89,
	Hash128_Hash128ToStringImpl_mB227F7E22C9558FDDE51A8E9D839FFB708F331FE,
	Hash128_ComputeFromString_mAB2F22A4CD167B729C3A7CE56ACAC250C06FDF02,
	Hash128_Compute_m6A62DCDE72D17F89013487150F0D7B469AEDDC6A,
	Hash128_Equals_m2FEA62200ECEC6BA066924F3153C9FBA96B0E3FF_AdjustorThunk,
	Hash128_Equals_mA77DD31AF975A04BF4BD9D1F5B2A143497C9F468_AdjustorThunk,
	Hash128_GetHashCode_mBEB470B9988886E4EB3FDA22903EBB699D1B7EA6_AdjustorThunk,
	Hash128_CompareTo_m28ACD34C28C044C2BEF2109446DAAEB53F4EC619_AdjustorThunk,
	Hash128_op_Equality_m721DA198FB723CC694EE07A75766F1E173D5CD4F,
	Hash128_op_LessThan_m2BD510597CBD5898BF81AFBAB25D10AA696AFBE5,
	Hash128_op_GreaterThan_m1F45666EBE8039F9187FF344CB86D0D8D0E367BB,
	Hash128_Parse_Injected_m0DB13A1F604BB3FC73E276279E0AC33F14531FEB,
	Hash128_Hash128ToStringImpl_Injected_mDC1801343943BC014300C43AE3EC444D18A19F98,
	Cursor_SetCursor_m7EBC8C73DB36A950EFA4AC7DA1EC4E8CD10867FC,
	Cursor_set_visible_m4747F0DC20D06D1932EC740C5CCC738C1664903D,
	Cursor_get_lockState_mCE4888D80E92560908B4779FA38754B3864700C3,
	Cursor_set_lockState_mC0739186A04F4C278F02E8C1714D99B491E3A217,
	Cursor_SetCursor_Injected_mE467159BD4155ADD3D62DF6ACE36DF51C707EF93,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Logger__ctor_m01E91C7EFD28E110D491C1A6F316E5DD32616DE1,
	Logger_get_logHandler_mE3531B52B745AAF6D304ED9CC5AC5D7BAF7E2024,
	Logger_set_logHandler_m07110CE12B6DC759DB4292CF11B0CC2288DA4114,
	Logger_get_logEnabled_m12171AB0161FEDC83121C7A7ABA52AA3609F2D1F,
	Logger_set_logEnabled_mE7274CE2DFF3669A88486479F7E2ED3DE208AA07,
	Logger_get_filterLogType_mCD8726167BE9731AF85A23FE65AAFAD9353AE8A4,
	Logger_set_filterLogType_m5AFFB4C91E331F17DBEF4B85232EE07F73C040A2,
	Logger_IsLogTypeAllowed_m1AB436520161E88D0A7DDEF6F955BB88BE47A278,
	Logger_GetString_mDC6359E20D3C69C29FAE80797B7CA0340506BA7B,
	Logger_Log_mBAF75BD87C8B66198F52DEFF72132C42CA369881,
	Logger_Log_mD84CAE986DDEB614141DEDBDD023F7EB2EA511E7,
	Logger_LogFormat_mD6C153D96E0A869D48B2866E4D72D76A3E7CA2EF,
	Logger_LogFormat_m7D6FBEBB9C9708A50311878D7AF5A96E6E9A11F4,
	Logger_LogException_m207DC0A45A598148B848CF37BE3A20E6C3BB10F1,
	UnityLogWriter_WriteStringToUnityLog_m7DF2A8AB78591F20C87B8947A22D2C845F207A20,
	UnityLogWriter_WriteStringToUnityLogImpl_mBD8544A13E44C89FF1BCBD8685EDB0D1E760487F,
	UnityLogWriter_Init_m84D1792BF114717225B36DD1AA45DC1201BA77FE,
	UnityLogWriter_get_Encoding_m6CFE536A5740F8E3F6547C4B89F40C49E5AEAE33,
	UnityLogWriter_Write_m26CB2B40367CCA97725387637F0457998DED9230,
	UnityLogWriter_Write_m256BEE6E2FB31EEFCD721BFEE676653E9CD00AD1,
	UnityLogWriter_Write_m28FD8721A9896EE519A36770139213E697C57372,
	UnityLogWriter__ctor_mB7AF0B7C8C546F210699D5F3AA23F370F1963A25,
	Color__ctor_m679019E6084BF7A6F82590F66F5F695F6A50ECC5_AdjustorThunk,
	Color__ctor_m9FEDC8486B9D40C01BF10FDC821F5E76C8705494_AdjustorThunk,
	Color_ToString_m2C9303D88F39CDAE35C613A3C816D8982C58B5FC_AdjustorThunk,
	Color_ToString_m927424DFCE95E13D26A1F8BF91FA0AB3F6C2FC02_AdjustorThunk,
	Color_GetHashCode_mAF5E7EE6AFA983D3FA5E3D316E672EE1511F97CF_AdjustorThunk,
	Color_Equals_m90F8A5EF85416D809F5E3C0ACCADDD4F299AD8FC_AdjustorThunk,
	Color_Equals_mB531F532B5F7BE6168CFD4A6C89358C16F058D00_AdjustorThunk,
	Color_op_Multiply_mFD03CB228034C2D37F326B7AFF27C861E95447B7,
	Color_op_Multiply_m1A1E7DECD013FBEB99018CEDDC30B8A7CF99941D,
	Color_op_Equality_m4975788CDFEF5571E3C51AE8363E6DF65C28A996,
	Color_op_Inequality_m6A9C7B9297D92024848ABFD305DDFE13DF40C86D,
	Color_Lerp_mC986D7F29103536908D76BD8FC59AA11DC33C197,
	Color_RGBMultiplied_mEE82A8761F22790ECD29CD8AE13B1184441CB6C8_AdjustorThunk,
	Color_get_red_m9BD55EBF7A74A515330FA5F7AC7A67C8A8913DD8,
	Color_get_green_mFF9BD42534D385A0717B1EAD083ADF08712984B9,
	Color_get_blue_m6D62D515CA10A6E760848E1BFB997E27B90BD07B,
	Color_get_white_mB21E47D20959C3AEC41AF8BA04F63AC89FAF319E,
	Color_get_black_m67E91EB7017FC74D9AB5ADEF6B6929B7EFC9A982,
	Color_get_yellow_m9FD4BDABA7E40E136BE57EE7872CEA6B1B2FA1D1,
	Color_get_cyan_m0C608BC083FD98C45C1F4F15AE803D288C647686,
	Color_get_magenta_m46B928AB3005B062069E5DF9CB271E1373A29FE0,
	Color_get_gray_m34BEEC2EEF30819B433125EF748CE22BE17C9B6E,
	Color_get_grey_mB2E29B47327F20233856F933DC00ACADEBFDBDFA,
	Color_get_clear_m9F8076CEFE7B8119A9903212DCBF2BFED114E97F,
	Color_get_linear_m56FB2709C862D1A8E2B16B646FCD2E5FDF3CA904_AdjustorThunk,
	Color_get_maxColorComponent_mAB6964B3523DC9FDDF312F3329EB224DBFECE761_AdjustorThunk,
	Color_op_Implicit_mECB4D0C812888ADAEE478E633B2ECF8F8FDB96C5,
	Color_op_Implicit_m9B1A4B721726FCDA1844A0DC505C2FF8F8C50FC0,
	Color_RGBToHSV_mDC3A14DCF9D4A898AF97613CD07D94BFF8402194,
	Color_RGBToHSVHelper_m45E49F91ACB621F0705F8FB42C75F4BEC3958E89,
	Color_HSVToRGB_m8B61783B65A70BC889424B9A64FD40D48E735FEF,
	Color_HSVToRGB_m48EC4738BCDB7B4846D0B2815AE901E9BA4122F1,
	Color32__ctor_m9D07EC69256BB7ED2784E543848DE7B8484A5C94_AdjustorThunk,
	Color32_op_Implicit_mD17E8145D2D32EF369EFE349C4D32E839F7D7AA4,
	Color32_op_Implicit_m63F14F1A14B1A9A3EE4D154413EE229D3E001623,
	Color32_Lerp_m15C150E00B311BD21CFA7660595C42574AA07269,
	Color32_ToString_m11295D5492D1FB41F25635A83B87C20058DEA256_AdjustorThunk,
	Color32_ToString_m5BB9D04F00C5B22C5B295F6253C99972767102F5_AdjustorThunk,
	ColorUtility_DoTryParseHtmlColor_mC35ED0130470E7096A6A2BD349321E989CBA17A2,
	ColorUtility_TryParseHtmlString_m69BEFAF655920930399471B79CF668FC3BAD4069,
	ColorUtility_ToHtmlStringRGBA_mA60155FDB13ABB8720CFD3024EB8F5EDF23D79D4,
	Gradient_Init_mF271EE940AEEA629E2646BADD07DF0BFFDC5EBA1,
	Gradient_Cleanup_m0F4C6F0E90C86E8A5855170AA5B3FC6EC80DEF0C,
	Gradient_Internal_Equals_mA15F4C17B0910C9C9B0BAE3825F673C9F46B2054,
	Gradient__ctor_m4B95822B3C5187566CE4FA66E283600DCC916C5A,
	Gradient_Finalize_m2E940A5D5AE433B43D83B8E676FB9844E86F8CD0,
	Gradient_Equals_m75D0B1625C55AAAEC024A951456300FEF4546EFF,
	Gradient_Equals_m2F4EB14CAD1222F30E7DA925696DB1AF41CAF691,
	Gradient_GetHashCode_m31528AF94CBACB9F6C453FD35BCDFABB77C9AED5,
	Matrix4x4_GetRotation_m2613C86699F582B97CA1802BF47FA81A9BD6BF1C_AdjustorThunk,
	Matrix4x4_GetLossyScale_m4358084CE88EF575BCBF48D1CB77963F45FA15B4_AdjustorThunk,
	Matrix4x4_get_rotation_m3F80DDCCBDC01EBF36D61F382749AE704603C379_AdjustorThunk,
	Matrix4x4_get_lossyScale_m540B8C7CAA4F2FFF4B8C1DBED639B49F5EFB81CF_AdjustorThunk,
	Matrix4x4_TRS_m0CBC696D0BDF58DCEC40B99BC32C716FAD024CE5,
	Matrix4x4_Inverse_m2A60D822437B96567202296F39BFBD617D49E72F,
	Matrix4x4_get_inverse_mFA34ECC790B269522F60FC32370D628DAFCAE225_AdjustorThunk,
	Matrix4x4_Ortho_m9EBA11F0B670FC5563E88E4D98BAF072FFC549F1,
	Matrix4x4__ctor_mFDDCE13D7171353ED7BA9A9B6885212DFC9E1076_AdjustorThunk,
	Matrix4x4_get_Item_mCE077E076AEE65232C5602DBBB5CC0D236184D0F_AdjustorThunk,
	Matrix4x4_set_Item_mE286A89718710DDF166DF6ACF8A480D15FE06B2F_AdjustorThunk,
	Matrix4x4_get_Item_mA0E634EF5A723EA9DD824391D4C62F4100C64813_AdjustorThunk,
	Matrix4x4_set_Item_m27BC97EE3093FDADFE0804FB80AFF5CDB03D355B_AdjustorThunk,
	Matrix4x4_GetHashCode_m102B903082CD1C786C221268A19679820E365B59_AdjustorThunk,
	Matrix4x4_Equals_mF6EB7A6D466F5AE1D1A872451359645D1C69843D_AdjustorThunk,
	Matrix4x4_Equals_mAE7AC284A922B094E4ACCC04A1C48B247E9A7997_AdjustorThunk,
	Matrix4x4_op_Multiply_mC2B30D333D4399C1693414F1A73D87FB3450F39F,
	Matrix4x4_op_Multiply_m6967C4B4CC9F36FE461F5420DF7175B4201E2585,
	Matrix4x4_GetColumn_m5CAA237D7FD65AA772B84A1134E8B0551F9F8480_AdjustorThunk,
	Matrix4x4_SetRow_m86E7E23D71224106549DC25CEDFB992218C52187_AdjustorThunk,
	Matrix4x4_MultiplyPoint_mE92BEE4DED3B602983C2BBE06C44AD29564EDA83_AdjustorThunk,
	Matrix4x4_MultiplyPoint3x4_mA0A34C5FD162DA8E5421596F1F921436F3E7B2FC_AdjustorThunk,
	Matrix4x4_MultiplyVector_m88C4BE23EB0B45BB701514AF3E1CA5A857F8212C_AdjustorThunk,
	Matrix4x4_get_zero_m653FAA37D26B00352B992A4B87CE534251FB91CC,
	Matrix4x4_get_identity_mC91289718DDD3DDBE0A10551BDA59A446414A596,
	Matrix4x4_ToString_mF45C45AD892B0707C34BEE0C716C91C0B5FD2831_AdjustorThunk,
	Matrix4x4_ToString_mC2CC8C3C358C9C982F25F633BC21105D2C3BCEFB_AdjustorThunk,
	Matrix4x4__cctor_m98C56DA6312BFD0230C12C0BABB7CF6627A9CC87,
	Matrix4x4_GetRotation_Injected_m370347C2A20639ACAAF078CD5E0093A1D932B49A,
	Matrix4x4_GetLossyScale_Injected_mDD5A46BA5733BF1E74CD3F8E703DBFD9FC938D90,
	Matrix4x4_TRS_Injected_m1595DD592EF50D0F0F33217285FB2884A81DF308,
	Matrix4x4_Inverse_Injected_m6DB86474590576CF7E1401A213812E05B41ABC37,
	Matrix4x4_Ortho_Injected_mCD0A8F3D5A71423D806A246D50D6ABD0B59E263A,
	Vector3_OrthoNormalize2_m569EE92448DA705CD5E7B45A51E95FF8A7EBB6A6,
	Vector3_OrthoNormalize_m4D0DA9DFEE300B3A4965FAB1F8CB77468BF9F2A1,
	Vector3_RotateTowards_mCE2B2820B2483A44056A74E9C2C22359ED7D1AD5,
	Vector3_Lerp_m8E095584FFA10CF1D3EABCD04F4C83FB82EC5524,
	Vector3_get_Item_m7E5B57E02F6873804F40DD48F8BEA00247AFF5AC_AdjustorThunk,
	Vector3_set_Item_mF3E5D7FFAD5F81973283AE6C1D15C9B238AEE346_AdjustorThunk,
	Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_AdjustorThunk,
	Vector3__ctor_mF7FCDE24496D619F4BB1A0BA44AF17DCB5D697FF_AdjustorThunk,
	Vector3_Scale_m8805EE8D2586DE7B6143FA35819B3D5CF1981FB3,
	Vector3_Scale_m52F0C3F26DBEE84B5E292F2E084DA2F7329B6EFF_AdjustorThunk,
	Vector3_Cross_m63414F0C545EBB616F339FF8830D37F9230736A4,
	Vector3_GetHashCode_m9F18401DA6025110A012F55BBB5ACABD36FA9A0A_AdjustorThunk,
	Vector3_Equals_m210CB160B594355581D44D4B87CF3D3994ABFED0_AdjustorThunk,
	Vector3_Equals_mA92800CD98ED6A42DD7C55C5DB22DAB4DEAA6397_AdjustorThunk,
	Vector3_Reflect_m46273855FA396DF91C2EE4780B8F227C96C8A024,
	Vector3_Normalize_m7C9B0E84BCB84D54A16D1212F3DE5AB2A386FCD9,
	Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780_AdjustorThunk,
	Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5_AdjustorThunk,
	Vector3_Dot_mD19905B093915BA12852732EA27AA2DBE030D11F,
	Vector3_ProjectOnPlane_m066BDEFD60B2828C4B531CD96C4DBFADF6B0EF3B,
	Vector3_Angle_m3715AB03A36C59D8CF08F8D71E2F46454EB884C1,
	Vector3_SignedAngle_m816C32A674665A4C3C9D850FB0A107E69A4D3E0A,
	Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677,
	Vector3_Magnitude_mFBD4702FB2F35452191EC918B9B09766A5761854,
	Vector3_get_magnitude_mDDD40612220D8104E77E993E18A101A69A944991_AdjustorThunk,
	Vector3_get_sqrMagnitude_mC567EE6DF411501A8FE1F23A0038862630B88249_AdjustorThunk,
	Vector3_Min_m400631CF8796AD247ABBAC2E40FD5BED64FA9BD0,
	Vector3_Max_m1BEB59C24069DAAE250E28C903B047431DC53155,
	Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6,
	Vector3_get_one_m9CDE5C456038B133ED94402673859EC37B1C1CCB,
	Vector3_get_forward_m3082920F8A24AA02E4F542B6771EB0B63A91AC90,
	Vector3_get_back_mD521DF1A2C26E145578E07D618E1E4D08A1C6220,
	Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50,
	Vector3_get_down_mFA85B870E184121D30F66395BB183ECAB9DD8629,
	Vector3_get_left_mDAB848C352B9D30E2DDDA7F56308ABC32A4315A5,
	Vector3_get_right_mF5A51F81961474E0A7A31C2757FD00921FB79C44,
	Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0,
	Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58,
	Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7,
	Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A,
	Vector3_op_Multiply_m079B29E4F58127F03BD52558C1FE1A528547328F,
	Vector3_op_Division_mE5ACBFB168FED529587457A83BA98B7DB32E2A05,
	Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296,
	Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710,
	Vector3_ToString_mD5085501F9A0483542E9F7B18CD09C0AB977E318_AdjustorThunk,
	Vector3_ToString_m8E771CC90555B06B8BDBA5F691EC5D8D87D68414_AdjustorThunk,
	Vector3__cctor_m1630C6F57B6D41EFCDFC7A10F52A4D2448BFB2E7,
	Vector3_RotateTowards_Injected_mE5549382553BABB025BCE9985C5B5A7EACA8D1F2,
	Quaternion_FromToRotation_mD0EBB9993FC7C6A45724D0365B09F11F1CEADB80,
	Quaternion_Inverse_mE2A449C7AC8A40350AAC3761AE1AFC170062CAC9,
	Quaternion_Internal_FromEulerRad_m3D0312F9F199A8ADD7463C450B24081061C884A7,
	Quaternion_Internal_ToEulerRad_m442827358B6C9EB81ADCC01F316AA2201453001E,
	Quaternion_AngleAxis_m4644D20F58ADF03E9EA297CB4A845E5BCDA1E398,
	Quaternion_LookRotation_m8A7DB5BDBC361586191AB67ACF857F46160EE3F1,
	Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF,
	Quaternion__ctor_m564FA9302F5B9DA8BAB97B0A2D86FFE83ACAA421_AdjustorThunk,
	Quaternion_get_identity_mF2E565DBCE793A1AE6208056D42CA7C59D83A702,
	Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0,
	Quaternion_op_Multiply_mDC5F913E6B21FEC72AB2CF737D34CC6C7A69803D,
	Quaternion_IsEqualUsingDot_mC57C44978B13AD1592750B1D523AAB4549BD5643,
	Quaternion_op_Equality_m7EC909C253064DBECF7DB83BCF7C2E42163685BE,
	Quaternion_op_Inequality_m37169F3E8ADDA24A5A221AD7397835B437B71439,
	Quaternion_Dot_m7F12C5843352AB2EA687923444CC987D51515F9A,
	Quaternion_Internal_MakePositive_mD0A2B4D1439FC9AC4B487C2392836C8436323857,
	Quaternion_get_eulerAngles_m3DA616CAD670235A407E8A7A75925AA8E22338C3_AdjustorThunk,
	Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3,
	Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB,
	Quaternion_GetHashCode_mFCEA4CA542544DC9BD222C66F524C2F3CFE60744_AdjustorThunk,
	Quaternion_Equals_m3EDD7DBA22F59A5797B91820AE4BBA64576D240F_AdjustorThunk,
	Quaternion_Equals_m02CE0D27C1DA0C037D8721750E30BB1FAF1A7DAD_AdjustorThunk,
	Quaternion_ToString_mD3D4C66907C994D30D99E76063623F7000F6998E_AdjustorThunk,
	Quaternion_ToString_mF10FE18AAC385F9CFE721ECD8B66F14346774F31_AdjustorThunk,
	Quaternion__cctor_m580F8269E5FCCBE5C27222B87E7726823CEEC5E0,
	Quaternion_FromToRotation_Injected_mF0D47B601B2A983EF001C4BDDA1819DB1CAAC68E,
	Quaternion_Inverse_Injected_m709B75EDEB9B03431C31D5D5A100237FAB9F34D6,
	Quaternion_Internal_FromEulerRad_Injected_m5220AD64F37DB56C6DFF9DAE8B78F4E3F7185110,
	Quaternion_Internal_ToEulerRad_Injected_m4626950CCFC82D6D08FEDC4EA7F3C594C9C3DED0,
	Quaternion_AngleAxis_Injected_m651561C71005DEB6C7A0BF672B631DBF121B5B61,
	Quaternion_LookRotation_Injected_m5D096FB4F030FA09FACCB550040173DE7E3F9356,
	Mathf_NextPowerOfTwo_m89DB0674631948FE00FD5660B18D9E62CE85CAF5,
	Mathf_GammaToLinearSpace_mD7A738810039778B4592535A1DB5767C4CAD68FB,
	Mathf_LinearToGammaSpace_m5F74DEC2B3A7B45A75AC199284A22D92839118F3,
	Mathf_PerlinNoise_mBCF172821FEB8FAD7E7CF7F7982018846E702519,
	Mathf_Sin_m530E5197B1E4441946DB8A12412A0C51730F6BA1,
	Mathf_Cos_mF57C9B0E2B45B8A25619309BBAD6C201FF73AB98,
	Mathf_Tan_mC7E6A6883BF16BBF77F15A1A0C35AB06DDAF48DC,
	Mathf_Asin_mABB8C82CAE370E54952561960E17198441719187,
	Mathf_Acos_m199FA3A1E39D8E025CAC43BD3AF8BEE56027393C,
	Mathf_Atan_m12592B989E7F2CF919DF4223769CC480F888ADE5,
	Mathf_Atan2_mF79B8F61C6089913A04B7FB6ED5A27171051A005,
	Mathf_Sqrt_m7C3ABF6CADBBF2F97E316B025542BD0B85E87372,
	Mathf_Abs_m0F02EBECA39438E5A996BE2379431B6F5E8A353D,
	Mathf_Abs_mE46B08A540F26741910760E84ACB6AACD996C3C0,
	Mathf_Min_mD28BD5C9012619B74E475F204F96603193E99B14,
	Mathf_Min_m8038BC2CE141C9AF3ECA2E31B88A9768423B1519,
	Mathf_Max_m4CE510E1F1013B33275F01543731A51A58BA0775,
	Mathf_Max_mAB2544BF70651EC36982F5F4EBD250AEE283335A,
	Mathf_Pow_mA38993C7C8CCFCB94EE3F61236C79C45E878673E,
	Mathf_Exp_m78561917BA839F2FACFE60985B1B98E2A0135201,
	Mathf_Log_mF7F3624FA030AB57AD8C1F4CAF084B2DCC99897A,
	Mathf_Ceil_mD48EF2CA69275216AB32C63A47C8AF9428651D7D,
	Mathf_Floor_mB05B375ED9442CAB4ADEB7C442DB20C700F46BFB,
	Mathf_Round_mE2DE38D81FFB085CBC073D939CDF5CBB5D57E9C9,
	Mathf_CeilToInt_m3A3E7C0F6A3CF731411BB90F264F989D8311CC6F,
	Mathf_FloorToInt_m9164D538D17B8C3C8A6C4E4FA95032F757D9091E,
	Mathf_RoundToInt_m56850BDF60FF9E3441CE57E5EFEFEF36EDCDE6DD,
	Mathf_Sign_m01716387C82B9523CFFADED7B2037D75F57FE2FB,
	Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87,
	Mathf_Clamp_mAD0781EB7470594CD4482DD64A0D739E4E539C3C,
	Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C,
	Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616,
	Mathf_LerpUnclamped_mF68548D1AA22018863B6EBE911A5F7A959F94C1E,
	Mathf_Approximately_mC2A3F657E3FD0CCAD4A4936CEE2F67D624A2AA55,
	Mathf_SmoothDamp_m0B29D964FCB8460976BBE6BF56CBFDDC98EB5652,
	Mathf_SmoothDamp_mBA32FC6CC8693F9446A2CF974AA44DC9801099C8,
	Mathf_SmoothDampAngle_mB876CA3D4A313CF29074CCDDB784CAF5533EBA46,
	Mathf_SmoothDampAngle_m49AC737896E42840A706C2C895B0A93786149BF5,
	Mathf_Repeat_mBAB712BA039DF58DBB1B31B669E502C54F3F13CE,
	Mathf_InverseLerp_mCD2E6F9ADCFFB40EB7D3086E444DF2C702F9C29B,
	Mathf_DeltaAngle_mB1BD0E139ACCAE694968F7D9CB096C60F69CE9FE,
	Mathf__cctor_mFC5862C195961EAE43A5C7BB96E07648084C1525,
	Vector2_get_Item_m0F685FCCDE8FEFF108591D73A6D9F048CCEC5926_AdjustorThunk,
	Vector2_set_Item_m817FDD0709F52F09ECBB949C29DEE88E73889CAD_AdjustorThunk,
	Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_AdjustorThunk,
	Vector2_Scale_m54AA203304585B8BB6ECA4936A90F408BD880916,
	Vector2_Normalize_m0F1341493AD5F0B7DA4D504A44F52A1E2FFCCEC3_AdjustorThunk,
	Vector2_get_normalized_m1F7F7AA3B7AC2414F245395C3785880B847BF7F5_AdjustorThunk,
	Vector2_ToString_mBD48EFCDB703ACCDC29E86AEB0D4D62FBA50F840_AdjustorThunk,
	Vector2_ToString_m503AFEA3F57B8529C047FF93C2E72126C5591C23_AdjustorThunk,
	Vector2_GetHashCode_m9A5DD8406289F38806CC42C394E324C1C2AB3732_AdjustorThunk,
	Vector2_Equals_m67A842D914AA5A4DCC076E9EA20019925E6A85A0_AdjustorThunk,
	Vector2_Equals_m6E08A16717F2B9EE8B24EBA6B234A03098D5F05D_AdjustorThunk,
	Vector2_Dot_mB2DFFDDA2881BA755F0B75CB530A39E8EBE70B48,
	Vector2_get_magnitude_mD30DB8EB73C4A5CD395745AE1CA1C38DC61D2E85_AdjustorThunk,
	Vector2_get_sqrMagnitude_mF489F0EF7E88FF046BA36767ECC50B89674C925A_AdjustorThunk,
	Vector2_Distance_m7DFAD110E57AF0E903DDC47BDBD99D1CC62EA03F,
	Vector2_op_Addition_m5EACC2AEA80FEE29F380397CF1F4B11D04BE71CC,
	Vector2_op_Subtraction_m6E536A8C72FEAA37FF8D5E26E11D6E71EB59599A,
	Vector2_op_Multiply_m98AA5AF174918812B6E0C201C850FABB4A526813,
	Vector2_op_Division_m63A593A281BC0B6C36FCFF399056E1DE9F4C01E0,
	Vector2_op_UnaryNegation_mA3AA3A53CD43237B0BA7AC57C09A0468A940D7C5,
	Vector2_op_Multiply_mC7A7802352867555020A90205EBABA56EE5E36CB,
	Vector2_op_Division_m9E0ABD4CB731137B84249278B80D4C2624E58AC6,
	Vector2_op_Equality_mAE5F31E8419538F0F6AF19D9897E0BE1CE8DB1B0,
	Vector2_op_Inequality_mA9E4245E487F3051F0EBF086646A1C341213D24E,
	Vector2_op_Implicit_mE407CAF7446E342E059B00AA9EDB301AEC5B7B1A,
	Vector2_op_Implicit_m4FA146E613DBFE6C1C4B0E9B461D622E6F2FC294,
	Vector2_get_zero_m621041B9DF5FAE86C1EF4CB28C224FEA089CB828,
	Vector2_get_one_m9B2AFD26404B6DD0F520D19FC7F79371C5C18B42,
	Vector2_get_up_mCEC23A0CF0FC3A2070C557AFD9F84F3D9991866C,
	Vector2_get_down_m38F16950B2C1FAFBE218C3E62DA95D498443650F,
	Vector2_get_right_m42ED15112D219375D2B6879E62ED925D002F15AF,
	Vector2__cctor_m64DC76912D71BE91E6A3B2D15D63452E2B3AD6EC,
	Vector2Int_get_x_mDBEFBCDF9C7924767344ED2CEE1307885AED947B_AdjustorThunk,
	Vector2Int_set_x_m58F3B1895453A0A4BC964CA331D56B7C3D873B7C_AdjustorThunk,
	Vector2Int_get_y_m282591DEB0E70B02F4F4DDFAB90116AEC8E6B86C_AdjustorThunk,
	Vector2Int_set_y_m55A40AE7AF833E31D968E0C515A5C773F251C21A_AdjustorThunk,
	Vector2Int__ctor_mB2B73108B6DD3ADC1B515D7DD9116ECAC6833726_AdjustorThunk,
	Vector2Int_op_Implicit_m74C29CAFE091CE873934FAF6300CD01461D7FE6B,
	Vector2Int_Equals_m7EB52A67AE3584E8A1E8CAC550708DF13520F529_AdjustorThunk,
	Vector2Int_Equals_m96F4F602CE85AFD675A8096AB9D5E2D4544382FF_AdjustorThunk,
	Vector2Int_GetHashCode_mB963D0B9A29E161BC4B73F97AEAF2F843FC8EF71_AdjustorThunk,
	Vector2Int_ToString_m7928A3CC56D9CAAB370F6B3EE797CED4BE9B9B20_AdjustorThunk,
	Vector2Int_ToString_m608D5CEF9835892DD989B0891D7AE6F2FC0FBE02_AdjustorThunk,
	Vector2Int__cctor_mB461BECA877E11B4B65206DFAA8A8C66170861F2,
	Vector3Int_get_x_m5B1B86414F43D7CE0C83932F0094B1A94A9B4594_AdjustorThunk,
	Vector3Int_get_y_m62E0B990FBFDA9D416B82000A73B5B4F71CF0FA3_AdjustorThunk,
	Vector3Int_get_z_m14EC2E331A510D161E5A7A587837BBD2A3D225B6_AdjustorThunk,
	Vector3Int__ctor_m3785ECE3685842F2B477CBE64334D6969EB503DF_AdjustorThunk,
	Vector3Int_op_Equality_m724847B7E7A484A1E6F598CEC2D77CDE8ECE49E7,
	Vector3Int_Equals_mE2CEEF2FE79B510472A930F80DDB56E23B39CC11_AdjustorThunk,
	Vector3Int_Equals_m8BE683205BACD053B7EB560AB5B7EDE78B779C5F_AdjustorThunk,
	Vector3Int_GetHashCode_mE3102EAE8E76A05DD7344009C2617BA34CDD3C08_AdjustorThunk,
	Vector3Int_ToString_mF13025AB50754BB9C9E98659216EC0B79F1CAA61_AdjustorThunk,
	Vector3Int_ToString_m8C32AEB68C6D46DA1F2546378067687007FCF6A5_AdjustorThunk,
	Vector3Int__cctor_m2EC94FD622152EE49C25C960DE1DEB83A3212283,
	Vector4_get_Item_m469B9D88498D0F7CD14B71A9512915BAA0B9B3B7_AdjustorThunk,
	Vector4_set_Item_m7552B288FF218CA023F0DFB971BBA30D0362006A_AdjustorThunk,
	Vector4__ctor_mCAB598A37C4D5E80282277E828B8A3EAD936D3B2_AdjustorThunk,
	Vector4_GetHashCode_mCA7B312F8CA141F6F25BABDDF406F3D2BDD5E895_AdjustorThunk,
	Vector4_Equals_m71D14F39651C3FBEDE17214455DFA727921F07AA_AdjustorThunk,
	Vector4_Equals_m0919D35807550372D1748193EB31E4C5E406AE61_AdjustorThunk,
	Vector4_Dot_m3373C73B23A0BC07DDE8B9C99AA2FC933CD1143F,
	Vector4_get_sqrMagnitude_m1450744F6AAD57773CE0208B6F51DDEEE9A48E07_AdjustorThunk,
	Vector4_get_zero_m9E807FEBC8B638914DF4A0BA87C0BD95A19F5200,
	Vector4_get_one_m59B707729B52E58A045A6DE2ACDDE1D1600F48A4,
	Vector4_op_Multiply_m4B615DCAD6D77FE276AC56F17EA3ED0DCD942111,
	Vector4_op_Division_m8AF7C92DD640CE3275F975E9BCD62F04E29DEDB6,
	Vector4_op_Equality_mAC86329F5E0AF56A4A1067AB4299C291221720AE,
	Vector4_op_Inequality_m2B976EE8649EE1AE9DE1771CEE183AC70E3A75B7,
	Vector4_op_Implicit_mDCFA56E9D34979E1E2BFE6C2D61F1768D934A8EB,
	Vector4_op_Implicit_m5811604E04B684BE3F1A212A7FA46767619AB35B,
	Vector4_op_Implicit_mFFF2D39354FC98FDEDA761EDB4326E4F11B87504,
	Vector4_ToString_mF2D17142EBD75E91BC718B3E347F614AC45E9040_AdjustorThunk,
	Vector4_ToString_m0EC6AA83CD606E3EB5BE60108A1D9AC4ECB5517A_AdjustorThunk,
	Vector4__cctor_m35F167F24C48A767EAD837754896B5A5178C078A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_SendMessage_mE7A0F096977E9FEF4139A4D66DA05DC07C67399A,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_TrySendMessage_m9573E63723FDEBB68978AD4A14253DC648071582,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Poll_mA33C0EA45F7DFF196710206BD472896DD02BB0BF,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_RegisterInternal_mC10E40AF3DE9AC1E1DAC42BF2F4F738E42F1131E,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_UnregisterInternal_m5E7A02A5A9569D111F9197ED07D5E822357FADBF,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_Initialize_mC461084E67159FF60B78A2B995A0D6C6A5F05847,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_IsConnected_mFBCF58F025DCD0E95E1077019F30A915436221C8,
	PlayerConnectionInternal_UnityEngine_IPlayerEditorConnectionNative_DisconnectAll_m39E3D0780D7BA1BBBAA514AB02C8B7121E6F1607,
	PlayerConnectionInternal_IsConnected_m6E50FFCA993DB110A440A175417542F19360878C,
	PlayerConnectionInternal_Initialize_m0E2E758B321FDCBA8598FE99E453F371E8544676,
	PlayerConnectionInternal_RegisterInternal_mEA39746E226DE13CDA2AD91050A7B49BE6CEDFD6,
	PlayerConnectionInternal_UnregisterInternal_m22AB7635F9B4EE0EA1F23F61E212763375479EB0,
	PlayerConnectionInternal_SendMessage_m28075B14E3C74180BC8B54C013D22F7B0DDEFA8E,
	PlayerConnectionInternal_TrySendMessage_m654FD63C3F37CA5381FA3C956B4C6F68EFF8A202,
	PlayerConnectionInternal_PollInternal_m51A00BEDBF9EA39C42023981C0A1CA8B38116D86,
	PlayerConnectionInternal_DisconnectAll_mBE50046CA1DD3A969860F0D25B5FD3381907881D,
	PlayerConnectionInternal__ctor_m220EE8E01600348418FFBC1B83BF824C39A4441B,
	PlayerPrefsException__ctor_m8AC8F1E3590787B418BEDCAC022B2945C43928CB,
	PlayerPrefs_TrySetSetString_mC34C1AEA6A6E7BE93645BEB527BF1E6BF8D3A0F9,
	PlayerPrefs_SetString_m94CD8FF45692553A5726DFADF74935F7E1D1C633,
	PlayerPrefs_GetString_m5709C9DC233D10A7E9AF4BCC9639E3F18FE36831,
	PlayerPrefs_GetString_mE7654C1031622A56CD8F248F53714B105A35A159,
	PlayerPrefs_HasKey_m48BE5886380B51AB495B91C9A26115B7CB958A92,
	PropertyAttribute__ctor_mA13181D93341AEAE429F0615989CB4647F2EB8A7,
	TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042,
	SpaceAttribute__ctor_m9C74D8BD18B12F12D81F733115FF9A0BFE581D1D,
	SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44,
	HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E,
	RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000,
	TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042,
	ColorUsageAttribute__ctor_mB764B17A7C17E6199BFC147BFFE72D34FD55D76E,
	ColorUsageAttribute__ctor_mA53A82E036DBF33A6DEC0AD2F00144C52DCD3B09,
	Random_InitState_m9030E6387803E8EBAD0A5B0150254A89F8286A9C,
	Random_get_state_mB5BB303DE4D0FD30AB8C8060EC0925C38A4A9020,
	Random_set_state_m10870B08C03C9B4058B410F1ABD056946D392E3A,
	Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2,
	Random_Range_m4B3A0037ACA057F33C94508F908546B9317D996A,
	Random_RandomRangeInt_mC1A726071BB9634DCA04EF5DCC1E59360CE392DE,
	Random_get_value_m9AEBC7DF0BB6C57C928B0798349A7D3C0B3FB872,
	Random_get_insideUnitSphere_m43E5AE1F6A6CFA892BAE6E3ED71BEBFCE308CE90,
	Random_GetRandomUnitCircle_m2FB0AE6E31D08D017D1104DE2411A3F252982A0C,
	Random_get_insideUnitCircle_m0A73BD3F48690731663D2425D98687F5AD446190,
	Random_get_onUnitSphere_m909066B4EAF5EE0A05D3C1DCBDADC55A2D6A4EED,
	Random_get_state_Injected_m079170976C10F92A3036FE8BA5EB8C7589A11EC3,
	Random_set_state_Injected_mAE6755C8E67030DE2D11496C81D8EEF7918515B0,
	Random_get_insideUnitSphere_Injected_m79570880ED45493E9DF45AB745483B18512C180F,
	Random_get_onUnitSphere_Injected_m8B7EF1DF4AD0AB35507E1DFD6581EC0796F9C0E9,
	NULL,
	Resources_FindObjectsOfTypeAll_mAC4233995ECE942E2C0250607C5CFD9E805F2215,
	NULL,
	Resources_Load_m011631B3740AFD38D496838F10D3DA635A061120,
	NULL,
	Resources_Load_m6E8E5EA02A03F3AFC8FD2D775263DBBC64BF205C,
	Resources_GetBuiltinResource_m59A7993A48D44A0002E532B7DD79BDA426E0C8A6,
	NULL,
	Resources_UnloadUnusedAssets_m5BF7EFD195EFFE171CB47FF88CA880D76A751C70,
	AsyncOperation_InternalDestroy_mB659E46A7DE3337448BACCD77F5B64927877F482,
	AsyncOperation_get_isDone_m4592F121393149E539D2107239639A049493D877,
	AsyncOperation_get_progress_m2471A0564D5C2207116737619E2CED05FBBC2D19,
	AsyncOperation_Finalize_m1A989E71664DD802015D858E7A336A1158BD474C,
	AsyncOperation_InvokeCompletionEvent_m2BFBB3DD63950957EDE38AE0A8D2587B900CB8F5,
	AsyncOperation_add_completed_m44D28A82BB10C85AED56A43BB666850D2E9E59E8,
	AsyncOperation_remove_completed_m5B81CC486905074B13D3784BD0A061F2B4BCC3F3,
	AsyncOperation__ctor_mFC0E13622A23CD19A631B9ABBA506683B71A2E4A,
	AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m9BA10D3F7DEEE7FB825187CF60338BBECA83F4B2,
	AttributeHelperEngine_GetRequiredComponents_mE25F6E1B6C8E4BB44FDE3E418DD442A12AA24063,
	AttributeHelperEngine_GetExecuteMode_mB33B8C49CC1EE05341F8ADFBF9B0EEB4894C0864,
	AttributeHelperEngine_CheckIsEditorScript_m109EDB093200EAAA9DA7203E58385429F791CA24,
	AttributeHelperEngine_GetDefaultExecutionOrderFor_m4425CE70A70DB0716D3A5BF8C51C53C6A7891131,
	NULL,
	AttributeHelperEngine__cctor_m00AE154DE9BE8D99EBFBBE005F9FC40109E8FB19,
	DisallowMultipleComponent__ctor_mDCA4B0F84AB4B3E17D216DB29318032547AB7F0D,
	RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4,
	AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549,
	AddComponentMenu__ctor_m6405E10C6B6269CA2F0684BF0B356A7E6AB7BF56,
	CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1,
	CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6,
	CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65,
	ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B,
	ContextMenu__ctor_m2A623D0967274884B6D6BBB1E6242CBE60130D90,
	ContextMenu__ctor_mEF09B48BBF72E9A910B6DA5FB59721B15C6DAD36,
	ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A,
	ExecuteAlways__ctor_mDB73D23637E65E57DE87C7BAAFE4CE694AE9BEE0,
	HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9,
	HelpURLAttribute__ctor_mCC837CE7900738F3152D585D454A34A15793C215,
	DefaultExecutionOrder_get_order_m1C25A6D0F7F67A70D1B6B99D45C5A242DF92A4D2,
	ExcludeFromPresetAttribute__ctor_mE4AF4E74678A39848AD81121936DEDDA1F89AB8B,
	Behaviour_get_enabled_m08077AB79934634E1EAE909C2B482BEF4C15A800,
	Behaviour_set_enabled_mDE415591B28853D1CD764C53CB499A2142247F32,
	Behaviour_get_isActiveAndEnabled_mDD843C0271D492C1E08E0F8DEE8B6F1CFA951BFA,
	Behaviour__ctor_mCACD3614226521EA607B0F3640C0FAC7EACCBCE0,
	ClassLibraryInitializer_Init_m462E6CE3B1A6017AB1ADA0ACCEACBBB0B614F564,
	Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F,
	Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B,
	Component_GetComponent_m4DE64B46F790BD785FDDDAD364E0364CDDE05BDB,
	Component_GetComponentFastPath_m738F6C78381ECFB419C1B130FD8B968253F8186C,
	NULL,
	Component_GetComponentInChildren_m79A4BF58C839520872C6FB854CB11E6CFC57D50F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Component_GetComponentInParent_mC299BF144B9602E6B60C707B1BE695693381F264,
	NULL,
	NULL,
	NULL,
	NULL,
	Component_GetComponentsForListInternal_mEC716300A7894D57F9AF6C98FA1A69C12AB4F036,
	Component_GetComponents_m0268D42CD0215CD9247CF74AA881BAACE10357FC,
	NULL,
	Component_get_tag_m77B4A7356E58F985216CC53966F7A9699454803E,
	Component_set_tag_m6E921BD86BD4A0B5114725FFF0CCD62F26BD7E81,
	NULL,
	Component_SendMessage_m3B002D579E5933EEFCA4024A1845CACB4FBBC208,
	Component_SendMessage_m24896D34C08CE9336E8998CB942339604F4F822A,
	Component_SendMessage_m4DEDB29112224E363C9B1093004C6530FFE12792,
	Component__ctor_m0B00FA207EB3E560B78938D8AD877DB2BC1E3722,
	Coroutine__ctor_mCB658B6AD0EABDAB709A53D2B111955E06CE3C61,
	Coroutine_Finalize_m7248D49A93F72CA5E84EAD20A32ADE028905C536,
	Coroutine_ReleaseCoroutine_m2C46DD57BDB3BB7B64204EA229F4C5CDE342B18B,
	SetupCoroutine_InvokeMoveNext_m036E6EE8C2A4D2DAA957D5702F1A3CA51313F2C7,
	SetupCoroutine_InvokeMember_m953E000F2B95EA72D6B1BC2330F0C844A2C0C680,
	NULL,
	CustomYieldInstruction_get_Current_m719832E0EFC6B408579FCAB3B9D7A9C72A3EF80A,
	CustomYieldInstruction_MoveNext_m987F30FB5F8A82F8FA62C9E3BF5DCD7D8A8FDAA6,
	CustomYieldInstruction_Reset_m9EE8C2D060FD26FE95EA2578AEF89F7012466332,
	CustomYieldInstruction__ctor_m01929E3EEB78B751510038B32D889061960DA1BE,
	ExcludeFromObjectFactoryAttribute__ctor_mAF8163E246AD4F05E98775F7E0904F296770B06C,
	ExtensionOfNativeClassAttribute__ctor_mC11B09D547C215DC6E298F78CA6F05C2B28B8043,
	GameObject_CreatePrimitive_mB1E03B8D373EBECCD93444A277316A53EC7812AC,
	NULL,
	GameObject_GetComponent_mDF0C55D6EE63B6CA0DD45D627AD267004D6EC473,
	GameObject_GetComponentFastPath_mAC58DB8AC26576ED2A87C843A68A13C325E3C944,
	GameObject_GetComponentInChildren_m56CAFD886686C8F6025B5CDF016E8BC684A20EED,
	NULL,
	NULL,
	GameObject_GetComponentInParent_m5C1C3266DE9C2EE078C905787DDCD666AB157C2B,
	GameObject_GetComponentInParent_m84A8233060CF7F5043960E30EBC1FC715DDF9862,
	GameObject_GetComponentsInternal_mF491A337A167109189E2AB839584697EB2672E7D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	GameObject_Internal_AddComponentWithType_mAD63AAF65D0603B157D8CC6C27F3EC73C108ADB4,
	GameObject_AddComponent_mD183856CB5A1CCF1576221D7D6CEBC4092E734B8,
	NULL,
	GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34,
	GameObject_get_layer_m9D4C23A2FD105AF9964445BF18A77E8A49012F9F,
	GameObject_set_layer_m2F946916ACB41A59C46346F5243F2BAC235A36A6,
	GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86,
	GameObject_get_activeSelf_m4865097C24FB29F3C31F5C30619AF242297F23EE,
	GameObject_get_activeInHierarchy_mA3990AC5F61BB35283188E925C2BE7F7BF67734B,
	GameObject_get_tag_mC21F33D368C18A631040F2887036C678B96ABC33,
	GameObject_set_tag_m0EBA46574304C71E047A33BDD5F5D49E9D9A25BE,
	GameObject_CompareTag_mA692D8508984DBE4A2FEFD19E29CB1C9D5CDE001,
	GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F,
	GameObject_SendMessage_mD49CCADA51268480B585733DD7C6540CCCC6EF5C,
	GameObject_SendMessage_m9727C08D6F0A5E8F309FA9FFF389ADF8130D7BE7,
	GameObject_SendMessage_m592E9A21D51BE9E1D9E23A85750548E8CC8DB00D,
	GameObject__ctor_mDF8BF31EAE3E03F24421531B25FB4BEDB7C87144,
	GameObject__ctor_mACDBD7A1F25B33D006A60F67EF901B33DD3D52E9,
	GameObject__ctor_m9829583AE3BF1285861C580895202F760F3A82E8,
	GameObject_Internal_CreateGameObject_mA5BCF00F09243D45B7E9A1A421D8357610AE8633,
	GameObject_Find_m20157C941F1A9DA0E33E0ACA1324FAA41C2B199B,
	GameObject_get_scene_m7EBF95ABB5037FEE6811928F2E83C769C53F86C2,
	GameObject_get_gameObject_mD5FFECF7C3AC5039E847DF7A8842478539B701D6,
	GameObject_get_scene_Injected_mE08525424E75EFD26BD152B4AC37563B0D4053F2,
	LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0,
	LayerMask_op_Implicit_mC7EE32122D2A4786D3C00B93E41604B71BF1397C,
	LayerMask_get_value_m6380C7449537F99361797225E179A9448A53DDF9_AdjustorThunk,
	LayerMask_LayerToName_mDD0D3E5920B89D1CB4585B9295E93631AD2EF3B7,
	LayerMask_NameToLayer_m6A2BBB60EC90F7EC48A6A91CA98149C547A04C0A,
	ManagedStreamHelpers_ValidateLoadFromStream_mDE750EE2AF2986BB8E11941D8513AD18597F3B13,
	ManagedStreamHelpers_ManagedStreamRead_m5E2F163F844AE93A058C5B4E31A011938FAE236B,
	ManagedStreamHelpers_ManagedStreamSeek_mD7B16AF1079F3F11EE782A32F851ABD761BD2E9E,
	ManagedStreamHelpers_ManagedStreamLength_m352520A9914611ADA61F089CCA8D996F62F9857F,
	MonoBehaviour_IsInvoking_m7967B0E7FEE6F3FAD9E7565D37B009A8C43FE9A8,
	MonoBehaviour_CancelInvoke_mAF87B47704B16B114F82AC6914E4DA9AE034095D,
	MonoBehaviour_Invoke_m4AAB759653B1C6FB0653527F4DDC72D1E9162CC4,
	MonoBehaviour_InvokeRepeating_mB77F4276826FBA696A150831D190875CB5802C70,
	MonoBehaviour_CancelInvoke_mAD4E486A74AF79DC1AFA880691EF839CDDE630A9,
	MonoBehaviour_IsInvoking_m6BF9433789CE7CE12E12069DBF043D620DFB0B4F,
	MonoBehaviour_StartCoroutine_m338FBDDDEBF67D9FC1F9E5CDEE50E66726454E2E,
	MonoBehaviour_StartCoroutine_m81C113F45C28D065C9E31DD6D7566D1BF10CF851,
	MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719,
	MonoBehaviour_StartCoroutine_Auto_m78A5B32C9E51A3C64C19BB73ED4A9A9B7536677A,
	MonoBehaviour_StopCoroutine_m3AB89AE7770E06BDB33BF39104BE5C57DF90616B,
	MonoBehaviour_StopCoroutine_m5FF0476C9886FD8A3E6BA82BBE34B896CA279413,
	MonoBehaviour_StopCoroutine_m4DB2A899F9BDF8CA3264DD8C4130E767702B626B,
	MonoBehaviour_StopAllCoroutines_m6CFEADAA0266A99176A33B47129392DF954962B4,
	MonoBehaviour_get_useGUILayout_m42BE255035467866EB989932C62E99D5BC347524,
	MonoBehaviour_set_useGUILayout_m2FEF8B91090540BBED30EE904B11F0FB5F7C1E27,
	MonoBehaviour_print_m4F113B89EC1C221CAC6EC64365E6DAD0AF86F090,
	MonoBehaviour_Internal_CancelInvokeAll_mE619220C0A18AE2657DFABDBCC54E54F53C60D83,
	MonoBehaviour_Internal_IsInvokingAll_m898D1B5B37BBFC74C98EA5F86544987A5B41C49B,
	MonoBehaviour_InvokeDelayed_mD3EE47F65885A45357A5822A82F48E17D24C62C7,
	MonoBehaviour_CancelInvoke_m6BBDCBE18EEBE2584EED4F8706DA3BC6771E2529,
	MonoBehaviour_IsInvoking_m2584CADBA55F43D3A1D9955F1E9EAA579B8FD206,
	MonoBehaviour_IsObjectMonoBehaviour_mE580D905186AD91FD74214B643B26F55D54A46AF,
	MonoBehaviour_StartCoroutineManaged_mAA34ABF1564BF65264FF972AF41CF8C0F6A6B6F4,
	MonoBehaviour_StartCoroutineManaged2_m46EFC2DA4428D3CC64BA3F1394D24351E316577D,
	MonoBehaviour_StopCoroutineManaged_m8214F615A10BC1C8C78CEE92DF921C892BE3603D,
	MonoBehaviour_StopCoroutineFromEnumeratorManaged_m17DB11886ABA10DF9749F5E1DDCCD14AD4B5E31E,
	MonoBehaviour_GetScriptClassName_m2968E4771004FC58819BF2D9391DED766D1213E3,
	MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED,
	NoAllocHelpers_SafeLength_m02DFDBC654748FCB895D2ADDC5E5C92415C3A4B2,
	NULL,
	NoAllocHelpers_ExtractArrayFromList_m097334749C829402A9634BF025A54F3918D238DD,
	RangeInt_get_end_m6F8F3C6EA01F7A99BF3A094827F5A0D612AA179E_AdjustorThunk,
	RangeInt__ctor_m61527D982CDE91D896757816896BE6BDB366B9E0_AdjustorThunk,
	RuntimeInitializeOnLoadMethodAttribute__ctor_mAEDC96FCA281601682E7207BD386A1553C1B6081,
	RuntimeInitializeOnLoadMethodAttribute__ctor_mE79C8FD7B18EC53391334A6E6A66CAF09CDA8516,
	RuntimeInitializeOnLoadMethodAttribute_set_loadType_m5C045AAF89A8C1541871F7F9090B3C0A289E32C6,
	ScriptableObject__ctor_m8DAE6CDCFA34E16F2543B02CC3669669FF203063,
	ScriptableObject_CreateInstance_m5371BDC0B4F60FE15914A7BB3FBE07D0ACA0A8D4,
	NULL,
	ScriptableObject_CreateScriptableObject_m9627DCBB805911280823940601D996E008021D6B,
	ScriptableObject_CreateScriptableObjectInstanceFromType_mA2EB72F4D5FC5643D7CFFD07A29DD726CAB1B9AD,
	ScriptingUtility_IsManagedCodeWorking_m176E49DF0BCA69480A3D9360DAED8DDDB8732F68,
	SelectionBaseAttribute__ctor_mDCDA943585A570BA4243FEFB022DABA360910E11,
	StackTraceUtility_SetProjectFolder_m4CF077574CDE9A65A3546973395B4A228C1F957C,
	StackTraceUtility_ExtractStackTrace_mDD5E4AEC754FEB52C9FFA3B0675E57088B425EC6,
	StackTraceUtility_ExtractStringFromExceptionInternal_mE6192186E0D4CA0B148C602A5CDA6466EFA23D99,
	StackTraceUtility_ExtractFormattedStackTrace_m956907F6BE8EFF9BE9847275406FFBBB5FE7F093,
	StackTraceUtility__cctor_m8AFBE529BA4A1737BAF53BB4F8028E18D2D84A5F,
	UnityException__ctor_m8E5C592F5F76B6385D4EFDC2C28AA93B020279E7,
	UnityException__ctor_mB8EBFD7A68451D56285E7D51B42FBECFC8A141D8,
	UnityException__ctor_m00AA4E67E35F95220B5A1C46C9B59AF4ED0D6274,
	TextAsset_get_bytes_m5F15438DABBBAAF7434D53B6778A97A498C1940F,
	TextAsset_get_text_m89A756483BA3218E173F5D62A582070714BC1218,
	TextAsset_ToString_m55E5177185AA59560459F579E36C03CF1971EC57,
	TextAsset_DecodeString_m3CD8D6865DCE58592AE76360F4DB856A6962FE13,
	EncodingUtility__cctor_m6BADEB7670563CC438C10AF259028A7FF06AD65F,
	UnhandledExceptionHandler_RegisterUECatcher_m3A92AB19701D52AE35F525E9518DAD6763D66A93,
	U3CU3Ec__cctor_m5B3F5B8A2DD74DC42F3E777C1FDD1C880EFA95BA,
	U3CU3Ec__ctor_mB628041C94E761F86F2A26819A038D6BC59E324D,
	U3CU3Ec_U3CRegisterUECatcherU3Eb__0_0_mB2E6DD6B9C72FA3D5DB8D311DB281F272A587278,
	Object_GetInstanceID_m7CF962BC1DB5C03F3522F88728CB2F514582B501,
	Object_GetHashCode_m7CC0B54570AA90E51ED2D2D6F6F078BEF9996538,
	Object_Equals_m42425AB7E6C5B6E2BAB15B3184B23371AB0B3435,
	Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499,
	Object_CompareBaseObjects_m412CC4C56457345D91A509C83A0B04E3AE5A0AED,
	Object_IsNativeObjectAlive_mE631050FBED7A72BC8A0394F26FF1984F546B6D4,
	Object_GetCachedPtr_m9F3F26858655E3CE7D471324271E3E32C7AEFD7A,
	Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB,
	Object_set_name_m87C4006618ADB325ABE5439DF159E10DD8DD0781,
	Object_Instantiate_mADD3254CB5E6AB56FBB6964FE4B930F5187AB5AD,
	Object_Instantiate_m0B91F1876CDBE46242A9E5B32F9EE53FAF2BDD99,
	Object_Instantiate_m565A02EA45AEA7442E19FDC8E82695491938CB5A,
	NULL,
	NULL,
	Object_Destroy_mAAAA103F4911E9FA18634BF9605C28559F5E2AC7,
	Object_Destroy_m3EEDB6ECD49A541EC826EA8E1C8B599F7AF67D30,
	Object_DestroyImmediate_m69AA5B06236FACFF316DDFAD131B2622B397AC49,
	Object_DestroyImmediate_mCCED69F4D4C9A4FA3AC30A142CF3D7F085F7C422,
	Object_FindObjectsOfType_m0AEB81CC6F1D224A6F4DCC7D553482D54FC03C5A,
	Object_DontDestroyOnLoad_m03007A68ABBA4CCD8C27B944964983395E7640F9,
	Object_get_hideFlags_m0F6495E1FB440A08314B7D74CCA99C897FAC05D1,
	Object_set_hideFlags_m7DE229AF60B92F0C68819F77FEB27D775E66F3AC,
	NULL,
	NULL,
	Object_CheckNullArgument_mFA979ED3433CACA46AC9AE0029A537B46E17D080,
	Object_FindObjectOfType_mFDEFCE84CE9C644F2B51DCC26CDAC78AC7E89B1B,
	Object_ToString_m2B1E5081A55425442324F437090FAD552931A7E9,
	Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54,
	Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90,
	Object_GetOffsetOfInstanceIDInCPlusPlusObject_m7749BCDBEBAE50378BE38DA313E2D854D77BB18C,
	Object_Internal_CloneSingle_m6C669D602DFD7BC6C47ACA19B2F4D7C853F124BB,
	Object_Internal_InstantiateSingle_mE1865EB98332FDFEDC294AD90748D28ED272021A,
	Object_Internal_InstantiateSingleWithParent_m4702089B268A30D3A6FBE780B7E78833585054E4,
	Object_ToString_m4E499FDF54B8B2727FCCE6EB7BEB976BAB670030,
	Object_GetName_m6F0498EEECA37CD27B052F53ECDDA019129F3D7A,
	Object_SetName_mD80C3B5E390937EF4885BE21FBAC3D91A1C6EC96,
	Object_FindObjectFromInstanceID_m0AD731D3F583CBBDC7DC0E08B38F742B447FA44A,
	Object__ctor_m4DCF5CDB32C2C69290894101A81F473865169279,
	Object__cctor_mD6AB403C7A4ED74F04167372E52C3C876EC422A1,
	Object_Internal_InstantiateSingle_Injected_mD475F2979EDD95363A9AC52EC1BB3E98E6D1D3C2,
	Object_Internal_InstantiateSingleWithParent_Injected_mC883CE3023EAF9F3336A534D5B698C71457ECD97,
	UnitySynchronizationContext__ctor_mF71A02778FCC672CC2FE4F329D9D6C66C96F70CF,
	UnitySynchronizationContext__ctor_m266FAB5B25698C86EC1AC37CE4BBA1FA244BB4FC,
	UnitySynchronizationContext_Send_mEDA081A69B18358B9239E2A46E570466E5FA77F7,
	UnitySynchronizationContext_OperationStarted_mEC5A306F40CB91350A64AAAF6B114B4A994B630A,
	UnitySynchronizationContext_OperationCompleted_m069CD670B8D783A371D51E900B792EDCD71B69A7,
	UnitySynchronizationContext_Post_mE3277DB5A0DCB461E497EC7B9C13850D4E7AB076,
	UnitySynchronizationContext_CreateCopy_mBB5F2E7947732680B0715AD92187E89211AE5E61,
	UnitySynchronizationContext_Exec_mC89E49BFB922E69AAE753887480031A142016F81,
	UnitySynchronizationContext_HasPendingTasks_mBA93E72DC46200231B7ABCFB5F22FB0617A1B1EA,
	UnitySynchronizationContext_InitializeSynchronizationContext_mB581D86F8675566DC3A885C15DC5B9A7B8D42CD4,
	UnitySynchronizationContext_ExecuteTasks_m323E27C0CD442B806D966D024725D9809563E0DD,
	UnitySynchronizationContext_ExecutePendingTasks_m7FD629962A8BA72CB2F51583DC393A8FDA672DBC,
	WorkRequest__ctor_m13C7B4A89E47F4B97ED9B786DB99849DBC2B5603_AdjustorThunk,
	WorkRequest_Invoke_m1C292B7297918C5F2DBE70971895FE8D5C33AA20_AdjustorThunk,
	WaitForEndOfFrame__ctor_mEA41FB4A9236A64D566330BBE25F9902DEBB2EEA,
	WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4,
	WaitForSecondsRealtime_get_waitTime_m04ED4EACCB01E49DEC7E0E5A83789068A3525BC2,
	WaitForSecondsRealtime_set_waitTime_m241120AEE2F1BDD0DC3077D865C7C3D878448268,
	WaitForSecondsRealtime_get_keepWaiting_m96E9697A6DA22E3537EE2ED3823523C05838844D,
	WaitForSecondsRealtime__ctor_m7A69DE38F96121145BE8108B5AA62C789059F225,
	WaitForSecondsRealtime_Reset_m6CB6F149A3EC3BA4ADB6A78FD8A05F82246E9B01,
	WaitUntil_get_keepWaiting_mA846E5325F23E950FC58FD1FD4FC8829E3C54D88,
	WaitUntil__ctor_m4D825CB1C0570631514BFF24AEF1FB17D646C76F,
	YieldInstruction__ctor_mD8203310B47F2C36BED3EEC00CA1944C9D941AEF,
	SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3,
	SerializeReference__ctor_mC40E7D189848A9F9AA44469682822AAC6CF3742E,
	NULL,
	NULL,
	ComputeBuffer_Finalize_m0228E0E952755BECEF3790BA2BCD7F93AB3AFB03,
	ComputeBuffer_Dispose_m2B87F7A44073E119999E0684414768E0F6B810D3,
	ComputeBuffer_Dispose_m7B78DD02830F996D5B943458CE3BF6BF9798338D,
	ComputeBuffer_InitBuffer_m898B1315B258816EAE378CA8BBA170CBA2A4D305,
	ComputeBuffer_DestroyBuffer_mF4D63F7B14F4E1CF2EF115F95393CD3B6B6DE2C6,
	ComputeBuffer__ctor_m66E68E9066197B06CF35FDDAFAFDC8C1DECC5436,
	ComputeBuffer__ctor_mDF3FA4318409FED145709358E0273A296802DB10,
	ComputeBuffer_Release_m7F1D518D1542CD4B961B17E3A44376FA78E977EB,
	ComputeBuffer_SetData_m4A48A0AD92ADDE0C718BD077686C649BCF963EB8,
	ComputeBuffer_InternalSetData_mD66C53B1EBB6939FA9E8353F8FBE7507040E59D1,
	ComputeShader_FindKernel_mCA2683905A5DAB573D50389E2B24B48B18CD53D0,
	ComputeShader_SetVector_mC514C61427474F1B0E29FB667B6F6D1CB7B4A8E9,
	ComputeShader_SetTexture_mA023333A983918D3D846DD315C8FF59AA5EFC62C,
	ComputeShader_Internal_SetBuffer_m40C222A1E6FDF93B6ECD7FB1EDE78034FC30FD12,
	ComputeShader_SetBuffer_m134F25C2DAD8D04F0A6EBBDECB56E8A17E461A3D,
	ComputeShader_Dispatch_mCC2F94FF5EB215C5CC4824741C2CB8D94423CBA4,
	ComputeShader_SetVector_mC2E2E16685ECDDB22D92A6AF19F057CF012D98F9,
	ComputeShader_SetTexture_mE8EA0F8395891E76B8A27E19A1536D817F506E5B,
	ComputeShader_SetBuffer_m4EB505ABF717B7151A1EF0CD7364047432B3EC3C,
	ComputeShader_SetVector_Injected_m50EB28FBAE8176695248910223A8496C0445B633,
	LowerResBlitTexture_LowerResBlitTextureDontStripMe_mFF261E84D36BA5652A3EAB9B45159D6E2FE23A0E,
	PreloadData_PreloadDataDontStripMe_mE542B732FBF4F1E9F01B1D1C2160C43E37E70B7A,
	SystemInfo_get_batteryLevel_m630A753F505B1B642D97439908F01E5E3F972F93,
	SystemInfo_get_batteryStatus_mD6E9FA6A93D6D069E622A44B6DCBA5CC9E6E4F41,
	SystemInfo_get_operatingSystem_mF4A5701333A7EB228DDBEF6548200F204B58BEAF,
	SystemInfo_get_operatingSystemFamily_m797937E766B7FF87A5F1630263C49B814131DD95,
	SystemInfo_get_processorType_mA84DD6B72682E1F078486D5D8CAF9CD6E4FB9C4D,
	SystemInfo_get_processorFrequency_m0310D22751362753B75552EBECAE2014D1D9E08D,
	SystemInfo_get_processorCount_mBAA6A01218CF7F03638A6016B6881466830FD0D5,
	SystemInfo_get_systemMemorySize_m40E0CFB6034F14C4FF249C0940CBEC3E7F167EF0,
	SystemInfo_get_deviceUniqueIdentifier_m12CA3C3D8C75E44FBFA73E2E34D9E743AF732B1E,
	SystemInfo_get_deviceName_m6F221639891D4A635206A39DB2D65584BB708425,
	SystemInfo_get_deviceModel_m99131C20271BDA64F3A537AA009B252DCEDC5977,
	SystemInfo_get_supportsAccelerometer_m69A84B0B73391B1A816B1459A721DB9A21252DC2,
	SystemInfo_get_supportsGyroscope_m342A5CD283DB840C10B5CEC10C997E3E7086CD57,
	SystemInfo_get_supportsLocationService_mB42FEFD42C13D7E9E6BE870C688F0673D462617D,
	SystemInfo_get_supportsVibration_m08CB3483DC65D45284698CD059DAD1CB776FE4CC,
	SystemInfo_get_supportsAudio_mEAEA1E9AF746B866A21B04764497BEF9EA69E058,
	SystemInfo_get_deviceType_mC7A6628167ECFF848FE509510A6E3E2FA0820100,
	SystemInfo_get_graphicsMemorySize_m774717D305DC07B669D39485DEB4895500714E40,
	SystemInfo_get_graphicsDeviceName_m25563DB9012D2DB5EC4CB7A29BA4236926F93F33,
	SystemInfo_get_graphicsDeviceVendor_mA70A62E48AF223EA26B119B5428A5B488A4CA136,
	SystemInfo_get_graphicsDeviceID_mF25513D7FC3A94705110FA00673BEC4D8F7E8218,
	SystemInfo_get_graphicsDeviceVendorID_mC02AC12730B431B84FE0AA74A9F7FDDBA1EEEF40,
	SystemInfo_get_graphicsDeviceType_mC207E6B2221AD5AB39831C2412FF7FBD2F43CC02,
	SystemInfo_get_graphicsUVStartsAtTop_m8A76908C20DE5B8BEE70D7E21C16EE8CC7927501,
	SystemInfo_get_graphicsDeviceVersion_mA74AFB5D881DB29389D2BB05EB37DE60779BABD9,
	SystemInfo_get_graphicsShaderLevel_m2AB377CAE1D1A45C3E05A4ABF40383E3B4797A95,
	SystemInfo_get_graphicsMultiThreaded_m4F18D961FEC9604C0C866F884B99C9FD34C58354,
	SystemInfo_get_renderingThreadingMode_m29074A4856EC00C30886F74167F65895AB2533A2,
	SystemInfo_get_hasHiddenSurfaceRemovalOnGPU_m00DD0BE87F19B47150817BECFADAB712911E8376,
	SystemInfo_get_hasDynamicUniformArrayIndexingInFragmentShaders_m7FE923312B09BD0C756A32CE38F32A67C4F49AD0,
	SystemInfo_get_supportsShadows_mEBD0AB3F30C63DF888C53C50E7690D1D27C05AA3,
	SystemInfo_get_supportsRawShadowDepthSampling_mC563DFC3DF5B1D04C5F619F34C344A648446406F,
	SystemInfo_get_supportsRenderTextures_mEF4C2D195EBF734268B24BAD3599E385FC6AD4B3,
	SystemInfo_get_supportsMotionVectors_mC53D082851F990A59130D6C1B462D709B85E98DC,
	SystemInfo_get_supportsRenderToCubemap_m05B7F9253C53DFDC1C1ABE92EE2EC855A86E8FDE,
	SystemInfo_get_supportsImageEffects_m46EC93D9C657FF09E87B8853F8A42C5E91BAD224,
	SystemInfo_get_supports3DTextures_m1341FD9262C0E2D0BF9F14BFF0192BFABBE23B77,
	SystemInfo_get_supportsCompressed3DTextures_m415A0933067C209C0423AE06AD9D5FA072F08C48,
	SystemInfo_get_supports2DArrayTextures_m08D342F9FDE4826F5D9DF545F5F9059D8954CEEC,
	SystemInfo_get_supports3DRenderTextures_mB649E5BE0F25585DB3BC71B5B350494ECD2DA9A3,
	SystemInfo_get_supportsCubemapArrayTextures_mBA529111F8762D288E759C1F42463D1EFFB1C30E,
	SystemInfo_get_copyTextureSupport_m3C154FFBD2130048D9A5553E063604CC0AADC366,
	SystemInfo_get_supportsComputeShaders_mBEBA178F780915D8BC2FF9ED04D75FBB361987C6,
	SystemInfo_get_supportsGeometryShaders_m91A2CEC097D00CD1DB5ECD6067F7E935515003EC,
	SystemInfo_get_supportsTessellationShaders_mE06FE458554F2950E2CA7A4E006A6181680AD4D6,
	SystemInfo_get_supportsRenderTargetArrayIndexFromVertexShader_m78D7FCC1DBA677E462AE084586333EE554D411CF,
	SystemInfo_get_supportsInstancing_m754DA2DF0A412E7F36D1393F9BA1E9D75EFF48C3,
	SystemInfo_get_supportsHardwareQuadTopology_m337808DB50ED3BDF7FB67FD35771CC949628B7A9,
	SystemInfo_get_supports32bitsIndexBuffer_m4D5EF729115180BAB53C0C461C352ED0FBEC9705,
	SystemInfo_get_supportsSparseTextures_m704A277F55EF530C32A3297AE9D8689B2F4E6254,
	SystemInfo_get_supportedRenderTargetCount_mF1878047AAED7BE6839958B61C0CC7E2FB204B44,
	SystemInfo_get_supportsSeparatedRenderTargetsBlend_mD03C5AA1EB335581BEE0481ED1441D9F972B29DF,
	SystemInfo_get_supportedRandomWriteTargetCount_mE1A7898F7A48F47BD751352C8E7A6D10212ECBC1,
	SystemInfo_get_supportsMultisampledTextures_mFCCDF5D310228864027C1961B1058C76D80F1901,
	SystemInfo_get_supportsMultisampled2DArrayTextures_mB77D5417CE4F68DF92E54BF3F2E6A48262110006,
	SystemInfo_get_supportsMultisampleAutoResolve_mFCC3837F6419EE26E16F3162E8DB8031EBE069A5,
	SystemInfo_get_supportsTextureWrapMirrorOnce_m74AAE160A9692B1618BAA8441027D01FEAF83557,
	SystemInfo_get_usesReversedZBuffer_m89513740D4B01E6954FA0EE01816723B451BD259,
	SystemInfo_get_supportsStencil_m573648A39FD8CA327643D0B7D02095D0DD7AFDE1,
	SystemInfo_IsValidEnumValue_mDF4AFDCB30A42032742988AD9BC5E0E00EFA86C8,
	SystemInfo_SupportsRenderTextureFormat_m243F66021A643C711FEDEA6B6D002B49ECEEFE1B,
	SystemInfo_SupportsTextureFormat_mE7DA9DC2B167CB7E9A864924C8772307F1A2F0B9,
	SystemInfo_get_npotSupport_mB5BA13B7465E9C3EA141BB79EBD79EDF4410C191,
	SystemInfo_get_maxTextureSize_m92A710AC08A38C8BAF96D95D796C073B1C900D40,
	SystemInfo_get_maxCubemapSize_m163BBA5799F96A38A8D60026D84D7BE5C89BA2DC,
	SystemInfo_get_maxComputeBufferInputsVertex_m00265BD65B4A753AC2D47D8E0FC319248E0F4478,
	SystemInfo_get_maxComputeBufferInputsFragment_mE1AE69D8A293459FBF5BA7799CC7B699881D7F4C,
	SystemInfo_get_maxComputeBufferInputsGeometry_m4788CF5D5E5069820EC80803900C0CB8B9CC7CFA,
	SystemInfo_get_maxComputeBufferInputsDomain_m1A41A05DFC5EF0405A99EAE646A5136E210AA8DC,
	SystemInfo_get_maxComputeBufferInputsHull_m79B731A61CA0166B9CDF7DA288F9DBC412CDDAFB,
	SystemInfo_get_maxComputeBufferInputsCompute_m946B192F671A841A5B7EB31398A4EA91F52A0245,
	SystemInfo_get_maxComputeWorkGroupSize_m6287FA4911F343A8C7D05147EC0B5DD06360735E,
	SystemInfo_get_maxComputeWorkGroupSizeX_m7AFB3674CC79EBE0A4D3917E357129E4DCD95063,
	SystemInfo_get_maxComputeWorkGroupSizeY_m2ECD394008E42301F4B6AD4836FA7FA193189384,
	SystemInfo_get_maxComputeWorkGroupSizeZ_m60E55E49A7D2C4B3CE8C431E52BA2EB74C527610,
	SystemInfo_get_supportsAsyncCompute_m54699BD7BEB7AF7D195964E94F982B33F1C2AC6F,
	SystemInfo_get_supportsGpuRecorder_mBE41C4EE6F32832CB0F0C5CCA493A04C3FB759E4,
	SystemInfo_get_supportsGraphicsFence_mB1F2A268F648FF77C72070B99FBA1FA00881907A,
	SystemInfo_get_supportsAsyncGPUReadback_mC4DFC36451A1739A0DDA4614E434657CE6B196DF,
	SystemInfo_get_supportsRayTracing_m13DB5EB131598DB2FA563B333B7CD13A9B6390D0,
	SystemInfo_get_supportsSetConstantBuffer_m87E9386FCBDF9E64AB73FD6A5F6D1CDFA42C0A94,
	SystemInfo_get_minConstantBufferOffsetAlignment_mCB41A7EADCAB74E469CDDA9AE841A253CCF865DE,
	SystemInfo_get_hasMipMaxLevel_mB1B7C8C620AB827C4ADA8BC4FE1672C51D2A13CD,
	SystemInfo_get_supportsMipStreaming_m5AC48A0069CEA79C3E9DC896EF255336C5BAAD29,
	SystemInfo_get_graphicsPixelFillrate_mBDCFF1E3B471EB873760C493E1696E2670275236,
	SystemInfo_get_usesLoadStoreActions_mC70423023F6787FB3C35255E4E3D42DF0155EC62,
	SystemInfo_get_hdrDisplaySupportFlags_mD29DC579D697DBA774D883341FCD60D63164F78C,
	SystemInfo_get_supportsConservativeRaster_m04DBEBD8EF6ACEE88D6C0C72EA5B686FBAF1448E,
	SystemInfo_get_supportsVertexPrograms_m39A944A131FDBF276D5FD951A4A10C8F27C04492,
	SystemInfo_GetBatteryLevel_mC7AD536404709DBCBE392E60D627F5C789ABB064,
	SystemInfo_GetBatteryStatus_mB9D79AD7FA05CA452714A2A468D1F804B4664BB5,
	SystemInfo_GetOperatingSystem_m43505157C3545D360163565BB8E9EDDCDFA17062,
	SystemInfo_GetOperatingSystemFamily_mA28F08DC50049D25B1C1FB0E8F5C6EF00C7FEFCD,
	SystemInfo_GetProcessorType_mCE5AB6E00F496E308B5C8FC37E26B8A8EEEF4A75,
	SystemInfo_GetProcessorFrequencyMHz_mABE320F475286FBDBCBFB7DA6271C32C905C117C,
	SystemInfo_GetProcessorCount_m068A033A6F098AC920AA05A54F136876A9F387D1,
	SystemInfo_GetPhysicalMemoryMB_m4131DA4696B7AC99F56545D91881E55D510E9659,
	SystemInfo_GetDeviceUniqueIdentifier_mEFC98563366CED1C69EDDC6D5608D6BA8A4FC753,
	SystemInfo_GetDeviceName_m8461B7A550FFF7711555938D487779B734F663C3,
	SystemInfo_GetDeviceModel_m72051C6D2BBAAEFB8AEE4212E175D3C85045E49E,
	SystemInfo_SupportsAccelerometer_m281D6BCE55B08C3B12035512F982A632FE2F3CB2,
	SystemInfo_IsGyroAvailable_m9F8DCB923F69916AB300C06E1A77E5885F47805F,
	SystemInfo_SupportsLocationService_m61FC0DE7CE79AF165DCF60679C49E2FDFBBF7242,
	SystemInfo_SupportsVibration_m7EAAA4B0741D8BA5C50AF39962E602F5250F7CB8,
	SystemInfo_SupportsAudio_m0B4FE034E202629F3F383CBF40E2EBF09EF32148,
	SystemInfo_GetDeviceType_m0FA185D60B7A50C01A3AFB22AAE47C585E0F6BAB,
	SystemInfo_GetGraphicsMemorySize_mA3DC96C958BD9F685C3A9A112A22AC4CACE7A2B3,
	SystemInfo_GetGraphicsDeviceName_mFD99DAEC5AF5CEF9E5BD5297DA94A0A79DF5EE9C,
	SystemInfo_GetGraphicsDeviceVendor_mCE52C892914AD6FC791F88DD7AE2ACE28E641863,
	SystemInfo_GetGraphicsDeviceID_m3E4C5B6836E823CAE2A53BDD34C61DF78ADC473A,
	SystemInfo_GetGraphicsDeviceVendorID_mB3EC171381D1AE243B10C5BB0F6690E07E8F67F0,
	SystemInfo_GetGraphicsDeviceType_m8E30DF86F5AE77EBB1752A55B183890C3ACA8443,
	SystemInfo_GetGraphicsUVStartsAtTop_m05A4A723EEB7FBCB3E55C3A417AA03AF0B021E4D,
	SystemInfo_GetGraphicsDeviceVersion_m0AB62B4AC54343A28B8CAA0AEE59B22E281F1D71,
	SystemInfo_GetGraphicsShaderLevel_m5FA0FBC7F9E2EB33F845D4481BBD2E703FDC9774,
	SystemInfo_GetGraphicsMultiThreaded_m1D5B91AD64CB6959974C0900983F93C5F4B3EF22,
	SystemInfo_GetRenderingThreadingMode_m682EEFF66E4F688ACA133E371FF891F8D30C3F85,
	SystemInfo_HasHiddenSurfaceRemovalOnGPU_m1A886FAE9CA31DD2C7102C8ECCC715D04B01A2E3,
	SystemInfo_HasDynamicUniformArrayIndexingInFragmentShaders_m3699977B68DBBEC76C54608D887B0B42EBC52B35,
	SystemInfo_SupportsShadows_m32C81C01678FE781DB83DE30138DD6CAC58A3436,
	SystemInfo_SupportsRawShadowDepthSampling_mF8A5C69333BDBE965E65BA0D0219E3D0B93EB5B9,
	SystemInfo_SupportsMotionVectors_m1C2121EDB64B43A0EABDD42C933F127B5002FE8B,
	SystemInfo_Supports3DTextures_m592B058E2C3B1555B82F9CC819B7556D991B2B3A,
	SystemInfo_SupportsCompressed3DTextures_m335562164806358C9F2A3646501FEEF3EA7F9800,
	SystemInfo_Supports2DArrayTextures_m4945FB9A31EC8ACD1AA625D81C10E994CFF183AB,
	SystemInfo_Supports3DRenderTextures_m7833E0995D300986C7AF65B2B0BF4FC70E4DF68F,
	SystemInfo_SupportsCubemapArrayTextures_m76069C3C8035FA1FC2B17E8D881079E79BEF4E26,
	SystemInfo_GetCopyTextureSupport_m65B2F7B818C998EF26B7E497B9EC307AB2C89FB0,
	SystemInfo_SupportsComputeShaders_mCFF50395E4EC20377D30E391E412B434798B0B5A,
	SystemInfo_SupportsGeometryShaders_mC7508CCCDF1C21C0F0A15E2059462CA030D9E7D6,
	SystemInfo_SupportsTessellationShaders_m399FD1050199877B83B1AFF6793D859E04FC087F,
	SystemInfo_SupportsRenderTargetArrayIndexFromVertexShader_m281AE541D587F7EE4619FC377FE0AD804DDB0D5A,
	SystemInfo_SupportsInstancing_mBE34CD192E396397325D952C07B8BE73933173F2,
	SystemInfo_SupportsHardwareQuadTopology_m4F53B514058B7D60B4F7F2B836CBD90BB25A8000,
	SystemInfo_Supports32bitsIndexBuffer_mF7CA00A1DDC18C9EC8AA423FE8A29D78E548C5A8,
	SystemInfo_SupportsSparseTextures_m5A896E23280539E34139FC148A8C9515129AA879,
	SystemInfo_SupportedRenderTargetCount_mC0531AA9339B4BE7D7FEF57C4716560DD09A8F17,
	SystemInfo_SupportsSeparatedRenderTargetsBlend_m49D9FD7844682EA0F445B0617C3D7518ED536B56,
	SystemInfo_SupportedRandomWriteTargetCount_m0D8FEF1A53DA52747D072898F0A7FF02158DFDEA,
	SystemInfo_MaxComputeBufferInputsVertex_mAC4976451226A32E82E1703506A042B9AA89D0A8,
	SystemInfo_MaxComputeBufferInputsFragment_m39AC4CB4FB9202A71F7247CFB220C216BEC498DA,
	SystemInfo_MaxComputeBufferInputsGeometry_m98DED828662FE955597757899FA785EAC3924BC4,
	SystemInfo_MaxComputeBufferInputsDomain_mDBACD7DC42E017D2C7273CDE441063881CEAF787,
	SystemInfo_MaxComputeBufferInputsHull_m1F241A825D0514C96839FFC4B0D51179F94E2EF9,
	SystemInfo_MaxComputeBufferInputsCompute_mA76DDF21EC4973FC89A14B06B12B900871EA8A60,
	SystemInfo_SupportsMultisampledTextures_mDFF30091256075AE6D87F899521F454B45EF60B4,
	SystemInfo_SupportsMultisampled2DArrayTextures_m0D8DB9C8B25F2288F7BAB8B14E2A00A1877372A0,
	SystemInfo_SupportsMultisampleAutoResolve_m68A929965B7FDFA3157309A78A13CB6DF9A4AD81,
	SystemInfo_SupportsTextureWrapMirrorOnce_m81A08C77E2955F724704F315B3902F8CBB37958C,
	SystemInfo_UsesReversedZBuffer_m35DBEC617F6E8926ED483E06CC5CA4433606C4E0,
	SystemInfo_HasRenderTextureNative_mCAFDC38C167B083925D7F546A1AE1053AFE9626E,
	SystemInfo_SupportsTextureFormatNative_m1514BFE543D7EE39CEF43B429B52E2EC20AB8E75,
	SystemInfo_GetNPOTSupport_m039DB2B1D26432DF11CAC86D790EC6F5DCBBC12F,
	SystemInfo_GetMaxTextureSize_m75E58620AC261FD77800B8D94FA3865F55A3A906,
	SystemInfo_GetMaxCubemapSize_m28676F0ED1FFC3168EBD04FA1C782E14C7411C05,
	SystemInfo_GetMaxComputeWorkGroupSize_m96B34356E4F25BD5845BE11FCB11F9B755700C8C,
	SystemInfo_GetMaxComputeWorkGroupSizeX_mF7C75E96C7AAFF457B18A383861BED022A25A550,
	SystemInfo_GetMaxComputeWorkGroupSizeY_m0BCDEECB257B654F928C72CA4AE512A8CFE53F08,
	SystemInfo_GetMaxComputeWorkGroupSizeZ_mC3712B2429A16B5459EFA04EE9D3258B595F51B7,
	SystemInfo_SupportsAsyncCompute_mD3C93816DFB853CF3E229CE3533FD68AA5514A8E,
	SystemInfo_SupportsGpuRecorder_m90284551360D6358431B6FAB466A389CC6EA2D0B,
	SystemInfo_SupportsGPUFence_mF2432380CAD096A5EAA1966BE656D2C2715BBD2A,
	SystemInfo_SupportsAsyncGPUReadback_m1EC85AEF71882930BAE9149FEF51C753E8AC7400,
	SystemInfo_SupportsRayTracing_mBAC93D4F93BCA615F3C560A00A41F2949FDEDD86,
	SystemInfo_SupportsSetConstantBuffer_mBDD353A1F98DFD1A16D574B0E21C408973A9612C,
	SystemInfo_MinConstantBufferOffsetAlignment_mDF1F9AA4FD7FF4632750BF3F336A97B32BDFAB75,
	SystemInfo_HasMipMaxLevel_m3E6616FF2CBCD69A497E10D624A2B9ED37E072FF,
	SystemInfo_SupportsMipStreaming_m7CAB18917327CC364D7EA0A754F6CEFABFF8F484,
	SystemInfo_IsFormatSupported_m03EDA316B42377504BA47B256EB094F8A54BC75C,
	SystemInfo_GetCompatibleFormat_m02B5C5B1F3836661A92F3C83E44B66729C03B228,
	SystemInfo_GetGraphicsFormat_mE36FE85F87F085503FEAA34112E454E9F2AFEF55,
	SystemInfo_UsesLoadStoreActions_mCA7356EC82B95D4C3897221CFDB8908C68553E4B,
	SystemInfo_GetHDRDisplaySupportFlags_m2F31E0179C6A8B9E6D6D16AF132FE4C507A77E9D,
	SystemInfo_SupportsConservativeRaster_m515C655351F5973A04F78917A09CE46F3154EE28,
	SystemInfo_get_supportsGPUFence_m785FB762A86266D6F2D869FED86105D2234DBC66,
	SystemInfo__ctor_m8F4A5CF956C47EFC15614BF2C14CD18CFFA5C111,
	Time_get_time_m1A186074B1FCD448AB13A4B9D715AB9ED0B40844,
	Time_get_timeSinceLevelLoad_m47A90DE6CB3A3180D64F0049290BC72C186FC7FB,
	Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290,
	Time_get_fixedTime_mE4630B2127C67597CA450A81FFDF25E63A2CC7B5,
	Time_get_unscaledTime_m85A3479E3D78D05FEDEEFEF36944AC5EF9B31258,
	Time_get_unscaledDeltaTime_m2C153F1E5C77C6AF655054BC6C76D0C334C0DC84,
	Time_get_fixedDeltaTime_m8E94ECFF6A6A1D9B5D60BF82D116D540852484E5,
	Time_get_smoothDeltaTime_mA6EAFF9940EC75075565444A61A03895489CA96A,
	Time_get_timeScale_m082A05928ED5917AA986FAA6106E79D8446A26F4,
	Time_set_timeScale_m1987DE9E74FC6C0126CE4F59A6293E3B85BD01EA,
	Time_get_frameCount_m8601F5FB5B701680076B40D2F31405F304D963F0,
	Time_get_realtimeSinceStartup_m5228CC1C1E57213D4148E965499072EA70D8C02B,
	TouchScreenKeyboard_Internal_Destroy_m59FBAD63BC41007D106FA59C3378D547F67CA00D,
	TouchScreenKeyboard_Destroy_m2FFBCD2EF26EF68B394874335BA6DA21B95F65D2,
	TouchScreenKeyboard_Finalize_m3C44228F58044B8132724CF9BD1A1B2354EBB76E,
	TouchScreenKeyboard__ctor_mA82A33DB603000BB9373F70744D0774BAD5714F4,
	TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_mAAF0AC4D0E6D25AAFC9F71BF09447E053261EADB,
	TouchScreenKeyboard_get_isSupported_m0DB9F5600113241DD766588D28192A62185C158F,
	TouchScreenKeyboard_get_isInPlaceEditingAllowed_m8364EE991616DCA6A1BDDA598F93D577B68491FC,
	TouchScreenKeyboard_IsInPlaceEditingAllowed_m8328A436685488B94FDBE2E99F3E7992C3977ECC,
	TouchScreenKeyboard_Open_mE7311250DC20FBA07392E4F61B71212437956B6E,
	TouchScreenKeyboard_get_text_m46603E258E098841D53FE33A6D367A1169BDECA4,
	TouchScreenKeyboard_set_text_m8BA9BBE790EA59FFE1E55FE25BD05E85CEEE7A27,
	TouchScreenKeyboard_set_hideInput_m7A3F11FC569433CF00F71284991849E72E934D6F,
	TouchScreenKeyboard_get_active_m07DBA2A13D1062188AB6BE05BAA61C90197E55E2,
	TouchScreenKeyboard_set_active_m506FA44E4FA49466735258D0257AC14AAC6AC245,
	TouchScreenKeyboard_get_status_m05FBF0EF6E13308E24CDCD4259F0A532040F08D9,
	TouchScreenKeyboard_set_characterLimit_mE662ED65DD8BF31608A1E0C697053622893EC9DC,
	TouchScreenKeyboard_get_canGetSelection_m979FF4BC5D792F38CD9814DB2603EFA67C88EFF8,
	TouchScreenKeyboard_get_canSetSelection_mC75BB2BE09235F3B8BD5805C5D8F1097C3AAD442,
	TouchScreenKeyboard_get_selection_m3C092ED46B21E0C7BD694F5E9F2C7529F9D123E3,
	TouchScreenKeyboard_set_selection_mB53A2F70AAD20505589F58A61A086777BA8645AD,
	TouchScreenKeyboard_GetSelection_mE5F74F635FED7B7E2CA492AEB5B83EC316EB4E0E,
	TouchScreenKeyboard_SetSelection_mE48DEBFF4B65FD885A3A6C8009D61F086D758DC4,
	Pose__ctor_m57138889AE9BF5AFB50D31A007F6EE062991E8C9_AdjustorThunk,
	Pose_ToString_m9958D8F32A381D4ADB8DEABDE63561F75A12962B_AdjustorThunk,
	Pose_Equals_m93B87D733C2FBE5B400140040826521DCA4A2BB5_AdjustorThunk,
	Pose_Equals_mFAE0041090F2032B8FCE63A0F289DD206134BF15_AdjustorThunk,
	Pose_GetHashCode_mE1DD7FBCCB1C979B252F0B117276BA11CF4D2367_AdjustorThunk,
	Pose__cctor_m4E5498207D9033E7DB4B1C255ABDBE8F746F89F8,
	DrivenRectTransformTracker_Add_m65814604ABCE8B9F81270F3C2E1632CCB9E9A5E7_AdjustorThunk,
	DrivenRectTransformTracker_Clear_m41F9B0AA2025AF5B76D38E68B08C111D7D8EB845_AdjustorThunk,
	RectTransform_add_reapplyDrivenProperties_mCD8CB43C59C3C04528C842E4640AD1DC5B71F043,
	RectTransform_remove_reapplyDrivenProperties_m2F771726CC09F7CF3E9194B72D87ABB3B61001F1,
	RectTransform_get_rect_m7B24A1D6E0CB87F3481DDD2584C82C97025404E2,
	RectTransform_get_anchorMin_m5CBB2E649A3D4234A7A5A16B1BBAADAC9C033319,
	RectTransform_set_anchorMin_mD9E6E95890B701A5190C12F5AE42E622246AF798,
	RectTransform_get_anchorMax_mC1577047A20870209C9A6801B75FE6930AE56F1E,
	RectTransform_set_anchorMax_m67E04F54B5122804E32019D5FAE50C21CC67651D,
	RectTransform_get_anchoredPosition_mFDC4F160F99634B2FBC73FE5FB1F4F4127CDD975,
	RectTransform_set_anchoredPosition_m8143009B7D2B786DF8309D1D319F2212EFD24905,
	RectTransform_get_sizeDelta_mCFAE8C916280C173AB79BE32B910376E310D1C50,
	RectTransform_set_sizeDelta_m61943618442E31C6FF0556CDFC70940AE7AD04D0,
	RectTransform_get_pivot_m146F0BB5D3873FCEF3606DAFB8994BFA978095EE,
	RectTransform_set_pivot_m94F32EF88DC4EC9CA96721F8EDD8BFBC4FD07335,
	RectTransform_get_offsetMin_m6573EC7E31B25028A35C21D4195D96EA94645E5C,
	RectTransform_set_offsetMin_m86D7818770137C150B70A3842EBF03F494C34271,
	RectTransform_get_offsetMax_m27465B00CD222399CC8B3BB53A4A68CF342319C6,
	RectTransform_set_offsetMax_m5FDE1063C8BA1EC98D3C57C58DD2A1B9B721A8BF,
	RectTransform_GetLocalCorners_mA93C3DA0EF4915A399E111F23E8B0037FB0D897C,
	RectTransform_GetWorldCorners_m5351A825540654FFDBD0837AC37D2139F64A4FD8,
	RectTransform_SetSizeWithCurrentAnchors_m69641A375B011EA52C69C5E2553406FFB819F44B,
	RectTransform_SendReapplyDrivenProperties_m9139950FCE6E3C596110C5266174D8B2E56DC9CD,
	RectTransform_GetParentSize_mB360151D47F306B0614F87B85402156C8FD949D5,
	RectTransform_get_rect_Injected_m9E2423A68A47664E62278AF461D5F5E8444E3E63,
	RectTransform_get_anchorMin_Injected_m591E30B205148C8EE6B40DEFF59C26578B269E86,
	RectTransform_set_anchorMin_Injected_mE7DC3F6291CD07ECE04F3E602395B1E8C841B9DB,
	RectTransform_get_anchorMax_Injected_m66E954822B58B90A6C0BCF215BF8ADECF2AE82A5,
	RectTransform_set_anchorMax_Injected_m5C4650BC3A0CB3F5B9BB42020ED98310ED217D9F,
	RectTransform_get_anchoredPosition_Injected_mEAE78E52E8C07DF7C3FD72FC31E675557B7D2D21,
	RectTransform_set_anchoredPosition_Injected_m5F082F2C7BECB268DD87C04857157E2C50C44FB9,
	RectTransform_get_sizeDelta_Injected_mCDC20F4A6886D32FD2450EF690EA8B067769C093,
	RectTransform_set_sizeDelta_Injected_m7693B136F6C2F35B06D21E813FE4D90007D0FCEA,
	RectTransform_get_pivot_Injected_m37B7AD78DD72F2A181EC5B06AB9499EB11D20EB3,
	RectTransform_set_pivot_Injected_m9FE95D2C721B381940FCDA8D202B3A3AC5B03B56,
	ReapplyDrivenProperties__ctor_mD584B5E4A07E3D025352EA0BAE9B10FE5C13A583,
	ReapplyDrivenProperties_Invoke_m5B39EC5205C3910AC09DCF378EAA2D8E99391636,
	ReapplyDrivenProperties_BeginInvoke_mC7625A8FDFF392D73C7828526490DCB88FD87232,
	ReapplyDrivenProperties_EndInvoke_m89A593999C130CA23515BF8A9C02DDE5B39ECF67,
	Transform__ctor_m629D1F6D054AD8FA5BD74296A23FCA93BEB76803,
	Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341,
	Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91,
	Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2,
	Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC,
	Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F,
	Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E,
	Transform_get_localEulerAngles_m4C442107F523737ADAB54855FDC1777A9B71D545,
	Transform_set_localEulerAngles_mB63076996124DC76E6902A81677A6E3C814C693B,
	Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE,
	Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31,
	Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053,
	Transform_set_forward_mAE46B156F55F2F90AB495B17F7C20BF59A5D7D4D,
	Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200,
	Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4,
	Transform_get_localRotation_mA6472AE7509D762965275D79B645A14A9CCF5BE5,
	Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C,
	Transform_get_localScale_mD9DF6CA81108C2A6002B5EA2BE25A6CD2723D046,
	Transform_set_localScale_mF4D1611E48D1BA7566A1E166DC2DACF3ADD8BA3A,
	Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9,
	Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13,
	Transform_get_parentInternal_m6477F21AD3A2B2F3FE2C365B1AF64BB1AFDA7B4C,
	Transform_set_parentInternal_mED1BC58DB05A14DAC354E5A4B24C872A5D69D0C3,
	Transform_GetParent_mA53F6AE810935DDED00A9FEEE1830F4EF797F73B,
	Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E,
	Transform_SetParent_mA6A651EDE81F139E1D6C7BA894834AD71D07227A,
	Transform_get_worldToLocalMatrix_mE22FDE24767E1DE402D3E7A1C9803379B2E8399D,
	Transform_get_localToWorldMatrix_m6B810B0F20BA5DE48009461A4D662DD8BFF6A3CC,
	Transform_Translate_mFB58CBF3FA00BD0EE09EC67457608F62564D0DDE,
	Transform_Translate_mC9343E1E646DA8FD42BE37137ACCBB4B52093F5C,
	Transform_Rotate_m61816C8A09C86A5E157EA89965A9CC0510A1B378,
	Transform_Rotate_m027A155054DDC4206F679EFB86BE0960D45C33A7,
	Transform_Rotate_mE77655C011C18F49CAD740CED7940EF1C7000357,
	Transform_Rotate_mA3AE6D55AA9CC88A8F03C2B0B7CB3DB45ABA6A8E,
	Transform_RotateAroundInternal_mEEC98EFC71E388DB313FE62D9FAB642724A38216,
	Transform_Rotate_m12614C5FABB1F4A9A6800EE65BBFDB433D6D804D,
	Transform_Rotate_m2AA745C4A796363462642A13251E8971D5C7F4DC,
	Transform_RotateAround_m1F93A7A1807BE407BD23EC1BA49F03AD22FCE4BE,
	Transform_LookAt_m6037828F5E8329B052D62472060274F9A3261ECA,
	Transform_LookAt_m6BB4B39BB829A451C2F63215361D27650AA24D8C,
	Transform_LookAt_m996FADE2327B0A4412FF4A5179B8BABD9EB849BA,
	Transform_Internal_LookAt_m1A24125A99A766EDA6059424EA3B5FA9C5E8B61B,
	Transform_TransformDirection_m6B5E3F0A7C6323159DEC6D9BC035FB53ADD96E91,
	Transform_TransformPoint_m68AF95765A9279192E601208A9C5170027A5F0D2,
	Transform_InverseTransformPoint_m476ABC8F3F14824D7D82FE2C54CEE5A151A669B8,
	Transform_get_childCount_mCBED4F6D3F6A7386C4D97C2C3FD25C383A0BCD05,
	Transform_SetAsFirstSibling_mD5C02831BA6C7C3408CD491191EAF760ECB7E754,
	Transform_FindRelativeTransformWithPath_m8B6DE13079DE11DCCDD2CA40CEC59319FD70A12D,
	Transform_Find_mB1687901A4FB0D562C44A93CC67CD35DCFCAABA1,
	Transform_get_lossyScale_m469A16F93F135C1E4D5955C7EBDB893D1892A331,
	Transform_IsChildOf_m1783A88A490931E98F4D5E361595A518E09FD4BC,
	Transform_set_hasChanged_mD1CDCAE366DB514FBECD9DAAED0F7834029E1304,
	Transform_GetEnumerator_mBA0E884A69F0AA05FCB69F4EE5F700177F75DD7E,
	Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C,
	Transform_get_position_Injected_m43CE3FC8FB3C52896D709B07EB77340407800C13,
	Transform_set_position_Injected_m634DE2302555154065001583E6080CC48D58A602,
	Transform_get_localPosition_Injected_mBBD4D1AAD893D9B5DB40E9946A40E2B94E688782,
	Transform_set_localPosition_Injected_m228521F584224C612AEF8ED500AABF31C8E87E02,
	Transform_get_rotation_Injected_m1F756C98851F36F25BFBAC3401B67A4D2F176DF1,
	Transform_set_rotation_Injected_m9ACF0891D219140A329411F33858C7B0A026407F,
	Transform_get_localRotation_Injected_mCF48B92BAD51A015698EFE3973CD2F595048E74B,
	Transform_set_localRotation_Injected_m19EF26CC5E0F8331297D3FB17EFFC7FD217A9FCA,
	Transform_get_localScale_Injected_mC3D90F76FF1C9876761FBE40C5FF567213B86402,
	Transform_set_localScale_Injected_m7247850A81ED854FD10411376E0EF2C4F7C50B65,
	Transform_get_worldToLocalMatrix_Injected_m8B625E30EDAC79587E1D73943D2486385C403BB1,
	Transform_get_localToWorldMatrix_Injected_m990CE30D1A3D41A3247D4F9E73CA8B725466767B,
	Transform_RotateAroundInternal_Injected_m70D2B2635B5CB12008A8207829D178BF4970E9BD,
	Transform_Internal_LookAt_Injected_mE9622F74A863FA172B36D3F453BE0AA4BDEAC092,
	Transform_TransformDirection_Injected_mB27ACC95D32503153AEBE93976553C0A41BF4D2E,
	Transform_TransformPoint_Injected_mFCDA82BF83E47142F6115E18D515FA0D0A0E5319,
	Transform_InverseTransformPoint_Injected_mC6226F53D5631F42658A5CA83FEE16EC24670A36,
	Transform_get_lossyScale_Injected_m5D97E86A5663FF57E1977E5CA13EF00022B61648,
	Enumerator__ctor_m052C22273F1D789E58A09606D5EE5E87ABC2C91B,
	Enumerator_get_Current_m1CFECBB7AC3EACD05A11CC6848AE7A94A8123E9F,
	Enumerator_MoveNext_m346F9A121D9E89ADBA8296E6A7EF8763C5B58A14,
	Enumerator_Reset_mFA289646E280C94D82CC223C024E0B615F811C8E,
	Sprite__ctor_m121D88C6A901A2A2FA602306D01FDB8D7A0206F0,
	Sprite_GetPackingMode_m398C471B7DDCCA1EA0355217EBB7568E851E597A,
	Sprite_GetPacked_m6AC29F35C9ADE1B6394202132FB77DA9249DF5AE,
	Sprite_GetTextureRect_m6E19823AEA9A3FC4C9FE76E53C30F19316F63954,
	Sprite_GetInnerUVs_m394AF466930BBACE6F45425C418D0A8991600AD9,
	Sprite_GetOuterUVs_mEB9D18CA03A78C02CAF4FAD386A7AF009187ACDD,
	Sprite_GetPadding_mA039E911719B85FBB31F4C235B9EF9973F5E7FF3,
	Sprite_get_bounds_m364F852DE78702F755D1414FF4465F61F3F238EF,
	Sprite_get_rect_m146D3624E5D8DD6DF5B1F39CE618D701B9008C70,
	Sprite_get_border_m6AEB051C1A675509BB786427883FC2EE957F60A7,
	Sprite_get_texture_mD03E68058C9F727321FE643CBDB3A469F96E49FB,
	Sprite_get_pixelsPerUnit_mEA3201EE604FB43CB93E3D309B19A5D0B44C739E,
	Sprite_get_associatedAlphaSplitTexture_m212E3C39E4EE3385866E51194F5FC9AEDDEE4F00,
	Sprite_get_pivot_m39B1CFCDA5BB126D198CAEAB703EC39E763CC867,
	Sprite_get_packed_m075910C79D785DC2572B171DA93918CF2793B133,
	Sprite_get_packingMode_m1BF2656F34C1C650D1634F0AE81727074BE85E5F,
	Sprite_get_textureRect_m5B350C2B122C85549960912CBD6343E4A5B02C35,
	Sprite_get_vertices_m4A5EFBEDA14F12E5358C61831150AE368453F301,
	Sprite_get_triangles_mAE8C32A81703AEF45192E993E6B555AF659C5131,
	Sprite_get_uv_mBD902ADCF1FF8AE211C98881A6E3C310D73494B6,
	Sprite_GetTextureRect_Injected_m5D5B55E003133B5A537764AF7493BC094685F2BD,
	Sprite_GetInnerUVs_Injected_m6BBD450F64FCAA0EE51E16034E239267E53BADB7,
	Sprite_GetOuterUVs_Injected_m386A7B21043ED228AE4BBAB93060AFBFE19C5BD7,
	Sprite_GetPadding_Injected_m9C8743817FB7CD12F88DA90769BD653EA35273EE,
	Sprite_get_bounds_Injected_m4AE096B307AD9788AEDA44AF14C9605D5ABEEE1C,
	Sprite_get_rect_Injected_mE5951AA7D9D0CBBF4AF8263F8B77B8B3E203279D,
	Sprite_get_border_Injected_m7A2673F6D49E5085CA3CC2436763C7C7BE0F75C8,
	Sprite_get_pivot_Injected_mAAE0A9705B766EB97C8732BE5541E800E8090809,
	APIUpdaterRuntimeHelpers_GetMovedFromAttributeDataForType_mEDA7447F4AEBCBDE3B6C5A04ED735FA9BA2E7B52,
	APIUpdaterRuntimeHelpers_GetObsoleteTypeRedirection_mAD9DCC5AEEF51535CB9FCED2F1B38650C766D355,
	DataUtility_GetInnerUV_mDAA53C8F613CBB89345EE978D14599F5EE04891C,
	DataUtility_GetOuterUV_mC6B306F20527EE5490505B8A5929C70C842AB966,
	DataUtility_GetPadding_m6300930863B61A94EDF09C10C88668AA94E4EBD4,
	DataUtility_GetMinSize_mEDB6E71839C3EA17052EE74D2FEBDE1D2F7D0081,
	SpriteAtlasManager_RequestAtlas_m4EB540E080D8444FE4B53D8F3D44EA9C0C8C49A1,
	SpriteAtlasManager_add_atlasRegistered_mE6C9446A8FA30F4F4B317CFCFC5AE98EE060C3FE,
	SpriteAtlasManager_remove_atlasRegistered_m9B9CFC51E64BF35DFFCBC83EF2BBCDDA3870F0CE,
	SpriteAtlasManager_PostRegisteredAtlas_m0F58C324E58E39D7B13803FBF7B1AE16CF6B4B7B,
	SpriteAtlasManager_Register_m48E996EAD9A5CF419B7738799EB99A78D7095C73,
	SpriteAtlasManager__cctor_mDB99D76724E2DB007B46B61C2833878B624D5021,
	SpriteAtlas_CanBindTo_m01D0066BE9609582194ADA0DA70E598530DACF03,
	SpriteAtlas_GetSprite_mBDFF27666D4C361D5A5E6F55421A658A045C8DDB,
	Profiler_GetMonoUsedSizeLong_mFE5483CFA8A430C8BC7A44EB67A4C244DFF0CE02,
	DebugScreenCapture_set_rawImageDataReference_m9FE7228E0FB4696A255B2F477B6B50C902D7786B_AdjustorThunk,
	DebugScreenCapture_set_imageFormat_m46E9D97376E844826DAE5C3C69E4AAF4B7A58ECB_AdjustorThunk,
	DebugScreenCapture_set_width_m6928DB2B2BCD54DE97BDA4116D69897921CCACF6_AdjustorThunk,
	DebugScreenCapture_set_height_m6CD0F6872F123966B784E6F356C51986B8B19F84_AdjustorThunk,
	MetaData__ctor_mD7868B95DDB386C2F8AC614F09A6760F420A44D5,
	MemoryProfiler_PrepareMetadata_m571D85DE9BEAF3D3E0ED8269AE350960717D947E,
	MemoryProfiler_WriteIntToByteArray_mEC9056AEB48E7906BAA9FDA7FF538E7341233E8E,
	MemoryProfiler_WriteStringToByteArray_m48936038ADD56D3BF498870F4DA6AB12DE0CA9FC,
	MemoryProfiler_FinalizeSnapshot_m7DE2A0E49B6457B64D53255BE00F7F1C7EA30526,
	MemoryProfiler_SaveScreenshotToDisk_m9419808BAC900EAB213522E34F6268975722495A,
	WindowSizeChanged__ctor_mE184BC9192055FE6767E1E439A81B80A2FD5FCDF,
	WindowSizeChanged_Invoke_m9C5F9E8BDC77F578888B3B35273D0814D20E824A,
	WindowSizeChanged_BeginInvoke_m39C9F6101934C25EC32001AA9EB4216974D564DC,
	WindowSizeChanged_EndInvoke_mD6C472713445F18BD17E54BCACDD415D81921164,
	WindowActivated__ctor_m2725FDD17B58A484E5B2B5353E7892ADED8B6B05,
	WindowActivated_Invoke_mF077F7624898BA59AA03F39842F630D37BEA963E,
	WindowActivated_BeginInvoke_m4D5F0E5480B306DDEE4F135DD6646F66086A62DB,
	WindowActivated_EndInvoke_m186D0562CF86D246C08B253113E3B3EC2B82D065,
	Application_InvokeWindowSizeChangedEvent_mA7AC1CFF5F8477230890ADEE3408A28D75E78FC6,
	Application_InvokeWindowActivatedEvent_mAFCB006888BBE4F8499B3016782F75B3F8B731F7,
	PhraseRecognitionSystem_PhraseRecognitionSystem_InvokeErrorEvent_mAFD4E8DC3319962E46ADBCD2BB85DCB8381D52CF,
	PhraseRecognitionSystem_PhraseRecognitionSystem_InvokeStatusChangedEvent_m6BAFA3514D9614847F28720F792FF62F1E623577,
	ErrorDelegate__ctor_m2F691DE208DD6430077E3C17D4A5293AA9002FF4,
	ErrorDelegate_Invoke_m46CECD81317CD9717E11456CA73E6B14B88E6724,
	ErrorDelegate_BeginInvoke_mD2A98805D514FF2FE986EAB635AECA18A6CB42F8,
	ErrorDelegate_EndInvoke_mBA12D5FE7D6C9A5615AF473703969C011EDCCA3B,
	StatusDelegate__ctor_m8D463CDB5084EA78077004167A884277C11C8580,
	StatusDelegate_Invoke_m04916521B4B6FD2EAFF4A9DF96628F8C34E0C8E2,
	StatusDelegate_BeginInvoke_m5E20320BB35F9113700B3DA0DFAEF01DEE14DAC8,
	StatusDelegate_EndInvoke_mADE8C786A44A85CB58AEA7976CFC6E5B9E95078F,
	PhraseRecognizer_InvokePhraseRecognizedEvent_m71BC6E4E3474B530FB8060790335C73EA1A3949F,
	PhraseRecognizer_MarshalSemanticMeaning_mE499C2118292E6AB02370790295E913FD22A1C60,
	PhraseRecognizedDelegate__ctor_m01A9360909C670C8869C24BB40DC38C2EFCAA03C,
	PhraseRecognizedDelegate_Invoke_m492CAE6A50A50603EFFC20E55A5E600B3F44567E,
	PhraseRecognizedDelegate_BeginInvoke_m7AA286808D7E2C74F4ABA0C33D494837575D1056,
	PhraseRecognizedDelegate_EndInvoke_m1807091CC1EADD5595504824A06518F6E93045DE,
	DictationRecognizer_DictationRecognizer_InvokeHypothesisGeneratedEvent_mD73863E9B51235457513AEFCAF731D2CE5CD74A5,
	DictationRecognizer_DictationRecognizer_InvokeResultGeneratedEvent_m341798A02CC88FFBC30AD9D65A8B698CC26FF51C,
	DictationRecognizer_DictationRecognizer_InvokeCompletedEvent_mA57B1D8E190290090345F7F46769FCA3CB300381,
	DictationRecognizer_DictationRecognizer_InvokeErrorEvent_mD903A358EDB5EC2F026503EFB541AA0784D21030,
	DictationHypothesisDelegate__ctor_mC0ABE67DC4DBB4FA1F9869C18419245544A3E597,
	DictationHypothesisDelegate_Invoke_m172D4B7A778AF77B8DF601464D1C6F94323DA351,
	DictationHypothesisDelegate_BeginInvoke_m3C59FFB81CDAE4A1B9E42587468BA85EA85FAC98,
	DictationHypothesisDelegate_EndInvoke_m8DFC208965BBC072D5301AF8AA2A304134385826,
	DictationResultDelegate__ctor_mA245D7423472C29BE33A7340495C2673E4FF3A65,
	DictationResultDelegate_Invoke_mD1040B15ECCA7D8178544845A76F6D8CE8F8D7EB,
	DictationResultDelegate_BeginInvoke_mFDC155E178AFBE15333E08205F633D6B429EC4F8,
	DictationResultDelegate_EndInvoke_m2532D7823F4B3D7D6DB9A3E8193DF3A699FD5CB5,
	DictationCompletedDelegate__ctor_mC90654E14428DFAEF0ADC53BF45C64487DE4B028,
	DictationCompletedDelegate_Invoke_mFC2FDC432DD9C96C8185D6F4FE2CDB03934DB7E7,
	DictationCompletedDelegate_BeginInvoke_m71523637F04F434C0B724BED605FA9D3801B9C06,
	DictationCompletedDelegate_EndInvoke_mC10AC3ECC5701357846DF46323A7230B746D3333,
	DictationErrorHandler__ctor_m0D33C95E89B5B9F7361B33CBEFD9E12FAEBB7CA4,
	DictationErrorHandler_Invoke_m44F785E16BD6C75A458770FBD90081BBF9F92F82,
	DictationErrorHandler_BeginInvoke_m427CE87CC2C182A9622CEF829F4DB47B8E32C809,
	DictationErrorHandler_EndInvoke_m81F3A3DB125A5756A3A399410EDE07061CB696E5,
	PhraseRecognizedEventArgs__ctor_mDE0C6DB83D9AEC9FFBBFDD6F16FFDB59122B91BD_AdjustorThunk,
	PhotoCapture_MakeCaptureResult_mDB24427838429555162DA5395DE31032C291139A,
	PhotoCapture_InvokeOnCreatedResourceDelegate_m0590F8148BA223CF8FE089B9BEAF65181019894C,
	PhotoCapture__ctor_m272ED75FFE8FC350EA0FB9689FDCCC5E5A357683,
	PhotoCapture_InvokeOnPhotoModeStartedDelegate_m1655D665DEAD3E00A64186B78CDC53E1D2894C98,
	PhotoCapture_InvokeOnPhotoModeStoppedDelegate_mD86C8F3385AA2F0F8FC7E06C62C0E795DD8BA002,
	PhotoCapture_InvokeOnCapturedPhotoToDiskDelegate_m2D3054020413D1E8F62B61DDBD39CC023C198D1F,
	PhotoCapture_InvokeOnCapturedPhotoToMemoryDelegate_m76E1AF14059053B8CFD5F85AB7108CF8DD7C6942,
	PhotoCapture_Dispose_mC657D88FF9C469242501DC5E01C6EADA8DEE49CC,
	PhotoCapture_Dispose_Internal_mBAE794ED741E9B9D2E940AF80D91124A8FE03D83,
	PhotoCapture_Finalize_m2ECA2885C2AF55BCE899E5F61C460BE45C3331F4,
	PhotoCapture_DisposeThreaded_Internal_m638F1D622FD8AE8801D4FC6A1CDFAE0A67F6FF4D,
	PhotoCapture__cctor_m2D9F492A3F615EEB0C515F1AF1D7068D51085574,
	OnCaptureResourceCreatedCallback__ctor_m18862383676D88F484C4F1D55DE53FD76A254BF6,
	OnCaptureResourceCreatedCallback_Invoke_m00E949BE932CDE02940DAC09D88AC338B44678E5,
	OnCaptureResourceCreatedCallback_BeginInvoke_m70F7B03FF31B53058D1B51F05B0D5BB3E1C785E6,
	OnCaptureResourceCreatedCallback_EndInvoke_mA868D0097EBE8FE44E88BB93932C52B79E12910B,
	OnPhotoModeStartedCallback__ctor_mD122FA9A03DF5DD7A950ADD2DE0E1061779415B8,
	OnPhotoModeStartedCallback_Invoke_m243CF5632026C05A11143C2A52CEA80A8B46AAB3,
	OnPhotoModeStartedCallback_BeginInvoke_m4AB861825C43FBA4BB652B26EF01085FEF25B0CD,
	OnPhotoModeStartedCallback_EndInvoke_m0FA93D4397AEC6F2EA56B65863308AD72B017B0B,
	OnPhotoModeStoppedCallback__ctor_mF8240EE3B1A20732ED29A0CF0D1CCE30CD838E6B,
	OnPhotoModeStoppedCallback_Invoke_mC55D72686C2EE6D124859D426C76835DD3CDAAF6,
	OnPhotoModeStoppedCallback_BeginInvoke_m70BAC41BE079FC59B70B141F732DCE97FA7BC5AA,
	OnPhotoModeStoppedCallback_EndInvoke_m1DCAE195481843479432344016A8E634D6023E07,
	OnCapturedToDiskCallback__ctor_mF1937339779CDF2601A14371D2033EC6DB9EC297,
	OnCapturedToDiskCallback_Invoke_m1D853AAE852DF5DDBBC0815B0AE3F5854FCE5B70,
	OnCapturedToDiskCallback_BeginInvoke_m7470B5311AF8B8F32DA2E4025E242A0DDDA2CD89,
	OnCapturedToDiskCallback_EndInvoke_m6BE154AD3F6595D9F72B54D55427559A4495A4DB,
	OnCapturedToMemoryCallback__ctor_mE6373867AD9BC33554D11BECDCF720FF7F41F431,
	OnCapturedToMemoryCallback_Invoke_mA9753BDC67A357C072B167772505785858730753,
	OnCapturedToMemoryCallback_BeginInvoke_m00F1E3F01755247DA91F4B991E45812052EFFD78,
	OnCapturedToMemoryCallback_EndInvoke_mE71F50B61AA38C40151B2C124047B6B91866A90E,
	PhotoCaptureFrame_get_dataLength_m47499791125C48A4495A585D80985EAFA73152BA,
	PhotoCaptureFrame_set_dataLength_mF01C30F8CC0E6B038E233F22C5BAB1D4C0251817,
	PhotoCaptureFrame_set_hasLocationData_mCFE5AFBA1529278C8D69648778A502F40B4AC814,
	PhotoCaptureFrame_set_pixelFormat_m5ED1DEF417CEE9A2693DCF1450541236B48D1832,
	PhotoCaptureFrame_GetDataLength_m778C47B77CDFF4638647E53363366E1E16410CF2,
	PhotoCaptureFrame_GetHasLocationData_m0582FE1D5C964F77C0208B92D1A55B7782BAA649,
	PhotoCaptureFrame_GetCapturePixelFormat_m172C702F3A874A6FF2D93AA2FF7AEF955A2D2A7E,
	PhotoCaptureFrame__ctor_m54006C0D1C252AA254443BDDF659EA77D048F7B6,
	PhotoCaptureFrame_Cleanup_m3352FE56134490EA6AD718AA8D015F743C600069,
	PhotoCaptureFrame_Dispose_Internal_m6481298FEA6C2A1B1E86AF93B98D9366D1D53398,
	PhotoCaptureFrame_Dispose_m721C009AD08134027E0B83802ABAE1A22E248E02,
	PhotoCaptureFrame_Finalize_m566F6494D65D7036D4E3870794D1BABD874242A6,
	VideoCapture_MakeCaptureResult_m1B18134E624C914E7864DF53C04469488C373A7B,
	VideoCapture_InvokeOnCreatedVideoCaptureResourceDelegate_m8FDDFA04ADAB2F82C2E5724490C030F5CA0A9E15,
	VideoCapture__ctor_m59BBCFE085D02230DF34ED48710A9FE1677879E2,
	VideoCapture_InvokeOnVideoModeStartedDelegate_m79DB3233AD2A8F18940D0A5FB711F14BB7F411E5,
	VideoCapture_InvokeOnVideoModeStoppedDelegate_m9016BC3B3312B5A739D443075BF7728E58215AB6,
	VideoCapture_InvokeOnStartedRecordingVideoToDiskDelegate_m5ACD5DFAD5008371A0CBFBAA4CEFCC17C606E4CA,
	VideoCapture_InvokeOnStoppedRecordingVideoToDiskDelegate_m386C086EE16E072FF40964E31B87E617F0B6C07A,
	VideoCapture_Dispose_m7A39424EAE62A1BEAE17325F61B8A22C7F20EF2D,
	VideoCapture_Dispose_Internal_mB13763260E23253359B6CAE0AF929DE13D9A42F7,
	VideoCapture_Finalize_m3789179E8CF060E0C2A746A1FDDFFD21577E8ADB,
	VideoCapture_DisposeThreaded_Internal_mB47B014DFF05AFC026E5DD7D0FAF54106553B9DB,
	VideoCapture__cctor_m20292EE7FCFF5E79FB26BD15B3A89EBE5408EBD5,
	OnVideoCaptureResourceCreatedCallback__ctor_m367E31CF82FCBA5320647F05C0DA9CA047752B59,
	OnVideoCaptureResourceCreatedCallback_Invoke_m57F41946F7DA79E150143DD5882473FA61C84E07,
	OnVideoCaptureResourceCreatedCallback_BeginInvoke_mE36CE2C8E021A4FF484DBC059B46F8DC1841483A,
	OnVideoCaptureResourceCreatedCallback_EndInvoke_m91AF83FB7BD99EF34F87DF6CC07DDF0DB9CF815D,
	OnVideoModeStartedCallback__ctor_mD6DC7A8AD853556AD0D66C32339A735D72B09C47,
	OnVideoModeStartedCallback_Invoke_m13337457016034DE08D05247F064B9EECC351B2E,
	OnVideoModeStartedCallback_BeginInvoke_m4A3CEBB5158BB4DCB0F8DD884D88A53CE484CEFB,
	OnVideoModeStartedCallback_EndInvoke_mF9B72E6808B95E4DC060B1401CDF180A4030B5A8,
	OnVideoModeStoppedCallback__ctor_m833B3FF3AD7E58E090CB6238684518693AAF1A97,
	OnVideoModeStoppedCallback_Invoke_mD5CACF758DA742BCDC3FB7B27A648BAB02340498,
	OnVideoModeStoppedCallback_BeginInvoke_mDBF06C8C08B108AB38F91C075A660AFA1D40876C,
	OnVideoModeStoppedCallback_EndInvoke_m97E26F948C10C4E3C76FF735248FADEE7E3D8398,
	OnStartedRecordingVideoCallback__ctor_m2742271349A83A30DE2116DE61A29174E4186C15,
	OnStartedRecordingVideoCallback_Invoke_mA36E25A3C9AE663053A004D216FFCEF82AEF3790,
	OnStartedRecordingVideoCallback_BeginInvoke_m1E4D58E53791FAACD887CFB36445C5FEA8F7CD06,
	OnStartedRecordingVideoCallback_EndInvoke_m2941925C6B4AFEC5855E18BA32423C56EE0FA2A3,
	OnStoppedRecordingVideoCallback__ctor_mDED3B288194CBA9478D42A22F5FA3E20A8FC4D79,
	OnStoppedRecordingVideoCallback_Invoke_m0CB497020C4935AE90335928C7AB897789C91F16,
	OnStoppedRecordingVideoCallback_BeginInvoke_mA92F3961C471FBD5E16F95840A8CB912E20C54BC,
	OnStoppedRecordingVideoCallback_EndInvoke_m01C378DD8A73D83471A084E63276028C5317D7E2,
	UnityEventTools_TidyAssemblyTypeName_m7ABD7C25EA8BF24C586CD9BD637421A12D8C5B44,
	ArgumentCache_get_unityObjectArgument_m546FEA2DAB93AD1222C0000A882FFAF66DF88B47,
	ArgumentCache_get_unityObjectArgumentAssemblyTypeName_mAA84D93F6FE66B3422F4A99D794BCCA11882DE35,
	ArgumentCache_get_intArgument_m8BDE2E1DBF5870F5F91041540EC7F867F0D24CFF,
	ArgumentCache_get_floatArgument_m941EC215EC34675CA224A311BEDBBEF647F02150,
	ArgumentCache_get_stringArgument_mE71B434FCF1AA70BF2A922647F419BED0553E7FB,
	ArgumentCache_get_boolArgument_mB66F6E9F16B1246A82A368E8B6AB94C4E05AF127,
	ArgumentCache_OnBeforeSerialize_mF5B9D962D88C22BC20AE330FFBC33FB3042604D0,
	ArgumentCache_OnAfterDeserialize_mB50D42B36BF948499C4927084E7589D0B1D9C6FB,
	ArgumentCache__ctor_mF667890D72F5C3C3AE197688DEF71A23310248A0,
	BaseInvokableCall__ctor_mC60A356F5535F98996ADF5AF925032449DFAF2BB,
	BaseInvokableCall__ctor_mB7F553CF006225E89AFD3C57820E8FF0E777C3FB,
	NULL,
	NULL,
	BaseInvokableCall_AllowInvoke_m84704F31555A5C8AD726DAE1C80929D3F75A00DF,
	NULL,
	InvokableCall_add_Delegate_mA80FC3DDF9C96793161FED5B38577EC44E8ECCEC,
	InvokableCall_remove_Delegate_m32D6AD4353BD281EAAC1C319D211FF323E87FABF,
	InvokableCall__ctor_mB755E9394048D1920AA344EA3B3905810042E992,
	InvokableCall__ctor_m00346D35103888C24ED7915EC8E1081F0EAB477D,
	InvokableCall_Invoke_mE87495638C5A778726A99872D041A22CA957159E,
	InvokableCall_Invoke_mC0E0B4D35F0795DCF2626DC15E5E92417430AAD7,
	InvokableCall_Find_mE1AF062AF0ADE337AEB2728F401CAB23E95D9360,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PersistentCall_get_target_mE1268D2B40A4618C5E318D439F37022B969A6115,
	PersistentCall_get_targetAssemblyTypeName_m2F53F996EA6BFFDFCDF1D10B6361DE2A98DD9113,
	PersistentCall_get_methodName_m249643D059927A05051B73309FCEAC6A6C9F9C88,
	PersistentCall_get_mode_m7041C70D7CA9CC2D7FCB5D31C81E9C519AF1FEB8,
	PersistentCall_get_arguments_m9FDD073B5551520F0FF4A672C60E237CADBC3435,
	PersistentCall_IsValid_m3BDFC18A3D1AD8ECA8822EA4265127B35237E11B,
	PersistentCall_GetRuntimeCall_mCD3A4A0287D8A9C2CF00D894F378BD4E4684287A,
	PersistentCall_GetObjectCall_m5CDF291A373C8FD34513EF1A1D26C9B36ED7A95D,
	PersistentCall_OnBeforeSerialize_mD5109AA99B22304579296F4B6077647839456346,
	PersistentCall_OnAfterDeserialize_m73B5F8F782FC97AE4C4DCE3DB2C1D9F102455D9E,
	PersistentCall__ctor_mCA080B989171E4FE13761074DD7DE76683969140,
	PersistentCallGroup__ctor_m0F94D4591DB6C4D6C7B997FA45A39C680610B1E0,
	PersistentCallGroup_Initialize_m162FC343BA2C41CBBB6358D624CBD2CED9468833,
	InvokableCallList_AddPersistentInvokableCall_mE7A19335649958FBF5522D7468D6075EEC325D9C,
	InvokableCallList_AddListener_m07F4E145332E2059D570D8300CB422F56F6B0BF1,
	InvokableCallList_RemoveListener_mDE659068D9590D54D00436CCBDB3D27DCD263549,
	InvokableCallList_ClearPersistent_m9A59E6161F8E42120439B76FE952A17DA742443C,
	InvokableCallList_PrepareInvoke_mE340D329F5FC08EA77FC0F3662FF5E5BB85AE0B1,
	InvokableCallList__ctor_m986F4056CA5480D44854152DB768E031AF95C5D8,
	UnityEventBase__ctor_m8F423B800E573ED81DF59EB55CD88D48E817B101,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m4E84FB82333E5AA5812095E536810C4BCAF55727,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mA1BAB2B28C0E4A62BA06FEF5FCAD8A67C3449472,
	NULL,
	NULL,
	UnityEventBase_FindMethod_m665F2360483EC2BE2D190C6EFA4A624FB8722089,
	UnityEventBase_FindMethod_m9F887E46F769E2C1D0CFE3C71F98892F9C461025,
	UnityEventBase_DirtyPersistentCalls_m85DA5AEA84F8C32D2FDF5DD58D9B7EF704B7B238,
	UnityEventBase_RebuildPersistentCallsIfNeeded_m1FF3E4C46BB16B554103FAAAF3BBCE52C991D21F,
	UnityEventBase_AddCall_mB4825708CFE71BBF9B167EB16FC911CFF95D101C,
	UnityEventBase_RemoveListener_m0B091A723044E4120D592AC65CBD152AC3DF929E,
	UnityEventBase_PrepareInvoke_m999D0F37E0D88375576323D30B67ED7FDC7A779D,
	UnityEventBase_ToString_m0A3BBFEC8C23E044D52502CE201FE82EEF60A8E7,
	UnityEventBase_GetValidMethodInfo_mBA413E99550A6AD8B36F5BEB8682B1536E976C36,
	UnityAction__ctor_m48C04C4C0F46918CF216A2410A4E58D31B6362BA,
	UnityAction_Invoke_mFFF1FFE59D8285F8A81350E6D5C4D791F44F6AC9,
	UnityAction_BeginInvoke_m67AAC6F5871162346CBCCA0CA51AA38307A4ABB8,
	UnityAction_EndInvoke_mC02B54511B4220CF6D1B37F14BF07D522E91786B,
	UnityEvent__ctor_m98D9C5A59898546B23A45388CFACA25F52A9E5A6,
	UnityEvent_AddListener_m0ACFF0706176ECCB20E0BC2542D07396616F436D,
	UnityEvent_FindMethod_Impl_m763FB43F24EF4A092E26FAE6F6DF561CF360475A,
	UnityEvent_GetDelegate_m7AD1450601F49B957A87161BE6D14020720A1A56,
	UnityEvent_GetDelegate_m86F8240BF539D38F90F6BE322CF5CA91C17B13B9,
	UnityEvent_Invoke_mDA46AA9786AD4C34211C6C6ADFB0963DFF430AF5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D,
	PreserveAttribute__ctor_mBD1EEF1095DBD581365C77729CF4ACB914859CD2,
	MovedFromAttributeData_Set_mC5572E6033FCE1C53B8C43A8D5280E71CE388640_AdjustorThunk,
	MovedFromAttribute__ctor_mA4B2632CE3004A3E0EA8E1241736518320806568,
	Scene_GetNameInternal_mE4879219C90C34EE00F46188C29AE74D74FD15D2,
	Scene_GetBuildIndexInternal_m143AF2C97F3B5E24FA90B8FBC709411976F3EE23,
	Scene_get_handle_m57967C50E461CD48441CA60645AF8FA04F7B132C_AdjustorThunk,
	Scene_get_name_m38F195D7CA6417FED310C23E4D8E86150C7835B8_AdjustorThunk,
	Scene_get_buildIndex_mE32CE766EA0790E4636A351BA353A7FD71A11DA4_AdjustorThunk,
	Scene_op_Equality_m5B02632BCD075BF2857A75AF21F5F82AE22C4E4D,
	Scene_GetHashCode_mFC620B8CA1EAA64BF0D7B33E8D3EAFAEDC9A6DCB_AdjustorThunk,
	Scene_Equals_m78D2F82F3133AD32F35C7981B65D0980A6C3006D_AdjustorThunk,
	SceneManager_get_sceneCount_m57B8EB790D8B6673BA840442B4F125121CC5456E,
	SceneManager_GetActiveScene_mB9A5037FFB576B2432D0BFEF6A161B7C4C1921A4,
	SceneManager_GetSceneAt_m46AF96028C6A3A09198ABB313E4206D93A8D1F3F,
	SceneManager_LoadSceneAsyncNameIndexInternal_mD72656A5141151775F28886F59D2830B0EC1B659,
	SceneManager_add_sceneUnloaded_m2CE4194528D7AAE67F494EB15C8BBE650FBA4C72,
	SceneManager_remove_sceneUnloaded_mA7CD60918C6EE3B8D8623201A487C2CEC8D4EA5E,
	SceneManager_LoadScene_m7DAF30213E99396ECBDB1BD40CC34CCF36902092,
	SceneManager_LoadScene_m84A1D8B405E365CFC372040E311669E497D12591,
	SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5,
	SceneManager_LoadScene_m26A2A5DA3CBD39F367587C89ADC7ACD3B2C223C7,
	SceneManager_Internal_SceneLoaded_m3546B371F03BC8DC053FFF165AB25ADF44A183BE,
	SceneManager_Internal_SceneUnloaded_m9A68F15B0FA483633F24E0E50574CFB6DD2D5520,
	SceneManager_Internal_ActiveSceneChanged_m4094F4307C6B3DE0BB87ED646EA9BD6640B831DC,
	SceneManager_GetActiveScene_Injected_m7F05E2A355D8CA1E3496075D7E7CCC1327880ED6,
	SceneManager_GetSceneAt_Injected_m2649DB4E01B925CFDF17EBDFB48816447082771A,
	SceneManager_LoadSceneAsyncNameIndexInternal_Injected_m719D102B0AF20644651BDC09DA76836B331A80FE,
	LoadSceneParameters__ctor_m6B4C0245743813570AE22B68A8F75332248929AC_AdjustorThunk,
	UpdateFunction__ctor_mB10AB83A3F547AC95FF726E8A7B5FF9C16EC1319,
	UpdateFunction_Invoke_mB17C55B92FAECE51078028F59A9F1EAC2016B1AD,
	UpdateFunction_BeginInvoke_m1EB0935AB72A286D153ACEBD0E5D66BB38BD6799,
	UpdateFunction_EndInvoke_mB4BC0AA40E9C83274116DF6467D72DD4902DA624,
	MessageEventArgs__ctor_m18272C7358FAC22848ED84A952DCE17E8CD623BA,
	PlayerConnection_get_instance_mA46C9194C4D2FE0BB06C135C4C0521DD516592F6,
	PlayerConnection_get_isConnected_m315E30E90CA2BBEF2C2C366EF68BF0380C2BCF28,
	PlayerConnection_CreateInstance_m761E97DB4F693FC1A92418088B90872AA559BFFE,
	PlayerConnection_OnEnable_m89048E6B2F1A820F9FD99C3237CB55FF7F56F558,
	PlayerConnection_GetConnectionNativeApi_m94F4E5D341869936857659D92591483D5F6EE95F,
	PlayerConnection_Register_m97A0118B2F172AA0236D0D428F2C6F6E8326BF3D,
	PlayerConnection_Unregister_m1D2F6BE95E82D60F6EA02139AA64D7D58B4B889B,
	PlayerConnection_RegisterConnection_m3DA7DB08B44854C6914F9A154C486EFFCC2B4E8D,
	PlayerConnection_RegisterDisconnection_mA0D4C45243C1244339CAF66E45C65A6130E8B87A,
	PlayerConnection_UnregisterConnection_mC44D98524BD2E21B7658AB898B8283C34A5AD96E,
	PlayerConnection_UnregisterDisconnection_m1DB86C159F601428F59DAB3A4BB912C780F8EAFD,
	PlayerConnection_Send_m51137877D6E519903AEA4A506ABD48A30AF684F0,
	PlayerConnection_TrySend_m9AD3046E615B6DECE991776EEFAB10EA95E478A6,
	PlayerConnection_BlockUntilRecvMsg_m3C3CE1A885657B18DEE2C10066425B45F2B15FAC,
	PlayerConnection_DisconnectAll_m2A55CA3DECCC5683315504EACB9311E565D1EFF5,
	PlayerConnection_MessageCallbackInternal_m223C558BDA58E11AE597D73CEC359718A336FD74,
	PlayerConnection_ConnectedCallbackInternal_m9143E59EA71734C96F877CFD685F0935CEEBA13E,
	PlayerConnection_DisconnectedCallback_mCC356CB2D6C8820A435950F630BA349B7CA5E267,
	PlayerConnection__ctor_mBB02EDC32EB6458CB527F9703CFD4400C7CDCB92,
	U3CU3Ec__DisplayClass12_0__ctor_m66E5B9C72A27F0645F9854522ACE087CE32A5EEE,
	U3CU3Ec__DisplayClass12_0_U3CRegisterU3Eb__0_m27C1416BAD7AF0A1BF83339C8A03865A6487B234,
	U3CU3Ec__DisplayClass13_0__ctor_m75A8930EBA7C6BF81519358930656DAA2FBDDEDB,
	U3CU3Ec__DisplayClass13_0_U3CUnregisterU3Eb__0_mBA34804D7BB1B4FFE45D077BBB07BFA81C8DCB00,
	U3CU3Ec__DisplayClass20_0__ctor_m2B215926A23E33037D754DFAFDF8459D82243683,
	U3CU3Ec__DisplayClass20_0_U3CBlockUntilRecvMsgU3Eb__0_m1D6C3976419CFCE0B612D04C135A985707C215E2,
	PlayerEditorConnectionEvents_InvokeMessageIdSubscribers_m157C5C69D6E22A8BCBDC40929BA67E1B6E586389,
	PlayerEditorConnectionEvents_AddAndCreate_m9722774BB976599BF8AD8745CA8EC3218828306B,
	PlayerEditorConnectionEvents_UnregisterManagedCallback_mEB4C4FBCBA7A9527475914F61AFDB9676B8BEFB3,
	PlayerEditorConnectionEvents__ctor_m40946A5B9D9FB40849BEA07DCD600891229249A0,
	MessageEvent__ctor_mE4D70D8837C51E422C9A40C3F8F34ACB9AB572BB,
	ConnectionChangeEvent__ctor_m95EBD8B5EA1C4A14A5F2B71CEE1A2C18C73DB0A1,
	MessageTypeSubscribers_get_MessageTypeId_m5A873C55E97728BD12BA52B5DB72FFA366992DD9,
	MessageTypeSubscribers_set_MessageTypeId_m7125FF3E71F4E678644F056A4DC5C29EFB749762,
	MessageTypeSubscribers__ctor_mA51A6D3E38DBAA5B8237BAB1688F669F24982F29,
	U3CU3Ec__DisplayClass6_0__ctor_mEB8DB2BFDA9F6F083E77F1EC1CE3F4861CD1815A,
	U3CU3Ec__DisplayClass6_0_U3CInvokeMessageIdSubscribersU3Eb__0_mEF5D5DFC8F7C2CEC86378AC3BBD40E80A1D9C615,
	U3CU3Ec__DisplayClass7_0__ctor_m97B67DA8AA306A160A790CCDD869669878DDB7F3,
	U3CU3Ec__DisplayClass7_0_U3CAddAndCreateU3Eb__0_mAA3D205F35F7BE200A5DDD14099F56312FBED909,
	U3CU3Ec__DisplayClass8_0__ctor_m18AC0B2825D7BB04F59DC800DFF74D45921A28FA,
	U3CU3Ec__DisplayClass8_0_U3CUnregisterManagedCallbackU3Eb__0_m70805C4CE0F8DD258CC3975A8C90313A0A609BE3,
	DefaultValueAttribute__ctor_m7A9877491C22E8CDCFDAD240D04156D4FAE7D6C6,
	DefaultValueAttribute_get_Value_m333925936EF155BACAEF24CE1ADE07085722606E,
	DefaultValueAttribute_Equals_mD096E8E7C8C6051AD743AAB7CAADB27428871E51,
	DefaultValueAttribute_GetHashCode_mA74BBB26F6C0B49AA44C175DBFDB82F00E497595,
	ExcludeFromDocsAttribute__ctor_mFA14E76D8A30ED8CA3ADCDA83BE056E54753825D,
	CameraEventUtils_IsValid_m979977DB6D2CF349B44CF0D1D4B409216DAA8155,
	RenderTargetIdentifier__ctor_m8F174D7C9A3172B5C5D0C5C8933973801157CDBB_AdjustorThunk,
	RenderTargetIdentifier__ctor_m76845E674E8D1597F68C57F362F522E37CCFCD5D_AdjustorThunk,
	RenderTargetIdentifier__ctor_m23C0BAC2C6EF09642BC468CF9CABDF0D2B0DB9EC_AdjustorThunk,
	RenderTargetIdentifier_op_Implicit_mB7B58C1D295E2DAE3C76874D030D4878A825E359,
	RenderTargetIdentifier_op_Implicit_m065F5C06D85EAF99A60942A197E4CB25FB38B20B,
	RenderTargetIdentifier_op_Implicit_mFF9B98B136B3AB4E9413777F931392D02AA7A8FA,
	RenderTargetIdentifier_ToString_m6A676BAE1AD75ADAE5134EE9E243325DBC23CC4D_AdjustorThunk,
	RenderTargetIdentifier_GetHashCode_mB5C49CB1724A3086C19B504C1B659754D67AB967_AdjustorThunk,
	RenderTargetIdentifier_Equals_mFDF40A5663B058DA6026AAA920C0CFE6BAD05D1A_AdjustorThunk,
	RenderTargetIdentifier_Equals_m57C0226B891EA78C5A9387C7B2A225CE3E4A521A_AdjustorThunk,
	GraphicsSettings_get_lightsUseLinearIntensity_m02A37F59F36C77877FC86B93478BC5795F70FFB2,
	OnDemandRendering_get_renderFrameInterval_m7B3F913B3C16E756BE5DF901D86FC7F869759367,
	OnDemandRendering_GetRenderFrameInterval_mF254C740E4C04295AC2EF10D05D22BCA179D7685,
	OnDemandRendering__cctor_m3C3EFF1AE188B9FACE3E94D9F333CF873D110FD0,
	CommandBuffer_InitBuffer_mEF17DF124E9787290AF80B935FDE92C46E540F6D,
	CommandBuffer_ReleaseBuffer_m778F98EED78913829C13948F5942D3B68EDEC41C,
	CommandBuffer_set_name_m4217524F48F215BB404C2847176AA50360CA6CE8,
	CommandBuffer_Clear_mDE798ACE9294B43B9387A6B06E43B3D4A30A7092,
	CommandBuffer_Internal_DrawMesh_mE0A7DCCA8C72E6F9CB35D2AB2AC0C61452D2E9B6,
	CommandBuffer_Blit_Texture_m1F9A84A7C34C401D3B94382E8059C3657B17E98C,
	CommandBuffer_Blit_Identifier_m5AF32AB0279B7FCFD1DFFE934A9551495C1ACDB0,
	CommandBuffer_GetTemporaryRT_mF4C1B7600F2E8E0C292A718A6309A84ECD870C83,
	CommandBuffer_GetTemporaryRT_m6677EA531A6D98CE18EA2EFB2F425CE02E5B56DD,
	CommandBuffer_GetTemporaryRT_m7D44FEC654D1CC488530CC7D219BB4EBC9416F0C,
	CommandBuffer_GetTemporaryRT_m425B035D860BFC45398E5AF258333647E8FACC2A,
	CommandBuffer_GetTemporaryRT_m416F3104D78E2FBFFAB769F692C5A4DED3A334E9,
	CommandBuffer_GetTemporaryRT_mED03DAECE8A01E78B4BAE23124A039240BF39EAA,
	CommandBuffer_GetTemporaryRT_m1E03E47E671E9F017022CEDFB61B0C658E712FA7,
	CommandBuffer_GetTemporaryRT_m2EE88CD577B803F6A5491DC52E604C5FDFF8CDBB,
	CommandBuffer_GetTemporaryRT_m010AFBFF063C2ADC5A27A3642E55B9000EB1B7A4,
	CommandBuffer_GetTemporaryRT_mD8089EAEA2133AA0D545CB525BC56C5762BD3BBF,
	CommandBuffer_GetTemporaryRT_m57A18FCE25917C5F08FFCEC375E7D4D4AC78B389,
	CommandBuffer_ReleaseTemporaryRT_mE4A4A8DE6B315956BEAB66915FF720F1C61BCDC5,
	CommandBuffer_SetGlobalFloat_mE7B8587FABBF52E7DC1981E9676BBBD4D1E838BF,
	CommandBuffer_SetGlobalVector_m26AD5F4AF0ABDD9F7DA61583606824526A58ABEF,
	CommandBuffer_ValidateAgainstExecutionFlags_mF5464766DDBFD0B426A86EF3A828C2BDFA12272A,
	CommandBuffer_SetGlobalTexture_Impl_m58E72BA44C8BBB68770387C6AE87F9B06144B4B2,
	CommandBuffer_SetRenderTarget_m4F4A1974C1BC9F3849D54B5141EF296F37B4E7E3,
	CommandBuffer_SetRenderTargetMulti_Internal_mD0A3E932DB98F8071C378D721B5B50A1DE97B124,
	CommandBuffer_Finalize_m3D39159AB1026981EF8E349FD87BA6FD78B8248C,
	CommandBuffer_Dispose_m594BAF51BD0154E0CBAA12D1537D36CCF82EA0E2,
	CommandBuffer_Dispose_m12BD69C6CA4A9D4543CBEE4A5B11348634F6E8DA,
	CommandBuffer__ctor_mB7E1174EB3B4E2E53BCC6532840AB55ECE6D06CF,
	CommandBuffer_DrawMesh_m8AE72FB8773642F0293697038A3262E84E269AB0,
	CommandBuffer_DrawMesh_m1045759BE3456DC0CE14E6E372E13FF6625D1A83,
	CommandBuffer_Blit_m57FADAE57CE78D6E45D78A212905332B70560A2B,
	CommandBuffer_Blit_mDDA4CCEA9460315B9CEA882E52B7734F4033C362,
	CommandBuffer_Blit_m50AB7CE0D68B2B2DED83FFE9331504382FA2AB9A,
	CommandBuffer_SetGlobalTexture_mED69F96D6790798BFE82C22AFF2A065E5EDA41CF,
	CommandBuffer_Internal_DrawMesh_Injected_mE52B829DE1FA132BC528038FBF1137423B287FAB,
	CommandBuffer_Blit_Texture_Injected_m883660541CCF527552E2FD55686494441CCD1CE6,
	CommandBuffer_Blit_Identifier_Injected_m8AE03F3A3DB74691420314F637DF9E5C98937B35,
	CommandBuffer_SetGlobalVector_Injected_m803DA6619D95F23EB054BEBE1E53C3D8C6D13767,
	CommandBuffer_SetRenderTargetMulti_Internal_Injected_m6FAD10C4CFF47AD7C5CE8701DA70038320982D98,
	BatchCullingContext__ctor_m9257DEBD02B88BE06D6ED9D3E9BF686100081F7F_AdjustorThunk,
	BatchRendererGroup_InvokeOnPerformCulling_m164A637514875EBBFF4A12C55521BEA93474739C,
	OnPerformCulling__ctor_m1862F8A67E8CA14A19B80DE61BBC1881DF945E9F,
	OnPerformCulling_Invoke_m804E8422831E63707E5582FBBBFEF08E8A100525,
	OnPerformCulling_BeginInvoke_mAF6E29B6BAC52D4DCAD0C87B8C6939B94566EAA1,
	OnPerformCulling_EndInvoke_m643216ACF662C78BD6F98E09FA5AA45F92E2F9F0,
	LODParameters_Equals_mF803671C1F46ECEEE0B376EBF3AAF69D1236B4C0_AdjustorThunk,
	LODParameters_Equals_m8F8B356BCB62FAEAE0EFD8C51FFF9C5045A7DB5C_AdjustorThunk,
	LODParameters_GetHashCode_m5310697EE3BF4943F7358EF0EA2E7B3A9D7C1BAD_AdjustorThunk,
	NULL,
	RenderPipeline_InternalRender_mD6C2FEDA607A430F963066B487C02F4D2379FBDA,
	RenderPipeline_get_disposed_m67EE9900399CE2E9783C5C69BFD1FF08DF521C34,
	RenderPipeline_set_disposed_mB375F2860E0C96CA5E7124154610CB67CE3F80CA,
	RenderPipeline_Dispose_mE06FE618AE8AE1B563CAC05585ACBA8832849A7F,
	RenderPipeline_Dispose_mC61059BB1A9F1CE9DB36C49FD01A1EB5BF4CFFE8,
	RenderPipelineAsset_InternalCreatePipeline_mAE0E11E7E8D2D501F7F0F06D37DB484D37533406,
	RenderPipelineAsset_get_renderingLayerMaskNames_m099B8C66F13086B843E997A91D5F29DFA640BD5B,
	RenderPipelineAsset_get_defaultMaterial_mC04A65F5C07B7B186B734ACBDF5DA55DAFC88A4D,
	RenderPipelineAsset_get_autodeskInteractiveShader_mA096E3713307598F4F0714348A137E3A0F450EAF,
	RenderPipelineAsset_get_autodeskInteractiveTransparentShader_m09C4E71DE3D7BD8CDE7BB60CE379DAB150F309FF,
	RenderPipelineAsset_get_autodeskInteractiveMaskedShader_mD5AB54521766DF6A0FF42971D598761BCF7BC74A,
	RenderPipelineAsset_get_terrainDetailLitShader_mB60D130908834F5E6585578DDEA4E175DECCA654,
	RenderPipelineAsset_get_terrainDetailGrassShader_m3F41ABB79E3672A99A77594D7516855906BB71F8,
	RenderPipelineAsset_get_terrainDetailGrassBillboardShader_mED61CD4E3CF38C120FBC20866F99C17C5FE35FBB,
	RenderPipelineAsset_get_defaultParticleMaterial_mA67643D5E509468BC5C20EABAD895B9920636961,
	RenderPipelineAsset_get_defaultLineMaterial_mA76E800387087F9C4FB510E1C9526D43D7A430F3,
	RenderPipelineAsset_get_defaultTerrainMaterial_m29231CDF74C3D74809F5C990F6881E17759CE298,
	RenderPipelineAsset_get_defaultUIMaterial_m73C804E2798E17A2452687AB46A5570DD813AEC7,
	RenderPipelineAsset_get_defaultUIOverdrawMaterial_m75075F78B2FFC61DB9D05B09340D97B588FE6EB6,
	RenderPipelineAsset_get_defaultUIETC1SupportedMaterial_m530DC5B2F50F075DAB4CFA35A693293C8C98CA33,
	RenderPipelineAsset_get_default2DMaterial_mFC4CF7D4F8341D140CC0AB0B471B154AD580C4F0,
	RenderPipelineAsset_get_defaultShader_mE3420265AB2F3629C6C86E88CC7FDB7744B177C1,
	RenderPipelineAsset_get_defaultSpeedTree7Shader_m14E423504D20BC7D296EDD97045790DEA68DBE14,
	RenderPipelineAsset_get_defaultSpeedTree8Shader_mDAB93B789B21F1D58A6FE11858F8C18527ABD50B,
	NULL,
	RenderPipelineAsset_OnValidate_mEFE93B16F8AA5C809D95536A557FA9E29FF7B1DB,
	RenderPipelineAsset_OnDisable_m98A18DF5D40DBB36EF0A33C637CFA41A71FC03C1,
	RenderPipelineAsset__ctor_mC4E1451327751FBCB6F05982262D3B7ED8538DF4,
	RenderPipelineManager_get_currentPipeline_m096347F0BF45316A41A84B9344DB6B29F4D48611,
	RenderPipelineManager_set_currentPipeline_m03D0E14C590848C8D5ABC2C22C026935119CE526,
	RenderPipelineManager_CleanupRenderPipeline_m85443E0BE0778DFA602405C8047DCAE5DCF6D54E,
	RenderPipelineManager_GetCameras_m8BB39469D10975B096634537AC44FB7CF6D52938,
	RenderPipelineManager_DoRenderLoop_Internal_mB2DEE6063284E87AF7296E76461A3CF84F637E8B,
	RenderPipelineManager_PrepareRenderPipeline_m4C5F3624BD68AFCAAD4A9D800ED906B9DF4DFB55,
	RenderPipelineManager__cctor_mAF92ABD8F7586C49F9E8AAE720EBE21A2700CF6D,
	ScriptableRenderContext_GetNumberOfCameras_Internal_mDD1E260788000AB57C94A2D3F8F02A2B91DA653F_AdjustorThunk,
	ScriptableRenderContext_GetCamera_Internal_m20F51E42E84B8E2BBF2CC72F8C8EBFDCE6737646_AdjustorThunk,
	ScriptableRenderContext__ctor_mEA592FA995EF36C1F8F05EF2E51BC1089D7371CA_AdjustorThunk,
	ScriptableRenderContext_GetNumberOfCameras_m43FCE097543E85E40FCA641C570A25E718E3D6F6_AdjustorThunk,
	ScriptableRenderContext_GetCamera_mF8D58C4C1BB5667F63A2DCFDB72CE4C3C7ADBBFA_AdjustorThunk,
	ScriptableRenderContext_Equals_mDC10DFED8A46426E355FE7877624EC2D549EA7B7_AdjustorThunk,
	ScriptableRenderContext_Equals_mB04CFBF55095DF6179ED2229F4C9FE907F95799D_AdjustorThunk,
	ScriptableRenderContext_GetHashCode_mACDECBAC76686105322F409089D9A867DF4BD46D_AdjustorThunk,
	ScriptableRenderContext_GetNumberOfCameras_Internal_Injected_m65EF6B7DE8AFA868E6D83B2D75791315D9841426,
	ScriptableRenderContext_GetCamera_Internal_Injected_m17CA2AE7513419CA8FC464A270218F599D48C830,
	SupportedRenderingFeatures_get_active_mE16AFBB742C413071F2DB87563826A90A93EA04F,
	SupportedRenderingFeatures_set_active_m3BC49234CD45C5EFAE64E319D5198CA159143F54,
	SupportedRenderingFeatures_get_defaultMixedLightingModes_m7B53835BDDAF009835F9A0907BC59E4E88C71D9C,
	SupportedRenderingFeatures_get_mixedLightingModes_mE4A171C47A4A685E49F2F382AA51C556B48EE58C,
	SupportedRenderingFeatures_get_lightmapBakeTypes_mAF3B22ACCE625D1C45F411D30C2874E8A847605E,
	SupportedRenderingFeatures_get_lightmapsModes_m69B5455BF172B258A0A2DB6F52846E8934AD048A,
	SupportedRenderingFeatures_get_enlighten_mA4BDBEBFE0E8F1C161D7BE28B328E6D6E36720A9,
	SupportedRenderingFeatures_get_rendersUIOverlay_m8E56255490C51999C7B14F30CD072E49E2C39B4E,
	SupportedRenderingFeatures_FallbackMixedLightingModeByRef_m517CBD3BBD91EEA5D20CD5979DF8841FE0DBEDAC,
	SupportedRenderingFeatures_IsMixedLightingModeSupported_m8AFE3C9D450B1A6E52A31EA84C1B8F626AAC0CD0,
	SupportedRenderingFeatures_IsMixedLightingModeSupportedByRef_mC13FADD4B8DCEB26F58C6F5DD9D7899A621F9828,
	SupportedRenderingFeatures_IsLightmapBakeTypeSupported_m8BBA40E9CBAFFD8B176F3812C36DD31D9D60BBEA,
	SupportedRenderingFeatures_IsLightmapBakeTypeSupportedByRef_mB6F953153B328DBFD1F7E444D155F8C53ABCD876,
	SupportedRenderingFeatures_IsLightmapsModeSupportedByRef_m3477F21B073DD73F183FAD40A8EEF53843A8A581,
	SupportedRenderingFeatures_IsLightmapperSupportedByRef_mEAFB23042E154953C780501A57D605F76510BB11,
	SupportedRenderingFeatures_IsUIOverlayRenderedBySRP_m829585DA31B1BECD1349A02B69657B1D38D2DDC3,
	SupportedRenderingFeatures_FallbackLightmapperByRef_mD2BB8B58F329170010CB9B3BF72794755218046A,
	SupportedRenderingFeatures__ctor_m0612F2A9F55682A7255B3B276E9BAFCFC0CB4EF4,
	SupportedRenderingFeatures__cctor_mCC9E6E14A59B708435BCF2B25BE36943EC590E28,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Playable_get_Null_m008AFBC0B01AE584444CF8322CCCC038ED2B8B58,
	Playable__ctor_m4B5AC727703A68C00773F99DE1C711EFC973DCA8_AdjustorThunk,
	Playable_GetHandle_mA646BB041702651694A643FDE1A835112F718351_AdjustorThunk,
	Playable_Equals_m1BFA06EB851EEEB20705520563A2D504637CDA1C_AdjustorThunk,
	Playable__cctor_m83EA9DB5D7F0DAE681E34D0899A2562795301888,
	NULL,
	PlayableAsset_get_duration_m7D57ED7F9E30E414F2981357A61C42CBF3D8367D,
	PlayableAsset_get_outputs_m5F941EB83243BB24A93C27A7583BB55FF18CB38C,
	PlayableAsset_Internal_CreatePlayable_m5E4984A4BF33518FBE69B43A29DB486AF6501537,
	PlayableAsset_Internal_GetPlayableAssetDuration_mE27BD3B5B7D9E80D1C249D6430401BD81E4AFB4C,
	PlayableAsset__ctor_mAE1FA54D280C75ADC9486656C5C36AC1917D5FE4,
	PlayableBehaviour__ctor_m1EA2FC6B8DE3503745344E7BBAA0B40FDCD50FC8,
	PlayableBehaviour_OnGraphStart_mB302433EB8460D88FD2FD035D2BF9BF82BD82AA6,
	PlayableBehaviour_OnGraphStop_m24F852A18B98173C2CBF1B96C0477A1F77BB9CFE,
	PlayableBehaviour_OnPlayableCreate_m62F834EDF37B809612E47CCC9C4848435A89CD12,
	PlayableBehaviour_OnPlayableDestroy_m9F6E1A185DC7C98C312D9F3A0A7308DE3D32F745,
	PlayableBehaviour_OnBehaviourPlay_m71A792D97CD840F5D6C73889D00C2887F41A0F41,
	PlayableBehaviour_OnBehaviourPause_mCC4EAD0766538FE39656D3EE04268239BB091AD2,
	PlayableBehaviour_PrepareFrame_m45F96C7EDDA4ABF4C2271DB16163FBD873AA1432,
	PlayableBehaviour_ProcessFrame_m5EB22A817FFF0662E0E3AFAB34C41D7B09D4326F,
	PlayableBehaviour_Clone_m3265FD7A4EA58C9A607F0F28EFF108EBBD826C3A,
	PlayableBinding__cctor_m75475729474FE236198B967978A11DBA38CC6E47,
	CreateOutputMethod__ctor_m944A1B790F35F52E108EF2CC13BA02BD8DE62EB3,
	CreateOutputMethod_Invoke_mFD551E15D9A6FD8B2C70A2CC96AAD0D2D9D3D109,
	CreateOutputMethod_BeginInvoke_m082AE47F9DFBF0C1787081D2D628E0E5CECCBFF1,
	CreateOutputMethod_EndInvoke_mAA7AE5BF9E6A38C081A972827A16497ACA9FA9CE,
	NULL,
	PlayableHandle_get_Null_mD1C6FC2D7F6A7A23955ACDD87BE934B75463E612,
	PlayableHandle_op_Equality_mFD26CFA8ECF2B622B1A3D4117066CAE965C9F704,
	PlayableHandle_Equals_mEF30690ECF60C980C207198C86E401CB4DA5EF3F_AdjustorThunk,
	PlayableHandle_Equals_mB50663A8BC01BCED4A650EFADE88DCF47309E955_AdjustorThunk,
	PlayableHandle_GetHashCode_m26AD05C2D6209A017CA6A079F026BC8D28600D72_AdjustorThunk,
	PlayableHandle_CompareVersion_m7B2FDE7E81BCBB16D3E2667AEDAA879FA75EA1AA,
	PlayableHandle_IsValid_m237A5E7818768641BAC928BD08EC0AB439E3DAF6_AdjustorThunk,
	PlayableHandle_GetPlayableType_m5E523E4DA00DF2FAB2E209B0B43FD5AA430AE84F_AdjustorThunk,
	PlayableHandle__cctor_m14F0FDD932E8AB558039FFCD57C2E319AA25C989,
	PlayableHandle_IsValid_Injected_m177B41D5EF570AEA24146EE35628EFF2DE256ADF,
	PlayableHandle_GetPlayableType_Injected_m05EDC4CC2DC82043F97BC6BC97EA38B7AD537B9A,
	PlayableOutput__ctor_m69B5F29DF2D785051B714CFEAAAF09A237978297_AdjustorThunk,
	PlayableOutput_GetHandle_mC56A4F912A7AC8640DE78F95DD6C4F346E36820E_AdjustorThunk,
	PlayableOutput_Equals_m5A66A7B50C52A8524BB807F439345D22E96FD5CE_AdjustorThunk,
	PlayableOutput__cctor_m4F7AC7CBF2D9DD26FF45B6D521C4051CDC1A17D3,
	PlayableOutputHandle_get_Null_m33F7D36A76BFDC0B58633BF931E55FA5BBFFD93D,
	PlayableOutputHandle_GetHashCode_mB4BD207CB03FA499179FBEAF64CC66A91F49E59C_AdjustorThunk,
	PlayableOutputHandle_op_Equality_mD24E3FB556D12A8D2B6EBDB6953F667B963F5DA9,
	PlayableOutputHandle_Equals_m06CE237BED975C0A3E9B19D071767EF6E7A3F83C_AdjustorThunk,
	PlayableOutputHandle_Equals_mA8CE0F7B36B440F067705FA90DC4A836E92542E0_AdjustorThunk,
	PlayableOutputHandle_CompareVersion_mB96AE424417CD9E5610906A1508B299679814D8E,
	PlayableOutputHandle__cctor_mFC855E625CBD628602F990B3A15AF0DB8C7E3AC1,
	LinearColor_get_red_mA08BA9496EAFF59F4E1EAD61FDB5F57B0B0CC541_AdjustorThunk,
	LinearColor_set_red_mC0D9E4D96C36785145EAF7B0DBA90359EE193928_AdjustorThunk,
	LinearColor_get_green_m03F2EEBC25E91E1102A2D3252725B2BEDA7D7931_AdjustorThunk,
	LinearColor_set_green_m6E196AC12B067ED8AEF3B7C7E9DA1B80B9550B89_AdjustorThunk,
	LinearColor_get_blue_m0B87E7A2A5A5B6A6F5FD515890F6C3C528FC98FE_AdjustorThunk,
	LinearColor_set_blue_mB65DC7FD20C551501BC181C457D010DCEECDEE7F_AdjustorThunk,
	LinearColor_Convert_m7C35C63BFFDD93EBCC6E8050567B79562B82823A,
	LinearColor_Black_m50DB4626285C618DFD9F561573AA77F84AE7E180,
	LightDataGI_Init_m135DF5CF6CBECA4CBA0AC71F9CDEEF8DE25606DB_AdjustorThunk,
	LightDataGI_Init_m4E8BEBD383D5F6F1A1EDFEC763A718A7271C22A6_AdjustorThunk,
	LightDataGI_Init_mC9948FAC4A191C99E3E7D94677B7CFD241857C86_AdjustorThunk,
	LightDataGI_Init_mA0DF9747C6AD308EAAF2A9530B4225A3D65BB092_AdjustorThunk,
	LightDataGI_Init_mB96C3F3E00F10DD0806BD3DB93B58C2299D59E94_AdjustorThunk,
	LightDataGI_InitNoBake_mF600D612DE9A1CE4355C61317F6173E1AAEFBD57_AdjustorThunk,
	LightmapperUtils_Extract_m32B54C9DC94AE03162E3896C5FA00FE9678F9081,
	LightmapperUtils_ExtractIndirect_mC17A833A46BAAA01B55F8BA8A5821292AB104F54,
	LightmapperUtils_ExtractInnerCone_mEF618AE5A5D8EB690F3BA162196859FE6F662947,
	LightmapperUtils_ExtractColorTemperature_m5052DE4DC7630A7077EA2B8C6AA903257C95AFA5,
	LightmapperUtils_ApplyColorTemperature_mA49FB616EB2F9F31AF4CCB4C964592005DCEA346,
	LightmapperUtils_Extract_mCBEC26CC920C0D87DF6E25417533923E26CB6DFC,
	LightmapperUtils_Extract_mB11D8F3B35F96E176A89517A25CD1A54DCBD7D6E,
	LightmapperUtils_Extract_mB1572C38F682F043180745B7A231FBE07CD205E4,
	LightmapperUtils_Extract_mEABF77895D51E1CA5FD380682539C3DD3FC8A91C,
	LightmapperUtils_Extract_m93B350DDA0CB5B624054835BAF46C177472E9212,
	LightmapperUtils_Extract_mACAC5E823E243A53EDD2A01CF857DD16C3FDBE2A,
	Lightmapping_SetDelegate_m3C7B041BEEBD50C1EF3C0D9D5BC2162E93E19979,
	Lightmapping_GetDelegate_mD44EBB65CFD4038D77119A3F896B55905DF9A9DC,
	Lightmapping_ResetDelegate_m6EF8586283713477679876577D753EFDCA74A6F3,
	Lightmapping_RequestLights_m40C73984B1F2DB34C58965D1C4D321181C7E5976,
	Lightmapping__cctor_m94640A0363C80E0E1438E4EE650AED85AB3E576C,
	RequestLightsDelegate__ctor_m47823FBD9D2EE4ABA5EE8D889BBBC0783FB10A42,
	RequestLightsDelegate_Invoke_m8BC0D55744A3FA2E75444AC72B3D3E4FB858BECB,
	RequestLightsDelegate_BeginInvoke_m75482E3D4E28205DCB4A202F5C144CB7C95622A6,
	RequestLightsDelegate_EndInvoke_mA1FF787B6F3182ACF6157D13F9A84AFCBB66EE60,
	U3CU3Ec__cctor_m2A00D547FF8F6F8A03666B4737BB9A0620719987,
	U3CU3Ec__ctor_m8F825BEC75A119B6CDDA0985A3F7BA3DEA8AD5B2,
	U3CU3Ec_U3C_cctorU3Eb__7_0_m84A19BB5BB12263A1E3C52F1B351E3A24BE546C7,
	CameraPlayable_GetHandle_m12A0FB549E5257C9AEBCF76B6E2DC49FE5F8B7C5_AdjustorThunk,
	CameraPlayable_Equals_m82D036759943861CAB110D108F966C60216E6327_AdjustorThunk,
	MaterialEffectPlayable_GetHandle_mC4AA4C850DFB33EBA45EDA3D0524294EC03044FC_AdjustorThunk,
	MaterialEffectPlayable_Equals_mC805BF647B60EA8517040C2C0716CFCB7F04CAFB_AdjustorThunk,
	TextureMixerPlayable_GetHandle_m44A48E52180084F5A93942EA0AFA454C9DFBFD40_AdjustorThunk,
	TextureMixerPlayable_Equals_m49E1B77DF4F13F35321494AC6B5330538D0A1980_AdjustorThunk,
	BuiltinRuntimeReflectionSystem_TickRealtimeProbes_mDE45D3D3E07AB0FF8C10791544CB29FF6D44DFA2,
	BuiltinRuntimeReflectionSystem_Dispose_mFF4F5CDB37BB93A28975F7BFE970040964B48F5A,
	BuiltinRuntimeReflectionSystem_Dispose_mFBDDD8FE2956E99DB34533F8C29621C3E938BD13,
	BuiltinRuntimeReflectionSystem_BuiltinUpdate_mEDD980F13F6200E5B89742D6868C4EF4858AE405,
	BuiltinRuntimeReflectionSystem_Internal_BuiltinRuntimeReflectionSystem_New_mA4A701BE60FC41AD01F7AC965DACA950B4C9560C,
	BuiltinRuntimeReflectionSystem__ctor_m418427E040351EC086975D95B409F2C6E3E41045,
	NULL,
	ScriptableRuntimeReflectionSystemSettings_set_Internal_ScriptableRuntimeReflectionSystemSettings_system_mE9EF71AD222FC661C616AC9687961D98946D8680,
	ScriptableRuntimeReflectionSystemSettings_get_Internal_ScriptableRuntimeReflectionSystemSettings_instance_mC8110BFC8188AAFC7D4EC56780617AB33CB2D71C,
	ScriptableRuntimeReflectionSystemSettings_ScriptingDirtyReflectionSystemInstance_mE4FFB1863BE37B6E20388C15D2C48F4E01FCFEEF,
	ScriptableRuntimeReflectionSystemSettings__cctor_m24D01EC03C21F2E3A40CC9C0DC4A646C8690096A,
	ScriptableRuntimeReflectionSystemWrapper_get_implementation_mD0D0BB589A80E0B0C53491CC916EE406378649D6,
	ScriptableRuntimeReflectionSystemWrapper_set_implementation_m95A62C63F5D1D50EDCD5351D74F73EEBC0A239B1,
	ScriptableRuntimeReflectionSystemWrapper_Internal_ScriptableRuntimeReflectionSystemWrapper_TickRealtimeProbes_mC04DACDD9BF402C3D12DE78F286A36B3A81BA547,
	ScriptableRuntimeReflectionSystemWrapper__ctor_m14586B1A430F0316A379C966D52BDE6410BA5FCC,
	GraphicsFormatUtility_GetGraphicsFormat_mF9AFEB31DE7E63FC76D6A99AE31A108491A9F232,
	GraphicsFormatUtility_GetGraphicsFormat_Native_TextureFormat_mAE09EC30C3D01C3190767E2590DC0FD9358A1C5C,
	GraphicsFormatUtility_GetGraphicsFormat_mD73B7F075511D7D392D55B146711F19A76E5AF1C,
	GraphicsFormatUtility_GetGraphicsFormat_Native_RenderTextureFormat_m7F44D525B67F585D711628BC2981852A6DA7633B,
	GraphicsFormatUtility_GetGraphicsFormat_m5ED879E5A23929743CD65735DEDDF3BE701946D8,
	GraphicsFormatUtility_IsSRGBFormat_mDA5982709BD21EE1163A90381100F6C7C6F40F1C,
	GraphicsFormatUtility_GetRenderTextureFormat_m46863058423E437148320C07088F53250E781F95,
	GraphicsFormatUtility_IsCompressedTextureFormat_m740C48D113EFDF97CD6EB48308B486F68C03CF82,
	GraphicsFormatUtility_IsCrunchFormat_mB31D5C0C0D337A3B00D1AED3A7E036CD52540F66,
};
static const int32_t s_InvokerIndices[2517] = 
{
	3,
	32,
	26,
	14,
	23,
	26,
	1535,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	89,
	3,
	17,
	389,
	26,
	1536,
	1537,
	1538,
	1539,
	1540,
	349,
	1541,
	1542,
	23,
	23,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	23,
	23,
	-1,
	1543,
	354,
	1544,
	1545,
	238,
	94,
	109,
	109,
	1,
	109,
	0,
	-1,
	-1,
	-1,
	-1,
	-1,
	21,
	1546,
	761,
	339,
	25,
	24,
	1547,
	23,
	1548,
	14,
	26,
	1549,
	1549,
	1550,
	10,
	26,
	1550,
	14,
	26,
	23,
	9,
	9,
	10,
	1159,
	864,
	178,
	3,
	49,
	4,
	4,
	4,
	4,
	178,
	115,
	49,
	115,
	3,
	739,
	49,
	3,
	3,
	3,
	889,
	168,
	49,
	133,
	23,
	114,
	26,
	133,
	110,
	757,
	26,
	18,
	7,
	761,
	339,
	761,
	339,
	761,
	339,
	10,
	32,
	10,
	89,
	761,
	339,
	89,
	31,
	761,
	339,
	761,
	339,
	10,
	32,
	10,
	1551,
	1552,
	1553,
	10,
	32,
	10,
	32,
	1554,
	1554,
	10,
	10,
	14,
	26,
	10,
	1555,
	1551,
	1555,
	1551,
	1551,
	31,
	23,
	1556,
	1557,
	1557,
	1558,
	1558,
	1558,
	1559,
	1560,
	1561,
	1559,
	1560,
	1561,
	4,
	4,
	115,
	94,
	115,
	94,
	23,
	62,
	62,
	62,
	62,
	168,
	168,
	168,
	23,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	580,
	1562,
	1562,
	580,
	1562,
	1562,
	133,
	26,
	220,
	26,
	1563,
	133,
	1564,
	1565,
	26,
	375,
	168,
	1566,
	144,
	40,
	27,
	23,
	4,
	1567,
	1568,
	1569,
	1570,
	1567,
	3,
	168,
	144,
	144,
	168,
	144,
	144,
	203,
	168,
	144,
	168,
	144,
	144,
	203,
	142,
	3,
	1571,
	1572,
	10,
	9,
	1573,
	1574,
	1575,
	1574,
	1575,
	1574,
	1575,
	1574,
	1574,
	1576,
	1576,
	1572,
	1575,
	1577,
	14,
	114,
	1578,
	390,
	1574,
	1572,
	1579,
	1580,
	14,
	114,
	1572,
	1574,
	1574,
	1575,
	1581,
	14,
	114,
	1546,
	1582,
	1583,
	761,
	339,
	761,
	339,
	1584,
	1585,
	1584,
	1584,
	1584,
	761,
	339,
	761,
	339,
	1584,
	1585,
	761,
	339,
	761,
	339,
	761,
	339,
	761,
	339,
	1586,
	1578,
	1587,
	1588,
	1589,
	1590,
	1590,
	10,
	9,
	1588,
	14,
	114,
	10,
	10,
	10,
	10,
	14,
	114,
	1591,
	23,
	133,
	23,
	343,
	14,
	114,
	23,
	799,
	25,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	10,
	23,
	1570,
	1592,
	349,
	17,
	3,
	3,
	23,
	7,
	10,
	10,
	10,
	10,
	1593,
	4,
	168,
	3,
	1594,
	1594,
	1595,
	3,
	133,
	23,
	114,
	26,
	115,
	115,
	1596,
	1597,
	49,
	115,
	1598,
	916,
	1599,
	1600,
	4,
	17,
	115,
	1601,
	1602,
	1603,
	17,
	1604,
	144,
	1601,
	1605,
	1602,
	1603,
	1606,
	1606,
	1607,
	144,
	1268,
	203,
	1608,
	1609,
	1610,
	3,
	1611,
	1612,
	1612,
	1613,
	49,
	889,
	3,
	3,
	3,
	1614,
	1614,
	178,
	3,
	1615,
	1616,
	1617,
	10,
	10,
	14,
	178,
	115,
	178,
	1596,
	1618,
	178,
	115,
	178,
	115,
	939,
	4,
	115,
	23,
	339,
	339,
	1619,
	864,
	1053,
	799,
	25,
	23,
	23,
	23,
	977,
	1620,
	14,
	14,
	26,
	14,
	26,
	26,
	26,
	26,
	26,
	89,
	31,
	32,
	31,
	10,
	32,
	10,
	32,
	14,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	6,
	49,
	889,
	1596,
	1618,
	1596,
	1618,
	115,
	178,
	1621,
	1592,
	1596,
	1618,
	1621,
	1592,
	17,
	17,
	17,
	17,
	0,
	32,
	168,
	168,
	94,
	1622,
	1623,
	23,
	144,
	144,
	168,
	26,
	26,
	26,
	14,
	26,
	1552,
	1553,
	14,
	26,
	37,
	30,
	9,
	32,
	26,
	26,
	9,
	10,
	27,
	1137,
	108,
	30,
	26,
	14,
	26,
	14,
	26,
	1053,
	1624,
	1625,
	62,
	62,
	1626,
	1627,
	34,
	977,
	1053,
	139,
	138,
	1628,
	1624,
	1629,
	1630,
	1625,
	27,
	62,
	27,
	233,
	1626,
	1631,
	1627,
	1632,
	1633,
	28,
	34,
	864,
	864,
	864,
	10,
	32,
	761,
	1552,
	1553,
	761,
	339,
	761,
	761,
	339,
	1634,
	32,
	10,
	32,
	339,
	761,
	14,
	6,
	6,
	6,
	14,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	168,
	23,
	93,
	93,
	1635,
	32,
	30,
	1636,
	208,
	89,
	10,
	10,
	32,
	1620,
	1577,
	31,
	32,
	32,
	23,
	31,
	21,
	21,
	-1,
	-1,
	1636,
	-1,
	-1,
	-1,
	-1,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	14,
	14,
	26,
	26,
	35,
	209,
	26,
	35,
	209,
	26,
	35,
	209,
	26,
	35,
	209,
	-1,
	62,
	498,
	1637,
	23,
	890,
	30,
	30,
	14,
	26,
	34,
	93,
	38,
	1638,
	139,
	1474,
	1639,
	139,
	1474,
	1639,
	35,
	1640,
	787,
	31,
	23,
	23,
	23,
	32,
	32,
	23,
	31,
	6,
	6,
	23,
	10,
	10,
	10,
	10,
	10,
	32,
	10,
	32,
	10,
	89,
	10,
	32,
	10,
	32,
	32,
	339,
	1584,
	10,
	10,
	30,
	52,
	28,
	3,
	6,
	10,
	4,
	31,
	1641,
	1642,
	89,
	42,
	52,
	1643,
	1644,
	1645,
	1646,
	1647,
	1648,
	139,
	1649,
	1650,
	1651,
	34,
	14,
	844,
	54,
	1652,
	341,
	39,
	138,
	1653,
	1649,
	1654,
	26,
	1655,
	1656,
	42,
	31,
	23,
	52,
	1646,
	1647,
	1657,
	139,
	26,
	1649,
	1654,
	34,
	14,
	1658,
	1658,
	1659,
	1660,
	1661,
	1662,
	89,
	38,
	38,
	38,
	343,
	1663,
	1664,
	1172,
	89,
	1665,
	1666,
	42,
	139,
	26,
	526,
	526,
	307,
	526,
	1667,
	42,
	23,
	115,
	10,
	10,
	89,
	1665,
	1666,
	42,
	35,
	526,
	526,
	307,
	1668,
	1669,
	1667,
	42,
	89,
	1670,
	1671,
	343,
	343,
	526,
	1667,
	341,
	39,
	10,
	32,
	10,
	32,
	10,
	10,
	32,
	89,
	10,
	31,
	4,
	168,
	4,
	168,
	1672,
	1672,
	1672,
	1672,
	89,
	23,
	89,
	31,
	168,
	1673,
	1674,
	1675,
	168,
	10,
	32,
	23,
	1673,
	26,
	343,
	343,
	526,
	526,
	343,
	38,
	526,
	1674,
	1673,
	1676,
	182,
	1675,
	1677,
	1678,
	1679,
	6,
	6,
	6,
	6,
	96,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	32,
	10,
	32,
	10,
	32,
	32,
	32,
	32,
	32,
	343,
	526,
	859,
	31,
	31,
	3,
	1680,
	14,
	1681,
	1682,
	397,
	1681,
	9,
	1683,
	10,
	121,
	1684,
	1684,
	1684,
	397,
	96,
	1685,
	889,
	115,
	178,
	690,
	14,
	62,
	371,
	371,
	40,
	27,
	26,
	14,
	26,
	89,
	31,
	10,
	32,
	30,
	0,
	62,
	371,
	371,
	40,
	27,
	168,
	168,
	3,
	14,
	617,
	26,
	35,
	23,
	1546,
	1686,
	14,
	114,
	10,
	9,
	1687,
	1688,
	1689,
	1690,
	1690,
	1691,
	1692,
	1621,
	1621,
	1621,
	1621,
	1621,
	1621,
	1621,
	1621,
	1621,
	1621,
	1621,
	1552,
	761,
	1693,
	1694,
	1695,
	1696,
	1697,
	1698,
	85,
	1699,
	1700,
	1701,
	14,
	114,
	230,
	230,
	1702,
	799,
	23,
	1547,
	23,
	23,
	9,
	9,
	10,
	1703,
	1574,
	1703,
	1574,
	1704,
	1705,
	1555,
	1706,
	1707,
	1708,
	1709,
	1626,
	1053,
	10,
	9,
	1710,
	1711,
	1712,
	1633,
	1630,
	1558,
	1558,
	1558,
	1713,
	1713,
	14,
	114,
	3,
	349,
	349,
	1714,
	349,
	1715,
	349,
	349,
	1716,
	1717,
	1626,
	1053,
	1686,
	1718,
	1719,
	1575,
	1719,
	10,
	9,
	1578,
	1719,
	1593,
	23,
	1574,
	1720,
	1719,
	1720,
	1721,
	1720,
	1722,
	761,
	761,
	1719,
	1719,
	1723,
	1723,
	1723,
	1723,
	1723,
	1723,
	1723,
	1723,
	1719,
	1719,
	1593,
	1724,
	1725,
	1724,
	1726,
	1726,
	14,
	114,
	3,
	1727,
	1728,
	1729,
	1730,
	1731,
	1732,
	1728,
	1730,
	1546,
	1733,
	1734,
	1735,
	256,
	1736,
	1736,
	1737,
	1593,
	1574,
	1738,
	1730,
	10,
	9,
	1739,
	14,
	114,
	3,
	954,
	349,
	349,
	349,
	1740,
	954,
	21,
	446,
	446,
	448,
	446,
	446,
	446,
	446,
	446,
	446,
	448,
	446,
	446,
	21,
	448,
	182,
	448,
	182,
	448,
	446,
	448,
	446,
	446,
	446,
	266,
	266,
	266,
	446,
	1741,
	183,
	446,
	1741,
	1741,
	1742,
	1743,
	1744,
	1743,
	1744,
	448,
	1741,
	448,
	3,
	1626,
	1053,
	1718,
	1745,
	23,
	1584,
	14,
	114,
	10,
	9,
	1586,
	1746,
	761,
	761,
	1746,
	1745,
	1745,
	1745,
	1745,
	1747,
	1748,
	1748,
	1749,
	1749,
	1750,
	1751,
	1752,
	1752,
	1752,
	1752,
	1752,
	3,
	10,
	32,
	10,
	32,
	138,
	1753,
	9,
	1754,
	10,
	14,
	114,
	3,
	10,
	10,
	10,
	38,
	1755,
	9,
	1756,
	10,
	14,
	114,
	3,
	1626,
	1053,
	1546,
	10,
	9,
	1757,
	1758,
	761,
	1759,
	1759,
	1760,
	1760,
	1761,
	1761,
	1762,
	1763,
	1764,
	14,
	114,
	3,
	23,
	23,
	1765,
	1766,
	23,
	1068,
	1068,
	89,
	1765,
	1766,
	23,
	1068,
	1068,
	23,
	89,
	23,
	49,
	3,
	168,
	168,
	212,
	497,
	3,
	3,
	23,
	26,
	142,
	144,
	1,
	0,
	109,
	23,
	26,
	23,
	339,
	26,
	1718,
	138,
	31,
	42,
	178,
	1767,
	1768,
	448,
	182,
	182,
	1596,
	1723,
	17,
	1752,
	1723,
	17,
	17,
	17,
	17,
	-1,
	0,
	-1,
	0,
	-1,
	1,
	1,
	-1,
	4,
	25,
	89,
	761,
	23,
	23,
	26,
	26,
	23,
	0,
	0,
	94,
	94,
	94,
	-1,
	3,
	23,
	26,
	26,
	139,
	26,
	26,
	23,
	26,
	102,
	1414,
	23,
	23,
	23,
	26,
	10,
	23,
	89,
	31,
	89,
	23,
	3,
	14,
	14,
	28,
	133,
	-1,
	108,
	-1,
	-1,
	-1,
	-1,
	-1,
	28,
	-1,
	-1,
	-1,
	-1,
	27,
	27,
	-1,
	14,
	26,
	-1,
	26,
	110,
	139,
	23,
	23,
	23,
	25,
	1035,
	2,
	89,
	14,
	89,
	23,
	23,
	23,
	23,
	43,
	-1,
	28,
	133,
	108,
	-1,
	-1,
	108,
	28,
	1769,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	28,
	28,
	-1,
	14,
	10,
	32,
	31,
	89,
	89,
	14,
	26,
	9,
	0,
	110,
	27,
	26,
	26,
	23,
	27,
	144,
	0,
	1770,
	14,
	6,
	1771,
	1772,
	10,
	43,
	94,
	168,
	1773,
	1774,
	1035,
	89,
	23,
	977,
	1775,
	26,
	9,
	28,
	114,
	28,
	28,
	26,
	26,
	26,
	23,
	89,
	31,
	168,
	168,
	109,
	1776,
	144,
	142,
	109,
	114,
	28,
	26,
	26,
	14,
	23,
	94,
	-1,
	0,
	10,
	138,
	23,
	32,
	32,
	23,
	0,
	-1,
	168,
	125,
	49,
	23,
	168,
	4,
	467,
	0,
	3,
	23,
	26,
	120,
	14,
	14,
	14,
	0,
	3,
	3,
	3,
	23,
	27,
	10,
	10,
	9,
	109,
	142,
	109,
	15,
	14,
	26,
	1777,
	1778,
	0,
	-1,
	-1,
	1623,
	168,
	625,
	168,
	125,
	168,
	10,
	32,
	-1,
	-1,
	144,
	125,
	14,
	142,
	142,
	115,
	0,
	1777,
	1779,
	0,
	0,
	144,
	43,
	23,
	3,
	1780,
	1025,
	32,
	139,
	27,
	23,
	23,
	27,
	14,
	23,
	89,
	3,
	3,
	255,
	214,
	23,
	23,
	339,
	761,
	339,
	89,
	339,
	23,
	89,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	1781,
	168,
	138,
	526,
	23,
	26,
	782,
	121,
	1630,
	61,
	626,
	626,
	343,
	1629,
	371,
	371,
	864,
	23,
	23,
	1596,
	115,
	4,
	115,
	4,
	115,
	115,
	115,
	4,
	4,
	4,
	49,
	49,
	49,
	49,
	49,
	115,
	115,
	4,
	4,
	115,
	115,
	115,
	49,
	4,
	115,
	49,
	115,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	115,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	115,
	49,
	115,
	115,
	49,
	49,
	115,
	49,
	115,
	109,
	46,
	46,
	115,
	115,
	115,
	115,
	115,
	115,
	115,
	115,
	115,
	115,
	115,
	115,
	115,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	115,
	49,
	115,
	49,
	49,
	1596,
	115,
	4,
	115,
	4,
	115,
	115,
	115,
	4,
	4,
	4,
	49,
	49,
	49,
	49,
	49,
	115,
	115,
	4,
	4,
	115,
	115,
	115,
	49,
	4,
	115,
	49,
	115,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	115,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	115,
	49,
	115,
	115,
	115,
	115,
	115,
	115,
	115,
	115,
	49,
	49,
	115,
	49,
	46,
	46,
	115,
	115,
	115,
	115,
	115,
	115,
	115,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	49,
	53,
	182,
	21,
	49,
	115,
	49,
	49,
	23,
	1596,
	1596,
	1596,
	1596,
	1596,
	1596,
	1596,
	1596,
	1596,
	1618,
	115,
	1596,
	25,
	23,
	23,
	1782,
	1783,
	49,
	49,
	49,
	1784,
	14,
	26,
	889,
	89,
	31,
	10,
	32,
	89,
	89,
	1785,
	1786,
	349,
	179,
	1787,
	14,
	9,
	1788,
	10,
	3,
	110,
	23,
	168,
	168,
	1554,
	1584,
	1585,
	1584,
	1585,
	1584,
	1585,
	1584,
	1585,
	1584,
	1585,
	1584,
	1585,
	1584,
	1585,
	26,
	26,
	1053,
	168,
	1584,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	133,
	26,
	220,
	26,
	23,
	1574,
	1575,
	1574,
	1575,
	1574,
	1575,
	1574,
	1575,
	1574,
	1574,
	1574,
	1575,
	1703,
	1789,
	1703,
	1789,
	1574,
	1575,
	14,
	26,
	14,
	26,
	14,
	26,
	102,
	1555,
	1555,
	1790,
	1686,
	1790,
	1575,
	1791,
	1686,
	1792,
	1793,
	1792,
	1794,
	1795,
	1572,
	1575,
	1572,
	1558,
	1558,
	1558,
	10,
	23,
	221,
	28,
	1574,
	9,
	31,
	14,
	34,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	1796,
	580,
	580,
	580,
	580,
	6,
	26,
	14,
	89,
	23,
	23,
	10,
	10,
	1554,
	1797,
	1797,
	1797,
	1620,
	1554,
	1797,
	14,
	761,
	14,
	1584,
	89,
	10,
	1554,
	14,
	14,
	14,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	6,
	1087,
	1087,
	1798,
	1798,
	1798,
	1799,
	109,
	168,
	168,
	168,
	168,
	3,
	9,
	28,
	165,
	1800,
	32,
	32,
	32,
	23,
	4,
	593,
	379,
	625,
	1801,
	133,
	138,
	1802,
	26,
	133,
	32,
	602,
	26,
	179,
	178,
	178,
	178,
	133,
	32,
	602,
	26,
	133,
	32,
	602,
	26,
	1803,
	1804,
	133,
	1805,
	1806,
	26,
	26,
	139,
	32,
	139,
	133,
	26,
	220,
	26,
	133,
	139,
	149,
	26,
	133,
	32,
	602,
	26,
	133,
	139,
	149,
	26,
	1807,
	1808,
	1035,
	7,
	167,
	167,
	167,
	1809,
	23,
	23,
	23,
	23,
	3,
	133,
	26,
	220,
	26,
	133,
	1810,
	1811,
	26,
	133,
	1810,
	1811,
	26,
	133,
	1810,
	1811,
	26,
	133,
	1812,
	1813,
	26,
	10,
	32,
	31,
	32,
	10,
	89,
	10,
	7,
	23,
	23,
	23,
	23,
	1814,
	1035,
	7,
	167,
	167,
	167,
	167,
	23,
	23,
	23,
	23,
	3,
	133,
	26,
	220,
	26,
	133,
	1815,
	1816,
	26,
	133,
	1815,
	1816,
	26,
	133,
	1815,
	1816,
	26,
	133,
	1815,
	1816,
	26,
	0,
	14,
	14,
	10,
	761,
	14,
	89,
	23,
	23,
	23,
	23,
	27,
	26,
	-1,
	109,
	90,
	26,
	26,
	27,
	26,
	26,
	23,
	90,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	14,
	14,
	10,
	14,
	89,
	28,
	2,
	23,
	23,
	23,
	23,
	27,
	26,
	26,
	27,
	23,
	14,
	23,
	23,
	23,
	23,
	114,
	114,
	28,
	112,
	23,
	23,
	26,
	27,
	14,
	14,
	2,
	133,
	23,
	114,
	26,
	23,
	26,
	114,
	114,
	0,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	23,
	1447,
	26,
	43,
	21,
	10,
	14,
	10,
	1817,
	10,
	9,
	115,
	1818,
	1819,
	1820,
	168,
	168,
	168,
	1821,
	178,
	1822,
	1823,
	1824,
	1825,
	17,
	1826,
	1827,
	32,
	133,
	23,
	114,
	26,
	23,
	4,
	89,
	4,
	23,
	14,
	1828,
	1828,
	26,
	26,
	26,
	26,
	1828,
	1829,
	1830,
	23,
	1831,
	178,
	178,
	23,
	23,
	9,
	23,
	9,
	23,
	26,
	1765,
	1116,
	1828,
	23,
	23,
	23,
	730,
	1068,
	23,
	23,
	9,
	23,
	9,
	23,
	9,
	26,
	14,
	9,
	10,
	23,
	46,
	32,
	32,
	26,
	1832,
	1832,
	1833,
	14,
	10,
	1834,
	9,
	49,
	115,
	17,
	3,
	799,
	23,
	26,
	23,
	1835,
	1836,
	1837,
	1838,
	1839,
	308,
	307,
	1840,
	1841,
	1842,
	1843,
	308,
	307,
	526,
	32,
	1053,
	1630,
	52,
	679,
	1844,
	1845,
	23,
	23,
	31,
	23,
	1835,
	1846,
	1847,
	1848,
	1849,
	1850,
	1851,
	1852,
	1853,
	864,
	1854,
	1855,
	467,
	133,
	1856,
	1857,
	1858,
	1859,
	9,
	10,
	1860,
	1860,
	89,
	31,
	23,
	31,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	4,
	168,
	3,
	1861,
	1035,
	168,
	3,
	10,
	34,
	7,
	10,
	34,
	1862,
	9,
	10,
	504,
	303,
	4,
	168,
	10,
	10,
	10,
	10,
	89,
	89,
	25,
	46,
	1863,
	46,
	1863,
	1863,
	1863,
	25,
	25,
	23,
	3,
	1864,
	1865,
	1865,
	1865,
	1865,
	1866,
	1866,
	1866,
	1867,
	1868,
	1869,
	1870,
	1871,
	3,
	1872,
	466,
	14,
	1873,
	1035,
	23,
	23,
	1865,
	1865,
	1865,
	1865,
	1866,
	1866,
	1866,
	1867,
	14,
	3,
	133,
	1874,
	1875,
	1876,
	-1,
	1877,
	1878,
	9,
	1879,
	10,
	1878,
	89,
	14,
	3,
	389,
	96,
	1880,
	1881,
	1882,
	3,
	1883,
	10,
	1884,
	9,
	1885,
	1884,
	3,
	761,
	339,
	761,
	339,
	761,
	339,
	1886,
	1887,
	580,
	580,
	580,
	580,
	580,
	32,
	46,
	1888,
	495,
	1889,
	1890,
	397,
	397,
	397,
	397,
	397,
	397,
	168,
	4,
	3,
	1563,
	3,
	133,
	1891,
	1892,
	26,
	3,
	23,
	1891,
	1870,
	1893,
	1870,
	1894,
	1870,
	1895,
	89,
	23,
	31,
	49,
	4,
	23,
	89,
	168,
	4,
	3,
	3,
	14,
	26,
	6,
	23,
	302,
	302,
	302,
	302,
	182,
	46,
	21,
	46,
	46,
};
static const Il2CppTokenRangePair s_rgctxIndices[60] = 
{
	{ 0x0200000B, { 3, 6 } },
	{ 0x0200000F, { 12, 6 } },
	{ 0x02000021, { 18, 16 } },
	{ 0x02000022, { 34, 4 } },
	{ 0x0200013B, { 100, 7 } },
	{ 0x0200013C, { 107, 7 } },
	{ 0x0200013D, { 114, 9 } },
	{ 0x0200013E, { 123, 11 } },
	{ 0x0200013F, { 134, 3 } },
	{ 0x02000148, { 137, 8 } },
	{ 0x0200014A, { 145, 4 } },
	{ 0x0200014C, { 149, 9 } },
	{ 0x0200014E, { 158, 6 } },
	{ 0x06000009, { 0, 3 } },
	{ 0x06000011, { 9, 3 } },
	{ 0x06000054, { 38, 2 } },
	{ 0x06000055, { 40, 1 } },
	{ 0x06000056, { 41, 1 } },
	{ 0x06000058, { 42, 1 } },
	{ 0x06000277, { 43, 2 } },
	{ 0x06000278, { 45, 1 } },
	{ 0x0600027C, { 46, 1 } },
	{ 0x0600027D, { 47, 1 } },
	{ 0x06000298, { 48, 1 } },
	{ 0x060004FA, { 49, 2 } },
	{ 0x060004FC, { 51, 2 } },
	{ 0x060004FE, { 53, 2 } },
	{ 0x06000501, { 55, 2 } },
	{ 0x06000510, { 57, 2 } },
	{ 0x0600052B, { 59, 1 } },
	{ 0x0600052D, { 60, 2 } },
	{ 0x0600052E, { 62, 1 } },
	{ 0x0600052F, { 63, 1 } },
	{ 0x06000530, { 64, 1 } },
	{ 0x06000531, { 65, 1 } },
	{ 0x06000533, { 66, 2 } },
	{ 0x06000534, { 68, 1 } },
	{ 0x06000535, { 69, 1 } },
	{ 0x06000536, { 70, 1 } },
	{ 0x06000539, { 71, 1 } },
	{ 0x0600053C, { 72, 1 } },
	{ 0x0600054E, { 73, 1 } },
	{ 0x06000552, { 74, 1 } },
	{ 0x06000553, { 75, 2 } },
	{ 0x06000557, { 77, 2 } },
	{ 0x06000558, { 79, 1 } },
	{ 0x06000559, { 80, 2 } },
	{ 0x0600055A, { 82, 1 } },
	{ 0x0600055B, { 83, 1 } },
	{ 0x0600055C, { 84, 1 } },
	{ 0x0600055D, { 85, 2 } },
	{ 0x06000560, { 87, 2 } },
	{ 0x0600059D, { 89, 1 } },
	{ 0x060005A6, { 90, 2 } },
	{ 0x060005C8, { 92, 1 } },
	{ 0x060005C9, { 93, 1 } },
	{ 0x060005D2, { 94, 2 } },
	{ 0x060005D3, { 96, 2 } },
	{ 0x0600081F, { 98, 2 } },
	{ 0x0600097C, { 164, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[165] = 
{
	{ (Il2CppRGCTXDataType)3, 48874 },
	{ (Il2CppRGCTXDataType)3, 48875 },
	{ (Il2CppRGCTXDataType)2, 27442 },
	{ (Il2CppRGCTXDataType)2, 27443 },
	{ (Il2CppRGCTXDataType)1, 12138 },
	{ (Il2CppRGCTXDataType)3, 48876 },
	{ (Il2CppRGCTXDataType)2, 27444 },
	{ (Il2CppRGCTXDataType)3, 48877 },
	{ (Il2CppRGCTXDataType)2, 12138 },
	{ (Il2CppRGCTXDataType)3, 48878 },
	{ (Il2CppRGCTXDataType)3, 48879 },
	{ (Il2CppRGCTXDataType)2, 27445 },
	{ (Il2CppRGCTXDataType)2, 27446 },
	{ (Il2CppRGCTXDataType)1, 12154 },
	{ (Il2CppRGCTXDataType)3, 48880 },
	{ (Il2CppRGCTXDataType)2, 27447 },
	{ (Il2CppRGCTXDataType)3, 48881 },
	{ (Il2CppRGCTXDataType)2, 12154 },
	{ (Il2CppRGCTXDataType)3, 48882 },
	{ (Il2CppRGCTXDataType)2, 12201 },
	{ (Il2CppRGCTXDataType)3, 48883 },
	{ (Il2CppRGCTXDataType)3, 48884 },
	{ (Il2CppRGCTXDataType)3, 48885 },
	{ (Il2CppRGCTXDataType)3, 48886 },
	{ (Il2CppRGCTXDataType)3, 48887 },
	{ (Il2CppRGCTXDataType)3, 48888 },
	{ (Il2CppRGCTXDataType)3, 48889 },
	{ (Il2CppRGCTXDataType)2, 12202 },
	{ (Il2CppRGCTXDataType)3, 48890 },
	{ (Il2CppRGCTXDataType)3, 48891 },
	{ (Il2CppRGCTXDataType)2, 12201 },
	{ (Il2CppRGCTXDataType)3, 48892 },
	{ (Il2CppRGCTXDataType)3, 48893 },
	{ (Il2CppRGCTXDataType)3, 48894 },
	{ (Il2CppRGCTXDataType)3, 48895 },
	{ (Il2CppRGCTXDataType)3, 48896 },
	{ (Il2CppRGCTXDataType)3, 48897 },
	{ (Il2CppRGCTXDataType)2, 12210 },
	{ (Il2CppRGCTXDataType)3, 48898 },
	{ (Il2CppRGCTXDataType)3, 48899 },
	{ (Il2CppRGCTXDataType)2, 12247 },
	{ (Il2CppRGCTXDataType)2, 12248 },
	{ (Il2CppRGCTXDataType)2, 27450 },
	{ (Il2CppRGCTXDataType)2, 12498 },
	{ (Il2CppRGCTXDataType)2, 12498 },
	{ (Il2CppRGCTXDataType)3, 48900 },
	{ (Il2CppRGCTXDataType)3, 48901 },
	{ (Il2CppRGCTXDataType)3, 48902 },
	{ (Il2CppRGCTXDataType)3, 48903 },
	{ (Il2CppRGCTXDataType)2, 12642 },
	{ (Il2CppRGCTXDataType)2, 12643 },
	{ (Il2CppRGCTXDataType)1, 12645 },
	{ (Il2CppRGCTXDataType)3, 48904 },
	{ (Il2CppRGCTXDataType)1, 12646 },
	{ (Il2CppRGCTXDataType)2, 12646 },
	{ (Il2CppRGCTXDataType)1, 12647 },
	{ (Il2CppRGCTXDataType)2, 12647 },
	{ (Il2CppRGCTXDataType)1, 12654 },
	{ (Il2CppRGCTXDataType)2, 12654 },
	{ (Il2CppRGCTXDataType)1, 12684 },
	{ (Il2CppRGCTXDataType)1, 12685 },
	{ (Il2CppRGCTXDataType)2, 12685 },
	{ (Il2CppRGCTXDataType)3, 48905 },
	{ (Il2CppRGCTXDataType)3, 48906 },
	{ (Il2CppRGCTXDataType)3, 48907 },
	{ (Il2CppRGCTXDataType)3, 48908 },
	{ (Il2CppRGCTXDataType)1, 12694 },
	{ (Il2CppRGCTXDataType)2, 12694 },
	{ (Il2CppRGCTXDataType)3, 48909 },
	{ (Il2CppRGCTXDataType)3, 48910 },
	{ (Il2CppRGCTXDataType)3, 48911 },
	{ (Il2CppRGCTXDataType)1, 12703 },
	{ (Il2CppRGCTXDataType)3, 48912 },
	{ (Il2CppRGCTXDataType)1, 12719 },
	{ (Il2CppRGCTXDataType)3, 48913 },
	{ (Il2CppRGCTXDataType)1, 12721 },
	{ (Il2CppRGCTXDataType)2, 12721 },
	{ (Il2CppRGCTXDataType)1, 12723 },
	{ (Il2CppRGCTXDataType)2, 12722 },
	{ (Il2CppRGCTXDataType)1, 12725 },
	{ (Il2CppRGCTXDataType)1, 12727 },
	{ (Il2CppRGCTXDataType)2, 12726 },
	{ (Il2CppRGCTXDataType)1, 12729 },
	{ (Il2CppRGCTXDataType)3, 48914 },
	{ (Il2CppRGCTXDataType)1, 12733 },
	{ (Il2CppRGCTXDataType)1, 12735 },
	{ (Il2CppRGCTXDataType)2, 12734 },
	{ (Il2CppRGCTXDataType)1, 12736 },
	{ (Il2CppRGCTXDataType)2, 12736 },
	{ (Il2CppRGCTXDataType)3, 48915 },
	{ (Il2CppRGCTXDataType)1, 12757 },
	{ (Il2CppRGCTXDataType)2, 12757 },
	{ (Il2CppRGCTXDataType)2, 12785 },
	{ (Il2CppRGCTXDataType)2, 12786 },
	{ (Il2CppRGCTXDataType)1, 12788 },
	{ (Il2CppRGCTXDataType)3, 48916 },
	{ (Il2CppRGCTXDataType)1, 12789 },
	{ (Il2CppRGCTXDataType)2, 12789 },
	{ (Il2CppRGCTXDataType)2, 27451 },
	{ (Il2CppRGCTXDataType)1, 27451 },
	{ (Il2CppRGCTXDataType)2, 13016 },
	{ (Il2CppRGCTXDataType)3, 48917 },
	{ (Il2CppRGCTXDataType)1, 13016 },
	{ (Il2CppRGCTXDataType)3, 48918 },
	{ (Il2CppRGCTXDataType)3, 48919 },
	{ (Il2CppRGCTXDataType)2, 13017 },
	{ (Il2CppRGCTXDataType)3, 48920 },
	{ (Il2CppRGCTXDataType)1, 27452 },
	{ (Il2CppRGCTXDataType)2, 27452 },
	{ (Il2CppRGCTXDataType)3, 48921 },
	{ (Il2CppRGCTXDataType)3, 48922 },
	{ (Il2CppRGCTXDataType)2, 13022 },
	{ (Il2CppRGCTXDataType)2, 13023 },
	{ (Il2CppRGCTXDataType)3, 48923 },
	{ (Il2CppRGCTXDataType)1, 27453 },
	{ (Il2CppRGCTXDataType)2, 27453 },
	{ (Il2CppRGCTXDataType)3, 48924 },
	{ (Il2CppRGCTXDataType)3, 48925 },
	{ (Il2CppRGCTXDataType)3, 48926 },
	{ (Il2CppRGCTXDataType)2, 13026 },
	{ (Il2CppRGCTXDataType)2, 13027 },
	{ (Il2CppRGCTXDataType)2, 13028 },
	{ (Il2CppRGCTXDataType)3, 48927 },
	{ (Il2CppRGCTXDataType)1, 27454 },
	{ (Il2CppRGCTXDataType)2, 27454 },
	{ (Il2CppRGCTXDataType)3, 48928 },
	{ (Il2CppRGCTXDataType)3, 48929 },
	{ (Il2CppRGCTXDataType)3, 48930 },
	{ (Il2CppRGCTXDataType)3, 48931 },
	{ (Il2CppRGCTXDataType)2, 13033 },
	{ (Il2CppRGCTXDataType)2, 13034 },
	{ (Il2CppRGCTXDataType)2, 13035 },
	{ (Il2CppRGCTXDataType)2, 13036 },
	{ (Il2CppRGCTXDataType)3, 48932 },
	{ (Il2CppRGCTXDataType)3, 48933 },
	{ (Il2CppRGCTXDataType)2, 13038 },
	{ (Il2CppRGCTXDataType)3, 48934 },
	{ (Il2CppRGCTXDataType)3, 48935 },
	{ (Il2CppRGCTXDataType)2, 27455 },
	{ (Il2CppRGCTXDataType)1, 13068 },
	{ (Il2CppRGCTXDataType)2, 27456 },
	{ (Il2CppRGCTXDataType)3, 48936 },
	{ (Il2CppRGCTXDataType)3, 48937 },
	{ (Il2CppRGCTXDataType)3, 48938 },
	{ (Il2CppRGCTXDataType)2, 13068 },
	{ (Il2CppRGCTXDataType)1, 27457 },
	{ (Il2CppRGCTXDataType)1, 27458 },
	{ (Il2CppRGCTXDataType)2, 27459 },
	{ (Il2CppRGCTXDataType)3, 48939 },
	{ (Il2CppRGCTXDataType)1, 13082 },
	{ (Il2CppRGCTXDataType)1, 13083 },
	{ (Il2CppRGCTXDataType)1, 13084 },
	{ (Il2CppRGCTXDataType)2, 27460 },
	{ (Il2CppRGCTXDataType)3, 48940 },
	{ (Il2CppRGCTXDataType)3, 48941 },
	{ (Il2CppRGCTXDataType)2, 13082 },
	{ (Il2CppRGCTXDataType)2, 13083 },
	{ (Il2CppRGCTXDataType)2, 13084 },
	{ (Il2CppRGCTXDataType)1, 27461 },
	{ (Il2CppRGCTXDataType)1, 27462 },
	{ (Il2CppRGCTXDataType)1, 27463 },
	{ (Il2CppRGCTXDataType)1, 27464 },
	{ (Il2CppRGCTXDataType)2, 27465 },
	{ (Il2CppRGCTXDataType)3, 48942 },
	{ (Il2CppRGCTXDataType)1, 27466 },
};
extern const Il2CppCodeGenModule g_UnityEngine_CoreModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_CoreModuleCodeGenModule = 
{
	"UnityEngine.CoreModule.dll",
	2517,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	60,
	s_rgctxIndices,
	165,
	s_rgctxValues,
	NULL,
};
