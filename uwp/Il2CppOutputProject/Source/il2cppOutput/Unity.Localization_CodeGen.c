﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 TCollection UnityEngine.Pool.CollectionPool`2::Get()
// 0x00000002 UnityEngine.Pool.PooledObject`1<TCollection> UnityEngine.Pool.CollectionPool`2::Get(TCollection&)
// 0x00000003 System.Void UnityEngine.Pool.CollectionPool`2::Release(TCollection)
// 0x00000004 System.Void UnityEngine.Pool.CollectionPool`2::.ctor()
// 0x00000005 System.Void UnityEngine.Pool.CollectionPool`2::.cctor()
// 0x00000006 System.Void UnityEngine.Pool.CollectionPool`2_<>c::.cctor()
// 0x00000007 System.Void UnityEngine.Pool.CollectionPool`2_<>c::.ctor()
// 0x00000008 TCollection UnityEngine.Pool.CollectionPool`2_<>c::<.cctor>b__5_0()
// 0x00000009 System.Void UnityEngine.Pool.CollectionPool`2_<>c::<.cctor>b__5_1(TCollection)
// 0x0000000A System.Void UnityEngine.Pool.ListPool`1::.ctor()
// 0x0000000B System.Void UnityEngine.Pool.HashSetPool`1::.ctor()
// 0x0000000C System.Void UnityEngine.Pool.DictionaryPool`2::.ctor()
// 0x0000000D T UnityEngine.Pool.GenericPool`1::Get()
// 0x0000000E UnityEngine.Pool.PooledObject`1<T> UnityEngine.Pool.GenericPool`1::Get(T&)
// 0x0000000F System.Void UnityEngine.Pool.GenericPool`1::Release(T)
// 0x00000010 System.Void UnityEngine.Pool.GenericPool`1::.ctor()
// 0x00000011 System.Void UnityEngine.Pool.GenericPool`1::.cctor()
// 0x00000012 System.Void UnityEngine.Pool.GenericPool`1_<>c::.cctor()
// 0x00000013 System.Void UnityEngine.Pool.GenericPool`1_<>c::.ctor()
// 0x00000014 T UnityEngine.Pool.GenericPool`1_<>c::<.cctor>b__5_0()
// 0x00000015 System.Int32 UnityEngine.Pool.IObjectPool`1::get_CountInactive()
// 0x00000016 T UnityEngine.Pool.IObjectPool`1::Get()
// 0x00000017 UnityEngine.Pool.PooledObject`1<T> UnityEngine.Pool.IObjectPool`1::Get(T&)
// 0x00000018 System.Void UnityEngine.Pool.IObjectPool`1::Release(T)
// 0x00000019 System.Void UnityEngine.Pool.IObjectPool`1::Clear()
// 0x0000001A System.Void UnityEngine.Pool.LinkedPool`1::.ctor(System.Func`1<T>,System.Action`1<T>,System.Action`1<T>,System.Action`1<T>,System.Boolean,System.Int32)
// 0x0000001B System.Int32 UnityEngine.Pool.LinkedPool`1::get_CountInactive()
// 0x0000001C System.Void UnityEngine.Pool.LinkedPool`1::set_CountInactive(System.Int32)
// 0x0000001D T UnityEngine.Pool.LinkedPool`1::Get()
// 0x0000001E UnityEngine.Pool.PooledObject`1<T> UnityEngine.Pool.LinkedPool`1::Get(T&)
// 0x0000001F System.Void UnityEngine.Pool.LinkedPool`1::Release(T)
// 0x00000020 System.Void UnityEngine.Pool.LinkedPool`1::Clear()
// 0x00000021 System.Void UnityEngine.Pool.LinkedPool`1::Dispose()
// 0x00000022 System.Void UnityEngine.Pool.LinkedPool`1_LinkedPoolItem::.ctor()
// 0x00000023 System.Int32 UnityEngine.Pool.ObjectPool`1::get_CountAll()
// 0x00000024 System.Void UnityEngine.Pool.ObjectPool`1::set_CountAll(System.Int32)
// 0x00000025 System.Int32 UnityEngine.Pool.ObjectPool`1::get_CountActive()
// 0x00000026 System.Int32 UnityEngine.Pool.ObjectPool`1::get_CountInactive()
// 0x00000027 System.Void UnityEngine.Pool.ObjectPool`1::.ctor(System.Func`1<T>,System.Action`1<T>,System.Action`1<T>,System.Action`1<T>,System.Boolean,System.Int32,System.Int32)
// 0x00000028 T UnityEngine.Pool.ObjectPool`1::Get()
// 0x00000029 UnityEngine.Pool.PooledObject`1<T> UnityEngine.Pool.ObjectPool`1::Get(T&)
// 0x0000002A System.Void UnityEngine.Pool.ObjectPool`1::Release(T)
// 0x0000002B System.Void UnityEngine.Pool.ObjectPool`1::Clear()
// 0x0000002C System.Void UnityEngine.Pool.ObjectPool`1::Dispose()
// 0x0000002D System.Void UnityEngine.Pool.PooledObject`1::.ctor(T,UnityEngine.Pool.IObjectPool`1<T>)
// 0x0000002E System.Void UnityEngine.Pool.PooledObject`1::System.IDisposable.Dispose()
// 0x0000002F T UnityEngine.Pool.UnsafeGenericPool`1::Get()
// 0x00000030 UnityEngine.Pool.PooledObject`1<T> UnityEngine.Pool.UnsafeGenericPool`1::Get(T&)
// 0x00000031 System.Void UnityEngine.Pool.UnsafeGenericPool`1::Release(T)
// 0x00000032 System.Void UnityEngine.Pool.UnsafeGenericPool`1::.cctor()
// 0x00000033 System.Void UnityEngine.Pool.UnsafeGenericPool`1_<>c::.cctor()
// 0x00000034 System.Void UnityEngine.Pool.UnsafeGenericPool`1_<>c::.ctor()
// 0x00000035 T UnityEngine.Pool.UnsafeGenericPool`1_<>c::<.cctor>b__4_0()
// 0x00000036 System.String UnityEngine.Localization.DisplayNameAttribute::get_Name()
extern void DisplayNameAttribute_get_Name_m15138B442E7C617C8D05D0AF698AEDED5D8F47A9 (void);
// 0x00000037 System.Void UnityEngine.Localization.DisplayNameAttribute::set_Name(System.String)
extern void DisplayNameAttribute_set_Name_m593EC11603CD3596C338FE180936837E50BDA4B5 (void);
// 0x00000038 System.Void UnityEngine.Localization.DisplayNameAttribute::.ctor(System.String)
extern void DisplayNameAttribute__ctor_m5BBA768F930CDEAF9270F2F810E9CC0FF57A1A9C (void);
// 0x00000039 UnityEngine.Localization.LocaleIdentifier UnityEngine.Localization.LocaleIdentifier::get_Undefined()
extern void LocaleIdentifier_get_Undefined_m2208ACB79F98E1DA5C4B1851CCA25FE9FED4F2A0 (void);
// 0x0000003A System.String UnityEngine.Localization.LocaleIdentifier::get_Code()
extern void LocaleIdentifier_get_Code_m4302E99F7C34A1ADDD264A7DA580C09BB02E2E2A_AdjustorThunk (void);
// 0x0000003B System.Globalization.CultureInfo UnityEngine.Localization.LocaleIdentifier::get_CultureInfo()
extern void LocaleIdentifier_get_CultureInfo_m064EB6542663B3FECEB5D3DFF3F69F1FB2DE8B74_AdjustorThunk (void);
// 0x0000003C System.Void UnityEngine.Localization.LocaleIdentifier::.ctor(System.String)
extern void LocaleIdentifier__ctor_m133779944284E1633CFD7C294A589C03441EE514_AdjustorThunk (void);
// 0x0000003D System.Void UnityEngine.Localization.LocaleIdentifier::.ctor(System.Globalization.CultureInfo)
extern void LocaleIdentifier__ctor_m8EEB29CBE081A30BAF0E34B6B3FDAE5FB0F6817D_AdjustorThunk (void);
// 0x0000003E System.Void UnityEngine.Localization.LocaleIdentifier::.ctor(UnityEngine.SystemLanguage)
extern void LocaleIdentifier__ctor_m06E8C439AB7901836EBE39C8FB130290A3F33E85_AdjustorThunk (void);
// 0x0000003F UnityEngine.Localization.LocaleIdentifier UnityEngine.Localization.LocaleIdentifier::op_Implicit(System.String)
extern void LocaleIdentifier_op_Implicit_mACF46E0396F3868C554017858EA6B61BE0FD1012 (void);
// 0x00000040 UnityEngine.Localization.LocaleIdentifier UnityEngine.Localization.LocaleIdentifier::op_Implicit(System.Globalization.CultureInfo)
extern void LocaleIdentifier_op_Implicit_mE5E723265D9ADF34F57D5C71FF126AEB2DE7A972 (void);
// 0x00000041 UnityEngine.Localization.LocaleIdentifier UnityEngine.Localization.LocaleIdentifier::op_Implicit(UnityEngine.SystemLanguage)
extern void LocaleIdentifier_op_Implicit_mD7D242306FFAE932C235D617D7491F70FE9D50EC (void);
// 0x00000042 System.String UnityEngine.Localization.LocaleIdentifier::ToString()
extern void LocaleIdentifier_ToString_m4777A5A8A1559E833430E59E2B983CCAF83F26D5_AdjustorThunk (void);
// 0x00000043 System.Boolean UnityEngine.Localization.LocaleIdentifier::Equals(System.Object)
extern void LocaleIdentifier_Equals_m772A2378D63E76D6C0D1A462BD438AEA47B9AB4D_AdjustorThunk (void);
// 0x00000044 System.Boolean UnityEngine.Localization.LocaleIdentifier::Equals(UnityEngine.Localization.LocaleIdentifier)
extern void LocaleIdentifier_Equals_mFAE33C4F7EF2977EF21236931AA9DE6EAB15674D_AdjustorThunk (void);
// 0x00000045 System.Int32 UnityEngine.Localization.LocaleIdentifier::GetHashCode()
extern void LocaleIdentifier_GetHashCode_m0D9EC66EA2C2E3D25FD9122AA253E8ED2562D0D3_AdjustorThunk (void);
// 0x00000046 System.Int32 UnityEngine.Localization.LocaleIdentifier::CompareTo(UnityEngine.Localization.LocaleIdentifier)
extern void LocaleIdentifier_CompareTo_m718641E84134909C9379D717D4A21E867CDAFAC5_AdjustorThunk (void);
// 0x00000047 System.Boolean UnityEngine.Localization.LocaleIdentifier::op_Equality(UnityEngine.Localization.LocaleIdentifier,UnityEngine.Localization.LocaleIdentifier)
extern void LocaleIdentifier_op_Equality_m5AF71311A605106A2526B34E019BF34A6FC9CE8C (void);
// 0x00000048 System.Boolean UnityEngine.Localization.LocaleIdentifier::op_Inequality(UnityEngine.Localization.LocaleIdentifier,UnityEngine.Localization.LocaleIdentifier)
extern void LocaleIdentifier_op_Inequality_m083C0B1D213044E421E440BB598ACA82749095D6 (void);
// 0x00000049 UnityEngine.Localization.LocaleIdentifier UnityEngine.Localization.Locale::get_Identifier()
extern void Locale_get_Identifier_m621ADAB745B2B24ACACF4669A48DAC9E758AB3EA (void);
// 0x0000004A System.Void UnityEngine.Localization.Locale::set_Identifier(UnityEngine.Localization.LocaleIdentifier)
extern void Locale_set_Identifier_mA12CD7F8517E5C38ACC3D4F7289C9C77E445AEF3 (void);
// 0x0000004B UnityEngine.Localization.Metadata.MetadataCollection UnityEngine.Localization.Locale::get_Metadata()
extern void Locale_get_Metadata_m3DFE5F7112222CC8319EC90676E3F908DF541F89 (void);
// 0x0000004C System.Void UnityEngine.Localization.Locale::set_Metadata(UnityEngine.Localization.Metadata.MetadataCollection)
extern void Locale_set_Metadata_mA45EA3CD237AA40C0CAD6C0C859B0D53CC058D51 (void);
// 0x0000004D System.UInt16 UnityEngine.Localization.Locale::get_SortOrder()
extern void Locale_get_SortOrder_m9DBCC0285EF825609CE3A2E1DD97CEAEF7510E74 (void);
// 0x0000004E System.Void UnityEngine.Localization.Locale::set_SortOrder(System.UInt16)
extern void Locale_set_SortOrder_m1352804A3B0D817ECFA6266D18273549F5894D24 (void);
// 0x0000004F UnityEngine.Localization.Locale UnityEngine.Localization.Locale::CreateLocale(System.String)
extern void Locale_CreateLocale_m11558328BB212CDAB2CC1757FE08358B9D5F2A4F (void);
// 0x00000050 UnityEngine.Localization.Locale UnityEngine.Localization.Locale::CreateLocale(UnityEngine.Localization.LocaleIdentifier)
extern void Locale_CreateLocale_m859C02E2B909189755BE43640DB5B2E7CD3BDB4D (void);
// 0x00000051 UnityEngine.Localization.Locale UnityEngine.Localization.Locale::CreateLocale(UnityEngine.SystemLanguage)
extern void Locale_CreateLocale_m1BF652AEA4013DD2217867A85C42FFC6BA2EADE0 (void);
// 0x00000052 UnityEngine.Localization.Locale UnityEngine.Localization.Locale::CreateLocale(System.Globalization.CultureInfo)
extern void Locale_CreateLocale_m90CD02627809328FE140CFF9B65A402979BDA90B (void);
// 0x00000053 System.Int32 UnityEngine.Localization.Locale::CompareTo(UnityEngine.Localization.Locale)
extern void Locale_CompareTo_mDE4462D2FA0D6D2E40C551B977384A15B33B95D5 (void);
// 0x00000054 System.String UnityEngine.Localization.Locale::ToString()
extern void Locale_ToString_m6084FB667B4653E812CD590488C1065710A82573 (void);
// 0x00000055 System.Void UnityEngine.Localization.Locale::.ctor()
extern void Locale__ctor_m08E92E232D53B82694B92821FF780FD5E5035AF8 (void);
// 0x00000056 System.Nullable`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>> UnityEngine.Localization.LocalizedAsset`1::get_CurrentLoadingOperation()
// 0x00000057 System.Void UnityEngine.Localization.LocalizedAsset`1::set_CurrentLoadingOperation(System.Nullable`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>>)
// 0x00000058 System.Void UnityEngine.Localization.LocalizedAsset`1::add_AssetChanged(UnityEngine.Localization.LocalizedAsset`1_ChangeHandler<TObject>)
// 0x00000059 System.Void UnityEngine.Localization.LocalizedAsset`1::remove_AssetChanged(UnityEngine.Localization.LocalizedAsset`1_ChangeHandler<TObject>)
// 0x0000005A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.Localization.LocalizedAsset`1::LoadAssetAsync()
// 0x0000005B System.Void UnityEngine.Localization.LocalizedAsset`1::ForceUpdate()
// 0x0000005C System.Void UnityEngine.Localization.LocalizedAsset`1::HandleLocaleChange(UnityEngine.Localization.Locale)
// 0x0000005D System.Void UnityEngine.Localization.LocalizedAsset`1::AutomaticLoadingCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject>)
// 0x0000005E System.Void UnityEngine.Localization.LocalizedAsset`1::ClearLoadingOperation()
// 0x0000005F System.Void UnityEngine.Localization.LocalizedAsset`1::.ctor()
// 0x00000060 System.Void UnityEngine.Localization.LocalizedAsset`1_ChangeHandler::.ctor(System.Object,System.IntPtr)
// 0x00000061 System.Void UnityEngine.Localization.LocalizedAsset`1_ChangeHandler::Invoke(TObject)
// 0x00000062 System.IAsyncResult UnityEngine.Localization.LocalizedAsset`1_ChangeHandler::BeginInvoke(TObject,System.AsyncCallback,System.Object)
// 0x00000063 System.Void UnityEngine.Localization.LocalizedAsset`1_ChangeHandler::EndInvoke(System.IAsyncResult)
// 0x00000064 System.Void UnityEngine.Localization.LocalizedTexture::.ctor()
extern void LocalizedTexture__ctor_mB4718AC9BCDD3165897BBBFC9ABFB32912F51F0F (void);
// 0x00000065 System.Void UnityEngine.Localization.LocalizedAudioClip::.ctor()
extern void LocalizedAudioClip__ctor_mD104267A86F2DD9F92CD2FE1FDC04D8747EAE2CD (void);
// 0x00000066 System.Void UnityEngine.Localization.LocalizedSprite::.ctor()
extern void LocalizedSprite__ctor_m918EF089D8B07E286413824686FC3AE8ED04BEC0 (void);
// 0x00000067 System.Void UnityEngine.Localization.LocalizedGameObject::.ctor()
extern void LocalizedGameObject__ctor_mF3545FDED382B92B95AF6518898EC20E2F4A5518 (void);
// 0x00000068 UnityEngine.Localization.Settings.LocalizedDatabase`2<UnityEngine.Localization.Tables.AssetTable,UnityEngine.Localization.Tables.AssetTableEntry> UnityEngine.Localization.LocalizedAssetTable::get_Database()
extern void LocalizedAssetTable_get_Database_m4063B35D2B609296DBDF90AE0B4B49279620EDFB (void);
// 0x00000069 System.Void UnityEngine.Localization.LocalizedAssetTable::.ctor()
extern void LocalizedAssetTable__ctor_m8927284826B572D2C9E6D4AD6F7AA047A80346F6 (void);
// 0x0000006A UnityEngine.Localization.Tables.TableReference UnityEngine.Localization.LocalizedReference::get_TableReference()
extern void LocalizedReference_get_TableReference_mE587514AE3DE00E9F12B4E90009A6074A6642C60 (void);
// 0x0000006B System.Void UnityEngine.Localization.LocalizedReference::set_TableReference(UnityEngine.Localization.Tables.TableReference)
extern void LocalizedReference_set_TableReference_m4121FD7C826258D8C0141707B9CC63438475F886 (void);
// 0x0000006C UnityEngine.Localization.Tables.TableEntryReference UnityEngine.Localization.LocalizedReference::get_TableEntryReference()
extern void LocalizedReference_get_TableEntryReference_mC33B5D7329493701DB64EBD92CE705E78826F1F3 (void);
// 0x0000006D System.Void UnityEngine.Localization.LocalizedReference::set_TableEntryReference(UnityEngine.Localization.Tables.TableEntryReference)
extern void LocalizedReference_set_TableEntryReference_mB5C076BA456602F208CA7D0285293268C531843D (void);
// 0x0000006E System.Boolean UnityEngine.Localization.LocalizedReference::get_IsEmpty()
extern void LocalizedReference_get_IsEmpty_m9737303300623BF8F75719431208023CAC2418E7 (void);
// 0x0000006F System.Void UnityEngine.Localization.LocalizedReference::SetReference(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Tables.TableEntryReference)
extern void LocalizedReference_SetReference_mC33A39926E3D30985B4368573BD740E17B18741A (void);
// 0x00000070 System.String UnityEngine.Localization.LocalizedReference::ToString()
extern void LocalizedReference_ToString_m1F3DB906BE8C93140C50299C9D71C9FDC617152E (void);
// 0x00000071 System.Void UnityEngine.Localization.LocalizedReference::ForceUpdate()
// 0x00000072 System.Void UnityEngine.Localization.LocalizedReference::.ctor()
extern void LocalizedReference__ctor_m4E1E6D7670335A7948EBBCDCD995EE52D87DB999 (void);
// 0x00000073 System.Object[] UnityEngine.Localization.LocalizedString::get_Arguments()
extern void LocalizedString_get_Arguments_m5D8AE307C9A07D632B4077B8BDCAF02226EFEDF4 (void);
// 0x00000074 System.Void UnityEngine.Localization.LocalizedString::set_Arguments(System.Object[])
extern void LocalizedString_set_Arguments_m70FE61A7F71BDE5ED1E9161A24E176EE52ACEBCA (void);
// 0x00000075 System.Nullable`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult<UnityEngine.Localization.Tables.StringTable,UnityEngine.Localization.Tables.StringTableEntry>>> UnityEngine.Localization.LocalizedString::get_CurrentLoadingOperation()
extern void LocalizedString_get_CurrentLoadingOperation_mC245D181AFD5D1232BC24CD617C1CA0BDF29C7F6 (void);
// 0x00000076 System.Void UnityEngine.Localization.LocalizedString::set_CurrentLoadingOperation(System.Nullable`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult<UnityEngine.Localization.Tables.StringTable,UnityEngine.Localization.Tables.StringTableEntry>>>)
extern void LocalizedString_set_CurrentLoadingOperation_mFEF77C33E384E40E2B57971A938B0FDFF31707B1 (void);
// 0x00000077 System.Void UnityEngine.Localization.LocalizedString::add_StringChanged(UnityEngine.Localization.LocalizedString_ChangeHandler)
extern void LocalizedString_add_StringChanged_m0DA9B1CCE4A83CE494847522BBD21FB1A3BADA35 (void);
// 0x00000078 System.Void UnityEngine.Localization.LocalizedString::remove_StringChanged(UnityEngine.Localization.LocalizedString_ChangeHandler)
extern void LocalizedString_remove_StringChanged_m570195355B80479599CF66080BA5F7071CAF4DC0 (void);
// 0x00000079 System.Boolean UnityEngine.Localization.LocalizedString::RefreshString()
extern void LocalizedString_RefreshString_mDBB0844A41B8159A7897A917BE26A71F5A6699D1 (void);
// 0x0000007A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.LocalizedString::GetLocalizedString()
extern void LocalizedString_GetLocalizedString_mA71DFB44AFD003E5B72B746EEF4B5D1D7B88E3DE (void);
// 0x0000007B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.LocalizedString::GetLocalizedString(System.Object[])
extern void LocalizedString_GetLocalizedString_mC1ED8AFDB5B2A90D07C7F701C083A59FCA9689E6 (void);
// 0x0000007C System.Void UnityEngine.Localization.LocalizedString::ForceUpdate()
extern void LocalizedString_ForceUpdate_mAA87ACD7E50FB54A75FD84F05395EB42194C6764 (void);
// 0x0000007D System.Void UnityEngine.Localization.LocalizedString::HandleLocaleChange(UnityEngine.Localization.Locale)
extern void LocalizedString_HandleLocaleChange_m5A6539BC984F00526E93C858CC4BF19607B01C47 (void);
// 0x0000007E System.Void UnityEngine.Localization.LocalizedString::AutomaticLoadingCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult<UnityEngine.Localization.Tables.StringTable,UnityEngine.Localization.Tables.StringTableEntry>>)
extern void LocalizedString_AutomaticLoadingCompleted_mA23404E5D8AE555CFB19DB84A879828C3C1044A9 (void);
// 0x0000007F System.Void UnityEngine.Localization.LocalizedString::ClearLoadingOperation()
extern void LocalizedString_ClearLoadingOperation_m8556F2CAEE9B717A0BAF86B10D589B8D7CD01073 (void);
// 0x00000080 System.Void UnityEngine.Localization.LocalizedString::.ctor()
extern void LocalizedString__ctor_m21B176C32FF2D6A39CA3B90261E16C1BB31936FB (void);
// 0x00000081 System.Void UnityEngine.Localization.LocalizedString_ChangeHandler::.ctor(System.Object,System.IntPtr)
extern void ChangeHandler__ctor_m52682E05372D1D677D6327024A11BB8A05BD579E (void);
// 0x00000082 System.Void UnityEngine.Localization.LocalizedString_ChangeHandler::Invoke(System.String)
extern void ChangeHandler_Invoke_mF01B7475C92D862E30311036F50B395EB5CEADD6 (void);
// 0x00000083 System.IAsyncResult UnityEngine.Localization.LocalizedString_ChangeHandler::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ChangeHandler_BeginInvoke_m03B8103956FC241ACEE321DB3B9C5545445890FD (void);
// 0x00000084 System.Void UnityEngine.Localization.LocalizedString_ChangeHandler::EndInvoke(System.IAsyncResult)
extern void ChangeHandler_EndInvoke_mE29324A74F064F13B4489F3D1B132137E661E451 (void);
// 0x00000085 UnityEngine.Localization.Settings.LocalizedDatabase`2<UnityEngine.Localization.Tables.StringTable,UnityEngine.Localization.Tables.StringTableEntry> UnityEngine.Localization.LocalizedStringTable::get_Database()
extern void LocalizedStringTable_get_Database_m390D6D275E96A2A2E079581E97129CA6E052DB24 (void);
// 0x00000086 System.Void UnityEngine.Localization.LocalizedStringTable::.ctor()
extern void LocalizedStringTable__ctor_mB212473ACBD44AB4CFF150162DB03F87AA70E33F (void);
// 0x00000087 UnityEngine.Localization.Settings.LocalizedDatabase`2<TTable,TEntry> UnityEngine.Localization.LocalizedTable`2::get_Database()
// 0x00000088 System.Nullable`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable>> UnityEngine.Localization.LocalizedTable`2::get_CurrentLoadingOperation()
// 0x00000089 System.Void UnityEngine.Localization.LocalizedTable`2::set_CurrentLoadingOperation(System.Nullable`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable>>)
// 0x0000008A UnityEngine.Localization.Tables.TableReference UnityEngine.Localization.LocalizedTable`2::get_TableReference()
// 0x0000008B System.Void UnityEngine.Localization.LocalizedTable`2::set_TableReference(UnityEngine.Localization.Tables.TableReference)
// 0x0000008C System.Boolean UnityEngine.Localization.LocalizedTable`2::get_IsEmpty()
// 0x0000008D System.Void UnityEngine.Localization.LocalizedTable`2::add_TableChanged(UnityEngine.Localization.LocalizedTable`2_ChangeHandler<TTable,TEntry>)
// 0x0000008E System.Void UnityEngine.Localization.LocalizedTable`2::remove_TableChanged(UnityEngine.Localization.LocalizedTable`2_ChangeHandler<TTable,TEntry>)
// 0x0000008F System.Void UnityEngine.Localization.LocalizedTable`2::ForceUpdate()
// 0x00000090 System.Void UnityEngine.Localization.LocalizedTable`2::HandleLocaleChange(UnityEngine.Localization.Locale)
// 0x00000091 System.Void UnityEngine.Localization.LocalizedTable`2::AutomaticLoadingCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable>)
// 0x00000092 System.Void UnityEngine.Localization.LocalizedTable`2::ClearLoadingOperation()
// 0x00000093 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable> UnityEngine.Localization.LocalizedTable`2::GetTable()
// 0x00000094 System.String UnityEngine.Localization.LocalizedTable`2::ToString()
// 0x00000095 System.Void UnityEngine.Localization.LocalizedTable`2::.ctor()
// 0x00000096 System.Void UnityEngine.Localization.LocalizedTable`2_ChangeHandler::.ctor(System.Object,System.IntPtr)
// 0x00000097 System.Void UnityEngine.Localization.LocalizedTable`2_ChangeHandler::Invoke(TTable)
// 0x00000098 System.IAsyncResult UnityEngine.Localization.LocalizedTable`2_ChangeHandler::BeginInvoke(TTable,System.AsyncCallback,System.Object)
// 0x00000099 System.Void UnityEngine.Localization.LocalizedTable`2_ChangeHandler::EndInvoke(System.IAsyncResult)
// 0x0000009A System.Single UnityEngine.Localization.GroupIAsyncOperation::get_Progress()
extern void GroupIAsyncOperation_get_Progress_mBC0CB9E70D0D8369123537AEE5012871DFD41940 (void);
// 0x0000009B System.String UnityEngine.Localization.GroupIAsyncOperation::get_DebugName()
extern void GroupIAsyncOperation_get_DebugName_m065B6C693663E962FABECF38D7798AC92C9920AE (void);
// 0x0000009C System.Void UnityEngine.Localization.GroupIAsyncOperation::Init(System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void GroupIAsyncOperation_Init_mF5463DE0B23BD1B0EF7ECEF284BD6A50C84BB5D2 (void);
// 0x0000009D System.Void UnityEngine.Localization.GroupIAsyncOperation::Execute()
extern void GroupIAsyncOperation_Execute_mE6B2C97C7C637E1131F195A413432EED4E533851 (void);
// 0x0000009E System.Void UnityEngine.Localization.GroupIAsyncOperation::OnOperationCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void GroupIAsyncOperation_OnOperationCompleted_m52DD759DB8FBC1700AEE3D91346E3BD8ACC81324 (void);
// 0x0000009F System.Void UnityEngine.Localization.GroupIAsyncOperation::.ctor()
extern void GroupIAsyncOperation__ctor_m54F64AB87CDB3357820B8DA7FCCE04FCE7D441CE (void);
// 0x000000A0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.Localization.IPreloadRequired::get_PreloadOperation()
// 0x000000A1 System.Single UnityEngine.Localization.InitializationOperation::get_Progress()
extern void InitializationOperation_get_Progress_mD8335D02D2D22AB3331543ACF9D6386EF3652F54 (void);
// 0x000000A2 System.String UnityEngine.Localization.InitializationOperation::get_DebugName()
extern void InitializationOperation_get_DebugName_mD96F3628DCEE3A95A95EFE7A12ECB4A7A9C5A521 (void);
// 0x000000A3 System.Void UnityEngine.Localization.InitializationOperation::Init(UnityEngine.Localization.Settings.LocalizationSettings)
extern void InitializationOperation_Init_m7D9BF5D85A90FEF273BBD704D3012BA055898E44 (void);
// 0x000000A4 System.Void UnityEngine.Localization.InitializationOperation::Execute()
extern void InitializationOperation_Execute_mB7335F071F581FB53547B85D70972865E68E3093 (void);
// 0x000000A5 System.Void UnityEngine.Localization.InitializationOperation::LoadLocales()
extern void InitializationOperation_LoadLocales_m68DAD612D9B005E6523AA0DC650C71142EAD6964 (void);
// 0x000000A6 System.Void UnityEngine.Localization.InitializationOperation::PreloadOperationCompleted(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void InitializationOperation_PreloadOperationCompleted_m6999909CCD9F6ED2D2E6A7E38498B21732405DF1 (void);
// 0x000000A7 System.Void UnityEngine.Localization.InitializationOperation::PreLoadTables()
extern void InitializationOperation_PreLoadTables_mD8AC8126732DD915521237C29A57F11E22A3261F (void);
// 0x000000A8 System.Void UnityEngine.Localization.InitializationOperation::PostInitializeExtensions()
extern void InitializationOperation_PostInitializeExtensions_m229ECD97C8074AC58B5EFE63B6378482CBD6F227 (void);
// 0x000000A9 System.Void UnityEngine.Localization.InitializationOperation::FinishInitializing()
extern void InitializationOperation_FinishInitializing_m429D7963BD9C333D12481D4415503EA1016A205A (void);
// 0x000000AA System.Void UnityEngine.Localization.InitializationOperation::.ctor()
extern void InitializationOperation__ctor_m51E0D99162D446BC725BC030A666BCE9FB823B7F (void);
// 0x000000AB System.Void UnityEngine.Localization.InitializationOperation::<LoadLocales>b__13_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Locale>)
extern void InitializationOperation_U3CLoadLocalesU3Eb__13_0_m7E4808B00790B7416430832D17E1B5FE9A5A3AC1 (void);
// 0x000000AC System.Single UnityEngine.Localization.PreloadDatabaseOperation`2::get_Progress()
// 0x000000AD System.String UnityEngine.Localization.PreloadDatabaseOperation`2::get_DebugName()
// 0x000000AE System.Void UnityEngine.Localization.PreloadDatabaseOperation`2::Init(UnityEngine.Localization.Settings.LocalizedDatabase`2<TTable,TEntry>)
// 0x000000AF System.Void UnityEngine.Localization.PreloadDatabaseOperation`2::Execute()
// 0x000000B0 System.Void UnityEngine.Localization.PreloadDatabaseOperation`2::BeginPreloading()
// 0x000000B1 System.Void UnityEngine.Localization.PreloadDatabaseOperation`2::LoadTables(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<UnityEngine.ResourceManagement.ResourceLocations.IResourceLocation>>)
// 0x000000B2 System.Void UnityEngine.Localization.PreloadDatabaseOperation`2::TableLoaded(TTable)
// 0x000000B3 System.Void UnityEngine.Localization.PreloadDatabaseOperation`2::LoadTableContents(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.Collections.Generic.IList`1<TTable>>)
// 0x000000B4 System.Void UnityEngine.Localization.PreloadDatabaseOperation`2::FinishPreloading(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x000000B5 System.Void UnityEngine.Localization.PreloadDatabaseOperation`2::.ctor()
// 0x000000B6 System.String UnityEngine.Localization.AddressHelper::GetTableAddress(System.String,UnityEngine.Localization.LocaleIdentifier)
extern void AddressHelper_GetTableAddress_m6995DFCA1E7BBE707F62A8A6B48A15960A7F6C78 (void);
// 0x000000B7 System.String UnityEngine.Localization.AddressHelper::FormatAssetLabel(UnityEngine.Localization.LocaleIdentifier)
extern void AddressHelper_FormatAssetLabel_mD4A0B244D7A38ED77380897886AF17F9FB74E3BE (void);
// 0x000000B8 System.Boolean UnityEngine.Localization.AddressHelper::IsLocaleLabel(System.String)
extern void AddressHelper_IsLocaleLabel_mFFE8C02E6E0899EF96D3AE1EB8224310E6FE410F (void);
// 0x000000B9 UnityEngine.Localization.LocaleIdentifier UnityEngine.Localization.AddressHelper::LocaleLabelToId(System.String)
extern void AddressHelper_LocaleLabelToId_m711FC0B20C7526DC23A0B72A5AE7448C6703707F (void);
// 0x000000BA System.Void UnityEngine.Localization.AddressHelper::.ctor()
extern void AddressHelper__ctor_m35327DEB9B838CAAAAA442CC6F9526831FD33880 (void);
// 0x000000BB System.Text.StringBuilder UnityEngine.Localization.StringBuilderPool::Get()
extern void StringBuilderPool_Get_m0D5F95875AEDA814DA170CBE157905951DC6657B (void);
// 0x000000BC UnityEngine.Pool.PooledObject`1<System.Text.StringBuilder> UnityEngine.Localization.StringBuilderPool::Get(System.Text.StringBuilder&)
extern void StringBuilderPool_Get_mFD003EB5F95D0B9957A75788475FBDCCD85D8219 (void);
// 0x000000BD System.Void UnityEngine.Localization.StringBuilderPool::Release(System.Text.StringBuilder)
extern void StringBuilderPool_Release_m0CDE332654CE07026EA81B1D70A477B63DC8C5B9 (void);
// 0x000000BE System.Void UnityEngine.Localization.StringBuilderPool::.cctor()
extern void StringBuilderPool__cctor_m163BA084739B135A4B1C01983C018CA1F72B5824 (void);
// 0x000000BF System.Void UnityEngine.Localization.StringBuilderPool_<>c::.cctor()
extern void U3CU3Ec__cctor_m6B8A9ED223680D90E33945EA982AD188A3AA8AF7 (void);
// 0x000000C0 System.Void UnityEngine.Localization.StringBuilderPool_<>c::.ctor()
extern void U3CU3Ec__ctor_mFFC3C3C75857C24CD50789E394CB5C77D4397F13 (void);
// 0x000000C1 System.Text.StringBuilder UnityEngine.Localization.StringBuilderPool_<>c::<.cctor>b__4_0()
extern void U3CU3Ec_U3C_cctorU3Eb__4_0_m4A2E997621A35BF6D0F1B62BC363B9279072DF36 (void);
// 0x000000C2 System.Void UnityEngine.Localization.StringBuilderPool_<>c::<.cctor>b__4_1(System.Text.StringBuilder)
extern void U3CU3Ec_U3C_cctorU3Eb__4_1_m77890050B39F0C75314B5AFF1C4BD4D906E44E7F (void);
// 0x000000C3 System.String UnityEngine.Localization.SystemLanguageConverter::GetSystemLanguageCultureCode(UnityEngine.SystemLanguage)
extern void SystemLanguageConverter_GetSystemLanguageCultureCode_m696186E1DD47470FC99807AE900A7039A43CD12F (void);
// 0x000000C4 System.Nullable`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle> UnityEngine.Localization.Tables.AssetTableEntry::get_AsyncOperation()
extern void AssetTableEntry_get_AsyncOperation_mB5313EA52EE7F354BDCC53A0D46BDCBD88D149EC (void);
// 0x000000C5 System.Void UnityEngine.Localization.Tables.AssetTableEntry::set_AsyncOperation(System.Nullable`1<UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle>)
extern void AssetTableEntry_set_AsyncOperation_mF68A581A9C5D3821E1318C513FA7F0CF119C3CB6 (void);
// 0x000000C6 System.String UnityEngine.Localization.Tables.AssetTableEntry::get_Guid()
extern void AssetTableEntry_get_Guid_mFDB173E3FBBC0CB84CBE940598502251383B4F84 (void);
// 0x000000C7 System.Void UnityEngine.Localization.Tables.AssetTableEntry::set_Guid(System.String)
extern void AssetTableEntry_set_Guid_m414084741BA6710F418BEAB3C087586DE6466402 (void);
// 0x000000C8 System.Boolean UnityEngine.Localization.Tables.AssetTableEntry::get_IsEmpty()
extern void AssetTableEntry_get_IsEmpty_m57FF2869BCFF5CB121C90C1C831421E10B2974B5 (void);
// 0x000000C9 System.Void UnityEngine.Localization.Tables.AssetTableEntry::.ctor()
extern void AssetTableEntry__ctor_m74202A9165B46C9F09A848DB223769C00E1FD24A (void);
// 0x000000CA System.Void UnityEngine.Localization.Tables.AssetTableEntry::RemoveFromTable()
extern void AssetTableEntry_RemoveFromTable_m871C27047D6B989D9D2F9B2D77A8C6F272C3AFD8 (void);
// 0x000000CB UnityEngine.ResourceManagement.ResourceManager UnityEngine.Localization.Tables.AssetTable::get_ResourceManager()
extern void AssetTable_get_ResourceManager_m25F6142AA72A2503113F3C19664EE32218744C6E (void);
// 0x000000CC UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.Localization.Tables.AssetTable::get_PreloadOperation()
extern void AssetTable_get_PreloadOperation_mA136C648833B646014DF02BF60892D6E822AB140 (void);
// 0x000000CD UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.Localization.Tables.AssetTable::PreloadAssets()
extern void AssetTable_PreloadAssets_m8098674AEB07FDC7FCE1BADF9C02F1658B250387 (void);
// 0x000000CE UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.Localization.Tables.AssetTable::GetAssetAsync(System.Int64)
// 0x000000CF UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.Localization.Tables.AssetTable::GetAssetAsync(UnityEngine.Localization.Tables.AssetTableEntry)
// 0x000000D0 UnityEngine.Localization.Tables.AssetTableEntry UnityEngine.Localization.Tables.AssetTable::CreateTableEntry()
extern void AssetTable_CreateTableEntry_m923BF5153B8D40E4CCFEE693E9932B0745FF1459 (void);
// 0x000000D1 System.Void UnityEngine.Localization.Tables.AssetTable::.ctor()
extern void AssetTable__ctor_m8CE2C3CDC50BC350F2278DC50AEB3162C6E33B9A (void);
// 0x000000D2 UnityEngine.Localization.Tables.LocalizationTable UnityEngine.Localization.Tables.TableEntry::get_Table()
extern void TableEntry_get_Table_m198F8C944F31A183AAD2BBEB2DCC4F45AC656750 (void);
// 0x000000D3 System.Void UnityEngine.Localization.Tables.TableEntry::set_Table(UnityEngine.Localization.Tables.LocalizationTable)
extern void TableEntry_set_Table_mA3C74E9E65E0340D0E4F04649312D8DA2628863B (void);
// 0x000000D4 UnityEngine.Localization.Tables.TableEntryData UnityEngine.Localization.Tables.TableEntry::get_Data()
extern void TableEntry_get_Data_mDD18E697BFBC3D3608D9D1CC4D882F3539D20FBF (void);
// 0x000000D5 System.Void UnityEngine.Localization.Tables.TableEntry::set_Data(UnityEngine.Localization.Tables.TableEntryData)
extern void TableEntry_set_Data_mB540E511BE653FE5AA69C2DF3F806B3077DBCC0D (void);
// 0x000000D6 System.Int64 UnityEngine.Localization.Tables.TableEntry::get_KeyId()
extern void TableEntry_get_KeyId_mDDCE6AA8B6F2AA70E542B77868575B4E298022BF (void);
// 0x000000D7 System.String UnityEngine.Localization.Tables.TableEntry::get_LocalizedValue()
extern void TableEntry_get_LocalizedValue_m039200CA573ABEF47C455C385969EF2581FB0148 (void);
// 0x000000D8 System.Collections.Generic.IList`1<UnityEngine.Localization.Metadata.IMetadata> UnityEngine.Localization.Tables.TableEntry::get_MetadataEntries()
extern void TableEntry_get_MetadataEntries_m16E4EEA6A46A4B408B8774F87472BD1E33D23C2D (void);
// 0x000000D9 TObject UnityEngine.Localization.Tables.TableEntry::GetMetadata()
// 0x000000DA System.Void UnityEngine.Localization.Tables.TableEntry::GetMetadatas(System.Collections.Generic.IList`1<TObject>)
// 0x000000DB System.Collections.Generic.IList`1<TObject> UnityEngine.Localization.Tables.TableEntry::GetMetadatas()
// 0x000000DC System.Void UnityEngine.Localization.Tables.TableEntry::AddTagMetadata()
// 0x000000DD System.Void UnityEngine.Localization.Tables.TableEntry::AddSharedMetadata(UnityEngine.Localization.Metadata.SharedTableEntryMetadata)
extern void TableEntry_AddSharedMetadata_mBEA5219FE5DD930C4FD73C79502277DA81E7D96D (void);
// 0x000000DE System.Void UnityEngine.Localization.Tables.TableEntry::AddSharedMetadata(UnityEngine.Localization.Metadata.SharedTableCollectionMetadata)
extern void TableEntry_AddSharedMetadata_m4BB0F6A34F151BEEA64253EF4E2A47884E2F8660 (void);
// 0x000000DF System.Void UnityEngine.Localization.Tables.TableEntry::AddMetadata(UnityEngine.Localization.Metadata.IMetadata)
extern void TableEntry_AddMetadata_m6EC5448F2D0C020F6E7625AE37425260DC300DF1 (void);
// 0x000000E0 System.Void UnityEngine.Localization.Tables.TableEntry::RemoveTagMetadata()
// 0x000000E1 System.Void UnityEngine.Localization.Tables.TableEntry::RemoveSharedMetadata(UnityEngine.Localization.Metadata.SharedTableEntryMetadata)
extern void TableEntry_RemoveSharedMetadata_m07887DC53F85E5327641384D3F0CF4BE719BEC8B (void);
// 0x000000E2 System.Void UnityEngine.Localization.Tables.TableEntry::RemoveSharedMetadata(UnityEngine.Localization.Metadata.SharedTableCollectionMetadata)
extern void TableEntry_RemoveSharedMetadata_m3C597B1E6A1C685095C3EEB2DABCB857C856EB6F (void);
// 0x000000E3 System.Boolean UnityEngine.Localization.Tables.TableEntry::RemoveMetadata(UnityEngine.Localization.Metadata.IMetadata)
extern void TableEntry_RemoveMetadata_m9B12597714B0448BF24C7C78BE12E63A72378932 (void);
// 0x000000E4 System.Boolean UnityEngine.Localization.Tables.TableEntry::Contains(UnityEngine.Localization.Metadata.IMetadata)
extern void TableEntry_Contains_m4DD8C03BA75FA4FE438E2EAA0FBA285C91DEFC94 (void);
// 0x000000E5 System.String UnityEngine.Localization.Tables.TableEntry::ToString()
extern void TableEntry_ToString_m256995777339AA3245C629DE0658277F4F34FB51 (void);
// 0x000000E6 System.Void UnityEngine.Localization.Tables.TableEntry::.ctor()
extern void TableEntry__ctor_m67FB3D0B850C4A50C72207411E73E83DBF2C2F62 (void);
// 0x000000E7 System.Collections.Generic.ICollection`1<System.Int64> UnityEngine.Localization.Tables.DetailedLocalizationTable`1::System.Collections.Generic.IDictionary<System.Int64,TEntry>.get_Keys()
// 0x000000E8 System.Collections.Generic.ICollection`1<TEntry> UnityEngine.Localization.Tables.DetailedLocalizationTable`1::get_Values()
// 0x000000E9 System.Int32 UnityEngine.Localization.Tables.DetailedLocalizationTable`1::get_Count()
// 0x000000EA System.Boolean UnityEngine.Localization.Tables.DetailedLocalizationTable`1::get_IsReadOnly()
// 0x000000EB TEntry UnityEngine.Localization.Tables.DetailedLocalizationTable`1::get_Item(System.Int64)
// 0x000000EC System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1::set_Item(System.Int64,TEntry)
// 0x000000ED TEntry UnityEngine.Localization.Tables.DetailedLocalizationTable`1::get_Item(System.String)
// 0x000000EE System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1::set_Item(System.String,TEntry)
// 0x000000EF TEntry UnityEngine.Localization.Tables.DetailedLocalizationTable`1::CreateTableEntry()
// 0x000000F0 TEntry UnityEngine.Localization.Tables.DetailedLocalizationTable`1::CreateTableEntry(UnityEngine.Localization.Tables.TableEntryData)
// 0x000000F1 System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1::CreateEmpty(UnityEngine.Localization.Tables.TableEntryReference)
// 0x000000F2 TEntry UnityEngine.Localization.Tables.DetailedLocalizationTable`1::AddEntry(System.String,System.String)
// 0x000000F3 TEntry UnityEngine.Localization.Tables.DetailedLocalizationTable`1::AddEntry(System.Int64,System.String)
// 0x000000F4 TEntry UnityEngine.Localization.Tables.DetailedLocalizationTable`1::AddEntryFromReference(UnityEngine.Localization.Tables.TableEntryReference,System.String)
// 0x000000F5 System.Boolean UnityEngine.Localization.Tables.DetailedLocalizationTable`1::RemoveEntry(System.String)
// 0x000000F6 System.Boolean UnityEngine.Localization.Tables.DetailedLocalizationTable`1::RemoveEntry(System.Int64)
// 0x000000F7 TEntry UnityEngine.Localization.Tables.DetailedLocalizationTable`1::GetEntryFromReference(UnityEngine.Localization.Tables.TableEntryReference)
// 0x000000F8 TEntry UnityEngine.Localization.Tables.DetailedLocalizationTable`1::GetEntry(System.String)
// 0x000000F9 TEntry UnityEngine.Localization.Tables.DetailedLocalizationTable`1::GetEntry(System.Int64)
// 0x000000FA System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1::Add(System.Int64,TEntry)
// 0x000000FB System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1::Add(System.Collections.Generic.KeyValuePair`2<System.Int64,TEntry>)
// 0x000000FC System.Boolean UnityEngine.Localization.Tables.DetailedLocalizationTable`1::ContainsKey(System.Int64)
// 0x000000FD System.Boolean UnityEngine.Localization.Tables.DetailedLocalizationTable`1::ContainsValue(System.String)
// 0x000000FE System.Boolean UnityEngine.Localization.Tables.DetailedLocalizationTable`1::Contains(System.Collections.Generic.KeyValuePair`2<System.Int64,TEntry>)
// 0x000000FF System.Boolean UnityEngine.Localization.Tables.DetailedLocalizationTable`1::Remove(System.Int64)
// 0x00000100 System.Boolean UnityEngine.Localization.Tables.DetailedLocalizationTable`1::Remove(System.Collections.Generic.KeyValuePair`2<System.Int64,TEntry>)
// 0x00000101 System.Collections.Generic.IList`1<TEntry> UnityEngine.Localization.Tables.DetailedLocalizationTable`1::CheckForMissingSharedTableDataEntries(UnityEngine.Localization.Tables.MissingEntryAction)
// 0x00000102 System.Boolean UnityEngine.Localization.Tables.DetailedLocalizationTable`1::TryGetValue(System.Int64,TEntry&)
// 0x00000103 System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1::Clear()
// 0x00000104 System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1::CopyTo(System.Collections.Generic.KeyValuePair`2<System.Int64,TEntry>[],System.Int32)
// 0x00000105 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int64,TEntry>> UnityEngine.Localization.Tables.DetailedLocalizationTable`1::GetEnumerator()
// 0x00000106 System.Collections.IEnumerator UnityEngine.Localization.Tables.DetailedLocalizationTable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000107 System.String UnityEngine.Localization.Tables.DetailedLocalizationTable`1::ToString()
// 0x00000108 System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1::OnBeforeSerialize()
// 0x00000109 System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1::OnAfterDeserialize()
// 0x0000010A System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1::.ctor()
// 0x0000010B System.Boolean UnityEngine.Localization.Tables.DetailedLocalizationTable`1::<CheckForMissingSharedTableDataEntries>b__33_0(System.Collections.Generic.KeyValuePair`2<System.Int64,TEntry>)
// 0x0000010C System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1_<>c::.cctor()
// 0x0000010D System.Void UnityEngine.Localization.Tables.DetailedLocalizationTable`1_<>c::.ctor()
// 0x0000010E TEntry UnityEngine.Localization.Tables.DetailedLocalizationTable`1_<>c::<CheckForMissingSharedTableDataEntries>b__33_1(System.Collections.Generic.KeyValuePair`2<System.Int64,TEntry>)
// 0x0000010F System.Int64 UnityEngine.Localization.Tables.DetailedLocalizationTable`1_<>c::<OnAfterDeserialize>b__41_0(UnityEngine.Localization.Tables.TableEntryData)
// 0x00000110 System.Int64 UnityEngine.Localization.Tables.DistributedUIDGenerator::get_CustomEpoch()
extern void DistributedUIDGenerator_get_CustomEpoch_m813FD68FDB40D9DF9862E1FE567A50E2671B08B2 (void);
// 0x00000111 System.Int32 UnityEngine.Localization.Tables.DistributedUIDGenerator::get_MachineId()
extern void DistributedUIDGenerator_get_MachineId_m75051BDD3874DB6FBC812CF7512D8302A3057BEA (void);
// 0x00000112 System.Void UnityEngine.Localization.Tables.DistributedUIDGenerator::set_MachineId(System.Int32)
extern void DistributedUIDGenerator_set_MachineId_m79E3F9571FAC3B0486B31A1F352F2CCF599B2E3D (void);
// 0x00000113 System.Void UnityEngine.Localization.Tables.DistributedUIDGenerator::.ctor()
extern void DistributedUIDGenerator__ctor_mC137FF4985FD66B2CD09044D5F287D43F52D038F (void);
// 0x00000114 System.Void UnityEngine.Localization.Tables.DistributedUIDGenerator::.ctor(System.Int64)
extern void DistributedUIDGenerator__ctor_mA8C7CB497AC6BCF4F163F3F8F8A4B55CF81C9327 (void);
// 0x00000115 System.Int64 UnityEngine.Localization.Tables.DistributedUIDGenerator::GetNextKey()
extern void DistributedUIDGenerator_GetNextKey_m4E97FE94D6EF1A716FB6B23A2BC0568D768D1A7B (void);
// 0x00000116 System.Int64 UnityEngine.Localization.Tables.DistributedUIDGenerator::TimeStamp()
extern void DistributedUIDGenerator_TimeStamp_m51D4BABA0F7486FDED01F498456F8ABB50F9E47F (void);
// 0x00000117 System.Int32 UnityEngine.Localization.Tables.DistributedUIDGenerator::GetMachineId()
extern void DistributedUIDGenerator_GetMachineId_m4F9FD31873358E4A0087070188C279B09517F2C8 (void);
// 0x00000118 System.Int64 UnityEngine.Localization.Tables.DistributedUIDGenerator::WaitNextMillis(System.Int64)
extern void DistributedUIDGenerator_WaitNextMillis_m7B3DF926D5887B1C5F9146F569126A11AA5A2A25 (void);
// 0x00000119 System.Void UnityEngine.Localization.Tables.DistributedUIDGenerator::.cctor()
extern void DistributedUIDGenerator__cctor_mFC83D01727CE111878151F2DEF28B506BFF30BC1 (void);
// 0x0000011A System.Int64 UnityEngine.Localization.Tables.IKeyGenerator::GetNextKey()
// 0x0000011B System.Int64 UnityEngine.Localization.Tables.SequentialIDGenerator::get_NextAvailableId()
extern void SequentialIDGenerator_get_NextAvailableId_m6103EDB0B993B59721B1D045FF988AD8B0B37A41 (void);
// 0x0000011C System.Void UnityEngine.Localization.Tables.SequentialIDGenerator::.ctor()
extern void SequentialIDGenerator__ctor_m4CAC7AD7B76E2AB1ED74B685186F0D20EB1D5640 (void);
// 0x0000011D System.Void UnityEngine.Localization.Tables.SequentialIDGenerator::.ctor(System.Int64)
extern void SequentialIDGenerator__ctor_m24A65BD2B0E155ECC816E7E0457860ED15239E95 (void);
// 0x0000011E System.Int64 UnityEngine.Localization.Tables.SequentialIDGenerator::GetNextKey()
extern void SequentialIDGenerator_GetNextKey_mF083EBAA02D3EB21C8864932A3E84220178CB14F (void);
// 0x0000011F UnityEngine.Localization.LocaleIdentifier UnityEngine.Localization.Tables.LocalizationTable::get_LocaleIdentifier()
extern void LocalizationTable_get_LocaleIdentifier_mCDFBA2A154AA02D2055FB0607732F80D28491E77 (void);
// 0x00000120 System.Void UnityEngine.Localization.Tables.LocalizationTable::set_LocaleIdentifier(UnityEngine.Localization.LocaleIdentifier)
extern void LocalizationTable_set_LocaleIdentifier_m96B7561EC609F68A4BE80D59394418E6D69B8A52 (void);
// 0x00000121 System.String UnityEngine.Localization.Tables.LocalizationTable::get_TableCollectionName()
extern void LocalizationTable_get_TableCollectionName_m678F13B4610B474F7FC077C04FF429AFF4371BB3 (void);
// 0x00000122 UnityEngine.Localization.Tables.SharedTableData UnityEngine.Localization.Tables.LocalizationTable::get_SharedData()
extern void LocalizationTable_get_SharedData_mA7CE9852A576233876D61E0D60DB2187A0491054 (void);
// 0x00000123 System.Void UnityEngine.Localization.Tables.LocalizationTable::set_SharedData(UnityEngine.Localization.Tables.SharedTableData)
extern void LocalizationTable_set_SharedData_m96D7BB75944BBD28CBDFD8870054D5CDEB712AA0 (void);
// 0x00000124 System.Collections.Generic.List`1<UnityEngine.Localization.Tables.TableEntryData> UnityEngine.Localization.Tables.LocalizationTable::get_TableData()
extern void LocalizationTable_get_TableData_m78561AC7AAA47CBC85F617B7946F9682A106A505 (void);
// 0x00000125 System.Collections.Generic.IList`1<UnityEngine.Localization.Metadata.IMetadata> UnityEngine.Localization.Tables.LocalizationTable::get_MetadataEntries()
extern void LocalizationTable_get_MetadataEntries_mF0BB56C154B55E120A9EE25263AB0E5F5A2F677B (void);
// 0x00000126 TObject UnityEngine.Localization.Tables.LocalizationTable::GetMetadata()
// 0x00000127 System.Void UnityEngine.Localization.Tables.LocalizationTable::GetMetadatas(System.Collections.Generic.IList`1<TObject>)
// 0x00000128 System.Collections.Generic.IList`1<TObject> UnityEngine.Localization.Tables.LocalizationTable::GetMetadatas()
// 0x00000129 System.Void UnityEngine.Localization.Tables.LocalizationTable::AddMetadata(UnityEngine.Localization.Metadata.IMetadata)
extern void LocalizationTable_AddMetadata_m5E42D5FF1E3290B7A1463400561B55E4246A018D (void);
// 0x0000012A System.Boolean UnityEngine.Localization.Tables.LocalizationTable::RemoveMetadata(UnityEngine.Localization.Metadata.IMetadata)
extern void LocalizationTable_RemoveMetadata_m15B0DDF699FFED75A953F7BB445B669A7E7E5610 (void);
// 0x0000012B System.Boolean UnityEngine.Localization.Tables.LocalizationTable::Contains(UnityEngine.Localization.Metadata.IMetadata)
extern void LocalizationTable_Contains_mE3559B8D1C2130B1920B3A1B27BD80FF5F1D66EC (void);
// 0x0000012C System.Void UnityEngine.Localization.Tables.LocalizationTable::CreateEmpty(UnityEngine.Localization.Tables.TableEntryReference)
// 0x0000012D System.Int64 UnityEngine.Localization.Tables.LocalizationTable::FindKeyId(System.String,System.Boolean)
extern void LocalizationTable_FindKeyId_m31684AC7BE81E1CA28042F38A02F414082C484A4 (void);
// 0x0000012E System.Void UnityEngine.Localization.Tables.LocalizationTable::VerifySharedTableDataIsNotNull()
extern void LocalizationTable_VerifySharedTableDataIsNotNull_m0F79FEB3B99E8E437A95F1925EF71EF906265228 (void);
// 0x0000012F System.String UnityEngine.Localization.Tables.LocalizationTable::ToString()
extern void LocalizationTable_ToString_m1CC7839B88480412862717724D0613199D248E21 (void);
// 0x00000130 System.Int32 UnityEngine.Localization.Tables.LocalizationTable::CompareTo(UnityEngine.Localization.Tables.LocalizationTable)
extern void LocalizationTable_CompareTo_m4ED56AC885C9B698CEAF20E5E2ABF6D546523017 (void);
// 0x00000131 System.Void UnityEngine.Localization.Tables.LocalizationTable::.ctor()
extern void LocalizationTable__ctor_m330FEFEB93885A171739272F2F492429B17D8EB2 (void);
// 0x00000132 System.Collections.Generic.List`1<UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry> UnityEngine.Localization.Tables.SharedTableData::get_Entries()
extern void SharedTableData_get_Entries_mAA5CDB7F148EB71063D9686F4D55146D95F4BAB2 (void);
// 0x00000133 System.String UnityEngine.Localization.Tables.SharedTableData::get_TableCollectionName()
extern void SharedTableData_get_TableCollectionName_m72856600FD145DC0C130F0736F17A84EFCF9E64C (void);
// 0x00000134 System.Void UnityEngine.Localization.Tables.SharedTableData::set_TableCollectionName(System.String)
extern void SharedTableData_set_TableCollectionName_m24BDB7E83AEF335FF76CB0F1CD2484E998FC4D3D (void);
// 0x00000135 System.Guid UnityEngine.Localization.Tables.SharedTableData::get_TableCollectionNameGuid()
extern void SharedTableData_get_TableCollectionNameGuid_mECF51D3522303934BCDCA7E8C8C9BC7AEA32DAB0 (void);
// 0x00000136 System.Void UnityEngine.Localization.Tables.SharedTableData::set_TableCollectionNameGuid(System.Guid)
extern void SharedTableData_set_TableCollectionNameGuid_m72546494ED70DB610F7C7C9FB3EC1E6B8015764F (void);
// 0x00000137 UnityEngine.Localization.Metadata.MetadataCollection UnityEngine.Localization.Tables.SharedTableData::get_Metadata()
extern void SharedTableData_get_Metadata_mA9214D6B75B18E98E0A81A012698DFE32F43533E (void);
// 0x00000138 System.Void UnityEngine.Localization.Tables.SharedTableData::set_Metadata(UnityEngine.Localization.Metadata.MetadataCollection)
extern void SharedTableData_set_Metadata_mDC6AE078B4E3EF6A6150CE8D43B47E2378499F90 (void);
// 0x00000139 UnityEngine.Localization.Tables.IKeyGenerator UnityEngine.Localization.Tables.SharedTableData::get_KeyGenerator()
extern void SharedTableData_get_KeyGenerator_m3F2DE0652F16E90127512BAED22DF92D41264EED (void);
// 0x0000013A System.Void UnityEngine.Localization.Tables.SharedTableData::set_KeyGenerator(UnityEngine.Localization.Tables.IKeyGenerator)
extern void SharedTableData_set_KeyGenerator_m2D9386E05F6D8921F6A7B8174723513CD2F562DF (void);
// 0x0000013B System.String UnityEngine.Localization.Tables.SharedTableData::GetKey(System.Int64)
extern void SharedTableData_GetKey_m1FD87D09A1ACE0B88AA5D0272D2741471792837C (void);
// 0x0000013C System.Int64 UnityEngine.Localization.Tables.SharedTableData::GetId(System.String)
extern void SharedTableData_GetId_m90619F58B422E957B5527FB90921D65025200190 (void);
// 0x0000013D System.Int64 UnityEngine.Localization.Tables.SharedTableData::GetId(System.String,System.Boolean)
extern void SharedTableData_GetId_m8E983275B27ED7A64E6AC2DB18EB983F3B6B7B22 (void);
// 0x0000013E UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry UnityEngine.Localization.Tables.SharedTableData::GetEntryFromReference(UnityEngine.Localization.Tables.TableEntryReference)
extern void SharedTableData_GetEntryFromReference_m9CE1863DC085144CB18ACDAC502DA3A271626480 (void);
// 0x0000013F UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry UnityEngine.Localization.Tables.SharedTableData::GetEntry(System.Int64)
extern void SharedTableData_GetEntry_m654FA769A326078483AE9084CB5FF574B90E9189 (void);
// 0x00000140 UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry UnityEngine.Localization.Tables.SharedTableData::GetEntry(System.String)
extern void SharedTableData_GetEntry_m3B05CC7B4F765893CC6A6E55864AC37AE761108E (void);
// 0x00000141 System.Boolean UnityEngine.Localization.Tables.SharedTableData::Contains(System.Int64)
extern void SharedTableData_Contains_mF2F66F01B385BE8B9C8F5E0006B22C855A4ED8F8 (void);
// 0x00000142 System.Boolean UnityEngine.Localization.Tables.SharedTableData::Contains(System.String)
extern void SharedTableData_Contains_mA79D44D9CE1555FD1F4F0BBA7170D0C3DB6CFFCE (void);
// 0x00000143 UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry UnityEngine.Localization.Tables.SharedTableData::AddKey(System.String)
extern void SharedTableData_AddKey_mFCEEFE720C0D561153289132595CE0881B1F75A9 (void);
// 0x00000144 UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry UnityEngine.Localization.Tables.SharedTableData::AddKey(System.String,System.Int64)
extern void SharedTableData_AddKey_m7EC6BB7176B0AE2BDE9696765051E890D5A332CE (void);
// 0x00000145 UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry UnityEngine.Localization.Tables.SharedTableData::AddKey()
extern void SharedTableData_AddKey_mB7BE4A74F085771C05D7F20983CF4F8FBE32EF54 (void);
// 0x00000146 System.Void UnityEngine.Localization.Tables.SharedTableData::RemoveKey(System.Int64)
extern void SharedTableData_RemoveKey_mB67741750E5EBF1D5D569B4F5E6B66FDD8A7F167 (void);
// 0x00000147 System.Void UnityEngine.Localization.Tables.SharedTableData::RemoveKey(System.String)
extern void SharedTableData_RemoveKey_mD7C242AA34819E857289EDC7577106C67632F755 (void);
// 0x00000148 System.Void UnityEngine.Localization.Tables.SharedTableData::RenameKey(System.Int64,System.String)
extern void SharedTableData_RenameKey_m48B68C52026E4E858656FA01607B62EDCBA68BCE (void);
// 0x00000149 System.Void UnityEngine.Localization.Tables.SharedTableData::RenameKey(System.String,System.String)
extern void SharedTableData_RenameKey_m216DA159F7ACFF823F097DEA6DE98EE07D7920B8 (void);
// 0x0000014A System.Boolean UnityEngine.Localization.Tables.SharedTableData::RemapId(System.Int64,System.Int64)
extern void SharedTableData_RemapId_mE0C26DCD74141AE8D1EBC2EABC40A17934D779BF (void);
// 0x0000014B UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry UnityEngine.Localization.Tables.SharedTableData::FindSimilarKey(System.String,System.Int32&)
extern void SharedTableData_FindSimilarKey_mC3191D2AA7F58A000256EA0A797DFD7DB6CFFE93 (void);
// 0x0000014C System.Int32 UnityEngine.Localization.Tables.SharedTableData::ComputeLevenshteinDistance(System.String,System.String)
extern void SharedTableData_ComputeLevenshteinDistance_m6D04691E13AFE199AF2F50550B7F508BE7778FE7 (void);
// 0x0000014D UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry UnityEngine.Localization.Tables.SharedTableData::AddKeyInternal(System.String)
extern void SharedTableData_AddKeyInternal_m03013F80856905A1EFBD3812507255F77D15BC57 (void);
// 0x0000014E UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry UnityEngine.Localization.Tables.SharedTableData::AddKeyInternal(System.String,System.Int64)
extern void SharedTableData_AddKeyInternal_mF5D7905E1C28A7FC142B83E82BFBDA11B136BD8D (void);
// 0x0000014F System.Void UnityEngine.Localization.Tables.SharedTableData::RenameKeyInternal(UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry,System.String)
extern void SharedTableData_RenameKeyInternal_m938FF1FAA074C6CE3744752ABC2E8591A18DF37E (void);
// 0x00000150 System.Void UnityEngine.Localization.Tables.SharedTableData::RemoveKeyInternal(UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry)
extern void SharedTableData_RemoveKeyInternal_m56C085641143AB191367890548C9AF6442A9DB29 (void);
// 0x00000151 UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry UnityEngine.Localization.Tables.SharedTableData::FindWithId(System.Int64)
extern void SharedTableData_FindWithId_m22954CAD35DC70A7F389219C287284030F2C4413 (void);
// 0x00000152 UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry UnityEngine.Localization.Tables.SharedTableData::FindWithKey(System.String)
extern void SharedTableData_FindWithKey_m383F7ECACC4906543D10324930FA23652C61710E (void);
// 0x00000153 System.String UnityEngine.Localization.Tables.SharedTableData::ToString()
extern void SharedTableData_ToString_m1BCE5867E5E49D4110486CDB105AD827B623CE5D (void);
// 0x00000154 System.Void UnityEngine.Localization.Tables.SharedTableData::OnBeforeSerialize()
extern void SharedTableData_OnBeforeSerialize_m3A7694F1AF596891335EA39C1B0B8180494CE939 (void);
// 0x00000155 System.Void UnityEngine.Localization.Tables.SharedTableData::OnAfterDeserialize()
extern void SharedTableData_OnAfterDeserialize_m865D497E4615E285A54C9423CA68C84324F477A0 (void);
// 0x00000156 System.Void UnityEngine.Localization.Tables.SharedTableData::.ctor()
extern void SharedTableData__ctor_m7F85BA4B33E99FDCF3F73F5206059CB75580958F (void);
// 0x00000157 System.Int64 UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry::get_Id()
extern void SharedTableEntry_get_Id_m76E7DC1967E1DF86CD9CC4B8E5050C91A761A031 (void);
// 0x00000158 System.Void UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry::set_Id(System.Int64)
extern void SharedTableEntry_set_Id_mEC8465D11C17CD6A58129B741E0521CFBABBE14F (void);
// 0x00000159 System.String UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry::get_Key()
extern void SharedTableEntry_get_Key_mD04A1F7B3F414B36D6F4EAAAD31E0398FEAC2023 (void);
// 0x0000015A System.Void UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry::set_Key(System.String)
extern void SharedTableEntry_set_Key_m90D913399333B83337DCBCB9BC8B93C2D905B5BD (void);
// 0x0000015B UnityEngine.Localization.Metadata.MetadataCollection UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry::get_Metadata()
extern void SharedTableEntry_get_Metadata_m9E59A5CECC9B5C4B9DCCD8599BF08A41A5D726E4 (void);
// 0x0000015C System.Void UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry::set_Metadata(UnityEngine.Localization.Metadata.MetadataCollection)
extern void SharedTableEntry_set_Metadata_mDE342A6B182B6457F3B84C1A7B5AE8845B2B69FC (void);
// 0x0000015D System.String UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry::ToString()
extern void SharedTableEntry_ToString_mDF719896E888CECF5AE1D9FED7F1B81BBEE67D02 (void);
// 0x0000015E System.Void UnityEngine.Localization.Tables.SharedTableData_SharedTableEntry::.ctor()
extern void SharedTableEntry__ctor_m061C1F354B8649432AF9305804A0EEFDD9A11F94 (void);
// 0x0000015F System.String UnityEngine.Localization.Tables.StringTableEntry::get_Value()
extern void StringTableEntry_get_Value_mC877F1980AD7162FDAAE1DBA2EAEB5F7A58E8B2D (void);
// 0x00000160 System.Void UnityEngine.Localization.Tables.StringTableEntry::set_Value(System.String)
extern void StringTableEntry_set_Value_mEDFB3CE813A29CEB59D08C50F3DA2F9537193C54 (void);
// 0x00000161 System.Boolean UnityEngine.Localization.Tables.StringTableEntry::get_IsSmart()
extern void StringTableEntry_get_IsSmart_m0FDD0518AEA22C9B9E9BB71D811CD20F34937A86 (void);
// 0x00000162 System.Void UnityEngine.Localization.Tables.StringTableEntry::set_IsSmart(System.Boolean)
extern void StringTableEntry_set_IsSmart_mFC7D1B7382561EDEB2567D5114EBFE47BF1405A4 (void);
// 0x00000163 System.Void UnityEngine.Localization.Tables.StringTableEntry::.ctor()
extern void StringTableEntry__ctor_m07A76F3699E5F764BEB63CD849FCA493E1126E96 (void);
// 0x00000164 System.Void UnityEngine.Localization.Tables.StringTableEntry::RemoveFromTable()
extern void StringTableEntry_RemoveFromTable_m1615BFD64092A652D53F71DAAF84ECA9E6D4DE27 (void);
// 0x00000165 System.String UnityEngine.Localization.Tables.StringTableEntry::GetLocalizedString()
extern void StringTableEntry_GetLocalizedString_m5EC063A53AE08621967F5AB13BB24B7EB0C6BA83 (void);
// 0x00000166 System.String UnityEngine.Localization.Tables.StringTableEntry::GetLocalizedString(System.Object[])
extern void StringTableEntry_GetLocalizedString_mE1839039C1914B91FB098A687371DC45D48B19AB (void);
// 0x00000167 UnityEngine.Localization.Tables.StringTableEntry UnityEngine.Localization.Tables.StringTable::CreateTableEntry()
extern void StringTable_CreateTableEntry_m12D24600ECD65C9699C593790D7F40AFC87E6ABB (void);
// 0x00000168 System.Void UnityEngine.Localization.Tables.StringTable::.ctor()
extern void StringTable__ctor_m88D548D8544DC17069C15A0EB7068969265B731C (void);
// 0x00000169 System.Int64 UnityEngine.Localization.Tables.TableEntryData::get_Id()
extern void TableEntryData_get_Id_mD425E6667F610A63618793A12705C55ED68BC440 (void);
// 0x0000016A System.Void UnityEngine.Localization.Tables.TableEntryData::set_Id(System.Int64)
extern void TableEntryData_set_Id_m6D41CBF1B569B93379A5EC32F15D6BE79C12FBF5 (void);
// 0x0000016B System.String UnityEngine.Localization.Tables.TableEntryData::get_Localized()
extern void TableEntryData_get_Localized_mE3A6AA11D96CCAE04E0FB2A6D605619145A7F603 (void);
// 0x0000016C System.Void UnityEngine.Localization.Tables.TableEntryData::set_Localized(System.String)
extern void TableEntryData_set_Localized_m05BE69FCB707305908C7C0F298B4B1971E1E1109 (void);
// 0x0000016D UnityEngine.Localization.Metadata.MetadataCollection UnityEngine.Localization.Tables.TableEntryData::get_Metadata()
extern void TableEntryData_get_Metadata_mFF32EE01919FE5216723BC946398A309B28B4AC3 (void);
// 0x0000016E System.Void UnityEngine.Localization.Tables.TableEntryData::set_Metadata(UnityEngine.Localization.Metadata.MetadataCollection)
extern void TableEntryData_set_Metadata_m21EE16A9FFDAD0A80D985C33DA4DBCCF5CD9A67D (void);
// 0x0000016F System.Void UnityEngine.Localization.Tables.TableEntryData::.ctor()
extern void TableEntryData__ctor_m80BE41D0D3BC72D30CCD2101D51F2BDFD94A1096 (void);
// 0x00000170 System.Void UnityEngine.Localization.Tables.TableEntryData::.ctor(System.Int64)
extern void TableEntryData__ctor_m94759C3707CF9A517C921502DCF65AD45E10782D (void);
// 0x00000171 System.Void UnityEngine.Localization.Tables.TableEntryData::.ctor(System.Int64,System.String)
extern void TableEntryData__ctor_mD2558A9278ED1BC9E8904CC577DE0FCEE4641BD3 (void);
// 0x00000172 UnityEngine.Localization.Tables.TableReference_Type UnityEngine.Localization.Tables.TableReference::get_ReferenceType()
extern void TableReference_get_ReferenceType_mC893ED4BB004BD59ED3BC2D808C54ADF0741E6BE_AdjustorThunk (void);
// 0x00000173 System.Void UnityEngine.Localization.Tables.TableReference::set_ReferenceType(UnityEngine.Localization.Tables.TableReference_Type)
extern void TableReference_set_ReferenceType_m75A728B1FE51186F8E8042911C0730B52CCE6DD0_AdjustorThunk (void);
// 0x00000174 System.Guid UnityEngine.Localization.Tables.TableReference::get_TableCollectionNameGuid()
extern void TableReference_get_TableCollectionNameGuid_mE63A5EE8BCA498A930F9E4A7CA63155CE14D6C97_AdjustorThunk (void);
// 0x00000175 System.Void UnityEngine.Localization.Tables.TableReference::set_TableCollectionNameGuid(System.Guid)
extern void TableReference_set_TableCollectionNameGuid_m06C127AD797D1BB0703F6836FBF2882CB1DD020E_AdjustorThunk (void);
// 0x00000176 System.String UnityEngine.Localization.Tables.TableReference::get_TableCollectionName()
extern void TableReference_get_TableCollectionName_mECB9C94F17424B265C16AFC589A295A8FBD4AA25_AdjustorThunk (void);
// 0x00000177 System.Void UnityEngine.Localization.Tables.TableReference::set_TableCollectionName(System.String)
extern void TableReference_set_TableCollectionName_m7E7B174DB6B85BD406D2F67854974C6024D6DDE2_AdjustorThunk (void);
// 0x00000178 UnityEngine.Localization.Tables.TableReference UnityEngine.Localization.Tables.TableReference::op_Implicit(System.String)
extern void TableReference_op_Implicit_m38ABCB1A36550166E739CEEB36732AA909BCCDAD (void);
// 0x00000179 UnityEngine.Localization.Tables.TableReference UnityEngine.Localization.Tables.TableReference::op_Implicit(System.Guid)
extern void TableReference_op_Implicit_m38CA289C42CCEA86764AE78F29755527E7BEA133 (void);
// 0x0000017A System.String UnityEngine.Localization.Tables.TableReference::op_Implicit(UnityEngine.Localization.Tables.TableReference)
extern void TableReference_op_Implicit_mA3DA44CAC93CACDA1843648492BDA54F8D0FDCE0 (void);
// 0x0000017B System.Guid UnityEngine.Localization.Tables.TableReference::op_Implicit(UnityEngine.Localization.Tables.TableReference)
extern void TableReference_op_Implicit_m958243FB07A695A2CC852C7428466A52E949AA46 (void);
// 0x0000017C System.Void UnityEngine.Localization.Tables.TableReference::Validate()
extern void TableReference_Validate_m52665FBDBEA49EEE2B65FF46FA5142E034420DEA_AdjustorThunk (void);
// 0x0000017D System.String UnityEngine.Localization.Tables.TableReference::GetSerializedString()
extern void TableReference_GetSerializedString_m024253B2264C9C25AACFE6C23921C11BB27649F9_AdjustorThunk (void);
// 0x0000017E System.String UnityEngine.Localization.Tables.TableReference::ToString()
extern void TableReference_ToString_mB32AE382DEA9B163A3AE0E61B1F83431E151129F_AdjustorThunk (void);
// 0x0000017F System.Boolean UnityEngine.Localization.Tables.TableReference::Equals(System.Object)
extern void TableReference_Equals_m76717DC107E3638DF3717C4DE9C738287E440F9C_AdjustorThunk (void);
// 0x00000180 System.Int32 UnityEngine.Localization.Tables.TableReference::GetHashCode()
extern void TableReference_GetHashCode_m1801B22266D705F42D108EBE165D795DCDDA2D31_AdjustorThunk (void);
// 0x00000181 System.Boolean UnityEngine.Localization.Tables.TableReference::Equals(UnityEngine.Localization.Tables.TableReference)
extern void TableReference_Equals_m8DFA7C7D14B873326653F4C144926D5741D773D7_AdjustorThunk (void);
// 0x00000182 System.Guid UnityEngine.Localization.Tables.TableReference::GuidFromString(System.String)
extern void TableReference_GuidFromString_m3F0C95ECEF1E7CE530CC2610244E3746C6668A69 (void);
// 0x00000183 System.String UnityEngine.Localization.Tables.TableReference::StringFromGuid(System.Guid)
extern void TableReference_StringFromGuid_m4DAB416BB95EEA2D9BC7B68D2F84926364A3E745 (void);
// 0x00000184 UnityEngine.Localization.Tables.TableReference UnityEngine.Localization.Tables.TableReference::TableReferenceFromString(System.String)
extern void TableReference_TableReferenceFromString_m0568E59E756C386550443C37DCA9292AE4824A71 (void);
// 0x00000185 System.Boolean UnityEngine.Localization.Tables.TableReference::IsGuid(System.String)
extern void TableReference_IsGuid_mFC86D33A0B8581ADDC1424249B73383EA8F388E1 (void);
// 0x00000186 System.Void UnityEngine.Localization.Tables.TableReference::OnBeforeSerialize()
extern void TableReference_OnBeforeSerialize_m3CB3EEFE383275AEDB7BE695091CA7B20584DF9B_AdjustorThunk (void);
// 0x00000187 System.Void UnityEngine.Localization.Tables.TableReference::OnAfterDeserialize()
extern void TableReference_OnAfterDeserialize_mC96C0055D863D3B1126921751A00E6A5059372EA_AdjustorThunk (void);
// 0x00000188 UnityEngine.Localization.Tables.TableEntryReference_Type UnityEngine.Localization.Tables.TableEntryReference::get_ReferenceType()
extern void TableEntryReference_get_ReferenceType_mC145F5B4C8E39097B71AF7799E18AB079FEB9556_AdjustorThunk (void);
// 0x00000189 System.Void UnityEngine.Localization.Tables.TableEntryReference::set_ReferenceType(UnityEngine.Localization.Tables.TableEntryReference_Type)
extern void TableEntryReference_set_ReferenceType_m329A5A1A46BA792BA5497C0EC35E194EA83531E1_AdjustorThunk (void);
// 0x0000018A System.Int64 UnityEngine.Localization.Tables.TableEntryReference::get_KeyId()
extern void TableEntryReference_get_KeyId_m74FA67F6DAD6B6E151838ED9F103388AE41C1153_AdjustorThunk (void);
// 0x0000018B System.Void UnityEngine.Localization.Tables.TableEntryReference::set_KeyId(System.Int64)
extern void TableEntryReference_set_KeyId_m3B0708CC919B41C02EF0CF24B3F9883074172CE0_AdjustorThunk (void);
// 0x0000018C System.String UnityEngine.Localization.Tables.TableEntryReference::get_Key()
extern void TableEntryReference_get_Key_mC8BE974FF56F790B5E798EC401FBB425B6F78D44_AdjustorThunk (void);
// 0x0000018D System.Void UnityEngine.Localization.Tables.TableEntryReference::set_Key(System.String)
extern void TableEntryReference_set_Key_m224E76DD1AA1CE7241714EBF612EF19ACA961A83_AdjustorThunk (void);
// 0x0000018E UnityEngine.Localization.Tables.TableEntryReference UnityEngine.Localization.Tables.TableEntryReference::op_Implicit(System.String)
extern void TableEntryReference_op_Implicit_m62B8A8F98EF657EE6CC0421484D376D05F1BDC88 (void);
// 0x0000018F UnityEngine.Localization.Tables.TableEntryReference UnityEngine.Localization.Tables.TableEntryReference::op_Implicit(System.Int64)
extern void TableEntryReference_op_Implicit_mA6852C9B79787C3A4AE07E576B66D8260D2322DE (void);
// 0x00000190 System.String UnityEngine.Localization.Tables.TableEntryReference::op_Implicit(UnityEngine.Localization.Tables.TableEntryReference)
extern void TableEntryReference_op_Implicit_m6245960816D37B46B0A91D49951836E87C8F891E (void);
// 0x00000191 System.Int64 UnityEngine.Localization.Tables.TableEntryReference::op_Implicit(UnityEngine.Localization.Tables.TableEntryReference)
extern void TableEntryReference_op_Implicit_m47C2342B7D120624531322751EE9A36E58EF0D62 (void);
// 0x00000192 System.Void UnityEngine.Localization.Tables.TableEntryReference::Validate()
extern void TableEntryReference_Validate_m01EE19D2965E93860AD29D8EEEF7B1AFFCB658F5_AdjustorThunk (void);
// 0x00000193 System.String UnityEngine.Localization.Tables.TableEntryReference::ResolveKeyName(UnityEngine.Localization.Tables.SharedTableData)
extern void TableEntryReference_ResolveKeyName_m0DE9C31AA9570F5BD7852A8F5C3C9D1F18A7191A_AdjustorThunk (void);
// 0x00000194 System.String UnityEngine.Localization.Tables.TableEntryReference::ToString()
extern void TableEntryReference_ToString_m5251A9B652CCB8AE01F60738FC8E5FB702D08245_AdjustorThunk (void);
// 0x00000195 System.Boolean UnityEngine.Localization.Tables.TableEntryReference::Equals(System.Object)
extern void TableEntryReference_Equals_m74489CFE2D45F251D2508B64E419C8248571CF3D_AdjustorThunk (void);
// 0x00000196 System.Boolean UnityEngine.Localization.Tables.TableEntryReference::Equals(UnityEngine.Localization.Tables.TableEntryReference)
extern void TableEntryReference_Equals_m17C4BD6E7C66ABC81069D97842574877A287CBCE_AdjustorThunk (void);
// 0x00000197 System.Int32 UnityEngine.Localization.Tables.TableEntryReference::GetHashCode()
extern void TableEntryReference_GetHashCode_m8891B49029FADB026C11C6F163B5ACA344C7CC6A_AdjustorThunk (void);
// 0x00000198 System.Void UnityEngine.Localization.Tables.TableEntryReference::OnBeforeSerialize()
extern void TableEntryReference_OnBeforeSerialize_mE30C216732C38BB66587F9CEB6C817EC29DAA464_AdjustorThunk (void);
// 0x00000199 System.Void UnityEngine.Localization.Tables.TableEntryReference::OnAfterDeserialize()
extern void TableEntryReference_OnAfterDeserialize_m06BC208220A8B545622EE3AAEE68F1ADD2CCA952_AdjustorThunk (void);
// 0x0000019A System.Void UnityEngine.Localization.SmartFormat.FormattingErrorEventArgs::.ctor(System.String,System.Int32,System.Boolean)
extern void FormattingErrorEventArgs__ctor_m0FCF09D85986FA330CDAB8AB76EFB08B01DF7357 (void);
// 0x0000019B System.String UnityEngine.Localization.SmartFormat.FormattingErrorEventArgs::get_Placeholder()
extern void FormattingErrorEventArgs_get_Placeholder_mCA5AD225FC7170A146CD5B0C64AE558F452A302C (void);
// 0x0000019C System.Void UnityEngine.Localization.SmartFormat.FormattingErrorEventArgs::set_Placeholder(System.String)
extern void FormattingErrorEventArgs_set_Placeholder_m1765C64FAFCF3E863A44DD71A8EB7C2025AECFD9 (void);
// 0x0000019D System.Int32 UnityEngine.Localization.SmartFormat.FormattingErrorEventArgs::get_ErrorIndex()
extern void FormattingErrorEventArgs_get_ErrorIndex_m6F49589584BFA61E05557E94A828AE5C64774B16 (void);
// 0x0000019E System.Void UnityEngine.Localization.SmartFormat.FormattingErrorEventArgs::set_ErrorIndex(System.Int32)
extern void FormattingErrorEventArgs_set_ErrorIndex_m44E22A7244DA09F8D6A83B2512643DD670ABC6D7 (void);
// 0x0000019F System.Boolean UnityEngine.Localization.SmartFormat.FormattingErrorEventArgs::get_IgnoreError()
extern void FormattingErrorEventArgs_get_IgnoreError_m6A6453B4FB6D2A5B1531A5D6A790A153549ABDBB (void);
// 0x000001A0 System.Void UnityEngine.Localization.SmartFormat.FormattingErrorEventArgs::set_IgnoreError(System.Boolean)
extern void FormattingErrorEventArgs_set_IgnoreError_m40ED1C4B86E224CB272BC2B40B493EDB8BED07FF (void);
// 0x000001A1 System.String UnityEngine.Localization.SmartFormat.Smart::Format(System.String,System.Object[])
extern void Smart_Format_m77690F4F96CBD5D287EAF0BDCCE65D16C81D9F32 (void);
// 0x000001A2 System.String UnityEngine.Localization.SmartFormat.Smart::Format(System.IFormatProvider,System.String,System.Object[])
extern void Smart_Format_m018ABA7FAC3126A082471C4F8E0A150C274D6F3A (void);
// 0x000001A3 System.String UnityEngine.Localization.SmartFormat.Smart::Format(System.String,System.Object,System.Object,System.Object)
extern void Smart_Format_m43257B51DC0A0EF74CAA48D79F3F6B9E8436ACC5 (void);
// 0x000001A4 System.String UnityEngine.Localization.SmartFormat.Smart::Format(System.String,System.Object,System.Object)
extern void Smart_Format_m62AD7FEBBC436CB0C0ABC9248D2141DB37218970 (void);
// 0x000001A5 System.String UnityEngine.Localization.SmartFormat.Smart::Format(System.String,System.Object)
extern void Smart_Format_m04D01E16C7B7079CEAD97C73BCB56F4CBA4DCD35 (void);
// 0x000001A6 UnityEngine.Localization.SmartFormat.SmartFormatter UnityEngine.Localization.SmartFormat.Smart::get_Default()
extern void Smart_get_Default_m3194B892D21B184218F50FDE39D1BC12E7639814 (void);
// 0x000001A7 System.Void UnityEngine.Localization.SmartFormat.Smart::set_Default(UnityEngine.Localization.SmartFormat.SmartFormatter)
extern void Smart_set_Default_m6480FA90F04C186235C9DE64AA22322A3107B5B8 (void);
// 0x000001A8 UnityEngine.Localization.SmartFormat.SmartFormatter UnityEngine.Localization.SmartFormat.Smart::CreateDefaultSmartFormat()
extern void Smart_CreateDefaultSmartFormat_m79E3058FF0C33E0AA990CE401F67F31C25C7ED01 (void);
// 0x000001A9 System.Void UnityEngine.Localization.SmartFormat.SmartExtensions::AppendSmart(System.Text.StringBuilder,System.String,System.Object[])
extern void SmartExtensions_AppendSmart_mDABC8B72ED79A36200F5F5A9623D993BAF5D726B (void);
// 0x000001AA System.Void UnityEngine.Localization.SmartFormat.SmartExtensions::AppendLineSmart(System.Text.StringBuilder,System.String,System.Object[])
extern void SmartExtensions_AppendLineSmart_mD1815723E3DAF50087B68E8A41BF885E785927F1 (void);
// 0x000001AB System.Void UnityEngine.Localization.SmartFormat.SmartExtensions::WriteSmart(System.IO.TextWriter,System.String,System.Object[])
extern void SmartExtensions_WriteSmart_mC445BFB80CFCC576F01863091BAEC8A9CA6EA583 (void);
// 0x000001AC System.Void UnityEngine.Localization.SmartFormat.SmartExtensions::WriteLineSmart(System.IO.TextWriter,System.String,System.Object[])
extern void SmartExtensions_WriteLineSmart_mC739A4F89F8CAA34DBEF29DF974C9B06CDD4FC5F (void);
// 0x000001AD System.String UnityEngine.Localization.SmartFormat.SmartExtensions::FormatSmart(System.String,System.Object[])
extern void SmartExtensions_FormatSmart_m036379833C432E01646E79E55920623018B6AA2B (void);
// 0x000001AE System.String UnityEngine.Localization.SmartFormat.SmartExtensions::FormatSmart(System.String,UnityEngine.Localization.SmartFormat.Core.Formatting.FormatCache&,System.Object[])
extern void SmartExtensions_FormatSmart_mC201ADA9C175D3CF77C1F2E219F8C3D7A172B4A6 (void);
// 0x000001AF System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::add_OnFormattingFailure(System.EventHandler`1<UnityEngine.Localization.SmartFormat.FormattingErrorEventArgs>)
extern void SmartFormatter_add_OnFormattingFailure_m679F18D60035AB977E26CF01FF1F0145C7793FB7 (void);
// 0x000001B0 System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::remove_OnFormattingFailure(System.EventHandler`1<UnityEngine.Localization.SmartFormat.FormattingErrorEventArgs>)
extern void SmartFormatter_remove_OnFormattingFailure_mAC56D7DEA77C5D16B9A0E356C01E2EA98F893D81 (void);
// 0x000001B1 System.Collections.Generic.List`1<UnityEngine.Localization.SmartFormat.Core.Extensions.ISource> UnityEngine.Localization.SmartFormat.SmartFormatter::get_SourceExtensions()
extern void SmartFormatter_get_SourceExtensions_mA06C14A9FDD59462C7DDEFED51841D84CC43DD55 (void);
// 0x000001B2 System.Collections.Generic.List`1<UnityEngine.Localization.SmartFormat.Core.Extensions.IFormatter> UnityEngine.Localization.SmartFormat.SmartFormatter::get_FormatterExtensions()
extern void SmartFormatter_get_FormatterExtensions_mCB78E4331F11DAA0A4E7D126FC2CAC2ED37B3992 (void);
// 0x000001B3 System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::.ctor()
extern void SmartFormatter__ctor_m3299B28E9A742658E3E5978783C0AA37C2F007EB (void);
// 0x000001B4 System.String[] UnityEngine.Localization.SmartFormat.SmartFormatter::GetNotEmptyFormatterExtensionNames()
extern void SmartFormatter_GetNotEmptyFormatterExtensionNames_m56A15B799B974C07E44CCAFD7D6E325D962A5104 (void);
// 0x000001B5 System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::AddExtensions(UnityEngine.Localization.SmartFormat.Core.Extensions.ISource[])
extern void SmartFormatter_AddExtensions_m8A39AE0BE3937BC0FB1D1C9F6A71ADC0052D8102 (void);
// 0x000001B6 System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::AddExtensions(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormatter[])
extern void SmartFormatter_AddExtensions_mD68C822FFC4F4FD20745C9CC54DB0F8771CDC8C9 (void);
// 0x000001B7 T UnityEngine.Localization.SmartFormat.SmartFormatter::GetSourceExtension()
// 0x000001B8 T UnityEngine.Localization.SmartFormat.SmartFormatter::GetFormatterExtension()
// 0x000001B9 UnityEngine.Localization.SmartFormat.Core.Parsing.Parser UnityEngine.Localization.SmartFormat.SmartFormatter::get_Parser()
extern void SmartFormatter_get_Parser_mFE5C5B7AAE50F5939B3DE060C3A8CAF2F5CECA1B (void);
// 0x000001BA System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::set_Parser(UnityEngine.Localization.SmartFormat.Core.Parsing.Parser)
extern void SmartFormatter_set_Parser_m42DB5CD7BF8BD4F7FC667C34D03E570787661017 (void);
// 0x000001BB UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings UnityEngine.Localization.SmartFormat.SmartFormatter::get_Settings()
extern void SmartFormatter_get_Settings_m72BCAF6A8DF029BC22F2F6317CCD0E76C29294A3 (void);
// 0x000001BC System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::set_Settings(UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings)
extern void SmartFormatter_set_Settings_mDF0AD730BE1812A0F22644CEF6962EC900C03C70 (void);
// 0x000001BD System.String UnityEngine.Localization.SmartFormat.SmartFormatter::Format(System.String,System.Object[])
extern void SmartFormatter_Format_m43D58AE58C7CD4B00DE1F129F7D3B736B6E26352 (void);
// 0x000001BE System.String UnityEngine.Localization.SmartFormat.SmartFormatter::Format(System.IFormatProvider,System.String,System.Object[])
extern void SmartFormatter_Format_mD7818F4C7C8D70C4BCDCE12AD0BB16EF6D9AD759 (void);
// 0x000001BF System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::FormatInto(UnityEngine.Localization.SmartFormat.Core.Output.IOutput,System.String,System.Object[])
extern void SmartFormatter_FormatInto_mE206546C1414BC59AB48F599A2692E43F2E6EC86 (void);
// 0x000001C0 System.String UnityEngine.Localization.SmartFormat.SmartFormatter::FormatWithCache(UnityEngine.Localization.SmartFormat.Core.Formatting.FormatCache&,System.String,System.Object[])
extern void SmartFormatter_FormatWithCache_m9ECB0D0172914F15A2665F8298DDD760B516FB9A (void);
// 0x000001C1 System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::FormatWithCacheInto(UnityEngine.Localization.SmartFormat.Core.Formatting.FormatCache&,UnityEngine.Localization.SmartFormat.Core.Output.IOutput,System.String,System.Object[])
extern void SmartFormatter_FormatWithCacheInto_mEBB38DB761C62CB7AB3065960FDF3BB6648FF9A8 (void);
// 0x000001C2 System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::Format(UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails,UnityEngine.Localization.SmartFormat.Core.Parsing.Format,System.Object)
extern void SmartFormatter_Format_m948383FDE793AA21CE26AD131C8C2272441CF8E7 (void);
// 0x000001C3 System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::Format(UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo)
extern void SmartFormatter_Format_m95F7FE7BE6FC224DFAE66B45200E30936E9C3AEC (void);
// 0x000001C4 System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::FormatError(UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem,System.Exception,System.Int32,UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo)
extern void SmartFormatter_FormatError_mED0E6137A09124962C85E30D33ED63085E5FA3DF (void);
// 0x000001C5 System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::CheckForExtensions()
extern void SmartFormatter_CheckForExtensions_m6039A81A2C7CA763D1C633D966B4A42A0EE86F8F (void);
// 0x000001C6 System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::EvaluateSelectors(UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo)
extern void SmartFormatter_EvaluateSelectors_m7EE5356BAA4055D9734832ADEC6B93016414418E (void);
// 0x000001C7 System.Boolean UnityEngine.Localization.SmartFormat.SmartFormatter::InvokeSourceExtensions(UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo)
extern void SmartFormatter_InvokeSourceExtensions_m0A4494CD18FCD7E2D69B698F50C8AF34B3525797 (void);
// 0x000001C8 System.Void UnityEngine.Localization.SmartFormat.SmartFormatter::EvaluateFormatters(UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo)
extern void SmartFormatter_EvaluateFormatters_m988ADF4BCEF2BD1B7FDB0D39D4A53EBD26D3B61D (void);
// 0x000001C9 System.Boolean UnityEngine.Localization.SmartFormat.SmartFormatter::InvokeFormatterExtensions(UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo)
extern void SmartFormatter_InvokeFormatterExtensions_mCA55B56C369B71034FD400974F283521623B062D (void);
// 0x000001CA System.Void UnityEngine.Localization.SmartFormat.SmartFormatter_<>c::.cctor()
extern void U3CU3Ec__cctor_m8A7E682F59821B1B14E6F0C5B1717A940714699D (void);
// 0x000001CB System.Void UnityEngine.Localization.SmartFormat.SmartFormatter_<>c::.ctor()
extern void U3CU3Ec__ctor_mE56A15E0E22FE75A251FD6817999AFA415EDCB6A (void);
// 0x000001CC System.Boolean UnityEngine.Localization.SmartFormat.SmartFormatter_<>c::<GetNotEmptyFormatterExtensionNames>b__12_0(System.String)
extern void U3CU3Ec_U3CGetNotEmptyFormatterExtensionNamesU3Eb__12_0_mCA14B7E2909FDDA06CC9D4F504516E57E2D0D5A6 (void);
// 0x000001CD System.Void UnityEngine.Localization.SmartFormat.SmartObjects::.ctor()
extern void SmartObjects__ctor_m82B92502B31A13D2FF7E95BB335DDA7C76552993 (void);
// 0x000001CE System.Void UnityEngine.Localization.SmartFormat.SmartObjects::.ctor(System.Collections.Generic.IEnumerable`1<System.Object>)
extern void SmartObjects__ctor_mD13FAEDE0F8DE428DAB7125964DEC5C445BC1B42 (void);
// 0x000001CF System.Void UnityEngine.Localization.SmartFormat.SmartObjects::Add(System.Object)
extern void SmartObjects_Add_mAF45C8FA419B8841C25F61AA9FFC9B0FD8C50BF1 (void);
// 0x000001D0 System.Void UnityEngine.Localization.SmartFormat.SmartObjects::AddRange(System.Collections.Generic.IEnumerable`1<System.Object>)
extern void SmartObjects_AddRange_mCDE2A1D43A199261E239A5F83CC64C15B82B6984 (void);
// 0x000001D1 System.Void UnityEngine.Localization.SmartFormat.SmartObjects_<>c::.cctor()
extern void U3CU3Ec__cctor_m52C0804EF6D15F40ACC5088C70240498D558DBDB (void);
// 0x000001D2 System.Void UnityEngine.Localization.SmartFormat.SmartObjects_<>c::.ctor()
extern void U3CU3Ec__ctor_m2679C13017C4B2FB7A4D11C78938A50037EF4929 (void);
// 0x000001D3 System.Boolean UnityEngine.Localization.SmartFormat.SmartObjects_<>c::<AddRange>b__3_0(System.Object)
extern void U3CU3Ec_U3CAddRangeU3Eb__3_0_mB58EAA24647D396DFF41B912E215F06A01504904 (void);
// 0x000001D4 System.Void UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime::SetDateTime(System.DateTime)
extern void SystemTime_SetDateTime_mB934A8D1E39DCA853CE944C5D1EAD3276DD70AB4 (void);
// 0x000001D5 System.Void UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime::SetDateTimeOffset(System.DateTimeOffset)
extern void SystemTime_SetDateTimeOffset_mF2AA356E1D2D86806AE73C12ABA56C4D71D46A65 (void);
// 0x000001D6 System.Void UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime::ResetDateTime()
extern void SystemTime_ResetDateTime_m0EB8BACB3BC6A0AD1126877185F62F07936AC0E1 (void);
// 0x000001D7 System.Void UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime::.cctor()
extern void SystemTime__cctor_mD36E3FC7D60602597D178F97DF6E770ED36EED53 (void);
// 0x000001D8 System.Void UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime_<>c__DisplayClass1_0::.ctor()
extern void U3CU3Ec__DisplayClass1_0__ctor_m7270CF242C3E536A8125E0720847BE85DBF98298 (void);
// 0x000001D9 System.DateTime UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime_<>c__DisplayClass1_0::<SetDateTime>b__0()
extern void U3CU3Ec__DisplayClass1_0_U3CSetDateTimeU3Eb__0_mABFE1EA30C49F3BC6A933101A6482F7A94DB56B6 (void);
// 0x000001DA System.Void UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime_<>c__DisplayClass3_0::.ctor()
extern void U3CU3Ec__DisplayClass3_0__ctor_mB8A09AAE5604E5150EAC83CA31A142B124E41C8D (void);
// 0x000001DB System.DateTimeOffset UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime_<>c__DisplayClass3_0::<SetDateTimeOffset>b__0()
extern void U3CU3Ec__DisplayClass3_0_U3CSetDateTimeOffsetU3Eb__0_m5FF3D516BCD9066F75BEE0F6B340E64271954BB0 (void);
// 0x000001DC System.Void UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime_<>c::.cctor()
extern void U3CU3Ec__cctor_m5B4ECBB31CF43D2DE9641C7A620426A4DF69FF24 (void);
// 0x000001DD System.Void UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime_<>c::.ctor()
extern void U3CU3Ec__ctor_m287CAC9D675FE3A0F42878A6A712780710A50A1D (void);
// 0x000001DE System.DateTime UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime_<>c::<ResetDateTime>b__4_0()
extern void U3CU3Ec_U3CResetDateTimeU3Eb__4_0_m2ECF6D3DA83FE03CC3B0B1FF2607D7DA539266D2 (void);
// 0x000001DF System.DateTimeOffset UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime_<>c::<ResetDateTime>b__4_1()
extern void U3CU3Ec_U3CResetDateTimeU3Eb__4_1_m0832954B62DCCDE34C84FD4D010ABD0A677ED5A5 (void);
// 0x000001E0 System.DateTime UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime_<>c::<.cctor>b__5_0()
extern void U3CU3Ec_U3C_cctorU3Eb__5_0_mD6F388760D3F769FA12D167A1F79034991636F8D (void);
// 0x000001E1 System.DateTimeOffset UnityEngine.Localization.SmartFormat.Net.Utilities.SystemTime_<>c::<.cctor>b__5_1()
extern void U3CU3Ec_U3C_cctorU3Eb__5_1_mAD3E59C2401CDDE2AE2D531F5FF375D6E55E8343 (void);
// 0x000001E2 System.Void UnityEngine.Localization.SmartFormat.Utilities.FormatDelegate::.ctor(System.Func`2<System.String,System.String>)
extern void FormatDelegate__ctor_m9F63037B73F60119CDB04C40894E13FF3A233CBC (void);
// 0x000001E3 System.Void UnityEngine.Localization.SmartFormat.Utilities.FormatDelegate::.ctor(System.Func`3<System.String,System.IFormatProvider,System.String>)
extern void FormatDelegate__ctor_m45E40092A5A25091E0F5A795711273AE1F9AE3B5 (void);
// 0x000001E4 System.String UnityEngine.Localization.SmartFormat.Utilities.FormatDelegate::ToString(System.String,System.IFormatProvider)
extern void FormatDelegate_ToString_mB90BDF563A36446326DAA089BC557AF05A5D7A2C (void);
// 0x000001E5 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Singular()
extern void PluralRules_get_Singular_m2C87AE7CE3EEA0E7CD339C2BC28E5F91BCFCABE0 (void);
// 0x000001E6 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_DualOneOther()
extern void PluralRules_get_DualOneOther_mA4080DF68B812D424A49C49EFCE45383B1B5FA13 (void);
// 0x000001E7 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_DualWithZero()
extern void PluralRules_get_DualWithZero_m65BE61D2E884E5A1B2FD6708CD072683D3CB7E33 (void);
// 0x000001E8 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_DualFromZeroToTwo()
extern void PluralRules_get_DualFromZeroToTwo_m000A1CAA48D31A6193F6FD0343FCE531F9EC03A2 (void);
// 0x000001E9 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_TripleOneTwoOther()
extern void PluralRules_get_TripleOneTwoOther_m37F42181B9E4A536DD9ABB0FB8CCBF7FD04578FE (void);
// 0x000001EA UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_RussianSerboCroatian()
extern void PluralRules_get_RussianSerboCroatian_m9787101BFD3BDDB54293A152FE6B5B792C09BD88 (void);
// 0x000001EB UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Arabic()
extern void PluralRules_get_Arabic_m3A15626E7EFFB121B29D88714037CAFB1E1AD7F6 (void);
// 0x000001EC UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Breton()
extern void PluralRules_get_Breton_mF9679C09F774D2400F7F8369A2B93C94A2D41D08 (void);
// 0x000001ED UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Czech()
extern void PluralRules_get_Czech_m99DFACF1BB621E132270E29FD5ECDB30DB916DC1 (void);
// 0x000001EE UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Welsh()
extern void PluralRules_get_Welsh_m27277F52D476AC8DF846736F4172107E4BD42AE0 (void);
// 0x000001EF UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Manx()
extern void PluralRules_get_Manx_m10ECCF9D363C879709400300D9784EF7299F52A4 (void);
// 0x000001F0 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Langi()
extern void PluralRules_get_Langi_m7D1A7AE2340E86AE5732219737C981FC36123590 (void);
// 0x000001F1 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Lithuanian()
extern void PluralRules_get_Lithuanian_m279C8404FE08C4C7D9773382D36590104CA132D0 (void);
// 0x000001F2 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Latvian()
extern void PluralRules_get_Latvian_m260D30810C4A1790C59BD880E85800A0CD4CF70B (void);
// 0x000001F3 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Macedonian()
extern void PluralRules_get_Macedonian_m80F594A713B27B16148C086EBF8C4CD895DB1356 (void);
// 0x000001F4 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Moldavian()
extern void PluralRules_get_Moldavian_m290B00C70E5D3376DE14A9F699E7EBAB2080E1A0 (void);
// 0x000001F5 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Maltese()
extern void PluralRules_get_Maltese_m2C2F925193021257D6D2860FD41E8493A893E26D (void);
// 0x000001F6 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Polish()
extern void PluralRules_get_Polish_m1544CD0CDB4596F313CC6AD1F088623B4C4C0FF0 (void);
// 0x000001F7 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Romanian()
extern void PluralRules_get_Romanian_m44BE080709A688A1141C535E2AECFC3A5BC3C52B (void);
// 0x000001F8 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Tachelhit()
extern void PluralRules_get_Tachelhit_mE08A447FA6D63FFE85507D36B116A219D5A10A9B (void);
// 0x000001F9 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Slovak()
extern void PluralRules_get_Slovak_m683830BA253144EA0AC0D9CE99C35EC892206BEB (void);
// 0x000001FA UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_Slovenian()
extern void PluralRules_get_Slovenian_m61C0AFAF6A9A088F219B412B1671106ABFDF3FFF (void);
// 0x000001FB UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::get_CentralMoroccoTamazight()
extern void PluralRules_get_CentralMoroccoTamazight_mDB00959F1DC6634D7526B1E7C6BC610FD6CB6DB0 (void);
// 0x000001FC UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Utilities.PluralRules::GetPluralRule(System.String)
extern void PluralRules_GetPluralRule_mC3BCEB65C440CA6C237EB808B87A2F4C47901CBA (void);
// 0x000001FD System.Boolean UnityEngine.Localization.SmartFormat.Utilities.PluralRules::Between(System.Decimal,System.Decimal,System.Decimal)
extern void PluralRules_Between_mDA8D4100BAA0F76E4E66B929B69C66F049F4D134 (void);
// 0x000001FE System.Void UnityEngine.Localization.SmartFormat.Utilities.PluralRules::.cctor()
extern void PluralRules__cctor_mC5AB5F97400E9D7F7AB6D736E97A7C8D789A8187 (void);
// 0x000001FF System.Void UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate::.ctor(System.Object,System.IntPtr)
extern void PluralRuleDelegate__ctor_m960632B9CD2F303AEE2A59D7C2EDBEEA7A3C5D7A (void);
// 0x00000200 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate::Invoke(System.Decimal,System.Int32)
extern void PluralRuleDelegate_Invoke_m62AB59931D895D02E65BC49E1651B4ABA422D472 (void);
// 0x00000201 System.IAsyncResult UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate::BeginInvoke(System.Decimal,System.Int32,System.AsyncCallback,System.Object)
extern void PluralRuleDelegate_BeginInvoke_m4651103F8348F32E8A32F7B5B37749A0BBF5068E (void);
// 0x00000202 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate::EndInvoke(System.IAsyncResult)
extern void PluralRuleDelegate_EndInvoke_mAD5C9F291C67E38CBD76074BD68983031D57D99F (void);
// 0x00000203 System.Void UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::.cctor()
extern void U3CU3Ec__cctor_mFC466118C4817D715C94057CE1E63AD55AC940C7 (void);
// 0x00000204 System.Void UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::.ctor()
extern void U3CU3Ec__ctor_mE052DF8096EB8DCAEBD49287D2C50BB7C1560D67 (void);
// 0x00000205 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Singular>b__2_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_SingularU3Eb__2_0_m208BEFDADFBC6299983E9C9B4B7F02284F29C003 (void);
// 0x00000206 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_DualOneOther>b__4_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_DualOneOtherU3Eb__4_0_m68B6F3141AB1DE7D68454094554D8D8D82F28D8C (void);
// 0x00000207 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_DualWithZero>b__6_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_DualWithZeroU3Eb__6_0_mF8849E74374CFB28E2EA3724CC8DA70496F898F7 (void);
// 0x00000208 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_DualFromZeroToTwo>b__8_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_DualFromZeroToTwoU3Eb__8_0_m16E29988526CFFD8BC86F98B495CD51C363B520B (void);
// 0x00000209 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_TripleOneTwoOther>b__10_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_TripleOneTwoOtherU3Eb__10_0_m62C02CFC46901BEB4D2E51779514115F2C08AF6F (void);
// 0x0000020A System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_RussianSerboCroatian>b__12_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_RussianSerboCroatianU3Eb__12_0_m8FDCE6C82579C9DCB89D2EB5A482BF66F71050F0 (void);
// 0x0000020B System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Arabic>b__14_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_ArabicU3Eb__14_0_m93D2BFA80C8B9CDECE0916FB3C63D55CE3DD6C23 (void);
// 0x0000020C System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Breton>b__16_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_BretonU3Eb__16_0_m20014F34275D4EECD459186CD893372E63073A22 (void);
// 0x0000020D System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Czech>b__18_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_CzechU3Eb__18_0_m8DDEDDE1ECA953029018DE08B52041782B88D971 (void);
// 0x0000020E System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Welsh>b__20_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_WelshU3Eb__20_0_m8DC15140493F7ED0F9DBEB000D126A5C24C05A5B (void);
// 0x0000020F System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Manx>b__22_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_ManxU3Eb__22_0_m0A7B8C055C65913660BD10EF166D6D8FA4441D22 (void);
// 0x00000210 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Langi>b__24_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_LangiU3Eb__24_0_m4EF2E9558B4DAF22664A68B41AF42B739D582297 (void);
// 0x00000211 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Lithuanian>b__26_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_LithuanianU3Eb__26_0_mAA86ADC22D7DD202D28FC8729422E7F1A30508F0 (void);
// 0x00000212 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Latvian>b__28_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_LatvianU3Eb__28_0_m29D88F562038ADA6923928BC2E4913A3480AE1DE (void);
// 0x00000213 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Macedonian>b__30_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_MacedonianU3Eb__30_0_mCC639A8202526C27D25BB6615C76F72BB76D451D (void);
// 0x00000214 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Moldavian>b__32_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_MoldavianU3Eb__32_0_mF661F8F3D46CF994461C90D100FB4A171447CFE3 (void);
// 0x00000215 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Maltese>b__34_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_MalteseU3Eb__34_0_mCAE873B873A29416E38C5702FBF22952D3629296 (void);
// 0x00000216 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Polish>b__36_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_PolishU3Eb__36_0_mDA421462AC2140C41429817F693F66AD4EBF968E (void);
// 0x00000217 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Romanian>b__38_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_RomanianU3Eb__38_0_mB7112B6180A3FCE7A6C617828DACA78C6B940323 (void);
// 0x00000218 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Tachelhit>b__40_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_TachelhitU3Eb__40_0_m0614B9D2EBF7F25E1A7FD24BE3A4678FDAED1D83 (void);
// 0x00000219 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Slovak>b__42_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_SlovakU3Eb__42_0_m07B22F72A61D60B28F48784932568E6F81AE3E4C (void);
// 0x0000021A System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_Slovenian>b__44_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_SlovenianU3Eb__44_0_mABD8E5EDE6D5051B67B405BB8A34156AE0B3B2E0 (void);
// 0x0000021B System.Int32 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_<>c::<get_CentralMoroccoTamazight>b__46_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3Cget_CentralMoroccoTamazightU3Eb__46_0_mA1C1940D32B959C8CE8D208009C391CB9CA52BA0 (void);
// 0x0000021C System.String UnityEngine.Localization.SmartFormat.Utilities.TimeSpanUtility::ToTimeString(System.TimeSpan,UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions,UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo)
extern void TimeSpanUtility_ToTimeString_mACB1DA4EC9BF8575F7E9825E48F4D7243E7A1855 (void);
// 0x0000021D System.Void UnityEngine.Localization.SmartFormat.Utilities.TimeSpanUtility::.cctor()
extern void TimeSpanUtility__cctor_mEA5840220333FBCB685AC174AC931B49D7BBCD55 (void);
// 0x0000021E UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions UnityEngine.Localization.SmartFormat.Utilities.TimeSpanUtility::get_DefaultFormatOptions()
extern void TimeSpanUtility_get_DefaultFormatOptions_m583DFFDB410BEFC8EF7C4FCFB88F4564C17D2C81 (void);
// 0x0000021F System.Void UnityEngine.Localization.SmartFormat.Utilities.TimeSpanUtility::set_DefaultFormatOptions(UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions)
extern void TimeSpanUtility_set_DefaultFormatOptions_m343F7F546FDF57A4CBC192D703A344876EB730D7 (void);
// 0x00000220 UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions UnityEngine.Localization.SmartFormat.Utilities.TimeSpanUtility::get_AbsoluteDefaults()
extern void TimeSpanUtility_get_AbsoluteDefaults_m6CCA8D9B58F6DD809B3417C4EE552253B45A937A (void);
// 0x00000221 System.TimeSpan UnityEngine.Localization.SmartFormat.Utilities.TimeSpanUtility::Floor(System.TimeSpan,System.Int64)
extern void TimeSpanUtility_Floor_m874DC65104BEE0A3945EB0912942C1EE2CBB9C95 (void);
// 0x00000222 System.TimeSpan UnityEngine.Localization.SmartFormat.Utilities.TimeSpanUtility::Ceiling(System.TimeSpan,System.Int64)
extern void TimeSpanUtility_Ceiling_m776F960767EB03FE13CCB95E38873F5E8E290111 (void);
// 0x00000223 System.TimeSpan UnityEngine.Localization.SmartFormat.Utilities.TimeSpanUtility::Round(System.TimeSpan,System.Int64)
extern void TimeSpanUtility_Round_m2A0E0E647D5638CDFB1B878CFBD28AFB423A703D (void);
// 0x00000224 UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter::Merge(UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions,UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions)
extern void TimeSpanFormatOptionsConverter_Merge_m84AF3C2B0D5413ECBCD81BEC1B27DA99CC66981A (void);
// 0x00000225 UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter::Mask(UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions,UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions)
extern void TimeSpanFormatOptionsConverter_Mask_m32A86F9069A6988D402F875A7EE521DCEEA0D1BB (void);
// 0x00000226 System.Collections.Generic.IEnumerable`1<UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions> UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter::AllFlags(UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions)
extern void TimeSpanFormatOptionsConverter_AllFlags_m99688E79614FB14AF5C51EC65735262D203BC19C (void);
// 0x00000227 UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter::Parse(System.String)
extern void TimeSpanFormatOptionsConverter_Parse_mD6D2CF787BB673DEF8907335663666B068480478 (void);
// 0x00000228 System.Void UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter::.cctor()
extern void TimeSpanFormatOptionsConverter__cctor_m40404F78BDA44AC924EB344FAD9DDE5C64795654 (void);
// 0x00000229 System.Void UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter_<AllFlags>d__3::.ctor(System.Int32)
extern void U3CAllFlagsU3Ed__3__ctor_mC79E9668766055ED89139D401199B8D21FC35A47 (void);
// 0x0000022A System.Void UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter_<AllFlags>d__3::System.IDisposable.Dispose()
extern void U3CAllFlagsU3Ed__3_System_IDisposable_Dispose_m39E9DF5DCCCA61F7675F3907285F9D95EF034AD0 (void);
// 0x0000022B System.Boolean UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter_<AllFlags>d__3::MoveNext()
extern void U3CAllFlagsU3Ed__3_MoveNext_m5796B61E9B5DE077039EB3C9A0548F3582A01D40 (void);
// 0x0000022C UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter_<AllFlags>d__3::System.Collections.Generic.IEnumerator<UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions>.get_Current()
extern void U3CAllFlagsU3Ed__3_System_Collections_Generic_IEnumeratorU3CUnityEngine_Localization_SmartFormat_Utilities_TimeSpanFormatOptionsU3E_get_Current_m834F97AB6507F599EA62A558DB909BBE9D367455 (void);
// 0x0000022D System.Void UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter_<AllFlags>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAllFlagsU3Ed__3_System_Collections_IEnumerator_Reset_m23BC97A31705736A684C0498842D7A34D8824EB6 (void);
// 0x0000022E System.Object UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter_<AllFlags>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAllFlagsU3Ed__3_System_Collections_IEnumerator_get_Current_mDB1A6ECDE75EC35BF98FB63BB7B9B095A75DAC68 (void);
// 0x0000022F System.Collections.Generic.IEnumerator`1<UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions> UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter_<AllFlags>d__3::System.Collections.Generic.IEnumerable<UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions>.GetEnumerator()
extern void U3CAllFlagsU3Ed__3_System_Collections_Generic_IEnumerableU3CUnityEngine_Localization_SmartFormat_Utilities_TimeSpanFormatOptionsU3E_GetEnumerator_mE081D96C0FEA4B3FA86C7DE485BC1E82FA8977E6 (void);
// 0x00000230 System.Collections.IEnumerator UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptionsConverter_<AllFlags>d__3::System.Collections.IEnumerable.GetEnumerator()
extern void U3CAllFlagsU3Ed__3_System_Collections_IEnumerable_GetEnumerator_mABC0E82E38A0F0CECE178A3E28EB632F49DBC402 (void);
// 0x00000231 System.Void UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo::.ctor(UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate,System.String[],System.String[],System.String[],System.String[],System.String[],System.String[],System.String[],System.String[],System.String[],System.String[],System.String[],System.String[],System.String)
extern void TimeTextInfo__ctor_m2AF78F4F8E4ADB503DC107AB5C59A4955CEBCF70 (void);
// 0x00000232 System.Void UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo::.ctor(System.String,System.String,System.String,System.String,System.String,System.String,System.String)
extern void TimeTextInfo__ctor_m68329FE460CE0C26E320CD40001988B3B9469435 (void);
// 0x00000233 System.String UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo::GetValue(UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate,System.Int32,System.String[])
extern void TimeTextInfo_GetValue_m1B2FF31D9D045025A217F07D28F77CB8496C5C8C (void);
// 0x00000234 System.String UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo::GetLessThanText(System.String)
extern void TimeTextInfo_GetLessThanText_m133F833815BB2F122FA039540F7C0960A7C20F4C (void);
// 0x00000235 System.String UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo::GetUnitText(UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions,System.Int32,System.Boolean)
extern void TimeTextInfo_GetUnitText_m272075DAF0D4101A88B5DB59BB5465F91558D10E (void);
// 0x00000236 System.Void UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo_<>c::.cctor()
extern void U3CU3Ec__cctor_m5324F1287FFE01C82A7191D58ED3C2E36FFE0A7C (void);
// 0x00000237 System.Void UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo_<>c::.ctor()
extern void U3CU3Ec__ctor_mB5432B7F957A54F8EA2826AA2747CD4995C02634 (void);
// 0x00000238 System.Int32 UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo_<>c::<.ctor>b__15_0(System.Decimal,System.Int32)
extern void U3CU3Ec_U3C_ctorU3Eb__15_0_mF2768F5C27C083C8BADAF12C06A54F9F1D50AAD3 (void);
// 0x00000239 UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo UnityEngine.Localization.SmartFormat.Utilities.CommonLanguagesTimeTextInfo::get_English()
extern void CommonLanguagesTimeTextInfo_get_English_m2BE2F5E4481D58527187AB57217F6507BDD50F72 (void);
// 0x0000023A UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo UnityEngine.Localization.SmartFormat.Utilities.CommonLanguagesTimeTextInfo::GetTimeTextInfo(System.String)
extern void CommonLanguagesTimeTextInfo_GetTimeTextInfo_m160B71567CE85E2006C8E7FC28268DD00A40F384 (void);
// 0x0000023B System.Char UnityEngine.Localization.SmartFormat.Extensions.ChooseFormatter::get_SplitChar()
extern void ChooseFormatter_get_SplitChar_m597C58796422E7010C802A500015B437AD2863A1 (void);
// 0x0000023C System.Void UnityEngine.Localization.SmartFormat.Extensions.ChooseFormatter::set_SplitChar(System.Char)
extern void ChooseFormatter_set_SplitChar_m0A1A04AFB8449AF579DEA08116B3C0B779F8C039 (void);
// 0x0000023D System.Void UnityEngine.Localization.SmartFormat.Extensions.ChooseFormatter::.ctor()
extern void ChooseFormatter__ctor_m61180AC1109DD8487C83768B0B30EBC9908CB9E4 (void);
// 0x0000023E System.String[] UnityEngine.Localization.SmartFormat.Extensions.ChooseFormatter::get_DefaultNames()
extern void ChooseFormatter_get_DefaultNames_m15B828E73C6FB4A44AD2B76A887017199BDEE22E (void);
// 0x0000023F System.Boolean UnityEngine.Localization.SmartFormat.Extensions.ChooseFormatter::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void ChooseFormatter_TryEvaluateFormat_mC845B97580DC1663AE72A631A3BBBF5CDD5150CF (void);
// 0x00000240 UnityEngine.Localization.SmartFormat.Core.Parsing.Format UnityEngine.Localization.SmartFormat.Extensions.ChooseFormatter::DetermineChosenFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo,System.Collections.Generic.IList`1<UnityEngine.Localization.SmartFormat.Core.Parsing.Format>,System.String[])
extern void ChooseFormatter_DetermineChosenFormat_mEEE90DAFF2E5881C4DA63C6EE363D511BF71A187 (void);
// 0x00000241 System.Void UnityEngine.Localization.SmartFormat.Extensions.ConditionalFormatter::.ctor()
extern void ConditionalFormatter__ctor_m1E302CBFC62D164313F07A1793B7546C6A8A4DC4 (void);
// 0x00000242 System.String[] UnityEngine.Localization.SmartFormat.Extensions.ConditionalFormatter::get_DefaultNames()
extern void ConditionalFormatter_get_DefaultNames_m6D539C17C2ADDF1961B72725A0B9B0EFDCCFB408 (void);
// 0x00000243 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.ConditionalFormatter::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void ConditionalFormatter_TryEvaluateFormat_mD4CC9D315ADC389EAB09D4AA12AFB34156E493EC (void);
// 0x00000244 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.ConditionalFormatter::TryEvaluateCondition(UnityEngine.Localization.SmartFormat.Core.Parsing.Format,System.Decimal,System.Boolean&,UnityEngine.Localization.SmartFormat.Core.Parsing.Format&)
extern void ConditionalFormatter_TryEvaluateCondition_m8B2334659049E18E4951950A9692D2D5D63FE95E (void);
// 0x00000245 System.Void UnityEngine.Localization.SmartFormat.Extensions.ConditionalFormatter::.cctor()
extern void ConditionalFormatter__cctor_m929DF55169E6B40B06A827EDEBF529FEFE3E0DFC (void);
// 0x00000246 System.Void UnityEngine.Localization.SmartFormat.Extensions.DefaultFormatter::.ctor()
extern void DefaultFormatter__ctor_m3465921ED231898699627B3C8EC71CC5479891FE (void);
// 0x00000247 System.String[] UnityEngine.Localization.SmartFormat.Extensions.DefaultFormatter::get_DefaultNames()
extern void DefaultFormatter_get_DefaultNames_m62E5EF0581A031C5882DC8E80406C585C5C9CAF3 (void);
// 0x00000248 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.DefaultFormatter::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void DefaultFormatter_TryEvaluateFormat_mE5DEFD25A022A128B4859B1CC6EEA65EAE6CBAEB (void);
// 0x00000249 System.Void UnityEngine.Localization.SmartFormat.Extensions.DefaultSource::.ctor(UnityEngine.Localization.SmartFormat.SmartFormatter)
extern void DefaultSource__ctor_m6C5BA9D7268AFBD577A6C136DDA38CCAA94B6BFA (void);
// 0x0000024A System.Boolean UnityEngine.Localization.SmartFormat.Extensions.DefaultSource::TryEvaluateSelector(UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo)
extern void DefaultSource_TryEvaluateSelector_m9599CB8D758AEC4C0C57FC1A7B619C42847397EB (void);
// 0x0000024B System.Void UnityEngine.Localization.SmartFormat.Extensions.DictionarySource::.ctor(UnityEngine.Localization.SmartFormat.SmartFormatter)
extern void DictionarySource__ctor_mE5B6D073E629B96AF093440EA269C5974131790A (void);
// 0x0000024C System.Boolean UnityEngine.Localization.SmartFormat.Extensions.DictionarySource::TryEvaluateSelector(UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo)
extern void DictionarySource_TryEvaluateSelector_m0CCB7BCFB8DAE45F08AB4B1ECB05A38C0795C942 (void);
// 0x0000024D System.Void UnityEngine.Localization.SmartFormat.Extensions.DictionarySource_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m46C24DB34E58B3AE8F2C9ED1D99DB182DA0506EA (void);
// 0x0000024E System.Boolean UnityEngine.Localization.SmartFormat.Extensions.DictionarySource_<>c__DisplayClass2_0::<TryEvaluateSelector>b__0(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern void U3CU3Ec__DisplayClass2_0_U3CTryEvaluateSelectorU3Eb__0_mADBC19AA2C20886D096019E3EFDA45E382E7F8CB (void);
// 0x0000024F System.Void UnityEngine.Localization.SmartFormat.Extensions.IsMatchFormatter::.ctor()
extern void IsMatchFormatter__ctor_m0DE1F209D7BC0886C9D6CBABE32406E5D27E36FB (void);
// 0x00000250 System.String[] UnityEngine.Localization.SmartFormat.Extensions.IsMatchFormatter::get_DefaultNames()
extern void IsMatchFormatter_get_DefaultNames_mF2873B5E99BBC3A4C13E8E5C29F8E0A4FECCF78B (void);
// 0x00000251 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.IsMatchFormatter::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void IsMatchFormatter_TryEvaluateFormat_m97FD4CDDF5FF36E7E2883266F52C126BB4911D4D (void);
// 0x00000252 System.Text.RegularExpressions.RegexOptions UnityEngine.Localization.SmartFormat.Extensions.IsMatchFormatter::get_RegexOptions()
extern void IsMatchFormatter_get_RegexOptions_m72245A39C7A24F8684A916FCCBF768D598ADAB23 (void);
// 0x00000253 System.Void UnityEngine.Localization.SmartFormat.Extensions.IsMatchFormatter::set_RegexOptions(System.Text.RegularExpressions.RegexOptions)
extern void IsMatchFormatter_set_RegexOptions_m8C016FDEE6C253827A0EE2915A82F86856755C71 (void);
// 0x00000254 System.Void UnityEngine.Localization.SmartFormat.Extensions.ListFormatter::.ctor(UnityEngine.Localization.SmartFormat.SmartFormatter)
extern void ListFormatter__ctor_mF82AF58A87AC188316BB280245F3ED69CE50F888 (void);
// 0x00000255 System.String[] UnityEngine.Localization.SmartFormat.Extensions.ListFormatter::get_DefaultNames()
extern void ListFormatter_get_DefaultNames_mDB85E0438298AC4F85B51F87C333FDBBBC7244B1 (void);
// 0x00000256 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.ListFormatter::TryEvaluateSelector(UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo)
extern void ListFormatter_TryEvaluateSelector_mD1DC4858B7988ED5C07637E649B980752298997A (void);
// 0x00000257 System.Int32 UnityEngine.Localization.SmartFormat.Extensions.ListFormatter::get_CollectionIndex()
extern void ListFormatter_get_CollectionIndex_m6D91CD03F9F63578613A7B96246B20111BE6DD21 (void);
// 0x00000258 System.Void UnityEngine.Localization.SmartFormat.Extensions.ListFormatter::set_CollectionIndex(System.Int32)
extern void ListFormatter_set_CollectionIndex_m283D0E62FD77ADA2076F30FAB6084E7B23871149 (void);
// 0x00000259 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.ListFormatter::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void ListFormatter_TryEvaluateFormat_mC8FCC44429D86B96143A078A4322B91D893EE95D (void);
// 0x0000025A System.Void UnityEngine.Localization.SmartFormat.Extensions.ListFormatter::.cctor()
extern void ListFormatter__cctor_mA513715E82E8A7C95A9238533AE9A3931FC078B4 (void);
// 0x0000025B System.String UnityEngine.Localization.SmartFormat.Extensions.PluralLocalizationFormatter::get_DefaultTwoLetterISOLanguageName()
extern void PluralLocalizationFormatter_get_DefaultTwoLetterISOLanguageName_mE8CE0B61A3D10B4A4215795454C5C3E1D9C7E4C0 (void);
// 0x0000025C System.Void UnityEngine.Localization.SmartFormat.Extensions.PluralLocalizationFormatter::set_DefaultTwoLetterISOLanguageName(System.String)
extern void PluralLocalizationFormatter_set_DefaultTwoLetterISOLanguageName_mACFAC6EB51A69FE35968CD694B5FAAB635A57544 (void);
// 0x0000025D System.Void UnityEngine.Localization.SmartFormat.Extensions.PluralLocalizationFormatter::.ctor()
extern void PluralLocalizationFormatter__ctor_mED41BCB0B15719A3B061AB9965888A8A2BA76229 (void);
// 0x0000025E System.String[] UnityEngine.Localization.SmartFormat.Extensions.PluralLocalizationFormatter::get_DefaultNames()
extern void PluralLocalizationFormatter_get_DefaultNames_mB5530968E8FBEEAF501D54BFFEFF75471AD79AC4 (void);
// 0x0000025F System.Boolean UnityEngine.Localization.SmartFormat.Extensions.PluralLocalizationFormatter::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void PluralLocalizationFormatter_TryEvaluateFormat_m5AAE1BF9B535D34D8530EDDFA233427936BE57FF (void);
// 0x00000260 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Extensions.PluralLocalizationFormatter::GetPluralRule(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void PluralLocalizationFormatter_GetPluralRule_mF9C2631A53845042ABC990C61C98A075CDCF54C9 (void);
// 0x00000261 System.Void UnityEngine.Localization.SmartFormat.Extensions.CustomPluralRuleProvider::.ctor(UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate)
extern void CustomPluralRuleProvider__ctor_mB636D1BFCA9D2D567F8FFE45BE7EDB69C517C700 (void);
// 0x00000262 System.Object UnityEngine.Localization.SmartFormat.Extensions.CustomPluralRuleProvider::GetFormat(System.Type)
extern void CustomPluralRuleProvider_GetFormat_mFC579F7BF49CED3AFD42CFB04D31CF38FA249389 (void);
// 0x00000263 UnityEngine.Localization.SmartFormat.Utilities.PluralRules_PluralRuleDelegate UnityEngine.Localization.SmartFormat.Extensions.CustomPluralRuleProvider::GetPluralRule()
extern void CustomPluralRuleProvider_GetPluralRule_m70BD1E3D0C4C790CE5F7B3B76650BCDF1E648D7B (void);
// 0x00000264 System.Void UnityEngine.Localization.SmartFormat.Extensions.ReflectionSource::.ctor(UnityEngine.Localization.SmartFormat.SmartFormatter)
extern void ReflectionSource__ctor_m20F812795E558420C72F1BD33C328979F153DDE9 (void);
// 0x00000265 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.ReflectionSource::TryEvaluateSelector(UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo)
extern void ReflectionSource_TryEvaluateSelector_m36589BD871F8E52B85EBEBECA4C62B455248F392 (void);
// 0x00000266 System.Void UnityEngine.Localization.SmartFormat.Extensions.ReflectionSource_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mFEB4F0DE82E9EB1E9EE357AA71EF728E9F204F44 (void);
// 0x00000267 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.ReflectionSource_<>c__DisplayClass2_0::<TryEvaluateSelector>b__0(System.Reflection.MemberInfo)
extern void U3CU3Ec__DisplayClass2_0_U3CTryEvaluateSelectorU3Eb__0_m2F0A003767C47B32AE88D1E7E3D723AB6D1A4F3D (void);
// 0x00000268 System.Void UnityEngine.Localization.SmartFormat.Extensions.SubStringFormatter::.ctor()
extern void SubStringFormatter__ctor_m8EA75160E91CF76DF89F4EC96967C42B16591C38 (void);
// 0x00000269 System.String[] UnityEngine.Localization.SmartFormat.Extensions.SubStringFormatter::get_DefaultNames()
extern void SubStringFormatter_get_DefaultNames_m816FC604F28031812AFA787E7CAA51D5F69EB689 (void);
// 0x0000026A System.Char UnityEngine.Localization.SmartFormat.Extensions.SubStringFormatter::get_ParameterDelimiter()
extern void SubStringFormatter_get_ParameterDelimiter_m38617916B4CC10962FC85059A3FA78221357CF3C (void);
// 0x0000026B System.Void UnityEngine.Localization.SmartFormat.Extensions.SubStringFormatter::set_ParameterDelimiter(System.Char)
extern void SubStringFormatter_set_ParameterDelimiter_mF6EA22056D6AB5E1E942EBE6D9C1886F676A5F4B (void);
// 0x0000026C System.String UnityEngine.Localization.SmartFormat.Extensions.SubStringFormatter::get_NullDisplayString()
extern void SubStringFormatter_get_NullDisplayString_m037336232D62F1EF4B64CBE0BD86D4E85954718A (void);
// 0x0000026D System.Void UnityEngine.Localization.SmartFormat.Extensions.SubStringFormatter::set_NullDisplayString(System.String)
extern void SubStringFormatter_set_NullDisplayString_m851ACAF644798AF810FB08C8466B89FBAA2C0B2A (void);
// 0x0000026E System.Boolean UnityEngine.Localization.SmartFormat.Extensions.SubStringFormatter::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void SubStringFormatter_TryEvaluateFormat_m84A3B2B3750BD7023DC31906DCA2A7A4A10FEB39 (void);
// 0x0000026F System.Collections.Generic.IDictionary`2<System.String,UnityEngine.Localization.SmartFormat.Core.Parsing.Format> UnityEngine.Localization.SmartFormat.Extensions.TemplateFormatter::get_Templates()
extern void TemplateFormatter_get_Templates_m41DA7A985CE840A9CACF01538A10A1BA4E2C60B9 (void);
// 0x00000270 System.Void UnityEngine.Localization.SmartFormat.Extensions.TemplateFormatter::.ctor(UnityEngine.Localization.SmartFormat.SmartFormatter)
extern void TemplateFormatter__ctor_mC98B261F38F632511030CFB5B6ED412AF47CE528 (void);
// 0x00000271 System.String[] UnityEngine.Localization.SmartFormat.Extensions.TemplateFormatter::get_DefaultNames()
extern void TemplateFormatter_get_DefaultNames_m6671054519A1B7649F504E0A4D1710E78D69EEEA (void);
// 0x00000272 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.TemplateFormatter::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void TemplateFormatter_TryEvaluateFormat_m4BD0FD2450299F5BCC44E23FF7ED62371CCBD3B2 (void);
// 0x00000273 System.Void UnityEngine.Localization.SmartFormat.Extensions.TemplateFormatter::Register(System.String,System.String)
extern void TemplateFormatter_Register_m486F8EE85E7E1D676C4ED33A92A0C304B23C6DCC (void);
// 0x00000274 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.TemplateFormatter::Remove(System.String)
extern void TemplateFormatter_Remove_m61A48946A34817C3B2B6B0D85CC95D56C106D5C0 (void);
// 0x00000275 System.Void UnityEngine.Localization.SmartFormat.Extensions.TemplateFormatter::Clear()
extern void TemplateFormatter_Clear_m3EF3F5E09AACC5A13801523D0FEF83EA54D774BC (void);
// 0x00000276 System.Void UnityEngine.Localization.SmartFormat.Extensions.TimeFormatter::.ctor()
extern void TimeFormatter__ctor_m9B91312EB9107E6C8DA554349F7519E10B048074 (void);
// 0x00000277 System.String[] UnityEngine.Localization.SmartFormat.Extensions.TimeFormatter::get_DefaultNames()
extern void TimeFormatter_get_DefaultNames_m40907DE50FB1ED2398545EC953D44BBCD0FFF1F8 (void);
// 0x00000278 UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions UnityEngine.Localization.SmartFormat.Extensions.TimeFormatter::get_DefaultFormatOptions()
extern void TimeFormatter_get_DefaultFormatOptions_mF0E30F63EFEDC1F0AFF915C62FF97D9102B992EC (void);
// 0x00000279 System.Void UnityEngine.Localization.SmartFormat.Extensions.TimeFormatter::set_DefaultFormatOptions(UnityEngine.Localization.SmartFormat.Utilities.TimeSpanFormatOptions)
extern void TimeFormatter_set_DefaultFormatOptions_m9051239D08C4F473D3E2BB0BF391E5DA9371FBBF (void);
// 0x0000027A System.String UnityEngine.Localization.SmartFormat.Extensions.TimeFormatter::get_DefaultTwoLetterISOLanguageName()
extern void TimeFormatter_get_DefaultTwoLetterISOLanguageName_mD4B269B885C2D70DD797380C227452F6861EF34D (void);
// 0x0000027B System.Void UnityEngine.Localization.SmartFormat.Extensions.TimeFormatter::set_DefaultTwoLetterISOLanguageName(System.String)
extern void TimeFormatter_set_DefaultTwoLetterISOLanguageName_m943DA24A291F148D77DC43401389D5C01C55284E (void);
// 0x0000027C System.Boolean UnityEngine.Localization.SmartFormat.Extensions.TimeFormatter::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void TimeFormatter_TryEvaluateFormat_m8FC3B03137F3EA0CD9871778BD77CE582F6FE82E (void);
// 0x0000027D UnityEngine.Localization.SmartFormat.Utilities.TimeTextInfo UnityEngine.Localization.SmartFormat.Extensions.TimeFormatter::GetTimeTextInfo(System.IFormatProvider)
extern void TimeFormatter_GetTimeTextInfo_mFC7410C7C9264D71154970A09F2155C8AC0D1499 (void);
// 0x0000027E System.Void UnityEngine.Localization.SmartFormat.Extensions.XElementFormatter::.ctor()
extern void XElementFormatter__ctor_m71A305F5E2A1F2C66D066057ECA7E2C27CD70DE8 (void);
// 0x0000027F System.String[] UnityEngine.Localization.SmartFormat.Extensions.XElementFormatter::get_DefaultNames()
extern void XElementFormatter_get_DefaultNames_mE11F8B655BA48802C1AEE292D59DAC64DD084952 (void);
// 0x00000280 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.XElementFormatter::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void XElementFormatter_TryEvaluateFormat_m039A77B7EF3B13957E37C222899B7E19B84DFC9B (void);
// 0x00000281 System.Void UnityEngine.Localization.SmartFormat.Extensions.XmlSource::.ctor(UnityEngine.Localization.SmartFormat.SmartFormatter)
extern void XmlSource__ctor_m708C6638A0D81A0BBACDF26EB04AFFC4D074BBCC (void);
// 0x00000282 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.XmlSource::TryEvaluateSelector(UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo)
extern void XmlSource_TryEvaluateSelector_mC8BFBC736CE0AB20A762F6F4F5ABFD15800BA89B (void);
// 0x00000283 System.Void UnityEngine.Localization.SmartFormat.Extensions.XmlSource_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mE01F9F6DE9D0F57E8F22807BD3E1B3CBCA940C43 (void);
// 0x00000284 System.Boolean UnityEngine.Localization.SmartFormat.Extensions.XmlSource_<>c__DisplayClass2_0::<TryEvaluateSelector>b__0(System.Xml.Linq.XElement)
extern void U3CU3Ec__DisplayClass2_0_U3CTryEvaluateSelectorU3Eb__0_mEAB076A436F38301BC04BBDF30DAF26775ED19A5 (void);
// 0x00000285 System.Void UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings::.ctor()
extern void SmartSettings__ctor_m329FEACDC588265CBDE3CE39C4CE82E5D6D9D759 (void);
// 0x00000286 UnityEngine.Localization.SmartFormat.Core.Settings.ErrorAction UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings::get_FormatErrorAction()
extern void SmartSettings_get_FormatErrorAction_mCB5304DFF89D228B06468725590A3EAD32412D23 (void);
// 0x00000287 System.Void UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings::set_FormatErrorAction(UnityEngine.Localization.SmartFormat.Core.Settings.ErrorAction)
extern void SmartSettings_set_FormatErrorAction_mC65ECF66D66E9CBDA00D22E7324CBE1D7B720A09 (void);
// 0x00000288 UnityEngine.Localization.SmartFormat.Core.Settings.ErrorAction UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings::get_ParseErrorAction()
extern void SmartSettings_get_ParseErrorAction_mF48F8D1F90F4F5C93FE25BAB59E5824F51E7E096 (void);
// 0x00000289 System.Void UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings::set_ParseErrorAction(UnityEngine.Localization.SmartFormat.Core.Settings.ErrorAction)
extern void SmartSettings_set_ParseErrorAction_m73A32BA0A363AB97F69182BB975B17491B56EC44 (void);
// 0x0000028A UnityEngine.Localization.SmartFormat.Core.Settings.CaseSensitivityType UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings::get_CaseSensitivity()
extern void SmartSettings_get_CaseSensitivity_m2BFF138F2744EE59FEAF6B7FCE3082129799CE36 (void);
// 0x0000028B System.Void UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings::set_CaseSensitivity(UnityEngine.Localization.SmartFormat.Core.Settings.CaseSensitivityType)
extern void SmartSettings_set_CaseSensitivity_mA08ACC14CDB370EBC259168EC72613E2A7B53963 (void);
// 0x0000028C System.Boolean UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings::get_ConvertCharacterStringLiterals()
extern void SmartSettings_get_ConvertCharacterStringLiterals_m97B811F159CD0DB549B983BCA6740AB1EEDE0750 (void);
// 0x0000028D System.Void UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings::set_ConvertCharacterStringLiterals(System.Boolean)
extern void SmartSettings_set_ConvertCharacterStringLiterals_mB6E22CAC40D5CFE7B9497E6FC46FBB831D5C1FAD (void);
// 0x0000028E System.Collections.Generic.IEqualityComparer`1<System.String> UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings::GetCaseSensitivityComparer()
extern void SmartSettings_GetCaseSensitivityComparer_m92E1AC3DE57533B91D8A4364DFB7EA7C804806B1 (void);
// 0x0000028F System.StringComparison UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings::GetCaseSensitivityComparison()
extern void SmartSettings_GetCaseSensitivityComparison_m53F2D447BEABB492B224620116CA6C3E66DC50B4 (void);
// 0x00000290 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Format::.ctor(UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings,System.String)
extern void Format__ctor_m4299ED61255B18D7C6CAF9DF768C40740D817E91 (void);
// 0x00000291 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Format::.ctor(UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings,UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder,System.Int32)
extern void Format__ctor_m5DA8ADC050432CD296620717ADABC7952763E7B8 (void);
// 0x00000292 System.Collections.Generic.List`1<UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem> UnityEngine.Localization.SmartFormat.Core.Parsing.Format::get_Items()
extern void Format_get_Items_m0992D7B02BC652626810D9A186FBEC4A74DD1E20 (void);
// 0x00000293 System.Boolean UnityEngine.Localization.SmartFormat.Core.Parsing.Format::get_HasNested()
extern void Format_get_HasNested_mE2F9AF3CABE8172CC1DACB55F27D4F939C1C2786 (void);
// 0x00000294 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Format::set_HasNested(System.Boolean)
extern void Format_set_HasNested_m143C9DF0F559E6D495AB5C3A2D38B689C6FE1E60 (void);
// 0x00000295 UnityEngine.Localization.SmartFormat.Core.Parsing.Format UnityEngine.Localization.SmartFormat.Core.Parsing.Format::Substring(System.Int32)
extern void Format_Substring_mF151318B2037A0B6B9F7A9E4DC05F4BF17D230AC (void);
// 0x00000296 UnityEngine.Localization.SmartFormat.Core.Parsing.Format UnityEngine.Localization.SmartFormat.Core.Parsing.Format::Substring(System.Int32,System.Int32)
extern void Format_Substring_mE70F01D1A3314FEA6C7EBE809EBC6071255DE6D5 (void);
// 0x00000297 System.Int32 UnityEngine.Localization.SmartFormat.Core.Parsing.Format::IndexOf(System.Char)
extern void Format_IndexOf_mBA6040BF5774356A0BD0BF8F4A88D66C2A4A7254 (void);
// 0x00000298 System.Int32 UnityEngine.Localization.SmartFormat.Core.Parsing.Format::IndexOf(System.Char,System.Int32)
extern void Format_IndexOf_m5D21639B2A573FE61A6E1B9722656F8A470BE7D6 (void);
// 0x00000299 System.Collections.Generic.IList`1<System.Int32> UnityEngine.Localization.SmartFormat.Core.Parsing.Format::FindAll(System.Char)
extern void Format_FindAll_mA73792B5FE23BCEDF887F3B9F12DF1964379CB4E (void);
// 0x0000029A System.Collections.Generic.IList`1<System.Int32> UnityEngine.Localization.SmartFormat.Core.Parsing.Format::FindAll(System.Char,System.Int32)
extern void Format_FindAll_mFB7E3C1174DF351BF5CEA3DCCAFCA059C2505372 (void);
// 0x0000029B System.Collections.Generic.IList`1<UnityEngine.Localization.SmartFormat.Core.Parsing.Format> UnityEngine.Localization.SmartFormat.Core.Parsing.Format::Split(System.Char)
extern void Format_Split_m1919E700F0321FE74B484AAA8772335C6C5CD0D6 (void);
// 0x0000029C System.Collections.Generic.IList`1<UnityEngine.Localization.SmartFormat.Core.Parsing.Format> UnityEngine.Localization.SmartFormat.Core.Parsing.Format::Split(System.Char,System.Int32)
extern void Format_Split_m5AAB70255F69FB04DDD3BA9A5E01A4EF529FBFE7 (void);
// 0x0000029D System.String UnityEngine.Localization.SmartFormat.Core.Parsing.Format::GetLiteralText()
extern void Format_GetLiteralText_m490BAFDA416B166006F98738ED88DA0D0C1BC527 (void);
// 0x0000029E System.String UnityEngine.Localization.SmartFormat.Core.Parsing.Format::ToString()
extern void Format_ToString_mFB5CF793CD9A442F89B5462A82A560A03390056D (void);
// 0x0000029F System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::.ctor(UnityEngine.Localization.SmartFormat.Core.Parsing.Format,System.Collections.Generic.IList`1<System.Int32>)
extern void SplitList__ctor_m5EEB6DBA3F74C56E0430BE70551396FA55B711DD (void);
// 0x000002A0 UnityEngine.Localization.SmartFormat.Core.Parsing.Format UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::get_Item(System.Int32)
extern void SplitList_get_Item_mE3C3E8C7457E84FEFFBAE689FE6E109967F6F908 (void);
// 0x000002A1 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::set_Item(System.Int32,UnityEngine.Localization.SmartFormat.Core.Parsing.Format)
extern void SplitList_set_Item_m3F3084FC555DDF9C832C5B55FF4DB211D91BFFD8 (void);
// 0x000002A2 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::CopyTo(UnityEngine.Localization.SmartFormat.Core.Parsing.Format[],System.Int32)
extern void SplitList_CopyTo_m231F708E9DB3C399288D43C97446AECAD97FA24F (void);
// 0x000002A3 System.Int32 UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::get_Count()
extern void SplitList_get_Count_m75ACE910BABA5D175EE14E4C4BC8934FB0E8277B (void);
// 0x000002A4 System.Boolean UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::get_IsReadOnly()
extern void SplitList_get_IsReadOnly_m61BFB77E563665F2464AE75D664BCF34EF0B266B (void);
// 0x000002A5 System.Int32 UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::IndexOf(UnityEngine.Localization.SmartFormat.Core.Parsing.Format)
extern void SplitList_IndexOf_m56EF7730D832501D839E066284F3DE3CA5176DE5 (void);
// 0x000002A6 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::Insert(System.Int32,UnityEngine.Localization.SmartFormat.Core.Parsing.Format)
extern void SplitList_Insert_m17117F22DD7FBAF6CA7DB50EC8CB292E79FEFF9A (void);
// 0x000002A7 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::RemoveAt(System.Int32)
extern void SplitList_RemoveAt_m6521602D8B905DA306C6C2F2F728CCA394D99C78 (void);
// 0x000002A8 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::Add(UnityEngine.Localization.SmartFormat.Core.Parsing.Format)
extern void SplitList_Add_m8559EA9B6BAD574022AF7299A5696A58F783C55A (void);
// 0x000002A9 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::Clear()
extern void SplitList_Clear_m1527DEAAE32A2CECE0454A2DB703BAFD36F5C5B7 (void);
// 0x000002AA System.Boolean UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::Contains(UnityEngine.Localization.SmartFormat.Core.Parsing.Format)
extern void SplitList_Contains_m12F4B85CAE117A33A2135FBEE337FBB45E2FAB52 (void);
// 0x000002AB System.Boolean UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::Remove(UnityEngine.Localization.SmartFormat.Core.Parsing.Format)
extern void SplitList_Remove_m0A12DC433A469AA53386BE35ADC1D89A67A2C789 (void);
// 0x000002AC System.Collections.Generic.IEnumerator`1<UnityEngine.Localization.SmartFormat.Core.Parsing.Format> UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::GetEnumerator()
extern void SplitList_GetEnumerator_m25E5001A2A351296E47A089E3F838E4F8E141EFE (void);
// 0x000002AD System.Collections.IEnumerator UnityEngine.Localization.SmartFormat.Core.Parsing.Format_SplitList::System.Collections.IEnumerable.GetEnumerator()
extern void SplitList_System_Collections_IEnumerable_GetEnumerator_m2FB09B8079AE028BFB325658353DBD701A3A3636 (void);
// 0x000002AE System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem::.ctor(UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings,UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem,System.Int32)
extern void FormatItem__ctor_mC089E34FE24E67FF5D3B1BDA62BDD33A397D7DFB (void);
// 0x000002AF System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem::.ctor(UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings,System.String,System.Int32,System.Int32)
extern void FormatItem__ctor_m2D8613B1B4C73B46E450F1925BFD033D243A1CD7 (void);
// 0x000002B0 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem::get_RawText()
extern void FormatItem_get_RawText_m32DC9CAC468C7C782EF1102A946642B813ABF530 (void);
// 0x000002B1 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem::ToString()
extern void FormatItem_ToString_m16B9E383A7FC4CE753864A12EC63FB2FD937A4DB (void);
// 0x000002B2 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.LiteralText::.ctor(UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings,UnityEngine.Localization.SmartFormat.Core.Parsing.Format,System.Int32)
extern void LiteralText__ctor_mD5DBC69D442F979BC9C7680A4021A5C3CC225327 (void);
// 0x000002B3 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.LiteralText::.ctor(UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings,UnityEngine.Localization.SmartFormat.Core.Parsing.Format)
extern void LiteralText__ctor_m12758A6A5EC727CC30B00AE71A4DDE1CAF7DD89F (void);
// 0x000002B4 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.LiteralText::ToString()
extern void LiteralText_ToString_mBEF3EC6538A879747D87904B4284A9B25C9DA4AD (void);
// 0x000002B5 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.LiteralText::ConvertCharacterLiteralsToUnicode()
extern void LiteralText_ConvertCharacterLiteralsToUnicode_m4F85C3C36B978E2CF73C3EB1F1041AC1BA254EEA (void);
// 0x000002B6 UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::get_Settings()
extern void Parser_get_Settings_m1014D5467F507176DEF8ED285653BABEC274BE3D (void);
// 0x000002B7 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::set_Settings(UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings)
extern void Parser_set_Settings_m0A61999120A9A405B80E7E0A08B1CE274564A5D5 (void);
// 0x000002B8 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::add_OnParsingFailure(System.EventHandler`1<UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrorEventArgs>)
extern void Parser_add_OnParsingFailure_m635C88244453080AE0BA4E3D9600B647458038A4 (void);
// 0x000002B9 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::remove_OnParsingFailure(System.EventHandler`1<UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrorEventArgs>)
extern void Parser_remove_OnParsingFailure_m72B70DE79B5FB54C6390C123F2F70FD9D425BC26 (void);
// 0x000002BA System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::.ctor(UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings)
extern void Parser__ctor_mE9A01F0A2DA0CBC200CB7619FB27F9146C5D71DB (void);
// 0x000002BB System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::AddAlphanumericSelectors()
extern void Parser_AddAlphanumericSelectors_m61ED37C993D6CE573D7F795CE7FD445A1BBC5DE9 (void);
// 0x000002BC System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::AddAdditionalSelectorChars(System.String)
extern void Parser_AddAdditionalSelectorChars_mEDAC91BFD9F1373A6CA0931D0476857FAC2CA3A2 (void);
// 0x000002BD System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::AddOperators(System.String)
extern void Parser_AddOperators_mD4CF7685F2C26A5C2203B7C5256E2C74C6D5FF3D (void);
// 0x000002BE System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::UseAlternativeEscapeChar(System.Char)
extern void Parser_UseAlternativeEscapeChar_m90CE62D9E6B39EFBD24BC13B12223C20BE314C54 (void);
// 0x000002BF System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::UseBraceEscaping()
extern void Parser_UseBraceEscaping_m30D437A03BA8E0596A669F6EDB3634071D68ACEC (void);
// 0x000002C0 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::UseAlternativeBraces(System.Char,System.Char)
extern void Parser_UseAlternativeBraces_mD97D4AB2565A804100270A184E4910192C720D68 (void);
// 0x000002C1 UnityEngine.Localization.SmartFormat.Core.Parsing.Format UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::ParseFormat(System.String,System.String[])
extern void Parser_ParseFormat_m219E4CC2CE1D1C56C579FC7FA14FB86137758B24 (void);
// 0x000002C2 System.Boolean UnityEngine.Localization.SmartFormat.Core.Parsing.Parser::FormatterNameExists(System.String,System.String[])
extern void Parser_FormatterNameExists_mAC2581B6F968D75ADD19DB9A24D49DD4CA452CB1 (void);
// 0x000002C3 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser_ParsingErrorText::.ctor()
extern void ParsingErrorText__ctor_mA30B61821BF51A368A6BF647B866E9B99C52A266 (void);
// 0x000002C4 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.Parser_ParsingErrorText::get_Item(UnityEngine.Localization.SmartFormat.Core.Parsing.Parser_ParsingError)
extern void ParsingErrorText_get_Item_m19757E51DCC534FD2022CC2687457AB3C2868E17 (void);
// 0x000002C5 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Parser_<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_m8C11AAC45CCD328AD5615938CD27AFCA913BDE27 (void);
// 0x000002C6 System.Boolean UnityEngine.Localization.SmartFormat.Core.Parsing.Parser_<>c__DisplayClass23_0::<FormatterNameExists>b__0(System.String)
extern void U3CU3Ec__DisplayClass23_0_U3CFormatterNameExistsU3Eb__0_m7EE6D45807D33CECA28A9AEFABCE09084CC46809 (void);
// 0x000002C7 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrorEventArgs::.ctor(UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors,System.Boolean)
extern void ParsingErrorEventArgs__ctor_m6404DF2154B9B7B58F2AAED24FE8F47E6BFC361D (void);
// 0x000002C8 UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrorEventArgs::get_Errors()
extern void ParsingErrorEventArgs_get_Errors_mB0C455D7BEF92D40B789BD6534960D342A6254E3 (void);
// 0x000002C9 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrorEventArgs::set_Errors(UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors)
extern void ParsingErrorEventArgs_set_Errors_m6947D34F47F515E87906B524998D7B14539AC3B1 (void);
// 0x000002CA System.Boolean UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrorEventArgs::get_ThrowsException()
extern void ParsingErrorEventArgs_get_ThrowsException_m946EC48CA6159C4FC70A3F43C7BCB4A871A0D623 (void);
// 0x000002CB System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrorEventArgs::set_ThrowsException(System.Boolean)
extern void ParsingErrorEventArgs_set_ThrowsException_m7B3EA5729001A259A4D94FC698EED3508EFFADFC (void);
// 0x000002CC System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors::.ctor(UnityEngine.Localization.SmartFormat.Core.Parsing.Format)
extern void ParsingErrors__ctor_m4E717C5475500D5A9F86F3DEB7F272152576391F (void);
// 0x000002CD System.Collections.Generic.List`1<UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors_ParsingIssue> UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors::get_Issues()
extern void ParsingErrors_get_Issues_m39F3A72D22DF3BA9DFB4966A9679BD2F74564D1C (void);
// 0x000002CE System.Boolean UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors::get_HasIssues()
extern void ParsingErrors_get_HasIssues_m88D82BCDE60A9C480AAEFAB461CBAA221C84A35D (void);
// 0x000002CF System.String UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors::get_MessageShort()
extern void ParsingErrors_get_MessageShort_m4F028B65CB5B790108BCDCE5D2B6A1945AB1CB89 (void);
// 0x000002D0 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors::get_Message()
extern void ParsingErrors_get_Message_m47CB71802E44C452394D05ABF8F8319A42E88C0D (void);
// 0x000002D1 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors::AddIssue(System.String,System.Int32,System.Int32)
extern void ParsingErrors_AddIssue_m64BE92BD60351144F18FF7EC5C89E06384FA6461 (void);
// 0x000002D2 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors_ParsingIssue::.ctor(System.String,System.Int32,System.Int32)
extern void ParsingIssue__ctor_m8605CFE3F48A4B70BD084EF56CCA625EDDE80FAA (void);
// 0x000002D3 System.Int32 UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors_ParsingIssue::get_Index()
extern void ParsingIssue_get_Index_mD2BE27CD02934D38D2EF550EA1A6A67B41EF16F6 (void);
// 0x000002D4 System.Int32 UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors_ParsingIssue::get_Length()
extern void ParsingIssue_get_Length_m8CD30D6F32DA663683F4014EC2FEB3A1EE61F3EE (void);
// 0x000002D5 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors_ParsingIssue::get_Issue()
extern void ParsingIssue_get_Issue_m38D87FBEF53CC2D7E6D6CDB7D0295685BAAB5A78 (void);
// 0x000002D6 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors_<>c::.cctor()
extern void U3CU3Ec__cctor_m9743F43EFFE6F64617C60C6C86CDB0113236135D (void);
// 0x000002D7 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors_<>c::.ctor()
extern void U3CU3Ec__ctor_m5275D13FE6FD09ED6643B83A4EC37764BB8FA8B2 (void);
// 0x000002D8 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors_<>c::<get_MessageShort>b__8_0(UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors_ParsingIssue)
extern void U3CU3Ec_U3Cget_MessageShortU3Eb__8_0_m942630FD9F94FFF15815805D2A35C2D10025DDA4 (void);
// 0x000002D9 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors_<>c::<get_Message>b__10_0(UnityEngine.Localization.SmartFormat.Core.Parsing.ParsingErrors_ParsingIssue)
extern void U3CU3Ec_U3Cget_MessageU3Eb__10_0_m5A30818EABA2BF862D805798CF379101EB64A3E9 (void);
// 0x000002DA System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::.ctor(UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings,UnityEngine.Localization.SmartFormat.Core.Parsing.Format,System.Int32,System.Int32)
extern void Placeholder__ctor_m67EC3BCEC97BF560BBB3A9345D239494FEE0FF65 (void);
// 0x000002DB System.Int32 UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::get_NestedDepth()
extern void Placeholder_get_NestedDepth_mE4B8FACD85261841166228C366D04AB86F14579C (void);
// 0x000002DC System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::set_NestedDepth(System.Int32)
extern void Placeholder_set_NestedDepth_m84722D71AEFE1FB69F2DEF0AFCEBC04DEB59149F (void);
// 0x000002DD System.Collections.Generic.List`1<UnityEngine.Localization.SmartFormat.Core.Parsing.Selector> UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::get_Selectors()
extern void Placeholder_get_Selectors_m78F7DF93351F54DE087806895F72F79F986EA38F (void);
// 0x000002DE System.Int32 UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::get_Alignment()
extern void Placeholder_get_Alignment_m3E772854BBCC095C9771FB454EC99BB99B14BC32 (void);
// 0x000002DF System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::set_Alignment(System.Int32)
extern void Placeholder_set_Alignment_mF5BB752518D2655AF619AEB8DA7223CA4647B5CB (void);
// 0x000002E0 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::get_FormatterName()
extern void Placeholder_get_FormatterName_mF5D38E5E8FC87E3A4A146AB9D131FEBB56462C56 (void);
// 0x000002E1 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::set_FormatterName(System.String)
extern void Placeholder_set_FormatterName_m2C5E68F209CEFB4EE5981E43B92B94B4F6BE5454 (void);
// 0x000002E2 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::get_FormatterOptions()
extern void Placeholder_get_FormatterOptions_mDE49FDA7EB6482473B96D90208DC150573AD2163 (void);
// 0x000002E3 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::set_FormatterOptions(System.String)
extern void Placeholder_set_FormatterOptions_mD5442923FFA2BE790F7C7B2D8B801EC09DC5FAA4 (void);
// 0x000002E4 UnityEngine.Localization.SmartFormat.Core.Parsing.Format UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::get_Format()
extern void Placeholder_get_Format_mCA973EB4035382B627E69FA99CDC0705BFF70AEB (void);
// 0x000002E5 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::set_Format(UnityEngine.Localization.SmartFormat.Core.Parsing.Format)
extern void Placeholder_set_Format_mB3B13D8313232BDD2DAA389BBFF3B4B40B8B54B5 (void);
// 0x000002E6 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder::ToString()
extern void Placeholder_ToString_m620CF2F2D84B474529C24A07965CF3E5A1272754 (void);
// 0x000002E7 System.Void UnityEngine.Localization.SmartFormat.Core.Parsing.Selector::.ctor(UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings,System.String,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Selector__ctor_m56E8FED296B4E9B0F12FFDEC884D2F5422B5FEE0 (void);
// 0x000002E8 System.Int32 UnityEngine.Localization.SmartFormat.Core.Parsing.Selector::get_SelectorIndex()
extern void Selector_get_SelectorIndex_m24C1BF67DD4AC6BD5F81CBEDE4A03024673AEA6E (void);
// 0x000002E9 System.String UnityEngine.Localization.SmartFormat.Core.Parsing.Selector::get_Operator()
extern void Selector_get_Operator_mF98A9B16551276E1FFDD54F14FD666C96554CB97 (void);
// 0x000002EA System.Void UnityEngine.Localization.SmartFormat.Core.Output.IOutput::Write(System.String,UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
// 0x000002EB System.Void UnityEngine.Localization.SmartFormat.Core.Output.IOutput::Write(System.String,System.Int32,System.Int32,UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
// 0x000002EC System.Void UnityEngine.Localization.SmartFormat.Core.Output.StringOutput::.ctor()
extern void StringOutput__ctor_m87B1302FD70A5EB6CCDFA12CC73AABA9BDDBFACB (void);
// 0x000002ED System.Void UnityEngine.Localization.SmartFormat.Core.Output.StringOutput::.ctor(System.Int32)
extern void StringOutput__ctor_m54596C0CE98DA3D4682B81A064BDBD6ADB45673C (void);
// 0x000002EE System.Void UnityEngine.Localization.SmartFormat.Core.Output.StringOutput::.ctor(System.Text.StringBuilder)
extern void StringOutput__ctor_m12959DA09B70097A71EC2C96B8267AB17486FC9E (void);
// 0x000002EF System.Void UnityEngine.Localization.SmartFormat.Core.Output.StringOutput::Write(System.String,UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void StringOutput_Write_mA9F41BFAFFFF988CBF34A761914D981C3D8DC82A (void);
// 0x000002F0 System.Void UnityEngine.Localization.SmartFormat.Core.Output.StringOutput::Write(System.String,System.Int32,System.Int32,UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void StringOutput_Write_m033581CDB554C76228DDE7EC30D1AC44A8410DB0 (void);
// 0x000002F1 System.String UnityEngine.Localization.SmartFormat.Core.Output.StringOutput::ToString()
extern void StringOutput_ToString_m031F9DEA525C699F4D5E6DA0CE7E59476914B897 (void);
// 0x000002F2 System.Void UnityEngine.Localization.SmartFormat.Core.Output.TextWriterOutput::.ctor(System.IO.TextWriter)
extern void TextWriterOutput__ctor_m4BDDACCE5E317470D2D2E502F73748D0CAA51CD2 (void);
// 0x000002F3 System.IO.TextWriter UnityEngine.Localization.SmartFormat.Core.Output.TextWriterOutput::get_Output()
extern void TextWriterOutput_get_Output_mD45A38D2455C0F3D00B5A3E5B7342EC00CFF2E92 (void);
// 0x000002F4 System.Void UnityEngine.Localization.SmartFormat.Core.Output.TextWriterOutput::Write(System.String,UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void TextWriterOutput_Write_m95157623829068CCE3B71EDC31918E753014CC16 (void);
// 0x000002F5 System.Void UnityEngine.Localization.SmartFormat.Core.Output.TextWriterOutput::Write(System.String,System.Int32,System.Int32,UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
extern void TextWriterOutput_Write_m0725C57CAF0DC0C91A6728045B105DF6C8887DBA (void);
// 0x000002F6 System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormatCache::.ctor(UnityEngine.Localization.SmartFormat.Core.Parsing.Format)
extern void FormatCache__ctor_m730CDDDC9C5B5D10B8F31A37F437D762734D1034 (void);
// 0x000002F7 UnityEngine.Localization.SmartFormat.Core.Parsing.Format UnityEngine.Localization.SmartFormat.Core.Formatting.FormatCache::get_Format()
extern void FormatCache_get_Format_mAE882A959492DCDDBF659EF0B569C229F12F19C0 (void);
// 0x000002F8 System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Localization.SmartFormat.Core.Formatting.FormatCache::get_CachedObjects()
extern void FormatCache_get_CachedObjects_mE17225AF13E2E80B6CF994AD2A9B93BB5D56CE7D (void);
// 0x000002F9 System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails::.ctor(UnityEngine.Localization.SmartFormat.SmartFormatter,UnityEngine.Localization.SmartFormat.Core.Parsing.Format,System.Object[],UnityEngine.Localization.SmartFormat.Core.Formatting.FormatCache,System.IFormatProvider,UnityEngine.Localization.SmartFormat.Core.Output.IOutput)
extern void FormatDetails__ctor_m37DA5E98766C30D86BA185FCA67BD4C48304F7A3 (void);
// 0x000002FA UnityEngine.Localization.SmartFormat.SmartFormatter UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails::get_Formatter()
extern void FormatDetails_get_Formatter_m0BA8C9A471ADB6834DC357B9539CAD2CB5155065 (void);
// 0x000002FB UnityEngine.Localization.SmartFormat.Core.Parsing.Format UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails::get_OriginalFormat()
extern void FormatDetails_get_OriginalFormat_m7AE1531109BA92201591EF80DF951498766E2DC6 (void);
// 0x000002FC System.Object[] UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails::get_OriginalArgs()
extern void FormatDetails_get_OriginalArgs_mECF8AB417C2231A1E08AC81C51132D76AF87464A (void);
// 0x000002FD UnityEngine.Localization.SmartFormat.Core.Formatting.FormatCache UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails::get_FormatCache()
extern void FormatDetails_get_FormatCache_mB4F9A48972104B8174C37C42902A187E148430DA (void);
// 0x000002FE System.IFormatProvider UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails::get_Provider()
extern void FormatDetails_get_Provider_mA687395FA8CCAFFED100D1AB503F8B31DC13F93B (void);
// 0x000002FF UnityEngine.Localization.SmartFormat.Core.Output.IOutput UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails::get_Output()
extern void FormatDetails_get_Output_mCAFE0FF63635B2B2C67D100D1EA875860BDC150D (void);
// 0x00000300 UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingException UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails::get_FormattingException()
extern void FormatDetails_get_FormattingException_m513380EFD931DEF7CA507C6E84D9D8EDF4ACF728 (void);
// 0x00000301 System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails::set_FormattingException(UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingException)
extern void FormatDetails_set_FormattingException_mAC2B4B1F7E1FB137350F704A07D2A4F41319CD06 (void);
// 0x00000302 UnityEngine.Localization.SmartFormat.Core.Settings.SmartSettings UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails::get_Settings()
extern void FormatDetails_get_Settings_mB9944B35BC9D263A11C41B98626A9B3848772B66 (void);
// 0x00000303 System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingException::.ctor(UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem,System.Exception,System.Int32)
extern void FormattingException__ctor_m37BA1E033051463A749452ACC3AAB2E1975BA5BD (void);
// 0x00000304 System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingException::.ctor(UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem,System.String,System.Int32)
extern void FormattingException__ctor_mE5DBEAF25E39B5997692848C0117BB371B03A7AF (void);
// 0x00000305 System.String UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingException::get_Format()
extern void FormattingException_get_Format_mFE339C33BEF4A2A0837035445F9F39ABACC7EAA8 (void);
// 0x00000306 UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingException::get_ErrorItem()
extern void FormattingException_get_ErrorItem_mC84464B150038CE1D2890EA96A673404934AE8FB (void);
// 0x00000307 System.String UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingException::get_Issue()
extern void FormattingException_get_Issue_mF923A1A0C1506356BB4EF87FE5E40C2DDCECCB79 (void);
// 0x00000308 System.Int32 UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingException::get_Index()
extern void FormattingException_get_Index_mBEE83CEB45D7CA0AC582DD212C42D9836221256C (void);
// 0x00000309 System.String UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingException::get_Message()
extern void FormattingException_get_Message_mC57B4F2A265C31394EBD8E1E8370962B047AAADE (void);
// 0x0000030A System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::.ctor(UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails,UnityEngine.Localization.SmartFormat.Core.Parsing.Format,System.Object)
extern void FormattingInfo__ctor_m1C8F193FD1566CDD45CEB43CD9610AA5D487B089 (void);
// 0x0000030B System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::.ctor(UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo,UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails,UnityEngine.Localization.SmartFormat.Core.Parsing.Format,System.Object)
extern void FormattingInfo__ctor_mC0D692DAF9D03BAA7B89C9B854D938F68FFAB858 (void);
// 0x0000030C System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::.ctor(UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo,UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails,UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder,System.Object)
extern void FormattingInfo__ctor_mAB6C65BACD452B4C377A072AA9C040208E3290AF (void);
// 0x0000030D UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_Parent()
extern void FormattingInfo_get_Parent_mAE7DD8394040FA6E4825C5A00FEEDA3F2CE2F253 (void);
// 0x0000030E UnityEngine.Localization.SmartFormat.Core.Parsing.Selector UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_Selector()
extern void FormattingInfo_get_Selector_m8EF09755A5B5F576E417AC4D4024FDB588816E4B (void);
// 0x0000030F System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::set_Selector(UnityEngine.Localization.SmartFormat.Core.Parsing.Selector)
extern void FormattingInfo_set_Selector_mE87085F03BA56CFAF6B543A32AAD13BC043B73B7 (void);
// 0x00000310 UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_FormatDetails()
extern void FormattingInfo_get_FormatDetails_mA146969097AEBFAAED5A358DB5214C8576303C10 (void);
// 0x00000311 System.Object UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_CurrentValue()
extern void FormattingInfo_get_CurrentValue_m0C4CA3529884806EBDFAACCA14DB526870860AE4 (void);
// 0x00000312 System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::set_CurrentValue(System.Object)
extern void FormattingInfo_set_CurrentValue_m2AC459B51D0C34488D8034FCF5B4DC84A4FA9328 (void);
// 0x00000313 UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_Placeholder()
extern void FormattingInfo_get_Placeholder_mCE1E1C36774E451D671E38FBC60CC2D05AE9B660 (void);
// 0x00000314 System.Int32 UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_Alignment()
extern void FormattingInfo_get_Alignment_mDA56BF21C064B7D3CD5E44D42DB28744D4D1EE0F (void);
// 0x00000315 System.String UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_FormatterOptions()
extern void FormattingInfo_get_FormatterOptions_mBAF04070A6C23DACDBC67CDE178B4CF67E6038A2 (void);
// 0x00000316 UnityEngine.Localization.SmartFormat.Core.Parsing.Format UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_Format()
extern void FormattingInfo_get_Format_m2A120E8B3ECF342AB18CBA8B74F85A78DEAADD24 (void);
// 0x00000317 System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::Write(System.String)
extern void FormattingInfo_Write_mC26CD17C3804FC0C5F300105AF65919C64734C45 (void);
// 0x00000318 System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::Write(System.String,System.Int32,System.Int32)
extern void FormattingInfo_Write_m82D9A0343E804FD42652F8F16019540646CCC4DF (void);
// 0x00000319 System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::Write(UnityEngine.Localization.SmartFormat.Core.Parsing.Format,System.Object)
extern void FormattingInfo_Write_m2340E31A66CF35F3A9183EA5E1F7B1C107882A00 (void);
// 0x0000031A UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingException UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::FormattingException(System.String,UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem,System.Int32)
extern void FormattingInfo_FormattingException_mFE1A4DA9C904AADF6CD1A6F4095A348EFBDD138E (void);
// 0x0000031B System.String UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_SelectorText()
extern void FormattingInfo_get_SelectorText_mF90E6D1CD5EB7A9DC744EA36C6E278030498C6B4 (void);
// 0x0000031C System.Int32 UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_SelectorIndex()
extern void FormattingInfo_get_SelectorIndex_m63A49E99EB76502C57A9CA221E43B91020A131AC (void);
// 0x0000031D System.String UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_SelectorOperator()
extern void FormattingInfo_get_SelectorOperator_mDA5A95DEE408CCFFE5ABD90C6FD0793D6F7ABC50 (void);
// 0x0000031E System.Object UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::get_Result()
extern void FormattingInfo_get_Result_m71F169E02D508C9A46A46D992C8C453C3C41F7C5 (void);
// 0x0000031F System.Void UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::set_Result(System.Object)
extern void FormattingInfo_set_Result_mEA4FD28FDF0CBCC443EFC2FF3B214ACCDC9A9C66 (void);
// 0x00000320 UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::CreateChild(UnityEngine.Localization.SmartFormat.Core.Parsing.Format,System.Object)
extern void FormattingInfo_CreateChild_m5BB5EF746CDBA831EEBA28C0E7FB9ED889048EAE (void);
// 0x00000321 UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingInfo::CreateChild(UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder)
extern void FormattingInfo_CreateChild_m7EE900A7DCDDEC3F91293EFABE99D1E2EB8BDA5E (void);
// 0x00000322 System.String[] UnityEngine.Localization.SmartFormat.Core.Extensions.FormatterBase::get_Names()
extern void FormatterBase_get_Names_m32DA01F538339A417A3D0F160B0596ADC57E0659 (void);
// 0x00000323 System.Void UnityEngine.Localization.SmartFormat.Core.Extensions.FormatterBase::set_Names(System.String[])
extern void FormatterBase_set_Names_m39C8D01FB66F9744091305ED0196A8E718609271 (void);
// 0x00000324 System.String[] UnityEngine.Localization.SmartFormat.Core.Extensions.FormatterBase::get_DefaultNames()
// 0x00000325 System.Boolean UnityEngine.Localization.SmartFormat.Core.Extensions.FormatterBase::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
// 0x00000326 System.Void UnityEngine.Localization.SmartFormat.Core.Extensions.FormatterBase::OnAfterDeserialize()
extern void FormatterBase_OnAfterDeserialize_mFCF8F39B4F9FC9E54C1AD48B533355B1EF8E39B0 (void);
// 0x00000327 System.Void UnityEngine.Localization.SmartFormat.Core.Extensions.FormatterBase::OnBeforeSerialize()
extern void FormatterBase_OnBeforeSerialize_m69A492B9FF21A59617E8B8960E6FB7A73F2D3035 (void);
// 0x00000328 System.Void UnityEngine.Localization.SmartFormat.Core.Extensions.FormatterBase::.ctor()
extern void FormatterBase__ctor_m8D992BEF29DD0BE65216473DB477FE331FC1685F (void);
// 0x00000329 System.String[] UnityEngine.Localization.SmartFormat.Core.Extensions.IFormatter::get_Names()
// 0x0000032A System.Void UnityEngine.Localization.SmartFormat.Core.Extensions.IFormatter::set_Names(System.String[])
// 0x0000032B System.Boolean UnityEngine.Localization.SmartFormat.Core.Extensions.IFormatter::TryEvaluateFormat(UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo)
// 0x0000032C System.Object UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo::get_CurrentValue()
// 0x0000032D UnityEngine.Localization.SmartFormat.Core.Parsing.Format UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo::get_Format()
// 0x0000032E UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo::get_Placeholder()
// 0x0000032F System.Int32 UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo::get_Alignment()
// 0x00000330 System.String UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo::get_FormatterOptions()
// 0x00000331 UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo::get_FormatDetails()
// 0x00000332 System.Void UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo::Write(System.String)
// 0x00000333 System.Void UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo::Write(System.String,System.Int32,System.Int32)
// 0x00000334 System.Void UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo::Write(UnityEngine.Localization.SmartFormat.Core.Parsing.Format,System.Object)
// 0x00000335 UnityEngine.Localization.SmartFormat.Core.Formatting.FormattingException UnityEngine.Localization.SmartFormat.Core.Extensions.IFormattingInfo::FormattingException(System.String,UnityEngine.Localization.SmartFormat.Core.Parsing.FormatItem,System.Int32)
// 0x00000336 System.Object UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo::get_CurrentValue()
// 0x00000337 System.String UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo::get_SelectorText()
// 0x00000338 System.Int32 UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo::get_SelectorIndex()
// 0x00000339 System.String UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo::get_SelectorOperator()
// 0x0000033A System.Object UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo::get_Result()
// 0x0000033B System.Void UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo::set_Result(System.Object)
// 0x0000033C UnityEngine.Localization.SmartFormat.Core.Parsing.Placeholder UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo::get_Placeholder()
// 0x0000033D UnityEngine.Localization.SmartFormat.Core.Formatting.FormatDetails UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo::get_FormatDetails()
// 0x0000033E System.Boolean UnityEngine.Localization.SmartFormat.Core.Extensions.ISource::TryEvaluateSelector(UnityEngine.Localization.SmartFormat.Core.Extensions.ISelectorInfo)
// 0x0000033F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.Localization.Settings.LocalizedAssetDatabase::GetLocalizedAssetAsync(UnityEngine.Localization.Tables.TableEntryReference)
// 0x00000340 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.Localization.Settings.LocalizedAssetDatabase::GetLocalizedAssetAsync(UnityEngine.Localization.Tables.TableEntryReference,UnityEngine.Localization.Locale)
// 0x00000341 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.Localization.Settings.LocalizedAssetDatabase::GetLocalizedAssetAsync(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Tables.TableEntryReference)
// 0x00000342 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.Localization.Settings.LocalizedAssetDatabase::GetLocalizedAssetAsync(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Tables.TableEntryReference,UnityEngine.Localization.Locale)
// 0x00000343 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.Localization.Settings.LocalizedAssetDatabase::GetLocalizedAssetAsyncInternal(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Tables.TableEntryReference,UnityEngine.Localization.Locale)
// 0x00000344 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.Localization.Settings.LocalizedAssetDatabase::GetLocalizedAssetLoadAsset(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult<UnityEngine.Localization.Tables.AssetTable,UnityEngine.Localization.Tables.AssetTableEntry>>,UnityEngine.Localization.Tables.TableEntryReference)
// 0x00000345 System.Void UnityEngine.Localization.Settings.LocalizedAssetDatabase::OnLocaleChanged(UnityEngine.Localization.Locale)
extern void LocalizedAssetDatabase_OnLocaleChanged_mE13BD8BD2DAEA616372EE885CD5E0A968984B330 (void);
// 0x00000346 System.Void UnityEngine.Localization.Settings.LocalizedAssetDatabase::.ctor()
extern void LocalizedAssetDatabase__ctor_m5DB4A3C4C6D49D7D23803BDF4A9C1535D9CD0B16 (void);
// 0x00000347 System.Void UnityEngine.Localization.Settings.LocalizedAssetDatabase_<>c__DisplayClass2_0`1::.ctor()
// 0x00000348 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.Localization.Settings.LocalizedAssetDatabase_<>c__DisplayClass2_0`1::<GetLocalizedAssetAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Locale>)
// 0x00000349 System.Void UnityEngine.Localization.Settings.LocalizedAssetDatabase_<>c__DisplayClass4_0`1::.ctor()
// 0x0000034A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TObject> UnityEngine.Localization.Settings.LocalizedAssetDatabase_<>c__DisplayClass4_0`1::<GetLocalizedAssetAsyncInternal>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
// 0x0000034B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.Localization.Settings.LocalizedDatabase`2::get_PreloadOperation()
// 0x0000034C UnityEngine.ResourceManagement.ResourceManager UnityEngine.Localization.Settings.LocalizedDatabase`2::get_ResourceManager()
// 0x0000034D System.Collections.Generic.Dictionary`2<System.ValueTuple`2<UnityEngine.Localization.LocaleIdentifier,System.String>,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable>> UnityEngine.Localization.Settings.LocalizedDatabase`2::get_TableOperations()
// 0x0000034E System.Collections.Generic.Dictionary`2<System.Guid,UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Tables.SharedTableData>> UnityEngine.Localization.Settings.LocalizedDatabase`2::get_SharedTableDataOperations()
// 0x0000034F UnityEngine.Localization.Tables.TableReference UnityEngine.Localization.Settings.LocalizedDatabase`2::get_DefaultTable()
// 0x00000350 System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2::set_DefaultTable(UnityEngine.Localization.Tables.TableReference)
// 0x00000351 System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2::RegisterTableOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable>,UnityEngine.Localization.LocaleIdentifier,System.String)
// 0x00000352 System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2::RegisterSharedTableDataOperation(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable>)
// 0x00000353 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable> UnityEngine.Localization.Settings.LocalizedDatabase`2::GetTableAsync(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Locale)
// 0x00000354 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable> UnityEngine.Localization.Settings.LocalizedDatabase`2::GetTableAsync(UnityEngine.Localization.Tables.TableReference)
// 0x00000355 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult<TTable,TEntry>> UnityEngine.Localization.Settings.LocalizedDatabase`2::GetTableEntryAsync(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Tables.TableEntryReference,UnityEngine.Localization.Locale)
// 0x00000356 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult<TTable,TEntry>> UnityEngine.Localization.Settings.LocalizedDatabase`2::GetTableEntryAsync(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Tables.TableEntryReference)
// 0x00000357 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Tables.SharedTableData> UnityEngine.Localization.Settings.LocalizedDatabase`2::GetSharedTableData(System.Guid)
// 0x00000358 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable> UnityEngine.Localization.Settings.LocalizedDatabase`2::GetTableLoadTable(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Locale)
// 0x00000359 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable> UnityEngine.Localization.Settings.LocalizedDatabase`2::GetTableLoadTable(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Tables.SharedTableData>,UnityEngine.Localization.Locale)
// 0x0000035A UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult<TTable,TEntry>> UnityEngine.Localization.Settings.LocalizedDatabase`2::GetTableEntryFindEntry(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable>,UnityEngine.Localization.Tables.TableEntryReference)
// 0x0000035B System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2::OnLocaleChanged(UnityEngine.Localization.Locale)
// 0x0000035C System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2::.ctor()
// 0x0000035D TEntry UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult::get_Entry()
// 0x0000035E System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult::set_Entry(TEntry)
// 0x0000035F TTable UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult::get_Table()
// 0x00000360 System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult::set_Table(TTable)
// 0x00000361 System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult::.ctor(TEntry,TTable)
// 0x00000362 System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2_<>c__DisplayClass19_0::.ctor()
// 0x00000363 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable> UnityEngine.Localization.Settings.LocalizedDatabase`2_<>c__DisplayClass19_0::<GetTableAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizationSettings>)
// 0x00000364 System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2_<>c__DisplayClass20_0::.ctor()
// 0x00000365 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable> UnityEngine.Localization.Settings.LocalizedDatabase`2_<>c__DisplayClass20_0::<GetTableAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizationSettings>)
// 0x00000366 System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2_<>c__DisplayClass21_0::.ctor()
// 0x00000367 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult<TTable,TEntry>> UnityEngine.Localization.Settings.LocalizedDatabase`2_<>c__DisplayClass21_0::<GetTableEntryAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable>)
// 0x00000368 System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2_<>c__DisplayClass22_0::.ctor()
// 0x00000369 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult<TTable,TEntry>> UnityEngine.Localization.Settings.LocalizedDatabase`2_<>c__DisplayClass22_0::<GetTableEntryAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Locale>)
// 0x0000036A System.Void UnityEngine.Localization.Settings.LocalizedDatabase`2_<>c__DisplayClass24_0::.ctor()
// 0x0000036B UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<TTable> UnityEngine.Localization.Settings.LocalizedDatabase`2_<>c__DisplayClass24_0::<GetTableLoadTable>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Tables.SharedTableData>)
// 0x0000036C System.String UnityEngine.Localization.Settings.LocalizedStringDatabase::get_NoTranslationFoundFormat()
extern void LocalizedStringDatabase_get_NoTranslationFoundFormat_m2AEE5EC4131DF6176C59DDE41659459CBE8C49EE (void);
// 0x0000036D System.Void UnityEngine.Localization.Settings.LocalizedStringDatabase::set_NoTranslationFoundFormat(System.String)
extern void LocalizedStringDatabase_set_NoTranslationFoundFormat_m4C2C11BBAF680B717CF780FDA68038B150B37253 (void);
// 0x0000036E UnityEngine.Localization.SmartFormat.SmartFormatter UnityEngine.Localization.Settings.LocalizedStringDatabase::get_SmartFormatter()
extern void LocalizedStringDatabase_get_SmartFormatter_m800491DEA7C2B6E4D07E53B84CDB0C4E92A12F10 (void);
// 0x0000036F System.Void UnityEngine.Localization.Settings.LocalizedStringDatabase::set_SmartFormatter(UnityEngine.Localization.SmartFormat.SmartFormatter)
extern void LocalizedStringDatabase_set_SmartFormatter_m4EAE4A4B2B169C876FA12327806DE85A82870158 (void);
// 0x00000370 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase::GetLocalizedStringAsync(UnityEngine.Localization.Tables.TableEntryReference)
extern void LocalizedStringDatabase_GetLocalizedStringAsync_m278295DD9B4E4B0A5FD91F19B4BF3CEE1D75909E (void);
// 0x00000371 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase::GetLocalizedStringAsync(UnityEngine.Localization.Tables.TableEntryReference,UnityEngine.Localization.Locale)
extern void LocalizedStringDatabase_GetLocalizedStringAsync_m5F0B27E1B8E98317943FEEC30CA7113FC5904068 (void);
// 0x00000372 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase::GetLocalizedStringAsync(UnityEngine.Localization.Tables.TableEntryReference,System.Object[])
extern void LocalizedStringDatabase_GetLocalizedStringAsync_m87A233578CEA0CB2FB0FF7A6E37D4110798B2948 (void);
// 0x00000373 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase::GetLocalizedStringAsync(UnityEngine.Localization.Tables.TableEntryReference,UnityEngine.Localization.Locale,System.Object[])
extern void LocalizedStringDatabase_GetLocalizedStringAsync_m87C0F0141F7FE9FEBA7EDB918ECD2CB13CF1AE74 (void);
// 0x00000374 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase::GetLocalizedStringAsync(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Tables.TableEntryReference)
extern void LocalizedStringDatabase_GetLocalizedStringAsync_m8B6DE04F6C1E16647EA5350D0717E293770CF5D7 (void);
// 0x00000375 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase::GetLocalizedStringAsync(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Tables.TableEntryReference,UnityEngine.Localization.Locale)
extern void LocalizedStringDatabase_GetLocalizedStringAsync_m75B4AC71488062A45BAC728F5AB5AF98F593E222 (void);
// 0x00000376 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase::GetLocalizedStringAsync(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Tables.TableEntryReference,System.Object[])
extern void LocalizedStringDatabase_GetLocalizedStringAsync_mBAA39A741108F8DF76B040C72734FF0C7A4470BD (void);
// 0x00000377 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase::GetLocalizedStringAsync(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Tables.TableEntryReference,UnityEngine.Localization.Locale,System.Object[])
extern void LocalizedStringDatabase_GetLocalizedStringAsync_m0A7939FC200CD3725373A58DD1508023E1200E2E (void);
// 0x00000378 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase::GetLocalizedStringAsyncInternal(UnityEngine.Localization.Tables.TableReference,UnityEngine.Localization.Tables.TableEntryReference,UnityEngine.Localization.Locale,System.Object[])
extern void LocalizedStringDatabase_GetLocalizedStringAsyncInternal_m654196A83729AC22F4723A09EBC1F1A103159502 (void);
// 0x00000379 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase::GetLocalizedStringProcessTableEntry(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult<UnityEngine.Localization.Tables.StringTable,UnityEngine.Localization.Tables.StringTableEntry>>,UnityEngine.Localization.Tables.TableEntryReference,UnityEngine.Localization.Locale,System.Object[])
extern void LocalizedStringDatabase_GetLocalizedStringProcessTableEntry_mAE54BCB8BDDD4DD6B007F3F3398343FD99B87E30 (void);
// 0x0000037A System.String UnityEngine.Localization.Settings.LocalizedStringDatabase::ProcessUntranslatedText(System.String)
extern void LocalizedStringDatabase_ProcessUntranslatedText_m50A6C5D99D2E6C6D4C21C4CB53FE3A7615BA556E (void);
// 0x0000037B System.Void UnityEngine.Localization.Settings.LocalizedStringDatabase::.ctor()
extern void LocalizedStringDatabase__ctor_mD338FFC579B687B1A1FD8DB5BC38AF724D481D7A (void);
// 0x0000037C System.Void UnityEngine.Localization.Settings.LocalizedStringDatabase_<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m05DA1A826E1449033A774A303FF2D8723919AD28 (void);
// 0x0000037D UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase_<>c__DisplayClass14_0::<GetLocalizedStringAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Locale>)
extern void U3CU3Ec__DisplayClass14_0_U3CGetLocalizedStringAsyncU3Eb__0_m1FB02699BB8EF70C16F3645C3DB5C3D9B5E0E709 (void);
// 0x0000037E System.Void UnityEngine.Localization.Settings.LocalizedStringDatabase_<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m4F548A19A7071BA8C7E02BAF4321C50FEC470EE7 (void);
// 0x0000037F UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase_<>c__DisplayClass15_0::<GetLocalizedStringAsync>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizationSettings>)
extern void U3CU3Ec__DisplayClass15_0_U3CGetLocalizedStringAsyncU3Eb__0_m130106CAF6BA52909F4E29F2D1798BDC309DCB40 (void);
// 0x00000380 System.Void UnityEngine.Localization.Settings.LocalizedStringDatabase_<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_mF98AC837155DE4A463AA1E7493ADF8BD930BFAB5 (void);
// 0x00000381 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<System.String> UnityEngine.Localization.Settings.LocalizedStringDatabase_<>c__DisplayClass16_0::<GetLocalizedStringAsyncInternal>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizedDatabase`2_TableEntryResult<UnityEngine.Localization.Tables.StringTable,UnityEngine.Localization.Tables.StringTableEntry>>)
extern void U3CU3Ec__DisplayClass16_0_U3CGetLocalizedStringAsyncInternalU3Eb__0_mEB197A10B6000A1F1356563C6F10475614879E7E (void);
// 0x00000382 System.Void UnityEngine.Localization.Settings.IInitialize::PostInitialization(UnityEngine.Localization.Settings.LocalizationSettings)
// 0x00000383 System.Collections.Generic.List`1<UnityEngine.Localization.Locale> UnityEngine.Localization.Settings.ILocalesProvider::get_Locales()
// 0x00000384 UnityEngine.Localization.Locale UnityEngine.Localization.Settings.ILocalesProvider::GetLocale(UnityEngine.Localization.LocaleIdentifier)
// 0x00000385 System.Void UnityEngine.Localization.Settings.ILocalesProvider::AddLocale(UnityEngine.Localization.Locale)
// 0x00000386 System.Boolean UnityEngine.Localization.Settings.ILocalesProvider::RemoveLocale(UnityEngine.Localization.Locale)
// 0x00000387 UnityEngine.Localization.Locale UnityEngine.Localization.Settings.IStartupLocaleSelector::GetStartupLocale(UnityEngine.Localization.Settings.ILocalesProvider)
// 0x00000388 System.Collections.Generic.List`1<UnityEngine.Localization.Locale> UnityEngine.Localization.Settings.LocalesProvider::get_Locales()
extern void LocalesProvider_get_Locales_m2B83055FCFFBD61FBA7FADC0A0AFD32E87264CE1 (void);
// 0x00000389 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle UnityEngine.Localization.Settings.LocalesProvider::get_PreloadOperation()
extern void LocalesProvider_get_PreloadOperation_m9BAEC90B3E0F8547F49790951C1B01187F42E620 (void);
// 0x0000038A UnityEngine.Localization.Locale UnityEngine.Localization.Settings.LocalesProvider::GetLocale(UnityEngine.Localization.LocaleIdentifier)
extern void LocalesProvider_GetLocale_m79EED450D57DA179AF392B6C3B7302A878AE3D3A (void);
// 0x0000038B UnityEngine.Localization.Locale UnityEngine.Localization.Settings.LocalesProvider::GetLocale(System.String)
extern void LocalesProvider_GetLocale_m52FB65768A3A3FA0FAE30187F0DB1B61724CE05F (void);
// 0x0000038C UnityEngine.Localization.Locale UnityEngine.Localization.Settings.LocalesProvider::GetLocale(UnityEngine.SystemLanguage)
extern void LocalesProvider_GetLocale_m7F8C38604E727E434FFD5F47602AE65CCF9DF169 (void);
// 0x0000038D System.Void UnityEngine.Localization.Settings.LocalesProvider::AddLocale(UnityEngine.Localization.Locale)
extern void LocalesProvider_AddLocale_mDBC97B063A551A5E0CA43F2CE8266637E4A28FD8 (void);
// 0x0000038E System.Boolean UnityEngine.Localization.Settings.LocalesProvider::RemoveLocale(UnityEngine.Localization.Locale)
extern void LocalesProvider_RemoveLocale_mC94CC3CDF27F090FD5C2D08EE588145AB8E28B4E (void);
// 0x0000038F System.Void UnityEngine.Localization.Settings.LocalesProvider::.ctor()
extern void LocalesProvider__ctor_m253757B01AC7611F849117297E27211D7F5C61A8 (void);
// 0x00000390 System.Void UnityEngine.Localization.Settings.LocalizationSettings::add_OnSelectedLocaleChanged(System.Action`1<UnityEngine.Localization.Locale>)
extern void LocalizationSettings_add_OnSelectedLocaleChanged_m6F97509E7A0D79555A9F54687BCB917018AE988A (void);
// 0x00000391 System.Void UnityEngine.Localization.Settings.LocalizationSettings::remove_OnSelectedLocaleChanged(System.Action`1<UnityEngine.Localization.Locale>)
extern void LocalizationSettings_remove_OnSelectedLocaleChanged_m4DE58F84877FB5C33D8280C8AFE9978AAC8650D6 (void);
// 0x00000392 System.Boolean UnityEngine.Localization.Settings.LocalizationSettings::get_HasSettings()
extern void LocalizationSettings_get_HasSettings_mE1048773C95C4A8FCAACA00383DB10BB8FB43543 (void);
// 0x00000393 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizationSettings> UnityEngine.Localization.Settings.LocalizationSettings::get_InitializationOperation()
extern void LocalizationSettings_get_InitializationOperation_m84D4F3BDD9AFE154FACD3989F2190E76DDB95665 (void);
// 0x00000394 System.Boolean UnityEngine.Localization.Settings.LocalizationSettings::get_HasStringDatabase()
extern void LocalizationSettings_get_HasStringDatabase_m706D7DFC0052CFFCCA71A39B323D4237AA1091EF (void);
// 0x00000395 UnityEngine.Localization.Settings.LocalizationSettings UnityEngine.Localization.Settings.LocalizationSettings::get_Instance()
extern void LocalizationSettings_get_Instance_m858475AA219C1B2ADC57A991F16B58CA1F6AD097 (void);
// 0x00000396 System.Void UnityEngine.Localization.Settings.LocalizationSettings::set_Instance(UnityEngine.Localization.Settings.LocalizationSettings)
extern void LocalizationSettings_set_Instance_m40B407376EB537C12E598027AA3555A588D47B6D (void);
// 0x00000397 System.Collections.Generic.List`1<UnityEngine.Localization.Settings.IStartupLocaleSelector> UnityEngine.Localization.Settings.LocalizationSettings::get_StartupLocaleSelectors()
extern void LocalizationSettings_get_StartupLocaleSelectors_m7294CC7A7E487A26CE8860CC96FD2DC49251A286 (void);
// 0x00000398 UnityEngine.Localization.Settings.ILocalesProvider UnityEngine.Localization.Settings.LocalizationSettings::get_AvailableLocales()
extern void LocalizationSettings_get_AvailableLocales_m01933AB3F94AF4900DDC8A67E65007052706E503 (void);
// 0x00000399 System.Void UnityEngine.Localization.Settings.LocalizationSettings::set_AvailableLocales(UnityEngine.Localization.Settings.ILocalesProvider)
extern void LocalizationSettings_set_AvailableLocales_m2340D2D312A1DFE9055C8F16F43D03CF5C1F5265 (void);
// 0x0000039A UnityEngine.Localization.Settings.LocalizedAssetDatabase UnityEngine.Localization.Settings.LocalizationSettings::get_AssetDatabase()
extern void LocalizationSettings_get_AssetDatabase_m56016766FAB0617AACDED7EF26685A27D942FE44 (void);
// 0x0000039B System.Void UnityEngine.Localization.Settings.LocalizationSettings::set_AssetDatabase(UnityEngine.Localization.Settings.LocalizedAssetDatabase)
extern void LocalizationSettings_set_AssetDatabase_mAEDB65F107C7386D2A86ED40548ECEB94B328E26 (void);
// 0x0000039C UnityEngine.Localization.Settings.LocalizedStringDatabase UnityEngine.Localization.Settings.LocalizationSettings::get_StringDatabase()
extern void LocalizationSettings_get_StringDatabase_mC7CEAB5CF022465EB1FD2CE0BF5185F7B620E248 (void);
// 0x0000039D System.Void UnityEngine.Localization.Settings.LocalizationSettings::set_StringDatabase(UnityEngine.Localization.Settings.LocalizedStringDatabase)
extern void LocalizationSettings_set_StringDatabase_m630CEE1507C8D5EB3A4B4AEBE1749C1AB7160AFE (void);
// 0x0000039E UnityEngine.Localization.Locale UnityEngine.Localization.Settings.LocalizationSettings::get_SelectedLocale()
extern void LocalizationSettings_get_SelectedLocale_mDA8C08BC2F7EAC6D8F951C9ABC44AD8BA440177D (void);
// 0x0000039F System.Void UnityEngine.Localization.Settings.LocalizationSettings::set_SelectedLocale(UnityEngine.Localization.Locale)
extern void LocalizationSettings_set_SelectedLocale_mF17B5B817B8011A1BD8985BF4E65543F5204B3A3 (void);
// 0x000003A0 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Locale> UnityEngine.Localization.Settings.LocalizationSettings::get_SelectedLocaleAsync()
extern void LocalizationSettings_get_SelectedLocaleAsync_m1FAB63CF6A814224E6D66ABEF2D0131EEA29D61D (void);
// 0x000003A1 System.Void UnityEngine.Localization.Settings.LocalizationSettings::add_SelectedLocaleChanged(System.Action`1<UnityEngine.Localization.Locale>)
extern void LocalizationSettings_add_SelectedLocaleChanged_m31E7AA89EDEEC4100AE174DCF8675B18A01668E2 (void);
// 0x000003A2 System.Void UnityEngine.Localization.Settings.LocalizationSettings::remove_SelectedLocaleChanged(System.Action`1<UnityEngine.Localization.Locale>)
extern void LocalizationSettings_remove_SelectedLocaleChanged_mCD7B355A524E58C47603BCB90D17940BB092ADEA (void);
// 0x000003A3 UnityEngine.ResourceManagement.ResourceManager UnityEngine.Localization.Settings.LocalizationSettings::get_ResourceManager()
extern void LocalizationSettings_get_ResourceManager_mD14837B26976031CC31E9A5A260757C004E4ADBF (void);
// 0x000003A4 System.Void UnityEngine.Localization.Settings.LocalizationSettings::OnEnable()
extern void LocalizationSettings_OnEnable_m52953BDC7A9FDB6D2B395CCC136A939D7215ACF0 (void);
// 0x000003A5 System.Void UnityEngine.Localization.Settings.LocalizationSettings::ValidateSettingsExist(System.String)
extern void LocalizationSettings_ValidateSettingsExist_m434862921AFE7D6B3E0392623779249CA8A07A9A (void);
// 0x000003A6 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizationSettings> UnityEngine.Localization.Settings.LocalizationSettings::GetInitializationOperation()
extern void LocalizationSettings_GetInitializationOperation_m42587A6731081F943506D6BD7D5D26617226BE4B (void);
// 0x000003A7 System.Collections.Generic.List`1<UnityEngine.Localization.Settings.IStartupLocaleSelector> UnityEngine.Localization.Settings.LocalizationSettings::GetStartupLocaleSelectors()
extern void LocalizationSettings_GetStartupLocaleSelectors_m252A3D5851E1C10ACF91E66AB6B146FB11D525C1 (void);
// 0x000003A8 System.Void UnityEngine.Localization.Settings.LocalizationSettings::SetAvailableLocales(UnityEngine.Localization.Settings.ILocalesProvider)
extern void LocalizationSettings_SetAvailableLocales_mECC9C5ADFB095B8E4A069DA872C71A002286CF0A (void);
// 0x000003A9 UnityEngine.Localization.Settings.ILocalesProvider UnityEngine.Localization.Settings.LocalizationSettings::GetAvailableLocales()
extern void LocalizationSettings_GetAvailableLocales_mFA3DEA197923012CA8CB0A845121C71C91B4129F (void);
// 0x000003AA System.Void UnityEngine.Localization.Settings.LocalizationSettings::SetAssetDatabase(UnityEngine.Localization.Settings.LocalizedAssetDatabase)
extern void LocalizationSettings_SetAssetDatabase_mB391C8081DAAE79605FEB1E001F753BAAEBEDCE7 (void);
// 0x000003AB UnityEngine.Localization.Settings.LocalizedAssetDatabase UnityEngine.Localization.Settings.LocalizationSettings::GetAssetDatabase()
extern void LocalizationSettings_GetAssetDatabase_mB69C883E989C6A01DE4C62349559773B658D49A9 (void);
// 0x000003AC System.Void UnityEngine.Localization.Settings.LocalizationSettings::SetStringDatabase(UnityEngine.Localization.Settings.LocalizedStringDatabase)
extern void LocalizationSettings_SetStringDatabase_mDB2A8BB01A3D73C382F3758F6E52CE2A598FA7FE (void);
// 0x000003AD UnityEngine.Localization.Settings.LocalizedStringDatabase UnityEngine.Localization.Settings.LocalizationSettings::GetStringDatabase()
extern void LocalizationSettings_GetStringDatabase_m17A70537DC0537FC601C47D37B67626F6348C8B6 (void);
// 0x000003AE System.Void UnityEngine.Localization.Settings.LocalizationSettings::SendLocaleChangedEvents(UnityEngine.Localization.Locale)
extern void LocalizationSettings_SendLocaleChangedEvents_m5399D1AAB20EEDDA9785BB1F7548DAF360DA13C5 (void);
// 0x000003AF UnityEngine.Localization.Locale UnityEngine.Localization.Settings.LocalizationSettings::SelectLocale()
extern void LocalizationSettings_SelectLocale_m0D60AC69C8B4B8CCC9FEBCF3689E88EA25119D61 (void);
// 0x000003B0 System.Collections.Generic.List`1<UnityEngine.Localization.Settings.IInitialize> UnityEngine.Localization.Settings.LocalizationSettings::GetInitializers()
extern void LocalizationSettings_GetInitializers_mC1F1EF3DC73D9C61CFCC285440679E9F492293B7 (void);
// 0x000003B1 System.Void UnityEngine.Localization.Settings.LocalizationSettings::SetSelectedLocale(UnityEngine.Localization.Locale)
extern void LocalizationSettings_SetSelectedLocale_m051078C141B9DF1EE42C5FD9C63006E30541D069 (void);
// 0x000003B2 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Locale> UnityEngine.Localization.Settings.LocalizationSettings::GetSelectedLocaleAsync()
extern void LocalizationSettings_GetSelectedLocaleAsync_m5758BDA1BE9093161F662121BDCE2752933194D3 (void);
// 0x000003B3 UnityEngine.Localization.Locale UnityEngine.Localization.Settings.LocalizationSettings::GetSelectedLocale()
extern void LocalizationSettings_GetSelectedLocale_m33FDAFBA720F38EFE0EB1532989938F89C71D401 (void);
// 0x000003B4 System.Void UnityEngine.Localization.Settings.LocalizationSettings::OnLocaleRemoved(UnityEngine.Localization.Locale)
extern void LocalizationSettings_OnLocaleRemoved_mF723AFABD605B5CC6810119256E603A14DBF4555 (void);
// 0x000003B5 UnityEngine.Localization.Settings.LocalizationSettings UnityEngine.Localization.Settings.LocalizationSettings::GetInstanceDontCreateDefault()
extern void LocalizationSettings_GetInstanceDontCreateDefault_mA324B5DDFDD5D55BCFC076CC4BDFD8637748DDBE (void);
// 0x000003B6 UnityEngine.Localization.Settings.LocalizationSettings UnityEngine.Localization.Settings.LocalizationSettings::GetOrCreateSettings()
extern void LocalizationSettings_GetOrCreateSettings_m51DD37093BD77E2EB0526B97877B43369E48162A (void);
// 0x000003B7 System.Void UnityEngine.Localization.Settings.LocalizationSettings::.ctor()
extern void LocalizationSettings__ctor_mFCFC7B608AC4457BC174CC8CE165DBCE1C17465E (void);
// 0x000003B8 UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Locale> UnityEngine.Localization.Settings.LocalizationSettings::<GetSelectedLocaleAsync>b__59_0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle)
extern void LocalizationSettings_U3CGetSelectedLocaleAsyncU3Eb__59_0_m4BA52F7262D8C75710F4028E54F1F86911E5BF60 (void);
// 0x000003B9 System.Void UnityEngine.Localization.Settings.LocalizationSettings_<>c__DisplayClass55_0::.ctor()
extern void U3CU3Ec__DisplayClass55_0__ctor_m43552449E8F3FF450196A2FE4E01D269FBB144F4 (void);
// 0x000003BA System.Void UnityEngine.Localization.Settings.LocalizationSettings_<>c__DisplayClass55_0::<SendLocaleChangedEvents>b__0(UnityEngine.ResourceManagement.AsyncOperations.AsyncOperationHandle`1<UnityEngine.Localization.Settings.LocalizationSettings>)
extern void U3CU3Ec__DisplayClass55_0_U3CSendLocaleChangedEventsU3Eb__0_mE14232958D783ACB73EC568DF74DAA07DE419945 (void);
// 0x000003BB System.String UnityEngine.Localization.Settings.CommandLineLocaleSelector::get_CommandLineArgument()
extern void CommandLineLocaleSelector_get_CommandLineArgument_m3E26219BDFDE509098C0185BA28239BA567EEFED (void);
// 0x000003BC System.Void UnityEngine.Localization.Settings.CommandLineLocaleSelector::set_CommandLineArgument(System.String)
extern void CommandLineLocaleSelector_set_CommandLineArgument_m15D60DBAFBFE9A314EF5B384BF2D4DD334A7865E (void);
// 0x000003BD UnityEngine.Localization.Locale UnityEngine.Localization.Settings.CommandLineLocaleSelector::GetStartupLocale(UnityEngine.Localization.Settings.ILocalesProvider)
extern void CommandLineLocaleSelector_GetStartupLocale_mB9E971858855FF194A87ABCDA3709332B9180425 (void);
// 0x000003BE System.Void UnityEngine.Localization.Settings.CommandLineLocaleSelector::.ctor()
extern void CommandLineLocaleSelector__ctor_m99610A26FD2AE07132E1C64CD016FA4E55E13D53 (void);
// 0x000003BF System.String UnityEngine.Localization.Settings.PlayerPrefLocaleSelector::get_PlayerPreferenceKey()
extern void PlayerPrefLocaleSelector_get_PlayerPreferenceKey_mD59D9D12F986DAAAFC3D0F82912793B5AEF02DF7 (void);
// 0x000003C0 System.Void UnityEngine.Localization.Settings.PlayerPrefLocaleSelector::set_PlayerPreferenceKey(System.String)
extern void PlayerPrefLocaleSelector_set_PlayerPreferenceKey_mBD81A58E8764C827CF0426F8CC7C3CFA49B1E356 (void);
// 0x000003C1 System.Void UnityEngine.Localization.Settings.PlayerPrefLocaleSelector::PostInitialization(UnityEngine.Localization.Settings.LocalizationSettings)
extern void PlayerPrefLocaleSelector_PostInitialization_m22626CB98030145B2A6AC9D5F741DEDD89ED06CA (void);
// 0x000003C2 UnityEngine.Localization.Locale UnityEngine.Localization.Settings.PlayerPrefLocaleSelector::GetStartupLocale(UnityEngine.Localization.Settings.ILocalesProvider)
extern void PlayerPrefLocaleSelector_GetStartupLocale_m2B1A05943C57AD7E3529B49440CE34DCB9370987 (void);
// 0x000003C3 System.Void UnityEngine.Localization.Settings.PlayerPrefLocaleSelector::.ctor()
extern void PlayerPrefLocaleSelector__ctor_mAD37411499979DB03611DFD08864025D39B147CB (void);
// 0x000003C4 UnityEngine.Localization.LocaleIdentifier UnityEngine.Localization.Settings.SpecificLocaleSelector::get_LocaleId()
extern void SpecificLocaleSelector_get_LocaleId_m73C9A4DEAB3F9929BD89AF7F9B1DBDBF15C0A667 (void);
// 0x000003C5 System.Void UnityEngine.Localization.Settings.SpecificLocaleSelector::set_LocaleId(UnityEngine.Localization.LocaleIdentifier)
extern void SpecificLocaleSelector_set_LocaleId_mD33DA26061A3206717966E8822D4B7D5835209F3 (void);
// 0x000003C6 UnityEngine.Localization.Locale UnityEngine.Localization.Settings.SpecificLocaleSelector::GetStartupLocale(UnityEngine.Localization.Settings.ILocalesProvider)
extern void SpecificLocaleSelector_GetStartupLocale_m1F6CFACA281B08B7758FB35DE69E22C798006FD4 (void);
// 0x000003C7 System.Void UnityEngine.Localization.Settings.SpecificLocaleSelector::.ctor()
extern void SpecificLocaleSelector__ctor_m88693636FB9378E548D5703DC257ABC3A3DD6C0A (void);
// 0x000003C8 UnityEngine.Localization.Locale UnityEngine.Localization.Settings.SystemLocaleSelector::GetStartupLocale(UnityEngine.Localization.Settings.ILocalesProvider)
extern void SystemLocaleSelector_GetStartupLocale_m856C9D33DA91A31AC13734F8082002D123D3F6B0 (void);
// 0x000003C9 System.Globalization.CultureInfo UnityEngine.Localization.Settings.SystemLocaleSelector::GetSystemCulture()
extern void SystemLocaleSelector_GetSystemCulture_m00C0E730A4AB2A00DFB07C445B37CFF4FA2E3BD6 (void);
// 0x000003CA UnityEngine.SystemLanguage UnityEngine.Localization.Settings.SystemLocaleSelector::GetApplicationSystemLanguage()
extern void SystemLocaleSelector_GetApplicationSystemLanguage_m62641134AC3D5E70948392B86BF827EC37F8E3BD (void);
// 0x000003CB System.Void UnityEngine.Localization.Settings.SystemLocaleSelector::.ctor()
extern void SystemLocaleSelector__ctor_m83E108B4FBEEFD5655F5E1007AC56816DA763C9F (void);
// 0x000003CC System.Void UnityEngine.Localization.Pseudo.IPseudoLocalizationMethod::Transform(UnityEngine.Localization.Pseudo.Message)
// 0x000003CD System.Int32 UnityEngine.Localization.Pseudo.MessageFragment::get_Length()
extern void MessageFragment_get_Length_mA0712812B1C6AA17F7253C1E0FE439BD73106349 (void);
// 0x000003CE UnityEngine.Localization.Pseudo.Message UnityEngine.Localization.Pseudo.MessageFragment::get_Message()
extern void MessageFragment_get_Message_mED3AD18B54B7D7A248E02F4418D2C36B151AF12E (void);
// 0x000003CF System.Void UnityEngine.Localization.Pseudo.MessageFragment::set_Message(UnityEngine.Localization.Pseudo.Message)
extern void MessageFragment_set_Message_m11FDC80D4E1742181F69C50083B239D339EDD8A9 (void);
// 0x000003D0 System.Void UnityEngine.Localization.Pseudo.MessageFragment::Initialize(UnityEngine.Localization.Pseudo.Message,System.String,System.Int32,System.Int32)
extern void MessageFragment_Initialize_m9368F1A5226AB212CF375F3C24D7F55A845F87F9 (void);
// 0x000003D1 System.Void UnityEngine.Localization.Pseudo.MessageFragment::Initialize(UnityEngine.Localization.Pseudo.Message,System.String)
extern void MessageFragment_Initialize_m5C3A405B236080DE6739652D3896D4A1A25D3876 (void);
// 0x000003D2 UnityEngine.Localization.Pseudo.WritableMessageFragment UnityEngine.Localization.Pseudo.MessageFragment::CreateTextFragment(System.Int32,System.Int32)
extern void MessageFragment_CreateTextFragment_mFC4F48943006B07C125D6FE2AECD8A5D55172828 (void);
// 0x000003D3 UnityEngine.Localization.Pseudo.ReadOnlyMessageFragment UnityEngine.Localization.Pseudo.MessageFragment::CreateReadonlyTextFragment(System.Int32,System.Int32)
extern void MessageFragment_CreateReadonlyTextFragment_m51F414632A02C697BCDF274A7A5C38D9E4835E7D (void);
// 0x000003D4 System.String UnityEngine.Localization.Pseudo.MessageFragment::ToString()
extern void MessageFragment_ToString_mE0D856891856DAADC0CC277FF66264A80042E0D8 (void);
// 0x000003D5 System.Void UnityEngine.Localization.Pseudo.MessageFragment::BuildString(System.Text.StringBuilder)
extern void MessageFragment_BuildString_mC2098EBF795A5C82C577D1C898BED2B9A79A0DBD (void);
// 0x000003D6 System.Char UnityEngine.Localization.Pseudo.MessageFragment::get_Item(System.Int32)
extern void MessageFragment_get_Item_mAE485CA791EE256217A8B01ADA40A306AAB65B82 (void);
// 0x000003D7 System.Void UnityEngine.Localization.Pseudo.MessageFragment::.ctor()
extern void MessageFragment__ctor_mB9EEBFB7669574F7E4EB186767853974F2A07F52 (void);
// 0x000003D8 System.String UnityEngine.Localization.Pseudo.WritableMessageFragment::get_Text()
extern void WritableMessageFragment_get_Text_m48073076F93C18875CF7ADEE0E5BD48CEF5EAEEC (void);
// 0x000003D9 System.Void UnityEngine.Localization.Pseudo.WritableMessageFragment::set_Text(System.String)
extern void WritableMessageFragment_set_Text_mA4FF6E78F43575475C88DBA01F94C5028B136D5C (void);
// 0x000003DA System.Void UnityEngine.Localization.Pseudo.WritableMessageFragment::.ctor()
extern void WritableMessageFragment__ctor_m910629A438880957CBC6C445A0B7DDBCF501665E (void);
// 0x000003DB System.String UnityEngine.Localization.Pseudo.ReadOnlyMessageFragment::get_Text()
extern void ReadOnlyMessageFragment_get_Text_mC2F0DEE10ADCDA6E70C66995587D46D55ED44C24 (void);
// 0x000003DC System.Void UnityEngine.Localization.Pseudo.ReadOnlyMessageFragment::.ctor()
extern void ReadOnlyMessageFragment__ctor_mD9E860EB17680044881BC4496293591F51B50FAB (void);
// 0x000003DD System.String UnityEngine.Localization.Pseudo.Message::get_Original()
extern void Message_get_Original_m76E16F0A97ADDF18457C66DCC044A1DC9D37D84D (void);
// 0x000003DE System.Void UnityEngine.Localization.Pseudo.Message::set_Original(System.String)
extern void Message_set_Original_mD2A73245F4F2540846DAA185DAA748A7D3A21C9F (void);
// 0x000003DF System.Collections.Generic.List`1<UnityEngine.Localization.Pseudo.MessageFragment> UnityEngine.Localization.Pseudo.Message::get_Fragments()
extern void Message_get_Fragments_mA852822AC6E656104C14BF7CF381ABBC3A1783A5 (void);
// 0x000003E0 System.Void UnityEngine.Localization.Pseudo.Message::set_Fragments(System.Collections.Generic.List`1<UnityEngine.Localization.Pseudo.MessageFragment>)
extern void Message_set_Fragments_m6B0383E813998D1D8086133CA9D85161DA918D3C (void);
// 0x000003E1 System.Int32 UnityEngine.Localization.Pseudo.Message::get_Length()
extern void Message_get_Length_m468559F45EA2ACD931604C58EA89D8C2B33E414E (void);
// 0x000003E2 UnityEngine.Localization.Pseudo.WritableMessageFragment UnityEngine.Localization.Pseudo.Message::CreateTextFragment(System.String,System.Int32,System.Int32)
extern void Message_CreateTextFragment_mA0F87CAA724E5231DA2BA0C1D6521304AA545C5A (void);
// 0x000003E3 UnityEngine.Localization.Pseudo.WritableMessageFragment UnityEngine.Localization.Pseudo.Message::CreateTextFragment(System.String)
extern void Message_CreateTextFragment_m3AE111031A505C5E31183A953685453DA4E0108C (void);
// 0x000003E4 UnityEngine.Localization.Pseudo.ReadOnlyMessageFragment UnityEngine.Localization.Pseudo.Message::CreateReadonlyTextFragment(System.String,System.Int32,System.Int32)
extern void Message_CreateReadonlyTextFragment_mF6ACDE6924B87A21044ABA8DA1B2B2213686F02D (void);
// 0x000003E5 UnityEngine.Localization.Pseudo.ReadOnlyMessageFragment UnityEngine.Localization.Pseudo.Message::CreateReadonlyTextFragment(System.String)
extern void Message_CreateReadonlyTextFragment_mFACBE5411DE45804693DC2326A84A79076F87C97 (void);
// 0x000003E6 System.Void UnityEngine.Localization.Pseudo.Message::ReplaceFragment(UnityEngine.Localization.Pseudo.MessageFragment,UnityEngine.Localization.Pseudo.MessageFragment)
extern void Message_ReplaceFragment_mE93AB7917B9F1197DB03F3E26CC4A48143589A04 (void);
// 0x000003E7 System.Void UnityEngine.Localization.Pseudo.Message::ReleaseFragment(UnityEngine.Localization.Pseudo.MessageFragment)
extern void Message_ReleaseFragment_m24A5B47A79AAD91E811AA0EFA276E1DABC3C6E9C (void);
// 0x000003E8 UnityEngine.Localization.Pseudo.Message UnityEngine.Localization.Pseudo.Message::CreateMessage(System.String)
extern void Message_CreateMessage_m58146A1A00B602B645D382B2B02C6C765BCDDF63 (void);
// 0x000003E9 System.Void UnityEngine.Localization.Pseudo.Message::Release()
extern void Message_Release_m7AB8C06D07378C6E3D2DFE5467372F0D8D9A4B45 (void);
// 0x000003EA System.String UnityEngine.Localization.Pseudo.Message::ToString()
extern void Message_ToString_m5B991ECFD10578467BF9164B0DB522213BB7595D (void);
// 0x000003EB System.Void UnityEngine.Localization.Pseudo.Message::.ctor()
extern void Message__ctor_m17592F5E27715634D4FE830F2AB10B616630FD12 (void);
// 0x000003EC System.Void UnityEngine.Localization.Pseudo.Accenter::.ctor()
extern void Accenter__ctor_m31DAC864F1DFE1F753E87F92286A0F623E0A00A1 (void);
// 0x000003ED System.Void UnityEngine.Localization.Pseudo.Accenter::AddDefaults()
extern void Accenter_AddDefaults_mC74F3FFB1513D4DD4583377AF5B610FC1263DA70 (void);
// 0x000003EE UnityEngine.Localization.Pseudo.CharacterSubstitutor_SubstitutionMethod UnityEngine.Localization.Pseudo.CharacterSubstitutor::get_Method()
extern void CharacterSubstitutor_get_Method_m240B59532A960E0C2D9B700F28D5A4F18E516EED (void);
// 0x000003EF System.Void UnityEngine.Localization.Pseudo.CharacterSubstitutor::set_Method(UnityEngine.Localization.Pseudo.CharacterSubstitutor_SubstitutionMethod)
extern void CharacterSubstitutor_set_Method_mA2AB243B5A890AD2211BA987B5FFDE0E0111811B (void);
// 0x000003F0 System.Collections.Generic.Dictionary`2<System.Char,System.Char> UnityEngine.Localization.Pseudo.CharacterSubstitutor::get_ReplacementMap()
extern void CharacterSubstitutor_get_ReplacementMap_m4A8ED9408BADE6C785B714492F370368D193F696 (void);
// 0x000003F1 System.Void UnityEngine.Localization.Pseudo.CharacterSubstitutor::set_ReplacementMap(System.Collections.Generic.Dictionary`2<System.Char,System.Char>)
extern void CharacterSubstitutor_set_ReplacementMap_m91B8713CCDA1C7926BC536BC32F057F5465286B2 (void);
// 0x000003F2 UnityEngine.Localization.Pseudo.CharacterSubstitutor_ListSelectionMethod UnityEngine.Localization.Pseudo.CharacterSubstitutor::get_ListMode()
extern void CharacterSubstitutor_get_ListMode_m4B8C99812D1D17351B4A1C015D768E1F93D0FA84 (void);
// 0x000003F3 System.Void UnityEngine.Localization.Pseudo.CharacterSubstitutor::set_ListMode(UnityEngine.Localization.Pseudo.CharacterSubstitutor_ListSelectionMethod)
extern void CharacterSubstitutor_set_ListMode_mA137C8C35C2A8D52AAA72BC71425B63004F6DE56 (void);
// 0x000003F4 System.Collections.Generic.List`1<System.Char> UnityEngine.Localization.Pseudo.CharacterSubstitutor::get_ReplacementList()
extern void CharacterSubstitutor_get_ReplacementList_mF8C831F88BC6AC341BA447D34C2766863AC2DB66 (void);
// 0x000003F5 System.Int32 UnityEngine.Localization.Pseudo.CharacterSubstitutor::GetRandomSeed(System.String)
extern void CharacterSubstitutor_GetRandomSeed_mA50EAB688D0B5BED23B19808E51E2CA183775E83 (void);
// 0x000003F6 System.Char UnityEngine.Localization.Pseudo.CharacterSubstitutor::ReplaceCharFromMap(System.Char)
extern void CharacterSubstitutor_ReplaceCharFromMap_m80FC722B11715347048AEA2E6F400D562F0657E2 (void);
// 0x000003F7 System.Void UnityEngine.Localization.Pseudo.CharacterSubstitutor::OnBeforeSerialize()
extern void CharacterSubstitutor_OnBeforeSerialize_m6949A0533EB56FAB544433175CDC59364642E290 (void);
// 0x000003F8 System.Void UnityEngine.Localization.Pseudo.CharacterSubstitutor::OnAfterDeserialize()
extern void CharacterSubstitutor_OnAfterDeserialize_m08FB7880883678D435CD3DB055B701EC8CB1B136 (void);
// 0x000003F9 System.Void UnityEngine.Localization.Pseudo.CharacterSubstitutor::TransformFragment(UnityEngine.Localization.Pseudo.WritableMessageFragment)
extern void CharacterSubstitutor_TransformFragment_mDFF31C2990990C7889B2523C9B41B35904FCFEF9 (void);
// 0x000003FA System.Void UnityEngine.Localization.Pseudo.CharacterSubstitutor::Transform(UnityEngine.Localization.Pseudo.Message)
extern void CharacterSubstitutor_Transform_mD48FE056F5A96C6D2F54A29F3C3CAA1B40FDD122 (void);
// 0x000003FB System.Void UnityEngine.Localization.Pseudo.CharacterSubstitutor::.ctor()
extern void CharacterSubstitutor__ctor_m604B0271FFC7D5CD9E2E91B3D9A74905D4159200 (void);
// 0x000003FC System.String UnityEngine.Localization.Pseudo.Encapsulator::get_Start()
extern void Encapsulator_get_Start_m44A71CA66598891BC7466D3EA54131993D37796B (void);
// 0x000003FD System.Void UnityEngine.Localization.Pseudo.Encapsulator::set_Start(System.String)
extern void Encapsulator_set_Start_m40081476BCBAA60FE5F5D5F29023E8EFC344C2C9 (void);
// 0x000003FE System.String UnityEngine.Localization.Pseudo.Encapsulator::get_End()
extern void Encapsulator_get_End_mB609C22B5A91AEC3CF25453505A975C56381F113 (void);
// 0x000003FF System.Void UnityEngine.Localization.Pseudo.Encapsulator::set_End(System.String)
extern void Encapsulator_set_End_mF6708825B850050910D0C36160FB3B44AC20BECE (void);
// 0x00000400 System.Void UnityEngine.Localization.Pseudo.Encapsulator::Transform(UnityEngine.Localization.Pseudo.Message)
extern void Encapsulator_Transform_m082D3F700080F3ABC4A14C17709BB92C7EE52990 (void);
// 0x00000401 System.Void UnityEngine.Localization.Pseudo.Encapsulator::.ctor()
extern void Encapsulator__ctor_m54A56A851E7C1CB8CAE53BFE60090C2C58EF1FC7 (void);
// 0x00000402 System.Collections.Generic.List`1<UnityEngine.Localization.Pseudo.Expander_ExpansionRule> UnityEngine.Localization.Pseudo.Expander::get_ExpansionRules()
extern void Expander_get_ExpansionRules_mCE133E517F62580A555EE90ED7B39DABABF185E4 (void);
// 0x00000403 UnityEngine.Localization.Pseudo.Expander_InsertLocation UnityEngine.Localization.Pseudo.Expander::get_Location()
extern void Expander_get_Location_m0717B9ACE2A13501C6464539051CC8343AA066AD (void);
// 0x00000404 System.Void UnityEngine.Localization.Pseudo.Expander::set_Location(UnityEngine.Localization.Pseudo.Expander_InsertLocation)
extern void Expander_set_Location_mF835FE8501562E5E676D68B506D1DD833F231DA8 (void);
// 0x00000405 System.Collections.Generic.List`1<System.Char> UnityEngine.Localization.Pseudo.Expander::get_PaddingCharacters()
extern void Expander_get_PaddingCharacters_m0F6BD459FA5415C54CE003B07EDE758327B53B92 (void);
// 0x00000406 System.Int32 UnityEngine.Localization.Pseudo.Expander::get_MinimumStringLength()
extern void Expander_get_MinimumStringLength_m2E1302AAAA53684685747203B2A25A7090C7937F (void);
// 0x00000407 System.Void UnityEngine.Localization.Pseudo.Expander::set_MinimumStringLength(System.Int32)
extern void Expander_set_MinimumStringLength_mC752354855300F77DE9967B15167CB1089132400 (void);
// 0x00000408 System.Void UnityEngine.Localization.Pseudo.Expander::.ctor()
extern void Expander__ctor_m1FA3C911F596FF95E2D78222B63D4ADF375C20B7 (void);
// 0x00000409 System.Void UnityEngine.Localization.Pseudo.Expander::.ctor(System.Char)
extern void Expander__ctor_m1EC30009C9A352E0FC04D770403D65AEFF28563C (void);
// 0x0000040A System.Void UnityEngine.Localization.Pseudo.Expander::.ctor(System.Char,System.Char)
extern void Expander__ctor_m72DB21F5208DD55F40ABF96132391A99ABCDDEC2 (void);
// 0x0000040B System.Void UnityEngine.Localization.Pseudo.Expander::AddCharacterRange(System.Char,System.Char)
extern void Expander_AddCharacterRange_m34E1DBE2AC55E816358CF8E9FA176083C97A929D (void);
// 0x0000040C System.Void UnityEngine.Localization.Pseudo.Expander::SetConstantExpansion(System.Single)
extern void Expander_SetConstantExpansion_m0DBFC020A9D6DD7D9264BCA7D6882B090937E509 (void);
// 0x0000040D System.Void UnityEngine.Localization.Pseudo.Expander::AddExpansionRule(System.Int32,System.Int32,System.Single)
extern void Expander_AddExpansionRule_m00C7E1FB03E7E61D5AF83ADAAC8700566059CBE3 (void);
// 0x0000040E System.Single UnityEngine.Localization.Pseudo.Expander::GetExpansionForLength(System.Int32)
extern void Expander_GetExpansionForLength_mC14AF73F519D93E62016A59C69148978187A5CA7 (void);
// 0x0000040F System.Void UnityEngine.Localization.Pseudo.Expander::Transform(UnityEngine.Localization.Pseudo.Message)
extern void Expander_Transform_mDCE4E8517118D6D75394A6EB3F7B21490C28F415 (void);
// 0x00000410 System.Void UnityEngine.Localization.Pseudo.Expander::AddPaddingToMessage(UnityEngine.Localization.Pseudo.Message,System.Char[])
extern void Expander_AddPaddingToMessage_m9285A81B7A29A347BB7AF745925198FC65E535BD (void);
// 0x00000411 System.Int32 UnityEngine.Localization.Pseudo.Expander::GetRandomSeed(System.String)
extern void Expander_GetRandomSeed_mFD72023935458DF91BEEB64E940F7A5BC990ECB9 (void);
// 0x00000412 System.Int32 UnityEngine.Localization.Pseudo.Expander_ExpansionRule::get_MinCharacters()
extern void ExpansionRule_get_MinCharacters_m6D36BF2C0FD5540EE6723FB3AEA5C825A829AF01_AdjustorThunk (void);
// 0x00000413 System.Void UnityEngine.Localization.Pseudo.Expander_ExpansionRule::set_MinCharacters(System.Int32)
extern void ExpansionRule_set_MinCharacters_m1CEBE64E9D97578F14B87AE49184EEC6038924CB_AdjustorThunk (void);
// 0x00000414 System.Int32 UnityEngine.Localization.Pseudo.Expander_ExpansionRule::get_MaxCharacters()
extern void ExpansionRule_get_MaxCharacters_m73F140050699A01185BD5C7A067A93AD8897DF2B_AdjustorThunk (void);
// 0x00000415 System.Void UnityEngine.Localization.Pseudo.Expander_ExpansionRule::set_MaxCharacters(System.Int32)
extern void ExpansionRule_set_MaxCharacters_m22A1833EED0978BA0B73056C743A5C808E1FE3BE_AdjustorThunk (void);
// 0x00000416 System.Single UnityEngine.Localization.Pseudo.Expander_ExpansionRule::get_ExpansionAmount()
extern void ExpansionRule_get_ExpansionAmount_mEEDEC2BF7693AC3B529CA9979C8008B8D78936EF_AdjustorThunk (void);
// 0x00000417 System.Void UnityEngine.Localization.Pseudo.Expander_ExpansionRule::set_ExpansionAmount(System.Single)
extern void ExpansionRule_set_ExpansionAmount_mAF1EFF6225478043C1997827638D048980EB8D2A_AdjustorThunk (void);
// 0x00000418 System.Void UnityEngine.Localization.Pseudo.Expander_ExpansionRule::.ctor(System.Int32,System.Int32,System.Single)
extern void ExpansionRule__ctor_m01842E2E049B9464FF3514721ADDAE88C8771490_AdjustorThunk (void);
// 0x00000419 System.Boolean UnityEngine.Localization.Pseudo.Expander_ExpansionRule::InRange(System.Int32)
extern void ExpansionRule_InRange_mC243D964B0DC3D4206F159F80487610672598238_AdjustorThunk (void);
// 0x0000041A System.Int32 UnityEngine.Localization.Pseudo.Expander_ExpansionRule::CompareTo(UnityEngine.Localization.Pseudo.Expander_ExpansionRule)
extern void ExpansionRule_CompareTo_mF4D487034D0BCA6B81B787326D5B201C752D87B1_AdjustorThunk (void);
// 0x0000041B System.Void UnityEngine.Localization.Pseudo.Mirror::Transform(UnityEngine.Localization.Pseudo.Message)
extern void Mirror_Transform_m78DCC2062EA50862D99695FA4204D6CD076DDA3D (void);
// 0x0000041C System.Void UnityEngine.Localization.Pseudo.Mirror::MirrorFragment(UnityEngine.Localization.Pseudo.WritableMessageFragment)
extern void Mirror_MirrorFragment_mD3CD17E169A9AADAE622068D4D6C3033DB84610B (void);
// 0x0000041D System.Void UnityEngine.Localization.Pseudo.Mirror::.ctor()
extern void Mirror__ctor_mCFDF1940842B0ECDD5C2C359421DC542A96D4D19 (void);
// 0x0000041E System.Char UnityEngine.Localization.Pseudo.PreserveTags::get_Opening()
extern void PreserveTags_get_Opening_m653A94D322F3B611AC0FDA0EA14CC6036C7A3C98 (void);
// 0x0000041F System.Void UnityEngine.Localization.Pseudo.PreserveTags::set_Opening(System.Char)
extern void PreserveTags_set_Opening_m17A19E66FD234F9593C3CFAB0334F40A4FD30D58 (void);
// 0x00000420 System.Char UnityEngine.Localization.Pseudo.PreserveTags::get_Closing()
extern void PreserveTags_get_Closing_m7C699A7B91EC5A6CC9526AF27D157BDE2F2A0529 (void);
// 0x00000421 System.Void UnityEngine.Localization.Pseudo.PreserveTags::set_Closing(System.Char)
extern void PreserveTags_set_Closing_m10FEA0396006483AAF9435DC8C5C63E525470588 (void);
// 0x00000422 System.Void UnityEngine.Localization.Pseudo.PreserveTags::Transform(UnityEngine.Localization.Pseudo.Message)
extern void PreserveTags_Transform_m92943328A8155EAA8D2354D6A46B9D63DDE779FA (void);
// 0x00000423 System.Void UnityEngine.Localization.Pseudo.PreserveTags::.ctor()
extern void PreserveTags__ctor_m6EEA9F399A673393516CDDA1247DA6F3A2BB985E (void);
// 0x00000424 System.Collections.Generic.List`1<UnityEngine.Localization.Pseudo.IPseudoLocalizationMethod> UnityEngine.Localization.Pseudo.PseudoLocale::get_Methods()
extern void PseudoLocale_get_Methods_m73945BE2B479793F75E10252D218AE5AE31BE470 (void);
// 0x00000425 UnityEngine.Localization.Pseudo.PseudoLocale UnityEngine.Localization.Pseudo.PseudoLocale::CreatePseudoLocale()
extern void PseudoLocale_CreatePseudoLocale_m9438FA7BAA469CC4EED492AE6B546C9CE6C9660B (void);
// 0x00000426 System.Void UnityEngine.Localization.Pseudo.PseudoLocale::.ctor()
extern void PseudoLocale__ctor_m7944EABEF32DED913172FF4A14D963ACA21A6172 (void);
// 0x00000427 System.Void UnityEngine.Localization.Pseudo.PseudoLocale::Reset()
extern void PseudoLocale_Reset_m671D1C3A01A1091868C50F44BD3774BF70305C2A (void);
// 0x00000428 System.String UnityEngine.Localization.Pseudo.PseudoLocale::GetPseudoString(System.String)
extern void PseudoLocale_GetPseudoString_m7056161C4369467C879A09C0769BE8E77B23086A (void);
// 0x00000429 System.String UnityEngine.Localization.Pseudo.PseudoLocale::ToString()
extern void PseudoLocale_ToString_mD14317D3537073588811ECECEFCDF698EC4B1DD1 (void);
// 0x0000042A System.Char[] UnityEngine.Localization.Pseudo.TypicalCharacterSets::GetTypicalCharactersForLanguage(UnityEngine.SystemLanguage)
extern void TypicalCharacterSets_GetTypicalCharactersForLanguage_m57F081FAADDFF493F5121620FFC11DD4FD7277CB (void);
// 0x0000042B System.Void UnityEngine.Localization.Pseudo.TypicalCharacterSets::.cctor()
extern void TypicalCharacterSets__cctor_mACA61663EDAB37E29C27815F4261B6E9B03A9868 (void);
// 0x0000042C System.Void UnityEngine.Localization.Components.UnityEventAudioClip::.ctor()
extern void UnityEventAudioClip__ctor_mF3FCFF88B6B57FCC2E696C23D5E07054B3CCB357 (void);
// 0x0000042D System.Void UnityEngine.Localization.Components.LocalizeAudioClipEvent::.ctor()
extern void LocalizeAudioClipEvent__ctor_m32A4C38BFAC48800D092A13518174DAE8458C5F3 (void);
// 0x0000042E System.Void UnityEngine.Localization.Components.UnityEventSprite::.ctor()
extern void UnityEventSprite__ctor_m1825285F2BB8BB31A972394A04A1B10D00CBF312 (void);
// 0x0000042F System.Void UnityEngine.Localization.Components.LocalizeSpriteEvent::.ctor()
extern void LocalizeSpriteEvent__ctor_mB31BEDAAB2AD4298F53F4D8915E637E9D1FBEBAE (void);
// 0x00000430 UnityEngine.Localization.LocalizedString UnityEngine.Localization.Components.LocalizeStringEvent::get_StringReference()
extern void LocalizeStringEvent_get_StringReference_mAB06FAA47CE385FE485CF85E39F2DF70FA452EDE (void);
// 0x00000431 System.Void UnityEngine.Localization.Components.LocalizeStringEvent::set_StringReference(UnityEngine.Localization.LocalizedString)
extern void LocalizeStringEvent_set_StringReference_m261CA87D82BA3B93802ED2580F201C8388600667 (void);
// 0x00000432 UnityEngine.Localization.Components.LocalizeStringEvent_StringUnityEvent UnityEngine.Localization.Components.LocalizeStringEvent::get_OnUpdateString()
extern void LocalizeStringEvent_get_OnUpdateString_mCC3A1BACE2CAF665427CBE26D44C6BCFE6148927 (void);
// 0x00000433 System.Void UnityEngine.Localization.Components.LocalizeStringEvent::set_OnUpdateString(UnityEngine.Localization.Components.LocalizeStringEvent_StringUnityEvent)
extern void LocalizeStringEvent_set_OnUpdateString_m8679A1FF9EEB7D02DE5DF5AF781A1D92BA6C756D (void);
// 0x00000434 System.Void UnityEngine.Localization.Components.LocalizeStringEvent::OnEnable()
extern void LocalizeStringEvent_OnEnable_m9C0AF42F40C9944C080EBAEB44EB8A2878432333 (void);
// 0x00000435 System.Void UnityEngine.Localization.Components.LocalizeStringEvent::OnDisable()
extern void LocalizeStringEvent_OnDisable_m81BDCA35B9515BEA4F97B7D4D8D6FF32E4B66AB2 (void);
// 0x00000436 System.Void UnityEngine.Localization.Components.LocalizeStringEvent::UpdateString(System.String)
extern void LocalizeStringEvent_UpdateString_m241988EEC8258CAE138384FECBBCBB756EAD4A78 (void);
// 0x00000437 System.Void UnityEngine.Localization.Components.LocalizeStringEvent::RegisterChangeHandler()
extern void LocalizeStringEvent_RegisterChangeHandler_mEED3D3D50C61C83A9582FF04D1308102F2E53A3F (void);
// 0x00000438 System.Void UnityEngine.Localization.Components.LocalizeStringEvent::ClearChangeHandler()
extern void LocalizeStringEvent_ClearChangeHandler_m3F48567D36ECD4FEA0BC06B2FFE544CAE2E927EC (void);
// 0x00000439 System.Void UnityEngine.Localization.Components.LocalizeStringEvent::.ctor()
extern void LocalizeStringEvent__ctor_m5E89CD7346A2724DF4EB24F9C8A2FAC9D63E0C79 (void);
// 0x0000043A System.Void UnityEngine.Localization.Components.LocalizeStringEvent_StringUnityEvent::.ctor()
extern void StringUnityEvent__ctor_m5442E27FDB074BB94A71A2806A1D7E31209876A9 (void);
// 0x0000043B System.Void UnityEngine.Localization.Components.UnityEventTexture::.ctor()
extern void UnityEventTexture__ctor_m94DA293A0097DB9111DADBC30D08346F24836B85 (void);
// 0x0000043C System.Void UnityEngine.Localization.Components.LocalizeTextureEvent::.ctor()
extern void LocalizeTextureEvent__ctor_mD387244084054E2ABE561ED47A13D1DF8D9ACBEF (void);
// 0x0000043D TReference UnityEngine.Localization.Components.LocalizedAssetBehaviour`2::get_AssetReference()
// 0x0000043E System.Void UnityEngine.Localization.Components.LocalizedAssetBehaviour`2::set_AssetReference(TReference)
// 0x0000043F System.Void UnityEngine.Localization.Components.LocalizedAssetBehaviour`2::OnEnable()
// 0x00000440 System.Void UnityEngine.Localization.Components.LocalizedAssetBehaviour`2::OnDisable()
// 0x00000441 System.Void UnityEngine.Localization.Components.LocalizedAssetBehaviour`2::UpdateAsset(TObject)
// 0x00000442 System.Void UnityEngine.Localization.Components.LocalizedAssetBehaviour`2::.ctor()
// 0x00000443 TEvent UnityEngine.Localization.Components.LocalizedAssetEvent`3::get_OnUpdateAsset()
// 0x00000444 System.Void UnityEngine.Localization.Components.LocalizedAssetEvent`3::set_OnUpdateAsset(TEvent)
// 0x00000445 System.Void UnityEngine.Localization.Components.LocalizedAssetEvent`3::UpdateAsset(TObject)
// 0x00000446 System.Void UnityEngine.Localization.Components.LocalizedAssetEvent`3::.ctor()
// 0x00000447 UnityEngine.Localization.Metadata.MetadataType UnityEngine.Localization.Metadata.MetadataTypeAttribute::get_Type()
extern void MetadataTypeAttribute_get_Type_m7015C5A17403BE26ACD1C95ECA6955770003CA2C (void);
// 0x00000448 System.Void UnityEngine.Localization.Metadata.MetadataTypeAttribute::set_Type(UnityEngine.Localization.Metadata.MetadataType)
extern void MetadataTypeAttribute_set_Type_m4BBF1A85CEEF81B3513B66DFEC305F8BF0E118B3 (void);
// 0x00000449 System.Void UnityEngine.Localization.Metadata.MetadataTypeAttribute::.ctor(UnityEngine.Localization.Metadata.MetadataType)
extern void MetadataTypeAttribute__ctor_mAB0EBC0EF5F270C114844DF7BCA8025EFE65D3FE (void);
// 0x0000044A System.String UnityEngine.Localization.Metadata.MetadataAttribute::get_MenuItem()
extern void MetadataAttribute_get_MenuItem_m3DBD782DE5ADDD2ED728C162494249E9FD4FA89C (void);
// 0x0000044B System.Void UnityEngine.Localization.Metadata.MetadataAttribute::set_MenuItem(System.String)
extern void MetadataAttribute_set_MenuItem_m58350CA8E7574D94DF90CADC436EB19995BA66F8 (void);
// 0x0000044C System.Boolean UnityEngine.Localization.Metadata.MetadataAttribute::get_AllowMultiple()
extern void MetadataAttribute_get_AllowMultiple_m76996A93063F7102805AEC38393EB66D580E2930 (void);
// 0x0000044D System.Void UnityEngine.Localization.Metadata.MetadataAttribute::set_AllowMultiple(System.Boolean)
extern void MetadataAttribute_set_AllowMultiple_mF9CA86ABBED5DA37CB5DD306C74EA6463B575F21 (void);
// 0x0000044E UnityEngine.Localization.Metadata.MetadataType UnityEngine.Localization.Metadata.MetadataAttribute::get_AllowedTypes()
extern void MetadataAttribute_get_AllowedTypes_m16BC0738C276E85F42A1B500D575802110169B44 (void);
// 0x0000044F System.Void UnityEngine.Localization.Metadata.MetadataAttribute::set_AllowedTypes(UnityEngine.Localization.Metadata.MetadataType)
extern void MetadataAttribute_set_AllowedTypes_m9BCE86B4D8BFD6CF24F28822F88D0E2D6218137A (void);
// 0x00000450 System.Void UnityEngine.Localization.Metadata.MetadataAttribute::.ctor()
extern void MetadataAttribute__ctor_mCEC26AB01CD5805DFEB901A94E2267E7AC473EB7 (void);
// 0x00000451 System.Type UnityEngine.Localization.Metadata.AssetTypeMetadata::get_Type()
extern void AssetTypeMetadata_get_Type_m851560556ADD149542EB1A44956B88E276DD5EAF (void);
// 0x00000452 System.Void UnityEngine.Localization.Metadata.AssetTypeMetadata::set_Type(System.Type)
extern void AssetTypeMetadata_set_Type_m58361EA5FC553AFA6F3E88DEEAE8FAD7AA9C2FB5 (void);
// 0x00000453 System.Void UnityEngine.Localization.Metadata.AssetTypeMetadata::OnBeforeSerialize()
extern void AssetTypeMetadata_OnBeforeSerialize_mED15BA31869AA924DA5AE7EF669AFE5B826B3D11 (void);
// 0x00000454 System.Void UnityEngine.Localization.Metadata.AssetTypeMetadata::OnAfterDeserialize()
extern void AssetTypeMetadata_OnAfterDeserialize_m245E32006D229ED95BE138D7AE8BA0E0BA3E08D6 (void);
// 0x00000455 System.Void UnityEngine.Localization.Metadata.AssetTypeMetadata::.ctor()
extern void AssetTypeMetadata__ctor_mA949DC80059489E2A95122EE5902C5490A403406 (void);
// 0x00000456 System.String UnityEngine.Localization.Metadata.Comment::get_CommentText()
extern void Comment_get_CommentText_mDA0E972A8124ECA30862063F0C423B3E58E19521 (void);
// 0x00000457 System.Void UnityEngine.Localization.Metadata.Comment::set_CommentText(System.String)
extern void Comment_set_CommentText_mF6440CF598FCE8ADE414C5F130A93C4FC3A4FDAB (void);
// 0x00000458 System.Void UnityEngine.Localization.Metadata.Comment::.ctor()
extern void Comment__ctor_m6A89B8CE9D2ABB3A32960DE746F644B159A6E14B (void);
// 0x00000459 System.Void UnityEngine.Localization.Metadata.FallbackLocale::.ctor()
extern void FallbackLocale__ctor_mF7A1336CED7009090CBBE386E12CB1ACF5CA9096 (void);
// 0x0000045A System.Void UnityEngine.Localization.Metadata.FallbackLocale::.ctor(UnityEngine.Localization.Locale)
extern void FallbackLocale__ctor_mEDB0F64B549006978F340D155ABCF72EA7DCD9E8 (void);
// 0x0000045B UnityEngine.Localization.Locale UnityEngine.Localization.Metadata.FallbackLocale::get_Locale()
extern void FallbackLocale_get_Locale_mB1586E6DDD6A47ACAA38A7779707B17E43364F04 (void);
// 0x0000045C System.Void UnityEngine.Localization.Metadata.FallbackLocale::set_Locale(UnityEngine.Localization.Locale)
extern void FallbackLocale_set_Locale_m724BEA676FA5BD53591AE417E50E043108EC1688 (void);
// 0x0000045D System.Boolean UnityEngine.Localization.Metadata.FallbackLocale::IsCyclic(UnityEngine.Localization.Locale)
extern void FallbackLocale_IsCyclic_mCD063CCA8893BC7EAFC047DA5D2BCA8CE1AA5666 (void);
// 0x0000045E System.Boolean UnityEngine.Localization.Metadata.ISharedMetadata::Contains(System.Int64)
// 0x0000045F System.Void UnityEngine.Localization.Metadata.ISharedMetadata::AddEntry(System.Int64)
// 0x00000460 System.Void UnityEngine.Localization.Metadata.ISharedMetadata::RemoveEntry(System.Int64)
// 0x00000461 System.Collections.Generic.IList`1<UnityEngine.Localization.Metadata.IMetadata> UnityEngine.Localization.Metadata.IMetadataCollection::get_MetadataEntries()
// 0x00000462 TObject UnityEngine.Localization.Metadata.IMetadataCollection::GetMetadata()
// 0x00000463 System.Void UnityEngine.Localization.Metadata.IMetadataCollection::GetMetadatas(System.Collections.Generic.IList`1<TObject>)
// 0x00000464 System.Collections.Generic.IList`1<TObject> UnityEngine.Localization.Metadata.IMetadataCollection::GetMetadatas()
// 0x00000465 System.Void UnityEngine.Localization.Metadata.IMetadataCollection::AddMetadata(UnityEngine.Localization.Metadata.IMetadata)
// 0x00000466 System.Boolean UnityEngine.Localization.Metadata.IMetadataCollection::RemoveMetadata(UnityEngine.Localization.Metadata.IMetadata)
// 0x00000467 System.Boolean UnityEngine.Localization.Metadata.IMetadataCollection::Contains(UnityEngine.Localization.Metadata.IMetadata)
// 0x00000468 System.Collections.Generic.IList`1<UnityEngine.Localization.Metadata.IMetadata> UnityEngine.Localization.Metadata.MetadataCollection::get_MetadataEntries()
extern void MetadataCollection_get_MetadataEntries_mE1F9A4D7D46635CB2B2693549C4EDE9547DF2804 (void);
// 0x00000469 System.Boolean UnityEngine.Localization.Metadata.MetadataCollection::get_HasData()
extern void MetadataCollection_get_HasData_m67A440D68E8F5B3FD6199B05E4423140950992A0 (void);
// 0x0000046A TObject UnityEngine.Localization.Metadata.MetadataCollection::GetMetadata()
// 0x0000046B System.Void UnityEngine.Localization.Metadata.MetadataCollection::GetMetadatas(System.Collections.Generic.IList`1<TObject>)
// 0x0000046C System.Collections.Generic.IList`1<TObject> UnityEngine.Localization.Metadata.MetadataCollection::GetMetadatas()
// 0x0000046D System.Void UnityEngine.Localization.Metadata.MetadataCollection::AddMetadata(UnityEngine.Localization.Metadata.IMetadata)
extern void MetadataCollection_AddMetadata_mD3799E1933DB8882EE02F637F368BE6E50F50D96 (void);
// 0x0000046E System.Boolean UnityEngine.Localization.Metadata.MetadataCollection::RemoveMetadata(UnityEngine.Localization.Metadata.IMetadata)
extern void MetadataCollection_RemoveMetadata_m80CE7DEA4B398B6C47A6FC1C6A23B1A19E1353F3 (void);
// 0x0000046F System.Boolean UnityEngine.Localization.Metadata.MetadataCollection::Contains(UnityEngine.Localization.Metadata.IMetadata)
extern void MetadataCollection_Contains_mE0029F958F6223F82AFEC7078E045AF36754D161 (void);
// 0x00000470 System.Void UnityEngine.Localization.Metadata.MetadataCollection::.ctor()
extern void MetadataCollection__ctor_m4D1388E31851B3F421C638C8EA1A537BD73A79A0 (void);
// 0x00000471 UnityEngine.Localization.Metadata.PreloadAssetTableMetadata_PreloadBehaviour UnityEngine.Localization.Metadata.PreloadAssetTableMetadata::get_Behaviour()
extern void PreloadAssetTableMetadata_get_Behaviour_mE0F1E99BDBF6CECCCFB04A43D41A3E0F0F4A6C73 (void);
// 0x00000472 System.Void UnityEngine.Localization.Metadata.PreloadAssetTableMetadata::set_Behaviour(UnityEngine.Localization.Metadata.PreloadAssetTableMetadata_PreloadBehaviour)
extern void PreloadAssetTableMetadata_set_Behaviour_m932814B76693F9EE47EAFAE0E967C2A1A771DC52 (void);
// 0x00000473 System.Void UnityEngine.Localization.Metadata.PreloadAssetTableMetadata::.ctor()
extern void PreloadAssetTableMetadata__ctor_m87DC9A04ACBE65D650AF2D57766F4BF6E258BF22 (void);
// 0x00000474 System.Boolean UnityEngine.Localization.Metadata.SharedTableCollectionMetadata::get_IsEmpty()
extern void SharedTableCollectionMetadata_get_IsEmpty_mECD644F644FC2931930A23D4400F79F1A17961C3 (void);
// 0x00000475 System.Boolean UnityEngine.Localization.Metadata.SharedTableCollectionMetadata::Contains(System.Int64)
extern void SharedTableCollectionMetadata_Contains_m8D84E94C1E2BB57F2A475A11DA82C336C04CFB51 (void);
// 0x00000476 System.Boolean UnityEngine.Localization.Metadata.SharedTableCollectionMetadata::Contains(System.Int64,System.String)
extern void SharedTableCollectionMetadata_Contains_m251F85DF27CA5F721B33A80919ADA5511E628B2E (void);
// 0x00000477 System.Void UnityEngine.Localization.Metadata.SharedTableCollectionMetadata::AddEntry(System.Int64,System.String)
extern void SharedTableCollectionMetadata_AddEntry_m6AA6489F8F0DD6F575F87187410A9B4E186AF7FE (void);
// 0x00000478 System.Void UnityEngine.Localization.Metadata.SharedTableCollectionMetadata::RemoveEntry(System.Int64,System.String)
extern void SharedTableCollectionMetadata_RemoveEntry_m2A111F466312F07CB02BACF976DDBA9070E49F8F (void);
// 0x00000479 System.Void UnityEngine.Localization.Metadata.SharedTableCollectionMetadata::OnBeforeSerialize()
extern void SharedTableCollectionMetadata_OnBeforeSerialize_mE98CA40B8870052CAE836E3F0EA84C13AB696BF0 (void);
// 0x0000047A System.Void UnityEngine.Localization.Metadata.SharedTableCollectionMetadata::OnAfterDeserialize()
extern void SharedTableCollectionMetadata_OnAfterDeserialize_mCFFF323F17F5ED1586F40741A43CF1D7B703C8FB (void);
// 0x0000047B System.Void UnityEngine.Localization.Metadata.SharedTableCollectionMetadata::.ctor()
extern void SharedTableCollectionMetadata__ctor_m366AB5BD02584099090728F533724C584A39B13C (void);
// 0x0000047C System.Int64 UnityEngine.Localization.Metadata.SharedTableCollectionMetadata_Item::get_KeyId()
extern void Item_get_KeyId_m33636465F4A2709BE7C6E1F2D8E40735CF5739CF (void);
// 0x0000047D System.Void UnityEngine.Localization.Metadata.SharedTableCollectionMetadata_Item::set_KeyId(System.Int64)
extern void Item_set_KeyId_m372CA165C190CF956D73C76AE67027115192DDB4 (void);
// 0x0000047E System.Collections.Generic.List`1<System.String> UnityEngine.Localization.Metadata.SharedTableCollectionMetadata_Item::get_Tables()
extern void Item_get_Tables_mEFB9B026B7C7187EB3D5134148F249F37A11F602 (void);
// 0x0000047F System.Void UnityEngine.Localization.Metadata.SharedTableCollectionMetadata_Item::set_Tables(System.Collections.Generic.List`1<System.String>)
extern void Item_set_Tables_m0CB4421F69EE823555E7C82A19C4EC580D37F660 (void);
// 0x00000480 System.Void UnityEngine.Localization.Metadata.SharedTableCollectionMetadata_Item::.ctor()
extern void Item__ctor_m4CEBCCAD94221D9BCE884DA69AB3496DCF093F64 (void);
// 0x00000481 System.Int32 UnityEngine.Localization.Metadata.SharedTableEntryMetadata::get_Count()
extern void SharedTableEntryMetadata_get_Count_m4979897098E58C40CC5A82CAE40239A8D9F33824 (void);
// 0x00000482 System.Boolean UnityEngine.Localization.Metadata.SharedTableEntryMetadata::IsRegistered(UnityEngine.Localization.Tables.TableEntry)
extern void SharedTableEntryMetadata_IsRegistered_m88D87131CE3476CDB5146EDAF5A407EAFAF618A3 (void);
// 0x00000483 System.Void UnityEngine.Localization.Metadata.SharedTableEntryMetadata::Register(UnityEngine.Localization.Tables.TableEntry)
extern void SharedTableEntryMetadata_Register_m761A1AED3086AAAE4EF8071A05B8EC82D071958E (void);
// 0x00000484 System.Void UnityEngine.Localization.Metadata.SharedTableEntryMetadata::Unregister(UnityEngine.Localization.Tables.TableEntry)
extern void SharedTableEntryMetadata_Unregister_mF8DEE603F342D9E056F2D998F029566F5FB13D68 (void);
// 0x00000485 System.Void UnityEngine.Localization.Metadata.SharedTableEntryMetadata::OnBeforeSerialize()
extern void SharedTableEntryMetadata_OnBeforeSerialize_m2D84B53AA47254FC535E32B70E61A71225AC4161 (void);
// 0x00000486 System.Void UnityEngine.Localization.Metadata.SharedTableEntryMetadata::OnAfterDeserialize()
extern void SharedTableEntryMetadata_OnAfterDeserialize_m8B273F478AA6AFCE7363FC5D703EE2DBD45616CA (void);
// 0x00000487 System.Void UnityEngine.Localization.Metadata.SharedTableEntryMetadata::.ctor()
extern void SharedTableEntryMetadata__ctor_m69183A00F58CD30446202257759A310A65AA0AF4 (void);
// 0x00000488 System.Void UnityEngine.Localization.Metadata.SmartFormatTag::.ctor()
extern void SmartFormatTag__ctor_m9DE2228D777B8B8695E9A46C5C576A0FABFDE5DD (void);
// 0x00000489 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m34097AD7E4254D26DFA4C7A41A4D52C580190EC1 (void);
static Il2CppMethodPointer s_methodPointers[1161] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DisplayNameAttribute_get_Name_m15138B442E7C617C8D05D0AF698AEDED5D8F47A9,
	DisplayNameAttribute_set_Name_m593EC11603CD3596C338FE180936837E50BDA4B5,
	DisplayNameAttribute__ctor_m5BBA768F930CDEAF9270F2F810E9CC0FF57A1A9C,
	LocaleIdentifier_get_Undefined_m2208ACB79F98E1DA5C4B1851CCA25FE9FED4F2A0,
	LocaleIdentifier_get_Code_m4302E99F7C34A1ADDD264A7DA580C09BB02E2E2A_AdjustorThunk,
	LocaleIdentifier_get_CultureInfo_m064EB6542663B3FECEB5D3DFF3F69F1FB2DE8B74_AdjustorThunk,
	LocaleIdentifier__ctor_m133779944284E1633CFD7C294A589C03441EE514_AdjustorThunk,
	LocaleIdentifier__ctor_m8EEB29CBE081A30BAF0E34B6B3FDAE5FB0F6817D_AdjustorThunk,
	LocaleIdentifier__ctor_m06E8C439AB7901836EBE39C8FB130290A3F33E85_AdjustorThunk,
	LocaleIdentifier_op_Implicit_mACF46E0396F3868C554017858EA6B61BE0FD1012,
	LocaleIdentifier_op_Implicit_mE5E723265D9ADF34F57D5C71FF126AEB2DE7A972,
	LocaleIdentifier_op_Implicit_mD7D242306FFAE932C235D617D7491F70FE9D50EC,
	LocaleIdentifier_ToString_m4777A5A8A1559E833430E59E2B983CCAF83F26D5_AdjustorThunk,
	LocaleIdentifier_Equals_m772A2378D63E76D6C0D1A462BD438AEA47B9AB4D_AdjustorThunk,
	LocaleIdentifier_Equals_mFAE33C4F7EF2977EF21236931AA9DE6EAB15674D_AdjustorThunk,
	LocaleIdentifier_GetHashCode_m0D9EC66EA2C2E3D25FD9122AA253E8ED2562D0D3_AdjustorThunk,
	LocaleIdentifier_CompareTo_m718641E84134909C9379D717D4A21E867CDAFAC5_AdjustorThunk,
	LocaleIdentifier_op_Equality_m5AF71311A605106A2526B34E019BF34A6FC9CE8C,
	LocaleIdentifier_op_Inequality_m083C0B1D213044E421E440BB598ACA82749095D6,
	Locale_get_Identifier_m621ADAB745B2B24ACACF4669A48DAC9E758AB3EA,
	Locale_set_Identifier_mA12CD7F8517E5C38ACC3D4F7289C9C77E445AEF3,
	Locale_get_Metadata_m3DFE5F7112222CC8319EC90676E3F908DF541F89,
	Locale_set_Metadata_mA45EA3CD237AA40C0CAD6C0C859B0D53CC058D51,
	Locale_get_SortOrder_m9DBCC0285EF825609CE3A2E1DD97CEAEF7510E74,
	Locale_set_SortOrder_m1352804A3B0D817ECFA6266D18273549F5894D24,
	Locale_CreateLocale_m11558328BB212CDAB2CC1757FE08358B9D5F2A4F,
	Locale_CreateLocale_m859C02E2B909189755BE43640DB5B2E7CD3BDB4D,
	Locale_CreateLocale_m1BF652AEA4013DD2217867A85C42FFC6BA2EADE0,
	Locale_CreateLocale_m90CD02627809328FE140CFF9B65A402979BDA90B,
	Locale_CompareTo_mDE4462D2FA0D6D2E40C551B977384A15B33B95D5,
	Locale_ToString_m6084FB667B4653E812CD590488C1065710A82573,
	Locale__ctor_m08E92E232D53B82694B92821FF780FD5E5035AF8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LocalizedTexture__ctor_mB4718AC9BCDD3165897BBBFC9ABFB32912F51F0F,
	LocalizedAudioClip__ctor_mD104267A86F2DD9F92CD2FE1FDC04D8747EAE2CD,
	LocalizedSprite__ctor_m918EF089D8B07E286413824686FC3AE8ED04BEC0,
	LocalizedGameObject__ctor_mF3545FDED382B92B95AF6518898EC20E2F4A5518,
	LocalizedAssetTable_get_Database_m4063B35D2B609296DBDF90AE0B4B49279620EDFB,
	LocalizedAssetTable__ctor_m8927284826B572D2C9E6D4AD6F7AA047A80346F6,
	LocalizedReference_get_TableReference_mE587514AE3DE00E9F12B4E90009A6074A6642C60,
	LocalizedReference_set_TableReference_m4121FD7C826258D8C0141707B9CC63438475F886,
	LocalizedReference_get_TableEntryReference_mC33B5D7329493701DB64EBD92CE705E78826F1F3,
	LocalizedReference_set_TableEntryReference_mB5C076BA456602F208CA7D0285293268C531843D,
	LocalizedReference_get_IsEmpty_m9737303300623BF8F75719431208023CAC2418E7,
	LocalizedReference_SetReference_mC33A39926E3D30985B4368573BD740E17B18741A,
	LocalizedReference_ToString_m1F3DB906BE8C93140C50299C9D71C9FDC617152E,
	NULL,
	LocalizedReference__ctor_m4E1E6D7670335A7948EBBCDCD995EE52D87DB999,
	LocalizedString_get_Arguments_m5D8AE307C9A07D632B4077B8BDCAF02226EFEDF4,
	LocalizedString_set_Arguments_m70FE61A7F71BDE5ED1E9161A24E176EE52ACEBCA,
	LocalizedString_get_CurrentLoadingOperation_mC245D181AFD5D1232BC24CD617C1CA0BDF29C7F6,
	LocalizedString_set_CurrentLoadingOperation_mFEF77C33E384E40E2B57971A938B0FDFF31707B1,
	LocalizedString_add_StringChanged_m0DA9B1CCE4A83CE494847522BBD21FB1A3BADA35,
	LocalizedString_remove_StringChanged_m570195355B80479599CF66080BA5F7071CAF4DC0,
	LocalizedString_RefreshString_mDBB0844A41B8159A7897A917BE26A71F5A6699D1,
	LocalizedString_GetLocalizedString_mA71DFB44AFD003E5B72B746EEF4B5D1D7B88E3DE,
	LocalizedString_GetLocalizedString_mC1ED8AFDB5B2A90D07C7F701C083A59FCA9689E6,
	LocalizedString_ForceUpdate_mAA87ACD7E50FB54A75FD84F05395EB42194C6764,
	LocalizedString_HandleLocaleChange_m5A6539BC984F00526E93C858CC4BF19607B01C47,
	LocalizedString_AutomaticLoadingCompleted_mA23404E5D8AE555CFB19DB84A879828C3C1044A9,
	LocalizedString_ClearLoadingOperation_m8556F2CAEE9B717A0BAF86B10D589B8D7CD01073,
	LocalizedString__ctor_m21B176C32FF2D6A39CA3B90261E16C1BB31936FB,
	ChangeHandler__ctor_m52682E05372D1D677D6327024A11BB8A05BD579E,
	ChangeHandler_Invoke_mF01B7475C92D862E30311036F50B395EB5CEADD6,
	ChangeHandler_BeginInvoke_m03B8103956FC241ACEE321DB3B9C5545445890FD,
	ChangeHandler_EndInvoke_mE29324A74F064F13B4489F3D1B132137E661E451,
	LocalizedStringTable_get_Database_m390D6D275E96A2A2E079581E97129CA6E052DB24,
	LocalizedStringTable__ctor_mB212473ACBD44AB4CFF150162DB03F87AA70E33F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	GroupIAsyncOperation_get_Progress_mBC0CB9E70D0D8369123537AEE5012871DFD41940,
	GroupIAsyncOperation_get_DebugName_m065B6C693663E962FABECF38D7798AC92C9920AE,
	GroupIAsyncOperation_Init_mF5463DE0B23BD1B0EF7ECEF284BD6A50C84BB5D2,
	GroupIAsyncOperation_Execute_mE6B2C97C7C637E1131F195A413432EED4E533851,
	GroupIAsyncOperation_OnOperationCompleted_m52DD759DB8FBC1700AEE3D91346E3BD8ACC81324,
	GroupIAsyncOperation__ctor_m54F64AB87CDB3357820B8DA7FCCE04FCE7D441CE,
	NULL,
	InitializationOperation_get_Progress_mD8335D02D2D22AB3331543ACF9D6386EF3652F54,
	InitializationOperation_get_DebugName_mD96F3628DCEE3A95A95EFE7A12ECB4A7A9C5A521,
	InitializationOperation_Init_m7D9BF5D85A90FEF273BBD704D3012BA055898E44,
	InitializationOperation_Execute_mB7335F071F581FB53547B85D70972865E68E3093,
	InitializationOperation_LoadLocales_m68DAD612D9B005E6523AA0DC650C71142EAD6964,
	InitializationOperation_PreloadOperationCompleted_m6999909CCD9F6ED2D2E6A7E38498B21732405DF1,
	InitializationOperation_PreLoadTables_mD8AC8126732DD915521237C29A57F11E22A3261F,
	InitializationOperation_PostInitializeExtensions_m229ECD97C8074AC58B5EFE63B6378482CBD6F227,
	InitializationOperation_FinishInitializing_m429D7963BD9C333D12481D4415503EA1016A205A,
	InitializationOperation__ctor_m51E0D99162D446BC725BC030A666BCE9FB823B7F,
	InitializationOperation_U3CLoadLocalesU3Eb__13_0_m7E4808B00790B7416430832D17E1B5FE9A5A3AC1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AddressHelper_GetTableAddress_m6995DFCA1E7BBE707F62A8A6B48A15960A7F6C78,
	AddressHelper_FormatAssetLabel_mD4A0B244D7A38ED77380897886AF17F9FB74E3BE,
	AddressHelper_IsLocaleLabel_mFFE8C02E6E0899EF96D3AE1EB8224310E6FE410F,
	AddressHelper_LocaleLabelToId_m711FC0B20C7526DC23A0B72A5AE7448C6703707F,
	AddressHelper__ctor_m35327DEB9B838CAAAAA442CC6F9526831FD33880,
	StringBuilderPool_Get_m0D5F95875AEDA814DA170CBE157905951DC6657B,
	StringBuilderPool_Get_mFD003EB5F95D0B9957A75788475FBDCCD85D8219,
	StringBuilderPool_Release_m0CDE332654CE07026EA81B1D70A477B63DC8C5B9,
	StringBuilderPool__cctor_m163BA084739B135A4B1C01983C018CA1F72B5824,
	U3CU3Ec__cctor_m6B8A9ED223680D90E33945EA982AD188A3AA8AF7,
	U3CU3Ec__ctor_mFFC3C3C75857C24CD50789E394CB5C77D4397F13,
	U3CU3Ec_U3C_cctorU3Eb__4_0_m4A2E997621A35BF6D0F1B62BC363B9279072DF36,
	U3CU3Ec_U3C_cctorU3Eb__4_1_m77890050B39F0C75314B5AFF1C4BD4D906E44E7F,
	SystemLanguageConverter_GetSystemLanguageCultureCode_m696186E1DD47470FC99807AE900A7039A43CD12F,
	AssetTableEntry_get_AsyncOperation_mB5313EA52EE7F354BDCC53A0D46BDCBD88D149EC,
	AssetTableEntry_set_AsyncOperation_mF68A581A9C5D3821E1318C513FA7F0CF119C3CB6,
	AssetTableEntry_get_Guid_mFDB173E3FBBC0CB84CBE940598502251383B4F84,
	AssetTableEntry_set_Guid_m414084741BA6710F418BEAB3C087586DE6466402,
	AssetTableEntry_get_IsEmpty_m57FF2869BCFF5CB121C90C1C831421E10B2974B5,
	AssetTableEntry__ctor_m74202A9165B46C9F09A848DB223769C00E1FD24A,
	AssetTableEntry_RemoveFromTable_m871C27047D6B989D9D2F9B2D77A8C6F272C3AFD8,
	AssetTable_get_ResourceManager_m25F6142AA72A2503113F3C19664EE32218744C6E,
	AssetTable_get_PreloadOperation_mA136C648833B646014DF02BF60892D6E822AB140,
	AssetTable_PreloadAssets_m8098674AEB07FDC7FCE1BADF9C02F1658B250387,
	NULL,
	NULL,
	AssetTable_CreateTableEntry_m923BF5153B8D40E4CCFEE693E9932B0745FF1459,
	AssetTable__ctor_m8CE2C3CDC50BC350F2278DC50AEB3162C6E33B9A,
	TableEntry_get_Table_m198F8C944F31A183AAD2BBEB2DCC4F45AC656750,
	TableEntry_set_Table_mA3C74E9E65E0340D0E4F04649312D8DA2628863B,
	TableEntry_get_Data_mDD18E697BFBC3D3608D9D1CC4D882F3539D20FBF,
	TableEntry_set_Data_mB540E511BE653FE5AA69C2DF3F806B3077DBCC0D,
	TableEntry_get_KeyId_mDDCE6AA8B6F2AA70E542B77868575B4E298022BF,
	TableEntry_get_LocalizedValue_m039200CA573ABEF47C455C385969EF2581FB0148,
	TableEntry_get_MetadataEntries_m16E4EEA6A46A4B408B8774F87472BD1E33D23C2D,
	NULL,
	NULL,
	NULL,
	NULL,
	TableEntry_AddSharedMetadata_mBEA5219FE5DD930C4FD73C79502277DA81E7D96D,
	TableEntry_AddSharedMetadata_m4BB0F6A34F151BEEA64253EF4E2A47884E2F8660,
	TableEntry_AddMetadata_m6EC5448F2D0C020F6E7625AE37425260DC300DF1,
	NULL,
	TableEntry_RemoveSharedMetadata_m07887DC53F85E5327641384D3F0CF4BE719BEC8B,
	TableEntry_RemoveSharedMetadata_m3C597B1E6A1C685095C3EEB2DABCB857C856EB6F,
	TableEntry_RemoveMetadata_m9B12597714B0448BF24C7C78BE12E63A72378932,
	TableEntry_Contains_m4DD8C03BA75FA4FE438E2EAA0FBA285C91DEFC94,
	TableEntry_ToString_m256995777339AA3245C629DE0658277F4F34FB51,
	TableEntry__ctor_m67FB3D0B850C4A50C72207411E73E83DBF2C2F62,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	DistributedUIDGenerator_get_CustomEpoch_m813FD68FDB40D9DF9862E1FE567A50E2671B08B2,
	DistributedUIDGenerator_get_MachineId_m75051BDD3874DB6FBC812CF7512D8302A3057BEA,
	DistributedUIDGenerator_set_MachineId_m79E3F9571FAC3B0486B31A1F352F2CCF599B2E3D,
	DistributedUIDGenerator__ctor_mC137FF4985FD66B2CD09044D5F287D43F52D038F,
	DistributedUIDGenerator__ctor_mA8C7CB497AC6BCF4F163F3F8F8A4B55CF81C9327,
	DistributedUIDGenerator_GetNextKey_m4E97FE94D6EF1A716FB6B23A2BC0568D768D1A7B,
	DistributedUIDGenerator_TimeStamp_m51D4BABA0F7486FDED01F498456F8ABB50F9E47F,
	DistributedUIDGenerator_GetMachineId_m4F9FD31873358E4A0087070188C279B09517F2C8,
	DistributedUIDGenerator_WaitNextMillis_m7B3DF926D5887B1C5F9146F569126A11AA5A2A25,
	DistributedUIDGenerator__cctor_mFC83D01727CE111878151F2DEF28B506BFF30BC1,
	NULL,
	SequentialIDGenerator_get_NextAvailableId_m6103EDB0B993B59721B1D045FF988AD8B0B37A41,
	SequentialIDGenerator__ctor_m4CAC7AD7B76E2AB1ED74B685186F0D20EB1D5640,
	SequentialIDGenerator__ctor_m24A65BD2B0E155ECC816E7E0457860ED15239E95,
	SequentialIDGenerator_GetNextKey_mF083EBAA02D3EB21C8864932A3E84220178CB14F,
	LocalizationTable_get_LocaleIdentifier_mCDFBA2A154AA02D2055FB0607732F80D28491E77,
	LocalizationTable_set_LocaleIdentifier_m96B7561EC609F68A4BE80D59394418E6D69B8A52,
	LocalizationTable_get_TableCollectionName_m678F13B4610B474F7FC077C04FF429AFF4371BB3,
	LocalizationTable_get_SharedData_mA7CE9852A576233876D61E0D60DB2187A0491054,
	LocalizationTable_set_SharedData_m96D7BB75944BBD28CBDFD8870054D5CDEB712AA0,
	LocalizationTable_get_TableData_m78561AC7AAA47CBC85F617B7946F9682A106A505,
	LocalizationTable_get_MetadataEntries_mF0BB56C154B55E120A9EE25263AB0E5F5A2F677B,
	NULL,
	NULL,
	NULL,
	LocalizationTable_AddMetadata_m5E42D5FF1E3290B7A1463400561B55E4246A018D,
	LocalizationTable_RemoveMetadata_m15B0DDF699FFED75A953F7BB445B669A7E7E5610,
	LocalizationTable_Contains_mE3559B8D1C2130B1920B3A1B27BD80FF5F1D66EC,
	NULL,
	LocalizationTable_FindKeyId_m31684AC7BE81E1CA28042F38A02F414082C484A4,
	LocalizationTable_VerifySharedTableDataIsNotNull_m0F79FEB3B99E8E437A95F1925EF71EF906265228,
	LocalizationTable_ToString_m1CC7839B88480412862717724D0613199D248E21,
	LocalizationTable_CompareTo_m4ED56AC885C9B698CEAF20E5E2ABF6D546523017,
	LocalizationTable__ctor_m330FEFEB93885A171739272F2F492429B17D8EB2,
	SharedTableData_get_Entries_mAA5CDB7F148EB71063D9686F4D55146D95F4BAB2,
	SharedTableData_get_TableCollectionName_m72856600FD145DC0C130F0736F17A84EFCF9E64C,
	SharedTableData_set_TableCollectionName_m24BDB7E83AEF335FF76CB0F1CD2484E998FC4D3D,
	SharedTableData_get_TableCollectionNameGuid_mECF51D3522303934BCDCA7E8C8C9BC7AEA32DAB0,
	SharedTableData_set_TableCollectionNameGuid_m72546494ED70DB610F7C7C9FB3EC1E6B8015764F,
	SharedTableData_get_Metadata_mA9214D6B75B18E98E0A81A012698DFE32F43533E,
	SharedTableData_set_Metadata_mDC6AE078B4E3EF6A6150CE8D43B47E2378499F90,
	SharedTableData_get_KeyGenerator_m3F2DE0652F16E90127512BAED22DF92D41264EED,
	SharedTableData_set_KeyGenerator_m2D9386E05F6D8921F6A7B8174723513CD2F562DF,
	SharedTableData_GetKey_m1FD87D09A1ACE0B88AA5D0272D2741471792837C,
	SharedTableData_GetId_m90619F58B422E957B5527FB90921D65025200190,
	SharedTableData_GetId_m8E983275B27ED7A64E6AC2DB18EB983F3B6B7B22,
	SharedTableData_GetEntryFromReference_m9CE1863DC085144CB18ACDAC502DA3A271626480,
	SharedTableData_GetEntry_m654FA769A326078483AE9084CB5FF574B90E9189,
	SharedTableData_GetEntry_m3B05CC7B4F765893CC6A6E55864AC37AE761108E,
	SharedTableData_Contains_mF2F66F01B385BE8B9C8F5E0006B22C855A4ED8F8,
	SharedTableData_Contains_mA79D44D9CE1555FD1F4F0BBA7170D0C3DB6CFFCE,
	SharedTableData_AddKey_mFCEEFE720C0D561153289132595CE0881B1F75A9,
	SharedTableData_AddKey_m7EC6BB7176B0AE2BDE9696765051E890D5A332CE,
	SharedTableData_AddKey_mB7BE4A74F085771C05D7F20983CF4F8FBE32EF54,
	SharedTableData_RemoveKey_mB67741750E5EBF1D5D569B4F5E6B66FDD8A7F167,
	SharedTableData_RemoveKey_mD7C242AA34819E857289EDC7577106C67632F755,
	SharedTableData_RenameKey_m48B68C52026E4E858656FA01607B62EDCBA68BCE,
	SharedTableData_RenameKey_m216DA159F7ACFF823F097DEA6DE98EE07D7920B8,
	SharedTableData_RemapId_mE0C26DCD74141AE8D1EBC2EABC40A17934D779BF,
	SharedTableData_FindSimilarKey_mC3191D2AA7F58A000256EA0A797DFD7DB6CFFE93,
	SharedTableData_ComputeLevenshteinDistance_m6D04691E13AFE199AF2F50550B7F508BE7778FE7,
	SharedTableData_AddKeyInternal_m03013F80856905A1EFBD3812507255F77D15BC57,
	SharedTableData_AddKeyInternal_mF5D7905E1C28A7FC142B83E82BFBDA11B136BD8D,
	SharedTableData_RenameKeyInternal_m938FF1FAA074C6CE3744752ABC2E8591A18DF37E,
	SharedTableData_RemoveKeyInternal_m56C085641143AB191367890548C9AF6442A9DB29,
	SharedTableData_FindWithId_m22954CAD35DC70A7F389219C287284030F2C4413,
	SharedTableData_FindWithKey_m383F7ECACC4906543D10324930FA23652C61710E,
	SharedTableData_ToString_m1BCE5867E5E49D4110486CDB105AD827B623CE5D,
	SharedTableData_OnBeforeSerialize_m3A7694F1AF596891335EA39C1B0B8180494CE939,
	SharedTableData_OnAfterDeserialize_m865D497E4615E285A54C9423CA68C84324F477A0,
	SharedTableData__ctor_m7F85BA4B33E99FDCF3F73F5206059CB75580958F,
	SharedTableEntry_get_Id_m76E7DC1967E1DF86CD9CC4B8E5050C91A761A031,
	SharedTableEntry_set_Id_mEC8465D11C17CD6A58129B741E0521CFBABBE14F,
	SharedTableEntry_get_Key_mD04A1F7B3F414B36D6F4EAAAD31E0398FEAC2023,
	SharedTableEntry_set_Key_m90D913399333B83337DCBCB9BC8B93C2D905B5BD,
	SharedTableEntry_get_Metadata_m9E59A5CECC9B5C4B9DCCD8599BF08A41A5D726E4,
	SharedTableEntry_set_Metadata_mDE342A6B182B6457F3B84C1A7B5AE8845B2B69FC,
	SharedTableEntry_ToString_mDF719896E888CECF5AE1D9FED7F1B81BBEE67D02,
	SharedTableEntry__ctor_m061C1F354B8649432AF9305804A0EEFDD9A11F94,
	StringTableEntry_get_Value_mC877F1980AD7162FDAAE1DBA2EAEB5F7A58E8B2D,
	StringTableEntry_set_Value_mEDFB3CE813A29CEB59D08C50F3DA2F9537193C54,
	StringTableEntry_get_IsSmart_m0FDD0518AEA22C9B9E9BB71D811CD20F34937A86,
	StringTableEntry_set_IsSmart_mFC7D1B7382561EDEB2567D5114EBFE47BF1405A4,
	StringTableEntry__ctor_m07A76F3699E5F764BEB63CD849FCA493E1126E96,
	StringTableEntry_RemoveFromTable_m1615BFD64092A652D53F71DAAF84ECA9E6D4DE27,
	StringTableEntry_GetLocalizedString_m5EC063A53AE08621967F5AB13BB24B7EB0C6BA83,
	StringTableEntry_GetLocalizedString_mE1839039C1914B91FB098A687371DC45D48B19AB,
	StringTable_CreateTableEntry_m12D24600ECD65C9699C593790D7F40AFC87E6ABB,
	StringTable__ctor_m88D548D8544DC17069C15A0EB7068969265B731C,
	TableEntryData_get_Id_mD425E6667F610A63618793A12705C55ED68BC440,
	TableEntryData_set_Id_m6D41CBF1B569B93379A5EC32F15D6BE79C12FBF5,
	TableEntryData_get_Localized_mE3A6AA11D96CCAE04E0FB2A6D605619145A7F603,
	TableEntryData_set_Localized_m05BE69FCB707305908C7C0F298B4B1971E1E1109,
	TableEntryData_get_Metadata_mFF32EE01919FE5216723BC946398A309B28B4AC3,
	TableEntryData_set_Metadata_m21EE16A9FFDAD0A80D985C33DA4DBCCF5CD9A67D,
	TableEntryData__ctor_m80BE41D0D3BC72D30CCD2101D51F2BDFD94A1096,
	TableEntryData__ctor_m94759C3707CF9A517C921502DCF65AD45E10782D,
	TableEntryData__ctor_mD2558A9278ED1BC9E8904CC577DE0FCEE4641BD3,
	TableReference_get_ReferenceType_mC893ED4BB004BD59ED3BC2D808C54ADF0741E6BE_AdjustorThunk,
	TableReference_set_ReferenceType_m75A728B1FE51186F8E8042911C0730B52CCE6DD0_AdjustorThunk,
	TableReference_get_TableCollectionNameGuid_mE63A5EE8BCA498A930F9E4A7CA63155CE14D6C97_AdjustorThunk,
	TableReference_set_TableCollectionNameGuid_m06C127AD797D1BB0703F6836FBF2882CB1DD020E_AdjustorThunk,
	TableReference_get_TableCollectionName_mECB9C94F17424B265C16AFC589A295A8FBD4AA25_AdjustorThunk,
	TableReference_set_TableCollectionName_m7E7B174DB6B85BD406D2F67854974C6024D6DDE2_AdjustorThunk,
	TableReference_op_Implicit_m38ABCB1A36550166E739CEEB36732AA909BCCDAD,
	TableReference_op_Implicit_m38CA289C42CCEA86764AE78F29755527E7BEA133,
	TableReference_op_Implicit_mA3DA44CAC93CACDA1843648492BDA54F8D0FDCE0,
	TableReference_op_Implicit_m958243FB07A695A2CC852C7428466A52E949AA46,
	TableReference_Validate_m52665FBDBEA49EEE2B65FF46FA5142E034420DEA_AdjustorThunk,
	TableReference_GetSerializedString_m024253B2264C9C25AACFE6C23921C11BB27649F9_AdjustorThunk,
	TableReference_ToString_mB32AE382DEA9B163A3AE0E61B1F83431E151129F_AdjustorThunk,
	TableReference_Equals_m76717DC107E3638DF3717C4DE9C738287E440F9C_AdjustorThunk,
	TableReference_GetHashCode_m1801B22266D705F42D108EBE165D795DCDDA2D31_AdjustorThunk,
	TableReference_Equals_m8DFA7C7D14B873326653F4C144926D5741D773D7_AdjustorThunk,
	TableReference_GuidFromString_m3F0C95ECEF1E7CE530CC2610244E3746C6668A69,
	TableReference_StringFromGuid_m4DAB416BB95EEA2D9BC7B68D2F84926364A3E745,
	TableReference_TableReferenceFromString_m0568E59E756C386550443C37DCA9292AE4824A71,
	TableReference_IsGuid_mFC86D33A0B8581ADDC1424249B73383EA8F388E1,
	TableReference_OnBeforeSerialize_m3CB3EEFE383275AEDB7BE695091CA7B20584DF9B_AdjustorThunk,
	TableReference_OnAfterDeserialize_mC96C0055D863D3B1126921751A00E6A5059372EA_AdjustorThunk,
	TableEntryReference_get_ReferenceType_mC145F5B4C8E39097B71AF7799E18AB079FEB9556_AdjustorThunk,
	TableEntryReference_set_ReferenceType_m329A5A1A46BA792BA5497C0EC35E194EA83531E1_AdjustorThunk,
	TableEntryReference_get_KeyId_m74FA67F6DAD6B6E151838ED9F103388AE41C1153_AdjustorThunk,
	TableEntryReference_set_KeyId_m3B0708CC919B41C02EF0CF24B3F9883074172CE0_AdjustorThunk,
	TableEntryReference_get_Key_mC8BE974FF56F790B5E798EC401FBB425B6F78D44_AdjustorThunk,
	TableEntryReference_set_Key_m224E76DD1AA1CE7241714EBF612EF19ACA961A83_AdjustorThunk,
	TableEntryReference_op_Implicit_m62B8A8F98EF657EE6CC0421484D376D05F1BDC88,
	TableEntryReference_op_Implicit_mA6852C9B79787C3A4AE07E576B66D8260D2322DE,
	TableEntryReference_op_Implicit_m6245960816D37B46B0A91D49951836E87C8F891E,
	TableEntryReference_op_Implicit_m47C2342B7D120624531322751EE9A36E58EF0D62,
	TableEntryReference_Validate_m01EE19D2965E93860AD29D8EEEF7B1AFFCB658F5_AdjustorThunk,
	TableEntryReference_ResolveKeyName_m0DE9C31AA9570F5BD7852A8F5C3C9D1F18A7191A_AdjustorThunk,
	TableEntryReference_ToString_m5251A9B652CCB8AE01F60738FC8E5FB702D08245_AdjustorThunk,
	TableEntryReference_Equals_m74489CFE2D45F251D2508B64E419C8248571CF3D_AdjustorThunk,
	TableEntryReference_Equals_m17C4BD6E7C66ABC81069D97842574877A287CBCE_AdjustorThunk,
	TableEntryReference_GetHashCode_m8891B49029FADB026C11C6F163B5ACA344C7CC6A_AdjustorThunk,
	TableEntryReference_OnBeforeSerialize_mE30C216732C38BB66587F9CEB6C817EC29DAA464_AdjustorThunk,
	TableEntryReference_OnAfterDeserialize_m06BC208220A8B545622EE3AAEE68F1ADD2CCA952_AdjustorThunk,
	FormattingErrorEventArgs__ctor_m0FCF09D85986FA330CDAB8AB76EFB08B01DF7357,
	FormattingErrorEventArgs_get_Placeholder_mCA5AD225FC7170A146CD5B0C64AE558F452A302C,
	FormattingErrorEventArgs_set_Placeholder_m1765C64FAFCF3E863A44DD71A8EB7C2025AECFD9,
	FormattingErrorEventArgs_get_ErrorIndex_m6F49589584BFA61E05557E94A828AE5C64774B16,
	FormattingErrorEventArgs_set_ErrorIndex_m44E22A7244DA09F8D6A83B2512643DD670ABC6D7,
	FormattingErrorEventArgs_get_IgnoreError_m6A6453B4FB6D2A5B1531A5D6A790A153549ABDBB,
	FormattingErrorEventArgs_set_IgnoreError_m40ED1C4B86E224CB272BC2B40B493EDB8BED07FF,
	Smart_Format_m77690F4F96CBD5D287EAF0BDCCE65D16C81D9F32,
	Smart_Format_m018ABA7FAC3126A082471C4F8E0A150C274D6F3A,
	Smart_Format_m43257B51DC0A0EF74CAA48D79F3F6B9E8436ACC5,
	Smart_Format_m62AD7FEBBC436CB0C0ABC9248D2141DB37218970,
	Smart_Format_m04D01E16C7B7079CEAD97C73BCB56F4CBA4DCD35,
	Smart_get_Default_m3194B892D21B184218F50FDE39D1BC12E7639814,
	Smart_set_Default_m6480FA90F04C186235C9DE64AA22322A3107B5B8,
	Smart_CreateDefaultSmartFormat_m79E3058FF0C33E0AA990CE401F67F31C25C7ED01,
	SmartExtensions_AppendSmart_mDABC8B72ED79A36200F5F5A9623D993BAF5D726B,
	SmartExtensions_AppendLineSmart_mD1815723E3DAF50087B68E8A41BF885E785927F1,
	SmartExtensions_WriteSmart_mC445BFB80CFCC576F01863091BAEC8A9CA6EA583,
	SmartExtensions_WriteLineSmart_mC739A4F89F8CAA34DBEF29DF974C9B06CDD4FC5F,
	SmartExtensions_FormatSmart_m036379833C432E01646E79E55920623018B6AA2B,
	SmartExtensions_FormatSmart_mC201ADA9C175D3CF77C1F2E219F8C3D7A172B4A6,
	SmartFormatter_add_OnFormattingFailure_m679F18D60035AB977E26CF01FF1F0145C7793FB7,
	SmartFormatter_remove_OnFormattingFailure_mAC56D7DEA77C5D16B9A0E356C01E2EA98F893D81,
	SmartFormatter_get_SourceExtensions_mA06C14A9FDD59462C7DDEFED51841D84CC43DD55,
	SmartFormatter_get_FormatterExtensions_mCB78E4331F11DAA0A4E7D126FC2CAC2ED37B3992,
	SmartFormatter__ctor_m3299B28E9A742658E3E5978783C0AA37C2F007EB,
	SmartFormatter_GetNotEmptyFormatterExtensionNames_m56A15B799B974C07E44CCAFD7D6E325D962A5104,
	SmartFormatter_AddExtensions_m8A39AE0BE3937BC0FB1D1C9F6A71ADC0052D8102,
	SmartFormatter_AddExtensions_mD68C822FFC4F4FD20745C9CC54DB0F8771CDC8C9,
	NULL,
	NULL,
	SmartFormatter_get_Parser_mFE5C5B7AAE50F5939B3DE060C3A8CAF2F5CECA1B,
	SmartFormatter_set_Parser_m42DB5CD7BF8BD4F7FC667C34D03E570787661017,
	SmartFormatter_get_Settings_m72BCAF6A8DF029BC22F2F6317CCD0E76C29294A3,
	SmartFormatter_set_Settings_mDF0AD730BE1812A0F22644CEF6962EC900C03C70,
	SmartFormatter_Format_m43D58AE58C7CD4B00DE1F129F7D3B736B6E26352,
	SmartFormatter_Format_mD7818F4C7C8D70C4BCDCE12AD0BB16EF6D9AD759,
	SmartFormatter_FormatInto_mE206546C1414BC59AB48F599A2692E43F2E6EC86,
	SmartFormatter_FormatWithCache_m9ECB0D0172914F15A2665F8298DDD760B516FB9A,
	SmartFormatter_FormatWithCacheInto_mEBB38DB761C62CB7AB3065960FDF3BB6648FF9A8,
	SmartFormatter_Format_m948383FDE793AA21CE26AD131C8C2272441CF8E7,
	SmartFormatter_Format_m95F7FE7BE6FC224DFAE66B45200E30936E9C3AEC,
	SmartFormatter_FormatError_mED0E6137A09124962C85E30D33ED63085E5FA3DF,
	SmartFormatter_CheckForExtensions_m6039A81A2C7CA763D1C633D966B4A42A0EE86F8F,
	SmartFormatter_EvaluateSelectors_m7EE5356BAA4055D9734832ADEC6B93016414418E,
	SmartFormatter_InvokeSourceExtensions_m0A4494CD18FCD7E2D69B698F50C8AF34B3525797,
	SmartFormatter_EvaluateFormatters_m988ADF4BCEF2BD1B7FDB0D39D4A53EBD26D3B61D,
	SmartFormatter_InvokeFormatterExtensions_mCA55B56C369B71034FD400974F283521623B062D,
	U3CU3Ec__cctor_m8A7E682F59821B1B14E6F0C5B1717A940714699D,
	U3CU3Ec__ctor_mE56A15E0E22FE75A251FD6817999AFA415EDCB6A,
	U3CU3Ec_U3CGetNotEmptyFormatterExtensionNamesU3Eb__12_0_mCA14B7E2909FDDA06CC9D4F504516E57E2D0D5A6,
	SmartObjects__ctor_m82B92502B31A13D2FF7E95BB335DDA7C76552993,
	SmartObjects__ctor_mD13FAEDE0F8DE428DAB7125964DEC5C445BC1B42,
	SmartObjects_Add_mAF45C8FA419B8841C25F61AA9FFC9B0FD8C50BF1,
	SmartObjects_AddRange_mCDE2A1D43A199261E239A5F83CC64C15B82B6984,
	U3CU3Ec__cctor_m52C0804EF6D15F40ACC5088C70240498D558DBDB,
	U3CU3Ec__ctor_m2679C13017C4B2FB7A4D11C78938A50037EF4929,
	U3CU3Ec_U3CAddRangeU3Eb__3_0_mB58EAA24647D396DFF41B912E215F06A01504904,
	SystemTime_SetDateTime_mB934A8D1E39DCA853CE944C5D1EAD3276DD70AB4,
	SystemTime_SetDateTimeOffset_mF2AA356E1D2D86806AE73C12ABA56C4D71D46A65,
	SystemTime_ResetDateTime_m0EB8BACB3BC6A0AD1126877185F62F07936AC0E1,
	SystemTime__cctor_mD36E3FC7D60602597D178F97DF6E770ED36EED53,
	U3CU3Ec__DisplayClass1_0__ctor_m7270CF242C3E536A8125E0720847BE85DBF98298,
	U3CU3Ec__DisplayClass1_0_U3CSetDateTimeU3Eb__0_mABFE1EA30C49F3BC6A933101A6482F7A94DB56B6,
	U3CU3Ec__DisplayClass3_0__ctor_mB8A09AAE5604E5150EAC83CA31A142B124E41C8D,
	U3CU3Ec__DisplayClass3_0_U3CSetDateTimeOffsetU3Eb__0_m5FF3D516BCD9066F75BEE0F6B340E64271954BB0,
	U3CU3Ec__cctor_m5B4ECBB31CF43D2DE9641C7A620426A4DF69FF24,
	U3CU3Ec__ctor_m287CAC9D675FE3A0F42878A6A712780710A50A1D,
	U3CU3Ec_U3CResetDateTimeU3Eb__4_0_m2ECF6D3DA83FE03CC3B0B1FF2607D7DA539266D2,
	U3CU3Ec_U3CResetDateTimeU3Eb__4_1_m0832954B62DCCDE34C84FD4D010ABD0A677ED5A5,
	U3CU3Ec_U3C_cctorU3Eb__5_0_mD6F388760D3F769FA12D167A1F79034991636F8D,
	U3CU3Ec_U3C_cctorU3Eb__5_1_mAD3E59C2401CDDE2AE2D531F5FF375D6E55E8343,
	FormatDelegate__ctor_m9F63037B73F60119CDB04C40894E13FF3A233CBC,
	FormatDelegate__ctor_m45E40092A5A25091E0F5A795711273AE1F9AE3B5,
	FormatDelegate_ToString_mB90BDF563A36446326DAA089BC557AF05A5D7A2C,
	PluralRules_get_Singular_m2C87AE7CE3EEA0E7CD339C2BC28E5F91BCFCABE0,
	PluralRules_get_DualOneOther_mA4080DF68B812D424A49C49EFCE45383B1B5FA13,
	PluralRules_get_DualWithZero_m65BE61D2E884E5A1B2FD6708CD072683D3CB7E33,
	PluralRules_get_DualFromZeroToTwo_m000A1CAA48D31A6193F6FD0343FCE531F9EC03A2,
	PluralRules_get_TripleOneTwoOther_m37F42181B9E4A536DD9ABB0FB8CCBF7FD04578FE,
	PluralRules_get_RussianSerboCroatian_m9787101BFD3BDDB54293A152FE6B5B792C09BD88,
	PluralRules_get_Arabic_m3A15626E7EFFB121B29D88714037CAFB1E1AD7F6,
	PluralRules_get_Breton_mF9679C09F774D2400F7F8369A2B93C94A2D41D08,
	PluralRules_get_Czech_m99DFACF1BB621E132270E29FD5ECDB30DB916DC1,
	PluralRules_get_Welsh_m27277F52D476AC8DF846736F4172107E4BD42AE0,
	PluralRules_get_Manx_m10ECCF9D363C879709400300D9784EF7299F52A4,
	PluralRules_get_Langi_m7D1A7AE2340E86AE5732219737C981FC36123590,
	PluralRules_get_Lithuanian_m279C8404FE08C4C7D9773382D36590104CA132D0,
	PluralRules_get_Latvian_m260D30810C4A1790C59BD880E85800A0CD4CF70B,
	PluralRules_get_Macedonian_m80F594A713B27B16148C086EBF8C4CD895DB1356,
	PluralRules_get_Moldavian_m290B00C70E5D3376DE14A9F699E7EBAB2080E1A0,
	PluralRules_get_Maltese_m2C2F925193021257D6D2860FD41E8493A893E26D,
	PluralRules_get_Polish_m1544CD0CDB4596F313CC6AD1F088623B4C4C0FF0,
	PluralRules_get_Romanian_m44BE080709A688A1141C535E2AECFC3A5BC3C52B,
	PluralRules_get_Tachelhit_mE08A447FA6D63FFE85507D36B116A219D5A10A9B,
	PluralRules_get_Slovak_m683830BA253144EA0AC0D9CE99C35EC892206BEB,
	PluralRules_get_Slovenian_m61C0AFAF6A9A088F219B412B1671106ABFDF3FFF,
	PluralRules_get_CentralMoroccoTamazight_mDB00959F1DC6634D7526B1E7C6BC610FD6CB6DB0,
	PluralRules_GetPluralRule_mC3BCEB65C440CA6C237EB808B87A2F4C47901CBA,
	PluralRules_Between_mDA8D4100BAA0F76E4E66B929B69C66F049F4D134,
	PluralRules__cctor_mC5AB5F97400E9D7F7AB6D736E97A7C8D789A8187,
	PluralRuleDelegate__ctor_m960632B9CD2F303AEE2A59D7C2EDBEEA7A3C5D7A,
	PluralRuleDelegate_Invoke_m62AB59931D895D02E65BC49E1651B4ABA422D472,
	PluralRuleDelegate_BeginInvoke_m4651103F8348F32E8A32F7B5B37749A0BBF5068E,
	PluralRuleDelegate_EndInvoke_mAD5C9F291C67E38CBD76074BD68983031D57D99F,
	U3CU3Ec__cctor_mFC466118C4817D715C94057CE1E63AD55AC940C7,
	U3CU3Ec__ctor_mE052DF8096EB8DCAEBD49287D2C50BB7C1560D67,
	U3CU3Ec_U3Cget_SingularU3Eb__2_0_m208BEFDADFBC6299983E9C9B4B7F02284F29C003,
	U3CU3Ec_U3Cget_DualOneOtherU3Eb__4_0_m68B6F3141AB1DE7D68454094554D8D8D82F28D8C,
	U3CU3Ec_U3Cget_DualWithZeroU3Eb__6_0_mF8849E74374CFB28E2EA3724CC8DA70496F898F7,
	U3CU3Ec_U3Cget_DualFromZeroToTwoU3Eb__8_0_m16E29988526CFFD8BC86F98B495CD51C363B520B,
	U3CU3Ec_U3Cget_TripleOneTwoOtherU3Eb__10_0_m62C02CFC46901BEB4D2E51779514115F2C08AF6F,
	U3CU3Ec_U3Cget_RussianSerboCroatianU3Eb__12_0_m8FDCE6C82579C9DCB89D2EB5A482BF66F71050F0,
	U3CU3Ec_U3Cget_ArabicU3Eb__14_0_m93D2BFA80C8B9CDECE0916FB3C63D55CE3DD6C23,
	U3CU3Ec_U3Cget_BretonU3Eb__16_0_m20014F34275D4EECD459186CD893372E63073A22,
	U3CU3Ec_U3Cget_CzechU3Eb__18_0_m8DDEDDE1ECA953029018DE08B52041782B88D971,
	U3CU3Ec_U3Cget_WelshU3Eb__20_0_m8DC15140493F7ED0F9DBEB000D126A5C24C05A5B,
	U3CU3Ec_U3Cget_ManxU3Eb__22_0_m0A7B8C055C65913660BD10EF166D6D8FA4441D22,
	U3CU3Ec_U3Cget_LangiU3Eb__24_0_m4EF2E9558B4DAF22664A68B41AF42B739D582297,
	U3CU3Ec_U3Cget_LithuanianU3Eb__26_0_mAA86ADC22D7DD202D28FC8729422E7F1A30508F0,
	U3CU3Ec_U3Cget_LatvianU3Eb__28_0_m29D88F562038ADA6923928BC2E4913A3480AE1DE,
	U3CU3Ec_U3Cget_MacedonianU3Eb__30_0_mCC639A8202526C27D25BB6615C76F72BB76D451D,
	U3CU3Ec_U3Cget_MoldavianU3Eb__32_0_mF661F8F3D46CF994461C90D100FB4A171447CFE3,
	U3CU3Ec_U3Cget_MalteseU3Eb__34_0_mCAE873B873A29416E38C5702FBF22952D3629296,
	U3CU3Ec_U3Cget_PolishU3Eb__36_0_mDA421462AC2140C41429817F693F66AD4EBF968E,
	U3CU3Ec_U3Cget_RomanianU3Eb__38_0_mB7112B6180A3FCE7A6C617828DACA78C6B940323,
	U3CU3Ec_U3Cget_TachelhitU3Eb__40_0_m0614B9D2EBF7F25E1A7FD24BE3A4678FDAED1D83,
	U3CU3Ec_U3Cget_SlovakU3Eb__42_0_m07B22F72A61D60B28F48784932568E6F81AE3E4C,
	U3CU3Ec_U3Cget_SlovenianU3Eb__44_0_mABD8E5EDE6D5051B67B405BB8A34156AE0B3B2E0,
	U3CU3Ec_U3Cget_CentralMoroccoTamazightU3Eb__46_0_mA1C1940D32B959C8CE8D208009C391CB9CA52BA0,
	TimeSpanUtility_ToTimeString_mACB1DA4EC9BF8575F7E9825E48F4D7243E7A1855,
	TimeSpanUtility__cctor_mEA5840220333FBCB685AC174AC931B49D7BBCD55,
	TimeSpanUtility_get_DefaultFormatOptions_m583DFFDB410BEFC8EF7C4FCFB88F4564C17D2C81,
	TimeSpanUtility_set_DefaultFormatOptions_m343F7F546FDF57A4CBC192D703A344876EB730D7,
	TimeSpanUtility_get_AbsoluteDefaults_m6CCA8D9B58F6DD809B3417C4EE552253B45A937A,
	TimeSpanUtility_Floor_m874DC65104BEE0A3945EB0912942C1EE2CBB9C95,
	TimeSpanUtility_Ceiling_m776F960767EB03FE13CCB95E38873F5E8E290111,
	TimeSpanUtility_Round_m2A0E0E647D5638CDFB1B878CFBD28AFB423A703D,
	TimeSpanFormatOptionsConverter_Merge_m84AF3C2B0D5413ECBCD81BEC1B27DA99CC66981A,
	TimeSpanFormatOptionsConverter_Mask_m32A86F9069A6988D402F875A7EE521DCEEA0D1BB,
	TimeSpanFormatOptionsConverter_AllFlags_m99688E79614FB14AF5C51EC65735262D203BC19C,
	TimeSpanFormatOptionsConverter_Parse_mD6D2CF787BB673DEF8907335663666B068480478,
	TimeSpanFormatOptionsConverter__cctor_m40404F78BDA44AC924EB344FAD9DDE5C64795654,
	U3CAllFlagsU3Ed__3__ctor_mC79E9668766055ED89139D401199B8D21FC35A47,
	U3CAllFlagsU3Ed__3_System_IDisposable_Dispose_m39E9DF5DCCCA61F7675F3907285F9D95EF034AD0,
	U3CAllFlagsU3Ed__3_MoveNext_m5796B61E9B5DE077039EB3C9A0548F3582A01D40,
	U3CAllFlagsU3Ed__3_System_Collections_Generic_IEnumeratorU3CUnityEngine_Localization_SmartFormat_Utilities_TimeSpanFormatOptionsU3E_get_Current_m834F97AB6507F599EA62A558DB909BBE9D367455,
	U3CAllFlagsU3Ed__3_System_Collections_IEnumerator_Reset_m23BC97A31705736A684C0498842D7A34D8824EB6,
	U3CAllFlagsU3Ed__3_System_Collections_IEnumerator_get_Current_mDB1A6ECDE75EC35BF98FB63BB7B9B095A75DAC68,
	U3CAllFlagsU3Ed__3_System_Collections_Generic_IEnumerableU3CUnityEngine_Localization_SmartFormat_Utilities_TimeSpanFormatOptionsU3E_GetEnumerator_mE081D96C0FEA4B3FA86C7DE485BC1E82FA8977E6,
	U3CAllFlagsU3Ed__3_System_Collections_IEnumerable_GetEnumerator_mABC0E82E38A0F0CECE178A3E28EB632F49DBC402,
	TimeTextInfo__ctor_m2AF78F4F8E4ADB503DC107AB5C59A4955CEBCF70,
	TimeTextInfo__ctor_m68329FE460CE0C26E320CD40001988B3B9469435,
	TimeTextInfo_GetValue_m1B2FF31D9D045025A217F07D28F77CB8496C5C8C,
	TimeTextInfo_GetLessThanText_m133F833815BB2F122FA039540F7C0960A7C20F4C,
	TimeTextInfo_GetUnitText_m272075DAF0D4101A88B5DB59BB5465F91558D10E,
	U3CU3Ec__cctor_m5324F1287FFE01C82A7191D58ED3C2E36FFE0A7C,
	U3CU3Ec__ctor_mB5432B7F957A54F8EA2826AA2747CD4995C02634,
	U3CU3Ec_U3C_ctorU3Eb__15_0_mF2768F5C27C083C8BADAF12C06A54F9F1D50AAD3,
	CommonLanguagesTimeTextInfo_get_English_m2BE2F5E4481D58527187AB57217F6507BDD50F72,
	CommonLanguagesTimeTextInfo_GetTimeTextInfo_m160B71567CE85E2006C8E7FC28268DD00A40F384,
	ChooseFormatter_get_SplitChar_m597C58796422E7010C802A500015B437AD2863A1,
	ChooseFormatter_set_SplitChar_m0A1A04AFB8449AF579DEA08116B3C0B779F8C039,
	ChooseFormatter__ctor_m61180AC1109DD8487C83768B0B30EBC9908CB9E4,
	ChooseFormatter_get_DefaultNames_m15B828E73C6FB4A44AD2B76A887017199BDEE22E,
	ChooseFormatter_TryEvaluateFormat_mC845B97580DC1663AE72A631A3BBBF5CDD5150CF,
	ChooseFormatter_DetermineChosenFormat_mEEE90DAFF2E5881C4DA63C6EE363D511BF71A187,
	ConditionalFormatter__ctor_m1E302CBFC62D164313F07A1793B7546C6A8A4DC4,
	ConditionalFormatter_get_DefaultNames_m6D539C17C2ADDF1961B72725A0B9B0EFDCCFB408,
	ConditionalFormatter_TryEvaluateFormat_mD4CC9D315ADC389EAB09D4AA12AFB34156E493EC,
	ConditionalFormatter_TryEvaluateCondition_m8B2334659049E18E4951950A9692D2D5D63FE95E,
	ConditionalFormatter__cctor_m929DF55169E6B40B06A827EDEBF529FEFE3E0DFC,
	DefaultFormatter__ctor_m3465921ED231898699627B3C8EC71CC5479891FE,
	DefaultFormatter_get_DefaultNames_m62E5EF0581A031C5882DC8E80406C585C5C9CAF3,
	DefaultFormatter_TryEvaluateFormat_mE5DEFD25A022A128B4859B1CC6EEA65EAE6CBAEB,
	DefaultSource__ctor_m6C5BA9D7268AFBD577A6C136DDA38CCAA94B6BFA,
	DefaultSource_TryEvaluateSelector_m9599CB8D758AEC4C0C57FC1A7B619C42847397EB,
	DictionarySource__ctor_mE5B6D073E629B96AF093440EA269C5974131790A,
	DictionarySource_TryEvaluateSelector_m0CCB7BCFB8DAE45F08AB4B1ECB05A38C0795C942,
	U3CU3Ec__DisplayClass2_0__ctor_m46C24DB34E58B3AE8F2C9ED1D99DB182DA0506EA,
	U3CU3Ec__DisplayClass2_0_U3CTryEvaluateSelectorU3Eb__0_mADBC19AA2C20886D096019E3EFDA45E382E7F8CB,
	IsMatchFormatter__ctor_m0DE1F209D7BC0886C9D6CBABE32406E5D27E36FB,
	IsMatchFormatter_get_DefaultNames_mF2873B5E99BBC3A4C13E8E5C29F8E0A4FECCF78B,
	IsMatchFormatter_TryEvaluateFormat_m97FD4CDDF5FF36E7E2883266F52C126BB4911D4D,
	IsMatchFormatter_get_RegexOptions_m72245A39C7A24F8684A916FCCBF768D598ADAB23,
	IsMatchFormatter_set_RegexOptions_m8C016FDEE6C253827A0EE2915A82F86856755C71,
	ListFormatter__ctor_mF82AF58A87AC188316BB280245F3ED69CE50F888,
	ListFormatter_get_DefaultNames_mDB85E0438298AC4F85B51F87C333FDBBBC7244B1,
	ListFormatter_TryEvaluateSelector_mD1DC4858B7988ED5C07637E649B980752298997A,
	ListFormatter_get_CollectionIndex_m6D91CD03F9F63578613A7B96246B20111BE6DD21,
	ListFormatter_set_CollectionIndex_m283D0E62FD77ADA2076F30FAB6084E7B23871149,
	ListFormatter_TryEvaluateFormat_mC8FCC44429D86B96143A078A4322B91D893EE95D,
	ListFormatter__cctor_mA513715E82E8A7C95A9238533AE9A3931FC078B4,
	PluralLocalizationFormatter_get_DefaultTwoLetterISOLanguageName_mE8CE0B61A3D10B4A4215795454C5C3E1D9C7E4C0,
	PluralLocalizationFormatter_set_DefaultTwoLetterISOLanguageName_mACFAC6EB51A69FE35968CD694B5FAAB635A57544,
	PluralLocalizationFormatter__ctor_mED41BCB0B15719A3B061AB9965888A8A2BA76229,
	PluralLocalizationFormatter_get_DefaultNames_mB5530968E8FBEEAF501D54BFFEFF75471AD79AC4,
	PluralLocalizationFormatter_TryEvaluateFormat_m5AAE1BF9B535D34D8530EDDFA233427936BE57FF,
	PluralLocalizationFormatter_GetPluralRule_mF9C2631A53845042ABC990C61C98A075CDCF54C9,
	CustomPluralRuleProvider__ctor_mB636D1BFCA9D2D567F8FFE45BE7EDB69C517C700,
	CustomPluralRuleProvider_GetFormat_mFC579F7BF49CED3AFD42CFB04D31CF38FA249389,
	CustomPluralRuleProvider_GetPluralRule_m70BD1E3D0C4C790CE5F7B3B76650BCDF1E648D7B,
	ReflectionSource__ctor_m20F812795E558420C72F1BD33C328979F153DDE9,
	ReflectionSource_TryEvaluateSelector_m36589BD871F8E52B85EBEBECA4C62B455248F392,
	U3CU3Ec__DisplayClass2_0__ctor_mFEB4F0DE82E9EB1E9EE357AA71EF728E9F204F44,
	U3CU3Ec__DisplayClass2_0_U3CTryEvaluateSelectorU3Eb__0_m2F0A003767C47B32AE88D1E7E3D723AB6D1A4F3D,
	SubStringFormatter__ctor_m8EA75160E91CF76DF89F4EC96967C42B16591C38,
	SubStringFormatter_get_DefaultNames_m816FC604F28031812AFA787E7CAA51D5F69EB689,
	SubStringFormatter_get_ParameterDelimiter_m38617916B4CC10962FC85059A3FA78221357CF3C,
	SubStringFormatter_set_ParameterDelimiter_mF6EA22056D6AB5E1E942EBE6D9C1886F676A5F4B,
	SubStringFormatter_get_NullDisplayString_m037336232D62F1EF4B64CBE0BD86D4E85954718A,
	SubStringFormatter_set_NullDisplayString_m851ACAF644798AF810FB08C8466B89FBAA2C0B2A,
	SubStringFormatter_TryEvaluateFormat_m84A3B2B3750BD7023DC31906DCA2A7A4A10FEB39,
	TemplateFormatter_get_Templates_m41DA7A985CE840A9CACF01538A10A1BA4E2C60B9,
	TemplateFormatter__ctor_mC98B261F38F632511030CFB5B6ED412AF47CE528,
	TemplateFormatter_get_DefaultNames_m6671054519A1B7649F504E0A4D1710E78D69EEEA,
	TemplateFormatter_TryEvaluateFormat_m4BD0FD2450299F5BCC44E23FF7ED62371CCBD3B2,
	TemplateFormatter_Register_m486F8EE85E7E1D676C4ED33A92A0C304B23C6DCC,
	TemplateFormatter_Remove_m61A48946A34817C3B2B6B0D85CC95D56C106D5C0,
	TemplateFormatter_Clear_m3EF3F5E09AACC5A13801523D0FEF83EA54D774BC,
	TimeFormatter__ctor_m9B91312EB9107E6C8DA554349F7519E10B048074,
	TimeFormatter_get_DefaultNames_m40907DE50FB1ED2398545EC953D44BBCD0FFF1F8,
	TimeFormatter_get_DefaultFormatOptions_mF0E30F63EFEDC1F0AFF915C62FF97D9102B992EC,
	TimeFormatter_set_DefaultFormatOptions_m9051239D08C4F473D3E2BB0BF391E5DA9371FBBF,
	TimeFormatter_get_DefaultTwoLetterISOLanguageName_mD4B269B885C2D70DD797380C227452F6861EF34D,
	TimeFormatter_set_DefaultTwoLetterISOLanguageName_m943DA24A291F148D77DC43401389D5C01C55284E,
	TimeFormatter_TryEvaluateFormat_m8FC3B03137F3EA0CD9871778BD77CE582F6FE82E,
	TimeFormatter_GetTimeTextInfo_mFC7410C7C9264D71154970A09F2155C8AC0D1499,
	XElementFormatter__ctor_m71A305F5E2A1F2C66D066057ECA7E2C27CD70DE8,
	XElementFormatter_get_DefaultNames_mE11F8B655BA48802C1AEE292D59DAC64DD084952,
	XElementFormatter_TryEvaluateFormat_m039A77B7EF3B13957E37C222899B7E19B84DFC9B,
	XmlSource__ctor_m708C6638A0D81A0BBACDF26EB04AFFC4D074BBCC,
	XmlSource_TryEvaluateSelector_mC8BFBC736CE0AB20A762F6F4F5ABFD15800BA89B,
	U3CU3Ec__DisplayClass2_0__ctor_mE01F9F6DE9D0F57E8F22807BD3E1B3CBCA940C43,
	U3CU3Ec__DisplayClass2_0_U3CTryEvaluateSelectorU3Eb__0_mEAB076A436F38301BC04BBDF30DAF26775ED19A5,
	SmartSettings__ctor_m329FEACDC588265CBDE3CE39C4CE82E5D6D9D759,
	SmartSettings_get_FormatErrorAction_mCB5304DFF89D228B06468725590A3EAD32412D23,
	SmartSettings_set_FormatErrorAction_mC65ECF66D66E9CBDA00D22E7324CBE1D7B720A09,
	SmartSettings_get_ParseErrorAction_mF48F8D1F90F4F5C93FE25BAB59E5824F51E7E096,
	SmartSettings_set_ParseErrorAction_m73A32BA0A363AB97F69182BB975B17491B56EC44,
	SmartSettings_get_CaseSensitivity_m2BFF138F2744EE59FEAF6B7FCE3082129799CE36,
	SmartSettings_set_CaseSensitivity_mA08ACC14CDB370EBC259168EC72613E2A7B53963,
	SmartSettings_get_ConvertCharacterStringLiterals_m97B811F159CD0DB549B983BCA6740AB1EEDE0750,
	SmartSettings_set_ConvertCharacterStringLiterals_mB6E22CAC40D5CFE7B9497E6FC46FBB831D5C1FAD,
	SmartSettings_GetCaseSensitivityComparer_m92E1AC3DE57533B91D8A4364DFB7EA7C804806B1,
	SmartSettings_GetCaseSensitivityComparison_m53F2D447BEABB492B224620116CA6C3E66DC50B4,
	Format__ctor_m4299ED61255B18D7C6CAF9DF768C40740D817E91,
	Format__ctor_m5DA8ADC050432CD296620717ADABC7952763E7B8,
	Format_get_Items_m0992D7B02BC652626810D9A186FBEC4A74DD1E20,
	Format_get_HasNested_mE2F9AF3CABE8172CC1DACB55F27D4F939C1C2786,
	Format_set_HasNested_m143C9DF0F559E6D495AB5C3A2D38B689C6FE1E60,
	Format_Substring_mF151318B2037A0B6B9F7A9E4DC05F4BF17D230AC,
	Format_Substring_mE70F01D1A3314FEA6C7EBE809EBC6071255DE6D5,
	Format_IndexOf_mBA6040BF5774356A0BD0BF8F4A88D66C2A4A7254,
	Format_IndexOf_m5D21639B2A573FE61A6E1B9722656F8A470BE7D6,
	Format_FindAll_mA73792B5FE23BCEDF887F3B9F12DF1964379CB4E,
	Format_FindAll_mFB7E3C1174DF351BF5CEA3DCCAFCA059C2505372,
	Format_Split_m1919E700F0321FE74B484AAA8772335C6C5CD0D6,
	Format_Split_m5AAB70255F69FB04DDD3BA9A5E01A4EF529FBFE7,
	Format_GetLiteralText_m490BAFDA416B166006F98738ED88DA0D0C1BC527,
	Format_ToString_mFB5CF793CD9A442F89B5462A82A560A03390056D,
	SplitList__ctor_m5EEB6DBA3F74C56E0430BE70551396FA55B711DD,
	SplitList_get_Item_mE3C3E8C7457E84FEFFBAE689FE6E109967F6F908,
	SplitList_set_Item_m3F3084FC555DDF9C832C5B55FF4DB211D91BFFD8,
	SplitList_CopyTo_m231F708E9DB3C399288D43C97446AECAD97FA24F,
	SplitList_get_Count_m75ACE910BABA5D175EE14E4C4BC8934FB0E8277B,
	SplitList_get_IsReadOnly_m61BFB77E563665F2464AE75D664BCF34EF0B266B,
	SplitList_IndexOf_m56EF7730D832501D839E066284F3DE3CA5176DE5,
	SplitList_Insert_m17117F22DD7FBAF6CA7DB50EC8CB292E79FEFF9A,
	SplitList_RemoveAt_m6521602D8B905DA306C6C2F2F728CCA394D99C78,
	SplitList_Add_m8559EA9B6BAD574022AF7299A5696A58F783C55A,
	SplitList_Clear_m1527DEAAE32A2CECE0454A2DB703BAFD36F5C5B7,
	SplitList_Contains_m12F4B85CAE117A33A2135FBEE337FBB45E2FAB52,
	SplitList_Remove_m0A12DC433A469AA53386BE35ADC1D89A67A2C789,
	SplitList_GetEnumerator_m25E5001A2A351296E47A089E3F838E4F8E141EFE,
	SplitList_System_Collections_IEnumerable_GetEnumerator_m2FB09B8079AE028BFB325658353DBD701A3A3636,
	FormatItem__ctor_mC089E34FE24E67FF5D3B1BDA62BDD33A397D7DFB,
	FormatItem__ctor_m2D8613B1B4C73B46E450F1925BFD033D243A1CD7,
	FormatItem_get_RawText_m32DC9CAC468C7C782EF1102A946642B813ABF530,
	FormatItem_ToString_m16B9E383A7FC4CE753864A12EC63FB2FD937A4DB,
	LiteralText__ctor_mD5DBC69D442F979BC9C7680A4021A5C3CC225327,
	LiteralText__ctor_m12758A6A5EC727CC30B00AE71A4DDE1CAF7DD89F,
	LiteralText_ToString_mBEF3EC6538A879747D87904B4284A9B25C9DA4AD,
	LiteralText_ConvertCharacterLiteralsToUnicode_m4F85C3C36B978E2CF73C3EB1F1041AC1BA254EEA,
	Parser_get_Settings_m1014D5467F507176DEF8ED285653BABEC274BE3D,
	Parser_set_Settings_m0A61999120A9A405B80E7E0A08B1CE274564A5D5,
	Parser_add_OnParsingFailure_m635C88244453080AE0BA4E3D9600B647458038A4,
	Parser_remove_OnParsingFailure_m72B70DE79B5FB54C6390C123F2F70FD9D425BC26,
	Parser__ctor_mE9A01F0A2DA0CBC200CB7619FB27F9146C5D71DB,
	Parser_AddAlphanumericSelectors_m61ED37C993D6CE573D7F795CE7FD445A1BBC5DE9,
	Parser_AddAdditionalSelectorChars_mEDAC91BFD9F1373A6CA0931D0476857FAC2CA3A2,
	Parser_AddOperators_mD4CF7685F2C26A5C2203B7C5256E2C74C6D5FF3D,
	Parser_UseAlternativeEscapeChar_m90CE62D9E6B39EFBD24BC13B12223C20BE314C54,
	Parser_UseBraceEscaping_m30D437A03BA8E0596A669F6EDB3634071D68ACEC,
	Parser_UseAlternativeBraces_mD97D4AB2565A804100270A184E4910192C720D68,
	Parser_ParseFormat_m219E4CC2CE1D1C56C579FC7FA14FB86137758B24,
	Parser_FormatterNameExists_mAC2581B6F968D75ADD19DB9A24D49DD4CA452CB1,
	ParsingErrorText__ctor_mA30B61821BF51A368A6BF647B866E9B99C52A266,
	ParsingErrorText_get_Item_m19757E51DCC534FD2022CC2687457AB3C2868E17,
	U3CU3Ec__DisplayClass23_0__ctor_m8C11AAC45CCD328AD5615938CD27AFCA913BDE27,
	U3CU3Ec__DisplayClass23_0_U3CFormatterNameExistsU3Eb__0_m7EE6D45807D33CECA28A9AEFABCE09084CC46809,
	ParsingErrorEventArgs__ctor_m6404DF2154B9B7B58F2AAED24FE8F47E6BFC361D,
	ParsingErrorEventArgs_get_Errors_mB0C455D7BEF92D40B789BD6534960D342A6254E3,
	ParsingErrorEventArgs_set_Errors_m6947D34F47F515E87906B524998D7B14539AC3B1,
	ParsingErrorEventArgs_get_ThrowsException_m946EC48CA6159C4FC70A3F43C7BCB4A871A0D623,
	ParsingErrorEventArgs_set_ThrowsException_m7B3EA5729001A259A4D94FC698EED3508EFFADFC,
	ParsingErrors__ctor_m4E717C5475500D5A9F86F3DEB7F272152576391F,
	ParsingErrors_get_Issues_m39F3A72D22DF3BA9DFB4966A9679BD2F74564D1C,
	ParsingErrors_get_HasIssues_m88D82BCDE60A9C480AAEFAB461CBAA221C84A35D,
	ParsingErrors_get_MessageShort_m4F028B65CB5B790108BCDCE5D2B6A1945AB1CB89,
	ParsingErrors_get_Message_m47CB71802E44C452394D05ABF8F8319A42E88C0D,
	ParsingErrors_AddIssue_m64BE92BD60351144F18FF7EC5C89E06384FA6461,
	ParsingIssue__ctor_m8605CFE3F48A4B70BD084EF56CCA625EDDE80FAA,
	ParsingIssue_get_Index_mD2BE27CD02934D38D2EF550EA1A6A67B41EF16F6,
	ParsingIssue_get_Length_m8CD30D6F32DA663683F4014EC2FEB3A1EE61F3EE,
	ParsingIssue_get_Issue_m38D87FBEF53CC2D7E6D6CDB7D0295685BAAB5A78,
	U3CU3Ec__cctor_m9743F43EFFE6F64617C60C6C86CDB0113236135D,
	U3CU3Ec__ctor_m5275D13FE6FD09ED6643B83A4EC37764BB8FA8B2,
	U3CU3Ec_U3Cget_MessageShortU3Eb__8_0_m942630FD9F94FFF15815805D2A35C2D10025DDA4,
	U3CU3Ec_U3Cget_MessageU3Eb__10_0_m5A30818EABA2BF862D805798CF379101EB64A3E9,
	Placeholder__ctor_m67EC3BCEC97BF560BBB3A9345D239494FEE0FF65,
	Placeholder_get_NestedDepth_mE4B8FACD85261841166228C366D04AB86F14579C,
	Placeholder_set_NestedDepth_m84722D71AEFE1FB69F2DEF0AFCEBC04DEB59149F,
	Placeholder_get_Selectors_m78F7DF93351F54DE087806895F72F79F986EA38F,
	Placeholder_get_Alignment_m3E772854BBCC095C9771FB454EC99BB99B14BC32,
	Placeholder_set_Alignment_mF5BB752518D2655AF619AEB8DA7223CA4647B5CB,
	Placeholder_get_FormatterName_mF5D38E5E8FC87E3A4A146AB9D131FEBB56462C56,
	Placeholder_set_FormatterName_m2C5E68F209CEFB4EE5981E43B92B94B4F6BE5454,
	Placeholder_get_FormatterOptions_mDE49FDA7EB6482473B96D90208DC150573AD2163,
	Placeholder_set_FormatterOptions_mD5442923FFA2BE790F7C7B2D8B801EC09DC5FAA4,
	Placeholder_get_Format_mCA973EB4035382B627E69FA99CDC0705BFF70AEB,
	Placeholder_set_Format_mB3B13D8313232BDD2DAA389BBFF3B4B40B8B54B5,
	Placeholder_ToString_m620CF2F2D84B474529C24A07965CF3E5A1272754,
	Selector__ctor_m56E8FED296B4E9B0F12FFDEC884D2F5422B5FEE0,
	Selector_get_SelectorIndex_m24C1BF67DD4AC6BD5F81CBEDE4A03024673AEA6E,
	Selector_get_Operator_mF98A9B16551276E1FFDD54F14FD666C96554CB97,
	NULL,
	NULL,
	StringOutput__ctor_m87B1302FD70A5EB6CCDFA12CC73AABA9BDDBFACB,
	StringOutput__ctor_m54596C0CE98DA3D4682B81A064BDBD6ADB45673C,
	StringOutput__ctor_m12959DA09B70097A71EC2C96B8267AB17486FC9E,
	StringOutput_Write_mA9F41BFAFFFF988CBF34A761914D981C3D8DC82A,
	StringOutput_Write_m033581CDB554C76228DDE7EC30D1AC44A8410DB0,
	StringOutput_ToString_m031F9DEA525C699F4D5E6DA0CE7E59476914B897,
	TextWriterOutput__ctor_m4BDDACCE5E317470D2D2E502F73748D0CAA51CD2,
	TextWriterOutput_get_Output_mD45A38D2455C0F3D00B5A3E5B7342EC00CFF2E92,
	TextWriterOutput_Write_m95157623829068CCE3B71EDC31918E753014CC16,
	TextWriterOutput_Write_m0725C57CAF0DC0C91A6728045B105DF6C8887DBA,
	FormatCache__ctor_m730CDDDC9C5B5D10B8F31A37F437D762734D1034,
	FormatCache_get_Format_mAE882A959492DCDDBF659EF0B569C229F12F19C0,
	FormatCache_get_CachedObjects_mE17225AF13E2E80B6CF994AD2A9B93BB5D56CE7D,
	FormatDetails__ctor_m37DA5E98766C30D86BA185FCA67BD4C48304F7A3,
	FormatDetails_get_Formatter_m0BA8C9A471ADB6834DC357B9539CAD2CB5155065,
	FormatDetails_get_OriginalFormat_m7AE1531109BA92201591EF80DF951498766E2DC6,
	FormatDetails_get_OriginalArgs_mECF8AB417C2231A1E08AC81C51132D76AF87464A,
	FormatDetails_get_FormatCache_mB4F9A48972104B8174C37C42902A187E148430DA,
	FormatDetails_get_Provider_mA687395FA8CCAFFED100D1AB503F8B31DC13F93B,
	FormatDetails_get_Output_mCAFE0FF63635B2B2C67D100D1EA875860BDC150D,
	FormatDetails_get_FormattingException_m513380EFD931DEF7CA507C6E84D9D8EDF4ACF728,
	FormatDetails_set_FormattingException_mAC2B4B1F7E1FB137350F704A07D2A4F41319CD06,
	FormatDetails_get_Settings_mB9944B35BC9D263A11C41B98626A9B3848772B66,
	FormattingException__ctor_m37BA1E033051463A749452ACC3AAB2E1975BA5BD,
	FormattingException__ctor_mE5DBEAF25E39B5997692848C0117BB371B03A7AF,
	FormattingException_get_Format_mFE339C33BEF4A2A0837035445F9F39ABACC7EAA8,
	FormattingException_get_ErrorItem_mC84464B150038CE1D2890EA96A673404934AE8FB,
	FormattingException_get_Issue_mF923A1A0C1506356BB4EF87FE5E40C2DDCECCB79,
	FormattingException_get_Index_mBEE83CEB45D7CA0AC582DD212C42D9836221256C,
	FormattingException_get_Message_mC57B4F2A265C31394EBD8E1E8370962B047AAADE,
	FormattingInfo__ctor_m1C8F193FD1566CDD45CEB43CD9610AA5D487B089,
	FormattingInfo__ctor_mC0D692DAF9D03BAA7B89C9B854D938F68FFAB858,
	FormattingInfo__ctor_mAB6C65BACD452B4C377A072AA9C040208E3290AF,
	FormattingInfo_get_Parent_mAE7DD8394040FA6E4825C5A00FEEDA3F2CE2F253,
	FormattingInfo_get_Selector_m8EF09755A5B5F576E417AC4D4024FDB588816E4B,
	FormattingInfo_set_Selector_mE87085F03BA56CFAF6B543A32AAD13BC043B73B7,
	FormattingInfo_get_FormatDetails_mA146969097AEBFAAED5A358DB5214C8576303C10,
	FormattingInfo_get_CurrentValue_m0C4CA3529884806EBDFAACCA14DB526870860AE4,
	FormattingInfo_set_CurrentValue_m2AC459B51D0C34488D8034FCF5B4DC84A4FA9328,
	FormattingInfo_get_Placeholder_mCE1E1C36774E451D671E38FBC60CC2D05AE9B660,
	FormattingInfo_get_Alignment_mDA56BF21C064B7D3CD5E44D42DB28744D4D1EE0F,
	FormattingInfo_get_FormatterOptions_mBAF04070A6C23DACDBC67CDE178B4CF67E6038A2,
	FormattingInfo_get_Format_m2A120E8B3ECF342AB18CBA8B74F85A78DEAADD24,
	FormattingInfo_Write_mC26CD17C3804FC0C5F300105AF65919C64734C45,
	FormattingInfo_Write_m82D9A0343E804FD42652F8F16019540646CCC4DF,
	FormattingInfo_Write_m2340E31A66CF35F3A9183EA5E1F7B1C107882A00,
	FormattingInfo_FormattingException_mFE1A4DA9C904AADF6CD1A6F4095A348EFBDD138E,
	FormattingInfo_get_SelectorText_mF90E6D1CD5EB7A9DC744EA36C6E278030498C6B4,
	FormattingInfo_get_SelectorIndex_m63A49E99EB76502C57A9CA221E43B91020A131AC,
	FormattingInfo_get_SelectorOperator_mDA5A95DEE408CCFFE5ABD90C6FD0793D6F7ABC50,
	FormattingInfo_get_Result_m71F169E02D508C9A46A46D992C8C453C3C41F7C5,
	FormattingInfo_set_Result_mEA4FD28FDF0CBCC443EFC2FF3B214ACCDC9A9C66,
	FormattingInfo_CreateChild_m5BB5EF746CDBA831EEBA28C0E7FB9ED889048EAE,
	FormattingInfo_CreateChild_m7EE900A7DCDDEC3F91293EFABE99D1E2EB8BDA5E,
	FormatterBase_get_Names_m32DA01F538339A417A3D0F160B0596ADC57E0659,
	FormatterBase_set_Names_m39C8D01FB66F9744091305ED0196A8E718609271,
	NULL,
	NULL,
	FormatterBase_OnAfterDeserialize_mFCF8F39B4F9FC9E54C1AD48B533355B1EF8E39B0,
	FormatterBase_OnBeforeSerialize_m69A492B9FF21A59617E8B8960E6FB7A73F2D3035,
	FormatterBase__ctor_m8D992BEF29DD0BE65216473DB477FE331FC1685F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LocalizedAssetDatabase_OnLocaleChanged_mE13BD8BD2DAEA616372EE885CD5E0A968984B330,
	LocalizedAssetDatabase__ctor_m5DB4A3C4C6D49D7D23803BDF4A9C1535D9CD0B16,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LocalizedStringDatabase_get_NoTranslationFoundFormat_m2AEE5EC4131DF6176C59DDE41659459CBE8C49EE,
	LocalizedStringDatabase_set_NoTranslationFoundFormat_m4C2C11BBAF680B717CF780FDA68038B150B37253,
	LocalizedStringDatabase_get_SmartFormatter_m800491DEA7C2B6E4D07E53B84CDB0C4E92A12F10,
	LocalizedStringDatabase_set_SmartFormatter_m4EAE4A4B2B169C876FA12327806DE85A82870158,
	LocalizedStringDatabase_GetLocalizedStringAsync_m278295DD9B4E4B0A5FD91F19B4BF3CEE1D75909E,
	LocalizedStringDatabase_GetLocalizedStringAsync_m5F0B27E1B8E98317943FEEC30CA7113FC5904068,
	LocalizedStringDatabase_GetLocalizedStringAsync_m87A233578CEA0CB2FB0FF7A6E37D4110798B2948,
	LocalizedStringDatabase_GetLocalizedStringAsync_m87C0F0141F7FE9FEBA7EDB918ECD2CB13CF1AE74,
	LocalizedStringDatabase_GetLocalizedStringAsync_m8B6DE04F6C1E16647EA5350D0717E293770CF5D7,
	LocalizedStringDatabase_GetLocalizedStringAsync_m75B4AC71488062A45BAC728F5AB5AF98F593E222,
	LocalizedStringDatabase_GetLocalizedStringAsync_mBAA39A741108F8DF76B040C72734FF0C7A4470BD,
	LocalizedStringDatabase_GetLocalizedStringAsync_m0A7939FC200CD3725373A58DD1508023E1200E2E,
	LocalizedStringDatabase_GetLocalizedStringAsyncInternal_m654196A83729AC22F4723A09EBC1F1A103159502,
	LocalizedStringDatabase_GetLocalizedStringProcessTableEntry_mAE54BCB8BDDD4DD6B007F3F3398343FD99B87E30,
	LocalizedStringDatabase_ProcessUntranslatedText_m50A6C5D99D2E6C6D4C21C4CB53FE3A7615BA556E,
	LocalizedStringDatabase__ctor_mD338FFC579B687B1A1FD8DB5BC38AF724D481D7A,
	U3CU3Ec__DisplayClass14_0__ctor_m05DA1A826E1449033A774A303FF2D8723919AD28,
	U3CU3Ec__DisplayClass14_0_U3CGetLocalizedStringAsyncU3Eb__0_m1FB02699BB8EF70C16F3645C3DB5C3D9B5E0E709,
	U3CU3Ec__DisplayClass15_0__ctor_m4F548A19A7071BA8C7E02BAF4321C50FEC470EE7,
	U3CU3Ec__DisplayClass15_0_U3CGetLocalizedStringAsyncU3Eb__0_m130106CAF6BA52909F4E29F2D1798BDC309DCB40,
	U3CU3Ec__DisplayClass16_0__ctor_mF98AC837155DE4A463AA1E7493ADF8BD930BFAB5,
	U3CU3Ec__DisplayClass16_0_U3CGetLocalizedStringAsyncInternalU3Eb__0_mEB197A10B6000A1F1356563C6F10475614879E7E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LocalesProvider_get_Locales_m2B83055FCFFBD61FBA7FADC0A0AFD32E87264CE1,
	LocalesProvider_get_PreloadOperation_m9BAEC90B3E0F8547F49790951C1B01187F42E620,
	LocalesProvider_GetLocale_m79EED450D57DA179AF392B6C3B7302A878AE3D3A,
	LocalesProvider_GetLocale_m52FB65768A3A3FA0FAE30187F0DB1B61724CE05F,
	LocalesProvider_GetLocale_m7F8C38604E727E434FFD5F47602AE65CCF9DF169,
	LocalesProvider_AddLocale_mDBC97B063A551A5E0CA43F2CE8266637E4A28FD8,
	LocalesProvider_RemoveLocale_mC94CC3CDF27F090FD5C2D08EE588145AB8E28B4E,
	LocalesProvider__ctor_m253757B01AC7611F849117297E27211D7F5C61A8,
	LocalizationSettings_add_OnSelectedLocaleChanged_m6F97509E7A0D79555A9F54687BCB917018AE988A,
	LocalizationSettings_remove_OnSelectedLocaleChanged_m4DE58F84877FB5C33D8280C8AFE9978AAC8650D6,
	LocalizationSettings_get_HasSettings_mE1048773C95C4A8FCAACA00383DB10BB8FB43543,
	LocalizationSettings_get_InitializationOperation_m84D4F3BDD9AFE154FACD3989F2190E76DDB95665,
	LocalizationSettings_get_HasStringDatabase_m706D7DFC0052CFFCCA71A39B323D4237AA1091EF,
	LocalizationSettings_get_Instance_m858475AA219C1B2ADC57A991F16B58CA1F6AD097,
	LocalizationSettings_set_Instance_m40B407376EB537C12E598027AA3555A588D47B6D,
	LocalizationSettings_get_StartupLocaleSelectors_m7294CC7A7E487A26CE8860CC96FD2DC49251A286,
	LocalizationSettings_get_AvailableLocales_m01933AB3F94AF4900DDC8A67E65007052706E503,
	LocalizationSettings_set_AvailableLocales_m2340D2D312A1DFE9055C8F16F43D03CF5C1F5265,
	LocalizationSettings_get_AssetDatabase_m56016766FAB0617AACDED7EF26685A27D942FE44,
	LocalizationSettings_set_AssetDatabase_mAEDB65F107C7386D2A86ED40548ECEB94B328E26,
	LocalizationSettings_get_StringDatabase_mC7CEAB5CF022465EB1FD2CE0BF5185F7B620E248,
	LocalizationSettings_set_StringDatabase_m630CEE1507C8D5EB3A4B4AEBE1749C1AB7160AFE,
	LocalizationSettings_get_SelectedLocale_mDA8C08BC2F7EAC6D8F951C9ABC44AD8BA440177D,
	LocalizationSettings_set_SelectedLocale_mF17B5B817B8011A1BD8985BF4E65543F5204B3A3,
	LocalizationSettings_get_SelectedLocaleAsync_m1FAB63CF6A814224E6D66ABEF2D0131EEA29D61D,
	LocalizationSettings_add_SelectedLocaleChanged_m31E7AA89EDEEC4100AE174DCF8675B18A01668E2,
	LocalizationSettings_remove_SelectedLocaleChanged_mCD7B355A524E58C47603BCB90D17940BB092ADEA,
	LocalizationSettings_get_ResourceManager_mD14837B26976031CC31E9A5A260757C004E4ADBF,
	LocalizationSettings_OnEnable_m52953BDC7A9FDB6D2B395CCC136A939D7215ACF0,
	LocalizationSettings_ValidateSettingsExist_m434862921AFE7D6B3E0392623779249CA8A07A9A,
	LocalizationSettings_GetInitializationOperation_m42587A6731081F943506D6BD7D5D26617226BE4B,
	LocalizationSettings_GetStartupLocaleSelectors_m252A3D5851E1C10ACF91E66AB6B146FB11D525C1,
	LocalizationSettings_SetAvailableLocales_mECC9C5ADFB095B8E4A069DA872C71A002286CF0A,
	LocalizationSettings_GetAvailableLocales_mFA3DEA197923012CA8CB0A845121C71C91B4129F,
	LocalizationSettings_SetAssetDatabase_mB391C8081DAAE79605FEB1E001F753BAAEBEDCE7,
	LocalizationSettings_GetAssetDatabase_mB69C883E989C6A01DE4C62349559773B658D49A9,
	LocalizationSettings_SetStringDatabase_mDB2A8BB01A3D73C382F3758F6E52CE2A598FA7FE,
	LocalizationSettings_GetStringDatabase_m17A70537DC0537FC601C47D37B67626F6348C8B6,
	LocalizationSettings_SendLocaleChangedEvents_m5399D1AAB20EEDDA9785BB1F7548DAF360DA13C5,
	LocalizationSettings_SelectLocale_m0D60AC69C8B4B8CCC9FEBCF3689E88EA25119D61,
	LocalizationSettings_GetInitializers_mC1F1EF3DC73D9C61CFCC285440679E9F492293B7,
	LocalizationSettings_SetSelectedLocale_m051078C141B9DF1EE42C5FD9C63006E30541D069,
	LocalizationSettings_GetSelectedLocaleAsync_m5758BDA1BE9093161F662121BDCE2752933194D3,
	LocalizationSettings_GetSelectedLocale_m33FDAFBA720F38EFE0EB1532989938F89C71D401,
	LocalizationSettings_OnLocaleRemoved_mF723AFABD605B5CC6810119256E603A14DBF4555,
	LocalizationSettings_GetInstanceDontCreateDefault_mA324B5DDFDD5D55BCFC076CC4BDFD8637748DDBE,
	LocalizationSettings_GetOrCreateSettings_m51DD37093BD77E2EB0526B97877B43369E48162A,
	LocalizationSettings__ctor_mFCFC7B608AC4457BC174CC8CE165DBCE1C17465E,
	LocalizationSettings_U3CGetSelectedLocaleAsyncU3Eb__59_0_m4BA52F7262D8C75710F4028E54F1F86911E5BF60,
	U3CU3Ec__DisplayClass55_0__ctor_m43552449E8F3FF450196A2FE4E01D269FBB144F4,
	U3CU3Ec__DisplayClass55_0_U3CSendLocaleChangedEventsU3Eb__0_mE14232958D783ACB73EC568DF74DAA07DE419945,
	CommandLineLocaleSelector_get_CommandLineArgument_m3E26219BDFDE509098C0185BA28239BA567EEFED,
	CommandLineLocaleSelector_set_CommandLineArgument_m15D60DBAFBFE9A314EF5B384BF2D4DD334A7865E,
	CommandLineLocaleSelector_GetStartupLocale_mB9E971858855FF194A87ABCDA3709332B9180425,
	CommandLineLocaleSelector__ctor_m99610A26FD2AE07132E1C64CD016FA4E55E13D53,
	PlayerPrefLocaleSelector_get_PlayerPreferenceKey_mD59D9D12F986DAAAFC3D0F82912793B5AEF02DF7,
	PlayerPrefLocaleSelector_set_PlayerPreferenceKey_mBD81A58E8764C827CF0426F8CC7C3CFA49B1E356,
	PlayerPrefLocaleSelector_PostInitialization_m22626CB98030145B2A6AC9D5F741DEDD89ED06CA,
	PlayerPrefLocaleSelector_GetStartupLocale_m2B1A05943C57AD7E3529B49440CE34DCB9370987,
	PlayerPrefLocaleSelector__ctor_mAD37411499979DB03611DFD08864025D39B147CB,
	SpecificLocaleSelector_get_LocaleId_m73C9A4DEAB3F9929BD89AF7F9B1DBDBF15C0A667,
	SpecificLocaleSelector_set_LocaleId_mD33DA26061A3206717966E8822D4B7D5835209F3,
	SpecificLocaleSelector_GetStartupLocale_m1F6CFACA281B08B7758FB35DE69E22C798006FD4,
	SpecificLocaleSelector__ctor_m88693636FB9378E548D5703DC257ABC3A3DD6C0A,
	SystemLocaleSelector_GetStartupLocale_m856C9D33DA91A31AC13734F8082002D123D3F6B0,
	SystemLocaleSelector_GetSystemCulture_m00C0E730A4AB2A00DFB07C445B37CFF4FA2E3BD6,
	SystemLocaleSelector_GetApplicationSystemLanguage_m62641134AC3D5E70948392B86BF827EC37F8E3BD,
	SystemLocaleSelector__ctor_m83E108B4FBEEFD5655F5E1007AC56816DA763C9F,
	NULL,
	MessageFragment_get_Length_mA0712812B1C6AA17F7253C1E0FE439BD73106349,
	MessageFragment_get_Message_mED3AD18B54B7D7A248E02F4418D2C36B151AF12E,
	MessageFragment_set_Message_m11FDC80D4E1742181F69C50083B239D339EDD8A9,
	MessageFragment_Initialize_m9368F1A5226AB212CF375F3C24D7F55A845F87F9,
	MessageFragment_Initialize_m5C3A405B236080DE6739652D3896D4A1A25D3876,
	MessageFragment_CreateTextFragment_mFC4F48943006B07C125D6FE2AECD8A5D55172828,
	MessageFragment_CreateReadonlyTextFragment_m51F414632A02C697BCDF274A7A5C38D9E4835E7D,
	MessageFragment_ToString_mE0D856891856DAADC0CC277FF66264A80042E0D8,
	MessageFragment_BuildString_mC2098EBF795A5C82C577D1C898BED2B9A79A0DBD,
	MessageFragment_get_Item_mAE485CA791EE256217A8B01ADA40A306AAB65B82,
	MessageFragment__ctor_mB9EEBFB7669574F7E4EB186767853974F2A07F52,
	WritableMessageFragment_get_Text_m48073076F93C18875CF7ADEE0E5BD48CEF5EAEEC,
	WritableMessageFragment_set_Text_mA4FF6E78F43575475C88DBA01F94C5028B136D5C,
	WritableMessageFragment__ctor_m910629A438880957CBC6C445A0B7DDBCF501665E,
	ReadOnlyMessageFragment_get_Text_mC2F0DEE10ADCDA6E70C66995587D46D55ED44C24,
	ReadOnlyMessageFragment__ctor_mD9E860EB17680044881BC4496293591F51B50FAB,
	Message_get_Original_m76E16F0A97ADDF18457C66DCC044A1DC9D37D84D,
	Message_set_Original_mD2A73245F4F2540846DAA185DAA748A7D3A21C9F,
	Message_get_Fragments_mA852822AC6E656104C14BF7CF381ABBC3A1783A5,
	Message_set_Fragments_m6B0383E813998D1D8086133CA9D85161DA918D3C,
	Message_get_Length_m468559F45EA2ACD931604C58EA89D8C2B33E414E,
	Message_CreateTextFragment_mA0F87CAA724E5231DA2BA0C1D6521304AA545C5A,
	Message_CreateTextFragment_m3AE111031A505C5E31183A953685453DA4E0108C,
	Message_CreateReadonlyTextFragment_mF6ACDE6924B87A21044ABA8DA1B2B2213686F02D,
	Message_CreateReadonlyTextFragment_mFACBE5411DE45804693DC2326A84A79076F87C97,
	Message_ReplaceFragment_mE93AB7917B9F1197DB03F3E26CC4A48143589A04,
	Message_ReleaseFragment_m24A5B47A79AAD91E811AA0EFA276E1DABC3C6E9C,
	Message_CreateMessage_m58146A1A00B602B645D382B2B02C6C765BCDDF63,
	Message_Release_m7AB8C06D07378C6E3D2DFE5467372F0D8D9A4B45,
	Message_ToString_m5B991ECFD10578467BF9164B0DB522213BB7595D,
	Message__ctor_m17592F5E27715634D4FE830F2AB10B616630FD12,
	Accenter__ctor_m31DAC864F1DFE1F753E87F92286A0F623E0A00A1,
	Accenter_AddDefaults_mC74F3FFB1513D4DD4583377AF5B610FC1263DA70,
	CharacterSubstitutor_get_Method_m240B59532A960E0C2D9B700F28D5A4F18E516EED,
	CharacterSubstitutor_set_Method_mA2AB243B5A890AD2211BA987B5FFDE0E0111811B,
	CharacterSubstitutor_get_ReplacementMap_m4A8ED9408BADE6C785B714492F370368D193F696,
	CharacterSubstitutor_set_ReplacementMap_m91B8713CCDA1C7926BC536BC32F057F5465286B2,
	CharacterSubstitutor_get_ListMode_m4B8C99812D1D17351B4A1C015D768E1F93D0FA84,
	CharacterSubstitutor_set_ListMode_mA137C8C35C2A8D52AAA72BC71425B63004F6DE56,
	CharacterSubstitutor_get_ReplacementList_mF8C831F88BC6AC341BA447D34C2766863AC2DB66,
	CharacterSubstitutor_GetRandomSeed_mA50EAB688D0B5BED23B19808E51E2CA183775E83,
	CharacterSubstitutor_ReplaceCharFromMap_m80FC722B11715347048AEA2E6F400D562F0657E2,
	CharacterSubstitutor_OnBeforeSerialize_m6949A0533EB56FAB544433175CDC59364642E290,
	CharacterSubstitutor_OnAfterDeserialize_m08FB7880883678D435CD3DB055B701EC8CB1B136,
	CharacterSubstitutor_TransformFragment_mDFF31C2990990C7889B2523C9B41B35904FCFEF9,
	CharacterSubstitutor_Transform_mD48FE056F5A96C6D2F54A29F3C3CAA1B40FDD122,
	CharacterSubstitutor__ctor_m604B0271FFC7D5CD9E2E91B3D9A74905D4159200,
	Encapsulator_get_Start_m44A71CA66598891BC7466D3EA54131993D37796B,
	Encapsulator_set_Start_m40081476BCBAA60FE5F5D5F29023E8EFC344C2C9,
	Encapsulator_get_End_mB609C22B5A91AEC3CF25453505A975C56381F113,
	Encapsulator_set_End_mF6708825B850050910D0C36160FB3B44AC20BECE,
	Encapsulator_Transform_m082D3F700080F3ABC4A14C17709BB92C7EE52990,
	Encapsulator__ctor_m54A56A851E7C1CB8CAE53BFE60090C2C58EF1FC7,
	Expander_get_ExpansionRules_mCE133E517F62580A555EE90ED7B39DABABF185E4,
	Expander_get_Location_m0717B9ACE2A13501C6464539051CC8343AA066AD,
	Expander_set_Location_mF835FE8501562E5E676D68B506D1DD833F231DA8,
	Expander_get_PaddingCharacters_m0F6BD459FA5415C54CE003B07EDE758327B53B92,
	Expander_get_MinimumStringLength_m2E1302AAAA53684685747203B2A25A7090C7937F,
	Expander_set_MinimumStringLength_mC752354855300F77DE9967B15167CB1089132400,
	Expander__ctor_m1FA3C911F596FF95E2D78222B63D4ADF375C20B7,
	Expander__ctor_m1EC30009C9A352E0FC04D770403D65AEFF28563C,
	Expander__ctor_m72DB21F5208DD55F40ABF96132391A99ABCDDEC2,
	Expander_AddCharacterRange_m34E1DBE2AC55E816358CF8E9FA176083C97A929D,
	Expander_SetConstantExpansion_m0DBFC020A9D6DD7D9264BCA7D6882B090937E509,
	Expander_AddExpansionRule_m00C7E1FB03E7E61D5AF83ADAAC8700566059CBE3,
	Expander_GetExpansionForLength_mC14AF73F519D93E62016A59C69148978187A5CA7,
	Expander_Transform_mDCE4E8517118D6D75394A6EB3F7B21490C28F415,
	Expander_AddPaddingToMessage_m9285A81B7A29A347BB7AF745925198FC65E535BD,
	Expander_GetRandomSeed_mFD72023935458DF91BEEB64E940F7A5BC990ECB9,
	ExpansionRule_get_MinCharacters_m6D36BF2C0FD5540EE6723FB3AEA5C825A829AF01_AdjustorThunk,
	ExpansionRule_set_MinCharacters_m1CEBE64E9D97578F14B87AE49184EEC6038924CB_AdjustorThunk,
	ExpansionRule_get_MaxCharacters_m73F140050699A01185BD5C7A067A93AD8897DF2B_AdjustorThunk,
	ExpansionRule_set_MaxCharacters_m22A1833EED0978BA0B73056C743A5C808E1FE3BE_AdjustorThunk,
	ExpansionRule_get_ExpansionAmount_mEEDEC2BF7693AC3B529CA9979C8008B8D78936EF_AdjustorThunk,
	ExpansionRule_set_ExpansionAmount_mAF1EFF6225478043C1997827638D048980EB8D2A_AdjustorThunk,
	ExpansionRule__ctor_m01842E2E049B9464FF3514721ADDAE88C8771490_AdjustorThunk,
	ExpansionRule_InRange_mC243D964B0DC3D4206F159F80487610672598238_AdjustorThunk,
	ExpansionRule_CompareTo_mF4D487034D0BCA6B81B787326D5B201C752D87B1_AdjustorThunk,
	Mirror_Transform_m78DCC2062EA50862D99695FA4204D6CD076DDA3D,
	Mirror_MirrorFragment_mD3CD17E169A9AADAE622068D4D6C3033DB84610B,
	Mirror__ctor_mCFDF1940842B0ECDD5C2C359421DC542A96D4D19,
	PreserveTags_get_Opening_m653A94D322F3B611AC0FDA0EA14CC6036C7A3C98,
	PreserveTags_set_Opening_m17A19E66FD234F9593C3CFAB0334F40A4FD30D58,
	PreserveTags_get_Closing_m7C699A7B91EC5A6CC9526AF27D157BDE2F2A0529,
	PreserveTags_set_Closing_m10FEA0396006483AAF9435DC8C5C63E525470588,
	PreserveTags_Transform_m92943328A8155EAA8D2354D6A46B9D63DDE779FA,
	PreserveTags__ctor_m6EEA9F399A673393516CDDA1247DA6F3A2BB985E,
	PseudoLocale_get_Methods_m73945BE2B479793F75E10252D218AE5AE31BE470,
	PseudoLocale_CreatePseudoLocale_m9438FA7BAA469CC4EED492AE6B546C9CE6C9660B,
	PseudoLocale__ctor_m7944EABEF32DED913172FF4A14D963ACA21A6172,
	PseudoLocale_Reset_m671D1C3A01A1091868C50F44BD3774BF70305C2A,
	PseudoLocale_GetPseudoString_m7056161C4369467C879A09C0769BE8E77B23086A,
	PseudoLocale_ToString_mD14317D3537073588811ECECEFCDF698EC4B1DD1,
	TypicalCharacterSets_GetTypicalCharactersForLanguage_m57F081FAADDFF493F5121620FFC11DD4FD7277CB,
	TypicalCharacterSets__cctor_mACA61663EDAB37E29C27815F4261B6E9B03A9868,
	UnityEventAudioClip__ctor_mF3FCFF88B6B57FCC2E696C23D5E07054B3CCB357,
	LocalizeAudioClipEvent__ctor_m32A4C38BFAC48800D092A13518174DAE8458C5F3,
	UnityEventSprite__ctor_m1825285F2BB8BB31A972394A04A1B10D00CBF312,
	LocalizeSpriteEvent__ctor_mB31BEDAAB2AD4298F53F4D8915E637E9D1FBEBAE,
	LocalizeStringEvent_get_StringReference_mAB06FAA47CE385FE485CF85E39F2DF70FA452EDE,
	LocalizeStringEvent_set_StringReference_m261CA87D82BA3B93802ED2580F201C8388600667,
	LocalizeStringEvent_get_OnUpdateString_mCC3A1BACE2CAF665427CBE26D44C6BCFE6148927,
	LocalizeStringEvent_set_OnUpdateString_m8679A1FF9EEB7D02DE5DF5AF781A1D92BA6C756D,
	LocalizeStringEvent_OnEnable_m9C0AF42F40C9944C080EBAEB44EB8A2878432333,
	LocalizeStringEvent_OnDisable_m81BDCA35B9515BEA4F97B7D4D8D6FF32E4B66AB2,
	LocalizeStringEvent_UpdateString_m241988EEC8258CAE138384FECBBCBB756EAD4A78,
	LocalizeStringEvent_RegisterChangeHandler_mEED3D3D50C61C83A9582FF04D1308102F2E53A3F,
	LocalizeStringEvent_ClearChangeHandler_m3F48567D36ECD4FEA0BC06B2FFE544CAE2E927EC,
	LocalizeStringEvent__ctor_m5E89CD7346A2724DF4EB24F9C8A2FAC9D63E0C79,
	StringUnityEvent__ctor_m5442E27FDB074BB94A71A2806A1D7E31209876A9,
	UnityEventTexture__ctor_m94DA293A0097DB9111DADBC30D08346F24836B85,
	LocalizeTextureEvent__ctor_mD387244084054E2ABE561ED47A13D1DF8D9ACBEF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MetadataTypeAttribute_get_Type_m7015C5A17403BE26ACD1C95ECA6955770003CA2C,
	MetadataTypeAttribute_set_Type_m4BBF1A85CEEF81B3513B66DFEC305F8BF0E118B3,
	MetadataTypeAttribute__ctor_mAB0EBC0EF5F270C114844DF7BCA8025EFE65D3FE,
	MetadataAttribute_get_MenuItem_m3DBD782DE5ADDD2ED728C162494249E9FD4FA89C,
	MetadataAttribute_set_MenuItem_m58350CA8E7574D94DF90CADC436EB19995BA66F8,
	MetadataAttribute_get_AllowMultiple_m76996A93063F7102805AEC38393EB66D580E2930,
	MetadataAttribute_set_AllowMultiple_mF9CA86ABBED5DA37CB5DD306C74EA6463B575F21,
	MetadataAttribute_get_AllowedTypes_m16BC0738C276E85F42A1B500D575802110169B44,
	MetadataAttribute_set_AllowedTypes_m9BCE86B4D8BFD6CF24F28822F88D0E2D6218137A,
	MetadataAttribute__ctor_mCEC26AB01CD5805DFEB901A94E2267E7AC473EB7,
	AssetTypeMetadata_get_Type_m851560556ADD149542EB1A44956B88E276DD5EAF,
	AssetTypeMetadata_set_Type_m58361EA5FC553AFA6F3E88DEEAE8FAD7AA9C2FB5,
	AssetTypeMetadata_OnBeforeSerialize_mED15BA31869AA924DA5AE7EF669AFE5B826B3D11,
	AssetTypeMetadata_OnAfterDeserialize_m245E32006D229ED95BE138D7AE8BA0E0BA3E08D6,
	AssetTypeMetadata__ctor_mA949DC80059489E2A95122EE5902C5490A403406,
	Comment_get_CommentText_mDA0E972A8124ECA30862063F0C423B3E58E19521,
	Comment_set_CommentText_mF6440CF598FCE8ADE414C5F130A93C4FC3A4FDAB,
	Comment__ctor_m6A89B8CE9D2ABB3A32960DE746F644B159A6E14B,
	FallbackLocale__ctor_mF7A1336CED7009090CBBE386E12CB1ACF5CA9096,
	FallbackLocale__ctor_mEDB0F64B549006978F340D155ABCF72EA7DCD9E8,
	FallbackLocale_get_Locale_mB1586E6DDD6A47ACAA38A7779707B17E43364F04,
	FallbackLocale_set_Locale_m724BEA676FA5BD53591AE417E50E043108EC1688,
	FallbackLocale_IsCyclic_mCD063CCA8893BC7EAFC047DA5D2BCA8CE1AA5666,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MetadataCollection_get_MetadataEntries_mE1F9A4D7D46635CB2B2693549C4EDE9547DF2804,
	MetadataCollection_get_HasData_m67A440D68E8F5B3FD6199B05E4423140950992A0,
	NULL,
	NULL,
	NULL,
	MetadataCollection_AddMetadata_mD3799E1933DB8882EE02F637F368BE6E50F50D96,
	MetadataCollection_RemoveMetadata_m80CE7DEA4B398B6C47A6FC1C6A23B1A19E1353F3,
	MetadataCollection_Contains_mE0029F958F6223F82AFEC7078E045AF36754D161,
	MetadataCollection__ctor_m4D1388E31851B3F421C638C8EA1A537BD73A79A0,
	PreloadAssetTableMetadata_get_Behaviour_mE0F1E99BDBF6CECCCFB04A43D41A3E0F0F4A6C73,
	PreloadAssetTableMetadata_set_Behaviour_m932814B76693F9EE47EAFAE0E967C2A1A771DC52,
	PreloadAssetTableMetadata__ctor_m87DC9A04ACBE65D650AF2D57766F4BF6E258BF22,
	SharedTableCollectionMetadata_get_IsEmpty_mECD644F644FC2931930A23D4400F79F1A17961C3,
	SharedTableCollectionMetadata_Contains_m8D84E94C1E2BB57F2A475A11DA82C336C04CFB51,
	SharedTableCollectionMetadata_Contains_m251F85DF27CA5F721B33A80919ADA5511E628B2E,
	SharedTableCollectionMetadata_AddEntry_m6AA6489F8F0DD6F575F87187410A9B4E186AF7FE,
	SharedTableCollectionMetadata_RemoveEntry_m2A111F466312F07CB02BACF976DDBA9070E49F8F,
	SharedTableCollectionMetadata_OnBeforeSerialize_mE98CA40B8870052CAE836E3F0EA84C13AB696BF0,
	SharedTableCollectionMetadata_OnAfterDeserialize_mCFFF323F17F5ED1586F40741A43CF1D7B703C8FB,
	SharedTableCollectionMetadata__ctor_m366AB5BD02584099090728F533724C584A39B13C,
	Item_get_KeyId_m33636465F4A2709BE7C6E1F2D8E40735CF5739CF,
	Item_set_KeyId_m372CA165C190CF956D73C76AE67027115192DDB4,
	Item_get_Tables_mEFB9B026B7C7187EB3D5134148F249F37A11F602,
	Item_set_Tables_m0CB4421F69EE823555E7C82A19C4EC580D37F660,
	Item__ctor_m4CEBCCAD94221D9BCE884DA69AB3496DCF093F64,
	SharedTableEntryMetadata_get_Count_m4979897098E58C40CC5A82CAE40239A8D9F33824,
	SharedTableEntryMetadata_IsRegistered_m88D87131CE3476CDB5146EDAF5A407EAFAF618A3,
	SharedTableEntryMetadata_Register_m761A1AED3086AAAE4EF8071A05B8EC82D071958E,
	SharedTableEntryMetadata_Unregister_mF8DEE603F342D9E056F2D998F029566F5FB13D68,
	SharedTableEntryMetadata_OnBeforeSerialize_m2D84B53AA47254FC535E32B70E61A71225AC4161,
	SharedTableEntryMetadata_OnAfterDeserialize_m8B273F478AA6AFCE7363FC5D703EE2DBD45616CA,
	SharedTableEntryMetadata__ctor_m69183A00F58CD30446202257759A310A65AA0AF4,
	SmartFormatTag__ctor_m9DE2228D777B8B8695E9A46C5C576A0FABFDE5DD,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m34097AD7E4254D26DFA4C7A41A4D52C580190EC1,
};
static const int32_t s_InvokerIndices[1161] = 
{
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	26,
	26,
	2382,
	14,
	14,
	26,
	26,
	32,
	2383,
	2383,
	2384,
	14,
	9,
	2385,
	10,
	2386,
	2387,
	2387,
	2388,
	2389,
	14,
	26,
	251,
	617,
	0,
	2390,
	43,
	0,
	121,
	14,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	23,
	23,
	23,
	14,
	23,
	2391,
	2392,
	2393,
	2394,
	89,
	2395,
	14,
	23,
	23,
	14,
	26,
	2396,
	2397,
	26,
	26,
	89,
	2398,
	2399,
	23,
	26,
	2400,
	23,
	23,
	133,
	26,
	220,
	26,
	14,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	761,
	14,
	26,
	23,
	2073,
	23,
	2078,
	761,
	14,
	26,
	23,
	23,
	2073,
	23,
	23,
	23,
	23,
	2401,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	2402,
	2390,
	109,
	2383,
	23,
	4,
	2403,
	168,
	3,
	3,
	23,
	14,
	26,
	43,
	2404,
	2405,
	14,
	26,
	89,
	23,
	23,
	14,
	2078,
	2078,
	-1,
	-1,
	14,
	23,
	14,
	26,
	14,
	26,
	189,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	26,
	-1,
	26,
	26,
	9,
	9,
	14,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	189,
	10,
	32,
	23,
	215,
	189,
	189,
	115,
	1010,
	3,
	189,
	189,
	23,
	215,
	189,
	2388,
	2389,
	14,
	14,
	26,
	14,
	14,
	-1,
	-1,
	-1,
	26,
	9,
	9,
	2394,
	2406,
	23,
	14,
	121,
	23,
	14,
	14,
	26,
	730,
	1068,
	14,
	26,
	14,
	26,
	191,
	232,
	2406,
	2407,
	191,
	28,
	439,
	9,
	28,
	2408,
	14,
	215,
	26,
	2409,
	27,
	2410,
	487,
	145,
	28,
	2408,
	27,
	26,
	191,
	28,
	14,
	23,
	23,
	23,
	189,
	215,
	14,
	26,
	14,
	26,
	14,
	23,
	14,
	26,
	89,
	31,
	23,
	23,
	14,
	28,
	14,
	23,
	189,
	215,
	14,
	26,
	14,
	26,
	23,
	215,
	2409,
	10,
	32,
	730,
	1068,
	14,
	26,
	2411,
	2412,
	2413,
	2414,
	23,
	14,
	14,
	9,
	10,
	2415,
	425,
	577,
	2411,
	109,
	23,
	23,
	10,
	32,
	189,
	215,
	14,
	26,
	2416,
	2417,
	2418,
	2419,
	23,
	28,
	14,
	9,
	2420,
	10,
	23,
	23,
	362,
	14,
	26,
	10,
	32,
	89,
	31,
	1,
	2,
	359,
	2,
	1,
	4,
	168,
	4,
	203,
	203,
	203,
	203,
	1,
	1376,
	26,
	26,
	14,
	14,
	23,
	14,
	26,
	26,
	-1,
	-1,
	14,
	26,
	14,
	26,
	114,
	220,
	214,
	1323,
	2421,
	214,
	26,
	1458,
	23,
	26,
	9,
	26,
	9,
	3,
	23,
	9,
	23,
	26,
	26,
	26,
	3,
	23,
	9,
	1999,
	2422,
	3,
	3,
	23,
	119,
	23,
	2423,
	3,
	23,
	119,
	2423,
	119,
	2423,
	26,
	26,
	114,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	4,
	0,
	2424,
	3,
	133,
	2425,
	2426,
	121,
	3,
	23,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2425,
	2427,
	3,
	115,
	178,
	115,
	2428,
	2428,
	2428,
	182,
	182,
	43,
	94,
	3,
	32,
	23,
	89,
	10,
	23,
	14,
	14,
	14,
	2429,
	1012,
	254,
	28,
	811,
	3,
	23,
	2425,
	4,
	0,
	251,
	617,
	23,
	14,
	9,
	2,
	23,
	14,
	9,
	2430,
	3,
	23,
	14,
	9,
	26,
	9,
	26,
	9,
	23,
	1062,
	23,
	14,
	9,
	10,
	32,
	26,
	14,
	9,
	115,
	178,
	9,
	3,
	14,
	26,
	23,
	14,
	9,
	28,
	26,
	28,
	14,
	26,
	9,
	23,
	9,
	23,
	14,
	251,
	617,
	14,
	26,
	9,
	14,
	26,
	14,
	9,
	27,
	9,
	23,
	23,
	14,
	10,
	32,
	14,
	26,
	9,
	28,
	23,
	14,
	9,
	26,
	9,
	23,
	9,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	89,
	31,
	14,
	10,
	27,
	110,
	14,
	89,
	31,
	34,
	207,
	245,
	513,
	676,
	524,
	676,
	524,
	14,
	14,
	27,
	34,
	62,
	139,
	10,
	89,
	121,
	62,
	32,
	26,
	23,
	9,
	9,
	14,
	14,
	110,
	36,
	14,
	14,
	110,
	27,
	14,
	14,
	14,
	26,
	26,
	26,
	26,
	23,
	26,
	26,
	617,
	23,
	1166,
	114,
	142,
	23,
	34,
	23,
	9,
	102,
	14,
	26,
	89,
	31,
	26,
	14,
	89,
	14,
	14,
	35,
	35,
	10,
	10,
	14,
	3,
	23,
	28,
	28,
	36,
	10,
	32,
	14,
	10,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	2431,
	10,
	14,
	27,
	1180,
	23,
	32,
	26,
	27,
	1180,
	14,
	26,
	14,
	27,
	1180,
	26,
	14,
	14,
	944,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	26,
	14,
	110,
	110,
	14,
	14,
	14,
	10,
	14,
	214,
	450,
	450,
	14,
	14,
	26,
	14,
	14,
	26,
	14,
	10,
	14,
	14,
	26,
	35,
	27,
	111,
	14,
	10,
	14,
	14,
	26,
	114,
	28,
	14,
	26,
	14,
	9,
	23,
	23,
	23,
	14,
	26,
	9,
	14,
	14,
	14,
	10,
	14,
	14,
	26,
	35,
	27,
	111,
	14,
	14,
	10,
	14,
	14,
	26,
	14,
	14,
	9,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	14,
	26,
	14,
	26,
	2432,
	2433,
	2433,
	2434,
	2435,
	2436,
	2436,
	2437,
	2437,
	2438,
	28,
	23,
	23,
	2439,
	23,
	2440,
	23,
	2441,
	26,
	14,
	2442,
	26,
	9,
	28,
	14,
	2078,
	2442,
	28,
	34,
	26,
	9,
	23,
	26,
	26,
	49,
	2443,
	49,
	4,
	168,
	4,
	4,
	168,
	4,
	168,
	4,
	168,
	4,
	168,
	2444,
	168,
	168,
	4,
	23,
	168,
	2445,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	14,
	26,
	2446,
	14,
	26,
	4,
	4,
	23,
	2447,
	23,
	2448,
	14,
	26,
	28,
	23,
	14,
	26,
	26,
	28,
	23,
	2388,
	2389,
	28,
	23,
	28,
	14,
	10,
	23,
	26,
	10,
	14,
	26,
	36,
	27,
	207,
	207,
	14,
	26,
	422,
	23,
	14,
	26,
	23,
	14,
	23,
	14,
	26,
	14,
	26,
	10,
	54,
	28,
	54,
	28,
	27,
	26,
	0,
	23,
	14,
	23,
	23,
	23,
	10,
	32,
	14,
	26,
	10,
	32,
	14,
	121,
	665,
	23,
	23,
	26,
	26,
	23,
	14,
	26,
	14,
	26,
	26,
	23,
	14,
	10,
	32,
	14,
	10,
	32,
	23,
	617,
	1166,
	1166,
	339,
	1709,
	1626,
	26,
	27,
	121,
	10,
	32,
	10,
	32,
	761,
	339,
	1709,
	30,
	2449,
	26,
	26,
	23,
	251,
	617,
	251,
	617,
	26,
	23,
	14,
	4,
	23,
	23,
	28,
	14,
	43,
	3,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	10,
	32,
	32,
	14,
	26,
	89,
	31,
	10,
	32,
	23,
	14,
	26,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	26,
	14,
	26,
	9,
	439,
	215,
	215,
	14,
	-1,
	-1,
	-1,
	26,
	9,
	9,
	14,
	89,
	-1,
	-1,
	-1,
	26,
	9,
	9,
	23,
	10,
	32,
	23,
	89,
	439,
	2450,
	2409,
	2409,
	23,
	23,
	23,
	189,
	215,
	14,
	26,
	23,
	10,
	9,
	26,
	26,
	23,
	23,
	23,
	23,
	94,
};
static const Il2CppTokenRangePair s_rgctxIndices[49] = 
{
	{ 0x02000002, { 0, 13 } },
	{ 0x02000003, { 13, 6 } },
	{ 0x02000004, { 19, 2 } },
	{ 0x02000005, { 21, 2 } },
	{ 0x02000006, { 23, 2 } },
	{ 0x02000007, { 25, 10 } },
	{ 0x02000008, { 35, 4 } },
	{ 0x0200000A, { 39, 10 } },
	{ 0x0200000C, { 49, 19 } },
	{ 0x0200000D, { 68, 1 } },
	{ 0x0200000E, { 69, 10 } },
	{ 0x0200000F, { 79, 4 } },
	{ 0x02000013, { 83, 19 } },
	{ 0x0200001E, { 102, 24 } },
	{ 0x02000023, { 126, 23 } },
	{ 0x0200002C, { 163, 49 } },
	{ 0x0200002D, { 212, 4 } },
	{ 0x02000081, { 242, 1 } },
	{ 0x02000082, { 243, 1 } },
	{ 0x02000083, { 244, 71 } },
	{ 0x02000084, { 315, 2 } },
	{ 0x02000085, { 317, 1 } },
	{ 0x02000086, { 318, 1 } },
	{ 0x02000087, { 319, 1 } },
	{ 0x02000088, { 320, 1 } },
	{ 0x02000089, { 321, 1 } },
	{ 0x020000B2, { 322, 7 } },
	{ 0x020000B3, { 329, 6 } },
	{ 0x060000CE, { 149, 2 } },
	{ 0x060000CF, { 151, 5 } },
	{ 0x060000D9, { 156, 1 } },
	{ 0x060000DA, { 157, 1 } },
	{ 0x060000DB, { 158, 1 } },
	{ 0x060000DC, { 159, 2 } },
	{ 0x060000E0, { 161, 2 } },
	{ 0x06000126, { 216, 1 } },
	{ 0x06000127, { 217, 1 } },
	{ 0x06000128, { 218, 1 } },
	{ 0x060001B7, { 219, 2 } },
	{ 0x060001B8, { 221, 2 } },
	{ 0x0600033F, { 223, 1 } },
	{ 0x06000340, { 224, 1 } },
	{ 0x06000341, { 225, 7 } },
	{ 0x06000342, { 232, 1 } },
	{ 0x06000343, { 233, 7 } },
	{ 0x06000344, { 240, 2 } },
	{ 0x0600046A, { 335, 1 } },
	{ 0x0600046B, { 336, 2 } },
	{ 0x0600046C, { 338, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[341] = 
{
	{ (Il2CppRGCTXDataType)2, 27507 },
	{ (Il2CppRGCTXDataType)3, 49206 },
	{ (Il2CppRGCTXDataType)3, 49207 },
	{ (Il2CppRGCTXDataType)3, 49208 },
	{ (Il2CppRGCTXDataType)2, 27508 },
	{ (Il2CppRGCTXDataType)3, 49209 },
	{ (Il2CppRGCTXDataType)2, 27509 },
	{ (Il2CppRGCTXDataType)3, 49210 },
	{ (Il2CppRGCTXDataType)3, 49211 },
	{ (Il2CppRGCTXDataType)2, 27510 },
	{ (Il2CppRGCTXDataType)3, 49212 },
	{ (Il2CppRGCTXDataType)2, 27511 },
	{ (Il2CppRGCTXDataType)3, 49213 },
	{ (Il2CppRGCTXDataType)2, 27512 },
	{ (Il2CppRGCTXDataType)3, 49214 },
	{ (Il2CppRGCTXDataType)2, 27512 },
	{ (Il2CppRGCTXDataType)3, 49215 },
	{ (Il2CppRGCTXDataType)2, 16579 },
	{ (Il2CppRGCTXDataType)2, 16582 },
	{ (Il2CppRGCTXDataType)3, 49216 },
	{ (Il2CppRGCTXDataType)2, 16587 },
	{ (Il2CppRGCTXDataType)3, 49217 },
	{ (Il2CppRGCTXDataType)2, 16592 },
	{ (Il2CppRGCTXDataType)3, 49218 },
	{ (Il2CppRGCTXDataType)2, 16597 },
	{ (Il2CppRGCTXDataType)2, 27513 },
	{ (Il2CppRGCTXDataType)3, 49219 },
	{ (Il2CppRGCTXDataType)3, 49220 },
	{ (Il2CppRGCTXDataType)3, 49221 },
	{ (Il2CppRGCTXDataType)2, 27514 },
	{ (Il2CppRGCTXDataType)3, 49222 },
	{ (Il2CppRGCTXDataType)2, 27515 },
	{ (Il2CppRGCTXDataType)3, 49223 },
	{ (Il2CppRGCTXDataType)2, 27516 },
	{ (Il2CppRGCTXDataType)3, 49224 },
	{ (Il2CppRGCTXDataType)2, 27517 },
	{ (Il2CppRGCTXDataType)3, 49225 },
	{ (Il2CppRGCTXDataType)2, 27517 },
	{ (Il2CppRGCTXDataType)3, 49226 },
	{ (Il2CppRGCTXDataType)3, 49227 },
	{ (Il2CppRGCTXDataType)3, 49228 },
	{ (Il2CppRGCTXDataType)3, 49229 },
	{ (Il2CppRGCTXDataType)3, 49230 },
	{ (Il2CppRGCTXDataType)3, 49231 },
	{ (Il2CppRGCTXDataType)2, 16624 },
	{ (Il2CppRGCTXDataType)3, 49232 },
	{ (Il2CppRGCTXDataType)2, 27518 },
	{ (Il2CppRGCTXDataType)3, 49233 },
	{ (Il2CppRGCTXDataType)3, 49234 },
	{ (Il2CppRGCTXDataType)3, 49235 },
	{ (Il2CppRGCTXDataType)3, 49236 },
	{ (Il2CppRGCTXDataType)3, 49237 },
	{ (Il2CppRGCTXDataType)2, 27519 },
	{ (Il2CppRGCTXDataType)3, 49238 },
	{ (Il2CppRGCTXDataType)3, 49239 },
	{ (Il2CppRGCTXDataType)3, 49240 },
	{ (Il2CppRGCTXDataType)3, 49241 },
	{ (Il2CppRGCTXDataType)3, 49242 },
	{ (Il2CppRGCTXDataType)3, 49243 },
	{ (Il2CppRGCTXDataType)2, 16641 },
	{ (Il2CppRGCTXDataType)3, 49244 },
	{ (Il2CppRGCTXDataType)3, 49245 },
	{ (Il2CppRGCTXDataType)3, 49246 },
	{ (Il2CppRGCTXDataType)3, 49247 },
	{ (Il2CppRGCTXDataType)3, 49248 },
	{ (Il2CppRGCTXDataType)2, 27520 },
	{ (Il2CppRGCTXDataType)3, 49249 },
	{ (Il2CppRGCTXDataType)3, 49250 },
	{ (Il2CppRGCTXDataType)2, 16649 },
	{ (Il2CppRGCTXDataType)2, 27521 },
	{ (Il2CppRGCTXDataType)3, 49251 },
	{ (Il2CppRGCTXDataType)3, 49252 },
	{ (Il2CppRGCTXDataType)3, 49253 },
	{ (Il2CppRGCTXDataType)2, 27522 },
	{ (Il2CppRGCTXDataType)3, 49254 },
	{ (Il2CppRGCTXDataType)2, 27523 },
	{ (Il2CppRGCTXDataType)3, 49255 },
	{ (Il2CppRGCTXDataType)2, 27524 },
	{ (Il2CppRGCTXDataType)3, 49256 },
	{ (Il2CppRGCTXDataType)2, 27525 },
	{ (Il2CppRGCTXDataType)3, 49257 },
	{ (Il2CppRGCTXDataType)2, 27525 },
	{ (Il2CppRGCTXDataType)3, 49258 },
	{ (Il2CppRGCTXDataType)2, 16678 },
	{ (Il2CppRGCTXDataType)3, 49259 },
	{ (Il2CppRGCTXDataType)3, 49260 },
	{ (Il2CppRGCTXDataType)3, 49261 },
	{ (Il2CppRGCTXDataType)3, 49262 },
	{ (Il2CppRGCTXDataType)3, 49263 },
	{ (Il2CppRGCTXDataType)3, 49264 },
	{ (Il2CppRGCTXDataType)3, 49265 },
	{ (Il2CppRGCTXDataType)3, 49266 },
	{ (Il2CppRGCTXDataType)3, 49267 },
	{ (Il2CppRGCTXDataType)3, 49268 },
	{ (Il2CppRGCTXDataType)2, 16675 },
	{ (Il2CppRGCTXDataType)3, 49269 },
	{ (Il2CppRGCTXDataType)3, 49270 },
	{ (Il2CppRGCTXDataType)2, 27526 },
	{ (Il2CppRGCTXDataType)3, 49271 },
	{ (Il2CppRGCTXDataType)3, 49272 },
	{ (Il2CppRGCTXDataType)3, 49273 },
	{ (Il2CppRGCTXDataType)3, 49274 },
	{ (Il2CppRGCTXDataType)3, 49275 },
	{ (Il2CppRGCTXDataType)3, 49276 },
	{ (Il2CppRGCTXDataType)2, 16720 },
	{ (Il2CppRGCTXDataType)3, 49277 },
	{ (Il2CppRGCTXDataType)3, 49278 },
	{ (Il2CppRGCTXDataType)3, 49279 },
	{ (Il2CppRGCTXDataType)3, 49280 },
	{ (Il2CppRGCTXDataType)3, 49281 },
	{ (Il2CppRGCTXDataType)3, 49282 },
	{ (Il2CppRGCTXDataType)3, 49283 },
	{ (Il2CppRGCTXDataType)3, 49284 },
	{ (Il2CppRGCTXDataType)3, 49285 },
	{ (Il2CppRGCTXDataType)3, 49286 },
	{ (Il2CppRGCTXDataType)2, 16718 },
	{ (Il2CppRGCTXDataType)3, 49287 },
	{ (Il2CppRGCTXDataType)3, 49288 },
	{ (Il2CppRGCTXDataType)3, 49289 },
	{ (Il2CppRGCTXDataType)2, 27527 },
	{ (Il2CppRGCTXDataType)3, 49290 },
	{ (Il2CppRGCTXDataType)3, 49291 },
	{ (Il2CppRGCTXDataType)3, 49292 },
	{ (Il2CppRGCTXDataType)3, 49293 },
	{ (Il2CppRGCTXDataType)3, 49294 },
	{ (Il2CppRGCTXDataType)3, 49295 },
	{ (Il2CppRGCTXDataType)3, 49296 },
	{ (Il2CppRGCTXDataType)1, 16744 },
	{ (Il2CppRGCTXDataType)3, 49297 },
	{ (Il2CppRGCTXDataType)3, 49298 },
	{ (Il2CppRGCTXDataType)3, 49299 },
	{ (Il2CppRGCTXDataType)2, 27528 },
	{ (Il2CppRGCTXDataType)3, 49300 },
	{ (Il2CppRGCTXDataType)3, 49301 },
	{ (Il2CppRGCTXDataType)3, 49302 },
	{ (Il2CppRGCTXDataType)3, 49303 },
	{ (Il2CppRGCTXDataType)2, 27529 },
	{ (Il2CppRGCTXDataType)3, 49304 },
	{ (Il2CppRGCTXDataType)3, 49305 },
	{ (Il2CppRGCTXDataType)3, 49306 },
	{ (Il2CppRGCTXDataType)3, 49307 },
	{ (Il2CppRGCTXDataType)2, 27530 },
	{ (Il2CppRGCTXDataType)2, 27531 },
	{ (Il2CppRGCTXDataType)3, 49308 },
	{ (Il2CppRGCTXDataType)2, 16744 },
	{ (Il2CppRGCTXDataType)3, 49309 },
	{ (Il2CppRGCTXDataType)3, 49310 },
	{ (Il2CppRGCTXDataType)3, 49311 },
	{ (Il2CppRGCTXDataType)2, 16742 },
	{ (Il2CppRGCTXDataType)3, 49312 },
	{ (Il2CppRGCTXDataType)3, 49313 },
	{ (Il2CppRGCTXDataType)3, 49314 },
	{ (Il2CppRGCTXDataType)3, 49315 },
	{ (Il2CppRGCTXDataType)3, 49316 },
	{ (Il2CppRGCTXDataType)2, 16772 },
	{ (Il2CppRGCTXDataType)3, 49317 },
	{ (Il2CppRGCTXDataType)3, 49318 },
	{ (Il2CppRGCTXDataType)3, 49319 },
	{ (Il2CppRGCTXDataType)3, 49320 },
	{ (Il2CppRGCTXDataType)2, 27532 },
	{ (Il2CppRGCTXDataType)3, 49321 },
	{ (Il2CppRGCTXDataType)3, 49322 },
	{ (Il2CppRGCTXDataType)2, 27533 },
	{ (Il2CppRGCTXDataType)3, 49323 },
	{ (Il2CppRGCTXDataType)3, 49324 },
	{ (Il2CppRGCTXDataType)3, 49325 },
	{ (Il2CppRGCTXDataType)3, 49326 },
	{ (Il2CppRGCTXDataType)2, 16794 },
	{ (Il2CppRGCTXDataType)3, 49327 },
	{ (Il2CppRGCTXDataType)3, 49328 },
	{ (Il2CppRGCTXDataType)3, 49329 },
	{ (Il2CppRGCTXDataType)3, 49330 },
	{ (Il2CppRGCTXDataType)3, 49331 },
	{ (Il2CppRGCTXDataType)3, 49332 },
	{ (Il2CppRGCTXDataType)3, 49333 },
	{ (Il2CppRGCTXDataType)3, 49334 },
	{ (Il2CppRGCTXDataType)3, 49335 },
	{ (Il2CppRGCTXDataType)3, 49336 },
	{ (Il2CppRGCTXDataType)3, 49337 },
	{ (Il2CppRGCTXDataType)3, 49338 },
	{ (Il2CppRGCTXDataType)3, 49339 },
	{ (Il2CppRGCTXDataType)3, 49340 },
	{ (Il2CppRGCTXDataType)3, 49341 },
	{ (Il2CppRGCTXDataType)3, 49342 },
	{ (Il2CppRGCTXDataType)3, 49343 },
	{ (Il2CppRGCTXDataType)2, 27534 },
	{ (Il2CppRGCTXDataType)3, 49344 },
	{ (Il2CppRGCTXDataType)3, 49345 },
	{ (Il2CppRGCTXDataType)3, 49346 },
	{ (Il2CppRGCTXDataType)2, 27535 },
	{ (Il2CppRGCTXDataType)3, 49347 },
	{ (Il2CppRGCTXDataType)3, 49348 },
	{ (Il2CppRGCTXDataType)2, 27536 },
	{ (Il2CppRGCTXDataType)3, 49349 },
	{ (Il2CppRGCTXDataType)2, 27537 },
	{ (Il2CppRGCTXDataType)3, 49350 },
	{ (Il2CppRGCTXDataType)3, 49351 },
	{ (Il2CppRGCTXDataType)3, 49352 },
	{ (Il2CppRGCTXDataType)3, 49353 },
	{ (Il2CppRGCTXDataType)3, 49354 },
	{ (Il2CppRGCTXDataType)3, 49355 },
	{ (Il2CppRGCTXDataType)3, 49356 },
	{ (Il2CppRGCTXDataType)2, 27538 },
	{ (Il2CppRGCTXDataType)3, 49357 },
	{ (Il2CppRGCTXDataType)2, 16800 },
	{ (Il2CppRGCTXDataType)3, 49358 },
	{ (Il2CppRGCTXDataType)3, 49359 },
	{ (Il2CppRGCTXDataType)2, 27539 },
	{ (Il2CppRGCTXDataType)3, 49360 },
	{ (Il2CppRGCTXDataType)3, 49361 },
	{ (Il2CppRGCTXDataType)2, 27540 },
	{ (Il2CppRGCTXDataType)3, 49362 },
	{ (Il2CppRGCTXDataType)2, 27541 },
	{ (Il2CppRGCTXDataType)3, 49363 },
	{ (Il2CppRGCTXDataType)2, 27541 },
	{ (Il2CppRGCTXDataType)3, 49364 },
	{ (Il2CppRGCTXDataType)3, 49365 },
	{ (Il2CppRGCTXDataType)3, 49366 },
	{ (Il2CppRGCTXDataType)3, 49367 },
	{ (Il2CppRGCTXDataType)3, 49368 },
	{ (Il2CppRGCTXDataType)3, 49369 },
	{ (Il2CppRGCTXDataType)3, 49370 },
	{ (Il2CppRGCTXDataType)3, 49371 },
	{ (Il2CppRGCTXDataType)3, 49372 },
	{ (Il2CppRGCTXDataType)3, 49373 },
	{ (Il2CppRGCTXDataType)2, 27542 },
	{ (Il2CppRGCTXDataType)3, 49374 },
	{ (Il2CppRGCTXDataType)3, 49375 },
	{ (Il2CppRGCTXDataType)2, 27543 },
	{ (Il2CppRGCTXDataType)3, 49376 },
	{ (Il2CppRGCTXDataType)3, 49377 },
	{ (Il2CppRGCTXDataType)3, 49378 },
	{ (Il2CppRGCTXDataType)3, 49379 },
	{ (Il2CppRGCTXDataType)2, 27544 },
	{ (Il2CppRGCTXDataType)3, 49380 },
	{ (Il2CppRGCTXDataType)3, 49381 },
	{ (Il2CppRGCTXDataType)2, 27545 },
	{ (Il2CppRGCTXDataType)3, 49382 },
	{ (Il2CppRGCTXDataType)3, 49383 },
	{ (Il2CppRGCTXDataType)3, 49384 },
	{ (Il2CppRGCTXDataType)3, 49385 },
	{ (Il2CppRGCTXDataType)3, 49386 },
	{ (Il2CppRGCTXDataType)3, 49387 },
	{ (Il2CppRGCTXDataType)3, 49388 },
	{ (Il2CppRGCTXDataType)2, 27546 },
	{ (Il2CppRGCTXDataType)3, 49389 },
	{ (Il2CppRGCTXDataType)3, 49390 },
	{ (Il2CppRGCTXDataType)3, 49391 },
	{ (Il2CppRGCTXDataType)3, 49392 },
	{ (Il2CppRGCTXDataType)3, 49393 },
	{ (Il2CppRGCTXDataType)2, 27548 },
	{ (Il2CppRGCTXDataType)2, 17103 },
	{ (Il2CppRGCTXDataType)3, 49394 },
	{ (Il2CppRGCTXDataType)3, 49395 },
	{ (Il2CppRGCTXDataType)3, 49396 },
	{ (Il2CppRGCTXDataType)3, 49397 },
	{ (Il2CppRGCTXDataType)3, 49398 },
	{ (Il2CppRGCTXDataType)2, 27549 },
	{ (Il2CppRGCTXDataType)3, 49399 },
	{ (Il2CppRGCTXDataType)3, 49400 },
	{ (Il2CppRGCTXDataType)3, 49401 },
	{ (Il2CppRGCTXDataType)2, 17105 },
	{ (Il2CppRGCTXDataType)3, 49402 },
	{ (Il2CppRGCTXDataType)2, 27550 },
	{ (Il2CppRGCTXDataType)3, 49403 },
	{ (Il2CppRGCTXDataType)3, 49404 },
	{ (Il2CppRGCTXDataType)2, 27551 },
	{ (Il2CppRGCTXDataType)3, 49405 },
	{ (Il2CppRGCTXDataType)3, 49406 },
	{ (Il2CppRGCTXDataType)3, 49407 },
	{ (Il2CppRGCTXDataType)2, 27552 },
	{ (Il2CppRGCTXDataType)3, 49408 },
	{ (Il2CppRGCTXDataType)3, 49409 },
	{ (Il2CppRGCTXDataType)3, 49410 },
	{ (Il2CppRGCTXDataType)2, 27553 },
	{ (Il2CppRGCTXDataType)3, 49411 },
	{ (Il2CppRGCTXDataType)3, 49412 },
	{ (Il2CppRGCTXDataType)2, 27554 },
	{ (Il2CppRGCTXDataType)3, 49413 },
	{ (Il2CppRGCTXDataType)3, 49414 },
	{ (Il2CppRGCTXDataType)3, 49415 },
	{ (Il2CppRGCTXDataType)2, 27555 },
	{ (Il2CppRGCTXDataType)3, 49416 },
	{ (Il2CppRGCTXDataType)3, 49417 },
	{ (Il2CppRGCTXDataType)2, 27556 },
	{ (Il2CppRGCTXDataType)3, 49418 },
	{ (Il2CppRGCTXDataType)3, 49419 },
	{ (Il2CppRGCTXDataType)3, 49420 },
	{ (Il2CppRGCTXDataType)2, 27557 },
	{ (Il2CppRGCTXDataType)3, 49421 },
	{ (Il2CppRGCTXDataType)3, 49422 },
	{ (Il2CppRGCTXDataType)3, 49423 },
	{ (Il2CppRGCTXDataType)3, 49424 },
	{ (Il2CppRGCTXDataType)2, 27558 },
	{ (Il2CppRGCTXDataType)3, 49425 },
	{ (Il2CppRGCTXDataType)3, 49426 },
	{ (Il2CppRGCTXDataType)3, 49427 },
	{ (Il2CppRGCTXDataType)3, 49428 },
	{ (Il2CppRGCTXDataType)3, 49429 },
	{ (Il2CppRGCTXDataType)3, 49430 },
	{ (Il2CppRGCTXDataType)3, 49431 },
	{ (Il2CppRGCTXDataType)3, 49432 },
	{ (Il2CppRGCTXDataType)3, 49433 },
	{ (Il2CppRGCTXDataType)3, 49434 },
	{ (Il2CppRGCTXDataType)3, 49435 },
	{ (Il2CppRGCTXDataType)3, 49436 },
	{ (Il2CppRGCTXDataType)2, 17108 },
	{ (Il2CppRGCTXDataType)3, 49437 },
	{ (Il2CppRGCTXDataType)3, 49438 },
	{ (Il2CppRGCTXDataType)3, 49439 },
	{ (Il2CppRGCTXDataType)3, 49440 },
	{ (Il2CppRGCTXDataType)3, 49441 },
	{ (Il2CppRGCTXDataType)3, 49442 },
	{ (Il2CppRGCTXDataType)2, 27559 },
	{ (Il2CppRGCTXDataType)3, 49443 },
	{ (Il2CppRGCTXDataType)3, 49444 },
	{ (Il2CppRGCTXDataType)3, 49445 },
	{ (Il2CppRGCTXDataType)3, 49446 },
	{ (Il2CppRGCTXDataType)3, 49447 },
	{ (Il2CppRGCTXDataType)3, 49448 },
	{ (Il2CppRGCTXDataType)3, 49449 },
	{ (Il2CppRGCTXDataType)3, 49450 },
	{ (Il2CppRGCTXDataType)2, 17277 },
	{ (Il2CppRGCTXDataType)3, 49451 },
	{ (Il2CppRGCTXDataType)2, 27560 },
	{ (Il2CppRGCTXDataType)3, 49452 },
	{ (Il2CppRGCTXDataType)3, 49453 },
	{ (Il2CppRGCTXDataType)3, 49454 },
	{ (Il2CppRGCTXDataType)3, 49455 },
	{ (Il2CppRGCTXDataType)3, 49456 },
	{ (Il2CppRGCTXDataType)2, 17286 },
	{ (Il2CppRGCTXDataType)3, 49457 },
	{ (Il2CppRGCTXDataType)3, 49458 },
	{ (Il2CppRGCTXDataType)3, 49459 },
	{ (Il2CppRGCTXDataType)2, 17283 },
	{ (Il2CppRGCTXDataType)2, 17313 },
	{ (Il2CppRGCTXDataType)2, 27561 },
	{ (Il2CppRGCTXDataType)2, 17315 },
	{ (Il2CppRGCTXDataType)2, 27562 },
	{ (Il2CppRGCTXDataType)3, 49460 },
	{ (Il2CppRGCTXDataType)3, 49461 },
};
extern const Il2CppCodeGenModule g_Unity_LocalizationCodeGenModule;
const Il2CppCodeGenModule g_Unity_LocalizationCodeGenModule = 
{
	"Unity.Localization.dll",
	1161,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	49,
	s_rgctxIndices,
	341,
	s_rgctxValues,
	NULL,
};
