﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412 (void);
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57 (void);
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937 (void);
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018 (void);
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E (void);
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31 (void);
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1 (void);
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6 (void);
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93 (void);
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6 (void);
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460 (void);
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45 (void);
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B (void);
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C (void);
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268 (void);
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF (void);
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153 (void);
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477 (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91 (void);
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E (void);
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265 (void);
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4 (void);
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502 (void);
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1 (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465 (void);
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34 (void);
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB (void);
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252 (void);
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F (void);
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058 (void);
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218 (void);
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D (void);
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D (void);
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63 (void);
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD (void);
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E (void);
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF (void);
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB (void);
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E (void);
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598 (void);
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1 (void);
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679 (void);
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129 (void);
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A (void);
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD (void);
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2 (void);
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x00000031 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x00000032 System.Exception System.Linq.Error::NoMatch()
extern void Error_NoMatch_mA0FE78EC100066FA506B4C1C3AEC2E9E2DB79945 (void);
// 0x00000033 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000035 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000036 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
// 0x0000003A System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TCollection>>,System.Func`3<TSource,TCollection,TResult>)
// 0x0000003B System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Skip(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::SkipIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003F System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000040 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000041 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Concat(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000043 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::ConcatIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000044 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000045 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000046 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000047 System.Boolean System.Linq.Enumerable::SequenceEqual(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000048 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000049 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004A System.Collections.Generic.Dictionary`2<TKey,TSource> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000004B System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x0000004C System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x0000004D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfType(System.Collections.IEnumerable)
// 0x0000004E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::OfTypeIterator(System.Collections.IEnumerable)
// 0x0000004F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Cast(System.Collections.IEnumerable)
// 0x00000050 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::CastIterator(System.Collections.IEnumerable)
// 0x00000051 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000052 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000053 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000054 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000055 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000056 System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::Range(System.Int32,System.Int32)
extern void Enumerable_Range_mA545670D76B68795D0126AC84B994E2AD66E2415 (void);
// 0x00000057 System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::RangeIterator(System.Int32,System.Int32)
extern void Enumerable_RangeIterator_m8BC9AE9DF66A6AB3D05D8F7B55D65539133C984A (void);
// 0x00000058 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x00000059 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000005A System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000005B System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000005C System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000005D System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000005E System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x0000005F TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000060 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000061 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000062 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000063 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000064 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000065 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000066 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000067 System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000068 System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000069 System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000006A System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000006B System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000006C System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x0000006D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000006E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006F System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000070 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000071 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000072 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000073 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000074 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000075 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000076 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000077 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000078 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000079 System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000007A System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Clone()
// 0x0000007B System.Void System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Dispose()
// 0x0000007C System.Boolean System.Linq.Enumerable_WhereSelectEnumerableIterator`2::MoveNext()
// 0x0000007D System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000007E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007F System.Void System.Linq.Enumerable_WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000080 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Clone()
// 0x00000081 System.Boolean System.Linq.Enumerable_WhereSelectArrayIterator`2::MoveNext()
// 0x00000082 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000083 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000084 System.Void System.Linq.Enumerable_WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000085 System.Linq.Enumerable_Iterator`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Clone()
// 0x00000086 System.Boolean System.Linq.Enumerable_WhereSelectListIterator`2::MoveNext()
// 0x00000087 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable_WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000088 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable_WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000089 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000008A System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000008B System.Void System.Linq.Enumerable_<>c__DisplayClass7_0`3::.ctor()
// 0x0000008C TResult System.Linq.Enumerable_<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x0000008D System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x0000008E System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000008F System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__17`2::MoveNext()
// 0x00000090 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x00000091 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x00000092 TResult System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x00000093 System.Void System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x00000094 System.Object System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000095 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000096 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000097 System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::.ctor(System.Int32)
// 0x00000098 System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.IDisposable.Dispose()
// 0x00000099 System.Boolean System.Linq.Enumerable_<SelectManyIterator>d__23`3::MoveNext()
// 0x0000009A System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::<>m__Finally1()
// 0x0000009B System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::<>m__Finally2()
// 0x0000009C TResult System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000009D System.Void System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.IEnumerator.Reset()
// 0x0000009E System.Object System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.IEnumerator.get_Current()
// 0x0000009F System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000A0 System.Collections.IEnumerator System.Linq.Enumerable_<SelectManyIterator>d__23`3::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A1 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x000000A2 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x000000A3 System.Boolean System.Linq.Enumerable_<TakeIterator>d__25`1::MoveNext()
// 0x000000A4 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::<>m__Finally1()
// 0x000000A5 TSource System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000A6 System.Void System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x000000A7 System.Object System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x000000A8 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000A9 System.Collections.IEnumerator System.Linq.Enumerable_<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AA System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::.ctor(System.Int32)
// 0x000000AB System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::System.IDisposable.Dispose()
// 0x000000AC System.Boolean System.Linq.Enumerable_<SkipIterator>d__31`1::MoveNext()
// 0x000000AD System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::<>m__Finally1()
// 0x000000AE TSource System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000AF System.Void System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerator.Reset()
// 0x000000B0 System.Object System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerator.get_Current()
// 0x000000B1 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000B2 System.Collections.IEnumerator System.Linq.Enumerable_<SkipIterator>d__31`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B3 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::.ctor(System.Int32)
// 0x000000B4 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::System.IDisposable.Dispose()
// 0x000000B5 System.Boolean System.Linq.Enumerable_<ConcatIterator>d__59`1::MoveNext()
// 0x000000B6 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::<>m__Finally1()
// 0x000000B7 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::<>m__Finally2()
// 0x000000B8 TSource System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000B9 System.Void System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerator.Reset()
// 0x000000BA System.Object System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerator.get_Current()
// 0x000000BB System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000BC System.Collections.IEnumerator System.Linq.Enumerable_<ConcatIterator>d__59`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000BD System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x000000BE System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x000000BF System.Boolean System.Linq.Enumerable_<DistinctIterator>d__68`1::MoveNext()
// 0x000000C0 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::<>m__Finally1()
// 0x000000C1 TSource System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000C2 System.Void System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x000000C3 System.Object System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x000000C4 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000C5 System.Collections.IEnumerator System.Linq.Enumerable_<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C6 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::.ctor(System.Int32)
// 0x000000C7 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.IDisposable.Dispose()
// 0x000000C8 System.Boolean System.Linq.Enumerable_<OfTypeIterator>d__97`1::MoveNext()
// 0x000000C9 System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::<>m__Finally1()
// 0x000000CA TResult System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000CB System.Void System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.Reset()
// 0x000000CC System.Object System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerator.get_Current()
// 0x000000CD System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000CE System.Collections.IEnumerator System.Linq.Enumerable_<OfTypeIterator>d__97`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000CF System.Void System.Linq.Enumerable_<CastIterator>d__99`1::.ctor(System.Int32)
// 0x000000D0 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.IDisposable.Dispose()
// 0x000000D1 System.Boolean System.Linq.Enumerable_<CastIterator>d__99`1::MoveNext()
// 0x000000D2 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::<>m__Finally1()
// 0x000000D3 TResult System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000D4 System.Void System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.Reset()
// 0x000000D5 System.Object System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerator.get_Current()
// 0x000000D6 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000D7 System.Collections.IEnumerator System.Linq.Enumerable_<CastIterator>d__99`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000D8 System.Void System.Linq.Enumerable_<RangeIterator>d__115::.ctor(System.Int32)
extern void U3CRangeIteratorU3Ed__115__ctor_m3B8C9ADCE5DD64A09B124BD33754D2032A129161 (void);
// 0x000000D9 System.Void System.Linq.Enumerable_<RangeIterator>d__115::System.IDisposable.Dispose()
extern void U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m309B1CA342B62F07D81D8B0FD41FA270E49AEA40 (void);
// 0x000000DA System.Boolean System.Linq.Enumerable_<RangeIterator>d__115::MoveNext()
extern void U3CRangeIteratorU3Ed__115_MoveNext_m52450B0FF0EA2386F02F97A26B86EEDFB6F428DE (void);
// 0x000000DB System.Int32 System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.Generic.IEnumerator<System.Int32>.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m23A5F7D49A4221419AE2C01531FEC54669A78646 (void);
// 0x000000DC System.Void System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerator.Reset()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_mD099802F41E0B4017B1775F7A0F7A0C3EAE5C059 (void);
// 0x000000DD System.Object System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerator.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mC98C2271FCDACABA4C52610AB5E5A98C08DF2680 (void);
// 0x000000DE System.Collections.Generic.IEnumerator`1<System.Int32> System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.Generic.IEnumerable<System.Int32>.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_mF58B1118E0E226A7EC3F79DC9BF2ECF19E9A9B94 (void);
// 0x000000DF System.Collections.IEnumerator System.Linq.Enumerable_<RangeIterator>d__115::System.Collections.IEnumerable.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_m2FC1ECA81BE4003BFABCDE8654160C8BAD39BC9B (void);
// 0x000000E0 System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x000000E1 System.Func`2<TElement,TElement> System.Linq.IdentityFunction`1::get_Instance()
// 0x000000E2 System.Void System.Linq.IdentityFunction`1_<>c::.cctor()
// 0x000000E3 System.Void System.Linq.IdentityFunction`1_<>c::.ctor()
// 0x000000E4 TElement System.Linq.IdentityFunction`1_<>c::<get_Instance>b__1_0(TElement)
// 0x000000E5 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000E6 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000E7 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000E8 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000E9 System.Void System.Linq.Set`1::Resize()
// 0x000000EA System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000EB System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000EC System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000ED System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000EE System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000EF System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000F0 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000F1 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000F2 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x000000F3 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000F4 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000F5 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000F6 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000F7 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000F8 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000F9 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000FA System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000FB System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000FC System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000FD System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000FE System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000FF System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000100 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000101 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000102 System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32*,System.Int32)
extern void BitHelper__ctor_m2770BB414919277B2CF522840B54819F5082CD80 (void);
// 0x00000103 System.Void System.Collections.Generic.BitHelper::.ctor(System.Int32[],System.Int32)
extern void BitHelper__ctor_m7A8E43BE1D2A4ED086E708B6FFE693322FC9D2EB (void);
// 0x00000104 System.Void System.Collections.Generic.BitHelper::MarkBit(System.Int32)
extern void BitHelper_MarkBit_m1C6D787021BEA9D02DCA0762C09E5D443E04A86B (void);
// 0x00000105 System.Boolean System.Collections.Generic.BitHelper::IsMarked(System.Int32)
extern void BitHelper_IsMarked_m6036A81F50D820045D3F62E52D57098A332AB608 (void);
// 0x00000106 System.Int32 System.Collections.Generic.BitHelper::ToIntArrayLength(System.Int32)
extern void BitHelper_ToIntArrayLength_m32A0B1B014CB81891165AC325514784171C8E7B3 (void);
// 0x00000107 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000108 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000109 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000010A System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEnumerable`1<T>,System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000010B System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000010C System.Void System.Collections.Generic.HashSet`1::CopyFrom(System.Collections.Generic.HashSet`1<T>)
// 0x0000010D System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x0000010E System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x0000010F System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000110 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000111 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000112 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000113 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000114 System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000115 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000116 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000117 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000118 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000119 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000011A System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000011B System.Void System.Collections.Generic.HashSet`1::IntersectWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000011C System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000011D System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000011E System.Collections.Generic.IEqualityComparer`1<T> System.Collections.Generic.HashSet`1::get_Comparer()
// 0x0000011F System.Void System.Collections.Generic.HashSet`1::TrimExcess()
// 0x00000120 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000121 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000122 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000123 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000124 System.Void System.Collections.Generic.HashSet`1::AddValue(System.Int32,System.Int32,T)
// 0x00000125 System.Void System.Collections.Generic.HashSet`1::IntersectWithHashSetWithSameEC(System.Collections.Generic.HashSet`1<T>)
// 0x00000126 System.Void System.Collections.Generic.HashSet`1::IntersectWithEnumerable(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000127 System.Int32 System.Collections.Generic.HashSet`1::InternalIndexOf(T)
// 0x00000128 System.Boolean System.Collections.Generic.HashSet`1::AreEqualityComparersEqual(System.Collections.Generic.HashSet`1<T>,System.Collections.Generic.HashSet`1<T>)
// 0x00000129 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000012A System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000012B System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x0000012C System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000012D T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x0000012E System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000012F System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[303] = 
{
	SR_GetString_mD7FC73A3473F4F165E55F8B4A7088F2E9F9CC412,
	AesManaged__ctor_m79644F6BCD0E8C2D8BAF1B1E22E90D3C364F5C57,
	AesManaged_get_FeedbackSize_mCFE4C56DFF81F5E616CE535AB7D9E37DC1B7A937,
	AesManaged_get_IV_mB1D7896A5F5E71B8B7938A5DF3A743FC2E444018,
	AesManaged_set_IV_m1DBDC4FDAE66A5F2FA99AA4A4E76769BB8897D1E,
	AesManaged_get_Key_m4CC3B2D28A918B935AD42F3F8D54E93A6CB2FA31,
	AesManaged_set_Key_m35D61E5FD8942054840B1F24E685E91E3E6CA6E1,
	AesManaged_get_KeySize_mBE6EA533BD5978099974A74FF3DE3ECB8B173CD6,
	AesManaged_set_KeySize_m2003A2B9200003C23B544F56E949A0630AA87F93,
	AesManaged_get_Mode_mF9D7222B2AB685AC46F4564B6F2247114244AEF6,
	AesManaged_set_Mode_mA5CF4C1F3B41503C6E09373ADB0B8983A6F61460,
	AesManaged_get_Padding_mD81B3F96D3421F6CD2189A01D65736A9098ACD45,
	AesManaged_set_Padding_m6B07EC4A0F1F451417DC0AC64E9D637D7916866B,
	AesManaged_CreateDecryptor_m41AE4428FE60C9FD485640F3A09F1BF345452A3C,
	AesManaged_CreateDecryptor_m7240F8C38B99CE73159DE7455046E951C4900268,
	AesManaged_CreateEncryptor_mB2BBCAB8753A59FFB572091D2EF80F287CD951BF,
	AesManaged_CreateEncryptor_m1E4EB80DE75FCF9E940228E1D7664C0EA1378153,
	AesManaged_Dispose_mB0D969841D51825F37095A93E73A50C15C1A1477,
	AesManaged_GenerateIV_mBB19651CC37782273A882055D4E63370268F2D91,
	AesManaged_GenerateKey_mF6673B955AE82377595277C6B78C7DA8A16F480E,
	AesCryptoServiceProvider__ctor_mA9857852BC34D8AB0F463C1AF1837CBBD9102265,
	AesCryptoServiceProvider_GenerateIV_m18539D5136BA9A2FC71F439150D16E35AD3BF5C4,
	AesCryptoServiceProvider_GenerateKey_m574F877FD23D1F07033FC035E89BE232303F3502,
	AesCryptoServiceProvider_CreateDecryptor_mAB5FB857F549A86D986461C8665BE6B2393305D1,
	AesCryptoServiceProvider_CreateEncryptor_m6BF20D5D8424DB627CD3010D9E4C8555C6BD0465,
	AesCryptoServiceProvider_get_IV_m6A46F1C255ABE41F98BEE8C0C37D6AFBB9F29D34,
	AesCryptoServiceProvider_set_IV_mCB88C0F651B17F3EC7575F16E14C9E3BD2DB24DB,
	AesCryptoServiceProvider_get_Key_mAC979BC922E8F1F15B36220E77972AC9CE5D5252,
	AesCryptoServiceProvider_set_Key_m65785032C270005BC120157A0C9D019F6F6BC96F,
	AesCryptoServiceProvider_get_KeySize_m3081171DF6C11CA55ECEBA29B9559D18E78D8058,
	AesCryptoServiceProvider_set_KeySize_mA994D2D3098216C0B8C4F02C0F0A0F63D4256218,
	AesCryptoServiceProvider_get_FeedbackSize_m9DC2E1C3E84CC674ADB2D7E6B06066F333BEC89D,
	AesCryptoServiceProvider_get_Mode_m3E1CBFD4D7CE748F3AB615EB88DE1A5D7238285D,
	AesCryptoServiceProvider_set_Mode_mFE7044929761BABE312D1146B0ED51B331E35D63,
	AesCryptoServiceProvider_get_Padding_m89D49B05949BA2C6C557EFA5211B4934D279C7AD,
	AesCryptoServiceProvider_set_Padding_mD3353CD8F4B931AA00203000140520775643F96E,
	AesCryptoServiceProvider_CreateDecryptor_mB1F90A7339DA65542795E17DF9C37810BD088DDF,
	AesCryptoServiceProvider_CreateEncryptor_m9555DFFCA344DF06C8B88DDE2EB987B3958EC6BB,
	AesCryptoServiceProvider_Dispose_m7123198904819E2BF2B1398E20047B316C3D7D1E,
	AesTransform__ctor_m3903A599E8B2C3F7AB3B70E1258980151D639598,
	AesTransform_ECB_m2E2F4E2B307B0D34FEADF38684007E622FCEDFD1,
	AesTransform_SubByte_m2D77D545ABD3D84C04741B80ABB74BEFE8C55679,
	AesTransform_Encrypt128_m57DA74A7E05818DFD92F2614F8F65B0D1E696129,
	AesTransform_Decrypt128_m075F7BA40A4CFECA6F6A379065B731586EDDB23A,
	AesTransform__cctor_mAC6D46ED54345C2D23DFCA026C69029757222CFD,
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	Error_NoMatch_mA0FE78EC100066FA506B4C1C3AEC2E9E2DB79945,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Range_mA545670D76B68795D0126AC84B994E2AD66E2415,
	Enumerable_RangeIterator_m8BC9AE9DF66A6AB3D05D8F7B55D65539133C984A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CRangeIteratorU3Ed__115__ctor_m3B8C9ADCE5DD64A09B124BD33754D2032A129161,
	U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m309B1CA342B62F07D81D8B0FD41FA270E49AEA40,
	U3CRangeIteratorU3Ed__115_MoveNext_m52450B0FF0EA2386F02F97A26B86EEDFB6F428DE,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m23A5F7D49A4221419AE2C01531FEC54669A78646,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_mD099802F41E0B4017B1775F7A0F7A0C3EAE5C059,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mC98C2271FCDACABA4C52610AB5E5A98C08DF2680,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_mF58B1118E0E226A7EC3F79DC9BF2ECF19E9A9B94,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_m2FC1ECA81BE4003BFABCDE8654160C8BAD39BC9B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BitHelper__ctor_m2770BB414919277B2CF522840B54819F5082CD80,
	BitHelper__ctor_m7A8E43BE1D2A4ED086E708B6FFE693322FC9D2EB,
	BitHelper_MarkBit_m1C6D787021BEA9D02DCA0762C09E5D443E04A86B,
	BitHelper_IsMarked_m6036A81F50D820045D3F62E52D57098A332AB608,
	BitHelper_ToIntArrayLength_m32A0B1B014CB81891165AC325514784171C8E7B3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[303] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	114,
	14,
	114,
	31,
	23,
	23,
	23,
	23,
	23,
	114,
	114,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	960,
	27,
	37,
	214,
	214,
	3,
	0,
	0,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	592,
	592,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	89,
	10,
	23,
	14,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	64,
	139,
	32,
	30,
	21,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[77] = 
{
	{ 0x02000008, { 113, 4 } },
	{ 0x02000009, { 117, 9 } },
	{ 0x0200000A, { 128, 7 } },
	{ 0x0200000B, { 137, 10 } },
	{ 0x0200000C, { 149, 11 } },
	{ 0x0200000D, { 163, 9 } },
	{ 0x0200000E, { 175, 12 } },
	{ 0x0200000F, { 190, 1 } },
	{ 0x02000010, { 191, 2 } },
	{ 0x02000011, { 193, 12 } },
	{ 0x02000012, { 205, 13 } },
	{ 0x02000013, { 218, 8 } },
	{ 0x02000014, { 226, 8 } },
	{ 0x02000015, { 234, 9 } },
	{ 0x02000016, { 243, 11 } },
	{ 0x02000017, { 254, 6 } },
	{ 0x02000018, { 260, 6 } },
	{ 0x0200001A, { 266, 2 } },
	{ 0x0200001B, { 268, 4 } },
	{ 0x0200001C, { 272, 3 } },
	{ 0x0200001E, { 275, 8 } },
	{ 0x02000020, { 283, 3 } },
	{ 0x02000021, { 288, 5 } },
	{ 0x02000022, { 293, 7 } },
	{ 0x02000023, { 300, 3 } },
	{ 0x02000024, { 303, 7 } },
	{ 0x02000025, { 310, 4 } },
	{ 0x02000027, { 314, 40 } },
	{ 0x02000029, { 354, 2 } },
	{ 0x06000033, { 0, 10 } },
	{ 0x06000034, { 10, 10 } },
	{ 0x06000035, { 20, 5 } },
	{ 0x06000036, { 25, 5 } },
	{ 0x06000037, { 30, 1 } },
	{ 0x06000038, { 31, 2 } },
	{ 0x06000039, { 33, 1 } },
	{ 0x0600003A, { 34, 2 } },
	{ 0x0600003B, { 36, 1 } },
	{ 0x0600003C, { 37, 2 } },
	{ 0x0600003D, { 39, 1 } },
	{ 0x0600003E, { 40, 2 } },
	{ 0x0600003F, { 42, 2 } },
	{ 0x06000040, { 44, 2 } },
	{ 0x06000041, { 46, 1 } },
	{ 0x06000042, { 47, 1 } },
	{ 0x06000043, { 48, 2 } },
	{ 0x06000044, { 50, 1 } },
	{ 0x06000045, { 51, 2 } },
	{ 0x06000046, { 53, 1 } },
	{ 0x06000047, { 54, 5 } },
	{ 0x06000048, { 59, 3 } },
	{ 0x06000049, { 62, 2 } },
	{ 0x0600004A, { 64, 3 } },
	{ 0x0600004B, { 67, 1 } },
	{ 0x0600004C, { 68, 7 } },
	{ 0x0600004D, { 75, 1 } },
	{ 0x0600004E, { 76, 2 } },
	{ 0x0600004F, { 78, 2 } },
	{ 0x06000050, { 80, 2 } },
	{ 0x06000051, { 82, 4 } },
	{ 0x06000052, { 86, 3 } },
	{ 0x06000053, { 89, 3 } },
	{ 0x06000054, { 92, 4 } },
	{ 0x06000055, { 96, 3 } },
	{ 0x06000058, { 99, 1 } },
	{ 0x06000059, { 100, 1 } },
	{ 0x0600005A, { 101, 3 } },
	{ 0x0600005B, { 104, 2 } },
	{ 0x0600005C, { 106, 2 } },
	{ 0x0600005D, { 108, 5 } },
	{ 0x0600006D, { 126, 2 } },
	{ 0x06000072, { 135, 2 } },
	{ 0x06000077, { 147, 2 } },
	{ 0x0600007D, { 160, 3 } },
	{ 0x06000082, { 172, 3 } },
	{ 0x06000087, { 187, 3 } },
	{ 0x060000EE, { 286, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[356] = 
{
	{ (Il2CppRGCTXDataType)2, 27351 },
	{ (Il2CppRGCTXDataType)3, 48684 },
	{ (Il2CppRGCTXDataType)2, 27352 },
	{ (Il2CppRGCTXDataType)2, 27353 },
	{ (Il2CppRGCTXDataType)3, 48685 },
	{ (Il2CppRGCTXDataType)2, 27354 },
	{ (Il2CppRGCTXDataType)2, 27355 },
	{ (Il2CppRGCTXDataType)3, 48686 },
	{ (Il2CppRGCTXDataType)2, 27356 },
	{ (Il2CppRGCTXDataType)3, 48687 },
	{ (Il2CppRGCTXDataType)2, 27357 },
	{ (Il2CppRGCTXDataType)3, 48688 },
	{ (Il2CppRGCTXDataType)2, 27358 },
	{ (Il2CppRGCTXDataType)2, 27359 },
	{ (Il2CppRGCTXDataType)3, 48689 },
	{ (Il2CppRGCTXDataType)2, 27360 },
	{ (Il2CppRGCTXDataType)2, 27361 },
	{ (Il2CppRGCTXDataType)3, 48690 },
	{ (Il2CppRGCTXDataType)2, 27362 },
	{ (Il2CppRGCTXDataType)3, 48691 },
	{ (Il2CppRGCTXDataType)2, 27363 },
	{ (Il2CppRGCTXDataType)3, 48692 },
	{ (Il2CppRGCTXDataType)3, 48693 },
	{ (Il2CppRGCTXDataType)2, 11584 },
	{ (Il2CppRGCTXDataType)3, 48694 },
	{ (Il2CppRGCTXDataType)2, 27364 },
	{ (Il2CppRGCTXDataType)3, 48695 },
	{ (Il2CppRGCTXDataType)3, 48696 },
	{ (Il2CppRGCTXDataType)2, 11591 },
	{ (Il2CppRGCTXDataType)3, 48697 },
	{ (Il2CppRGCTXDataType)3, 48698 },
	{ (Il2CppRGCTXDataType)2, 27365 },
	{ (Il2CppRGCTXDataType)3, 48699 },
	{ (Il2CppRGCTXDataType)3, 48700 },
	{ (Il2CppRGCTXDataType)2, 27366 },
	{ (Il2CppRGCTXDataType)3, 48701 },
	{ (Il2CppRGCTXDataType)3, 48702 },
	{ (Il2CppRGCTXDataType)2, 27367 },
	{ (Il2CppRGCTXDataType)3, 48703 },
	{ (Il2CppRGCTXDataType)3, 48704 },
	{ (Il2CppRGCTXDataType)2, 27368 },
	{ (Il2CppRGCTXDataType)3, 48705 },
	{ (Il2CppRGCTXDataType)2, 27369 },
	{ (Il2CppRGCTXDataType)3, 48706 },
	{ (Il2CppRGCTXDataType)2, 27370 },
	{ (Il2CppRGCTXDataType)3, 48707 },
	{ (Il2CppRGCTXDataType)3, 48708 },
	{ (Il2CppRGCTXDataType)3, 48709 },
	{ (Il2CppRGCTXDataType)2, 27371 },
	{ (Il2CppRGCTXDataType)3, 48710 },
	{ (Il2CppRGCTXDataType)3, 48711 },
	{ (Il2CppRGCTXDataType)2, 27372 },
	{ (Il2CppRGCTXDataType)3, 48712 },
	{ (Il2CppRGCTXDataType)3, 48713 },
	{ (Il2CppRGCTXDataType)3, 48714 },
	{ (Il2CppRGCTXDataType)2, 27373 },
	{ (Il2CppRGCTXDataType)2, 11651 },
	{ (Il2CppRGCTXDataType)2, 27374 },
	{ (Il2CppRGCTXDataType)2, 11653 },
	{ (Il2CppRGCTXDataType)2, 27375 },
	{ (Il2CppRGCTXDataType)3, 48715 },
	{ (Il2CppRGCTXDataType)3, 48716 },
	{ (Il2CppRGCTXDataType)2, 11659 },
	{ (Il2CppRGCTXDataType)3, 48717 },
	{ (Il2CppRGCTXDataType)3, 48718 },
	{ (Il2CppRGCTXDataType)2, 27376 },
	{ (Il2CppRGCTXDataType)3, 48719 },
	{ (Il2CppRGCTXDataType)3, 48720 },
	{ (Il2CppRGCTXDataType)2, 11679 },
	{ (Il2CppRGCTXDataType)3, 48721 },
	{ (Il2CppRGCTXDataType)2, 11672 },
	{ (Il2CppRGCTXDataType)2, 27377 },
	{ (Il2CppRGCTXDataType)3, 48722 },
	{ (Il2CppRGCTXDataType)3, 48723 },
	{ (Il2CppRGCTXDataType)3, 48724 },
	{ (Il2CppRGCTXDataType)3, 48725 },
	{ (Il2CppRGCTXDataType)2, 27378 },
	{ (Il2CppRGCTXDataType)3, 48726 },
	{ (Il2CppRGCTXDataType)2, 11684 },
	{ (Il2CppRGCTXDataType)3, 48727 },
	{ (Il2CppRGCTXDataType)2, 27379 },
	{ (Il2CppRGCTXDataType)3, 48728 },
	{ (Il2CppRGCTXDataType)2, 27380 },
	{ (Il2CppRGCTXDataType)2, 27381 },
	{ (Il2CppRGCTXDataType)2, 11688 },
	{ (Il2CppRGCTXDataType)2, 27382 },
	{ (Il2CppRGCTXDataType)2, 11690 },
	{ (Il2CppRGCTXDataType)2, 27383 },
	{ (Il2CppRGCTXDataType)3, 48729 },
	{ (Il2CppRGCTXDataType)2, 11693 },
	{ (Il2CppRGCTXDataType)2, 27384 },
	{ (Il2CppRGCTXDataType)3, 48730 },
	{ (Il2CppRGCTXDataType)2, 27385 },
	{ (Il2CppRGCTXDataType)2, 27386 },
	{ (Il2CppRGCTXDataType)2, 11696 },
	{ (Il2CppRGCTXDataType)2, 27387 },
	{ (Il2CppRGCTXDataType)2, 11698 },
	{ (Il2CppRGCTXDataType)2, 27388 },
	{ (Il2CppRGCTXDataType)3, 48731 },
	{ (Il2CppRGCTXDataType)2, 27389 },
	{ (Il2CppRGCTXDataType)2, 11704 },
	{ (Il2CppRGCTXDataType)2, 11706 },
	{ (Il2CppRGCTXDataType)2, 27390 },
	{ (Il2CppRGCTXDataType)3, 48732 },
	{ (Il2CppRGCTXDataType)2, 27391 },
	{ (Il2CppRGCTXDataType)2, 11709 },
	{ (Il2CppRGCTXDataType)2, 27392 },
	{ (Il2CppRGCTXDataType)3, 48733 },
	{ (Il2CppRGCTXDataType)3, 48734 },
	{ (Il2CppRGCTXDataType)2, 27393 },
	{ (Il2CppRGCTXDataType)2, 11713 },
	{ (Il2CppRGCTXDataType)2, 27394 },
	{ (Il2CppRGCTXDataType)2, 11715 },
	{ (Il2CppRGCTXDataType)3, 48735 },
	{ (Il2CppRGCTXDataType)3, 48736 },
	{ (Il2CppRGCTXDataType)2, 11718 },
	{ (Il2CppRGCTXDataType)3, 48737 },
	{ (Il2CppRGCTXDataType)3, 48738 },
	{ (Il2CppRGCTXDataType)2, 11730 },
	{ (Il2CppRGCTXDataType)2, 27395 },
	{ (Il2CppRGCTXDataType)3, 48739 },
	{ (Il2CppRGCTXDataType)3, 48740 },
	{ (Il2CppRGCTXDataType)2, 11732 },
	{ (Il2CppRGCTXDataType)2, 27083 },
	{ (Il2CppRGCTXDataType)3, 48741 },
	{ (Il2CppRGCTXDataType)3, 48742 },
	{ (Il2CppRGCTXDataType)2, 27396 },
	{ (Il2CppRGCTXDataType)3, 48743 },
	{ (Il2CppRGCTXDataType)3, 48744 },
	{ (Il2CppRGCTXDataType)2, 11742 },
	{ (Il2CppRGCTXDataType)2, 27397 },
	{ (Il2CppRGCTXDataType)3, 48745 },
	{ (Il2CppRGCTXDataType)3, 48746 },
	{ (Il2CppRGCTXDataType)3, 47272 },
	{ (Il2CppRGCTXDataType)3, 48747 },
	{ (Il2CppRGCTXDataType)2, 27398 },
	{ (Il2CppRGCTXDataType)3, 48748 },
	{ (Il2CppRGCTXDataType)3, 48749 },
	{ (Il2CppRGCTXDataType)2, 11754 },
	{ (Il2CppRGCTXDataType)2, 27399 },
	{ (Il2CppRGCTXDataType)3, 48750 },
	{ (Il2CppRGCTXDataType)3, 48751 },
	{ (Il2CppRGCTXDataType)3, 48752 },
	{ (Il2CppRGCTXDataType)3, 48753 },
	{ (Il2CppRGCTXDataType)3, 48754 },
	{ (Il2CppRGCTXDataType)3, 47278 },
	{ (Il2CppRGCTXDataType)3, 48755 },
	{ (Il2CppRGCTXDataType)2, 27400 },
	{ (Il2CppRGCTXDataType)3, 48756 },
	{ (Il2CppRGCTXDataType)3, 48757 },
	{ (Il2CppRGCTXDataType)2, 11767 },
	{ (Il2CppRGCTXDataType)2, 27401 },
	{ (Il2CppRGCTXDataType)3, 48758 },
	{ (Il2CppRGCTXDataType)3, 48759 },
	{ (Il2CppRGCTXDataType)2, 11769 },
	{ (Il2CppRGCTXDataType)2, 27402 },
	{ (Il2CppRGCTXDataType)3, 48760 },
	{ (Il2CppRGCTXDataType)3, 48761 },
	{ (Il2CppRGCTXDataType)2, 27403 },
	{ (Il2CppRGCTXDataType)3, 48762 },
	{ (Il2CppRGCTXDataType)3, 48763 },
	{ (Il2CppRGCTXDataType)2, 27404 },
	{ (Il2CppRGCTXDataType)3, 48764 },
	{ (Il2CppRGCTXDataType)3, 48765 },
	{ (Il2CppRGCTXDataType)2, 11784 },
	{ (Il2CppRGCTXDataType)2, 27405 },
	{ (Il2CppRGCTXDataType)3, 48766 },
	{ (Il2CppRGCTXDataType)3, 48767 },
	{ (Il2CppRGCTXDataType)3, 48768 },
	{ (Il2CppRGCTXDataType)3, 47289 },
	{ (Il2CppRGCTXDataType)2, 27406 },
	{ (Il2CppRGCTXDataType)3, 48769 },
	{ (Il2CppRGCTXDataType)3, 48770 },
	{ (Il2CppRGCTXDataType)2, 27407 },
	{ (Il2CppRGCTXDataType)3, 48771 },
	{ (Il2CppRGCTXDataType)3, 48772 },
	{ (Il2CppRGCTXDataType)2, 11800 },
	{ (Il2CppRGCTXDataType)2, 27408 },
	{ (Il2CppRGCTXDataType)3, 48773 },
	{ (Il2CppRGCTXDataType)3, 48774 },
	{ (Il2CppRGCTXDataType)3, 48775 },
	{ (Il2CppRGCTXDataType)3, 48776 },
	{ (Il2CppRGCTXDataType)3, 48777 },
	{ (Il2CppRGCTXDataType)3, 48778 },
	{ (Il2CppRGCTXDataType)3, 47295 },
	{ (Il2CppRGCTXDataType)2, 27409 },
	{ (Il2CppRGCTXDataType)3, 48779 },
	{ (Il2CppRGCTXDataType)3, 48780 },
	{ (Il2CppRGCTXDataType)2, 27410 },
	{ (Il2CppRGCTXDataType)3, 48781 },
	{ (Il2CppRGCTXDataType)3, 48782 },
	{ (Il2CppRGCTXDataType)3, 48783 },
	{ (Il2CppRGCTXDataType)3, 48784 },
	{ (Il2CppRGCTXDataType)3, 48785 },
	{ (Il2CppRGCTXDataType)3, 48786 },
	{ (Il2CppRGCTXDataType)2, 27411 },
	{ (Il2CppRGCTXDataType)2, 27412 },
	{ (Il2CppRGCTXDataType)3, 48787 },
	{ (Il2CppRGCTXDataType)2, 11835 },
	{ (Il2CppRGCTXDataType)2, 11829 },
	{ (Il2CppRGCTXDataType)3, 48788 },
	{ (Il2CppRGCTXDataType)2, 11828 },
	{ (Il2CppRGCTXDataType)2, 27413 },
	{ (Il2CppRGCTXDataType)3, 48789 },
	{ (Il2CppRGCTXDataType)3, 48790 },
	{ (Il2CppRGCTXDataType)3, 48791 },
	{ (Il2CppRGCTXDataType)3, 48792 },
	{ (Il2CppRGCTXDataType)2, 27414 },
	{ (Il2CppRGCTXDataType)2, 27415 },
	{ (Il2CppRGCTXDataType)3, 48793 },
	{ (Il2CppRGCTXDataType)2, 11849 },
	{ (Il2CppRGCTXDataType)2, 27416 },
	{ (Il2CppRGCTXDataType)3, 48794 },
	{ (Il2CppRGCTXDataType)3, 48795 },
	{ (Il2CppRGCTXDataType)2, 11842 },
	{ (Il2CppRGCTXDataType)2, 27417 },
	{ (Il2CppRGCTXDataType)3, 48796 },
	{ (Il2CppRGCTXDataType)3, 48797 },
	{ (Il2CppRGCTXDataType)3, 48798 },
	{ (Il2CppRGCTXDataType)2, 11867 },
	{ (Il2CppRGCTXDataType)2, 11862 },
	{ (Il2CppRGCTXDataType)3, 48799 },
	{ (Il2CppRGCTXDataType)2, 11861 },
	{ (Il2CppRGCTXDataType)2, 27418 },
	{ (Il2CppRGCTXDataType)3, 48800 },
	{ (Il2CppRGCTXDataType)3, 48801 },
	{ (Il2CppRGCTXDataType)3, 48802 },
	{ (Il2CppRGCTXDataType)2, 11877 },
	{ (Il2CppRGCTXDataType)2, 11872 },
	{ (Il2CppRGCTXDataType)3, 48803 },
	{ (Il2CppRGCTXDataType)2, 11871 },
	{ (Il2CppRGCTXDataType)2, 27419 },
	{ (Il2CppRGCTXDataType)3, 48804 },
	{ (Il2CppRGCTXDataType)3, 48805 },
	{ (Il2CppRGCTXDataType)3, 48806 },
	{ (Il2CppRGCTXDataType)3, 48807 },
	{ (Il2CppRGCTXDataType)2, 11887 },
	{ (Il2CppRGCTXDataType)2, 11882 },
	{ (Il2CppRGCTXDataType)3, 48808 },
	{ (Il2CppRGCTXDataType)2, 11881 },
	{ (Il2CppRGCTXDataType)2, 27420 },
	{ (Il2CppRGCTXDataType)3, 48809 },
	{ (Il2CppRGCTXDataType)3, 48810 },
	{ (Il2CppRGCTXDataType)3, 48811 },
	{ (Il2CppRGCTXDataType)2, 27421 },
	{ (Il2CppRGCTXDataType)3, 48812 },
	{ (Il2CppRGCTXDataType)2, 11900 },
	{ (Il2CppRGCTXDataType)2, 11892 },
	{ (Il2CppRGCTXDataType)3, 48813 },
	{ (Il2CppRGCTXDataType)3, 48814 },
	{ (Il2CppRGCTXDataType)2, 11891 },
	{ (Il2CppRGCTXDataType)2, 27422 },
	{ (Il2CppRGCTXDataType)3, 48815 },
	{ (Il2CppRGCTXDataType)3, 48816 },
	{ (Il2CppRGCTXDataType)3, 48817 },
	{ (Il2CppRGCTXDataType)2, 11904 },
	{ (Il2CppRGCTXDataType)3, 48818 },
	{ (Il2CppRGCTXDataType)2, 27423 },
	{ (Il2CppRGCTXDataType)3, 48819 },
	{ (Il2CppRGCTXDataType)3, 48820 },
	{ (Il2CppRGCTXDataType)3, 48821 },
	{ (Il2CppRGCTXDataType)2, 11912 },
	{ (Il2CppRGCTXDataType)3, 48822 },
	{ (Il2CppRGCTXDataType)2, 27424 },
	{ (Il2CppRGCTXDataType)3, 48823 },
	{ (Il2CppRGCTXDataType)3, 48824 },
	{ (Il2CppRGCTXDataType)2, 27425 },
	{ (Il2CppRGCTXDataType)2, 27426 },
	{ (Il2CppRGCTXDataType)2, 27427 },
	{ (Il2CppRGCTXDataType)3, 48825 },
	{ (Il2CppRGCTXDataType)2, 11927 },
	{ (Il2CppRGCTXDataType)3, 48826 },
	{ (Il2CppRGCTXDataType)2, 27428 },
	{ (Il2CppRGCTXDataType)3, 48827 },
	{ (Il2CppRGCTXDataType)2, 27428 },
	{ (Il2CppRGCTXDataType)3, 48828 },
	{ (Il2CppRGCTXDataType)2, 27429 },
	{ (Il2CppRGCTXDataType)2, 27430 },
	{ (Il2CppRGCTXDataType)3, 48829 },
	{ (Il2CppRGCTXDataType)3, 48830 },
	{ (Il2CppRGCTXDataType)2, 11944 },
	{ (Il2CppRGCTXDataType)3, 48831 },
	{ (Il2CppRGCTXDataType)2, 11945 },
	{ (Il2CppRGCTXDataType)2, 27431 },
	{ (Il2CppRGCTXDataType)3, 48832 },
	{ (Il2CppRGCTXDataType)3, 48833 },
	{ (Il2CppRGCTXDataType)2, 27432 },
	{ (Il2CppRGCTXDataType)3, 48834 },
	{ (Il2CppRGCTXDataType)2, 27433 },
	{ (Il2CppRGCTXDataType)3, 48835 },
	{ (Il2CppRGCTXDataType)3, 48836 },
	{ (Il2CppRGCTXDataType)3, 48837 },
	{ (Il2CppRGCTXDataType)2, 11964 },
	{ (Il2CppRGCTXDataType)3, 48838 },
	{ (Il2CppRGCTXDataType)2, 11972 },
	{ (Il2CppRGCTXDataType)3, 48839 },
	{ (Il2CppRGCTXDataType)2, 27434 },
	{ (Il2CppRGCTXDataType)2, 27435 },
	{ (Il2CppRGCTXDataType)3, 48840 },
	{ (Il2CppRGCTXDataType)3, 48841 },
	{ (Il2CppRGCTXDataType)3, 48842 },
	{ (Il2CppRGCTXDataType)3, 48843 },
	{ (Il2CppRGCTXDataType)3, 48844 },
	{ (Il2CppRGCTXDataType)3, 48845 },
	{ (Il2CppRGCTXDataType)2, 11988 },
	{ (Il2CppRGCTXDataType)2, 27436 },
	{ (Il2CppRGCTXDataType)3, 48846 },
	{ (Il2CppRGCTXDataType)3, 48847 },
	{ (Il2CppRGCTXDataType)2, 11992 },
	{ (Il2CppRGCTXDataType)3, 48848 },
	{ (Il2CppRGCTXDataType)2, 27437 },
	{ (Il2CppRGCTXDataType)2, 12002 },
	{ (Il2CppRGCTXDataType)2, 12000 },
	{ (Il2CppRGCTXDataType)2, 27438 },
	{ (Il2CppRGCTXDataType)3, 48849 },
	{ (Il2CppRGCTXDataType)2, 27439 },
	{ (Il2CppRGCTXDataType)3, 48850 },
	{ (Il2CppRGCTXDataType)3, 48851 },
	{ (Il2CppRGCTXDataType)2, 12013 },
	{ (Il2CppRGCTXDataType)3, 48852 },
	{ (Il2CppRGCTXDataType)2, 12013 },
	{ (Il2CppRGCTXDataType)3, 48853 },
	{ (Il2CppRGCTXDataType)2, 12030 },
	{ (Il2CppRGCTXDataType)3, 48854 },
	{ (Il2CppRGCTXDataType)3, 48855 },
	{ (Il2CppRGCTXDataType)3, 48856 },
	{ (Il2CppRGCTXDataType)2, 27440 },
	{ (Il2CppRGCTXDataType)3, 48857 },
	{ (Il2CppRGCTXDataType)3, 48858 },
	{ (Il2CppRGCTXDataType)3, 48859 },
	{ (Il2CppRGCTXDataType)2, 12010 },
	{ (Il2CppRGCTXDataType)3, 48860 },
	{ (Il2CppRGCTXDataType)3, 48861 },
	{ (Il2CppRGCTXDataType)2, 12015 },
	{ (Il2CppRGCTXDataType)3, 48862 },
	{ (Il2CppRGCTXDataType)1, 27441 },
	{ (Il2CppRGCTXDataType)2, 12014 },
	{ (Il2CppRGCTXDataType)3, 48863 },
	{ (Il2CppRGCTXDataType)1, 12014 },
	{ (Il2CppRGCTXDataType)1, 12010 },
	{ (Il2CppRGCTXDataType)2, 27440 },
	{ (Il2CppRGCTXDataType)2, 12014 },
	{ (Il2CppRGCTXDataType)2, 12012 },
	{ (Il2CppRGCTXDataType)2, 12016 },
	{ (Il2CppRGCTXDataType)3, 48864 },
	{ (Il2CppRGCTXDataType)3, 48865 },
	{ (Il2CppRGCTXDataType)3, 48866 },
	{ (Il2CppRGCTXDataType)3, 48867 },
	{ (Il2CppRGCTXDataType)3, 48868 },
	{ (Il2CppRGCTXDataType)3, 48869 },
	{ (Il2CppRGCTXDataType)3, 48870 },
	{ (Il2CppRGCTXDataType)3, 48871 },
	{ (Il2CppRGCTXDataType)3, 48872 },
	{ (Il2CppRGCTXDataType)2, 12011 },
	{ (Il2CppRGCTXDataType)3, 48873 },
	{ (Il2CppRGCTXDataType)2, 12026 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	303,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	77,
	s_rgctxIndices,
	356,
	s_rgctxValues,
	NULL,
};
