﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.TerrainData UnityEngine.Terrain::get_terrainData()
extern void Terrain_get_terrainData_mDB60C324B3424339C3C9FA6CDF6DC1C9B47D6E41 (void);
// 0x00000002 System.Void UnityEngine.Terrain::set_terrainData(UnityEngine.TerrainData)
extern void Terrain_set_terrainData_mEDD9205514CD21D3518EC11C03EBBF1BF8B9E81B (void);
// 0x00000003 System.Void UnityEngine.Terrain::set_treeDistance(System.Single)
extern void Terrain_set_treeDistance_m1B8BFC8FC1B85FB4CBD0C9D45E52EB4702DED208 (void);
// 0x00000004 System.Void UnityEngine.Terrain::set_treeBillboardDistance(System.Single)
extern void Terrain_set_treeBillboardDistance_m60BBE7FBD4B66661E02C4848EA912390DCA5F0D8 (void);
// 0x00000005 System.Void UnityEngine.Terrain::set_treeCrossFadeLength(System.Single)
extern void Terrain_set_treeCrossFadeLength_m89D60280B3A24A1A73BE1734469E5E1401A0040C (void);
// 0x00000006 System.Void UnityEngine.Terrain::set_treeMaximumFullLODCount(System.Int32)
extern void Terrain_set_treeMaximumFullLODCount_m2B151A511B6136505C7068350AC559B009B52995 (void);
// 0x00000007 System.Void UnityEngine.Terrain::set_heightmapPixelError(System.Single)
extern void Terrain_set_heightmapPixelError_mEE15C5552F735184366148B729A2330EE9D9BEAB (void);
// 0x00000008 System.Void UnityEngine.Terrain::set_basemapDistance(System.Single)
extern void Terrain_set_basemapDistance_m989F9BF5A5DF73030CA226C0640D0B4E95CCE33B (void);
// 0x00000009 System.Void UnityEngine.Terrain::set_materialTemplate(UnityEngine.Material)
extern void Terrain_set_materialTemplate_m3626DE42EAE3A425DB95E6C2D33BB0A4F44DD1A9 (void);
// 0x0000000A System.Boolean UnityEngine.Terrain::get_allowAutoConnect()
extern void Terrain_get_allowAutoConnect_mC1B0AC480E9AA5E33EDF412E8F9AA3EB4832BA67 (void);
// 0x0000000B System.Int32 UnityEngine.Terrain::get_groupingID()
extern void Terrain_get_groupingID_m8390315914A192A424C890605D780E638F5E1CC9 (void);
// 0x0000000C System.Single UnityEngine.Terrain::SampleHeight(UnityEngine.Vector3)
extern void Terrain_SampleHeight_mBA0B969DA2750257CE1C35CCC18E4AA6EB98DA5B (void);
// 0x0000000D System.Void UnityEngine.Terrain::AddTreeInstance(UnityEngine.TreeInstance)
extern void Terrain_AddTreeInstance_mBFB4F85C8F017DB981FCF44FFF4C6A5D7FFCF17E (void);
// 0x0000000E System.Void UnityEngine.Terrain::SetNeighbors(UnityEngine.Terrain,UnityEngine.Terrain,UnityEngine.Terrain,UnityEngine.Terrain)
extern void Terrain_SetNeighbors_m8D84FD4852DE0F39C99BF04E6D4363C1869BF59F (void);
// 0x0000000F System.Void UnityEngine.Terrain::Flush()
extern void Terrain_Flush_m72233FA4768E704FA506A7F6B3819166C3F06C01 (void);
// 0x00000010 UnityEngine.Terrain[] UnityEngine.Terrain::get_activeTerrains()
extern void Terrain_get_activeTerrains_m4F358455EB7630E59F2AB221B142A11B750D23F9 (void);
// 0x00000011 UnityEngine.GameObject UnityEngine.Terrain::CreateTerrainGameObject(UnityEngine.TerrainData)
extern void Terrain_CreateTerrainGameObject_m312EF8811814D0378FEB322E94E68160A19378B6 (void);
// 0x00000012 System.Void UnityEngine.Terrain::set_materialType(UnityEngine.Terrain_MaterialType)
extern void Terrain_set_materialType_mD600EA20A7CADC59514B9247F517C570F82ADE84 (void);
// 0x00000013 System.Void UnityEngine.Terrain::.ctor()
extern void Terrain__ctor_m3E411CBA0F2F20E56475F1755B7AEDF0C9F57464 (void);
// 0x00000014 System.Single UnityEngine.Terrain::SampleHeight_Injected(UnityEngine.Vector3&)
extern void Terrain_SampleHeight_Injected_mF47269DC09193BA0AED54E7DC824DA8FE90728EE (void);
// 0x00000015 System.Void UnityEngine.Terrain::AddTreeInstance_Injected(UnityEngine.TreeInstance&)
extern void Terrain_AddTreeInstance_Injected_m5EA280A7922EEA57F37D51231BC1FC7AA3F90117 (void);
// 0x00000016 UnityEngine.GameObject UnityEngine.TreePrototype::get_prefab()
extern void TreePrototype_get_prefab_mC0D926D4AFCF488765F39348C08C93BEA94A7C99 (void);
// 0x00000017 System.Void UnityEngine.TreePrototype::set_prefab(UnityEngine.GameObject)
extern void TreePrototype_set_prefab_m281534A67041F69C643794C1B13B108610B8BA04 (void);
// 0x00000018 System.Single UnityEngine.TreePrototype::get_bendFactor()
extern void TreePrototype_get_bendFactor_m96DC8FC78BEE532247C2602ACF0F65DCA1A28C49 (void);
// 0x00000019 System.Void UnityEngine.TreePrototype::set_bendFactor(System.Single)
extern void TreePrototype_set_bendFactor_m7BC703D02DAEF8FB54D372E295F89BB24AE26750 (void);
// 0x0000001A System.Void UnityEngine.TreePrototype::.ctor()
extern void TreePrototype__ctor_mAF604B1B4D176E6072DFB258CAF2EC53E7714E4E (void);
// 0x0000001B System.Boolean UnityEngine.TreePrototype::Equals(System.Object)
extern void TreePrototype_Equals_m641A0426B09CA89751B8801F9302A671FBD43D93 (void);
// 0x0000001C System.Int32 UnityEngine.TreePrototype::GetHashCode()
extern void TreePrototype_GetHashCode_m03EAC2049D7D48966C2C5E49D24F93904D49BB2B (void);
// 0x0000001D System.Boolean UnityEngine.TreePrototype::Equals(UnityEngine.TreePrototype)
extern void TreePrototype_Equals_mE4DD65E5D108005542E35C847BF4A24C0F01C08B (void);
// 0x0000001E System.Int32 UnityEngine.TerrainData::GetBoundaryValue(UnityEngine.TerrainData_BoundaryValueType)
extern void TerrainData_GetBoundaryValue_mDDB33647E2918B15F5499701A647695B8EF9763C (void);
// 0x0000001F System.Void UnityEngine.TerrainData::.ctor()
extern void TerrainData__ctor_m09DE788EE93388ACD3E80CB586FC2ED551B66ED7 (void);
// 0x00000020 System.Void UnityEngine.TerrainData::Internal_Create(UnityEngine.TerrainData)
extern void TerrainData_Internal_Create_mA483D4EF29C637A9855A8825AB257DC97374A424 (void);
// 0x00000021 System.Int32 UnityEngine.TerrainData::get_heightmapResolution()
extern void TerrainData_get_heightmapResolution_mF665E4416056AEA15E008E37A814D02BED01FCF4 (void);
// 0x00000022 System.Void UnityEngine.TerrainData::set_heightmapResolution(System.Int32)
extern void TerrainData_set_heightmapResolution_m85A19331108A0A25E28BB7D65533DA2B746BD4F3 (void);
// 0x00000023 System.Int32 UnityEngine.TerrainData::get_internalHeightmapResolution()
extern void TerrainData_get_internalHeightmapResolution_mFD5798B355C78B724466325E9629009B259BD591 (void);
// 0x00000024 System.Void UnityEngine.TerrainData::set_internalHeightmapResolution(System.Int32)
extern void TerrainData_set_internalHeightmapResolution_m7561EC472E55FC852DDC096A212259A216DA0153 (void);
// 0x00000025 UnityEngine.Vector3 UnityEngine.TerrainData::get_heightmapScale()
extern void TerrainData_get_heightmapScale_m6C0C0B91AB2C26F3F97535BE5112A80AA3078A17 (void);
// 0x00000026 UnityEngine.Vector3 UnityEngine.TerrainData::get_size()
extern void TerrainData_get_size_mF68B76A49498AE26C506D77483EA81E4F816EB15 (void);
// 0x00000027 System.Void UnityEngine.TerrainData::set_size(UnityEngine.Vector3)
extern void TerrainData_set_size_m67596DE2C49F97BA157235CF4BB46E56B36C31D1 (void);
// 0x00000028 System.Void UnityEngine.TerrainData::SetHeights(System.Int32,System.Int32,System.Single[0...,0...])
extern void TerrainData_SetHeights_mB008164720CC8519F56BDA5B860FA358ED95B7AD (void);
// 0x00000029 System.Void UnityEngine.TerrainData::Internal_SetHeights(System.Int32,System.Int32,System.Int32,System.Int32,System.Single[0...,0...])
extern void TerrainData_Internal_SetHeights_mFF98295A741F7C3D834398F71FDCDC717A83050D (void);
// 0x0000002A System.Single UnityEngine.TerrainData::GetSteepness(System.Single,System.Single)
extern void TerrainData_GetSteepness_m593B1A252175BE0EED366A3C954B3F2C900F6734 (void);
// 0x0000002B System.Void UnityEngine.TerrainData::SetDetailResolution(System.Int32,System.Int32)
extern void TerrainData_SetDetailResolution_m5A6223B4EB73B2F63147DC0201EF7672CE2C3F45 (void);
// 0x0000002C System.Void UnityEngine.TerrainData::Internal_SetDetailResolution(System.Int32,System.Int32)
extern void TerrainData_Internal_SetDetailResolution_m4362CF3490B7341626C5943178F99D7020745899 (void);
// 0x0000002D System.Void UnityEngine.TerrainData::RefreshPrototypes()
extern void TerrainData_RefreshPrototypes_m30A5130FB2B87236FA4A94E56A3CDED84F323BDD (void);
// 0x0000002E System.Void UnityEngine.TerrainData::set_treeInstances(UnityEngine.TreeInstance[])
extern void TerrainData_set_treeInstances_m652DAE4CD2030623AE1F20A6FE16277A094798A0 (void);
// 0x0000002F System.Void UnityEngine.TerrainData::SetTreeInstances(UnityEngine.TreeInstance[],System.Boolean)
extern void TerrainData_SetTreeInstances_mD4EA1F1C6E9C324233C0148EE6816207700F0790 (void);
// 0x00000030 System.Int32 UnityEngine.TerrainData::get_treeInstanceCount()
extern void TerrainData_get_treeInstanceCount_m90094E61D9F293FEB486FB6FE91CBBC8A8807ECE (void);
// 0x00000031 UnityEngine.TreePrototype[] UnityEngine.TerrainData::get_treePrototypes()
extern void TerrainData_get_treePrototypes_mE083138D9260AD480EBB794632142BC68D4533C7 (void);
// 0x00000032 System.Void UnityEngine.TerrainData::set_treePrototypes(UnityEngine.TreePrototype[])
extern void TerrainData_set_treePrototypes_m57B2C160BD389266B666A5C38B0D268650AB753A (void);
// 0x00000033 System.Void UnityEngine.TerrainData::set_alphamapResolution(System.Int32)
extern void TerrainData_set_alphamapResolution_m403BBDCDFC18EABFB9E2CB57E665676F292EF025 (void);
// 0x00000034 System.Single UnityEngine.TerrainData::GetAlphamapResolutionInternal()
extern void TerrainData_GetAlphamapResolutionInternal_mB3D8631E512C887B38CE96496428B803C3837CCB (void);
// 0x00000035 System.Void UnityEngine.TerrainData::set_Internal_alphamapResolution(System.Int32)
extern void TerrainData_set_Internal_alphamapResolution_m64E1CA5EA113A82B3CAF0914F7401A26B158964C (void);
// 0x00000036 System.Void UnityEngine.TerrainData::set_baseMapResolution(System.Int32)
extern void TerrainData_set_baseMapResolution_mA4B72736B50CBED982C93BB0834D7C1C88ED5187 (void);
// 0x00000037 System.Void UnityEngine.TerrainData::set_Internal_baseMapResolution(System.Int32)
extern void TerrainData_set_Internal_baseMapResolution_mF93DA1488D7A3987F0E9DA376AE4F597271B1DD7 (void);
// 0x00000038 UnityEngine.Terrain[] UnityEngine.TerrainData::get_users()
extern void TerrainData_get_users_m4BBC80BD0296525664EB84FE7DD6F1ABAE1CAF0F (void);
// 0x00000039 System.Void UnityEngine.TerrainData::.cctor()
extern void TerrainData__cctor_m64E6CF88BD21FC182D29D169EBCA04D965C46517 (void);
// 0x0000003A System.Void UnityEngine.TerrainData::get_heightmapScale_Injected(UnityEngine.Vector3&)
extern void TerrainData_get_heightmapScale_Injected_m6DB12B98BED01EAAA4583F02C24A8F0F631C54E7 (void);
// 0x0000003B System.Void UnityEngine.TerrainData::get_size_Injected(UnityEngine.Vector3&)
extern void TerrainData_get_size_Injected_m181495692C7B755ACD1D7F7F115A2CE8DC6A9E64 (void);
// 0x0000003C System.Void UnityEngine.TerrainData::set_size_Injected(UnityEngine.Vector3&)
extern void TerrainData_set_size_Injected_mBE2047942BDF4CAD1031986F2621AD2479931502 (void);
// 0x0000003D System.Void UnityEngine.Experimental.TerrainAPI.TerrainCallbacks::InvokeHeightmapChangedCallback(UnityEngine.TerrainData,UnityEngine.RectInt,System.Boolean)
extern void TerrainCallbacks_InvokeHeightmapChangedCallback_m394735D1416B00373916335213992D011D5FDA86 (void);
// 0x0000003E System.Void UnityEngine.Experimental.TerrainAPI.TerrainCallbacks::InvokeTextureChangedCallback(UnityEngine.TerrainData,System.String,UnityEngine.RectInt,System.Boolean)
extern void TerrainCallbacks_InvokeTextureChangedCallback_m10A2EFE8E490EC932777717717CC61709FCA3307 (void);
// 0x0000003F System.Void UnityEngine.Experimental.TerrainAPI.TerrainCallbacks_HeightmapChangedCallback::.ctor(System.Object,System.IntPtr)
extern void HeightmapChangedCallback__ctor_mB63473491843FCAFE4EC51977A276DF20F11B1D0 (void);
// 0x00000040 System.Void UnityEngine.Experimental.TerrainAPI.TerrainCallbacks_HeightmapChangedCallback::Invoke(UnityEngine.Terrain,UnityEngine.RectInt,System.Boolean)
extern void HeightmapChangedCallback_Invoke_m24BDB8F85D5AC1B4B183E8C698905E3281CB4489 (void);
// 0x00000041 System.IAsyncResult UnityEngine.Experimental.TerrainAPI.TerrainCallbacks_HeightmapChangedCallback::BeginInvoke(UnityEngine.Terrain,UnityEngine.RectInt,System.Boolean,System.AsyncCallback,System.Object)
extern void HeightmapChangedCallback_BeginInvoke_m590DAC8F14CB6AC982D6FE89C27ACF10CFA17E05 (void);
// 0x00000042 System.Void UnityEngine.Experimental.TerrainAPI.TerrainCallbacks_HeightmapChangedCallback::EndInvoke(System.IAsyncResult)
extern void HeightmapChangedCallback_EndInvoke_m015EB6B241A8FC17A0870FC57A1048520DCEB1E0 (void);
// 0x00000043 System.Void UnityEngine.Experimental.TerrainAPI.TerrainCallbacks_TextureChangedCallback::.ctor(System.Object,System.IntPtr)
extern void TextureChangedCallback__ctor_m7086172D805BDFEFEF9901EAC1C78904DBB63D29 (void);
// 0x00000044 System.Void UnityEngine.Experimental.TerrainAPI.TerrainCallbacks_TextureChangedCallback::Invoke(UnityEngine.Terrain,System.String,UnityEngine.RectInt,System.Boolean)
extern void TextureChangedCallback_Invoke_mC92D41CF0240EA1783C1A1816696EA19895F5569 (void);
// 0x00000045 System.IAsyncResult UnityEngine.Experimental.TerrainAPI.TerrainCallbacks_TextureChangedCallback::BeginInvoke(UnityEngine.Terrain,System.String,UnityEngine.RectInt,System.Boolean,System.AsyncCallback,System.Object)
extern void TextureChangedCallback_BeginInvoke_mE19FD540CF24CED1C990B54DE4A84A270C5BA37C (void);
// 0x00000046 System.Void UnityEngine.Experimental.TerrainAPI.TerrainCallbacks_TextureChangedCallback::EndInvoke(System.IAsyncResult)
extern void TextureChangedCallback_EndInvoke_mE86E8C09F0F8EB087F90979A86270204AB551B84 (void);
// 0x00000047 System.Boolean UnityEngine.Experimental.TerrainAPI.TerrainUtility::HasValidTerrains()
extern void TerrainUtility_HasValidTerrains_mA6E2D0BE718C6B58CD4C1400C910CBF73AF3172D (void);
// 0x00000048 System.Void UnityEngine.Experimental.TerrainAPI.TerrainUtility::ClearConnectivity()
extern void TerrainUtility_ClearConnectivity_mC50EAA8DA06ED94944F6168505271B127389EC5A (void);
// 0x00000049 UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainGroups UnityEngine.Experimental.TerrainAPI.TerrainUtility::CollectTerrains(System.Boolean)
extern void TerrainUtility_CollectTerrains_m4630246A7274A15FB2AE8C13E653E8B73C129F9B (void);
// 0x0000004A System.Void UnityEngine.Experimental.TerrainAPI.TerrainUtility::AutoConnect()
extern void TerrainUtility_AutoConnect_m8526A29E63B328915E516505E3195637A1F100EF (void);
// 0x0000004B UnityEngine.Terrain UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap::GetTerrain(System.Int32,System.Int32)
extern void TerrainMap_GetTerrain_mF027E4E4677131A19CA44E9A22CCB89101145006 (void);
// 0x0000004C UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap::CreateFromPlacement(UnityEngine.Terrain,UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap_TerrainFilter,System.Boolean)
extern void TerrainMap_CreateFromPlacement_mBF5B980BA13C9390739DFEA1644596CA54D44337 (void);
// 0x0000004D UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap::CreateFromPlacement(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap_TerrainFilter,System.Boolean)
extern void TerrainMap_CreateFromPlacement_m8BCE09C1C736432F61D78CED8868DC43F9CCD25D (void);
// 0x0000004E System.Void UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap::.ctor()
extern void TerrainMap__ctor_m0A16A2E6ED5C4EFB2F87D72A5665EF7C4E62F761 (void);
// 0x0000004F System.Void UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap::AddTerrainInternal(System.Int32,System.Int32,UnityEngine.Terrain)
extern void TerrainMap_AddTerrainInternal_m82F62E3018D1D2A6E48FB7361DB6531F0E9BEB79 (void);
// 0x00000050 System.Boolean UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap::TryToAddTerrain(System.Int32,System.Int32,UnityEngine.Terrain)
extern void TerrainMap_TryToAddTerrain_m49A7085766F102EADE7E4A29259232F399735C61 (void);
// 0x00000051 System.Void UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap::ValidateTerrain(System.Int32,System.Int32)
extern void TerrainMap_ValidateTerrain_mFE264FDE78C3D68285943250BC9FABAC89D85764 (void);
// 0x00000052 UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap_ErrorCode UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap::Validate()
extern void TerrainMap_Validate_m9CD6FAF70E4F90C896BF25F083BC0A7F21C8FA56 (void);
// 0x00000053 System.Void UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap_TerrainFilter::.ctor(System.Object,System.IntPtr)
extern void TerrainFilter__ctor_m6A1F2AE7CF7A3B502AFBCB351B615EBBE942B838 (void);
// 0x00000054 System.Boolean UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap_TerrainFilter::Invoke(UnityEngine.Terrain)
extern void TerrainFilter_Invoke_m48E69E662BC21917E57559702D1F9D94E4F762F7 (void);
// 0x00000055 System.IAsyncResult UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap_TerrainFilter::BeginInvoke(UnityEngine.Terrain,System.AsyncCallback,System.Object)
extern void TerrainFilter_BeginInvoke_m4C17FEFE5AE0498C9A88E63A7ABEA673CD31C949 (void);
// 0x00000056 System.Boolean UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap_TerrainFilter::EndInvoke(System.IAsyncResult)
extern void TerrainFilter_EndInvoke_m483F3A9363FE8FDF3B5022AE2C284ACE661857B0 (void);
// 0x00000057 System.Void UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap_TileCoord::.ctor(System.Int32,System.Int32)
extern void TileCoord__ctor_m9EED41FD3E08320CDA102E34DC65236E5137F155_AdjustorThunk (void);
// 0x00000058 System.Void UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_mF6CE52C3D202B71510907E3EDCA198C369468888 (void);
// 0x00000059 System.Boolean UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainMap_<>c__DisplayClass4_0::<CreateFromPlacement>b__0(UnityEngine.Terrain)
extern void U3CU3Ec__DisplayClass4_0_U3CCreateFromPlacementU3Eb__0_mA0E2295171D220FA7ABA12660D2CB357BC721653 (void);
// 0x0000005A System.Void UnityEngine.Experimental.TerrainAPI.TerrainUtility_TerrainGroups::.ctor()
extern void TerrainGroups__ctor_mA9F11D4BE52D80563D0D31788BA80C8F5381FFB1 (void);
// 0x0000005B System.Void UnityEngine.Experimental.TerrainAPI.TerrainUtility_<>c__DisplayClass4_0::.ctor()
extern void U3CU3Ec__DisplayClass4_0__ctor_m857F329AF653D7F052DCF0BE6511BFE40CD13653 (void);
// 0x0000005C System.Void UnityEngine.Experimental.TerrainAPI.TerrainUtility_<>c__DisplayClass4_1::.ctor()
extern void U3CU3Ec__DisplayClass4_1__ctor_m2B5F521527B39BE091B856058F67DC7E3DE4B345 (void);
// 0x0000005D System.Boolean UnityEngine.Experimental.TerrainAPI.TerrainUtility_<>c__DisplayClass4_1::<CollectTerrains>b__0(UnityEngine.Terrain)
extern void U3CU3Ec__DisplayClass4_1_U3CCollectTerrainsU3Eb__0_m539C07F9B8F371A9E9C09A8AFD003DD4163C7810 (void);
static Il2CppMethodPointer s_methodPointers[93] = 
{
	Terrain_get_terrainData_mDB60C324B3424339C3C9FA6CDF6DC1C9B47D6E41,
	Terrain_set_terrainData_mEDD9205514CD21D3518EC11C03EBBF1BF8B9E81B,
	Terrain_set_treeDistance_m1B8BFC8FC1B85FB4CBD0C9D45E52EB4702DED208,
	Terrain_set_treeBillboardDistance_m60BBE7FBD4B66661E02C4848EA912390DCA5F0D8,
	Terrain_set_treeCrossFadeLength_m89D60280B3A24A1A73BE1734469E5E1401A0040C,
	Terrain_set_treeMaximumFullLODCount_m2B151A511B6136505C7068350AC559B009B52995,
	Terrain_set_heightmapPixelError_mEE15C5552F735184366148B729A2330EE9D9BEAB,
	Terrain_set_basemapDistance_m989F9BF5A5DF73030CA226C0640D0B4E95CCE33B,
	Terrain_set_materialTemplate_m3626DE42EAE3A425DB95E6C2D33BB0A4F44DD1A9,
	Terrain_get_allowAutoConnect_mC1B0AC480E9AA5E33EDF412E8F9AA3EB4832BA67,
	Terrain_get_groupingID_m8390315914A192A424C890605D780E638F5E1CC9,
	Terrain_SampleHeight_mBA0B969DA2750257CE1C35CCC18E4AA6EB98DA5B,
	Terrain_AddTreeInstance_mBFB4F85C8F017DB981FCF44FFF4C6A5D7FFCF17E,
	Terrain_SetNeighbors_m8D84FD4852DE0F39C99BF04E6D4363C1869BF59F,
	Terrain_Flush_m72233FA4768E704FA506A7F6B3819166C3F06C01,
	Terrain_get_activeTerrains_m4F358455EB7630E59F2AB221B142A11B750D23F9,
	Terrain_CreateTerrainGameObject_m312EF8811814D0378FEB322E94E68160A19378B6,
	Terrain_set_materialType_mD600EA20A7CADC59514B9247F517C570F82ADE84,
	Terrain__ctor_m3E411CBA0F2F20E56475F1755B7AEDF0C9F57464,
	Terrain_SampleHeight_Injected_mF47269DC09193BA0AED54E7DC824DA8FE90728EE,
	Terrain_AddTreeInstance_Injected_m5EA280A7922EEA57F37D51231BC1FC7AA3F90117,
	TreePrototype_get_prefab_mC0D926D4AFCF488765F39348C08C93BEA94A7C99,
	TreePrototype_set_prefab_m281534A67041F69C643794C1B13B108610B8BA04,
	TreePrototype_get_bendFactor_m96DC8FC78BEE532247C2602ACF0F65DCA1A28C49,
	TreePrototype_set_bendFactor_m7BC703D02DAEF8FB54D372E295F89BB24AE26750,
	TreePrototype__ctor_mAF604B1B4D176E6072DFB258CAF2EC53E7714E4E,
	TreePrototype_Equals_m641A0426B09CA89751B8801F9302A671FBD43D93,
	TreePrototype_GetHashCode_m03EAC2049D7D48966C2C5E49D24F93904D49BB2B,
	TreePrototype_Equals_mE4DD65E5D108005542E35C847BF4A24C0F01C08B,
	TerrainData_GetBoundaryValue_mDDB33647E2918B15F5499701A647695B8EF9763C,
	TerrainData__ctor_m09DE788EE93388ACD3E80CB586FC2ED551B66ED7,
	TerrainData_Internal_Create_mA483D4EF29C637A9855A8825AB257DC97374A424,
	TerrainData_get_heightmapResolution_mF665E4416056AEA15E008E37A814D02BED01FCF4,
	TerrainData_set_heightmapResolution_m85A19331108A0A25E28BB7D65533DA2B746BD4F3,
	TerrainData_get_internalHeightmapResolution_mFD5798B355C78B724466325E9629009B259BD591,
	TerrainData_set_internalHeightmapResolution_m7561EC472E55FC852DDC096A212259A216DA0153,
	TerrainData_get_heightmapScale_m6C0C0B91AB2C26F3F97535BE5112A80AA3078A17,
	TerrainData_get_size_mF68B76A49498AE26C506D77483EA81E4F816EB15,
	TerrainData_set_size_m67596DE2C49F97BA157235CF4BB46E56B36C31D1,
	TerrainData_SetHeights_mB008164720CC8519F56BDA5B860FA358ED95B7AD,
	TerrainData_Internal_SetHeights_mFF98295A741F7C3D834398F71FDCDC717A83050D,
	TerrainData_GetSteepness_m593B1A252175BE0EED366A3C954B3F2C900F6734,
	TerrainData_SetDetailResolution_m5A6223B4EB73B2F63147DC0201EF7672CE2C3F45,
	TerrainData_Internal_SetDetailResolution_m4362CF3490B7341626C5943178F99D7020745899,
	TerrainData_RefreshPrototypes_m30A5130FB2B87236FA4A94E56A3CDED84F323BDD,
	TerrainData_set_treeInstances_m652DAE4CD2030623AE1F20A6FE16277A094798A0,
	TerrainData_SetTreeInstances_mD4EA1F1C6E9C324233C0148EE6816207700F0790,
	TerrainData_get_treeInstanceCount_m90094E61D9F293FEB486FB6FE91CBBC8A8807ECE,
	TerrainData_get_treePrototypes_mE083138D9260AD480EBB794632142BC68D4533C7,
	TerrainData_set_treePrototypes_m57B2C160BD389266B666A5C38B0D268650AB753A,
	TerrainData_set_alphamapResolution_m403BBDCDFC18EABFB9E2CB57E665676F292EF025,
	TerrainData_GetAlphamapResolutionInternal_mB3D8631E512C887B38CE96496428B803C3837CCB,
	TerrainData_set_Internal_alphamapResolution_m64E1CA5EA113A82B3CAF0914F7401A26B158964C,
	TerrainData_set_baseMapResolution_mA4B72736B50CBED982C93BB0834D7C1C88ED5187,
	TerrainData_set_Internal_baseMapResolution_mF93DA1488D7A3987F0E9DA376AE4F597271B1DD7,
	TerrainData_get_users_m4BBC80BD0296525664EB84FE7DD6F1ABAE1CAF0F,
	TerrainData__cctor_m64E6CF88BD21FC182D29D169EBCA04D965C46517,
	TerrainData_get_heightmapScale_Injected_m6DB12B98BED01EAAA4583F02C24A8F0F631C54E7,
	TerrainData_get_size_Injected_m181495692C7B755ACD1D7F7F115A2CE8DC6A9E64,
	TerrainData_set_size_Injected_mBE2047942BDF4CAD1031986F2621AD2479931502,
	TerrainCallbacks_InvokeHeightmapChangedCallback_m394735D1416B00373916335213992D011D5FDA86,
	TerrainCallbacks_InvokeTextureChangedCallback_m10A2EFE8E490EC932777717717CC61709FCA3307,
	HeightmapChangedCallback__ctor_mB63473491843FCAFE4EC51977A276DF20F11B1D0,
	HeightmapChangedCallback_Invoke_m24BDB8F85D5AC1B4B183E8C698905E3281CB4489,
	HeightmapChangedCallback_BeginInvoke_m590DAC8F14CB6AC982D6FE89C27ACF10CFA17E05,
	HeightmapChangedCallback_EndInvoke_m015EB6B241A8FC17A0870FC57A1048520DCEB1E0,
	TextureChangedCallback__ctor_m7086172D805BDFEFEF9901EAC1C78904DBB63D29,
	TextureChangedCallback_Invoke_mC92D41CF0240EA1783C1A1816696EA19895F5569,
	TextureChangedCallback_BeginInvoke_mE19FD540CF24CED1C990B54DE4A84A270C5BA37C,
	TextureChangedCallback_EndInvoke_mE86E8C09F0F8EB087F90979A86270204AB551B84,
	TerrainUtility_HasValidTerrains_mA6E2D0BE718C6B58CD4C1400C910CBF73AF3172D,
	TerrainUtility_ClearConnectivity_mC50EAA8DA06ED94944F6168505271B127389EC5A,
	TerrainUtility_CollectTerrains_m4630246A7274A15FB2AE8C13E653E8B73C129F9B,
	TerrainUtility_AutoConnect_m8526A29E63B328915E516505E3195637A1F100EF,
	TerrainMap_GetTerrain_mF027E4E4677131A19CA44E9A22CCB89101145006,
	TerrainMap_CreateFromPlacement_mBF5B980BA13C9390739DFEA1644596CA54D44337,
	TerrainMap_CreateFromPlacement_m8BCE09C1C736432F61D78CED8868DC43F9CCD25D,
	TerrainMap__ctor_m0A16A2E6ED5C4EFB2F87D72A5665EF7C4E62F761,
	TerrainMap_AddTerrainInternal_m82F62E3018D1D2A6E48FB7361DB6531F0E9BEB79,
	TerrainMap_TryToAddTerrain_m49A7085766F102EADE7E4A29259232F399735C61,
	TerrainMap_ValidateTerrain_mFE264FDE78C3D68285943250BC9FABAC89D85764,
	TerrainMap_Validate_m9CD6FAF70E4F90C896BF25F083BC0A7F21C8FA56,
	TerrainFilter__ctor_m6A1F2AE7CF7A3B502AFBCB351B615EBBE942B838,
	TerrainFilter_Invoke_m48E69E662BC21917E57559702D1F9D94E4F762F7,
	TerrainFilter_BeginInvoke_m4C17FEFE5AE0498C9A88E63A7ABEA673CD31C949,
	TerrainFilter_EndInvoke_m483F3A9363FE8FDF3B5022AE2C284ACE661857B0,
	TileCoord__ctor_m9EED41FD3E08320CDA102E34DC65236E5137F155_AdjustorThunk,
	U3CU3Ec__DisplayClass4_0__ctor_mF6CE52C3D202B71510907E3EDCA198C369468888,
	U3CU3Ec__DisplayClass4_0_U3CCreateFromPlacementU3Eb__0_mA0E2295171D220FA7ABA12660D2CB357BC721653,
	TerrainGroups__ctor_mA9F11D4BE52D80563D0D31788BA80C8F5381FFB1,
	U3CU3Ec__DisplayClass4_0__ctor_m857F329AF653D7F052DCF0BE6511BFE40CD13653,
	U3CU3Ec__DisplayClass4_1__ctor_m2B5F521527B39BE091B856058F67DC7E3DE4B345,
	U3CU3Ec__DisplayClass4_1_U3CCollectTerrainsU3Eb__0_m539C07F9B8F371A9E9C09A8AFD003DD4163C7810,
};
static const int32_t s_InvokerIndices[93] = 
{
	14,
	26,
	339,
	339,
	339,
	32,
	339,
	339,
	26,
	89,
	10,
	2047,
	2048,
	450,
	23,
	4,
	0,
	32,
	23,
	2049,
	6,
	14,
	26,
	761,
	339,
	23,
	9,
	10,
	9,
	21,
	23,
	168,
	10,
	32,
	10,
	32,
	1574,
	1574,
	1575,
	626,
	1654,
	2050,
	138,
	138,
	23,
	26,
	102,
	10,
	14,
	26,
	32,
	761,
	32,
	32,
	32,
	14,
	3,
	6,
	6,
	6,
	2051,
	2052,
	133,
	2053,
	2054,
	26,
	133,
	2055,
	2056,
	26,
	49,
	3,
	855,
	3,
	207,
	221,
	2057,
	23,
	626,
	2058,
	138,
	10,
	133,
	9,
	220,
	9,
	138,
	23,
	9,
	23,
	23,
	23,
	9,
};
extern const Il2CppCodeGenModule g_UnityEngine_TerrainModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_TerrainModuleCodeGenModule = 
{
	"UnityEngine.TerrainModule.dll",
	93,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
