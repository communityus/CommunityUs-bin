﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void System.Xml.Linq.XName::.ctor(System.Xml.Linq.XNamespace,System.String)
extern void XName__ctor_m08CA9A842644AFE1545DDFB3EA1BF011D797CE59 (void);
// 0x00000002 System.String System.Xml.Linq.XName::get_LocalName()
extern void XName_get_LocalName_m696497ACA70F6707FAF6DDC7B0FB4297D7F4A007 (void);
// 0x00000003 System.Xml.Linq.XNamespace System.Xml.Linq.XName::get_Namespace()
extern void XName_get_Namespace_m27DDB135C04C90BB031CA5ECC7B3436A31423488 (void);
// 0x00000004 System.String System.Xml.Linq.XName::get_NamespaceName()
extern void XName_get_NamespaceName_m48B792158467825280622E60E7222C565B3BDB20 (void);
// 0x00000005 System.String System.Xml.Linq.XName::ToString()
extern void XName_ToString_m5095F3F0750DD9AF19A7A4817CECD4192B4C0DD7 (void);
// 0x00000006 System.Xml.Linq.XName System.Xml.Linq.XName::Get(System.String)
extern void XName_Get_mFE26FB1696ED2D60324938EBEB37C0363637BAB7 (void);
// 0x00000007 System.Xml.Linq.XName System.Xml.Linq.XName::op_Implicit(System.String)
extern void XName_op_Implicit_mC9F588EE5257A3A259D5CF52E9EBCC36B46C01C6 (void);
// 0x00000008 System.Boolean System.Xml.Linq.XName::Equals(System.Object)
extern void XName_Equals_m688E6032F6F05612771D7E920B5CD1723D7EF24A (void);
// 0x00000009 System.Int32 System.Xml.Linq.XName::GetHashCode()
extern void XName_GetHashCode_m4B3D379685B914C9BA6E29BB4A861853AF582D3A (void);
// 0x0000000A System.Boolean System.Xml.Linq.XName::op_Equality(System.Xml.Linq.XName,System.Xml.Linq.XName)
extern void XName_op_Equality_m303F10BF2291A8B7AD3E3ACB4BB00C0800777B06 (void);
// 0x0000000B System.Boolean System.Xml.Linq.XName::System.IEquatable<System.Xml.Linq.XName>.Equals(System.Xml.Linq.XName)
extern void XName_System_IEquatableU3CSystem_Xml_Linq_XNameU3E_Equals_mCDE24CE750E5DB3C7E3432F457FCFE7C6657FF81 (void);
// 0x0000000C System.Void System.Xml.Linq.XName::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void XName_System_Runtime_Serialization_ISerializable_GetObjectData_m45F244C932C673A2DBB248CDD1A640138A02DAFA (void);
// 0x0000000D System.Void System.Xml.Linq.XName::.ctor()
extern void XName__ctor_m2D7B85963C672FA3E6E5559CC101F790A4757ECE (void);
// 0x0000000E System.Void System.Xml.Linq.NameSerializer::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void NameSerializer__ctor_mFF83F9040F5363B72D87DC6CCEA0321DB215F822 (void);
// 0x0000000F System.Object System.Xml.Linq.NameSerializer::System.Runtime.Serialization.IObjectReference.GetRealObject(System.Runtime.Serialization.StreamingContext)
extern void NameSerializer_System_Runtime_Serialization_IObjectReference_GetRealObject_m67AA4E4AF30FB2A0898BEC4C2E9DB34BEA5AD4A6 (void);
// 0x00000010 System.Void System.Xml.Linq.NameSerializer::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void NameSerializer_System_Runtime_Serialization_ISerializable_GetObjectData_m309AB629E47E1D640D2A44960A9004BC34016B8C (void);
// 0x00000011 System.Void System.Xml.Linq.XNamespace::.ctor(System.String)
extern void XNamespace__ctor_m032BFBA1A9B9D4B33642F96DB64DC4297F331617 (void);
// 0x00000012 System.String System.Xml.Linq.XNamespace::get_NamespaceName()
extern void XNamespace_get_NamespaceName_mD97F3E27C3274AEB1B8AE634BE739F44219992DB (void);
// 0x00000013 System.Xml.Linq.XName System.Xml.Linq.XNamespace::GetName(System.String)
extern void XNamespace_GetName_mB9A7E789565459E55D929AF8F7C84CC38BBC8DFD (void);
// 0x00000014 System.String System.Xml.Linq.XNamespace::ToString()
extern void XNamespace_ToString_m1D5D03F1C2C1374DFCE24FB397394CD76584BC70 (void);
// 0x00000015 System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::get_None()
extern void XNamespace_get_None_m2795996F3AD1908D3CFC3F1AFCD58CFBED61EFE0 (void);
// 0x00000016 System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::get_Xml()
extern void XNamespace_get_Xml_mF816E04DDC8ED562C6B6E71449706594EECCA55E (void);
// 0x00000017 System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::get_Xmlns()
extern void XNamespace_get_Xmlns_m140AE4DD66BD99100AA9492D105D3B1383BC5112 (void);
// 0x00000018 System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::Get(System.String)
extern void XNamespace_Get_m74B196DD9CD7770D401FD672F86CE0FBB8FCE9F6 (void);
// 0x00000019 System.Boolean System.Xml.Linq.XNamespace::Equals(System.Object)
extern void XNamespace_Equals_m8964855B6CBF87B2BC5317DABE0678F6E24683E6 (void);
// 0x0000001A System.Int32 System.Xml.Linq.XNamespace::GetHashCode()
extern void XNamespace_GetHashCode_m90177556974B375FEBD8F55CA6D208924BA366D0 (void);
// 0x0000001B System.Boolean System.Xml.Linq.XNamespace::op_Equality(System.Xml.Linq.XNamespace,System.Xml.Linq.XNamespace)
extern void XNamespace_op_Equality_m59C867B675C5C15015A9F081A6BD2A47370A4DBE (void);
// 0x0000001C System.Boolean System.Xml.Linq.XNamespace::op_Inequality(System.Xml.Linq.XNamespace,System.Xml.Linq.XNamespace)
extern void XNamespace_op_Inequality_m075CAE6D2922DE14AB223F9C4C290E9BC478CD12 (void);
// 0x0000001D System.Xml.Linq.XName System.Xml.Linq.XNamespace::GetName(System.String,System.Int32,System.Int32)
extern void XNamespace_GetName_m59979BF3C0D17127A15B3369152BA0821FB10794 (void);
// 0x0000001E System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::Get(System.String,System.Int32,System.Int32)
extern void XNamespace_Get_mF8EF85F60D3C811461541AC8FD3DCFF9C081BCAA (void);
// 0x0000001F System.String System.Xml.Linq.XNamespace::ExtractLocalName(System.Xml.Linq.XName)
extern void XNamespace_ExtractLocalName_mFFC3D4C8CA969449FAD1F029330DED93180959A9 (void);
// 0x00000020 System.String System.Xml.Linq.XNamespace::ExtractNamespace(System.WeakReference)
extern void XNamespace_ExtractNamespace_mC09B1EB8F42638C5B58F853F81CB9A32B73D415A (void);
// 0x00000021 System.Xml.Linq.XNamespace System.Xml.Linq.XNamespace::EnsureNamespace(System.WeakReference&,System.String)
extern void XNamespace_EnsureNamespace_m0F4936CB583036C26C69C3DBBE0A37D4D3635F18 (void);
// 0x00000022 System.Void System.Xml.Linq.XHashtable`1::.ctor(System.Xml.Linq.XHashtable`1_ExtractKeyDelegate<TValue>,System.Int32)
// 0x00000023 System.Boolean System.Xml.Linq.XHashtable`1::TryGetValue(System.String,System.Int32,System.Int32,TValue&)
// 0x00000024 TValue System.Xml.Linq.XHashtable`1::Add(TValue)
// 0x00000025 System.Void System.Xml.Linq.XHashtable`1_ExtractKeyDelegate::.ctor(System.Object,System.IntPtr)
// 0x00000026 System.String System.Xml.Linq.XHashtable`1_ExtractKeyDelegate::Invoke(TValue)
// 0x00000027 System.IAsyncResult System.Xml.Linq.XHashtable`1_ExtractKeyDelegate::BeginInvoke(TValue,System.AsyncCallback,System.Object)
// 0x00000028 System.String System.Xml.Linq.XHashtable`1_ExtractKeyDelegate::EndInvoke(System.IAsyncResult)
// 0x00000029 System.Void System.Xml.Linq.XHashtable`1_XHashtableState::.ctor(System.Xml.Linq.XHashtable`1_ExtractKeyDelegate<TValue>,System.Int32)
// 0x0000002A System.Xml.Linq.XHashtable`1_XHashtableState<TValue> System.Xml.Linq.XHashtable`1_XHashtableState::Resize()
// 0x0000002B System.Boolean System.Xml.Linq.XHashtable`1_XHashtableState::TryGetValue(System.String,System.Int32,System.Int32,TValue&)
// 0x0000002C System.Boolean System.Xml.Linq.XHashtable`1_XHashtableState::TryAdd(TValue,TValue&)
// 0x0000002D System.Boolean System.Xml.Linq.XHashtable`1_XHashtableState::FindEntry(System.Int32,System.String,System.Int32,System.Int32,System.Int32&)
// 0x0000002E System.Int32 System.Xml.Linq.XHashtable`1_XHashtableState::ComputeHashCode(System.String,System.Int32,System.Int32)
// 0x0000002F System.Void System.Xml.Linq.XObject::.ctor()
extern void XObject__ctor_mEBF29FD8B8171A6AE6D42BA886E7BFAE31BE563D (void);
// 0x00000030 System.String System.Xml.Linq.XObject::get_BaseUri()
extern void XObject_get_BaseUri_m130E3178739EEEB478D7826AF5341937EBCA4E97 (void);
// 0x00000031 System.Xml.XmlNodeType System.Xml.Linq.XObject::get_NodeType()
// 0x00000032 System.Void System.Xml.Linq.XObject::AddAnnotation(System.Object)
extern void XObject_AddAnnotation_mCD582B99945D6A963D6C2FB0CFB3CBBEAFE37589 (void);
// 0x00000033 System.Object System.Xml.Linq.XObject::Annotation(System.Type)
extern void XObject_Annotation_m95490F1AA8945BB8554BC5564CC9A8D9C174AA67 (void);
// 0x00000034 T System.Xml.Linq.XObject::Annotation()
// 0x00000035 System.Boolean System.Xml.Linq.XObject::System.Xml.IXmlLineInfo.HasLineInfo()
extern void XObject_System_Xml_IXmlLineInfo_HasLineInfo_m5A8D102E176F2C0D75E8E8F647960A159F343F07 (void);
// 0x00000036 System.Int32 System.Xml.Linq.XObject::System.Xml.IXmlLineInfo.get_LineNumber()
extern void XObject_System_Xml_IXmlLineInfo_get_LineNumber_m24AD33EE2AB7E73E8E8A734B384AF246647CE4A4 (void);
// 0x00000037 System.Int32 System.Xml.Linq.XObject::System.Xml.IXmlLineInfo.get_LinePosition()
extern void XObject_System_Xml_IXmlLineInfo_get_LinePosition_m2B081F9CCF96364A83B0B34E4AEAAFCC20CDD184 (void);
// 0x00000038 System.Boolean System.Xml.Linq.XObject::get_HasBaseUri()
extern void XObject_get_HasBaseUri_m495D449FCEEAB86FEFD3119310D1D1C7F7271C5A (void);
// 0x00000039 System.Void System.Xml.Linq.XObject::SetBaseUri(System.String)
extern void XObject_SetBaseUri_m166BE057D57AACC044AB91ADE70D36408FD4ED27 (void);
// 0x0000003A System.Void System.Xml.Linq.XObject::SetLineInfo(System.Int32,System.Int32)
extern void XObject_SetLineInfo_m5A5F3FB9CE20DA7EC016C2263178A0CBBFD9AB95 (void);
// 0x0000003B System.Xml.Linq.SaveOptions System.Xml.Linq.XObject::GetSaveOptionsFromAnnotations()
extern void XObject_GetSaveOptionsFromAnnotations_m3DBE274F86C30B72962F8D9D76ABEA8E2E773D32 (void);
// 0x0000003C System.Void System.Xml.Linq.BaseUriAnnotation::.ctor(System.String)
extern void BaseUriAnnotation__ctor_m73734683B4E9AABD14F9F4FC06456033579CA178 (void);
// 0x0000003D System.Void System.Xml.Linq.LineInfoAnnotation::.ctor(System.Int32,System.Int32)
extern void LineInfoAnnotation__ctor_m8E8D7A480E7C181E9B65DC98003BC166AEF5AB38 (void);
// 0x0000003E System.Void System.Xml.Linq.LineInfoEndElementAnnotation::.ctor(System.Int32,System.Int32)
extern void LineInfoEndElementAnnotation__ctor_mC681F34922255C61324B88EF7174034FB58C3887 (void);
// 0x0000003F System.Void System.Xml.Linq.XNode::.ctor()
extern void XNode__ctor_mE21FF9BECBD8850494DDA2C81F01BA8FAF799D8A (void);
// 0x00000040 System.String System.Xml.Linq.XNode::ToString()
extern void XNode_ToString_m1BEC71637268EF8CCE9EA74B6F30859A4A3597CB (void);
// 0x00000041 System.Void System.Xml.Linq.XNode::WriteTo(System.Xml.XmlWriter)
// 0x00000042 System.Void System.Xml.Linq.XNode::AppendText(System.Text.StringBuilder)
extern void XNode_AppendText_mE2146B20CA977C9A065DF58A4FD15138D5B9BE8C (void);
// 0x00000043 System.Xml.Linq.XNode System.Xml.Linq.XNode::CloneNode()
// 0x00000044 System.Xml.XmlReaderSettings System.Xml.Linq.XNode::GetXmlReaderSettings(System.Xml.Linq.LoadOptions)
extern void XNode_GetXmlReaderSettings_mC92954C145134317C8FF8B82C9FF25B44F10E633 (void);
// 0x00000045 System.String System.Xml.Linq.XNode::GetXmlString(System.Xml.Linq.SaveOptions)
extern void XNode_GetXmlString_m430A1536E96B6502637DC174F04A9D8F330176E0 (void);
// 0x00000046 System.Void System.Xml.Linq.XText::.ctor(System.String)
extern void XText__ctor_m68D7A7EBA4977B7FC8570D10E60B34EDC4D8CA5B (void);
// 0x00000047 System.Void System.Xml.Linq.XText::.ctor(System.Xml.Linq.XText)
extern void XText__ctor_mC0BAC0C89D83D1AC431C1E0CC146752EAFDA29AF (void);
// 0x00000048 System.Xml.XmlNodeType System.Xml.Linq.XText::get_NodeType()
extern void XText_get_NodeType_mF1B647A9AF434D45DA76307855DCAE7E1E140557 (void);
// 0x00000049 System.String System.Xml.Linq.XText::get_Value()
extern void XText_get_Value_m9597869DABAA9D2BF8275CEDB4A6A85D46FF6BE5 (void);
// 0x0000004A System.Void System.Xml.Linq.XText::WriteTo(System.Xml.XmlWriter)
extern void XText_WriteTo_m45EE125458AB37E57B311DFDD80CCF755505BE7D (void);
// 0x0000004B System.Void System.Xml.Linq.XText::AppendText(System.Text.StringBuilder)
extern void XText_AppendText_m9CB12B0D10400CFBB5C612E05E3468697646183C (void);
// 0x0000004C System.Xml.Linq.XNode System.Xml.Linq.XText::CloneNode()
extern void XText_CloneNode_mEA4C49F38831E5EF3DF3D002EE22EF71EBF28029 (void);
// 0x0000004D System.Void System.Xml.Linq.XCData::.ctor(System.String)
extern void XCData__ctor_m9FA9F20CF152A7C7EEC9320F2FCD3F2F036C8F2E (void);
// 0x0000004E System.Void System.Xml.Linq.XCData::.ctor(System.Xml.Linq.XCData)
extern void XCData__ctor_m7FC6A1CFB551D65D4411EBFE077A4936037E3FFF (void);
// 0x0000004F System.Xml.XmlNodeType System.Xml.Linq.XCData::get_NodeType()
extern void XCData_get_NodeType_mF9CD12D94265A3B87F19209B6D32F4BE84DF8B48 (void);
// 0x00000050 System.Void System.Xml.Linq.XCData::WriteTo(System.Xml.XmlWriter)
extern void XCData_WriteTo_mE9BC80649A08BC72B78DF18F9C1FF7619BDFF5D2 (void);
// 0x00000051 System.Xml.Linq.XNode System.Xml.Linq.XCData::CloneNode()
extern void XCData_CloneNode_mD8FC76E124B8D62AF72D1086D57E17CC99C6C0C8 (void);
// 0x00000052 System.Void System.Xml.Linq.XContainer::.ctor()
extern void XContainer__ctor_m3C7E61824CB53C8FA18BC4226C6C0BA4BAE3B646 (void);
// 0x00000053 System.Void System.Xml.Linq.XContainer::.ctor(System.Xml.Linq.XContainer)
extern void XContainer__ctor_mA6A69DAB1077038C297FBE6B4697DD40AB426ED2 (void);
// 0x00000054 System.Xml.Linq.XElement System.Xml.Linq.XContainer::Element(System.Xml.Linq.XName)
extern void XContainer_Element_mDD805465CD32F3FD06807CA9E34BBD976A554BB5 (void);
// 0x00000055 System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer::Elements()
extern void XContainer_Elements_mE3525ED34CA3769624CB81CF4501C662FE824DB3 (void);
// 0x00000056 System.Void System.Xml.Linq.XContainer::AddNodeSkipNotify(System.Xml.Linq.XNode)
extern void XContainer_AddNodeSkipNotify_m3F02B171C8968D655B0DD2B7C544CB4BB66A16B5 (void);
// 0x00000057 System.Void System.Xml.Linq.XContainer::AddStringSkipNotify(System.String)
extern void XContainer_AddStringSkipNotify_m549907826B0971C84089F41A99115AB8791A3D2E (void);
// 0x00000058 System.Void System.Xml.Linq.XContainer::AppendNodeSkipNotify(System.Xml.Linq.XNode)
extern void XContainer_AppendNodeSkipNotify_m7E11E884F1504058BAB299925A1BD9092A7E0CF3 (void);
// 0x00000059 System.Void System.Xml.Linq.XContainer::AppendText(System.Text.StringBuilder)
extern void XContainer_AppendText_m551C5A592CE1FCD30897888F8E794F40459E8922 (void);
// 0x0000005A System.Void System.Xml.Linq.XContainer::ConvertTextToNode()
extern void XContainer_ConvertTextToNode_m814277C7AD70A979F8FC62CF38682F02409A7160 (void);
// 0x0000005B System.String System.Xml.Linq.XContainer::GetDateTimeString(System.DateTime)
extern void XContainer_GetDateTimeString_mD58B479E6A678F25B000540B0683BDEBBAC2CFEC (void);
// 0x0000005C System.Collections.Generic.IEnumerable`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer::GetElements(System.Xml.Linq.XName)
extern void XContainer_GetElements_mD0047470C5500C65C8EE064CEF831242E64766E3 (void);
// 0x0000005D System.String System.Xml.Linq.XContainer::GetStringValue(System.Object)
extern void XContainer_GetStringValue_m0446331D32F7BDCAE899B332243E52D6B16180EE (void);
// 0x0000005E System.Void System.Xml.Linq.XContainer::ReadContentFrom(System.Xml.XmlReader)
extern void XContainer_ReadContentFrom_mF11967A08DEE66ACFB5179D0C8F6B0190644A2AC (void);
// 0x0000005F System.Void System.Xml.Linq.XContainer::ReadContentFrom(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern void XContainer_ReadContentFrom_m10603F00B56DF27E71DCE896EF28269906D3F629 (void);
// 0x00000060 System.Void System.Xml.Linq.XContainer::ValidateNode(System.Xml.Linq.XNode,System.Xml.Linq.XNode)
extern void XContainer_ValidateNode_mCB3953BC2CCE733B956DF952FB8EA39089D15B96 (void);
// 0x00000061 System.Void System.Xml.Linq.XContainer::ValidateString(System.String)
extern void XContainer_ValidateString_m0E5A8B53A0F59FF13B97310D30FFF8C70DAF011F (void);
// 0x00000062 System.Void System.Xml.Linq.XContainer::WriteContentTo(System.Xml.XmlWriter)
extern void XContainer_WriteContentTo_mFA062B17E987BA975BF8CC8022E15A5B9E903C96 (void);
// 0x00000063 System.Void System.Xml.Linq.XContainer_<GetElements>d__40::.ctor(System.Int32)
extern void U3CGetElementsU3Ed__40__ctor_mF908BE084ADA6F221E2B54376F43CF6A79676663 (void);
// 0x00000064 System.Void System.Xml.Linq.XContainer_<GetElements>d__40::System.IDisposable.Dispose()
extern void U3CGetElementsU3Ed__40_System_IDisposable_Dispose_m26D03CD4EC581A64543916058C2744EF2113E353 (void);
// 0x00000065 System.Boolean System.Xml.Linq.XContainer_<GetElements>d__40::MoveNext()
extern void U3CGetElementsU3Ed__40_MoveNext_mBC40C788810006A11FE0F1EFB9730F31DC6B1D6E (void);
// 0x00000066 System.Xml.Linq.XElement System.Xml.Linq.XContainer_<GetElements>d__40::System.Collections.Generic.IEnumerator<System.Xml.Linq.XElement>.get_Current()
extern void U3CGetElementsU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_Xml_Linq_XElementU3E_get_Current_m557169F857BE3A2E8AE60A5B31793CCB5FEE6DF8 (void);
// 0x00000067 System.Void System.Xml.Linq.XContainer_<GetElements>d__40::System.Collections.IEnumerator.Reset()
extern void U3CGetElementsU3Ed__40_System_Collections_IEnumerator_Reset_mEBD70692F6B1A660FC8C4BE32AEC4079CC25BD10 (void);
// 0x00000068 System.Object System.Xml.Linq.XContainer_<GetElements>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CGetElementsU3Ed__40_System_Collections_IEnumerator_get_Current_mE30CD7CAB4C943F4FB52BAFB133941024A125CB9 (void);
// 0x00000069 System.Collections.Generic.IEnumerator`1<System.Xml.Linq.XElement> System.Xml.Linq.XContainer_<GetElements>d__40::System.Collections.Generic.IEnumerable<System.Xml.Linq.XElement>.GetEnumerator()
extern void U3CGetElementsU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_Xml_Linq_XElementU3E_GetEnumerator_m5985519113352CB9D6500FD7EE23018996093E8D (void);
// 0x0000006A System.Collections.IEnumerator System.Xml.Linq.XContainer_<GetElements>d__40::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetElementsU3Ed__40_System_Collections_IEnumerable_GetEnumerator_m1E01D5F2FFF1255AAD4FE9DE0E03ED6E59F228CA (void);
// 0x0000006B System.Xml.Linq.XNamespace System.Xml.Linq.NamespaceCache::Get(System.String)
extern void NamespaceCache_Get_mDA87B6F824AD4AAA424A68A15CCDB48205326995_AdjustorThunk (void);
// 0x0000006C System.Void System.Xml.Linq.XElement::.ctor(System.Xml.Linq.XName)
extern void XElement__ctor_m7C010BE83763396D50EB4E51C1B21A1DDDE258B2 (void);
// 0x0000006D System.Void System.Xml.Linq.XElement::.ctor(System.Xml.Linq.XElement)
extern void XElement__ctor_m13B7B350DE7B5DDB8B55F222AA15066B66B149A6 (void);
// 0x0000006E System.Void System.Xml.Linq.XElement::.ctor(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern void XElement__ctor_m2C56952E8343794394BD89FC13220C637CCE54D4 (void);
// 0x0000006F System.Xml.Linq.XName System.Xml.Linq.XElement::get_Name()
extern void XElement_get_Name_m1154DCAA063C86BB056B2A49FF9C4FA65CEEAD3D (void);
// 0x00000070 System.Xml.XmlNodeType System.Xml.Linq.XElement::get_NodeType()
extern void XElement_get_NodeType_m4C526FE6C4115275BADBDEE3F51292C0C272F2D4 (void);
// 0x00000071 System.String System.Xml.Linq.XElement::get_Value()
extern void XElement_get_Value_mBD9F8869BF4F3865FEDED418854020E9AFF031FA (void);
// 0x00000072 System.Xml.Linq.XAttribute System.Xml.Linq.XElement::Attribute(System.Xml.Linq.XName)
extern void XElement_Attribute_mE6D1589D13FFB71DA11EA7B41685C149D1E03AFD (void);
// 0x00000073 System.String System.Xml.Linq.XElement::GetPrefixOfNamespace(System.Xml.Linq.XNamespace)
extern void XElement_GetPrefixOfNamespace_mF9843F62F43762B9E43BBAF8DA634CFBBA393C8E (void);
// 0x00000074 System.Xml.Linq.XElement System.Xml.Linq.XElement::Load(System.String)
extern void XElement_Load_mC160CCC050573C9C28655C76735F9AFC38395DA8 (void);
// 0x00000075 System.Xml.Linq.XElement System.Xml.Linq.XElement::Load(System.String,System.Xml.Linq.LoadOptions)
extern void XElement_Load_mB362D7CC262570080607DD72CA938EA49715212D (void);
// 0x00000076 System.Xml.Linq.XElement System.Xml.Linq.XElement::Load(System.IO.TextReader)
extern void XElement_Load_mA5A76D44319CDABC99E4201B9F92BA73ED4A02FC (void);
// 0x00000077 System.Xml.Linq.XElement System.Xml.Linq.XElement::Load(System.IO.TextReader,System.Xml.Linq.LoadOptions)
extern void XElement_Load_m01E1AD52F0CE5EEBA293F249951044361D2B6FAA (void);
// 0x00000078 System.Xml.Linq.XElement System.Xml.Linq.XElement::Load(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern void XElement_Load_m7976C5FDE8448AA4846A3EB9032C88B9C1623861 (void);
// 0x00000079 System.Void System.Xml.Linq.XElement::WriteTo(System.Xml.XmlWriter)
extern void XElement_WriteTo_mD0544B1B917B698378053735D34076540D872154 (void);
// 0x0000007A System.String System.Xml.Linq.XElement::op_Explicit(System.Xml.Linq.XElement)
extern void XElement_op_Explicit_m07DA350ACA2F65E6ED8A74C6C1BCE6D13D99BC9E (void);
// 0x0000007B System.Void System.Xml.Linq.XElement::AppendAttributeSkipNotify(System.Xml.Linq.XAttribute)
extern void XElement_AppendAttributeSkipNotify_m161154367C52FF183725DCE878094059B0042843 (void);
// 0x0000007C System.Xml.Linq.XNode System.Xml.Linq.XElement::CloneNode()
extern void XElement_CloneNode_m0D89413E6EE83A35FD5F726A6E5131F0AF6A3712 (void);
// 0x0000007D System.String System.Xml.Linq.XElement::GetNamespaceOfPrefixInScope(System.String,System.Xml.Linq.XElement)
extern void XElement_GetNamespaceOfPrefixInScope_m976485B69E3219453A8A4412721F8C3C90D03A96 (void);
// 0x0000007E System.Void System.Xml.Linq.XElement::ReadElementFrom(System.Xml.XmlReader,System.Xml.Linq.LoadOptions)
extern void XElement_ReadElementFrom_mF82AFFF2679EAA2E7BD55E9C5D625A56FEBF06C3 (void);
// 0x0000007F System.Void System.Xml.Linq.XElement::SetEndElementLineInfo(System.Int32,System.Int32)
extern void XElement_SetEndElementLineInfo_m4AF7794890F6E0D65E87B8F967E9A3F6B13B41D5 (void);
// 0x00000080 System.Void System.Xml.Linq.XElement::ValidateNode(System.Xml.Linq.XNode,System.Xml.Linq.XNode)
extern void XElement_ValidateNode_mAFE6C941B26F52216906391DC84D40D587F58C3F (void);
// 0x00000081 System.Void System.Xml.Linq.ElementWriter::.ctor(System.Xml.XmlWriter)
extern void ElementWriter__ctor_mAF42701DBC7426DB8FF6F3C19F37C6A5B6E6F798_AdjustorThunk (void);
// 0x00000082 System.Void System.Xml.Linq.ElementWriter::WriteElement(System.Xml.Linq.XElement)
extern void ElementWriter_WriteElement_mCA724B1F81D4D096CA8A0A3BC3B38B8AAB7A6E7C_AdjustorThunk (void);
// 0x00000083 System.String System.Xml.Linq.ElementWriter::GetPrefixOfNamespace(System.Xml.Linq.XNamespace,System.Boolean)
extern void ElementWriter_GetPrefixOfNamespace_mFE90F844D9421A68634B9AF38ACBA1D127269F26_AdjustorThunk (void);
// 0x00000084 System.Void System.Xml.Linq.ElementWriter::PushAncestors(System.Xml.Linq.XElement)
extern void ElementWriter_PushAncestors_mAA5058229B36969BAD86E2332E5B85EF93C10DAD_AdjustorThunk (void);
// 0x00000085 System.Void System.Xml.Linq.ElementWriter::PushElement(System.Xml.Linq.XElement)
extern void ElementWriter_PushElement_mD74364B14FDE31D74A6DA824F21EF009142EBAC5_AdjustorThunk (void);
// 0x00000086 System.Void System.Xml.Linq.ElementWriter::WriteEndElement()
extern void ElementWriter_WriteEndElement_mB6853DBC6AC64F10185207107EBE974AB5A274CA_AdjustorThunk (void);
// 0x00000087 System.Void System.Xml.Linq.ElementWriter::WriteFullEndElement()
extern void ElementWriter_WriteFullEndElement_mAC1EC5BF80311A650B92992C4A9C689811A782F5_AdjustorThunk (void);
// 0x00000088 System.Void System.Xml.Linq.ElementWriter::WriteStartElement(System.Xml.Linq.XElement)
extern void ElementWriter_WriteStartElement_m09181D51C5C38A1BB15B41D85A88FE6B8A0BDBFF_AdjustorThunk (void);
// 0x00000089 System.Void System.Xml.Linq.NamespaceResolver::PushScope()
extern void NamespaceResolver_PushScope_m6A174DFEE5F6E262E8B050EAE30CD935ED3FD468_AdjustorThunk (void);
// 0x0000008A System.Void System.Xml.Linq.NamespaceResolver::PopScope()
extern void NamespaceResolver_PopScope_m48BFA95631D7F0301120439B128FF8F7A16C261B_AdjustorThunk (void);
// 0x0000008B System.Void System.Xml.Linq.NamespaceResolver::Add(System.String,System.Xml.Linq.XNamespace)
extern void NamespaceResolver_Add_mA16774DA6B6A470EC2416849D659179275D28F81_AdjustorThunk (void);
// 0x0000008C System.Void System.Xml.Linq.NamespaceResolver::AddFirst(System.String,System.Xml.Linq.XNamespace)
extern void NamespaceResolver_AddFirst_m7C861D08943FFBAA33EF102312B96499D73CF289_AdjustorThunk (void);
// 0x0000008D System.String System.Xml.Linq.NamespaceResolver::GetPrefixOfNamespace(System.Xml.Linq.XNamespace,System.Boolean)
extern void NamespaceResolver_GetPrefixOfNamespace_m69FAACB8B514D2F3C1E00473B9EC5A7B169F5C0F_AdjustorThunk (void);
// 0x0000008E System.Void System.Xml.Linq.NamespaceResolver_NamespaceDeclaration::.ctor()
extern void NamespaceDeclaration__ctor_m86ED6350C67E1DA1AAD7F9EAB3D460F12A9EE180 (void);
// 0x0000008F System.Void System.Xml.Linq.XDocument::.ctor(System.Xml.Linq.XDocument)
extern void XDocument__ctor_m35FD266D78A2F7C6BD941715B4EE65E86AF2E5A6 (void);
// 0x00000090 System.Xml.XmlNodeType System.Xml.Linq.XDocument::get_NodeType()
extern void XDocument_get_NodeType_m94EEA82F54C2D649D757B564D1BBC9D6EC516601 (void);
// 0x00000091 System.Void System.Xml.Linq.XDocument::WriteTo(System.Xml.XmlWriter)
extern void XDocument_WriteTo_mCA9178E00D706F20B0F0EC03956C778B4FD95A82 (void);
// 0x00000092 System.Xml.Linq.XNode System.Xml.Linq.XDocument::CloneNode()
extern void XDocument_CloneNode_m5CD649BF1FAB030E425751216C9CC3EF1B00C405 (void);
// 0x00000093 System.Boolean System.Xml.Linq.XDocument::IsWhitespace(System.String)
extern void XDocument_IsWhitespace_mD58CA1E5A1CEE372FE68D01AEA18DD7A8A705420 (void);
// 0x00000094 System.Void System.Xml.Linq.XDocument::ValidateNode(System.Xml.Linq.XNode,System.Xml.Linq.XNode)
extern void XDocument_ValidateNode_m910C70D956DB041302A7AF202D9B361BF1AD8B1F (void);
// 0x00000095 System.Void System.Xml.Linq.XDocument::ValidateDocument(System.Xml.Linq.XNode,System.Xml.XmlNodeType,System.Xml.XmlNodeType)
extern void XDocument_ValidateDocument_mF01236248E0025F9F140CC29CC233395C4BB4B46 (void);
// 0x00000096 System.Void System.Xml.Linq.XDocument::ValidateString(System.String)
extern void XDocument_ValidateString_m416D0F8419FDA051AA3E98D16C36E6D93C78DB8A (void);
// 0x00000097 System.Void System.Xml.Linq.XComment::.ctor(System.String)
extern void XComment__ctor_mD1594490E6706E752D9A5C9B748CF84AEC2B9B22 (void);
// 0x00000098 System.Void System.Xml.Linq.XComment::.ctor(System.Xml.Linq.XComment)
extern void XComment__ctor_m293F320541A1F11249027B6C8D623462AD383AA8 (void);
// 0x00000099 System.Xml.XmlNodeType System.Xml.Linq.XComment::get_NodeType()
extern void XComment_get_NodeType_m5D1AD71934D3979AF424213BFCC9CDC2CC9A1887 (void);
// 0x0000009A System.Void System.Xml.Linq.XComment::WriteTo(System.Xml.XmlWriter)
extern void XComment_WriteTo_m3686F4EF4AB18C7F42564BC580E33C1CAAEB2BB2 (void);
// 0x0000009B System.Xml.Linq.XNode System.Xml.Linq.XComment::CloneNode()
extern void XComment_CloneNode_m22A6E114042E24F367B21549A66EC368B4B9846E (void);
// 0x0000009C System.Void System.Xml.Linq.XProcessingInstruction::.ctor(System.String,System.String)
extern void XProcessingInstruction__ctor_m49A9FD814CED60CDF313649D7529DBE0AC8D4A7B (void);
// 0x0000009D System.Void System.Xml.Linq.XProcessingInstruction::.ctor(System.Xml.Linq.XProcessingInstruction)
extern void XProcessingInstruction__ctor_mFBD37F1027E57C2EB1BE94A0803BE86EC424BCE1 (void);
// 0x0000009E System.Xml.XmlNodeType System.Xml.Linq.XProcessingInstruction::get_NodeType()
extern void XProcessingInstruction_get_NodeType_m1F4FC7AC8000A33EEBAAE945C38B2ABDC9E77F38 (void);
// 0x0000009F System.Void System.Xml.Linq.XProcessingInstruction::WriteTo(System.Xml.XmlWriter)
extern void XProcessingInstruction_WriteTo_m3FB64607898D2DCC5DD36ACCFBDF4AC723FB1CC3 (void);
// 0x000000A0 System.Xml.Linq.XNode System.Xml.Linq.XProcessingInstruction::CloneNode()
extern void XProcessingInstruction_CloneNode_mA174650C7096205059FE436DB9A07159580BAC96 (void);
// 0x000000A1 System.Void System.Xml.Linq.XProcessingInstruction::ValidateName(System.String)
extern void XProcessingInstruction_ValidateName_m0EAC108733F1A398C88966D74AF9FFC4D7231E51 (void);
// 0x000000A2 System.Void System.Xml.Linq.XDeclaration::.ctor(System.Xml.Linq.XDeclaration)
extern void XDeclaration__ctor_mBCB4F6ED791E5154C266DBC138B2647309D9C3AE (void);
// 0x000000A3 System.String System.Xml.Linq.XDeclaration::get_Standalone()
extern void XDeclaration_get_Standalone_m15EF1F74D2226D5CAE08BB9847949FBD2F63D967 (void);
// 0x000000A4 System.String System.Xml.Linq.XDeclaration::ToString()
extern void XDeclaration_ToString_m62CE18C891848DF411864E40CEA3B1FB5490DCBE (void);
// 0x000000A5 System.Void System.Xml.Linq.XDocumentType::.ctor(System.String,System.String,System.String,System.String)
extern void XDocumentType__ctor_m8DD9AEBE8CA7BEE73B9AE7D221DA238FB47724C3 (void);
// 0x000000A6 System.Void System.Xml.Linq.XDocumentType::.ctor(System.Xml.Linq.XDocumentType)
extern void XDocumentType__ctor_m4973BAC4702EF6EDA6B304CC74DD77F34B1EA51A (void);
// 0x000000A7 System.Void System.Xml.Linq.XDocumentType::.ctor(System.String,System.String,System.String,System.String,System.Xml.IDtdInfo)
extern void XDocumentType__ctor_m92F5146E20DD2E0CFA9A8A8DFF85D27050B314D5 (void);
// 0x000000A8 System.Xml.XmlNodeType System.Xml.Linq.XDocumentType::get_NodeType()
extern void XDocumentType_get_NodeType_m6209474A166B3032CD79F42FE99C975EB44B6A6A (void);
// 0x000000A9 System.Void System.Xml.Linq.XDocumentType::WriteTo(System.Xml.XmlWriter)
extern void XDocumentType_WriteTo_m106DADF2D6955F901D34239ACC17F1CD47F06063 (void);
// 0x000000AA System.Xml.Linq.XNode System.Xml.Linq.XDocumentType::CloneNode()
extern void XDocumentType_CloneNode_mBD0170ABA11A33CDFD0D19E98429E4A08EE471E8 (void);
// 0x000000AB System.Void System.Xml.Linq.XAttribute::.ctor(System.Xml.Linq.XName,System.Object)
extern void XAttribute__ctor_m40547774AADEFD8633306215AA57995F4BA4470F (void);
// 0x000000AC System.Void System.Xml.Linq.XAttribute::.ctor(System.Xml.Linq.XAttribute)
extern void XAttribute__ctor_m3EA3008A0D6C0B66C3868AAEB7EC7563DDD57C7B (void);
// 0x000000AD System.Boolean System.Xml.Linq.XAttribute::get_IsNamespaceDeclaration()
extern void XAttribute_get_IsNamespaceDeclaration_mE2D54BB132D0A554FA334BDAF92D5664E2BF50EA (void);
// 0x000000AE System.Xml.Linq.XName System.Xml.Linq.XAttribute::get_Name()
extern void XAttribute_get_Name_m89A65BCA2C1D25349F1B814F823208D897CA79FB (void);
// 0x000000AF System.Xml.XmlNodeType System.Xml.Linq.XAttribute::get_NodeType()
extern void XAttribute_get_NodeType_m716AD291593A05B9829DEF1F8CB86A99C88FEE56 (void);
// 0x000000B0 System.String System.Xml.Linq.XAttribute::get_Value()
extern void XAttribute_get_Value_m2458D145A9C0231EE71816040601199252F53A84 (void);
// 0x000000B1 System.String System.Xml.Linq.XAttribute::ToString()
extern void XAttribute_ToString_m93ACF1DA23DA530929D9EA831ED8A95D19042E21 (void);
// 0x000000B2 System.String System.Xml.Linq.XAttribute::op_Explicit(System.Xml.Linq.XAttribute)
extern void XAttribute_op_Explicit_mBD277501C14D52654255A6E1C95FF59FD193578D (void);
// 0x000000B3 System.String System.Xml.Linq.XAttribute::GetPrefixOfNamespace(System.Xml.Linq.XNamespace)
extern void XAttribute_GetPrefixOfNamespace_mA63C230B534D3AE6BB47161C68D335D729FE8201 (void);
// 0x000000B4 System.Void System.Xml.Linq.XAttribute::ValidateAttribute(System.Xml.Linq.XName,System.String)
extern void XAttribute_ValidateAttribute_m131F4A72A9F7E79F93B34C2F7909C80995B6B7B2 (void);
// 0x000000B5 System.String System.Xml.Linq.Res::GetString(System.String)
extern void Res_GetString_mCACED687581128D64F4A26E6929E41E44DC17750 (void);
// 0x000000B6 System.String System.Xml.Linq.Res::GetString(System.String,System.Object[])
extern void Res_GetString_mA952BE145DB45E41118302D24CD5FF7772CDE9F0 (void);
// 0x000000B7 System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_m627B7273A2428AFBCF4C41D574196135907A582D (void);
// 0x000000B8 System.Void Unity.ThrowStub::ThrowNotSupportedException()
extern void ThrowStub_ThrowNotSupportedException_m1B65D6A1FF55272EEE13380F3E7C53E2015C3F44 (void);
static Il2CppMethodPointer s_methodPointers[184] = 
{
	XName__ctor_m08CA9A842644AFE1545DDFB3EA1BF011D797CE59,
	XName_get_LocalName_m696497ACA70F6707FAF6DDC7B0FB4297D7F4A007,
	XName_get_Namespace_m27DDB135C04C90BB031CA5ECC7B3436A31423488,
	XName_get_NamespaceName_m48B792158467825280622E60E7222C565B3BDB20,
	XName_ToString_m5095F3F0750DD9AF19A7A4817CECD4192B4C0DD7,
	XName_Get_mFE26FB1696ED2D60324938EBEB37C0363637BAB7,
	XName_op_Implicit_mC9F588EE5257A3A259D5CF52E9EBCC36B46C01C6,
	XName_Equals_m688E6032F6F05612771D7E920B5CD1723D7EF24A,
	XName_GetHashCode_m4B3D379685B914C9BA6E29BB4A861853AF582D3A,
	XName_op_Equality_m303F10BF2291A8B7AD3E3ACB4BB00C0800777B06,
	XName_System_IEquatableU3CSystem_Xml_Linq_XNameU3E_Equals_mCDE24CE750E5DB3C7E3432F457FCFE7C6657FF81,
	XName_System_Runtime_Serialization_ISerializable_GetObjectData_m45F244C932C673A2DBB248CDD1A640138A02DAFA,
	XName__ctor_m2D7B85963C672FA3E6E5559CC101F790A4757ECE,
	NameSerializer__ctor_mFF83F9040F5363B72D87DC6CCEA0321DB215F822,
	NameSerializer_System_Runtime_Serialization_IObjectReference_GetRealObject_m67AA4E4AF30FB2A0898BEC4C2E9DB34BEA5AD4A6,
	NameSerializer_System_Runtime_Serialization_ISerializable_GetObjectData_m309AB629E47E1D640D2A44960A9004BC34016B8C,
	XNamespace__ctor_m032BFBA1A9B9D4B33642F96DB64DC4297F331617,
	XNamespace_get_NamespaceName_mD97F3E27C3274AEB1B8AE634BE739F44219992DB,
	XNamespace_GetName_mB9A7E789565459E55D929AF8F7C84CC38BBC8DFD,
	XNamespace_ToString_m1D5D03F1C2C1374DFCE24FB397394CD76584BC70,
	XNamespace_get_None_m2795996F3AD1908D3CFC3F1AFCD58CFBED61EFE0,
	XNamespace_get_Xml_mF816E04DDC8ED562C6B6E71449706594EECCA55E,
	XNamespace_get_Xmlns_m140AE4DD66BD99100AA9492D105D3B1383BC5112,
	XNamespace_Get_m74B196DD9CD7770D401FD672F86CE0FBB8FCE9F6,
	XNamespace_Equals_m8964855B6CBF87B2BC5317DABE0678F6E24683E6,
	XNamespace_GetHashCode_m90177556974B375FEBD8F55CA6D208924BA366D0,
	XNamespace_op_Equality_m59C867B675C5C15015A9F081A6BD2A47370A4DBE,
	XNamespace_op_Inequality_m075CAE6D2922DE14AB223F9C4C290E9BC478CD12,
	XNamespace_GetName_m59979BF3C0D17127A15B3369152BA0821FB10794,
	XNamespace_Get_mF8EF85F60D3C811461541AC8FD3DCFF9C081BCAA,
	XNamespace_ExtractLocalName_mFFC3D4C8CA969449FAD1F029330DED93180959A9,
	XNamespace_ExtractNamespace_mC09B1EB8F42638C5B58F853F81CB9A32B73D415A,
	XNamespace_EnsureNamespace_m0F4936CB583036C26C69C3DBBE0A37D4D3635F18,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	XObject__ctor_mEBF29FD8B8171A6AE6D42BA886E7BFAE31BE563D,
	XObject_get_BaseUri_m130E3178739EEEB478D7826AF5341937EBCA4E97,
	NULL,
	XObject_AddAnnotation_mCD582B99945D6A963D6C2FB0CFB3CBBEAFE37589,
	XObject_Annotation_m95490F1AA8945BB8554BC5564CC9A8D9C174AA67,
	NULL,
	XObject_System_Xml_IXmlLineInfo_HasLineInfo_m5A8D102E176F2C0D75E8E8F647960A159F343F07,
	XObject_System_Xml_IXmlLineInfo_get_LineNumber_m24AD33EE2AB7E73E8E8A734B384AF246647CE4A4,
	XObject_System_Xml_IXmlLineInfo_get_LinePosition_m2B081F9CCF96364A83B0B34E4AEAAFCC20CDD184,
	XObject_get_HasBaseUri_m495D449FCEEAB86FEFD3119310D1D1C7F7271C5A,
	XObject_SetBaseUri_m166BE057D57AACC044AB91ADE70D36408FD4ED27,
	XObject_SetLineInfo_m5A5F3FB9CE20DA7EC016C2263178A0CBBFD9AB95,
	XObject_GetSaveOptionsFromAnnotations_m3DBE274F86C30B72962F8D9D76ABEA8E2E773D32,
	BaseUriAnnotation__ctor_m73734683B4E9AABD14F9F4FC06456033579CA178,
	LineInfoAnnotation__ctor_m8E8D7A480E7C181E9B65DC98003BC166AEF5AB38,
	LineInfoEndElementAnnotation__ctor_mC681F34922255C61324B88EF7174034FB58C3887,
	XNode__ctor_mE21FF9BECBD8850494DDA2C81F01BA8FAF799D8A,
	XNode_ToString_m1BEC71637268EF8CCE9EA74B6F30859A4A3597CB,
	NULL,
	XNode_AppendText_mE2146B20CA977C9A065DF58A4FD15138D5B9BE8C,
	NULL,
	XNode_GetXmlReaderSettings_mC92954C145134317C8FF8B82C9FF25B44F10E633,
	XNode_GetXmlString_m430A1536E96B6502637DC174F04A9D8F330176E0,
	XText__ctor_m68D7A7EBA4977B7FC8570D10E60B34EDC4D8CA5B,
	XText__ctor_mC0BAC0C89D83D1AC431C1E0CC146752EAFDA29AF,
	XText_get_NodeType_mF1B647A9AF434D45DA76307855DCAE7E1E140557,
	XText_get_Value_m9597869DABAA9D2BF8275CEDB4A6A85D46FF6BE5,
	XText_WriteTo_m45EE125458AB37E57B311DFDD80CCF755505BE7D,
	XText_AppendText_m9CB12B0D10400CFBB5C612E05E3468697646183C,
	XText_CloneNode_mEA4C49F38831E5EF3DF3D002EE22EF71EBF28029,
	XCData__ctor_m9FA9F20CF152A7C7EEC9320F2FCD3F2F036C8F2E,
	XCData__ctor_m7FC6A1CFB551D65D4411EBFE077A4936037E3FFF,
	XCData_get_NodeType_mF9CD12D94265A3B87F19209B6D32F4BE84DF8B48,
	XCData_WriteTo_mE9BC80649A08BC72B78DF18F9C1FF7619BDFF5D2,
	XCData_CloneNode_mD8FC76E124B8D62AF72D1086D57E17CC99C6C0C8,
	XContainer__ctor_m3C7E61824CB53C8FA18BC4226C6C0BA4BAE3B646,
	XContainer__ctor_mA6A69DAB1077038C297FBE6B4697DD40AB426ED2,
	XContainer_Element_mDD805465CD32F3FD06807CA9E34BBD976A554BB5,
	XContainer_Elements_mE3525ED34CA3769624CB81CF4501C662FE824DB3,
	XContainer_AddNodeSkipNotify_m3F02B171C8968D655B0DD2B7C544CB4BB66A16B5,
	XContainer_AddStringSkipNotify_m549907826B0971C84089F41A99115AB8791A3D2E,
	XContainer_AppendNodeSkipNotify_m7E11E884F1504058BAB299925A1BD9092A7E0CF3,
	XContainer_AppendText_m551C5A592CE1FCD30897888F8E794F40459E8922,
	XContainer_ConvertTextToNode_m814277C7AD70A979F8FC62CF38682F02409A7160,
	XContainer_GetDateTimeString_mD58B479E6A678F25B000540B0683BDEBBAC2CFEC,
	XContainer_GetElements_mD0047470C5500C65C8EE064CEF831242E64766E3,
	XContainer_GetStringValue_m0446331D32F7BDCAE899B332243E52D6B16180EE,
	XContainer_ReadContentFrom_mF11967A08DEE66ACFB5179D0C8F6B0190644A2AC,
	XContainer_ReadContentFrom_m10603F00B56DF27E71DCE896EF28269906D3F629,
	XContainer_ValidateNode_mCB3953BC2CCE733B956DF952FB8EA39089D15B96,
	XContainer_ValidateString_m0E5A8B53A0F59FF13B97310D30FFF8C70DAF011F,
	XContainer_WriteContentTo_mFA062B17E987BA975BF8CC8022E15A5B9E903C96,
	U3CGetElementsU3Ed__40__ctor_mF908BE084ADA6F221E2B54376F43CF6A79676663,
	U3CGetElementsU3Ed__40_System_IDisposable_Dispose_m26D03CD4EC581A64543916058C2744EF2113E353,
	U3CGetElementsU3Ed__40_MoveNext_mBC40C788810006A11FE0F1EFB9730F31DC6B1D6E,
	U3CGetElementsU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_Xml_Linq_XElementU3E_get_Current_m557169F857BE3A2E8AE60A5B31793CCB5FEE6DF8,
	U3CGetElementsU3Ed__40_System_Collections_IEnumerator_Reset_mEBD70692F6B1A660FC8C4BE32AEC4079CC25BD10,
	U3CGetElementsU3Ed__40_System_Collections_IEnumerator_get_Current_mE30CD7CAB4C943F4FB52BAFB133941024A125CB9,
	U3CGetElementsU3Ed__40_System_Collections_Generic_IEnumerableU3CSystem_Xml_Linq_XElementU3E_GetEnumerator_m5985519113352CB9D6500FD7EE23018996093E8D,
	U3CGetElementsU3Ed__40_System_Collections_IEnumerable_GetEnumerator_m1E01D5F2FFF1255AAD4FE9DE0E03ED6E59F228CA,
	NamespaceCache_Get_mDA87B6F824AD4AAA424A68A15CCDB48205326995_AdjustorThunk,
	XElement__ctor_m7C010BE83763396D50EB4E51C1B21A1DDDE258B2,
	XElement__ctor_m13B7B350DE7B5DDB8B55F222AA15066B66B149A6,
	XElement__ctor_m2C56952E8343794394BD89FC13220C637CCE54D4,
	XElement_get_Name_m1154DCAA063C86BB056B2A49FF9C4FA65CEEAD3D,
	XElement_get_NodeType_m4C526FE6C4115275BADBDEE3F51292C0C272F2D4,
	XElement_get_Value_mBD9F8869BF4F3865FEDED418854020E9AFF031FA,
	XElement_Attribute_mE6D1589D13FFB71DA11EA7B41685C149D1E03AFD,
	XElement_GetPrefixOfNamespace_mF9843F62F43762B9E43BBAF8DA634CFBBA393C8E,
	XElement_Load_mC160CCC050573C9C28655C76735F9AFC38395DA8,
	XElement_Load_mB362D7CC262570080607DD72CA938EA49715212D,
	XElement_Load_mA5A76D44319CDABC99E4201B9F92BA73ED4A02FC,
	XElement_Load_m01E1AD52F0CE5EEBA293F249951044361D2B6FAA,
	XElement_Load_m7976C5FDE8448AA4846A3EB9032C88B9C1623861,
	XElement_WriteTo_mD0544B1B917B698378053735D34076540D872154,
	XElement_op_Explicit_m07DA350ACA2F65E6ED8A74C6C1BCE6D13D99BC9E,
	XElement_AppendAttributeSkipNotify_m161154367C52FF183725DCE878094059B0042843,
	XElement_CloneNode_m0D89413E6EE83A35FD5F726A6E5131F0AF6A3712,
	XElement_GetNamespaceOfPrefixInScope_m976485B69E3219453A8A4412721F8C3C90D03A96,
	XElement_ReadElementFrom_mF82AFFF2679EAA2E7BD55E9C5D625A56FEBF06C3,
	XElement_SetEndElementLineInfo_m4AF7794890F6E0D65E87B8F967E9A3F6B13B41D5,
	XElement_ValidateNode_mAFE6C941B26F52216906391DC84D40D587F58C3F,
	ElementWriter__ctor_mAF42701DBC7426DB8FF6F3C19F37C6A5B6E6F798_AdjustorThunk,
	ElementWriter_WriteElement_mCA724B1F81D4D096CA8A0A3BC3B38B8AAB7A6E7C_AdjustorThunk,
	ElementWriter_GetPrefixOfNamespace_mFE90F844D9421A68634B9AF38ACBA1D127269F26_AdjustorThunk,
	ElementWriter_PushAncestors_mAA5058229B36969BAD86E2332E5B85EF93C10DAD_AdjustorThunk,
	ElementWriter_PushElement_mD74364B14FDE31D74A6DA824F21EF009142EBAC5_AdjustorThunk,
	ElementWriter_WriteEndElement_mB6853DBC6AC64F10185207107EBE974AB5A274CA_AdjustorThunk,
	ElementWriter_WriteFullEndElement_mAC1EC5BF80311A650B92992C4A9C689811A782F5_AdjustorThunk,
	ElementWriter_WriteStartElement_m09181D51C5C38A1BB15B41D85A88FE6B8A0BDBFF_AdjustorThunk,
	NamespaceResolver_PushScope_m6A174DFEE5F6E262E8B050EAE30CD935ED3FD468_AdjustorThunk,
	NamespaceResolver_PopScope_m48BFA95631D7F0301120439B128FF8F7A16C261B_AdjustorThunk,
	NamespaceResolver_Add_mA16774DA6B6A470EC2416849D659179275D28F81_AdjustorThunk,
	NamespaceResolver_AddFirst_m7C861D08943FFBAA33EF102312B96499D73CF289_AdjustorThunk,
	NamespaceResolver_GetPrefixOfNamespace_m69FAACB8B514D2F3C1E00473B9EC5A7B169F5C0F_AdjustorThunk,
	NamespaceDeclaration__ctor_m86ED6350C67E1DA1AAD7F9EAB3D460F12A9EE180,
	XDocument__ctor_m35FD266D78A2F7C6BD941715B4EE65E86AF2E5A6,
	XDocument_get_NodeType_m94EEA82F54C2D649D757B564D1BBC9D6EC516601,
	XDocument_WriteTo_mCA9178E00D706F20B0F0EC03956C778B4FD95A82,
	XDocument_CloneNode_m5CD649BF1FAB030E425751216C9CC3EF1B00C405,
	XDocument_IsWhitespace_mD58CA1E5A1CEE372FE68D01AEA18DD7A8A705420,
	XDocument_ValidateNode_m910C70D956DB041302A7AF202D9B361BF1AD8B1F,
	XDocument_ValidateDocument_mF01236248E0025F9F140CC29CC233395C4BB4B46,
	XDocument_ValidateString_m416D0F8419FDA051AA3E98D16C36E6D93C78DB8A,
	XComment__ctor_mD1594490E6706E752D9A5C9B748CF84AEC2B9B22,
	XComment__ctor_m293F320541A1F11249027B6C8D623462AD383AA8,
	XComment_get_NodeType_m5D1AD71934D3979AF424213BFCC9CDC2CC9A1887,
	XComment_WriteTo_m3686F4EF4AB18C7F42564BC580E33C1CAAEB2BB2,
	XComment_CloneNode_m22A6E114042E24F367B21549A66EC368B4B9846E,
	XProcessingInstruction__ctor_m49A9FD814CED60CDF313649D7529DBE0AC8D4A7B,
	XProcessingInstruction__ctor_mFBD37F1027E57C2EB1BE94A0803BE86EC424BCE1,
	XProcessingInstruction_get_NodeType_m1F4FC7AC8000A33EEBAAE945C38B2ABDC9E77F38,
	XProcessingInstruction_WriteTo_m3FB64607898D2DCC5DD36ACCFBDF4AC723FB1CC3,
	XProcessingInstruction_CloneNode_mA174650C7096205059FE436DB9A07159580BAC96,
	XProcessingInstruction_ValidateName_m0EAC108733F1A398C88966D74AF9FFC4D7231E51,
	XDeclaration__ctor_mBCB4F6ED791E5154C266DBC138B2647309D9C3AE,
	XDeclaration_get_Standalone_m15EF1F74D2226D5CAE08BB9847949FBD2F63D967,
	XDeclaration_ToString_m62CE18C891848DF411864E40CEA3B1FB5490DCBE,
	XDocumentType__ctor_m8DD9AEBE8CA7BEE73B9AE7D221DA238FB47724C3,
	XDocumentType__ctor_m4973BAC4702EF6EDA6B304CC74DD77F34B1EA51A,
	XDocumentType__ctor_m92F5146E20DD2E0CFA9A8A8DFF85D27050B314D5,
	XDocumentType_get_NodeType_m6209474A166B3032CD79F42FE99C975EB44B6A6A,
	XDocumentType_WriteTo_m106DADF2D6955F901D34239ACC17F1CD47F06063,
	XDocumentType_CloneNode_mBD0170ABA11A33CDFD0D19E98429E4A08EE471E8,
	XAttribute__ctor_m40547774AADEFD8633306215AA57995F4BA4470F,
	XAttribute__ctor_m3EA3008A0D6C0B66C3868AAEB7EC7563DDD57C7B,
	XAttribute_get_IsNamespaceDeclaration_mE2D54BB132D0A554FA334BDAF92D5664E2BF50EA,
	XAttribute_get_Name_m89A65BCA2C1D25349F1B814F823208D897CA79FB,
	XAttribute_get_NodeType_m716AD291593A05B9829DEF1F8CB86A99C88FEE56,
	XAttribute_get_Value_m2458D145A9C0231EE71816040601199252F53A84,
	XAttribute_ToString_m93ACF1DA23DA530929D9EA831ED8A95D19042E21,
	XAttribute_op_Explicit_mBD277501C14D52654255A6E1C95FF59FD193578D,
	XAttribute_GetPrefixOfNamespace_mA63C230B534D3AE6BB47161C68D335D729FE8201,
	XAttribute_ValidateAttribute_m131F4A72A9F7E79F93B34C2F7909C80995B6B7B2,
	Res_GetString_mCACED687581128D64F4A26E6929E41E44DC17750,
	Res_GetString_mA952BE145DB45E41118302D24CD5FF7772CDE9F0,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_m627B7273A2428AFBCF4C41D574196135907A582D,
	ThrowStub_ThrowNotSupportedException_m1B65D6A1FF55272EEE13380F3E7C53E2015C3F44,
};
static const int32_t s_InvokerIndices[184] = 
{
	27,
	14,
	14,
	14,
	14,
	0,
	0,
	9,
	10,
	142,
	9,
	120,
	23,
	120,
	586,
	120,
	26,
	14,
	28,
	14,
	4,
	4,
	4,
	0,
	9,
	10,
	142,
	142,
	54,
	210,
	0,
	0,
	713,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	14,
	10,
	26,
	28,
	-1,
	89,
	10,
	10,
	89,
	26,
	138,
	10,
	26,
	138,
	138,
	23,
	14,
	26,
	26,
	14,
	43,
	34,
	26,
	26,
	10,
	14,
	26,
	26,
	14,
	26,
	26,
	10,
	26,
	14,
	23,
	26,
	28,
	14,
	26,
	26,
	26,
	26,
	23,
	1286,
	28,
	0,
	26,
	139,
	27,
	26,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	28,
	26,
	26,
	139,
	14,
	10,
	14,
	28,
	28,
	0,
	124,
	0,
	124,
	124,
	26,
	0,
	26,
	14,
	114,
	139,
	138,
	27,
	26,
	26,
	108,
	26,
	26,
	23,
	23,
	26,
	23,
	23,
	27,
	27,
	108,
	23,
	26,
	10,
	26,
	14,
	109,
	27,
	35,
	26,
	26,
	26,
	10,
	26,
	14,
	27,
	26,
	10,
	26,
	14,
	168,
	26,
	14,
	14,
	450,
	26,
	741,
	10,
	26,
	14,
	27,
	26,
	89,
	14,
	10,
	14,
	14,
	0,
	28,
	144,
	0,
	1,
	94,
	3,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x02000005, { 0, 5 } },
	{ 0x02000007, { 5, 8 } },
	{ 0x06000034, { 13, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[14] = 
{
	{ (Il2CppRGCTXDataType)2, 27486 },
	{ (Il2CppRGCTXDataType)3, 49113 },
	{ (Il2CppRGCTXDataType)3, 49114 },
	{ (Il2CppRGCTXDataType)3, 49115 },
	{ (Il2CppRGCTXDataType)3, 49116 },
	{ (Il2CppRGCTXDataType)2, 27487 },
	{ (Il2CppRGCTXDataType)3, 49117 },
	{ (Il2CppRGCTXDataType)2, 15028 },
	{ (Il2CppRGCTXDataType)3, 49118 },
	{ (Il2CppRGCTXDataType)3, 49119 },
	{ (Il2CppRGCTXDataType)3, 49120 },
	{ (Il2CppRGCTXDataType)2, 15028 },
	{ (Il2CppRGCTXDataType)3, 49121 },
	{ (Il2CppRGCTXDataType)2, 15038 },
};
extern const Il2CppCodeGenModule g_System_Xml_LinqCodeGenModule;
const Il2CppCodeGenModule g_System_Xml_LinqCodeGenModule = 
{
	"System.Xml.Linq.dll",
	184,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	14,
	s_rgctxValues,
	NULL,
};
