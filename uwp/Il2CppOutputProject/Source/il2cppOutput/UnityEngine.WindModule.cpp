﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.String
struct String_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WindZone
struct WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tB890EC30005024FE5547A5AE829747FBB7A29E45 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.WindZoneMode
struct  WindZoneMode_tD348ADF5596FF94F15EA4684312FEAFFA92B4D7C 
{
public:
	// System.Int32 UnityEngine.WindZoneMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WindZoneMode_tD348ADF5596FF94F15EA4684312FEAFFA92B4D7C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.WindZone
struct  WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.WindZone::set_mode(UnityEngine.WindZoneMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WindZone_set_mode_mAD558427D8DD6EFC77801F211A4263DEB7876583 (WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	typedef void (*WindZone_set_mode_mAD558427D8DD6EFC77801F211A4263DEB7876583_ftn) (WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071 *, int32_t);
	static WindZone_set_mode_mAD558427D8DD6EFC77801F211A4263DEB7876583_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WindZone_set_mode_mAD558427D8DD6EFC77801F211A4263DEB7876583_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WindZone::set_mode(UnityEngine.WindZoneMode)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WindZone::set_windMain(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WindZone_set_windMain_mB256E2DBD0453C3E1F08AB9C114FD4AE115F395C (WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*WindZone_set_windMain_mB256E2DBD0453C3E1F08AB9C114FD4AE115F395C_ftn) (WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071 *, float);
	static WindZone_set_windMain_mB256E2DBD0453C3E1F08AB9C114FD4AE115F395C_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WindZone_set_windMain_mB256E2DBD0453C3E1F08AB9C114FD4AE115F395C_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WindZone::set_windMain(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WindZone::set_windTurbulence(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WindZone_set_windTurbulence_m1072207BC214F82A5C3AADC490DFF82842A25BFD (WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*WindZone_set_windTurbulence_m1072207BC214F82A5C3AADC490DFF82842A25BFD_ftn) (WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071 *, float);
	static WindZone_set_windTurbulence_m1072207BC214F82A5C3AADC490DFF82842A25BFD_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WindZone_set_windTurbulence_m1072207BC214F82A5C3AADC490DFF82842A25BFD_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WindZone::set_windTurbulence(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WindZone::set_windPulseMagnitude(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WindZone_set_windPulseMagnitude_m1F9A4FAD633F3C659E3E8B616DDCD6C7D0D41522 (WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*WindZone_set_windPulseMagnitude_m1F9A4FAD633F3C659E3E8B616DDCD6C7D0D41522_ftn) (WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071 *, float);
	static WindZone_set_windPulseMagnitude_m1F9A4FAD633F3C659E3E8B616DDCD6C7D0D41522_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WindZone_set_windPulseMagnitude_m1F9A4FAD633F3C659E3E8B616DDCD6C7D0D41522_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WindZone::set_windPulseMagnitude(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
// System.Void UnityEngine.WindZone::set_windPulseFrequency(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WindZone_set_windPulseFrequency_m723B4C6610AA3D7042F3EA122D182B4669680A0E (WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071 * __this, float ___value0, const RuntimeMethod* method)
{
	typedef void (*WindZone_set_windPulseFrequency_m723B4C6610AA3D7042F3EA122D182B4669680A0E_ftn) (WindZone_tD91B0F7793F504C04B54E8595C0CD0E8CF9C9071 *, float);
	static WindZone_set_windPulseFrequency_m723B4C6610AA3D7042F3EA122D182B4669680A0E_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (WindZone_set_windPulseFrequency_m723B4C6610AA3D7042F3EA122D182B4669680A0E_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.WindZone::set_windPulseFrequency(System.Single)");
	_il2cpp_icall_func(__this, ___value0);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
