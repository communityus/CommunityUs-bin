﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void System.Runtime.Serialization.KnownTypeAttribute::.ctor(System.Type)
extern void KnownTypeAttribute__ctor_m4E4832EA5ACAD7B15D9F02CABB221E6F8713482A (void);
static Il2CppMethodPointer s_methodPointers[1] = 
{
	KnownTypeAttribute__ctor_m4E4832EA5ACAD7B15D9F02CABB221E6F8713482A,
};
static const int32_t s_InvokerIndices[1] = 
{
	26,
};
extern const Il2CppCodeGenModule g_System_Runtime_SerializationCodeGenModule;
const Il2CppCodeGenModule g_System_Runtime_SerializationCodeGenModule = 
{
	"System.Runtime.Serialization.dll",
	1,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
