﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.TerrainCollider::set_terrainData(UnityEngine.TerrainData)
extern void TerrainCollider_set_terrainData_m5B51B5EDFF840056CDD269F3B50D0E4E32332D03 (void);
static Il2CppMethodPointer s_methodPointers[1] = 
{
	TerrainCollider_set_terrainData_m5B51B5EDFF840056CDD269F3B50D0E4E32332D03,
};
static const int32_t s_InvokerIndices[1] = 
{
	26,
};
extern const Il2CppCodeGenModule g_UnityEngine_TerrainPhysicsModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_TerrainPhysicsModuleCodeGenModule = 
{
	"UnityEngine.TerrainPhysicsModule.dll",
	1,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
