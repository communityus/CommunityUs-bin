﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename R, typename T1>
struct VirtFuncInvoker1
{
	typedef R (*Func)(void*, T1, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename T1, typename T2, typename T3>
struct VirtActionInvoker3
{
	typedef void (*Action)(void*, T1, T2, T3, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1, T2 p2, T3 p3)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, p2, p3, invokeData.method);
	}
};

// DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager
struct AssetManager_tE8E95DFD100B293A6AE3DB69958923CA2CB629D1;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// DaggerfallWorkshop.AudioSynthesis.Util.Riff.Chunk
struct Chunk_t9103F9401FE26193B5FE338EF34D83156956415C;
// System.Text.Decoder
struct Decoder_t91B2ED8AEC25AA24D23A00265203BE992B12C370;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope
struct Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter
struct Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator
struct Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E;
// DaggerfallWorkshop.AudioSynthesis.Sf2.Generator
struct Generator_t130837BC1C1D35AD971F96F41B246FF13AE84D93;
// DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor
struct GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters
struct GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// DaggerfallWorkshop.AudioSynthesis.IResource
struct IResource_t36F23412321B19147F3BDFC11838E6182D61EA57;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo
struct Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent
struct MidiEvent_t0A4643C6A8CBCA997171ABC2EFEC507BA8C5AE9C;
// DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator
struct Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83;
// DaggerfallWorkshop.AudioSynthesis.Sf2.ModulatorType
struct ModulatorType_t1C1EA8C665813F52ABF1788447771CD0C74A2C9B;
// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch
struct MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F;
// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch
struct Patch_t3144A9674F069F684A9B015803D7A714F366C7AD;
// DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank
struct PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SawGenerator
struct SawGenerator_t98C5A484F9F525263F1AF4793C9B77DD4F909552;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SineGenerator
struct SineGenerator_t271E0AB6E9F316CE63CFF199A1FBCBB8D4F06DCF;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SquareGenerator
struct SquareGenerator_t5F29A8328021A4242A7FCE442A16291C7442A86B;
// System.IO.Stream
struct Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters
struct SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer
struct Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB;
// DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemCommonEvent
struct SystemCommonEvent_t9ED10AE0931452F227792401D07F5DD1645A63CC;
// DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemExclusiveEvent
struct SystemExclusiveEvent_tC8271D73FDE64FB3488EAC4F9C891831E661A79A;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.TriangleGenerator
struct TriangleGenerator_tA00D6605C3FB1CD04F819ED1F1A2CBE3EC99F8E6;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice
struct Voice_tA46F178E48B0D771712A0AA48175620981094D47;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager
struct VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters
struct VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.WhiteNoiseGenerator
struct WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA;
// DaggerfallWorkshop.AudioSynthesis.Sf2.Zone
struct Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54;
// DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk
struct ZoneChunk_t894317B0A9F9371149E84A5F5D3AC09B86524F5A;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope/EnvelopeStage
struct EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3;
// DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.InstrumentChunk/RawInstrument
struct RawInstrument_tE571AEC155C68A7AC59C381739A8EC772671ED30;
// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch/PatchInterval
struct PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835;
// DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk/RawPreset
struct RawPreset_tA6795EFE241E18A1A886023CF5E17AE4ADF6A309;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode
struct VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E;
// DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk/RawZoneData
struct RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682;
// System.Collections.Generic.Dictionary`2<System.Int32,DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch[]>
struct Dictionary_2_t16D2CC739C7F5863A12AF7098E147C489B051CFE;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t6BC8BF32EFF6FF794B125939AD0F300DAAE19F85;
// System.Collections.Generic.IEnumerable`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>
struct IEnumerable_1_t8C8485A662D8FFF5A05615A5F10AB7C8500E28FB;
// System.Collections.Generic.IEnumerable`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode>
struct IEnumerable_1_t5BDBA8B1D25CBC574CF5A6A65E2437E12CA10F49;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.LinkedListNode`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>
struct LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F;
// System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>
struct LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B;
// System.Collections.Generic.Queue`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage>
struct Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965;
// System.Collections.Generic.Stack`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode>
struct Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8;
// System.Collections.Generic.Stack`1<System.Object>
struct Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981;
// System.ArgumentException
struct ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00;
// System.ArgumentNullException
struct ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB;
// System.IO.BinaryReader
struct BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128;
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Double[]
struct DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope/EnvelopeStage[]
struct EnvelopeStageU5BU5D_t5A737392361DF2F93226B588DAD7E2494C9C936B;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope[]
struct EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81;
// System.Exception
struct Exception_t;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter[]
struct FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters[]
struct GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A;
// DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[]
struct GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE;
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo[]
struct LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage[]
struct MidiMessageU5BU5D_t048A9411CBDF5DA1EC82A13311FD80536CD689A1;
// DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[]
struct ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch/PatchInterval[]
struct PatchIntervalU5BU5D_t30C33B6114206178CEB1742AC87864000E1CA7A5;
// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch[]
struct PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75;
// System.Random
struct Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118;
// DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk/RawZoneData[]
struct RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1;
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
// System.Single[][]
struct SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// System.String
struct String_t;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters[]
struct SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.UnionData[]
struct UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode[0...,0...]
struct VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode[]
struct VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665;
// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice[]
struct VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A;
// DaggerfallWorkshop.AudioSynthesis.Sf2.Zone[]
struct ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5;

IL2CPP_EXTERN_C RuntimeClass* ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Exception_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Type_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VoiceStateEnum_tE420EB9CD65647BED146DE806F4977C97B9BCD8E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Voice_tA46F178E48B0D771712A0AA48175620981094D47_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral04B1938F0F2796858CDB689F91CE3644BC836103;
IL2CPP_EXTERN_C String_t* _stringLiteral1AB514A8EA2B388C6E60F1CC62C6D7C61F45CA3D;
IL2CPP_EXTERN_C String_t* _stringLiteral59A7C853558F85DED57BAB572BA391C4F22417B3;
IL2CPP_EXTERN_C String_t* _stringLiteral5BEFD8CC60A79699B5BB00E37BAC5B62D371E174;
IL2CPP_EXTERN_C String_t* _stringLiteral609FA33CCE34CF9B337A270AFB44DC4BA1ED1C4F;
IL2CPP_EXTERN_C String_t* _stringLiteral67EF81B40230A7823C595B3E224C3D4212075F4C;
IL2CPP_EXTERN_C String_t* _stringLiteral6F9D4D7102B03DC5B3DF641B1F99444EF5B4F50F;
IL2CPP_EXTERN_C String_t* _stringLiteral8239DDE7DBC91495DACC42B52FAF15E9C617D4D7;
IL2CPP_EXTERN_C String_t* _stringLiteralA06B43CA30AE28998E1D4DD043AA4381F1EDC6E8;
IL2CPP_EXTERN_C String_t* _stringLiteralAEBE033E3176253CF068FAA15A2107F8903D8FDE;
IL2CPP_EXTERN_C String_t* _stringLiteralDFFFD27E2BB06614153E94D05DE45D16CB3C4D04;
IL2CPP_EXTERN_C String_t* _stringLiteralEF762113927B6A6A393880C8C84F932028DF315E;
IL2CPP_EXTERN_C String_t* _stringLiteralFB75270F0EDF5598A9A6D6104F21E5620D1ABBEF;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_Dispose_m290DF4EA35F9E4F1D910D42A42F4334930DABAA5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_MoveNext_mB7075ADE4B6DB1460DF2B652EE538430B536077B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerator_get_Current_m15363A9312B73B1AD3E358A70EBB8BDF280DE3E5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_AddFirst_m8B3FCFCACB01A06666CB311754F101AA005C505F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_AddLast_m9358ECB9018B698E73EF40B3E5A61C65339835CA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_RemoveFirst_m5BB3D19F10EFA916947A8A64808BB81F065FFA0B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_Remove_mECC8FD43A15126D559FFF8E2DDC62A1B6F27FADD_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1__ctor_m1A0E7433ED02FAF1B17980448DC31395B6DFDC7F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1__ctor_m90B1CFAE4273CB108436915F74E71A1650440B74_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_get_Count_m583DD5509171DBFF46F108B1FE6439956AA7E1D8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1_Dequeue_m4313D1B2E96A63B32AAEB715CBD1940EEC4F692F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1__ctor_m496CB994FA0D02F62AA06313AA8C460AD550F03E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Queue_1_get_Count_m6331507132D178B8517E38C3F2F0C1698AEC49D1_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Stack_1_GetEnumerator_m9110C85EAA6292636F99B48A549DE02B81ABE62A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Stack_1_Pop_m7119358526FF3EE669090369C7C813EBEF1B4563_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Stack_1_Push_m98F98B3188600DDF0EFBCBC85110CD878E52E155_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Stack_1__ctor_m1B694765A8AEC57CEF3638BAFAC8C7A68961D466_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Synthesizer_LoadBank_mB0296667D3FDB5AB407923A080E4A6C0B9A5D783_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Synthesizer__ctor_m5B2E9379382A890BBEA8FE575F2B4F01A8B83408_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* ZoneChunk__ctor_m4A647A21B1CB9F0430922669AF13D0A66539C513_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeType* SystemCommonTypeEnum_t8CD91F9148AD22D4C0B78296D4A1B7F233493B7D_0_0_0_var;
IL2CPP_EXTERN_C const uint32_t PatchInterval_ToString_m9E72F8C7309019A9B214AC620B39E94B8F5EAF9D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SquareGenerator_GetValue_mC0B58C49002D6C70197F1441E663A26ADB156F5D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SquareGenerator_GetValues_m917D82D8B7EC0FC6DCFB2100C07EC85602F63081_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SquareGenerator__ctor_mA92ED26FD608B9BCB0ABB3C7301956A490E75761_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SynthHelper_CentsToPitch_mDD57BCD740AA6583A7A5DB8F690AE9B03AE7CAB8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SynthHelper_DBtoLinear_m224252A9D521905D87410CE27FA28E548613E53D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SynthHelper_KeyToFrequency_m82BB6EF80E9FBE00A0FD57B67E794D9B01E6C9A4_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SynthParameters_UpdateCurrentPan_m08069E906C15598A7B038FB984A6D5D56B593090_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Synthesizer_FillWorkingBuffer_m53938F9A39846C0E6EC7A76C7A1DF39ABC640974_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Synthesizer_LoadBank_m5A866814011F2BC24C0AAD5F3E6BF71FD7D40580_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Synthesizer_LoadBank_mB0296667D3FDB5AB407923A080E4A6C0B9A5D783_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Synthesizer_NoteOffAll_m09B0E9EA1805A76EDADE4E4E0443AFD9AB7FAA88_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Synthesizer_NoteOn_m04E2EFEF32FD9B4E13480059E411E37F26FD5772_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Synthesizer_ReleaseAllHoldPedals_mAA8BF558958F700C063299A6DDC2D27B38F16239_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Synthesizer_ReleaseHoldPedal_m8DD7E693144C996CC0EC5451B000B5F5C6203029_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Synthesizer__cctor_m33EB9B17280769B6A4DD40FC6C6F28BE0E8A8CF2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Synthesizer__ctor_m5B2E9379382A890BBEA8FE575F2B4F01A8B83408_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SystemCommonEvent_ToString_m8DB3E6A0EDBB51469405DF4B6CE965AE75EE2CD5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Tables_CreateCentTable_mDA1EEA30489EB4C540F4F76AE4D8D946138D6F45_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Tables_CreateConcaveTable_m8DA88814E45FAB3F4ECCCA237BDEDB9AE8A68EC9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Tables_CreateConvexTable_m805C0608C71A722855983AFAEF2DEA8F689C5590_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Tables_CreateLinearTable_mE8A52196591057548DDA3DE71766B8E8423D5427_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Tables_CreateSemitoneTable_m8A0B5078A2F897C4D3C6A3538248C6F0CF41C072_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Tables_CreateSustainTable_mAFC5D677E0BB7E15AD4E03BC43A0AB78C9F65BF0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Tables_RemoveDenormals_mC91B0A68C891D1245952E94A24A2E79A61F32BC1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Tables__cctor_mCCC5D545C915A3798B31D354AE06708823EDCB6A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TriangleGenerator_GetValue_mAE8FFCF40053D2C226AA07F9091268DD787BCCAB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TriangleGenerator_GetValues_mD60A13DC9FC0A17E9E83426BD83E3047A15E6470_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TriangleGenerator__ctor_m1922B9805C5F5B03370052B44309CC04E2C62743_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoiceManager_AddToRegistry_m2055B93BCC9715FEE1E9D8D2C5E412F2EAD416F6_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoiceManager_ClearRegistry_mE7A9A5542422604EFAE47D0D4AF3B4D0A2F7B096_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoiceManager_GetFreeVoice_mAA1AE9A540D670DCE54385FBE800AB099AEB3BF5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoiceManager_RemoveFromRegistry_mA5518367D8A75DE5B022440CE4B55CB75E502DE3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoiceManager_StealOldest_m2DC42C690A28F3065FD16CB8E11DF2FCEC9EE24B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoiceManager_StealQuietestVoice_m0B1D16AD611B24B483268771F1D4D0F19AA0F826_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoiceManager_UnloadPatches_mE8C788BCE71CD06511BDD72292BA1339E7CB27D2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoiceManager__ctor_mF7DAE448488693FE2E641B7351FE9BC4A49850DF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoiceParameters_ToString_mD01B9CC55062092042898C61E8827A72819ECEEF_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t VoiceParameters__ctor_m7778D2A8080260881ACA92965AC75360900DF955_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Voice_ToString_mE732283D32E218AF05F567D5EF879665199EB580_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Voice__ctor_mC8F53C63FE90B4AC4517F3FB27EF8E2CF73ABA28_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WaveHelper_GetChannelPcmData_m5EB36FDDF8E6ACF093946271CBD782402302FFD5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WaveHelper_SwapEndianess_mB6A4A015BA00E24FB9527DD87B2AA23FB10AB9AC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhiteNoiseGenerator_GetValue_m444D451C4A8B7B12460EF17BCC2E6FABC9C1B49C_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhiteNoiseGenerator_GetValues_m7ABC9C1794FADE89766A90495F6B75598FBD9316_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhiteNoiseGenerator__cctor_m2692BF76CDB9FA3654951A44D17441BC49B11655_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t WhiteNoiseGenerator__ctor_mFA126753A3BAA805CA77B1B93A962EAF9B61A8D3_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZoneChunk_ToZones_m24160C8D66DBB5B4D8322607AE687113B21EB205_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t ZoneChunk__ctor_m4A647A21B1CB9F0430922669AF13D0A66539C513_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Zone_ToString_m136116DA43FF3C8E3CB7CA0E9517D84815B05EAF_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81;
struct FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1;
struct GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A;
struct LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52;
struct PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75;
struct RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E;
struct GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE;
struct ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7;
struct ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5;
struct SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE;
struct UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F;
struct VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB;
struct VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665;
struct VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A;
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726;
struct DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB;
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA;
struct SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope_EnvelopeStage
struct  EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3  : public RuntimeObject
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope_EnvelopeStage::time
	int32_t ___time_0;
	// System.Single[] DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope_EnvelopeStage::graph
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___graph_1;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope_EnvelopeStage::scale
	float ___scale_2;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope_EnvelopeStage::offset
	float ___offset_3;
	// System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope_EnvelopeStage::reverse
	bool ___reverse_4;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3, ___time_0)); }
	inline int32_t get_time_0() const { return ___time_0; }
	inline int32_t* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(int32_t value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_graph_1() { return static_cast<int32_t>(offsetof(EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3, ___graph_1)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_graph_1() const { return ___graph_1; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_graph_1() { return &___graph_1; }
	inline void set_graph_1(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___graph_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___graph_1), (void*)value);
	}

	inline static int32_t get_offset_of_scale_2() { return static_cast<int32_t>(offsetof(EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3, ___scale_2)); }
	inline float get_scale_2() const { return ___scale_2; }
	inline float* get_address_of_scale_2() { return &___scale_2; }
	inline void set_scale_2(float value)
	{
		___scale_2 = value;
	}

	inline static int32_t get_offset_of_offset_3() { return static_cast<int32_t>(offsetof(EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3, ___offset_3)); }
	inline float get_offset_3() const { return ___offset_3; }
	inline float* get_address_of_offset_3() { return &___offset_3; }
	inline void set_offset_3(float value)
	{
		___offset_3 = value;
	}

	inline static int32_t get_offset_of_reverse_4() { return static_cast<int32_t>(offsetof(EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3, ___reverse_4)); }
	inline bool get_reverse_4() const { return ___reverse_4; }
	inline bool* get_address_of_reverse_4() { return &___reverse_4; }
	inline void set_reverse_4(bool value)
	{
		___reverse_4 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank
struct  PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Int32,DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch[]> DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::bank
	Dictionary_2_t16D2CC739C7F5863A12AF7098E147C489B051CFE * ___bank_4;
	// DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::assets
	AssetManager_tE8E95DFD100B293A6AE3DB69958923CA2CB629D1 * ___assets_5;
	// System.String DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::bankName
	String_t* ___bankName_6;
	// System.String DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::comment
	String_t* ___comment_7;

public:
	inline static int32_t get_offset_of_bank_4() { return static_cast<int32_t>(offsetof(PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70, ___bank_4)); }
	inline Dictionary_2_t16D2CC739C7F5863A12AF7098E147C489B051CFE * get_bank_4() const { return ___bank_4; }
	inline Dictionary_2_t16D2CC739C7F5863A12AF7098E147C489B051CFE ** get_address_of_bank_4() { return &___bank_4; }
	inline void set_bank_4(Dictionary_2_t16D2CC739C7F5863A12AF7098E147C489B051CFE * value)
	{
		___bank_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bank_4), (void*)value);
	}

	inline static int32_t get_offset_of_assets_5() { return static_cast<int32_t>(offsetof(PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70, ___assets_5)); }
	inline AssetManager_tE8E95DFD100B293A6AE3DB69958923CA2CB629D1 * get_assets_5() const { return ___assets_5; }
	inline AssetManager_tE8E95DFD100B293A6AE3DB69958923CA2CB629D1 ** get_address_of_assets_5() { return &___assets_5; }
	inline void set_assets_5(AssetManager_tE8E95DFD100B293A6AE3DB69958923CA2CB629D1 * value)
	{
		___assets_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___assets_5), (void*)value);
	}

	inline static int32_t get_offset_of_bankName_6() { return static_cast<int32_t>(offsetof(PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70, ___bankName_6)); }
	inline String_t* get_bankName_6() const { return ___bankName_6; }
	inline String_t** get_address_of_bankName_6() { return &___bankName_6; }
	inline void set_bankName_6(String_t* value)
	{
		___bankName_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bankName_6), (void*)value);
	}

	inline static int32_t get_offset_of_comment_7() { return static_cast<int32_t>(offsetof(PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70, ___comment_7)); }
	inline String_t* get_comment_7() const { return ___comment_7; }
	inline String_t** get_address_of_comment_7() { return &___comment_7; }
	inline void set_comment_7(String_t* value)
	{
		___comment_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comment_7), (void*)value);
	}
};

struct PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::patchTypes
	Dictionary_2_t6BC8BF32EFF6FF794B125939AD0F300DAAE19F85 * ___patchTypes_3;

public:
	inline static int32_t get_offset_of_patchTypes_3() { return static_cast<int32_t>(offsetof(PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70_StaticFields, ___patchTypes_3)); }
	inline Dictionary_2_t6BC8BF32EFF6FF794B125939AD0F300DAAE19F85 * get_patchTypes_3() const { return ___patchTypes_3; }
	inline Dictionary_2_t6BC8BF32EFF6FF794B125939AD0F300DAAE19F85 ** get_address_of_patchTypes_3() { return &___patchTypes_3; }
	inline void set_patchTypes_3(Dictionary_2_t6BC8BF32EFF6FF794B125939AD0F300DAAE19F85 * value)
	{
		___patchTypes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___patchTypes_3), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval
struct  PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::patch
	Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * ___patch_0;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::startChannel
	uint8_t ___startChannel_1;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::startKey
	uint8_t ___startKey_2;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::startVelocity
	uint8_t ___startVelocity_3;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::endChannel
	uint8_t ___endChannel_4;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::endKey
	uint8_t ___endKey_5;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::endVelocity
	uint8_t ___endVelocity_6;

public:
	inline static int32_t get_offset_of_patch_0() { return static_cast<int32_t>(offsetof(PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835, ___patch_0)); }
	inline Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * get_patch_0() const { return ___patch_0; }
	inline Patch_t3144A9674F069F684A9B015803D7A714F366C7AD ** get_address_of_patch_0() { return &___patch_0; }
	inline void set_patch_0(Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * value)
	{
		___patch_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___patch_0), (void*)value);
	}

	inline static int32_t get_offset_of_startChannel_1() { return static_cast<int32_t>(offsetof(PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835, ___startChannel_1)); }
	inline uint8_t get_startChannel_1() const { return ___startChannel_1; }
	inline uint8_t* get_address_of_startChannel_1() { return &___startChannel_1; }
	inline void set_startChannel_1(uint8_t value)
	{
		___startChannel_1 = value;
	}

	inline static int32_t get_offset_of_startKey_2() { return static_cast<int32_t>(offsetof(PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835, ___startKey_2)); }
	inline uint8_t get_startKey_2() const { return ___startKey_2; }
	inline uint8_t* get_address_of_startKey_2() { return &___startKey_2; }
	inline void set_startKey_2(uint8_t value)
	{
		___startKey_2 = value;
	}

	inline static int32_t get_offset_of_startVelocity_3() { return static_cast<int32_t>(offsetof(PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835, ___startVelocity_3)); }
	inline uint8_t get_startVelocity_3() const { return ___startVelocity_3; }
	inline uint8_t* get_address_of_startVelocity_3() { return &___startVelocity_3; }
	inline void set_startVelocity_3(uint8_t value)
	{
		___startVelocity_3 = value;
	}

	inline static int32_t get_offset_of_endChannel_4() { return static_cast<int32_t>(offsetof(PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835, ___endChannel_4)); }
	inline uint8_t get_endChannel_4() const { return ___endChannel_4; }
	inline uint8_t* get_address_of_endChannel_4() { return &___endChannel_4; }
	inline void set_endChannel_4(uint8_t value)
	{
		___endChannel_4 = value;
	}

	inline static int32_t get_offset_of_endKey_5() { return static_cast<int32_t>(offsetof(PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835, ___endKey_5)); }
	inline uint8_t get_endKey_5() const { return ___endKey_5; }
	inline uint8_t* get_address_of_endKey_5() { return &___endKey_5; }
	inline void set_endKey_5(uint8_t value)
	{
		___endKey_5 = value;
	}

	inline static int32_t get_offset_of_endVelocity_6() { return static_cast<int32_t>(offsetof(PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835, ___endVelocity_6)); }
	inline uint8_t get_endVelocity_6() const { return ___endVelocity_6; }
	inline uint8_t* get_address_of_endVelocity_6() { return &___endVelocity_6; }
	inline void set_endVelocity_6(uint8_t value)
	{
		___endVelocity_6 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch
struct  Patch_t3144A9674F069F684A9B015803D7A714F366C7AD  : public RuntimeObject
{
public:
	// System.String DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::patchName
	String_t* ___patchName_0;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::exTarget
	int32_t ___exTarget_1;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::exGroup
	int32_t ___exGroup_2;

public:
	inline static int32_t get_offset_of_patchName_0() { return static_cast<int32_t>(offsetof(Patch_t3144A9674F069F684A9B015803D7A714F366C7AD, ___patchName_0)); }
	inline String_t* get_patchName_0() const { return ___patchName_0; }
	inline String_t** get_address_of_patchName_0() { return &___patchName_0; }
	inline void set_patchName_0(String_t* value)
	{
		___patchName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___patchName_0), (void*)value);
	}

	inline static int32_t get_offset_of_exTarget_1() { return static_cast<int32_t>(offsetof(Patch_t3144A9674F069F684A9B015803D7A714F366C7AD, ___exTarget_1)); }
	inline int32_t get_exTarget_1() const { return ___exTarget_1; }
	inline int32_t* get_address_of_exTarget_1() { return &___exTarget_1; }
	inline void set_exTarget_1(int32_t value)
	{
		___exTarget_1 = value;
	}

	inline static int32_t get_offset_of_exGroup_2() { return static_cast<int32_t>(offsetof(Patch_t3144A9674F069F684A9B015803D7A714F366C7AD, ___exGroup_2)); }
	inline int32_t get_exGroup_2() const { return ___exGroup_2; }
	inline int32_t* get_address_of_exGroup_2() { return &___exGroup_2; }
	inline void set_exGroup_2(int32_t value)
	{
		___exGroup_2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent
struct  MidiEvent_t0A4643C6A8CBCA997171ABC2EFEC507BA8C5AE9C  : public RuntimeObject
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::time
	int32_t ___time_0;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::message
	int32_t ___message_1;

public:
	inline static int32_t get_offset_of_time_0() { return static_cast<int32_t>(offsetof(MidiEvent_t0A4643C6A8CBCA997171ABC2EFEC507BA8C5AE9C, ___time_0)); }
	inline int32_t get_time_0() const { return ___time_0; }
	inline int32_t* get_address_of_time_0() { return &___time_0; }
	inline void set_time_0(int32_t value)
	{
		___time_0 = value;
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(MidiEvent_t0A4643C6A8CBCA997171ABC2EFEC507BA8C5AE9C, ___message_1)); }
	inline int32_t get_message_1() const { return ___message_1; }
	inline int32_t* get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(int32_t value)
	{
		___message_1 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.InstrumentChunk_RawInstrument
struct  RawInstrument_tE571AEC155C68A7AC59C381739A8EC772671ED30  : public RuntimeObject
{
public:
	// System.String DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.InstrumentChunk_RawInstrument::name
	String_t* ___name_0;
	// System.UInt16 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.InstrumentChunk_RawInstrument::startInstrumentZoneIndex
	uint16_t ___startInstrumentZoneIndex_1;
	// System.UInt16 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.InstrumentChunk_RawInstrument::endInstrumentZoneIndex
	uint16_t ___endInstrumentZoneIndex_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(RawInstrument_tE571AEC155C68A7AC59C381739A8EC772671ED30, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_startInstrumentZoneIndex_1() { return static_cast<int32_t>(offsetof(RawInstrument_tE571AEC155C68A7AC59C381739A8EC772671ED30, ___startInstrumentZoneIndex_1)); }
	inline uint16_t get_startInstrumentZoneIndex_1() const { return ___startInstrumentZoneIndex_1; }
	inline uint16_t* get_address_of_startInstrumentZoneIndex_1() { return &___startInstrumentZoneIndex_1; }
	inline void set_startInstrumentZoneIndex_1(uint16_t value)
	{
		___startInstrumentZoneIndex_1 = value;
	}

	inline static int32_t get_offset_of_endInstrumentZoneIndex_2() { return static_cast<int32_t>(offsetof(RawInstrument_tE571AEC155C68A7AC59C381739A8EC772671ED30, ___endInstrumentZoneIndex_2)); }
	inline uint16_t get_endInstrumentZoneIndex_2() const { return ___endInstrumentZoneIndex_2; }
	inline uint16_t* get_address_of_endInstrumentZoneIndex_2() { return &___endInstrumentZoneIndex_2; }
	inline void set_endInstrumentZoneIndex_2(uint16_t value)
	{
		___endInstrumentZoneIndex_2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk_RawPreset
struct  RawPreset_tA6795EFE241E18A1A886023CF5E17AE4ADF6A309  : public RuntimeObject
{
public:
	// System.String DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk_RawPreset::name
	String_t* ___name_0;
	// System.UInt16 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk_RawPreset::patchNumber
	uint16_t ___patchNumber_1;
	// System.UInt16 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk_RawPreset::bankNumber
	uint16_t ___bankNumber_2;
	// System.UInt16 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk_RawPreset::startPresetZoneIndex
	uint16_t ___startPresetZoneIndex_3;
	// System.UInt16 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk_RawPreset::endPresetZoneIndex
	uint16_t ___endPresetZoneIndex_4;
	// System.UInt32 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk_RawPreset::library
	uint32_t ___library_5;
	// System.UInt32 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk_RawPreset::genre
	uint32_t ___genre_6;
	// System.UInt32 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk_RawPreset::morphology
	uint32_t ___morphology_7;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(RawPreset_tA6795EFE241E18A1A886023CF5E17AE4ADF6A309, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___name_0), (void*)value);
	}

	inline static int32_t get_offset_of_patchNumber_1() { return static_cast<int32_t>(offsetof(RawPreset_tA6795EFE241E18A1A886023CF5E17AE4ADF6A309, ___patchNumber_1)); }
	inline uint16_t get_patchNumber_1() const { return ___patchNumber_1; }
	inline uint16_t* get_address_of_patchNumber_1() { return &___patchNumber_1; }
	inline void set_patchNumber_1(uint16_t value)
	{
		___patchNumber_1 = value;
	}

	inline static int32_t get_offset_of_bankNumber_2() { return static_cast<int32_t>(offsetof(RawPreset_tA6795EFE241E18A1A886023CF5E17AE4ADF6A309, ___bankNumber_2)); }
	inline uint16_t get_bankNumber_2() const { return ___bankNumber_2; }
	inline uint16_t* get_address_of_bankNumber_2() { return &___bankNumber_2; }
	inline void set_bankNumber_2(uint16_t value)
	{
		___bankNumber_2 = value;
	}

	inline static int32_t get_offset_of_startPresetZoneIndex_3() { return static_cast<int32_t>(offsetof(RawPreset_tA6795EFE241E18A1A886023CF5E17AE4ADF6A309, ___startPresetZoneIndex_3)); }
	inline uint16_t get_startPresetZoneIndex_3() const { return ___startPresetZoneIndex_3; }
	inline uint16_t* get_address_of_startPresetZoneIndex_3() { return &___startPresetZoneIndex_3; }
	inline void set_startPresetZoneIndex_3(uint16_t value)
	{
		___startPresetZoneIndex_3 = value;
	}

	inline static int32_t get_offset_of_endPresetZoneIndex_4() { return static_cast<int32_t>(offsetof(RawPreset_tA6795EFE241E18A1A886023CF5E17AE4ADF6A309, ___endPresetZoneIndex_4)); }
	inline uint16_t get_endPresetZoneIndex_4() const { return ___endPresetZoneIndex_4; }
	inline uint16_t* get_address_of_endPresetZoneIndex_4() { return &___endPresetZoneIndex_4; }
	inline void set_endPresetZoneIndex_4(uint16_t value)
	{
		___endPresetZoneIndex_4 = value;
	}

	inline static int32_t get_offset_of_library_5() { return static_cast<int32_t>(offsetof(RawPreset_tA6795EFE241E18A1A886023CF5E17AE4ADF6A309, ___library_5)); }
	inline uint32_t get_library_5() const { return ___library_5; }
	inline uint32_t* get_address_of_library_5() { return &___library_5; }
	inline void set_library_5(uint32_t value)
	{
		___library_5 = value;
	}

	inline static int32_t get_offset_of_genre_6() { return static_cast<int32_t>(offsetof(RawPreset_tA6795EFE241E18A1A886023CF5E17AE4ADF6A309, ___genre_6)); }
	inline uint32_t get_genre_6() const { return ___genre_6; }
	inline uint32_t* get_address_of_genre_6() { return &___genre_6; }
	inline void set_genre_6(uint32_t value)
	{
		___genre_6 = value;
	}

	inline static int32_t get_offset_of_morphology_7() { return static_cast<int32_t>(offsetof(RawPreset_tA6795EFE241E18A1A886023CF5E17AE4ADF6A309, ___morphology_7)); }
	inline uint32_t get_morphology_7() const { return ___morphology_7; }
	inline uint32_t* get_address_of_morphology_7() { return &___morphology_7; }
	inline void set_morphology_7(uint32_t value)
	{
		___morphology_7 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk_RawZoneData
struct  RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682  : public RuntimeObject
{
public:
	// System.UInt16 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk_RawZoneData::generatorIndex
	uint16_t ___generatorIndex_0;
	// System.UInt16 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk_RawZoneData::modulatorIndex
	uint16_t ___modulatorIndex_1;
	// System.UInt16 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk_RawZoneData::generatorCount
	uint16_t ___generatorCount_2;
	// System.UInt16 DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk_RawZoneData::modulatorCount
	uint16_t ___modulatorCount_3;

public:
	inline static int32_t get_offset_of_generatorIndex_0() { return static_cast<int32_t>(offsetof(RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682, ___generatorIndex_0)); }
	inline uint16_t get_generatorIndex_0() const { return ___generatorIndex_0; }
	inline uint16_t* get_address_of_generatorIndex_0() { return &___generatorIndex_0; }
	inline void set_generatorIndex_0(uint16_t value)
	{
		___generatorIndex_0 = value;
	}

	inline static int32_t get_offset_of_modulatorIndex_1() { return static_cast<int32_t>(offsetof(RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682, ___modulatorIndex_1)); }
	inline uint16_t get_modulatorIndex_1() const { return ___modulatorIndex_1; }
	inline uint16_t* get_address_of_modulatorIndex_1() { return &___modulatorIndex_1; }
	inline void set_modulatorIndex_1(uint16_t value)
	{
		___modulatorIndex_1 = value;
	}

	inline static int32_t get_offset_of_generatorCount_2() { return static_cast<int32_t>(offsetof(RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682, ___generatorCount_2)); }
	inline uint16_t get_generatorCount_2() const { return ___generatorCount_2; }
	inline uint16_t* get_address_of_generatorCount_2() { return &___generatorCount_2; }
	inline void set_generatorCount_2(uint16_t value)
	{
		___generatorCount_2 = value;
	}

	inline static int32_t get_offset_of_modulatorCount_3() { return static_cast<int32_t>(offsetof(RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682, ___modulatorCount_3)); }
	inline uint16_t get_modulatorCount_3() const { return ___modulatorCount_3; }
	inline uint16_t* get_address_of_modulatorCount_3() { return &___modulatorCount_3; }
	inline void set_modulatorCount_3(uint16_t value)
	{
		___modulatorCount_3 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Sf2.Zone
struct  Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[] DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::modulators
	ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* ___modulators_0;
	// DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[] DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::generators
	GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* ___generators_1;

public:
	inline static int32_t get_offset_of_modulators_0() { return static_cast<int32_t>(offsetof(Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54, ___modulators_0)); }
	inline ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* get_modulators_0() const { return ___modulators_0; }
	inline ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7** get_address_of_modulators_0() { return &___modulators_0; }
	inline void set_modulators_0(ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* value)
	{
		___modulators_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___modulators_0), (void*)value);
	}

	inline static int32_t get_offset_of_generators_1() { return static_cast<int32_t>(offsetof(Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54, ___generators_1)); }
	inline GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* get_generators_1() const { return ___generators_1; }
	inline GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE** get_address_of_generators_1() { return &___generators_1; }
	inline void set_generators_1(GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* value)
	{
		___generators_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___generators_1), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper
struct  SynthHelper_tE0A5A8D8D7A96094EDD1E62FC5B57B657A98CD11  : public RuntimeObject
{
public:

public:
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice
struct  Voice_tA46F178E48B0D771712A0AA48175620981094D47  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::patch
	Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * ___patch_0;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::voiceparams
	VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * ___voiceparams_1;

public:
	inline static int32_t get_offset_of_patch_0() { return static_cast<int32_t>(offsetof(Voice_tA46F178E48B0D771712A0AA48175620981094D47, ___patch_0)); }
	inline Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * get_patch_0() const { return ___patch_0; }
	inline Patch_t3144A9674F069F684A9B015803D7A714F366C7AD ** get_address_of_patch_0() { return &___patch_0; }
	inline void set_patch_0(Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * value)
	{
		___patch_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___patch_0), (void*)value);
	}

	inline static int32_t get_offset_of_voiceparams_1() { return static_cast<int32_t>(offsetof(Voice_tA46F178E48B0D771712A0AA48175620981094D47, ___voiceparams_1)); }
	inline VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * get_voiceparams_1() const { return ___voiceparams_1; }
	inline VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 ** get_address_of_voiceparams_1() { return &___voiceparams_1; }
	inline void set_voiceparams_1(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * value)
	{
		___voiceparams_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___voiceparams_1), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode
struct  VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode::Value
	Voice_tA46F178E48B0D771712A0AA48175620981094D47 * ___Value_0;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode::Next
	VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * ___Next_1;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E, ___Value_0)); }
	inline Voice_tA46F178E48B0D771712A0AA48175620981094D47 * get_Value_0() const { return ___Value_0; }
	inline Voice_tA46F178E48B0D771712A0AA48175620981094D47 ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(Voice_tA46F178E48B0D771712A0AA48175620981094D47 * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Value_0), (void*)value);
	}

	inline static int32_t get_offset_of_Next_1() { return static_cast<int32_t>(offsetof(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E, ___Next_1)); }
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * get_Next_1() const { return ___Next_1; }
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E ** get_address_of_Next_1() { return &___Next_1; }
	inline void set_Next_1(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * value)
	{
		___Next_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Next_1), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Util.Riff.Chunk
struct  Chunk_t9103F9401FE26193B5FE338EF34D83156956415C  : public RuntimeObject
{
public:
	// System.String DaggerfallWorkshop.AudioSynthesis.Util.Riff.Chunk::id
	String_t* ___id_0;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Util.Riff.Chunk::size
	int32_t ___size_1;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(Chunk_t9103F9401FE26193B5FE338EF34D83156956415C, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___id_0), (void*)value);
	}

	inline static int32_t get_offset_of_size_1() { return static_cast<int32_t>(offsetof(Chunk_t9103F9401FE26193B5FE338EF34D83156956415C, ___size_1)); }
	inline int32_t get_size_1() const { return ___size_1; }
	inline int32_t* get_address_of_size_1() { return &___size_1; }
	inline void set_size_1(int32_t value)
	{
		___size_1 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Util.Tables
struct  Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699  : public RuntimeObject
{
public:

public:
};

struct Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields
{
public:
	// System.Single[][] DaggerfallWorkshop.AudioSynthesis.Util.Tables::EnvelopeTables
	SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9* ___EnvelopeTables_0;
	// System.Double[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::SemitoneTable
	DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* ___SemitoneTable_1;
	// System.Double[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CentTable
	DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* ___CentTable_2;

public:
	inline static int32_t get_offset_of_EnvelopeTables_0() { return static_cast<int32_t>(offsetof(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields, ___EnvelopeTables_0)); }
	inline SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9* get_EnvelopeTables_0() const { return ___EnvelopeTables_0; }
	inline SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9** get_address_of_EnvelopeTables_0() { return &___EnvelopeTables_0; }
	inline void set_EnvelopeTables_0(SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9* value)
	{
		___EnvelopeTables_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EnvelopeTables_0), (void*)value);
	}

	inline static int32_t get_offset_of_SemitoneTable_1() { return static_cast<int32_t>(offsetof(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields, ___SemitoneTable_1)); }
	inline DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* get_SemitoneTable_1() const { return ___SemitoneTable_1; }
	inline DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB** get_address_of_SemitoneTable_1() { return &___SemitoneTable_1; }
	inline void set_SemitoneTable_1(DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* value)
	{
		___SemitoneTable_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SemitoneTable_1), (void*)value);
	}

	inline static int32_t get_offset_of_CentTable_2() { return static_cast<int32_t>(offsetof(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields, ___CentTable_2)); }
	inline DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* get_CentTable_2() const { return ___CentTable_2; }
	inline DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB** get_address_of_CentTable_2() { return &___CentTable_2; }
	inline void set_CentTable_2(DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* value)
	{
		___CentTable_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___CentTable_2), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Wave.WaveHelper
struct  WaveHelper_t652138CED2D2C1BCDD192248D5ED8AF693825EC9  : public RuntimeObject
{
public:

public:
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.LinkedListNode`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>
struct  LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::list
	LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::next
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * ___next_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::prev
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * ___prev_2;
	// T System.Collections.Generic.LinkedListNode`1::item
	Voice_tA46F178E48B0D771712A0AA48175620981094D47 * ___item_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675, ___list_0)); }
	inline LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * get_list_0() const { return ___list_0; }
	inline LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675, ___next_1)); }
	inline LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * get_next_1() const { return ___next_1; }
	inline LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___next_1), (void*)value);
	}

	inline static int32_t get_offset_of_prev_2() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675, ___prev_2)); }
	inline LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * get_prev_2() const { return ___prev_2; }
	inline LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 ** get_address_of_prev_2() { return &___prev_2; }
	inline void set_prev_2(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * value)
	{
		___prev_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prev_2), (void*)value);
	}

	inline static int32_t get_offset_of_item_3() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675, ___item_3)); }
	inline Voice_tA46F178E48B0D771712A0AA48175620981094D47 * get_item_3() const { return ___item_3; }
	inline Voice_tA46F178E48B0D771712A0AA48175620981094D47 ** get_address_of_item_3() { return &___item_3; }
	inline void set_item_3(Voice_tA46F178E48B0D771712A0AA48175620981094D47 * value)
	{
		___item_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___item_3), (void*)value);
	}
};


// System.Collections.Generic.LinkedListNode`1<System.Object>
struct  LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::list
	LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::next
	LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * ___next_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::prev
	LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * ___prev_2;
	// T System.Collections.Generic.LinkedListNode`1::item
	RuntimeObject * ___item_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F, ___list_0)); }
	inline LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * get_list_0() const { return ___list_0; }
	inline LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___list_0), (void*)value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F, ___next_1)); }
	inline LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * get_next_1() const { return ___next_1; }
	inline LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___next_1), (void*)value);
	}

	inline static int32_t get_offset_of_prev_2() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F, ___prev_2)); }
	inline LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * get_prev_2() const { return ___prev_2; }
	inline LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F ** get_address_of_prev_2() { return &___prev_2; }
	inline void set_prev_2(LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * value)
	{
		___prev_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___prev_2), (void*)value);
	}

	inline static int32_t get_offset_of_item_3() { return static_cast<int32_t>(offsetof(LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F, ___item_3)); }
	inline RuntimeObject * get_item_3() const { return ___item_3; }
	inline RuntimeObject ** get_address_of_item_3() { return &___item_3; }
	inline void set_item_3(RuntimeObject * value)
	{
		___item_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___item_3), (void*)value);
	}
};


// System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>
struct  LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::head
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * ___head_0;
	// System.Int32 System.Collections.Generic.LinkedList`1::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Generic.LinkedList`1::version
	int32_t ___version_2;
	// System.Object System.Collections.Generic.LinkedList`1::_syncRoot
	RuntimeObject * ____syncRoot_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::_siInfo
	SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ____siInfo_4;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413, ___head_0)); }
	inline LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * get_head_0() const { return ___head_0; }
	inline LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___head_0), (void*)value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_3), (void*)value);
	}

	inline static int32_t get_offset_of__siInfo_4() { return static_cast<int32_t>(offsetof(LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413, ____siInfo_4)); }
	inline SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * get__siInfo_4() const { return ____siInfo_4; }
	inline SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 ** get_address_of__siInfo_4() { return &____siInfo_4; }
	inline void set__siInfo_4(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * value)
	{
		____siInfo_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____siInfo_4), (void*)value);
	}
};


// System.Collections.Generic.LinkedList`1<System.Object>
struct  LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::head
	LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * ___head_0;
	// System.Int32 System.Collections.Generic.LinkedList`1::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Generic.LinkedList`1::version
	int32_t ___version_2;
	// System.Object System.Collections.Generic.LinkedList`1::_syncRoot
	RuntimeObject * ____syncRoot_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::_siInfo
	SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * ____siInfo_4;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B, ___head_0)); }
	inline LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * get_head_0() const { return ___head_0; }
	inline LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___head_0), (void*)value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_3), (void*)value);
	}

	inline static int32_t get_offset_of__siInfo_4() { return static_cast<int32_t>(offsetof(LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B, ____siInfo_4)); }
	inline SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * get__siInfo_4() const { return ____siInfo_4; }
	inline SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 ** get_address_of__siInfo_4() { return &____siInfo_4; }
	inline void set__siInfo_4(SerializationInfo_t097DA64D9DB49ED7F2458E964BE8CCCF63FC67C1 * value)
	{
		____siInfo_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____siInfo_4), (void*)value);
	}
};


// System.Collections.Generic.Queue`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage>
struct  Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Queue`1::_array
	MidiMessageU5BU5D_t048A9411CBDF5DA1EC82A13311FD80536CD689A1* ____array_0;
	// System.Int32 System.Collections.Generic.Queue`1::_head
	int32_t ____head_1;
	// System.Int32 System.Collections.Generic.Queue`1::_tail
	int32_t ____tail_2;
	// System.Int32 System.Collections.Generic.Queue`1::_size
	int32_t ____size_3;
	// System.Int32 System.Collections.Generic.Queue`1::_version
	int32_t ____version_4;
	// System.Object System.Collections.Generic.Queue`1::_syncRoot
	RuntimeObject * ____syncRoot_5;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965, ____array_0)); }
	inline MidiMessageU5BU5D_t048A9411CBDF5DA1EC82A13311FD80536CD689A1* get__array_0() const { return ____array_0; }
	inline MidiMessageU5BU5D_t048A9411CBDF5DA1EC82A13311FD80536CD689A1** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(MidiMessageU5BU5D_t048A9411CBDF5DA1EC82A13311FD80536CD689A1* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__head_1() { return static_cast<int32_t>(offsetof(Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965, ____head_1)); }
	inline int32_t get__head_1() const { return ____head_1; }
	inline int32_t* get_address_of__head_1() { return &____head_1; }
	inline void set__head_1(int32_t value)
	{
		____head_1 = value;
	}

	inline static int32_t get_offset_of__tail_2() { return static_cast<int32_t>(offsetof(Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965, ____tail_2)); }
	inline int32_t get__tail_2() const { return ____tail_2; }
	inline int32_t* get_address_of__tail_2() { return &____tail_2; }
	inline void set__tail_2(int32_t value)
	{
		____tail_2 = value;
	}

	inline static int32_t get_offset_of__size_3() { return static_cast<int32_t>(offsetof(Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965, ____size_3)); }
	inline int32_t get__size_3() const { return ____size_3; }
	inline int32_t* get_address_of__size_3() { return &____size_3; }
	inline void set__size_3(int32_t value)
	{
		____size_3 = value;
	}

	inline static int32_t get_offset_of__version_4() { return static_cast<int32_t>(offsetof(Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965, ____version_4)); }
	inline int32_t get__version_4() const { return ____version_4; }
	inline int32_t* get_address_of__version_4() { return &____version_4; }
	inline void set__version_4(int32_t value)
	{
		____version_4 = value;
	}

	inline static int32_t get_offset_of__syncRoot_5() { return static_cast<int32_t>(offsetof(Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965, ____syncRoot_5)); }
	inline RuntimeObject * get__syncRoot_5() const { return ____syncRoot_5; }
	inline RuntimeObject ** get_address_of__syncRoot_5() { return &____syncRoot_5; }
	inline void set__syncRoot_5(RuntimeObject * value)
	{
		____syncRoot_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_5), (void*)value);
	}
};


// System.Collections.Generic.Stack`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode>
struct  Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.Stack`1::_array
	VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665* ____array_0;
	// System.Int32 System.Collections.Generic.Stack`1::_size
	int32_t ____size_1;
	// System.Int32 System.Collections.Generic.Stack`1::_version
	int32_t ____version_2;
	// System.Object System.Collections.Generic.Stack`1::_syncRoot
	RuntimeObject * ____syncRoot_3;

public:
	inline static int32_t get_offset_of__array_0() { return static_cast<int32_t>(offsetof(Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8, ____array_0)); }
	inline VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665* get__array_0() const { return ____array_0; }
	inline VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665** get_address_of__array_0() { return &____array_0; }
	inline void set__array_0(VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665* value)
	{
		____array_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____array_0), (void*)value);
	}

	inline static int32_t get_offset_of__size_1() { return static_cast<int32_t>(offsetof(Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8, ____size_1)); }
	inline int32_t get__size_1() const { return ____size_1; }
	inline int32_t* get_address_of__size_1() { return &____size_1; }
	inline void set__size_1(int32_t value)
	{
		____size_1 = value;
	}

	inline static int32_t get_offset_of__version_2() { return static_cast<int32_t>(offsetof(Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8, ____version_2)); }
	inline int32_t get__version_2() const { return ____version_2; }
	inline int32_t* get_address_of__version_2() { return &____version_2; }
	inline void set__version_2(int32_t value)
	{
		____version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_3), (void*)value);
	}
};


// System.IO.BinaryReader
struct  BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128  : public RuntimeObject
{
public:
	// System.IO.Stream System.IO.BinaryReader::m_stream
	Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * ___m_stream_0;
	// System.Byte[] System.IO.BinaryReader::m_buffer
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___m_buffer_1;
	// System.Text.Decoder System.IO.BinaryReader::m_decoder
	Decoder_t91B2ED8AEC25AA24D23A00265203BE992B12C370 * ___m_decoder_2;
	// System.Byte[] System.IO.BinaryReader::m_charBytes
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___m_charBytes_3;
	// System.Char[] System.IO.BinaryReader::m_singleChar
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___m_singleChar_4;
	// System.Char[] System.IO.BinaryReader::m_charBuffer
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___m_charBuffer_5;
	// System.Int32 System.IO.BinaryReader::m_maxCharsSize
	int32_t ___m_maxCharsSize_6;
	// System.Boolean System.IO.BinaryReader::m_2BytesPerChar
	bool ___m_2BytesPerChar_7;
	// System.Boolean System.IO.BinaryReader::m_isMemoryStream
	bool ___m_isMemoryStream_8;
	// System.Boolean System.IO.BinaryReader::m_leaveOpen
	bool ___m_leaveOpen_9;

public:
	inline static int32_t get_offset_of_m_stream_0() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_stream_0)); }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * get_m_stream_0() const { return ___m_stream_0; }
	inline Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB ** get_address_of_m_stream_0() { return &___m_stream_0; }
	inline void set_m_stream_0(Stream_t5DC87DD578C2C5298D98E7802E92DEABB66E2ECB * value)
	{
		___m_stream_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stream_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_buffer_1() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_buffer_1)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_m_buffer_1() const { return ___m_buffer_1; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_m_buffer_1() { return &___m_buffer_1; }
	inline void set_m_buffer_1(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___m_buffer_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_buffer_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_decoder_2() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_decoder_2)); }
	inline Decoder_t91B2ED8AEC25AA24D23A00265203BE992B12C370 * get_m_decoder_2() const { return ___m_decoder_2; }
	inline Decoder_t91B2ED8AEC25AA24D23A00265203BE992B12C370 ** get_address_of_m_decoder_2() { return &___m_decoder_2; }
	inline void set_m_decoder_2(Decoder_t91B2ED8AEC25AA24D23A00265203BE992B12C370 * value)
	{
		___m_decoder_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_decoder_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_charBytes_3() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_charBytes_3)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_m_charBytes_3() const { return ___m_charBytes_3; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_m_charBytes_3() { return &___m_charBytes_3; }
	inline void set_m_charBytes_3(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___m_charBytes_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_charBytes_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_singleChar_4() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_singleChar_4)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_m_singleChar_4() const { return ___m_singleChar_4; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_m_singleChar_4() { return &___m_singleChar_4; }
	inline void set_m_singleChar_4(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___m_singleChar_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_singleChar_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_charBuffer_5() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_charBuffer_5)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_m_charBuffer_5() const { return ___m_charBuffer_5; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_m_charBuffer_5() { return &___m_charBuffer_5; }
	inline void set_m_charBuffer_5(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___m_charBuffer_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_charBuffer_5), (void*)value);
	}

	inline static int32_t get_offset_of_m_maxCharsSize_6() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_maxCharsSize_6)); }
	inline int32_t get_m_maxCharsSize_6() const { return ___m_maxCharsSize_6; }
	inline int32_t* get_address_of_m_maxCharsSize_6() { return &___m_maxCharsSize_6; }
	inline void set_m_maxCharsSize_6(int32_t value)
	{
		___m_maxCharsSize_6 = value;
	}

	inline static int32_t get_offset_of_m_2BytesPerChar_7() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_2BytesPerChar_7)); }
	inline bool get_m_2BytesPerChar_7() const { return ___m_2BytesPerChar_7; }
	inline bool* get_address_of_m_2BytesPerChar_7() { return &___m_2BytesPerChar_7; }
	inline void set_m_2BytesPerChar_7(bool value)
	{
		___m_2BytesPerChar_7 = value;
	}

	inline static int32_t get_offset_of_m_isMemoryStream_8() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_isMemoryStream_8)); }
	inline bool get_m_isMemoryStream_8() const { return ___m_isMemoryStream_8; }
	inline bool* get_address_of_m_isMemoryStream_8() { return &___m_isMemoryStream_8; }
	inline void set_m_isMemoryStream_8(bool value)
	{
		___m_isMemoryStream_8 = value;
	}

	inline static int32_t get_offset_of_m_leaveOpen_9() { return static_cast<int32_t>(offsetof(BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128, ___m_leaveOpen_9)); }
	inline bool get_m_leaveOpen_9() const { return ___m_leaveOpen_9; }
	inline bool* get_address_of_m_leaveOpen_9() { return &___m_leaveOpen_9; }
	inline void set_m_leaveOpen_9(bool value)
	{
		___m_leaveOpen_9 = value;
	}
};


// System.Random
struct  Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118  : public RuntimeObject
{
public:
	// System.Int32 System.Random::inext
	int32_t ___inext_3;
	// System.Int32 System.Random::inextp
	int32_t ___inextp_4;
	// System.Int32[] System.Random::SeedArray
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___SeedArray_5;

public:
	inline static int32_t get_offset_of_inext_3() { return static_cast<int32_t>(offsetof(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118, ___inext_3)); }
	inline int32_t get_inext_3() const { return ___inext_3; }
	inline int32_t* get_address_of_inext_3() { return &___inext_3; }
	inline void set_inext_3(int32_t value)
	{
		___inext_3 = value;
	}

	inline static int32_t get_offset_of_inextp_4() { return static_cast<int32_t>(offsetof(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118, ___inextp_4)); }
	inline int32_t get_inextp_4() const { return ___inextp_4; }
	inline int32_t* get_address_of_inextp_4() { return &___inextp_4; }
	inline void set_inextp_4(int32_t value)
	{
		___inextp_4 = value;
	}

	inline static int32_t get_offset_of_SeedArray_5() { return static_cast<int32_t>(offsetof(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118, ___SeedArray_5)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_SeedArray_5() const { return ___SeedArray_5; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_SeedArray_5() { return &___SeedArray_5; }
	inline void set_SeedArray_5(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___SeedArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SeedArray_5), (void*)value);
	}
};


// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemCommonEvent
struct  SystemCommonEvent_t9ED10AE0931452F227792401D07F5DD1645A63CC  : public MidiEvent_t0A4643C6A8CBCA997171ABC2EFEC507BA8C5AE9C
{
public:

public:
};


// DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk
struct  ZoneChunk_t894317B0A9F9371149E84A5F5D3AC09B86524F5A  : public Chunk_t9103F9401FE26193B5FE338EF34D83156956415C
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk_RawZoneData[] DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk::zoneData
	RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E* ___zoneData_2;

public:
	inline static int32_t get_offset_of_zoneData_2() { return static_cast<int32_t>(offsetof(ZoneChunk_t894317B0A9F9371149E84A5F5D3AC09B86524F5A, ___zoneData_2)); }
	inline RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E* get_zoneData_2() const { return ___zoneData_2; }
	inline RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E** get_address_of_zoneData_2() { return &___zoneData_2; }
	inline void set_zoneData_2(RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E* value)
	{
		___zoneData_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___zoneData_2), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue
struct  CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B 
{
public:
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::coarseValue
	uint8_t ___coarseValue_0;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::fineValue
	uint8_t ___fineValue_1;
	// System.Int16 DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::combined
	int16_t ___combined_2;

public:
	inline static int32_t get_offset_of_coarseValue_0() { return static_cast<int32_t>(offsetof(CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B, ___coarseValue_0)); }
	inline uint8_t get_coarseValue_0() const { return ___coarseValue_0; }
	inline uint8_t* get_address_of_coarseValue_0() { return &___coarseValue_0; }
	inline void set_coarseValue_0(uint8_t value)
	{
		___coarseValue_0 = value;
	}

	inline static int32_t get_offset_of_fineValue_1() { return static_cast<int32_t>(offsetof(CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B, ___fineValue_1)); }
	inline uint8_t get_fineValue_1() const { return ___fineValue_1; }
	inline uint8_t* get_address_of_fineValue_1() { return &___fineValue_1; }
	inline void set_fineValue_1(uint8_t value)
	{
		___fineValue_1 = value;
	}

	inline static int32_t get_offset_of_combined_2() { return static_cast<int32_t>(offsetof(CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B, ___combined_2)); }
	inline int16_t get_combined_2() const { return ___combined_2; }
	inline int16_t* get_address_of_combined_2() { return &___combined_2; }
	inline void set_combined_2(int16_t value)
	{
		___combined_2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage
struct  MidiMessage_t3810404F35143BF297802CC153A00F2D69755B24 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage::delta
	int32_t ___delta_0;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage::channel
	uint8_t ___channel_1;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage::command
	uint8_t ___command_2;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage::data1
	uint8_t ___data1_3;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage::data2
	uint8_t ___data2_4;

public:
	inline static int32_t get_offset_of_delta_0() { return static_cast<int32_t>(offsetof(MidiMessage_t3810404F35143BF297802CC153A00F2D69755B24, ___delta_0)); }
	inline int32_t get_delta_0() const { return ___delta_0; }
	inline int32_t* get_address_of_delta_0() { return &___delta_0; }
	inline void set_delta_0(int32_t value)
	{
		___delta_0 = value;
	}

	inline static int32_t get_offset_of_channel_1() { return static_cast<int32_t>(offsetof(MidiMessage_t3810404F35143BF297802CC153A00F2D69755B24, ___channel_1)); }
	inline uint8_t get_channel_1() const { return ___channel_1; }
	inline uint8_t* get_address_of_channel_1() { return &___channel_1; }
	inline void set_channel_1(uint8_t value)
	{
		___channel_1 = value;
	}

	inline static int32_t get_offset_of_command_2() { return static_cast<int32_t>(offsetof(MidiMessage_t3810404F35143BF297802CC153A00F2D69755B24, ___command_2)); }
	inline uint8_t get_command_2() const { return ___command_2; }
	inline uint8_t* get_address_of_command_2() { return &___command_2; }
	inline void set_command_2(uint8_t value)
	{
		___command_2 = value;
	}

	inline static int32_t get_offset_of_data1_3() { return static_cast<int32_t>(offsetof(MidiMessage_t3810404F35143BF297802CC153A00F2D69755B24, ___data1_3)); }
	inline uint8_t get_data1_3() const { return ___data1_3; }
	inline uint8_t* get_address_of_data1_3() { return &___data1_3; }
	inline void set_data1_3(uint8_t value)
	{
		___data1_3 = value;
	}

	inline static int32_t get_offset_of_data2_4() { return static_cast<int32_t>(offsetof(MidiMessage_t3810404F35143BF297802CC153A00F2D69755B24, ___data2_4)); }
	inline uint8_t get_data2_4() const { return ___data2_4; }
	inline uint8_t* get_address_of_data2_4() { return &___data2_4; }
	inline void set_data2_4(uint8_t value)
	{
		___data2_4 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.PanComponent
struct  PanComponent_tEF5B0C830A87834170A3E9781D0776F402CA5C3D 
{
public:
	// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.PanComponent::Left
	float ___Left_0;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.PanComponent::Right
	float ___Right_1;

public:
	inline static int32_t get_offset_of_Left_0() { return static_cast<int32_t>(offsetof(PanComponent_tEF5B0C830A87834170A3E9781D0776F402CA5C3D, ___Left_0)); }
	inline float get_Left_0() const { return ___Left_0; }
	inline float* get_address_of_Left_0() { return &___Left_0; }
	inline void set_Left_0(float value)
	{
		___Left_0 = value;
	}

	inline static int32_t get_offset_of_Right_1() { return static_cast<int32_t>(offsetof(PanComponent_tEF5B0C830A87834170A3E9781D0776F402CA5C3D, ___Right_1)); }
	inline float get_Right_1() const { return ___Right_1; }
	inline float* get_address_of_Right_1() { return &___Right_1; }
	inline void set_Right_1(float value)
	{
		___Right_1 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.UnionData
struct  UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Double DaggerfallWorkshop.AudioSynthesis.Synthesis.UnionData::double1
			double ___double1_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			double ___double1_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.UnionData::float1
			float ___float1_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			float ___float1_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___float2_2_OffsetPadding[4];
			// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.UnionData::float2
			float ___float2_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___float2_2_OffsetPadding_forAlignmentOnly[4];
			float ___float2_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.UnionData::int1
			int32_t ___int1_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___int1_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___int2_4_OffsetPadding[4];
			// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.UnionData::int2
			int32_t ___int2_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___int2_4_OffsetPadding_forAlignmentOnly[4];
			int32_t ___int2_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_double1_0() { return static_cast<int32_t>(offsetof(UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4, ___double1_0)); }
	inline double get_double1_0() const { return ___double1_0; }
	inline double* get_address_of_double1_0() { return &___double1_0; }
	inline void set_double1_0(double value)
	{
		___double1_0 = value;
	}

	inline static int32_t get_offset_of_float1_1() { return static_cast<int32_t>(offsetof(UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4, ___float1_1)); }
	inline float get_float1_1() const { return ___float1_1; }
	inline float* get_address_of_float1_1() { return &___float1_1; }
	inline void set_float1_1(float value)
	{
		___float1_1 = value;
	}

	inline static int32_t get_offset_of_float2_2() { return static_cast<int32_t>(offsetof(UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4, ___float2_2)); }
	inline float get_float2_2() const { return ___float2_2; }
	inline float* get_address_of_float2_2() { return &___float2_2; }
	inline void set_float2_2(float value)
	{
		___float2_2 = value;
	}

	inline static int32_t get_offset_of_int1_3() { return static_cast<int32_t>(offsetof(UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4, ___int1_3)); }
	inline int32_t get_int1_3() const { return ___int1_3; }
	inline int32_t* get_address_of_int1_3() { return &___int1_3; }
	inline void set_int1_3(int32_t value)
	{
		___int1_3 = value;
	}

	inline static int32_t get_offset_of_int2_4() { return static_cast<int32_t>(offsetof(UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4, ___int2_4)); }
	inline int32_t get_int2_4() const { return ___int2_4; }
	inline int32_t* get_address_of_int2_4() { return &___int2_4; }
	inline void set_int2_4(int32_t value)
	{
		___int2_4 = value;
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct  Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Collections.Generic.Stack`1_Enumerator<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode>
struct  Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF 
{
public:
	// System.Collections.Generic.Stack`1<T> System.Collections.Generic.Stack`1_Enumerator::_stack
	Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * ____stack_0;
	// System.Int32 System.Collections.Generic.Stack`1_Enumerator::_version
	int32_t ____version_1;
	// System.Int32 System.Collections.Generic.Stack`1_Enumerator::_index
	int32_t ____index_2;
	// T System.Collections.Generic.Stack`1_Enumerator::_currentElement
	VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * ____currentElement_3;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF, ____stack_0)); }
	inline Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * get__stack_0() const { return ____stack_0; }
	inline Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stack_0), (void*)value);
	}

	inline static int32_t get_offset_of__version_1() { return static_cast<int32_t>(offsetof(Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF, ____version_1)); }
	inline int32_t get__version_1() const { return ____version_1; }
	inline int32_t* get_address_of__version_1() { return &____version_1; }
	inline void set__version_1(int32_t value)
	{
		____version_1 = value;
	}

	inline static int32_t get_offset_of__index_2() { return static_cast<int32_t>(offsetof(Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF, ____index_2)); }
	inline int32_t get__index_2() const { return ____index_2; }
	inline int32_t* get_address_of__index_2() { return &____index_2; }
	inline void set__index_2(int32_t value)
	{
		____index_2 = value;
	}

	inline static int32_t get_offset_of__currentElement_3() { return static_cast<int32_t>(offsetof(Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF, ____currentElement_3)); }
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * get__currentElement_3() const { return ____currentElement_3; }
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E ** get_address_of__currentElement_3() { return &____currentElement_3; }
	inline void set__currentElement_3(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * value)
	{
		____currentElement_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentElement_3), (void*)value);
	}
};


// System.Collections.Generic.Stack`1_Enumerator<System.Object>
struct  Enumerator_tF6E3A9686966EF18B6DFA3748229B35E666CE514 
{
public:
	// System.Collections.Generic.Stack`1<T> System.Collections.Generic.Stack`1_Enumerator::_stack
	Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981 * ____stack_0;
	// System.Int32 System.Collections.Generic.Stack`1_Enumerator::_version
	int32_t ____version_1;
	// System.Int32 System.Collections.Generic.Stack`1_Enumerator::_index
	int32_t ____index_2;
	// T System.Collections.Generic.Stack`1_Enumerator::_currentElement
	RuntimeObject * ____currentElement_3;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(Enumerator_tF6E3A9686966EF18B6DFA3748229B35E666CE514, ____stack_0)); }
	inline Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981 * get__stack_0() const { return ____stack_0; }
	inline Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981 ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981 * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stack_0), (void*)value);
	}

	inline static int32_t get_offset_of__version_1() { return static_cast<int32_t>(offsetof(Enumerator_tF6E3A9686966EF18B6DFA3748229B35E666CE514, ____version_1)); }
	inline int32_t get__version_1() const { return ____version_1; }
	inline int32_t* get_address_of__version_1() { return &____version_1; }
	inline void set__version_1(int32_t value)
	{
		____version_1 = value;
	}

	inline static int32_t get_offset_of__index_2() { return static_cast<int32_t>(offsetof(Enumerator_tF6E3A9686966EF18B6DFA3748229B35E666CE514, ____index_2)); }
	inline int32_t get__index_2() const { return ____index_2; }
	inline int32_t* get_address_of__index_2() { return &____index_2; }
	inline void set__index_2(int32_t value)
	{
		____index_2 = value;
	}

	inline static int32_t get_offset_of__currentElement_3() { return static_cast<int32_t>(offsetof(Enumerator_tF6E3A9686966EF18B6DFA3748229B35E666CE514, ____currentElement_3)); }
	inline RuntimeObject * get__currentElement_3() const { return ____currentElement_3; }
	inline RuntimeObject ** get_address_of__currentElement_3() { return &____currentElement_3; }
	inline void set__currentElement_3(RuntimeObject * value)
	{
		____currentElement_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____currentElement_3), (void*)value);
	}
};


// System.Double
struct  Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181 
{
public:
	// System.Double System.Double::m_value
	double ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181, ___m_value_0)); }
	inline double get_m_value_0() const { return ___m_value_0; }
	inline double* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(double value)
	{
		___m_value_0 = value;
	}
};

struct Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields
{
public:
	// System.Double System.Double::NegativeZero
	double ___NegativeZero_7;

public:
	inline static int32_t get_offset_of_NegativeZero_7() { return static_cast<int32_t>(offsetof(Double_t42821932CB52DE2057E685D0E1AF3DE5033D2181_StaticFields, ___NegativeZero_7)); }
	inline double get_NegativeZero_7() const { return ___NegativeZero_7; }
	inline double* get_address_of_NegativeZero_7() { return &___NegativeZero_7; }
	inline void set_NegativeZero_7(double value)
	{
		___NegativeZero_7 = value;
	}
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int16
struct  Int16_tD0F031114106263BB459DA1F099FF9F42691295A 
{
public:
	// System.Int16 System.Int16::m_value
	int16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int16_tD0F031114106263BB459DA1F099FF9F42691295A, ___m_value_0)); }
	inline int16_t get_m_value_0() const { return ___m_value_0; }
	inline int16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int16_t value)
	{
		___m_value_0 = value;
	}
};


// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.UInt16
struct  UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD 
{
public:
	// System.UInt16 System.UInt16::m_value
	uint16_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt16_t894EA9D4FB7C799B244E7BBF2DF0EEEDBC77A8BD, ___m_value_0)); }
	inline uint16_t get_m_value_0() const { return ___m_value_0; }
	inline uint16_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint16_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.EnvelopeStateEnum
struct  EnvelopeStateEnum_t4ECE4A9EED173B425432F74A53AFF18735042EC0 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Components.EnvelopeStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EnvelopeStateEnum_t4ECE4A9EED173B425432F74A53AFF18735042EC0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.FilterTypeEnum
struct  FilterTypeEnum_t82062FB9F570F4D0CFA761BE2483A9CE692CFB03 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Components.FilterTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FilterTypeEnum_t82062FB9F570F4D0CFA761BE2483A9CE692CFB03, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.GeneratorStateEnum
struct  GeneratorStateEnum_tF8AAE3694CEA18455277FA1065228EF9A4EC5CB7 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Components.GeneratorStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GeneratorStateEnum_tF8AAE3694CEA18455277FA1065228EF9A4EC5CB7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.InterpolationEnum
struct  InterpolationEnum_tB8BE4A93EFC4BABBEE1FFB700DBC13A6A58FDEF8 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Components.InterpolationEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InterpolationEnum_tB8BE4A93EFC4BABBEE1FFB700DBC13A6A58FDEF8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.LfoStateEnum
struct  LfoStateEnum_t8968080A2C7D20496662E26F68D2ED71A826F16D 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Components.LfoStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LfoStateEnum_t8968080A2C7D20496662E26F68D2ED71A826F16D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.LoopModeEnum
struct  LoopModeEnum_tEF2C109EA68B6DCC63362AED0188AFBCB14C710E 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Components.LoopModeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LoopModeEnum_tEF2C109EA68B6DCC63362AED0188AFBCB14C710E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.WaveformEnum
struct  WaveformEnum_tCA5B6254A8D3AC6B3AC7B77913587D6354179F03 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Components.WaveformEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WaveformEnum_tCA5B6254A8D3AC6B3AC7B77913587D6354179F03, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Fm2Patch_SyncMode
struct  SyncMode_t869B2DBF1C82C4A6D33996D931A67FBB85C17029 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Fm2Patch_SyncMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SyncMode_t869B2DBF1C82C4A6D33996D931A67FBB85C17029, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_IntervalType
struct  IntervalType_tD3140D1D2431B3A272BE71AF2221B7FFBA0C10D2 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_IntervalType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IntervalType_tD3140D1D2431B3A272BE71AF2221B7FFBA0C10D2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemExclusiveEvent
struct  SystemExclusiveEvent_tC8271D73FDE64FB3488EAC4F9C891831E661A79A  : public SystemCommonEvent_t9ED10AE0931452F227792401D07F5DD1645A63CC
{
public:
	// System.Byte[] DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemExclusiveEvent::mdata
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___mdata_2;

public:
	inline static int32_t get_offset_of_mdata_2() { return static_cast<int32_t>(offsetof(SystemExclusiveEvent_tC8271D73FDE64FB3488EAC4F9C891831E661A79A, ___mdata_2)); }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* get_mdata_2() const { return ___mdata_2; }
	inline ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726** get_address_of_mdata_2() { return &___mdata_2; }
	inline void set_mdata_2(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* value)
	{
		___mdata_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mdata_2), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile_TimeFormat
struct  TimeFormat_tBF85A06C21A2AB3138F7FD9C8F7CC34C52155D02 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile_TimeFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TimeFormat_tBF85A06C21A2AB3138F7FD9C8F7CC34C52155D02, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile_TrackFormat
struct  TrackFormat_t5673455C6DFA0D7398E0B45AB196A0A38BE88249 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile_TrackFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TrackFormat_t5673455C6DFA0D7398E0B45AB196A0A38BE88249, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Midi.SystemCommonTypeEnum
struct  SystemCommonTypeEnum_t8CD91F9148AD22D4C0B78296D4A1B7F233493B7D 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.SystemCommonTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SystemCommonTypeEnum_t8CD91F9148AD22D4C0B78296D4A1B7F233493B7D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Sf2.GeneratorEnum
struct  GeneratorEnum_t60ED71106A020051EF0E0FCB19C37BB6F4091A0D 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Sf2.GeneratorEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GeneratorEnum_t60ED71106A020051EF0E0FCB19C37BB6F4091A0D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Sf2.SourceTypeEnum
struct  SourceTypeEnum_t3A29ACEF14BC2E61988D235D7BD7AB7F104AB7A1 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Sf2.SourceTypeEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SourceTypeEnum_t3A29ACEF14BC2E61988D235D7BD7AB7F104AB7A1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Sf2.TransformEnum
struct  TransformEnum_t7EA85B200308BACF15AAFC1E5CCF917D2A020A50 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Sf2.TransformEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TransformEnum_t7EA85B200308BACF15AAFC1E5CCF917D2A020A50, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters
struct  SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524  : public RuntimeObject
{
public:
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::program
	uint8_t ___program_0;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::bankSelect
	uint8_t ___bankSelect_1;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::channelAfterTouch
	uint8_t ___channelAfterTouch_2;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::pan
	CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  ___pan_3;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::volume
	CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  ___volume_4;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::expression
	CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  ___expression_5;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::modRange
	CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  ___modRange_6;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::pitchBend
	CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  ___pitchBend_7;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::pitchBendRangeCoarse
	uint8_t ___pitchBendRangeCoarse_8;
	// System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::pitchBendRangeFine
	uint8_t ___pitchBendRangeFine_9;
	// System.Int16 DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::masterCoarseTune
	int16_t ___masterCoarseTune_10;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::masterFineTune
	CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  ___masterFineTune_11;
	// System.Boolean DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::holdPedal
	bool ___holdPedal_12;
	// System.Boolean DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::legatoPedal
	bool ___legatoPedal_13;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::rpn
	CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  ___rpn_14;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::synth
	Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * ___synth_15;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::currentVolume
	float ___currentVolume_16;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::currentPitch
	int32_t ___currentPitch_17;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::currentMod
	int32_t ___currentMod_18;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.PanComponent DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::currentPan
	PanComponent_tEF5B0C830A87834170A3E9781D0776F402CA5C3D  ___currentPan_19;

public:
	inline static int32_t get_offset_of_program_0() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___program_0)); }
	inline uint8_t get_program_0() const { return ___program_0; }
	inline uint8_t* get_address_of_program_0() { return &___program_0; }
	inline void set_program_0(uint8_t value)
	{
		___program_0 = value;
	}

	inline static int32_t get_offset_of_bankSelect_1() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___bankSelect_1)); }
	inline uint8_t get_bankSelect_1() const { return ___bankSelect_1; }
	inline uint8_t* get_address_of_bankSelect_1() { return &___bankSelect_1; }
	inline void set_bankSelect_1(uint8_t value)
	{
		___bankSelect_1 = value;
	}

	inline static int32_t get_offset_of_channelAfterTouch_2() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___channelAfterTouch_2)); }
	inline uint8_t get_channelAfterTouch_2() const { return ___channelAfterTouch_2; }
	inline uint8_t* get_address_of_channelAfterTouch_2() { return &___channelAfterTouch_2; }
	inline void set_channelAfterTouch_2(uint8_t value)
	{
		___channelAfterTouch_2 = value;
	}

	inline static int32_t get_offset_of_pan_3() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___pan_3)); }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  get_pan_3() const { return ___pan_3; }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * get_address_of_pan_3() { return &___pan_3; }
	inline void set_pan_3(CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  value)
	{
		___pan_3 = value;
	}

	inline static int32_t get_offset_of_volume_4() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___volume_4)); }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  get_volume_4() const { return ___volume_4; }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * get_address_of_volume_4() { return &___volume_4; }
	inline void set_volume_4(CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  value)
	{
		___volume_4 = value;
	}

	inline static int32_t get_offset_of_expression_5() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___expression_5)); }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  get_expression_5() const { return ___expression_5; }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * get_address_of_expression_5() { return &___expression_5; }
	inline void set_expression_5(CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  value)
	{
		___expression_5 = value;
	}

	inline static int32_t get_offset_of_modRange_6() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___modRange_6)); }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  get_modRange_6() const { return ___modRange_6; }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * get_address_of_modRange_6() { return &___modRange_6; }
	inline void set_modRange_6(CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  value)
	{
		___modRange_6 = value;
	}

	inline static int32_t get_offset_of_pitchBend_7() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___pitchBend_7)); }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  get_pitchBend_7() const { return ___pitchBend_7; }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * get_address_of_pitchBend_7() { return &___pitchBend_7; }
	inline void set_pitchBend_7(CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  value)
	{
		___pitchBend_7 = value;
	}

	inline static int32_t get_offset_of_pitchBendRangeCoarse_8() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___pitchBendRangeCoarse_8)); }
	inline uint8_t get_pitchBendRangeCoarse_8() const { return ___pitchBendRangeCoarse_8; }
	inline uint8_t* get_address_of_pitchBendRangeCoarse_8() { return &___pitchBendRangeCoarse_8; }
	inline void set_pitchBendRangeCoarse_8(uint8_t value)
	{
		___pitchBendRangeCoarse_8 = value;
	}

	inline static int32_t get_offset_of_pitchBendRangeFine_9() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___pitchBendRangeFine_9)); }
	inline uint8_t get_pitchBendRangeFine_9() const { return ___pitchBendRangeFine_9; }
	inline uint8_t* get_address_of_pitchBendRangeFine_9() { return &___pitchBendRangeFine_9; }
	inline void set_pitchBendRangeFine_9(uint8_t value)
	{
		___pitchBendRangeFine_9 = value;
	}

	inline static int32_t get_offset_of_masterCoarseTune_10() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___masterCoarseTune_10)); }
	inline int16_t get_masterCoarseTune_10() const { return ___masterCoarseTune_10; }
	inline int16_t* get_address_of_masterCoarseTune_10() { return &___masterCoarseTune_10; }
	inline void set_masterCoarseTune_10(int16_t value)
	{
		___masterCoarseTune_10 = value;
	}

	inline static int32_t get_offset_of_masterFineTune_11() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___masterFineTune_11)); }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  get_masterFineTune_11() const { return ___masterFineTune_11; }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * get_address_of_masterFineTune_11() { return &___masterFineTune_11; }
	inline void set_masterFineTune_11(CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  value)
	{
		___masterFineTune_11 = value;
	}

	inline static int32_t get_offset_of_holdPedal_12() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___holdPedal_12)); }
	inline bool get_holdPedal_12() const { return ___holdPedal_12; }
	inline bool* get_address_of_holdPedal_12() { return &___holdPedal_12; }
	inline void set_holdPedal_12(bool value)
	{
		___holdPedal_12 = value;
	}

	inline static int32_t get_offset_of_legatoPedal_13() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___legatoPedal_13)); }
	inline bool get_legatoPedal_13() const { return ___legatoPedal_13; }
	inline bool* get_address_of_legatoPedal_13() { return &___legatoPedal_13; }
	inline void set_legatoPedal_13(bool value)
	{
		___legatoPedal_13 = value;
	}

	inline static int32_t get_offset_of_rpn_14() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___rpn_14)); }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  get_rpn_14() const { return ___rpn_14; }
	inline CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * get_address_of_rpn_14() { return &___rpn_14; }
	inline void set_rpn_14(CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B  value)
	{
		___rpn_14 = value;
	}

	inline static int32_t get_offset_of_synth_15() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___synth_15)); }
	inline Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * get_synth_15() const { return ___synth_15; }
	inline Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB ** get_address_of_synth_15() { return &___synth_15; }
	inline void set_synth_15(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * value)
	{
		___synth_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___synth_15), (void*)value);
	}

	inline static int32_t get_offset_of_currentVolume_16() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___currentVolume_16)); }
	inline float get_currentVolume_16() const { return ___currentVolume_16; }
	inline float* get_address_of_currentVolume_16() { return &___currentVolume_16; }
	inline void set_currentVolume_16(float value)
	{
		___currentVolume_16 = value;
	}

	inline static int32_t get_offset_of_currentPitch_17() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___currentPitch_17)); }
	inline int32_t get_currentPitch_17() const { return ___currentPitch_17; }
	inline int32_t* get_address_of_currentPitch_17() { return &___currentPitch_17; }
	inline void set_currentPitch_17(int32_t value)
	{
		___currentPitch_17 = value;
	}

	inline static int32_t get_offset_of_currentMod_18() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___currentMod_18)); }
	inline int32_t get_currentMod_18() const { return ___currentMod_18; }
	inline int32_t* get_address_of_currentMod_18() { return &___currentMod_18; }
	inline void set_currentMod_18(int32_t value)
	{
		___currentMod_18 = value;
	}

	inline static int32_t get_offset_of_currentPan_19() { return static_cast<int32_t>(offsetof(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524, ___currentPan_19)); }
	inline PanComponent_tEF5B0C830A87834170A3E9781D0776F402CA5C3D  get_currentPan_19() const { return ___currentPan_19; }
	inline PanComponent_tEF5B0C830A87834170A3E9781D0776F402CA5C3D * get_address_of_currentPan_19() { return &___currentPan_19; }
	inline void set_currentPan_19(PanComponent_tEF5B0C830A87834170A3E9781D0776F402CA5C3D  value)
	{
		___currentPan_19 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceStateEnum
struct  VoiceStateEnum_tE420EB9CD65647BED146DE806F4977C97B9BCD8E 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceStateEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VoiceStateEnum_tE420EB9CD65647BED146DE806F4977C97B9BCD8E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceStealEnum
struct  VoiceStealEnum_tC63661FBC768B3B1E6214694BABA6008ED766BE9 
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceStealEnum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VoiceStealEnum_tC63661FBC768B3B1E6214694BABA6008ED766BE9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Reflection.BindingFlags
struct  BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope
struct  Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.EnvelopeStateEnum DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::envState
	int32_t ___envState_0;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope_EnvelopeStage[] DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::stages
	EnvelopeStageU5BU5D_t5A737392361DF2F93226B588DAD7E2494C9C936B* ___stages_1;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope_EnvelopeStage DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::stage
	EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3 * ___stage_2;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::index
	int32_t ___index_3;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::value
	float ___value_4;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::depth
	float ___depth_5;

public:
	inline static int32_t get_offset_of_envState_0() { return static_cast<int32_t>(offsetof(Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40, ___envState_0)); }
	inline int32_t get_envState_0() const { return ___envState_0; }
	inline int32_t* get_address_of_envState_0() { return &___envState_0; }
	inline void set_envState_0(int32_t value)
	{
		___envState_0 = value;
	}

	inline static int32_t get_offset_of_stages_1() { return static_cast<int32_t>(offsetof(Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40, ___stages_1)); }
	inline EnvelopeStageU5BU5D_t5A737392361DF2F93226B588DAD7E2494C9C936B* get_stages_1() const { return ___stages_1; }
	inline EnvelopeStageU5BU5D_t5A737392361DF2F93226B588DAD7E2494C9C936B** get_address_of_stages_1() { return &___stages_1; }
	inline void set_stages_1(EnvelopeStageU5BU5D_t5A737392361DF2F93226B588DAD7E2494C9C936B* value)
	{
		___stages_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stages_1), (void*)value);
	}

	inline static int32_t get_offset_of_stage_2() { return static_cast<int32_t>(offsetof(Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40, ___stage_2)); }
	inline EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3 * get_stage_2() const { return ___stage_2; }
	inline EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3 ** get_address_of_stage_2() { return &___stage_2; }
	inline void set_stage_2(EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3 * value)
	{
		___stage_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___stage_2), (void*)value);
	}

	inline static int32_t get_offset_of_index_3() { return static_cast<int32_t>(offsetof(Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40, ___index_3)); }
	inline int32_t get_index_3() const { return ___index_3; }
	inline int32_t* get_address_of_index_3() { return &___index_3; }
	inline void set_index_3(int32_t value)
	{
		___index_3 = value;
	}

	inline static int32_t get_offset_of_value_4() { return static_cast<int32_t>(offsetof(Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40, ___value_4)); }
	inline float get_value_4() const { return ___value_4; }
	inline float* get_address_of_value_4() { return &___value_4; }
	inline void set_value_4(float value)
	{
		___value_4 = value;
	}

	inline static int32_t get_offset_of_depth_5() { return static_cast<int32_t>(offsetof(Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40, ___depth_5)); }
	inline float get_depth_5() const { return ___depth_5; }
	inline float* get_address_of_depth_5() { return &___depth_5; }
	inline void set_depth_5(float value)
	{
		___depth_5 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter
struct  Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.FilterTypeEnum DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::filterType
	int32_t ___filterType_0;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::a1
	float ___a1_1;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::a2
	float ___a2_2;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::b1
	float ___b1_3;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::b2
	float ___b2_4;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::m1
	float ___m1_5;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::m2
	float ___m2_6;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::m3
	float ___m3_7;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::cutOff
	double ___cutOff_8;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::resonance
	double ___resonance_9;
	// System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::coeffUpdateRequired
	bool ___coeffUpdateRequired_10;

public:
	inline static int32_t get_offset_of_filterType_0() { return static_cast<int32_t>(offsetof(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B, ___filterType_0)); }
	inline int32_t get_filterType_0() const { return ___filterType_0; }
	inline int32_t* get_address_of_filterType_0() { return &___filterType_0; }
	inline void set_filterType_0(int32_t value)
	{
		___filterType_0 = value;
	}

	inline static int32_t get_offset_of_a1_1() { return static_cast<int32_t>(offsetof(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B, ___a1_1)); }
	inline float get_a1_1() const { return ___a1_1; }
	inline float* get_address_of_a1_1() { return &___a1_1; }
	inline void set_a1_1(float value)
	{
		___a1_1 = value;
	}

	inline static int32_t get_offset_of_a2_2() { return static_cast<int32_t>(offsetof(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B, ___a2_2)); }
	inline float get_a2_2() const { return ___a2_2; }
	inline float* get_address_of_a2_2() { return &___a2_2; }
	inline void set_a2_2(float value)
	{
		___a2_2 = value;
	}

	inline static int32_t get_offset_of_b1_3() { return static_cast<int32_t>(offsetof(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B, ___b1_3)); }
	inline float get_b1_3() const { return ___b1_3; }
	inline float* get_address_of_b1_3() { return &___b1_3; }
	inline void set_b1_3(float value)
	{
		___b1_3 = value;
	}

	inline static int32_t get_offset_of_b2_4() { return static_cast<int32_t>(offsetof(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B, ___b2_4)); }
	inline float get_b2_4() const { return ___b2_4; }
	inline float* get_address_of_b2_4() { return &___b2_4; }
	inline void set_b2_4(float value)
	{
		___b2_4 = value;
	}

	inline static int32_t get_offset_of_m1_5() { return static_cast<int32_t>(offsetof(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B, ___m1_5)); }
	inline float get_m1_5() const { return ___m1_5; }
	inline float* get_address_of_m1_5() { return &___m1_5; }
	inline void set_m1_5(float value)
	{
		___m1_5 = value;
	}

	inline static int32_t get_offset_of_m2_6() { return static_cast<int32_t>(offsetof(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B, ___m2_6)); }
	inline float get_m2_6() const { return ___m2_6; }
	inline float* get_address_of_m2_6() { return &___m2_6; }
	inline void set_m2_6(float value)
	{
		___m2_6 = value;
	}

	inline static int32_t get_offset_of_m3_7() { return static_cast<int32_t>(offsetof(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B, ___m3_7)); }
	inline float get_m3_7() const { return ___m3_7; }
	inline float* get_address_of_m3_7() { return &___m3_7; }
	inline void set_m3_7(float value)
	{
		___m3_7 = value;
	}

	inline static int32_t get_offset_of_cutOff_8() { return static_cast<int32_t>(offsetof(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B, ___cutOff_8)); }
	inline double get_cutOff_8() const { return ___cutOff_8; }
	inline double* get_address_of_cutOff_8() { return &___cutOff_8; }
	inline void set_cutOff_8(double value)
	{
		___cutOff_8 = value;
	}

	inline static int32_t get_offset_of_resonance_9() { return static_cast<int32_t>(offsetof(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B, ___resonance_9)); }
	inline double get_resonance_9() const { return ___resonance_9; }
	inline double* get_address_of_resonance_9() { return &___resonance_9; }
	inline void set_resonance_9(double value)
	{
		___resonance_9 = value;
	}

	inline static int32_t get_offset_of_coeffUpdateRequired_10() { return static_cast<int32_t>(offsetof(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B, ___coeffUpdateRequired_10)); }
	inline bool get_coeffUpdateRequired_10() const { return ___coeffUpdateRequired_10; }
	inline bool* get_address_of_coeffUpdateRequired_10() { return &___coeffUpdateRequired_10; }
	inline void set_coeffUpdateRequired_10(bool value)
	{
		___coeffUpdateRequired_10 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator
struct  Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.LoopModeEnum DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::loopMethod
	int32_t ___loopMethod_4;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::loopStart
	double ___loopStart_5;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::loopEnd
	double ___loopEnd_6;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::start
	double ___start_7;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::end
	double ___end_8;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::startOffset
	double ___startOffset_9;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::genPeriod
	double ___genPeriod_10;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::freq
	double ___freq_11;
	// System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::root
	int16_t ___root_12;
	// System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::noteTrack
	int16_t ___noteTrack_13;
	// System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::velTrack
	int16_t ___velTrack_14;
	// System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::tuneCents
	int16_t ___tuneCents_15;

public:
	inline static int32_t get_offset_of_loopMethod_4() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___loopMethod_4)); }
	inline int32_t get_loopMethod_4() const { return ___loopMethod_4; }
	inline int32_t* get_address_of_loopMethod_4() { return &___loopMethod_4; }
	inline void set_loopMethod_4(int32_t value)
	{
		___loopMethod_4 = value;
	}

	inline static int32_t get_offset_of_loopStart_5() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___loopStart_5)); }
	inline double get_loopStart_5() const { return ___loopStart_5; }
	inline double* get_address_of_loopStart_5() { return &___loopStart_5; }
	inline void set_loopStart_5(double value)
	{
		___loopStart_5 = value;
	}

	inline static int32_t get_offset_of_loopEnd_6() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___loopEnd_6)); }
	inline double get_loopEnd_6() const { return ___loopEnd_6; }
	inline double* get_address_of_loopEnd_6() { return &___loopEnd_6; }
	inline void set_loopEnd_6(double value)
	{
		___loopEnd_6 = value;
	}

	inline static int32_t get_offset_of_start_7() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___start_7)); }
	inline double get_start_7() const { return ___start_7; }
	inline double* get_address_of_start_7() { return &___start_7; }
	inline void set_start_7(double value)
	{
		___start_7 = value;
	}

	inline static int32_t get_offset_of_end_8() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___end_8)); }
	inline double get_end_8() const { return ___end_8; }
	inline double* get_address_of_end_8() { return &___end_8; }
	inline void set_end_8(double value)
	{
		___end_8 = value;
	}

	inline static int32_t get_offset_of_startOffset_9() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___startOffset_9)); }
	inline double get_startOffset_9() const { return ___startOffset_9; }
	inline double* get_address_of_startOffset_9() { return &___startOffset_9; }
	inline void set_startOffset_9(double value)
	{
		___startOffset_9 = value;
	}

	inline static int32_t get_offset_of_genPeriod_10() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___genPeriod_10)); }
	inline double get_genPeriod_10() const { return ___genPeriod_10; }
	inline double* get_address_of_genPeriod_10() { return &___genPeriod_10; }
	inline void set_genPeriod_10(double value)
	{
		___genPeriod_10 = value;
	}

	inline static int32_t get_offset_of_freq_11() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___freq_11)); }
	inline double get_freq_11() const { return ___freq_11; }
	inline double* get_address_of_freq_11() { return &___freq_11; }
	inline void set_freq_11(double value)
	{
		___freq_11 = value;
	}

	inline static int32_t get_offset_of_root_12() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___root_12)); }
	inline int16_t get_root_12() const { return ___root_12; }
	inline int16_t* get_address_of_root_12() { return &___root_12; }
	inline void set_root_12(int16_t value)
	{
		___root_12 = value;
	}

	inline static int32_t get_offset_of_noteTrack_13() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___noteTrack_13)); }
	inline int16_t get_noteTrack_13() const { return ___noteTrack_13; }
	inline int16_t* get_address_of_noteTrack_13() { return &___noteTrack_13; }
	inline void set_noteTrack_13(int16_t value)
	{
		___noteTrack_13 = value;
	}

	inline static int32_t get_offset_of_velTrack_14() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___velTrack_14)); }
	inline int16_t get_velTrack_14() const { return ___velTrack_14; }
	inline int16_t* get_address_of_velTrack_14() { return &___velTrack_14; }
	inline void set_velTrack_14(int16_t value)
	{
		___velTrack_14 = value;
	}

	inline static int32_t get_offset_of_tuneCents_15() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E, ___tuneCents_15)); }
	inline int16_t get_tuneCents_15() const { return ___tuneCents_15; }
	inline int16_t* get_address_of_tuneCents_15() { return &___tuneCents_15; }
	inline void set_tuneCents_15(int16_t value)
	{
		___tuneCents_15 = value;
	}
};

struct Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E_StaticFields
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SineGenerator DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::DefaultSine
	SineGenerator_t271E0AB6E9F316CE63CFF199A1FBCBB8D4F06DCF * ___DefaultSine_0;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SawGenerator DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::DefaultSaw
	SawGenerator_t98C5A484F9F525263F1AF4793C9B77DD4F909552 * ___DefaultSaw_1;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SquareGenerator DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::DefaultSquare
	SquareGenerator_t5F29A8328021A4242A7FCE442A16291C7442A86B * ___DefaultSquare_2;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.TriangleGenerator DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::DefaultTriangle
	TriangleGenerator_tA00D6605C3FB1CD04F819ED1F1A2CBE3EC99F8E6 * ___DefaultTriangle_3;

public:
	inline static int32_t get_offset_of_DefaultSine_0() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E_StaticFields, ___DefaultSine_0)); }
	inline SineGenerator_t271E0AB6E9F316CE63CFF199A1FBCBB8D4F06DCF * get_DefaultSine_0() const { return ___DefaultSine_0; }
	inline SineGenerator_t271E0AB6E9F316CE63CFF199A1FBCBB8D4F06DCF ** get_address_of_DefaultSine_0() { return &___DefaultSine_0; }
	inline void set_DefaultSine_0(SineGenerator_t271E0AB6E9F316CE63CFF199A1FBCBB8D4F06DCF * value)
	{
		___DefaultSine_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultSine_0), (void*)value);
	}

	inline static int32_t get_offset_of_DefaultSaw_1() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E_StaticFields, ___DefaultSaw_1)); }
	inline SawGenerator_t98C5A484F9F525263F1AF4793C9B77DD4F909552 * get_DefaultSaw_1() const { return ___DefaultSaw_1; }
	inline SawGenerator_t98C5A484F9F525263F1AF4793C9B77DD4F909552 ** get_address_of_DefaultSaw_1() { return &___DefaultSaw_1; }
	inline void set_DefaultSaw_1(SawGenerator_t98C5A484F9F525263F1AF4793C9B77DD4F909552 * value)
	{
		___DefaultSaw_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultSaw_1), (void*)value);
	}

	inline static int32_t get_offset_of_DefaultSquare_2() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E_StaticFields, ___DefaultSquare_2)); }
	inline SquareGenerator_t5F29A8328021A4242A7FCE442A16291C7442A86B * get_DefaultSquare_2() const { return ___DefaultSquare_2; }
	inline SquareGenerator_t5F29A8328021A4242A7FCE442A16291C7442A86B ** get_address_of_DefaultSquare_2() { return &___DefaultSquare_2; }
	inline void set_DefaultSquare_2(SquareGenerator_t5F29A8328021A4242A7FCE442A16291C7442A86B * value)
	{
		___DefaultSquare_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultSquare_2), (void*)value);
	}

	inline static int32_t get_offset_of_DefaultTriangle_3() { return static_cast<int32_t>(offsetof(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E_StaticFields, ___DefaultTriangle_3)); }
	inline TriangleGenerator_tA00D6605C3FB1CD04F819ED1F1A2CBE3EC99F8E6 * get_DefaultTriangle_3() const { return ___DefaultTriangle_3; }
	inline TriangleGenerator_tA00D6605C3FB1CD04F819ED1F1A2CBE3EC99F8E6 ** get_address_of_DefaultTriangle_3() { return &___DefaultTriangle_3; }
	inline void set_DefaultTriangle_3(TriangleGenerator_tA00D6605C3FB1CD04F819ED1F1A2CBE3EC99F8E6 * value)
	{
		___DefaultTriangle_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___DefaultTriangle_3), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters
struct  GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237  : public RuntimeObject
{
public:
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters::phase
	double ___phase_0;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters::currentStart
	double ___currentStart_1;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters::currentEnd
	double ___currentEnd_2;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.GeneratorStateEnum DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters::currentState
	int32_t ___currentState_3;

public:
	inline static int32_t get_offset_of_phase_0() { return static_cast<int32_t>(offsetof(GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237, ___phase_0)); }
	inline double get_phase_0() const { return ___phase_0; }
	inline double* get_address_of_phase_0() { return &___phase_0; }
	inline void set_phase_0(double value)
	{
		___phase_0 = value;
	}

	inline static int32_t get_offset_of_currentStart_1() { return static_cast<int32_t>(offsetof(GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237, ___currentStart_1)); }
	inline double get_currentStart_1() const { return ___currentStart_1; }
	inline double* get_address_of_currentStart_1() { return &___currentStart_1; }
	inline void set_currentStart_1(double value)
	{
		___currentStart_1 = value;
	}

	inline static int32_t get_offset_of_currentEnd_2() { return static_cast<int32_t>(offsetof(GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237, ___currentEnd_2)); }
	inline double get_currentEnd_2() const { return ___currentEnd_2; }
	inline double* get_address_of_currentEnd_2() { return &___currentEnd_2; }
	inline void set_currentEnd_2(double value)
	{
		___currentEnd_2 = value;
	}

	inline static int32_t get_offset_of_currentState_3() { return static_cast<int32_t>(offsetof(GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237, ___currentState_3)); }
	inline int32_t get_currentState_3() const { return ___currentState_3; }
	inline int32_t* get_address_of_currentState_3() { return &___currentState_3; }
	inline void set_currentState_3(int32_t value)
	{
		___currentState_3 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo
struct  Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.LfoStateEnum DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::lfoState
	int32_t ___lfoState_0;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::phase
	double ___phase_1;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::value
	double ___value_2;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::increment
	double ___increment_3;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::frequency
	double ___frequency_4;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::depth
	double ___depth_5;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::delayTime
	int32_t ___delayTime_6;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::generator
	Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E * ___generator_7;

public:
	inline static int32_t get_offset_of_lfoState_0() { return static_cast<int32_t>(offsetof(Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A, ___lfoState_0)); }
	inline int32_t get_lfoState_0() const { return ___lfoState_0; }
	inline int32_t* get_address_of_lfoState_0() { return &___lfoState_0; }
	inline void set_lfoState_0(int32_t value)
	{
		___lfoState_0 = value;
	}

	inline static int32_t get_offset_of_phase_1() { return static_cast<int32_t>(offsetof(Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A, ___phase_1)); }
	inline double get_phase_1() const { return ___phase_1; }
	inline double* get_address_of_phase_1() { return &___phase_1; }
	inline void set_phase_1(double value)
	{
		___phase_1 = value;
	}

	inline static int32_t get_offset_of_value_2() { return static_cast<int32_t>(offsetof(Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A, ___value_2)); }
	inline double get_value_2() const { return ___value_2; }
	inline double* get_address_of_value_2() { return &___value_2; }
	inline void set_value_2(double value)
	{
		___value_2 = value;
	}

	inline static int32_t get_offset_of_increment_3() { return static_cast<int32_t>(offsetof(Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A, ___increment_3)); }
	inline double get_increment_3() const { return ___increment_3; }
	inline double* get_address_of_increment_3() { return &___increment_3; }
	inline void set_increment_3(double value)
	{
		___increment_3 = value;
	}

	inline static int32_t get_offset_of_frequency_4() { return static_cast<int32_t>(offsetof(Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A, ___frequency_4)); }
	inline double get_frequency_4() const { return ___frequency_4; }
	inline double* get_address_of_frequency_4() { return &___frequency_4; }
	inline void set_frequency_4(double value)
	{
		___frequency_4 = value;
	}

	inline static int32_t get_offset_of_depth_5() { return static_cast<int32_t>(offsetof(Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A, ___depth_5)); }
	inline double get_depth_5() const { return ___depth_5; }
	inline double* get_address_of_depth_5() { return &___depth_5; }
	inline void set_depth_5(double value)
	{
		___depth_5 = value;
	}

	inline static int32_t get_offset_of_delayTime_6() { return static_cast<int32_t>(offsetof(Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A, ___delayTime_6)); }
	inline int32_t get_delayTime_6() const { return ___delayTime_6; }
	inline int32_t* get_address_of_delayTime_6() { return &___delayTime_6; }
	inline void set_delayTime_6(int32_t value)
	{
		___delayTime_6 = value;
	}

	inline static int32_t get_offset_of_generator_7() { return static_cast<int32_t>(offsetof(Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A, ___generator_7)); }
	inline Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E * get_generator_7() const { return ___generator_7; }
	inline Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E ** get_address_of_generator_7() { return &___generator_7; }
	inline void set_generator_7(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E * value)
	{
		___generator_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___generator_7), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor
struct  GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.LoopModeEnum DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::LoopMethod
	int32_t ___LoopMethod_0;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.WaveformEnum DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::SamplerType
	int32_t ___SamplerType_1;
	// System.String DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::AssetName
	String_t* ___AssetName_2;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::EndPhase
	double ___EndPhase_3;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::StartPhase
	double ___StartPhase_4;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::LoopEndPhase
	double ___LoopEndPhase_5;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::LoopStartPhase
	double ___LoopStartPhase_6;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::Offset
	double ___Offset_7;
	// System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::Period
	double ___Period_8;
	// System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::Rootkey
	int16_t ___Rootkey_9;
	// System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::KeyTrack
	int16_t ___KeyTrack_10;
	// System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::VelTrack
	int16_t ___VelTrack_11;
	// System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::Tune
	int16_t ___Tune_12;

public:
	inline static int32_t get_offset_of_LoopMethod_0() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___LoopMethod_0)); }
	inline int32_t get_LoopMethod_0() const { return ___LoopMethod_0; }
	inline int32_t* get_address_of_LoopMethod_0() { return &___LoopMethod_0; }
	inline void set_LoopMethod_0(int32_t value)
	{
		___LoopMethod_0 = value;
	}

	inline static int32_t get_offset_of_SamplerType_1() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___SamplerType_1)); }
	inline int32_t get_SamplerType_1() const { return ___SamplerType_1; }
	inline int32_t* get_address_of_SamplerType_1() { return &___SamplerType_1; }
	inline void set_SamplerType_1(int32_t value)
	{
		___SamplerType_1 = value;
	}

	inline static int32_t get_offset_of_AssetName_2() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___AssetName_2)); }
	inline String_t* get_AssetName_2() const { return ___AssetName_2; }
	inline String_t** get_address_of_AssetName_2() { return &___AssetName_2; }
	inline void set_AssetName_2(String_t* value)
	{
		___AssetName_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___AssetName_2), (void*)value);
	}

	inline static int32_t get_offset_of_EndPhase_3() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___EndPhase_3)); }
	inline double get_EndPhase_3() const { return ___EndPhase_3; }
	inline double* get_address_of_EndPhase_3() { return &___EndPhase_3; }
	inline void set_EndPhase_3(double value)
	{
		___EndPhase_3 = value;
	}

	inline static int32_t get_offset_of_StartPhase_4() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___StartPhase_4)); }
	inline double get_StartPhase_4() const { return ___StartPhase_4; }
	inline double* get_address_of_StartPhase_4() { return &___StartPhase_4; }
	inline void set_StartPhase_4(double value)
	{
		___StartPhase_4 = value;
	}

	inline static int32_t get_offset_of_LoopEndPhase_5() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___LoopEndPhase_5)); }
	inline double get_LoopEndPhase_5() const { return ___LoopEndPhase_5; }
	inline double* get_address_of_LoopEndPhase_5() { return &___LoopEndPhase_5; }
	inline void set_LoopEndPhase_5(double value)
	{
		___LoopEndPhase_5 = value;
	}

	inline static int32_t get_offset_of_LoopStartPhase_6() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___LoopStartPhase_6)); }
	inline double get_LoopStartPhase_6() const { return ___LoopStartPhase_6; }
	inline double* get_address_of_LoopStartPhase_6() { return &___LoopStartPhase_6; }
	inline void set_LoopStartPhase_6(double value)
	{
		___LoopStartPhase_6 = value;
	}

	inline static int32_t get_offset_of_Offset_7() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___Offset_7)); }
	inline double get_Offset_7() const { return ___Offset_7; }
	inline double* get_address_of_Offset_7() { return &___Offset_7; }
	inline void set_Offset_7(double value)
	{
		___Offset_7 = value;
	}

	inline static int32_t get_offset_of_Period_8() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___Period_8)); }
	inline double get_Period_8() const { return ___Period_8; }
	inline double* get_address_of_Period_8() { return &___Period_8; }
	inline void set_Period_8(double value)
	{
		___Period_8 = value;
	}

	inline static int32_t get_offset_of_Rootkey_9() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___Rootkey_9)); }
	inline int16_t get_Rootkey_9() const { return ___Rootkey_9; }
	inline int16_t* get_address_of_Rootkey_9() { return &___Rootkey_9; }
	inline void set_Rootkey_9(int16_t value)
	{
		___Rootkey_9 = value;
	}

	inline static int32_t get_offset_of_KeyTrack_10() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___KeyTrack_10)); }
	inline int16_t get_KeyTrack_10() const { return ___KeyTrack_10; }
	inline int16_t* get_address_of_KeyTrack_10() { return &___KeyTrack_10; }
	inline void set_KeyTrack_10(int16_t value)
	{
		___KeyTrack_10 = value;
	}

	inline static int32_t get_offset_of_VelTrack_11() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___VelTrack_11)); }
	inline int16_t get_VelTrack_11() const { return ___VelTrack_11; }
	inline int16_t* get_address_of_VelTrack_11() { return &___VelTrack_11; }
	inline void set_VelTrack_11(int16_t value)
	{
		___VelTrack_11 = value;
	}

	inline static int32_t get_offset_of_Tune_12() { return static_cast<int32_t>(offsetof(GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69, ___Tune_12)); }
	inline int16_t get_Tune_12() const { return ___Tune_12; }
	inline int16_t* get_address_of_Tune_12() { return &___Tune_12; }
	inline void set_Tune_12(int16_t value)
	{
		___Tune_12 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch
struct  MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F  : public Patch_t3144A9674F069F684A9B015803D7A714F366C7AD
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_IntervalType DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::iType
	int32_t ___iType_3;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval[] DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::intervalList
	PatchIntervalU5BU5D_t30C33B6114206178CEB1742AC87864000E1CA7A5* ___intervalList_4;

public:
	inline static int32_t get_offset_of_iType_3() { return static_cast<int32_t>(offsetof(MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F, ___iType_3)); }
	inline int32_t get_iType_3() const { return ___iType_3; }
	inline int32_t* get_address_of_iType_3() { return &___iType_3; }
	inline void set_iType_3(int32_t value)
	{
		___iType_3 = value;
	}

	inline static int32_t get_offset_of_intervalList_4() { return static_cast<int32_t>(offsetof(MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F, ___intervalList_4)); }
	inline PatchIntervalU5BU5D_t30C33B6114206178CEB1742AC87864000E1CA7A5* get_intervalList_4() const { return ___intervalList_4; }
	inline PatchIntervalU5BU5D_t30C33B6114206178CEB1742AC87864000E1CA7A5** get_address_of_intervalList_4() { return &___intervalList_4; }
	inline void set_intervalList_4(PatchIntervalU5BU5D_t30C33B6114206178CEB1742AC87864000E1CA7A5* value)
	{
		___intervalList_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___intervalList_4), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Sf2.Generator
struct  Generator_t130837BC1C1D35AD971F96F41B246FF13AE84D93  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Sf2.GeneratorEnum DaggerfallWorkshop.AudioSynthesis.Sf2.Generator::gentype
	int32_t ___gentype_0;
	// System.UInt16 DaggerfallWorkshop.AudioSynthesis.Sf2.Generator::rawAmount
	uint16_t ___rawAmount_1;

public:
	inline static int32_t get_offset_of_gentype_0() { return static_cast<int32_t>(offsetof(Generator_t130837BC1C1D35AD971F96F41B246FF13AE84D93, ___gentype_0)); }
	inline int32_t get_gentype_0() const { return ___gentype_0; }
	inline int32_t* get_address_of_gentype_0() { return &___gentype_0; }
	inline void set_gentype_0(int32_t value)
	{
		___gentype_0 = value;
	}

	inline static int32_t get_offset_of_rawAmount_1() { return static_cast<int32_t>(offsetof(Generator_t130837BC1C1D35AD971F96F41B246FF13AE84D93, ___rawAmount_1)); }
	inline uint16_t get_rawAmount_1() const { return ___rawAmount_1; }
	inline uint16_t* get_address_of_rawAmount_1() { return &___rawAmount_1; }
	inline void set_rawAmount_1(uint16_t value)
	{
		___rawAmount_1 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator
struct  Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Sf2.ModulatorType DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator::sourceModulationData
	ModulatorType_t1C1EA8C665813F52ABF1788447771CD0C74A2C9B * ___sourceModulationData_0;
	// DaggerfallWorkshop.AudioSynthesis.Sf2.GeneratorEnum DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator::destinationGenerator
	int32_t ___destinationGenerator_1;
	// System.Int16 DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator::amount
	int16_t ___amount_2;
	// DaggerfallWorkshop.AudioSynthesis.Sf2.ModulatorType DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator::sourceModulationAmount
	ModulatorType_t1C1EA8C665813F52ABF1788447771CD0C74A2C9B * ___sourceModulationAmount_3;
	// DaggerfallWorkshop.AudioSynthesis.Sf2.TransformEnum DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator::sourceTransform
	int32_t ___sourceTransform_4;

public:
	inline static int32_t get_offset_of_sourceModulationData_0() { return static_cast<int32_t>(offsetof(Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83, ___sourceModulationData_0)); }
	inline ModulatorType_t1C1EA8C665813F52ABF1788447771CD0C74A2C9B * get_sourceModulationData_0() const { return ___sourceModulationData_0; }
	inline ModulatorType_t1C1EA8C665813F52ABF1788447771CD0C74A2C9B ** get_address_of_sourceModulationData_0() { return &___sourceModulationData_0; }
	inline void set_sourceModulationData_0(ModulatorType_t1C1EA8C665813F52ABF1788447771CD0C74A2C9B * value)
	{
		___sourceModulationData_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sourceModulationData_0), (void*)value);
	}

	inline static int32_t get_offset_of_destinationGenerator_1() { return static_cast<int32_t>(offsetof(Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83, ___destinationGenerator_1)); }
	inline int32_t get_destinationGenerator_1() const { return ___destinationGenerator_1; }
	inline int32_t* get_address_of_destinationGenerator_1() { return &___destinationGenerator_1; }
	inline void set_destinationGenerator_1(int32_t value)
	{
		___destinationGenerator_1 = value;
	}

	inline static int32_t get_offset_of_amount_2() { return static_cast<int32_t>(offsetof(Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83, ___amount_2)); }
	inline int16_t get_amount_2() const { return ___amount_2; }
	inline int16_t* get_address_of_amount_2() { return &___amount_2; }
	inline void set_amount_2(int16_t value)
	{
		___amount_2 = value;
	}

	inline static int32_t get_offset_of_sourceModulationAmount_3() { return static_cast<int32_t>(offsetof(Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83, ___sourceModulationAmount_3)); }
	inline ModulatorType_t1C1EA8C665813F52ABF1788447771CD0C74A2C9B * get_sourceModulationAmount_3() const { return ___sourceModulationAmount_3; }
	inline ModulatorType_t1C1EA8C665813F52ABF1788447771CD0C74A2C9B ** get_address_of_sourceModulationAmount_3() { return &___sourceModulationAmount_3; }
	inline void set_sourceModulationAmount_3(ModulatorType_t1C1EA8C665813F52ABF1788447771CD0C74A2C9B * value)
	{
		___sourceModulationAmount_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sourceModulationAmount_3), (void*)value);
	}

	inline static int32_t get_offset_of_sourceTransform_4() { return static_cast<int32_t>(offsetof(Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83, ___sourceTransform_4)); }
	inline int32_t get_sourceTransform_4() const { return ___sourceTransform_4; }
	inline int32_t* get_address_of_sourceTransform_4() { return &___sourceTransform_4; }
	inline void set_sourceTransform_4(int32_t value)
	{
		___sourceTransform_4 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer
struct  Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB  : public RuntimeObject
{
public:
	// System.Single[] DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::sampleBuffer
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___sampleBuffer_17;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::voiceManager
	VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * ___voiceManager_18;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::audioChannels
	int32_t ___audioChannels_19;
	// System.Boolean DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::littleEndian
	bool ___littleEndian_20;
	// DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::bank
	PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * ___bank_21;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::sampleRate
	int32_t ___sampleRate_22;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::mainVolume
	float ___mainVolume_23;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::synthGain
	float ___synthGain_24;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::microBufferSize
	int32_t ___microBufferSize_25;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::microBufferCount
	int32_t ___microBufferCount_26;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters[] DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::synthChannels
	SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* ___synthChannels_27;
	// System.Collections.Generic.Queue`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage> DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::midiEventQueue
	Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * ___midiEventQueue_28;
	// System.Int32[] DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::midiEventCounts
	Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* ___midiEventCounts_29;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch[] DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::layerList
	PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* ___layerList_30;

public:
	inline static int32_t get_offset_of_sampleBuffer_17() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___sampleBuffer_17)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_sampleBuffer_17() const { return ___sampleBuffer_17; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_sampleBuffer_17() { return &___sampleBuffer_17; }
	inline void set_sampleBuffer_17(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___sampleBuffer_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sampleBuffer_17), (void*)value);
	}

	inline static int32_t get_offset_of_voiceManager_18() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___voiceManager_18)); }
	inline VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * get_voiceManager_18() const { return ___voiceManager_18; }
	inline VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 ** get_address_of_voiceManager_18() { return &___voiceManager_18; }
	inline void set_voiceManager_18(VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * value)
	{
		___voiceManager_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___voiceManager_18), (void*)value);
	}

	inline static int32_t get_offset_of_audioChannels_19() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___audioChannels_19)); }
	inline int32_t get_audioChannels_19() const { return ___audioChannels_19; }
	inline int32_t* get_address_of_audioChannels_19() { return &___audioChannels_19; }
	inline void set_audioChannels_19(int32_t value)
	{
		___audioChannels_19 = value;
	}

	inline static int32_t get_offset_of_littleEndian_20() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___littleEndian_20)); }
	inline bool get_littleEndian_20() const { return ___littleEndian_20; }
	inline bool* get_address_of_littleEndian_20() { return &___littleEndian_20; }
	inline void set_littleEndian_20(bool value)
	{
		___littleEndian_20 = value;
	}

	inline static int32_t get_offset_of_bank_21() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___bank_21)); }
	inline PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * get_bank_21() const { return ___bank_21; }
	inline PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 ** get_address_of_bank_21() { return &___bank_21; }
	inline void set_bank_21(PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * value)
	{
		___bank_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___bank_21), (void*)value);
	}

	inline static int32_t get_offset_of_sampleRate_22() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___sampleRate_22)); }
	inline int32_t get_sampleRate_22() const { return ___sampleRate_22; }
	inline int32_t* get_address_of_sampleRate_22() { return &___sampleRate_22; }
	inline void set_sampleRate_22(int32_t value)
	{
		___sampleRate_22 = value;
	}

	inline static int32_t get_offset_of_mainVolume_23() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___mainVolume_23)); }
	inline float get_mainVolume_23() const { return ___mainVolume_23; }
	inline float* get_address_of_mainVolume_23() { return &___mainVolume_23; }
	inline void set_mainVolume_23(float value)
	{
		___mainVolume_23 = value;
	}

	inline static int32_t get_offset_of_synthGain_24() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___synthGain_24)); }
	inline float get_synthGain_24() const { return ___synthGain_24; }
	inline float* get_address_of_synthGain_24() { return &___synthGain_24; }
	inline void set_synthGain_24(float value)
	{
		___synthGain_24 = value;
	}

	inline static int32_t get_offset_of_microBufferSize_25() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___microBufferSize_25)); }
	inline int32_t get_microBufferSize_25() const { return ___microBufferSize_25; }
	inline int32_t* get_address_of_microBufferSize_25() { return &___microBufferSize_25; }
	inline void set_microBufferSize_25(int32_t value)
	{
		___microBufferSize_25 = value;
	}

	inline static int32_t get_offset_of_microBufferCount_26() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___microBufferCount_26)); }
	inline int32_t get_microBufferCount_26() const { return ___microBufferCount_26; }
	inline int32_t* get_address_of_microBufferCount_26() { return &___microBufferCount_26; }
	inline void set_microBufferCount_26(int32_t value)
	{
		___microBufferCount_26 = value;
	}

	inline static int32_t get_offset_of_synthChannels_27() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___synthChannels_27)); }
	inline SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* get_synthChannels_27() const { return ___synthChannels_27; }
	inline SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE** get_address_of_synthChannels_27() { return &___synthChannels_27; }
	inline void set_synthChannels_27(SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* value)
	{
		___synthChannels_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___synthChannels_27), (void*)value);
	}

	inline static int32_t get_offset_of_midiEventQueue_28() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___midiEventQueue_28)); }
	inline Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * get_midiEventQueue_28() const { return ___midiEventQueue_28; }
	inline Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 ** get_address_of_midiEventQueue_28() { return &___midiEventQueue_28; }
	inline void set_midiEventQueue_28(Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * value)
	{
		___midiEventQueue_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___midiEventQueue_28), (void*)value);
	}

	inline static int32_t get_offset_of_midiEventCounts_29() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___midiEventCounts_29)); }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* get_midiEventCounts_29() const { return ___midiEventCounts_29; }
	inline Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32** get_address_of_midiEventCounts_29() { return &___midiEventCounts_29; }
	inline void set_midiEventCounts_29(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* value)
	{
		___midiEventCounts_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___midiEventCounts_29), (void*)value);
	}

	inline static int32_t get_offset_of_layerList_30() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB, ___layerList_30)); }
	inline PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* get_layerList_30() const { return ___layerList_30; }
	inline PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75** get_address_of_layerList_30() { return &___layerList_30; }
	inline void set_layerList_30(PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* value)
	{
		___layerList_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___layerList_30), (void*)value);
	}
};

struct Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB_StaticFields
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.InterpolationEnum DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::InterpolationMode
	int32_t ___InterpolationMode_16;

public:
	inline static int32_t get_offset_of_InterpolationMode_16() { return static_cast<int32_t>(offsetof(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB_StaticFields, ___InterpolationMode_16)); }
	inline int32_t get_InterpolationMode_16() const { return ___InterpolationMode_16; }
	inline int32_t* get_address_of_InterpolationMode_16() { return &___InterpolationMode_16; }
	inline void set_InterpolationMode_16(int32_t value)
	{
		___InterpolationMode_16 = value;
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager
struct  VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9  : public RuntimeObject
{
public:
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceStealEnum DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::stealingMethod
	int32_t ___stealingMethod_0;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::polyphony
	int32_t ___polyphony_1;
	// System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice> DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::freeVoices
	LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * ___freeVoices_2;
	// System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice> DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::activeVoices
	LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * ___activeVoices_3;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode[0...,0...] DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::registry
	VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* ___registry_4;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice[] DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::voicePool
	VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A* ___voicePool_5;
	// System.Collections.Generic.Stack`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode> DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::vnodes
	Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * ___vnodes_6;

public:
	inline static int32_t get_offset_of_stealingMethod_0() { return static_cast<int32_t>(offsetof(VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9, ___stealingMethod_0)); }
	inline int32_t get_stealingMethod_0() const { return ___stealingMethod_0; }
	inline int32_t* get_address_of_stealingMethod_0() { return &___stealingMethod_0; }
	inline void set_stealingMethod_0(int32_t value)
	{
		___stealingMethod_0 = value;
	}

	inline static int32_t get_offset_of_polyphony_1() { return static_cast<int32_t>(offsetof(VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9, ___polyphony_1)); }
	inline int32_t get_polyphony_1() const { return ___polyphony_1; }
	inline int32_t* get_address_of_polyphony_1() { return &___polyphony_1; }
	inline void set_polyphony_1(int32_t value)
	{
		___polyphony_1 = value;
	}

	inline static int32_t get_offset_of_freeVoices_2() { return static_cast<int32_t>(offsetof(VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9, ___freeVoices_2)); }
	inline LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * get_freeVoices_2() const { return ___freeVoices_2; }
	inline LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 ** get_address_of_freeVoices_2() { return &___freeVoices_2; }
	inline void set_freeVoices_2(LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * value)
	{
		___freeVoices_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___freeVoices_2), (void*)value);
	}

	inline static int32_t get_offset_of_activeVoices_3() { return static_cast<int32_t>(offsetof(VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9, ___activeVoices_3)); }
	inline LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * get_activeVoices_3() const { return ___activeVoices_3; }
	inline LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 ** get_address_of_activeVoices_3() { return &___activeVoices_3; }
	inline void set_activeVoices_3(LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * value)
	{
		___activeVoices_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___activeVoices_3), (void*)value);
	}

	inline static int32_t get_offset_of_registry_4() { return static_cast<int32_t>(offsetof(VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9, ___registry_4)); }
	inline VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* get_registry_4() const { return ___registry_4; }
	inline VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB** get_address_of_registry_4() { return &___registry_4; }
	inline void set_registry_4(VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* value)
	{
		___registry_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___registry_4), (void*)value);
	}

	inline static int32_t get_offset_of_voicePool_5() { return static_cast<int32_t>(offsetof(VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9, ___voicePool_5)); }
	inline VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A* get_voicePool_5() const { return ___voicePool_5; }
	inline VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A** get_address_of_voicePool_5() { return &___voicePool_5; }
	inline void set_voicePool_5(VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A* value)
	{
		___voicePool_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___voicePool_5), (void*)value);
	}

	inline static int32_t get_offset_of_vnodes_6() { return static_cast<int32_t>(offsetof(VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9, ___vnodes_6)); }
	inline Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * get_vnodes_6() const { return ___vnodes_6; }
	inline Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 ** get_address_of_vnodes_6() { return &___vnodes_6; }
	inline void set_vnodes_6(Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * value)
	{
		___vnodes_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___vnodes_6), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters
struct  VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2  : public RuntimeObject
{
public:
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::channel
	int32_t ___channel_0;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::note
	int32_t ___note_1;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::velocity
	int32_t ___velocity_2;
	// System.Boolean DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::noteOffPending
	bool ___noteOffPending_3;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceStateEnum DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::state
	int32_t ___state_4;
	// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::pitchOffset
	int32_t ___pitchOffset_5;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::volOffset
	float ___volOffset_6;
	// System.Single[] DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::blockBuffer
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___blockBuffer_7;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.UnionData[] DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::pData
	UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F* ___pData_8;
	// DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::synthParams
	SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * ___synthParams_9;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters[] DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::generatorParams
	GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A* ___generatorParams_10;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope[] DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::envelopes
	EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81* ___envelopes_11;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter[] DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::filters
	FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1* ___filters_12;
	// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo[] DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::lfos
	LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52* ___lfos_13;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::mix1
	float ___mix1_14;
	// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::mix2
	float ___mix2_15;

public:
	inline static int32_t get_offset_of_channel_0() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___channel_0)); }
	inline int32_t get_channel_0() const { return ___channel_0; }
	inline int32_t* get_address_of_channel_0() { return &___channel_0; }
	inline void set_channel_0(int32_t value)
	{
		___channel_0 = value;
	}

	inline static int32_t get_offset_of_note_1() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___note_1)); }
	inline int32_t get_note_1() const { return ___note_1; }
	inline int32_t* get_address_of_note_1() { return &___note_1; }
	inline void set_note_1(int32_t value)
	{
		___note_1 = value;
	}

	inline static int32_t get_offset_of_velocity_2() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___velocity_2)); }
	inline int32_t get_velocity_2() const { return ___velocity_2; }
	inline int32_t* get_address_of_velocity_2() { return &___velocity_2; }
	inline void set_velocity_2(int32_t value)
	{
		___velocity_2 = value;
	}

	inline static int32_t get_offset_of_noteOffPending_3() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___noteOffPending_3)); }
	inline bool get_noteOffPending_3() const { return ___noteOffPending_3; }
	inline bool* get_address_of_noteOffPending_3() { return &___noteOffPending_3; }
	inline void set_noteOffPending_3(bool value)
	{
		___noteOffPending_3 = value;
	}

	inline static int32_t get_offset_of_state_4() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___state_4)); }
	inline int32_t get_state_4() const { return ___state_4; }
	inline int32_t* get_address_of_state_4() { return &___state_4; }
	inline void set_state_4(int32_t value)
	{
		___state_4 = value;
	}

	inline static int32_t get_offset_of_pitchOffset_5() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___pitchOffset_5)); }
	inline int32_t get_pitchOffset_5() const { return ___pitchOffset_5; }
	inline int32_t* get_address_of_pitchOffset_5() { return &___pitchOffset_5; }
	inline void set_pitchOffset_5(int32_t value)
	{
		___pitchOffset_5 = value;
	}

	inline static int32_t get_offset_of_volOffset_6() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___volOffset_6)); }
	inline float get_volOffset_6() const { return ___volOffset_6; }
	inline float* get_address_of_volOffset_6() { return &___volOffset_6; }
	inline void set_volOffset_6(float value)
	{
		___volOffset_6 = value;
	}

	inline static int32_t get_offset_of_blockBuffer_7() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___blockBuffer_7)); }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* get_blockBuffer_7() const { return ___blockBuffer_7; }
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** get_address_of_blockBuffer_7() { return &___blockBuffer_7; }
	inline void set_blockBuffer_7(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		___blockBuffer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___blockBuffer_7), (void*)value);
	}

	inline static int32_t get_offset_of_pData_8() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___pData_8)); }
	inline UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F* get_pData_8() const { return ___pData_8; }
	inline UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F** get_address_of_pData_8() { return &___pData_8; }
	inline void set_pData_8(UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F* value)
	{
		___pData_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___pData_8), (void*)value);
	}

	inline static int32_t get_offset_of_synthParams_9() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___synthParams_9)); }
	inline SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * get_synthParams_9() const { return ___synthParams_9; }
	inline SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 ** get_address_of_synthParams_9() { return &___synthParams_9; }
	inline void set_synthParams_9(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * value)
	{
		___synthParams_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___synthParams_9), (void*)value);
	}

	inline static int32_t get_offset_of_generatorParams_10() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___generatorParams_10)); }
	inline GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A* get_generatorParams_10() const { return ___generatorParams_10; }
	inline GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A** get_address_of_generatorParams_10() { return &___generatorParams_10; }
	inline void set_generatorParams_10(GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A* value)
	{
		___generatorParams_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___generatorParams_10), (void*)value);
	}

	inline static int32_t get_offset_of_envelopes_11() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___envelopes_11)); }
	inline EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81* get_envelopes_11() const { return ___envelopes_11; }
	inline EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81** get_address_of_envelopes_11() { return &___envelopes_11; }
	inline void set_envelopes_11(EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81* value)
	{
		___envelopes_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___envelopes_11), (void*)value);
	}

	inline static int32_t get_offset_of_filters_12() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___filters_12)); }
	inline FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1* get_filters_12() const { return ___filters_12; }
	inline FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1** get_address_of_filters_12() { return &___filters_12; }
	inline void set_filters_12(FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1* value)
	{
		___filters_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___filters_12), (void*)value);
	}

	inline static int32_t get_offset_of_lfos_13() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___lfos_13)); }
	inline LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52* get_lfos_13() const { return ___lfos_13; }
	inline LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52** get_address_of_lfos_13() { return &___lfos_13; }
	inline void set_lfos_13(LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52* value)
	{
		___lfos_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lfos_13), (void*)value);
	}

	inline static int32_t get_offset_of_mix1_14() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___mix1_14)); }
	inline float get_mix1_14() const { return ___mix1_14; }
	inline float* get_address_of_mix1_14() { return &___mix1_14; }
	inline void set_mix1_14(float value)
	{
		___mix1_14 = value;
	}

	inline static int32_t get_offset_of_mix2_15() { return static_cast<int32_t>(offsetof(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2, ___mix2_15)); }
	inline float get_mix2_15() const { return ___mix2_15; }
	inline float* get_address_of_mix2_15() { return &___mix2_15; }
	inline void set_mix2_15(float value)
	{
		___mix2_15 = value;
	}
};


// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SquareGenerator
struct  SquareGenerator_t5F29A8328021A4242A7FCE442A16291C7442A86B  : public Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E
{
public:

public:
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.TriangleGenerator
struct  TriangleGenerator_tA00D6605C3FB1CD04F819ED1F1A2CBE3EC99F8E6  : public Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E
{
public:

public:
};


// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.WhiteNoiseGenerator
struct  WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA  : public Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E
{
public:

public:
};

struct WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_StaticFields
{
public:
	// System.Random DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.WhiteNoiseGenerator::random
	Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * ___random_16;

public:
	inline static int32_t get_offset_of_random_16() { return static_cast<int32_t>(offsetof(WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_StaticFields, ___random_16)); }
	inline Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * get_random_16() const { return ___random_16; }
	inline Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 ** get_address_of_random_16() { return &___random_16; }
	inline void set_random_16(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * value)
	{
		___random_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___random_16), (void*)value);
	}
};


// System.ArgumentException
struct  ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.ArgumentNullException
struct  ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB  : public ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Single[]
struct SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) float m_Items[1];

public:
	inline float GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline float* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, float value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline float GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline float* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, float value)
	{
		m_Items[index] = value;
	}
};
// System.Double[]
struct DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) double m_Items[1];

public:
	inline double GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline double* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, double value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline double GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline double* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, double value)
	{
		m_Items[index] = value;
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters[]
struct SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * m_Items[1];

public:
	inline SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Int32[]
struct Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) int32_t m_Items[1];

public:
	inline int32_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline int32_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, int32_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline int32_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline int32_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, int32_t value)
	{
		m_Items[index] = value;
	}
};
// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch[]
struct PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * m_Items[1];

public:
	inline Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Patch_t3144A9674F069F684A9B015803D7A714F366C7AD ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Patch_t3144A9674F069F684A9B015803D7A714F366C7AD ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode[0...,0...]
struct VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * m_Items[1];

public:
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * GetAt(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E ** GetAddressAt(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t i, il2cpp_array_size_t j, VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * value)
	{
		il2cpp_array_size_t iBound = bounds[0].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(i, iBound);
		il2cpp_array_size_t jBound = bounds[1].length;
		IL2CPP_ARRAY_BOUNDS_CHECK(j, jBound);

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * GetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j) const
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items[index];
	}
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E ** GetAddressAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t i, il2cpp_array_size_t j, VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * value)
	{
		il2cpp_array_size_t jBound = bounds[1].length;

		il2cpp_array_size_t index = i * jBound + j;
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Byte[]
struct ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};
// System.Single[][]
struct SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* m_Items[1];

public:
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode[]
struct VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * m_Items[1];

public:
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice[]
struct VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Voice_tA46F178E48B0D771712A0AA48175620981094D47 * m_Items[1];

public:
	inline Voice_tA46F178E48B0D771712A0AA48175620981094D47 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Voice_tA46F178E48B0D771712A0AA48175620981094D47 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Voice_tA46F178E48B0D771712A0AA48175620981094D47 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Voice_tA46F178E48B0D771712A0AA48175620981094D47 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Voice_tA46F178E48B0D771712A0AA48175620981094D47 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Voice_tA46F178E48B0D771712A0AA48175620981094D47 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Synthesis.UnionData[]
struct UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4  m_Items[1];

public:
	inline UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, UnionData_t5A17035FBDF689BCC5883693FA743E364CFCD8A4  value)
	{
		m_Items[index] = value;
	}
};
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters[]
struct GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * m_Items[1];

public:
	inline GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope[]
struct EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40 * m_Items[1];

public:
	inline Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter[]
struct FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B * m_Items[1];

public:
	inline Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo[]
struct LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A * m_Items[1];

public:
	inline Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[]
struct ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83 * m_Items[1];

public:
	inline Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Modulator_t3B5760E8F10BFEF58A1CBFC91E6299CC1DEF7D83 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[]
struct GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Generator_t130837BC1C1D35AD971F96F41B246FF13AE84D93 * m_Items[1];

public:
	inline Generator_t130837BC1C1D35AD971F96F41B246FF13AE84D93 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Generator_t130837BC1C1D35AD971F96F41B246FF13AE84D93 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Generator_t130837BC1C1D35AD971F96F41B246FF13AE84D93 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Generator_t130837BC1C1D35AD971F96F41B246FF13AE84D93 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Generator_t130837BC1C1D35AD971F96F41B246FF13AE84D93 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Generator_t130837BC1C1D35AD971F96F41B246FF13AE84D93 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk_RawZoneData[]
struct RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * m_Items[1];

public:
	inline RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// DaggerfallWorkshop.AudioSynthesis.Sf2.Zone[]
struct ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * m_Items[1];

public:
	inline Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// System.Void System.Collections.Generic.Queue`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Queue_1__ctor_m496CB994FA0D02F62AA06313AA8C460AD550F03E_gshared (Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Queue_1_get_Count_m6331507132D178B8517E38C3F2F0C1698AEC49D1_gshared_inline (Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Queue`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage>::Dequeue()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR MidiMessage_t3810404F35143BF297802CC153A00F2D69755B24  Queue_1_Dequeue_m4313D1B2E96A63B32AAEB715CBD1940EEC4F692F_gshared (Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * __this, const RuntimeMethod* method);
// System.Collections.Generic.LinkedListNode`1<!0> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * LinkedList_1_get_First_m572476A3C4A5CF4A2EBBDF50A76FF35493337871_gshared_inline (LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * LinkedListNode_1_get_Value_m9A641BD5F0614D0060E19C703E9A756391DF7A32_gshared_inline (LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * __this, const RuntimeMethod* method);
// System.Collections.Generic.LinkedListNode`1<!0> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * LinkedListNode_1_get_Next_m414D0F863BD1835DD9116534EC70EBD5B0A14DEC_gshared (LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedList_1_Remove_m40B222689B278F7FAD91C86C1152F272D8AF19FB_gshared (LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * __this, LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * ___node0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::AddFirst(System.Collections.Generic.LinkedListNode`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedList_1_AddFirst_m8F10C4D4F1C00D46F98361EABF8845292950E843_gshared (LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * __this, LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * ___node0, const RuntimeMethod* method);
// System.Collections.Generic.LinkedListNode`1<!0> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * LinkedList_1_AddLast_m619B1130AEA7BAFF3B1D5FCDE25AE40F2B7F8779_gshared (LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * __this, RuntimeObject * ___value0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Stack_1__ctor_m69B6BDFA6505EB829EE255723DEA146E98F18C3C_gshared (Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedList_1__ctor_mCD925EA39B0DCFEBE5928F20616E894691A63518_gshared (LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * __this, RuntimeObject* ___collection0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedList_1__ctor_mF0412326B070EE3A04AB6BAC4836ED176A0F6186_gshared (LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * __this, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LinkedList_1_get_Count_m5AD2F760211A61C7FFD9BE298CFA10F1BF4BE54A_gshared_inline (LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveFirst()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LinkedList_1_RemoveFirst_mDE6EE4997FF1D4D1B9B17405473FD5983B478AE8_gshared (LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<System.Object>::Pop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Stack_1_Pop_m9503124BACE0FDA402D22BC901708C5D99063C12_gshared (Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<System.Object>::Push(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Stack_1_Push_m37749C6ED558EC2D89F38CF78C833D4EE8A2DF04_gshared (Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// System.Collections.Generic.Stack`1/Enumerator<!0> System.Collections.Generic.Stack`1<System.Object>::GetEnumerator()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Enumerator_tF6E3A9686966EF18B6DFA3748229B35E666CE514  Stack_1_GetEnumerator_mD8FAA6BF6E9189B10F3D13D22AAEE4CA795729BA_gshared (Stack_1_t92AC5F573A3C00899B24B775A71B4327D588E981 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1/Enumerator<System.Object>::get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Enumerator_get_Current_m68B7C2A1DD4BB2F486E536F45C1DAD433BCD379D_gshared (Enumerator_tF6E3A9686966EF18B6DFA3748229B35E666CE514 * __this, const RuntimeMethod* method);
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Object>::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Enumerator_MoveNext_mB9EE0FB05AC610C559C9E0C0A9DB6B665817F364_gshared (Enumerator_tF6E3A9686966EF18B6DFA3748229B35E666CE514 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Object>::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Enumerator_Dispose_mE091BA5830D82B8726C4141D15FE8F716E5FA7A9_gshared (Enumerator_tF6E3A9686966EF18B6DFA3748229B35E666CE514 * __this, const RuntimeMethod* method);

// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Generator__ctor_mBD9A3E5A89476277268E8BEBC0F17E5591A903A8 (Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E * __this, GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69 * ___description0, const RuntimeMethod* method);
// System.Int32 System.Math::Sign(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Sign_mF70767482E334D3EA66CAD19C6B575FBB2DAF565 (double ___value0, const RuntimeMethod* method);
// System.Double System.Math::Pow(System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double Math_Pow_mC2C8700DAAD1316AA457A1D271F78CDF0D61AC2F (double ___x0, double ___y1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::ResetControllers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters_ResetControllers_m0027611CEA3F87605275C99E8640F89D27B896E8 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::set_Combined(System.Int16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00 (CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * __this, int16_t ___value0, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::set_Fine(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CCValue_set_Fine_mC5FE00B563D7B73BD9135AC48B16B2FFB5B65B6D (CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * __this, uint8_t ___value0, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::set_Coarse(System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CCValue_set_Coarse_m5344D4D334F3D8D89B69019028B9DF82A2076649 (CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * __this, uint8_t ___value0, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentPan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters_UpdateCurrentPan_m08069E906C15598A7B038FB984A6D5D56B593090 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentPitch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters_UpdateCurrentPitch_mE15F0D19B7BE2079C6347066DA363D578A4FEB08 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentVolume()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters_UpdateCurrentVolume_mF65FAC56F5D1EA02E14EA6FB0D4FBFBFE85C2C61 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, const RuntimeMethod* method);
// System.Int16 DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::get_Combined()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t CCValue_get_Combined_mE4C5B314DE167D020D5A07C59295911EF384E5E4 (CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m6F0ED62933448F8B944E52872E1EE86F6705D306 (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args0, const RuntimeMethod* method);
// System.Void System.ArgumentException::.ctor(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentException__ctor_m71044C2110E357B71A1C30D2561C3F861AF1DC0D (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * __this, String_t* ___message0, String_t* ___paramName1, const RuntimeMethod* method);
// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::Clamp(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SynthHelper_Clamp_m8900A1DC809E91761D20C656937E5EE6B04454F1 (int32_t ___value0, int32_t ___min1, int32_t ___max2, const RuntimeMethod* method);
// System.Int32 System.Math::Max(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Max_mD8AA27386BF012C65303FCDEA041B0CC65056E7B (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::.ctor(DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters__ctor_mAAD2A21AC9314C750B70EA524E730F4235373711 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * ___synth0, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager__ctor_mF7DAE448488693FE2E641B7351FE9BC4A49850DF (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, int32_t ___voiceCount0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Queue`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage>::.ctor()
inline void Queue_1__ctor_m496CB994FA0D02F62AA06313AA8C460AD550F03E (Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * __this, const RuntimeMethod* method)
{
	((  void (*) (Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 *, const RuntimeMethod*))Queue_1__ctor_m496CB994FA0D02F62AA06313AA8C460AD550F03E_gshared)(__this, method);
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::.ctor(DaggerfallWorkshop.AudioSynthesis.IResource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PatchBank__ctor_mE5CC8AFFF8E92DD3C8037AF1BC76B76C8FDF4E0B (PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * __this, RuntimeObject* ___bankFile0, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::LoadBank(DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_LoadBank_mB0296667D3FDB5AB407923A080E4A6C0B9A5D783 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * ___bank0, const RuntimeMethod* method);
// System.Void System.ArgumentNullException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97 (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::UnloadBank()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_UnloadBank_m622C2A0C97C8AA07176FBFDC52E41D9CABE3FDA0 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::NoteOffAll(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_NoteOffAll_m09B0E9EA1805A76EDADE4E4E0443AFD9AB7FAA88 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, bool ___immediate0, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::UnloadPatches()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager_UnloadPatches_mE8C788BCE71CD06511BDD72292BA1339E7CB27D2 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ReleaseAllHoldPedals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_ReleaseAllHoldPedals_mAA8BF558958F700C063299A6DDC2D27B38F16239 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method);
// System.Void System.Array::Clear(System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Clear_mEB42D172C5E0825D340F6209F28578BDDDDCE34F (RuntimeArray * ___array0, int32_t ___index1, int32_t ___length2, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::FillWorkingBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_FillWorkingBuffer_m53938F9A39846C0E6EC7A76C7A1DF39ABC640974 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ConvertWorkingBuffer(System.Single[],System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_ConvertWorkingBuffer_mBC00D122A20A8577DC0AEAACA2B701D114E498FF (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___to0, SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___from1, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.Queue`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage>::get_Count()
inline int32_t Queue_1_get_Count_m6331507132D178B8517E38C3F2F0C1698AEC49D1_inline (Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 *, const RuntimeMethod*))Queue_1_get_Count_m6331507132D178B8517E38C3F2F0C1698AEC49D1_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.Queue`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage>::Dequeue()
inline MidiMessage_t3810404F35143BF297802CC153A00F2D69755B24  Queue_1_Dequeue_m4313D1B2E96A63B32AAEB715CBD1940EEC4F692F (Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * __this, const RuntimeMethod* method)
{
	return ((  MidiMessage_t3810404F35143BF297802CC153A00F2D69755B24  (*) (Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 *, const RuntimeMethod*))Queue_1_Dequeue_m4313D1B2E96A63B32AAEB715CBD1940EEC4F692F_gshared)(__this, method);
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ProcessMidiMessage(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_ProcessMidiMessage_m068A780E4843F4C24833F1C6BDCAFD33D22728BC (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, int32_t ___channel0, int32_t ___command1, int32_t ___data12, int32_t ___data23, const RuntimeMethod* method);
// System.Collections.Generic.LinkedListNode`1<!0> System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>::get_First()
inline LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * __this, const RuntimeMethod* method)
{
	return ((  LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * (*) (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 *, const RuntimeMethod*))LinkedList_1_get_First_m572476A3C4A5CF4A2EBBDF50A76FF35493337871_gshared_inline)(__this, method);
}
// !0 System.Collections.Generic.LinkedListNode`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>::get_Value()
inline Voice_tA46F178E48B0D771712A0AA48175620981094D47 * LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline (LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * __this, const RuntimeMethod* method)
{
	return ((  Voice_tA46F178E48B0D771712A0AA48175620981094D47 * (*) (LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *, const RuntimeMethod*))LinkedListNode_1_get_Value_m9A641BD5F0614D0060E19C703E9A756391DF7A32_gshared_inline)(__this, method);
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Process(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice_Process_m7893B616DA247441F4231BC8EEE204B01C0D348C (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, int32_t ___startIndex0, int32_t ___endIndex1, const RuntimeMethod* method);
// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::get_VoiceParams()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method);
// System.Collections.Generic.LinkedListNode`1<!0> System.Collections.Generic.LinkedListNode`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>::get_Next()
inline LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA (LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * __this, const RuntimeMethod* method)
{
	return ((  LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * (*) (LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *, const RuntimeMethod*))LinkedListNode_1_get_Next_m414D0F863BD1835DD9116534EC70EBD5B0A14DEC_gshared)(__this, method);
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::RemoveFromRegistry(DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, Voice_tA46F178E48B0D771712A0AA48175620981094D47 * ___voice0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>::Remove(System.Collections.Generic.LinkedListNode`1<!0>)
inline void LinkedList_1_Remove_mECC8FD43A15126D559FFF8E2DDC62A1B6F27FADD (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * __this, LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * ___node0, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 *, LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *, const RuntimeMethod*))LinkedList_1_Remove_m40B222689B278F7FAD91C86C1152F272D8AF19FB_gshared)(__this, ___node0, method);
}
// System.Void System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>::AddFirst(System.Collections.Generic.LinkedListNode`1<!0>)
inline void LinkedList_1_AddFirst_m8B3FCFCACB01A06666CB311754F101AA005C505F (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * __this, LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * ___node0, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 *, LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *, const RuntimeMethod*))LinkedList_1_AddFirst_m8F10C4D4F1C00D46F98361EABF8845292950E843_gshared)(__this, ___node0, method);
}
// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SynthHelper_Clamp_mABDAD8ED1DC346DC83659F2142445186C92D3120 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::GetPatch(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * PatchBank_GetPatch_mC86BDB6F5B4657124EE0CD4474172176A4C38FDF (PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * __this, int32_t ___bankNumber0, int32_t ___patchNumber1, const RuntimeMethod* method);
// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::FindPatches(System.Int32,System.Int32,System.Int32,DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t MultiPatch_FindPatches_m86E92FB9B2C65E94311DD2389C67152F477741EE (MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F * __this, int32_t ___channel0, int32_t ___key1, int32_t ___velocity2, PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* ___layers3, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice_Stop_m8E10957D7FABBF4A40F8370F5D8D77EE39A64567 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::RemoveFromRegistry(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager_RemoveFromRegistry_mA5518367D8A75DE5B022440CE4B55CB75E502DE3 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, int32_t ___channel0, int32_t ___note1, const RuntimeMethod* method);
// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::get_ExclusiveGroupTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Patch_get_ExclusiveGroupTarget_mC9F49AFFCEF49C1A5A40D5D87D7A9ABF89FC1AE9 (Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * __this, const RuntimeMethod* method);
// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::get_Patch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * Voice_get_Patch_m4758BB409337AC45CCFB570CF5283E155D725574 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method);
// System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::get_ExclusiveGroup()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Patch_get_ExclusiveGroup_m2E5B9F814EB6C23F1464473925B9DA97EC6529F6 (Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * __this, const RuntimeMethod* method);
// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::GetFreeVoice()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Voice_tA46F178E48B0D771712A0AA48175620981094D47 * VoiceManager_GetFreeVoice_mAA1AE9A540D670DCE54385FBE800AB099AEB3BF5 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Configure(System.Int32,System.Int32,System.Int32,DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch,DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice_Configure_mBA82ADC3E8D74554E84FEF95363DFB9343FEE6F8 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, int32_t ___channel0, int32_t ___note1, int32_t ___velocity2, Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * ___patch3, SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * ___synthParams4, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::AddToRegistry(DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager_AddToRegistry_m2055B93BCC9715FEE1E9D8D2C5E412F2EAD416F6 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, Voice_tA46F178E48B0D771712A0AA48175620981094D47 * ___voice0, const RuntimeMethod* method);
// System.Collections.Generic.LinkedListNode`1<!0> System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>::AddLast(!0)
inline LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * LinkedList_1_AddLast_m9358ECB9018B698E73EF40B3E5A61C65339835CA (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * __this, Voice_tA46F178E48B0D771712A0AA48175620981094D47 * ___value0, const RuntimeMethod* method)
{
	return ((  LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * (*) (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 *, Voice_tA46F178E48B0D771712A0AA48175620981094D47 *, const RuntimeMethod*))LinkedList_1_AddLast_m619B1130AEA7BAFF3B1D5FCDE25AE40F2B7F8779_gshared)(__this, ___value0, method);
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice_Start_mFD62C40C95388C7CA2E2677758C8F25A75BCF807 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::ClearRegistry()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager_ClearRegistry_mE7A9A5542422604EFAE47D0D4AF3B4D0A2F7B096 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::StopImmediately()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice_StopImmediately_m3675EECC2EF7522DE6BF677F419648631A557D21 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::NoteOff(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_NoteOff_mAA9E92FBA1972D7413249B4C79CA46C064BEB2E8 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, int32_t ___channel0, int32_t ___note1, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::NoteOn(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_NoteOn_m04E2EFEF32FD9B4E13480059E411E37F26FD5772 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, int32_t ___channel0, int32_t ___note1, int32_t ___velocity2, const RuntimeMethod* method);
// System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::IsBankLoaded(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PatchBank_IsBankLoaded_m8C02279495AA8D73EDA721748226D7C4058E188E (PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * __this, int32_t ___bankNumber0, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentMod()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters_UpdateCurrentMod_m8B938492578703025D6A3B5F01193E0D80B144E2 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ReleaseHoldPedal(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_ReleaseHoldPedal_m8DD7E693144C996CC0EC5451B000B5F5C6203029 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, int32_t ___channel0, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::.ctor(System.Int32,System.Byte,System.Byte,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MidiEvent__ctor_mC48EC23C4103B22E1ABE6FCD2B2288E9EFCCA423 (MidiEvent_t0A4643C6A8CBCA997171ABC2EFEC507BA8C5AE9C * __this, int32_t ___delta0, uint8_t ___status1, uint8_t ___data12, uint8_t ___data23, const RuntimeMethod* method);
// System.Type System.Type::GetTypeFromHandle(System.RuntimeTypeHandle)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Type_t * Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E (RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ___handle0, const RuntimeMethod* method);
// System.String System.Enum::GetName(System.Type,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Enum_GetName_mA141F96AFDC64AD7020374311750DBA47BFCA8FA (Type_t * ___enumType0, RuntimeObject * ___value1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemCommonEvent::.ctor(System.Int32,System.Byte,System.Byte,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SystemCommonEvent__ctor_m28761B5E8EB12382AEB313AFDA05F256C81D8598 (SystemCommonEvent_t9ED10AE0931452F227792401D07F5DD1645A63CC * __this, int32_t ___delta0, uint8_t ___status1, uint8_t ___data12, uint8_t ___data23, const RuntimeMethod* method);
// System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateSustainTable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* Tables_CreateSustainTable_mAFC5D677E0BB7E15AD4E03BC43A0AB78C9F65BF0 (int32_t ___size0, const RuntimeMethod* method);
// System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::RemoveDenormals(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* Tables_RemoveDenormals_mC91B0A68C891D1245952E94A24A2E79A61F32BC1 (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___data0, const RuntimeMethod* method);
// System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateLinearTable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* Tables_CreateLinearTable_mE8A52196591057548DDA3DE71766B8E8423D5427 (int32_t ___size0, const RuntimeMethod* method);
// System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateConcaveTable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* Tables_CreateConcaveTable_m8DA88814E45FAB3F4ECCCA237BDEDB9AE8A68EC9 (int32_t ___size0, const RuntimeMethod* method);
// System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateConvexTable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* Tables_CreateConvexTable_m805C0608C71A722855983AFAEF2DEA8F689C5590 (int32_t ___size0, const RuntimeMethod* method);
// System.Double[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateCentTable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* Tables_CreateCentTable_mDA1EEA30489EB4C540F4F76AE4D8D946138D6F45 (const RuntimeMethod* method);
// System.Double[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateSemitoneTable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* Tables_CreateSemitoneTable_m8A0B5078A2F897C4D3C6A3538248C6F0CF41C072 (const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceParameters__ctor_m7778D2A8080260881ACA92965AC75360900DF955 (VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceParameters_Reset_mDA734937FFF5A02839B368F183E970EA3EF2AD88 (VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * __this, const RuntimeMethod* method);
// System.String DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::get_Name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Patch_get_Name_m2536C44C9116E695C9BBE81DD5FF0A1A11DDC3B9 (Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44 (String_t* ___str00, String_t* ___str11, String_t* ___str22, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice__ctor_mC8F53C63FE90B4AC4517F3FB27EF8E2CF73ABA28 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceNode__ctor_mC30870DD5AAC5E8AC4D00ED2ADD82BD739D35BE1 (VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Stack`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void Stack_1__ctor_m1B694765A8AEC57CEF3638BAFAC8C7A68961D466 (Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 *, RuntimeObject*, const RuntimeMethod*))Stack_1__ctor_m69B6BDFA6505EB829EE255723DEA146E98F18C3C_gshared)(__this, ___collection0, method);
}
// System.Void System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>::.ctor(System.Collections.Generic.IEnumerable`1<!0>)
inline void LinkedList_1__ctor_m1A0E7433ED02FAF1B17980448DC31395B6DFDC7F (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * __this, RuntimeObject* ___collection0, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 *, RuntimeObject*, const RuntimeMethod*))LinkedList_1__ctor_mCD925EA39B0DCFEBE5928F20616E894691A63518_gshared)(__this, ___collection0, method);
}
// System.Void System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>::.ctor()
inline void LinkedList_1__ctor_m90B1CFAE4273CB108436915F74E71A1650440B74 (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * __this, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 *, const RuntimeMethod*))LinkedList_1__ctor_mF0412326B070EE3A04AB6BAC4836ED176A0F6186_gshared)(__this, method);
}
// System.Int32 System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>::get_Count()
inline int32_t LinkedList_1_get_Count_m583DD5509171DBFF46F108B1FE6439956AA7E1D8_inline (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 *, const RuntimeMethod*))LinkedList_1_get_Count_m5AD2F760211A61C7FFD9BE298CFA10F1BF4BE54A_gshared_inline)(__this, method);
}
// System.Void System.Collections.Generic.LinkedList`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice>::RemoveFirst()
inline void LinkedList_1_RemoveFirst_m5BB3D19F10EFA916947A8A64808BB81F065FFA0B (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * __this, const RuntimeMethod* method)
{
	((  void (*) (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 *, const RuntimeMethod*))LinkedList_1_RemoveFirst_mDE6EE4997FF1D4D1B9B17405473FD5983B478AE8_gshared)(__this, method);
}
// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::StealOldest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Voice_tA46F178E48B0D771712A0AA48175620981094D47 * VoiceManager_StealOldest_m2DC42C690A28F3065FD16CB8E11DF2FCEC9EE24B (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, const RuntimeMethod* method);
// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::StealQuietestVoice()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Voice_tA46F178E48B0D771712A0AA48175620981094D47 * VoiceManager_StealQuietestVoice_m0B1D16AD611B24B483268771F1D4D0F19AA0F826 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.Stack`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode>::Pop()
inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * Stack_1_Pop_m7119358526FF3EE669090369C7C813EBEF1B4563 (Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * __this, const RuntimeMethod* method)
{
	return ((  VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * (*) (Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 *, const RuntimeMethod*))Stack_1_Pop_m9503124BACE0FDA402D22BC901708C5D99063C12_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Stack`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode>::Push(!0)
inline void Stack_1_Push_m98F98B3188600DDF0EFBCBC85110CD878E52E155 (Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * __this, VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * ___item0, const RuntimeMethod* method)
{
	((  void (*) (Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 *, VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *, const RuntimeMethod*))Stack_1_Push_m37749C6ED558EC2D89F38CF78C833D4EE8A2DF04_gshared)(__this, ___item0, method);
}
// System.Collections.Generic.Stack`1/Enumerator<!0> System.Collections.Generic.Stack`1<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode>::GetEnumerator()
inline Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF  Stack_1_GetEnumerator_m9110C85EAA6292636F99B48A549DE02B81ABE62A (Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * __this, const RuntimeMethod* method)
{
	return ((  Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF  (*) (Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 *, const RuntimeMethod*))Stack_1_GetEnumerator_mD8FAA6BF6E9189B10F3D13D22AAEE4CA795729BA_gshared)(__this, method);
}
// !0 System.Collections.Generic.Stack`1/Enumerator<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode>::get_Current()
inline VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * Enumerator_get_Current_m15363A9312B73B1AD3E358A70EBB8BDF280DE3E5 (Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF * __this, const RuntimeMethod* method)
{
	return ((  VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * (*) (Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF *, const RuntimeMethod*))Enumerator_get_Current_m68B7C2A1DD4BB2F486E536F45C1DAD433BCD379D_gshared)(__this, method);
}
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode>::MoveNext()
inline bool Enumerator_MoveNext_mB7075ADE4B6DB1460DF2B652EE538430B536077B (Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF * __this, const RuntimeMethod* method)
{
	return ((  bool (*) (Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF *, const RuntimeMethod*))Enumerator_MoveNext_mB9EE0FB05AC610C559C9E0C0A9DB6B665817F364_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Stack`1/Enumerator<DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager/VoiceNode>::Dispose()
inline void Enumerator_Dispose_m290DF4EA35F9E4F1D910D42A42F4334930DABAA5 (Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF * __this, const RuntimeMethod* method)
{
	((  void (*) (Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF *, const RuntimeMethod*))Enumerator_Dispose_mE091BA5830D82B8726C4141D15FE8F716E5FA7A9_gshared)(__this, method);
}
// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::get_CombinedVolume()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float VoiceParameters_get_CombinedVolume_m94289D4D075359DB54ABF1CC96F2F6B5F84EF004 (VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GeneratorParameters__ctor_m3236F25398ABFBF56DC2671F38892A2E11339D2A (GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Envelope__ctor_mC790D05C10CDD1066A2347F220978EC7C3D812CA (Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Filter__ctor_mF15108D7CC9AC5426630CDBB7A7BB87479FF0574 (Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Lfo__ctor_m35CD7DD1CD4D7AA08749FC169FB112E7D95A1D10 (Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B (String_t* ___format0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method);
// System.Int32 System.Math::Min(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574 (int32_t ___val10, int32_t ___val21, const RuntimeMethod* method);
// System.Void System.Array::Copy(System.Array,System.Int32,System.Array,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877 (RuntimeArray * ___sourceArray0, int32_t ___sourceIndex1, RuntimeArray * ___destinationArray2, int32_t ___destinationIndex3, int32_t ___length4, const RuntimeMethod* method);
// System.Void System.Array::Reverse(System.Array)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Array_Reverse_mB87373AFAC1DBE600CAA60B79A985DD09555BF7D (RuntimeArray * ___array0, const RuntimeMethod* method);
// System.Void System.Random::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Random__ctor_mF40AD1812BABC06235B661CCE513E4F74EEE9F05 (Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * __this, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66 (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Util.Riff.Chunk::.ctor(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Chunk__ctor_m09D15006E79DF3FA8C2E92747839186ECB2661A4 (Chunk_t9103F9401FE26193B5FE338EF34D83156956415C * __this, String_t* ___id0, int32_t ___size1, const RuntimeMethod* method);
// System.Void System.Exception::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11 (Exception_t * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk/RawZoneData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RawZoneData__ctor_mC6A341B8DBB243161F41FA43121208581B74605D (RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zone__ctor_mFEE2F08A75809A1B25C605D48A2581E51922DF97 (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::set_Generators(DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zone_set_Generators_m051434F7E04B03839D9E45F687EF1D168E486E3D (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * __this, GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* ___value0, const RuntimeMethod* method);
// DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[] DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::get_Generators()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* Zone_get_Generators_m3BDD4F7A064D556CD8FD224F4828417D82EAA019 (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * __this, const RuntimeMethod* method);
// System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::set_Modulators(DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zone_set_Modulators_mCD8A63184ED534C3F54CB8C231624B866588FD75 (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * __this, ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* ___value0, const RuntimeMethod* method);
// DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[] DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::get_Modulators()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* Zone_get_Modulators_m819BC5A415FDD098C62F176644BC7B832E9D65DF (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SquareGenerator::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SquareGenerator__ctor_mA92ED26FD608B9BCB0ABB3C7301956A490E75761 (SquareGenerator_t5F29A8328021A4242A7FCE442A16291C7442A86B * __this, GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69 * ___description0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareGenerator__ctor_mA92ED26FD608B9BCB0ABB3C7301956A490E75761_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69 * L_0 = ___description0;
		IL2CPP_RUNTIME_CLASS_INIT(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E_il2cpp_TypeInfo_var);
		Generator__ctor_mBD9A3E5A89476277268E8BEBC0F17E5591A903A8(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_end_8();
		V_0 = (bool)((((int32_t)((((double)L_1) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (L_2)
		{
			goto IL_0030;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_end_8((6.2831853071795862));
	}

IL_0030:
	{
		double L_3 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_start_7();
		V_0 = (bool)((((int32_t)((((double)L_3) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_0;
		if (L_4)
		{
			goto IL_0057;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_start_7((0.0));
	}

IL_0057:
	{
		double L_5 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopEnd_6();
		V_0 = (bool)((((int32_t)((((double)L_5) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_0;
		if (L_6)
		{
			goto IL_007b;
		}
	}
	{
		double L_7 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_end_8();
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_loopEnd_6(L_7);
	}

IL_007b:
	{
		double L_8 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopStart_5();
		V_0 = (bool)((((int32_t)((((double)L_8) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_0;
		if (L_9)
		{
			goto IL_009f;
		}
	}
	{
		double L_10 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_start_7();
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_loopStart_5(L_10);
	}

IL_009f:
	{
		double L_11 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_genPeriod_10();
		V_0 = (bool)((((int32_t)((((double)L_11) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_12 = V_0;
		if (L_12)
		{
			goto IL_00c6;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_genPeriod_10((6.2831853071795862));
	}

IL_00c6:
	{
		int16_t L_13 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_root_12();
		V_0 = (bool)((((int32_t)((((int32_t)L_13) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_14 = V_0;
		if (L_14)
		{
			goto IL_00de;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_root_12((int16_t)((int32_t)69));
	}

IL_00de:
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_freq_11((440.0));
		return;
	}
}
// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SquareGenerator::GetValue(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SquareGenerator_GetValue_mC0B58C49002D6C70197F1441E663A26ADB156F5D (SquareGenerator_t5F29A8328021A4242A7FCE442A16291C7442A86B * __this, double ___phase0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareGenerator_GetValue_mC0B58C49002D6C70197F1441E663A26ADB156F5D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		double L_0 = ___phase0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1 = sin(L_0);
		int32_t L_2 = Math_Sign_mF70767482E334D3EA66CAD19C6B575FBB2DAF565(L_1, /*hidden argument*/NULL);
		V_0 = (((float)((float)L_2)));
		goto IL_0010;
	}

IL_0010:
	{
		float L_3 = V_0;
		return L_3;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SquareGenerator::GetValues(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters,System.Single[],System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SquareGenerator_GetValues_m917D82D8B7EC0FC6DCFB2100C07EC85602F63081 (SquareGenerator_t5F29A8328021A4242A7FCE442A16291C7442A86B * __this, GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * ___generatorParams0, SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___blockBuffer1, double ___increment2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SquareGenerator_GetValues_m917D82D8B7EC0FC6DCFB2100C07EC85602F63081_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	int32_t V_4 = 0;
	{
		V_0 = 0;
	}

IL_0003:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_0 = ___generatorParams0;
		NullCheck(L_0);
		double L_1 = L_0->get_currentEnd_2();
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_2 = ___generatorParams0;
		NullCheck(L_2);
		double L_3 = L_2->get_phase_0();
		double L_4 = ___increment2;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_5 = ceil(((double)((double)((double)il2cpp_codegen_subtract((double)L_1, (double)L_3))/(double)L_4)));
		V_1 = (((int32_t)((int32_t)L_5)));
		int32_t L_6 = V_1;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_7 = ___blockBuffer1;
		NullCheck(L_7);
		int32_t L_8 = V_0;
		V_3 = (bool)((((int32_t)((((int32_t)L_6) > ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))), (int32_t)L_8))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_3;
		if (L_9)
		{
			goto IL_0064;
		}
	}
	{
		goto IL_0054;
	}

IL_002c:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_10 = ___blockBuffer1;
		int32_t L_11 = V_0;
		int32_t L_12 = L_11;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_13 = ___generatorParams0;
		NullCheck(L_13);
		double L_14 = L_13->get_phase_0();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_15 = sin(L_14);
		int32_t L_16 = Math_Sign_mF70767482E334D3EA66CAD19C6B575FBB2DAF565(L_15, /*hidden argument*/NULL);
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_12), (float)(((float)((float)L_16))));
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_17 = ___generatorParams0;
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_18 = L_17;
		NullCheck(L_18);
		double L_19 = L_18->get_phase_0();
		double L_20 = ___increment2;
		NullCheck(L_18);
		L_18->set_phase_0(((double)il2cpp_codegen_add((double)L_19, (double)L_20)));
	}

IL_0054:
	{
		int32_t L_21 = V_0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_22 = ___blockBuffer1;
		NullCheck(L_22);
		V_3 = (bool)((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_22)->max_length))))))? 1 : 0);
		bool L_23 = V_3;
		if (L_23)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_0117;
	}

IL_0064:
	{
		int32_t L_24 = V_0;
		int32_t L_25 = V_1;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_24, (int32_t)L_25));
		goto IL_0093;
	}

IL_006b:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_26 = ___blockBuffer1;
		int32_t L_27 = V_0;
		int32_t L_28 = L_27;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_28, (int32_t)1));
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_29 = ___generatorParams0;
		NullCheck(L_29);
		double L_30 = L_29->get_phase_0();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_31 = sin(L_30);
		int32_t L_32 = Math_Sign_mF70767482E334D3EA66CAD19C6B575FBB2DAF565(L_31, /*hidden argument*/NULL);
		NullCheck(L_26);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_28), (float)(((float)((float)L_32))));
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_33 = ___generatorParams0;
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_34 = L_33;
		NullCheck(L_34);
		double L_35 = L_34->get_phase_0();
		double L_36 = ___increment2;
		NullCheck(L_34);
		L_34->set_phase_0(((double)il2cpp_codegen_add((double)L_35, (double)L_36)));
	}

IL_0093:
	{
		int32_t L_37 = V_0;
		int32_t L_38 = V_2;
		V_3 = (bool)((((int32_t)L_37) < ((int32_t)L_38))? 1 : 0);
		bool L_39 = V_3;
		if (L_39)
		{
			goto IL_006b;
		}
	}
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_40 = ___generatorParams0;
		NullCheck(L_40);
		int32_t L_41 = L_40->get_currentState_3();
		V_4 = L_41;
		int32_t L_42 = V_4;
		switch (L_42)
		{
			case 0:
			{
				goto IL_00b8;
			}
			case 1:
			{
				goto IL_00d9;
			}
			case 2:
			{
				goto IL_00f5;
			}
		}
	}
	{
		goto IL_0116;
	}

IL_00b8:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_43 = ___generatorParams0;
		double L_44 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopStart_5();
		NullCheck(L_43);
		L_43->set_currentStart_1(L_44);
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_45 = ___generatorParams0;
		double L_46 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopEnd_6();
		NullCheck(L_45);
		L_45->set_currentEnd_2(L_46);
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_47 = ___generatorParams0;
		NullCheck(L_47);
		L_47->set_currentState_3(1);
		goto IL_0116;
	}

IL_00d9:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_48 = ___generatorParams0;
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_49 = L_48;
		NullCheck(L_49);
		double L_50 = L_49->get_phase_0();
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_51 = ___generatorParams0;
		NullCheck(L_51);
		double L_52 = L_51->get_currentStart_1();
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_53 = ___generatorParams0;
		NullCheck(L_53);
		double L_54 = L_53->get_currentEnd_2();
		NullCheck(L_49);
		L_49->set_phase_0(((double)il2cpp_codegen_add((double)L_50, (double)((double)il2cpp_codegen_subtract((double)L_52, (double)L_54)))));
		goto IL_0116;
	}

IL_00f5:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_55 = ___generatorParams0;
		NullCheck(L_55);
		L_55->set_currentState_3(3);
		goto IL_010a;
	}

IL_00fe:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_56 = ___blockBuffer1;
		int32_t L_57 = V_0;
		int32_t L_58 = L_57;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_58, (int32_t)1));
		NullCheck(L_56);
		(L_56)->SetAt(static_cast<il2cpp_array_size_t>(L_58), (float)(0.0f));
	}

IL_010a:
	{
		int32_t L_59 = V_0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_60 = ___blockBuffer1;
		NullCheck(L_60);
		V_3 = (bool)((((int32_t)L_59) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_60)->max_length))))))? 1 : 0);
		bool L_61 = V_3;
		if (L_61)
		{
			goto IL_00fe;
		}
	}
	{
		goto IL_0116;
	}

IL_0116:
	{
	}

IL_0117:
	{
		int32_t L_62 = V_0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_63 = ___blockBuffer1;
		NullCheck(L_63);
		V_3 = (bool)((((int32_t)L_62) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_63)->max_length))))))? 1 : 0);
		bool L_64 = V_3;
		if (L_64)
		{
			goto IL_0003;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Double DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::Clamp(System.Double,System.Double,System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double SynthHelper_Clamp_mE5F96B195032983A5C4642B4FCAE05F1766CAEDF (double ___value0, double ___min1, double ___max2, const RuntimeMethod* method)
{
	double V_0 = 0.0;
	bool V_1 = false;
	{
		double L_0 = ___value0;
		double L_1 = ___min1;
		V_1 = (bool)((!(((double)L_0) <= ((double)L_1)))? 1 : 0);
		bool L_2 = V_1;
		if (L_2)
		{
			goto IL_000d;
		}
	}
	{
		double L_3 = ___min1;
		V_0 = L_3;
		goto IL_001d;
	}

IL_000d:
	{
		double L_4 = ___value0;
		double L_5 = ___max2;
		V_1 = (bool)((!(((double)L_4) >= ((double)L_5)))? 1 : 0);
		bool L_6 = V_1;
		if (L_6)
		{
			goto IL_0019;
		}
	}
	{
		double L_7 = ___max2;
		V_0 = L_7;
		goto IL_001d;
	}

IL_0019:
	{
		double L_8 = ___value0;
		V_0 = L_8;
		goto IL_001d;
	}

IL_001d:
	{
		double L_9 = V_0;
		return L_9;
	}
}
// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float SynthHelper_Clamp_mABDAD8ED1DC346DC83659F2142445186C92D3120 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	bool V_1 = false;
	{
		float L_0 = ___value0;
		float L_1 = ___min1;
		V_1 = (bool)((!(((float)L_0) <= ((float)L_1)))? 1 : 0);
		bool L_2 = V_1;
		if (L_2)
		{
			goto IL_000d;
		}
	}
	{
		float L_3 = ___min1;
		V_0 = L_3;
		goto IL_001d;
	}

IL_000d:
	{
		float L_4 = ___value0;
		float L_5 = ___max2;
		V_1 = (bool)((!(((float)L_4) >= ((float)L_5)))? 1 : 0);
		bool L_6 = V_1;
		if (L_6)
		{
			goto IL_0019;
		}
	}
	{
		float L_7 = ___max2;
		V_0 = L_7;
		goto IL_001d;
	}

IL_0019:
	{
		float L_8 = ___value0;
		V_0 = L_8;
		goto IL_001d;
	}

IL_001d:
	{
		float L_9 = V_0;
		return L_9;
	}
}
// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::Clamp(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SynthHelper_Clamp_m8900A1DC809E91761D20C656937E5EE6B04454F1 (int32_t ___value0, int32_t ___min1, int32_t ___max2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = ___value0;
		int32_t L_1 = ___min1;
		V_1 = (bool)((((int32_t)L_0) > ((int32_t)L_1))? 1 : 0);
		bool L_2 = V_1;
		if (L_2)
		{
			goto IL_000d;
		}
	}
	{
		int32_t L_3 = ___min1;
		V_0 = L_3;
		goto IL_001d;
	}

IL_000d:
	{
		int32_t L_4 = ___value0;
		int32_t L_5 = ___max2;
		V_1 = (bool)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
		bool L_6 = V_1;
		if (L_6)
		{
			goto IL_0019;
		}
	}
	{
		int32_t L_7 = ___max2;
		V_0 = L_7;
		goto IL_001d;
	}

IL_0019:
	{
		int32_t L_8 = ___value0;
		V_0 = L_8;
		goto IL_001d;
	}

IL_001d:
	{
		int32_t L_9 = V_0;
		return L_9;
	}
}
// System.Int16 DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::Clamp(System.Int16,System.Int16,System.Int16)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int16_t SynthHelper_Clamp_mBAC49A911FF04B30E21575B6C2E07A7B1F43C2BD (int16_t ___value0, int16_t ___min1, int16_t ___max2, const RuntimeMethod* method)
{
	int16_t V_0 = 0;
	bool V_1 = false;
	{
		int16_t L_0 = ___value0;
		int16_t L_1 = ___min1;
		V_1 = (bool)((((int32_t)L_0) > ((int32_t)L_1))? 1 : 0);
		bool L_2 = V_1;
		if (L_2)
		{
			goto IL_000d;
		}
	}
	{
		int16_t L_3 = ___min1;
		V_0 = L_3;
		goto IL_001d;
	}

IL_000d:
	{
		int16_t L_4 = ___value0;
		int16_t L_5 = ___max2;
		V_1 = (bool)((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
		bool L_6 = V_1;
		if (L_6)
		{
			goto IL_0019;
		}
	}
	{
		int16_t L_7 = ___max2;
		V_0 = L_7;
		goto IL_001d;
	}

IL_0019:
	{
		int16_t L_8 = ___value0;
		V_0 = L_8;
		goto IL_001d;
	}

IL_001d:
	{
		int16_t L_9 = V_0;
		return L_9;
	}
}
// System.Double DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::DBtoLinear(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double SynthHelper_DBtoLinear_m224252A9D521905D87410CE27FA28E548613E53D (double ___dBvalue0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SynthHelper_DBtoLinear_m224252A9D521905D87410CE27FA28E548613E53D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	{
		double L_0 = ___dBvalue0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_1 = Math_Pow_mC2C8700DAAD1316AA457A1D271F78CDF0D61AC2F((10.0), ((double)((double)L_0/(double)(20.0))), /*hidden argument*/NULL);
		V_0 = L_1;
		goto IL_001d;
	}

IL_001d:
	{
		double L_2 = V_0;
		return L_2;
	}
}
// System.Double DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::KeyToFrequency(System.Double,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double SynthHelper_KeyToFrequency_m82BB6EF80E9FBE00A0FD57B67E794D9B01E6C9A4 (double ___key0, int32_t ___rootkey1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SynthHelper_KeyToFrequency_m82BB6EF80E9FBE00A0FD57B67E794D9B01E6C9A4_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	{
		double L_0 = ___key0;
		int32_t L_1 = ___rootkey1;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_2 = Math_Pow_mC2C8700DAAD1316AA457A1D271F78CDF0D61AC2F((2.0), ((double)((double)((double)il2cpp_codegen_subtract((double)L_0, (double)(((double)((double)L_1)))))/(double)(12.0))), /*hidden argument*/NULL);
		V_0 = ((double)il2cpp_codegen_multiply((double)L_2, (double)(440.0)));
		goto IL_002a;
	}

IL_002a:
	{
		double L_3 = V_0;
		return L_3;
	}
}
// System.Double DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::CentsToPitch(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR double SynthHelper_CentsToPitch_mDD57BCD740AA6583A7A5DB8F690AE9B03AE7CAB8 (int32_t ___cents0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SynthHelper_CentsToPitch_mDD57BCD740AA6583A7A5DB8F690AE9B03AE7CAB8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	double V_1 = 0.0;
	bool V_2 = false;
	{
		int32_t L_0 = ___cents0;
		V_0 = ((int32_t)((int32_t)L_0/(int32_t)((int32_t)100)));
		int32_t L_1 = ___cents0;
		int32_t L_2 = V_0;
		___cents0 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_1, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_2, (int32_t)((int32_t)100)))));
		int32_t L_3 = V_0;
		V_2 = (bool)((((int32_t)((((int32_t)L_3) < ((int32_t)((int32_t)-127)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_2;
		if (L_4)
		{
			goto IL_001f;
		}
	}
	{
		V_0 = ((int32_t)-127);
		goto IL_002e;
	}

IL_001f:
	{
		int32_t L_5 = V_0;
		V_2 = (bool)((((int32_t)((((int32_t)L_5) > ((int32_t)((int32_t)127)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_2;
		if (L_6)
		{
			goto IL_002e;
		}
	}
	{
		V_0 = ((int32_t)127);
	}

IL_002e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_il2cpp_TypeInfo_var);
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_7 = ((Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields*)il2cpp_codegen_static_fields_for(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_il2cpp_TypeInfo_var))->get_SemitoneTable_1();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)127), (int32_t)L_8));
		double L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_11 = ((Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields*)il2cpp_codegen_static_fields_for(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_il2cpp_TypeInfo_var))->get_CentTable_2();
		int32_t L_12 = ___cents0;
		NullCheck(L_11);
		int32_t L_13 = ((int32_t)il2cpp_codegen_add((int32_t)((int32_t)100), (int32_t)L_12));
		double L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_1 = ((double)il2cpp_codegen_multiply((double)L_10, (double)L_14));
		goto IL_0046;
	}

IL_0046:
	{
		double L_15 = V_1;
		return L_15;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::.ctor(DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters__ctor_mAAD2A21AC9314C750B70EA524E730F4235373711 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * ___synth0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * L_0 = ___synth0;
		__this->set_synth_15(L_0);
		SynthParameters_ResetControllers_m0027611CEA3F87605275C99E8640F89D27B896E8(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::ResetControllers()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters_ResetControllers_m0027611CEA3F87605275C99E8640F89D27B896E8 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, const RuntimeMethod* method)
{
	{
		__this->set_program_0((uint8_t)0);
		__this->set_bankSelect_1((uint8_t)0);
		__this->set_channelAfterTouch_2((uint8_t)0);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_0 = __this->get_address_of_pan_3();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_0, (int16_t)((int32_t)8192), /*hidden argument*/NULL);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_1 = __this->get_address_of_volume_4();
		CCValue_set_Fine_mC5FE00B563D7B73BD9135AC48B16B2FFB5B65B6D((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_1, (uint8_t)0, /*hidden argument*/NULL);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_2 = __this->get_address_of_volume_4();
		CCValue_set_Coarse_m5344D4D334F3D8D89B69019028B9DF82A2076649((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_2, (uint8_t)((int32_t)100), /*hidden argument*/NULL);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_3 = __this->get_address_of_expression_5();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_3, (int16_t)((int32_t)16383), /*hidden argument*/NULL);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_4 = __this->get_address_of_modRange_6();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_4, (int16_t)0, /*hidden argument*/NULL);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_5 = __this->get_address_of_pitchBend_7();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_5, (int16_t)((int32_t)8192), /*hidden argument*/NULL);
		__this->set_pitchBendRangeCoarse_8((uint8_t)2);
		__this->set_pitchBendRangeFine_9((uint8_t)0);
		__this->set_masterCoarseTune_10((int16_t)0);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_6 = __this->get_address_of_masterFineTune_11();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_6, (int16_t)((int32_t)8192), /*hidden argument*/NULL);
		__this->set_holdPedal_12((bool)0);
		__this->set_legatoPedal_13((bool)0);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_7 = __this->get_address_of_rpn_14();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_7, (int16_t)((int32_t)16383), /*hidden argument*/NULL);
		SynthParameters_UpdateCurrentPan_m08069E906C15598A7B038FB984A6D5D56B593090(__this, /*hidden argument*/NULL);
		SynthParameters_UpdateCurrentPitch_mE15F0D19B7BE2079C6347066DA363D578A4FEB08(__this, /*hidden argument*/NULL);
		SynthParameters_UpdateCurrentVolume_mF65FAC56F5D1EA02E14EA6FB0D4FBFBFE85C2C61(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentVolume()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters_UpdateCurrentVolume_mF65FAC56F5D1EA02E14EA6FB0D4FBFBFE85C2C61 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, const RuntimeMethod* method)
{
	{
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_0 = __this->get_address_of_expression_5();
		int16_t L_1 = CCValue_get_Combined_mE4C5B314DE167D020D5A07C59295911EF384E5E4((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_0, /*hidden argument*/NULL);
		__this->set_currentVolume_16(((float)((float)(((float)((float)L_1)))/(float)(16383.0f))));
		float L_2 = __this->get_currentVolume_16();
		float L_3 = __this->get_currentVolume_16();
		__this->set_currentVolume_16(((float)il2cpp_codegen_multiply((float)L_2, (float)L_3)));
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentPitch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters_UpdateCurrentPitch_mE15F0D19B7BE2079C6347066DA363D578A4FEB08 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, const RuntimeMethod* method)
{
	{
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_0 = __this->get_address_of_pitchBend_7();
		int16_t L_1 = CCValue_get_Combined_mE4C5B314DE167D020D5A07C59295911EF384E5E4((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_0, /*hidden argument*/NULL);
		uint8_t L_2 = __this->get_pitchBendRangeCoarse_8();
		uint8_t L_3 = __this->get_pitchBendRangeFine_9();
		__this->set_currentPitch_17((((int32_t)((int32_t)((double)il2cpp_codegen_multiply((double)((double)((double)((double)il2cpp_codegen_subtract((double)(((double)((double)L_1))), (double)(8192.0)))/(double)(8192.0))), (double)(((double)((double)((int32_t)il2cpp_codegen_add((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)100), (int32_t)L_2)), (int32_t)L_3)))))))))));
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentMod()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters_UpdateCurrentMod_m8B938492578703025D6A3B5F01193E0D80B144E2 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, const RuntimeMethod* method)
{
	{
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_0 = __this->get_address_of_modRange_6();
		int16_t L_1 = CCValue_get_Combined_mE4C5B314DE167D020D5A07C59295911EF384E5E4((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_0, /*hidden argument*/NULL);
		__this->set_currentMod_18((((int32_t)((int32_t)((double)il2cpp_codegen_multiply((double)(100.0), (double)((double)((double)(((double)((double)L_1)))/(double)(16383.0)))))))));
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentPan()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SynthParameters_UpdateCurrentPan_m08069E906C15598A7B038FB984A6D5D56B593090 (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SynthParameters_UpdateCurrentPan_m08069E906C15598A7B038FB984A6D5D56B593090_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	double V_0 = 0.0;
	{
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_0 = __this->get_address_of_pan_3();
		int16_t L_1 = CCValue_get_Combined_mE4C5B314DE167D020D5A07C59295911EF384E5E4((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_0, /*hidden argument*/NULL);
		V_0 = ((double)il2cpp_codegen_multiply((double)(1.5707963267948966), (double)((double)((double)(((double)((double)L_1)))/(double)(16383.0)))));
		PanComponent_tEF5B0C830A87834170A3E9781D0776F402CA5C3D * L_2 = __this->get_address_of_currentPan_19();
		double L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_4 = cos(L_3);
		L_2->set_Left_0((((float)((float)L_4))));
		PanComponent_tEF5B0C830A87834170A3E9781D0776F402CA5C3D * L_5 = __this->get_address_of_currentPan_19();
		double L_6 = V_0;
		double L_7 = sin(L_6);
		L_5->set_Right_1((((float)((float)L_7))));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::get_MicroBufferSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Synthesizer_get_MicroBufferSize_mBD18E712321364ECB8646B7AB30FF00A03D65FF4 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_microBufferSize_25();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::get_WorkingBufferSize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Synthesizer_get_WorkingBufferSize_mC0FF10DBED7D3B78209E74A42AB6303D62A151DE (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_0 = __this->get_sampleBuffer_17();
		NullCheck(L_0);
		V_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length))));
		goto IL_000c;
	}

IL_000c:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::get_MixGain()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Synthesizer_get_MixGain_m1834D1A1C4F51536F338A4C7AB998A4A359B14A3 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_synthGain_24();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		float L_1 = V_0;
		return L_1;
	}
}
// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::get_SampleRate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Synthesizer_get_SampleRate_m600A7A592D514CD27764AF39B3586E022E6E2147 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_sampleRate_22();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::get_AudioChannels()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Synthesizer_get_AudioChannels_mFC9566B67AEF5AA6CB4F7742F8976A9E43AA42D2 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = __this->get_audioChannels_19();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer__ctor_m5B2E9379382A890BBEA8FE575F2B4F01A8B83408 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, int32_t ___sampleRate0, int32_t ___audioChannels1, int32_t ___bufferSize2, int32_t ___bufferCount3, int32_t ___polyphony4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Synthesizer__ctor_m5B2E9379382A890BBEA8FE575F2B4F01A8B83408_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* V_2 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B8_0 = 0;
	{
		__this->set_mainVolume_23((1.0f));
		__this->set_synthGain_24((0.349999994f));
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___sampleRate0;
		if ((((int32_t)L_0) < ((int32_t)((int32_t)8000))))
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_1 = ___sampleRate0;
		G_B3_0 = ((((int32_t)((((int32_t)L_1) > ((int32_t)((int32_t)96000)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0034;
	}

IL_0033:
	{
		G_B3_0 = 0;
	}

IL_0034:
	{
		V_1 = (bool)G_B3_0;
		bool L_2 = V_1;
		if (L_2)
		{
			goto IL_007b;
		}
	}
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_3 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		V_2 = L_3;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_4 = V_2;
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, _stringLiteralEF762113927B6A6A393880C8C84F932028DF315E);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteralEF762113927B6A6A393880C8C84F932028DF315E);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_5 = V_2;
		int32_t L_6 = ((int32_t)8000);
		RuntimeObject * L_7 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_7);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_8 = V_2;
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, _stringLiteral8239DDE7DBC91495DACC42B52FAF15E9C617D4D7);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral8239DDE7DBC91495DACC42B52FAF15E9C617D4D7);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_9 = V_2;
		int32_t L_10 = ((int32_t)96000);
		RuntimeObject * L_11 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_11);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_12 = V_2;
		String_t* L_13 = String_Concat_m6F0ED62933448F8B944E52872E1EE86F6705D306(L_12, /*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_14 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m71044C2110E357B71A1C30D2561C3F861AF1DC0D(L_14, L_13, _stringLiteral1AB514A8EA2B388C6E60F1CC62C6D7C61F45CA3D, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14, Synthesizer__ctor_m5B2E9379382A890BBEA8FE575F2B4F01A8B83408_RuntimeMethod_var);
	}

IL_007b:
	{
		int32_t L_15 = ___sampleRate0;
		__this->set_sampleRate_22(L_15);
		int32_t L_16 = ___audioChannels1;
		if ((((int32_t)L_16) < ((int32_t)1)))
		{
			goto IL_008f;
		}
	}
	{
		int32_t L_17 = ___audioChannels1;
		G_B8_0 = ((((int32_t)((((int32_t)L_17) > ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0090;
	}

IL_008f:
	{
		G_B8_0 = 0;
	}

IL_0090:
	{
		V_1 = (bool)G_B8_0;
		bool L_18 = V_1;
		if (L_18)
		{
			goto IL_00cf;
		}
	}
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_19 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		V_2 = L_19;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_20 = V_2;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, _stringLiteralAEBE033E3176253CF068FAA15A2107F8903D8FDE);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)_stringLiteralAEBE033E3176253CF068FAA15A2107F8903D8FDE);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_21 = V_2;
		int32_t L_22 = 1;
		RuntimeObject * L_23 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_21);
		ArrayElementTypeCheck (L_21, L_23);
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_23);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_24 = V_2;
		NullCheck(L_24);
		ArrayElementTypeCheck (L_24, _stringLiteral8239DDE7DBC91495DACC42B52FAF15E9C617D4D7);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)_stringLiteral8239DDE7DBC91495DACC42B52FAF15E9C617D4D7);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_25 = V_2;
		int32_t L_26 = 2;
		RuntimeObject * L_27 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_25);
		ArrayElementTypeCheck (L_25, L_27);
		(L_25)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_27);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_28 = V_2;
		String_t* L_29 = String_Concat_m6F0ED62933448F8B944E52872E1EE86F6705D306(L_28, /*hidden argument*/NULL);
		ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 * L_30 = (ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00 *)il2cpp_codegen_object_new(ArgumentException_t505FA8C11E883F2D96C797AD9D396490794DEE00_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m71044C2110E357B71A1C30D2561C3F861AF1DC0D(L_30, L_29, _stringLiteral04B1938F0F2796858CDB689F91CE3644BC836103, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_30, Synthesizer__ctor_m5B2E9379382A890BBEA8FE575F2B4F01A8B83408_RuntimeMethod_var);
	}

IL_00cf:
	{
		int32_t L_31 = ___audioChannels1;
		__this->set_audioChannels_19(L_31);
		int32_t L_32 = ___bufferSize2;
		int32_t L_33 = ___sampleRate0;
		int32_t L_34 = ___sampleRate0;
		int32_t L_35 = SynthHelper_Clamp_m8900A1DC809E91761D20C656937E5EE6B04454F1(L_32, (((int32_t)((int32_t)((double)il2cpp_codegen_multiply((double)(0.001), (double)(((double)((double)L_33)))))))), (((int32_t)((int32_t)((double)il2cpp_codegen_multiply((double)(0.050000000000000003), (double)(((double)((double)L_34)))))))), /*hidden argument*/NULL);
		__this->set_microBufferSize_25(L_35);
		int32_t L_36 = __this->get_microBufferSize_25();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_37 = ceil(((double)((double)(((double)((double)L_36)))/(double)(64.0))));
		__this->set_microBufferSize_25(((int32_t)il2cpp_codegen_multiply((int32_t)(((int32_t)((int32_t)L_37))), (int32_t)((int32_t)64))));
		int32_t L_38 = ___bufferCount3;
		int32_t L_39 = Math_Max_mD8AA27386BF012C65303FCDEA041B0CC65056E7B(1, L_38, /*hidden argument*/NULL);
		__this->set_microBufferCount_26(L_39);
		int32_t L_40 = __this->get_microBufferSize_25();
		int32_t L_41 = __this->get_microBufferCount_26();
		int32_t L_42 = ___audioChannels1;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_43 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_40, (int32_t)L_41)), (int32_t)L_42)));
		__this->set_sampleBuffer_17(L_43);
		__this->set_littleEndian_20((bool)1);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_44 = (SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE*)(SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE*)SZArrayNew(SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE_il2cpp_TypeInfo_var, (uint32_t)((int32_t)16));
		__this->set_synthChannels_27(L_44);
		V_0 = 0;
		goto IL_016e;
	}

IL_015c:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_45 = __this->get_synthChannels_27();
		int32_t L_46 = V_0;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_47 = (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 *)il2cpp_codegen_object_new(SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524_il2cpp_TypeInfo_var);
		SynthParameters__ctor_mAAD2A21AC9314C750B70EA524E730F4235373711(L_47, __this, /*hidden argument*/NULL);
		NullCheck(L_45);
		ArrayElementTypeCheck (L_45, L_47);
		(L_45)->SetAt(static_cast<il2cpp_array_size_t>(L_46), (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 *)L_47);
		int32_t L_48 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_48, (int32_t)1));
	}

IL_016e:
	{
		int32_t L_49 = V_0;
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_50 = __this->get_synthChannels_27();
		NullCheck(L_50);
		V_1 = (bool)((((int32_t)L_49) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_50)->max_length))))))? 1 : 0);
		bool L_51 = V_1;
		if (L_51)
		{
			goto IL_015c;
		}
	}
	{
		int32_t L_52 = ___polyphony4;
		int32_t L_53 = SynthHelper_Clamp_m8900A1DC809E91761D20C656937E5EE6B04454F1(L_52, 5, ((int32_t)250), /*hidden argument*/NULL);
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_54 = (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 *)il2cpp_codegen_object_new(VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9_il2cpp_TypeInfo_var);
		VoiceManager__ctor_mF7DAE448488693FE2E641B7351FE9BC4A49850DF(L_54, L_53, /*hidden argument*/NULL);
		__this->set_voiceManager_18(L_54);
		Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * L_55 = (Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 *)il2cpp_codegen_object_new(Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965_il2cpp_TypeInfo_var);
		Queue_1__ctor_m496CB994FA0D02F62AA06313AA8C460AD550F03E(L_55, /*hidden argument*/Queue_1__ctor_m496CB994FA0D02F62AA06313AA8C460AD550F03E_RuntimeMethod_var);
		__this->set_midiEventQueue_28(L_55);
		int32_t L_56 = __this->get_microBufferCount_26();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_57 = (Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32*)SZArrayNew(Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32_il2cpp_TypeInfo_var, (uint32_t)L_56);
		__this->set_midiEventCounts_29(L_57);
		PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* L_58 = (PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75*)(PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75*)SZArrayNew(PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75_il2cpp_TypeInfo_var, (uint32_t)((int32_t)15));
		__this->set_layerList_30(L_58);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::LoadBank(DaggerfallWorkshop.AudioSynthesis.IResource)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_LoadBank_m5A866814011F2BC24C0AAD5F3E6BF71FD7D40580 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, RuntimeObject* ___bankFile0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Synthesizer_LoadBank_m5A866814011F2BC24C0AAD5F3E6BF71FD7D40580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject* L_0 = ___bankFile0;
		PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * L_1 = (PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 *)il2cpp_codegen_object_new(PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70_il2cpp_TypeInfo_var);
		PatchBank__ctor_mE5CC8AFFF8E92DD3C8037AF1BC76B76C8FDF4E0B(L_1, L_0, /*hidden argument*/NULL);
		Synthesizer_LoadBank_mB0296667D3FDB5AB407923A080E4A6C0B9A5D783(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::LoadBank(DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_LoadBank_mB0296667D3FDB5AB407923A080E4A6C0B9A5D783 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * ___bank0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Synthesizer_LoadBank_mB0296667D3FDB5AB407923A080E4A6C0B9A5D783_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * L_0 = ___bank0;
		V_0 = (bool)((((int32_t)((((RuntimeObject*)(PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_0017;
		}
	}
	{
		ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB * L_2 = (ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB *)il2cpp_codegen_object_new(ArgumentNullException_tFB5C4621957BC53A7D1B4FDD5C38B4D6E15DB8FB_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m81AB157B93BFE2FBFDB08B88F84B444293042F97(L_2, _stringLiteralFB75270F0EDF5598A9A6D6104F21E5620D1ABBEF, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2, Synthesizer_LoadBank_mB0296667D3FDB5AB407923A080E4A6C0B9A5D783_RuntimeMethod_var);
	}

IL_0017:
	{
		Synthesizer_UnloadBank_m622C2A0C97C8AA07176FBFDC52E41D9CABE3FDA0(__this, /*hidden argument*/NULL);
		PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * L_3 = ___bank0;
		__this->set_bank_21(L_3);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::UnloadBank()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_UnloadBank_m622C2A0C97C8AA07176FBFDC52E41D9CABE3FDA0 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * L_0 = __this->get_bank_21();
		V_0 = (bool)((((RuntimeObject*)(PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 *)L_0) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_1 = V_0;
		if (L_1)
		{
			goto IL_002b;
		}
	}
	{
		Synthesizer_NoteOffAll_m09B0E9EA1805A76EDADE4E4E0443AFD9AB7FAA88(__this, (bool)1, /*hidden argument*/NULL);
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_2 = __this->get_voiceManager_18();
		NullCheck(L_2);
		VoiceManager_UnloadPatches_mE8C788BCE71CD06511BDD72292BA1339E7CB27D2(L_2, /*hidden argument*/NULL);
		__this->set_bank_21((PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 *)NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ResetSynthControls()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_ResetSynthControls_mD469453AF5D6CF0F67E9D50FDF7DB78DCBCF7D16 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		goto IL_0019;
	}

IL_0005:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_0 = __this->get_synthChannels_27();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		SynthParameters_ResetControllers_m0027611CEA3F87605275C99E8640F89D27B896E8(L_3, /*hidden argument*/NULL);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0019:
	{
		int32_t L_5 = V_0;
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_6 = __this->get_synthChannels_27();
		NullCheck(L_6);
		V_1 = (bool)((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length))))))? 1 : 0);
		bool L_7 = V_1;
		if (L_7)
		{
			goto IL_0005;
		}
	}
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_8 = __this->get_synthChannels_27();
		NullCheck(L_8);
		int32_t L_9 = ((int32_t)9);
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_10 = (L_8)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		L_10->set_bankSelect_1((uint8_t)((int32_t)128));
		Synthesizer_ReleaseAllHoldPedals_mAA8BF558958F700C063299A6DDC2D27B38F16239(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ResetPrograms()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_ResetPrograms_m5EF661697DAC332F0B06B51195B76F63DDB5AD5F (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		V_0 = 0;
		goto IL_0019;
	}

IL_0005:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_0 = __this->get_synthChannels_27();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		L_3->set_program_0((uint8_t)0);
		int32_t L_4 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_0019:
	{
		int32_t L_5 = V_0;
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_6 = __this->get_synthChannels_27();
		NullCheck(L_6);
		V_1 = (bool)((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length))))))? 1 : 0);
		bool L_7 = V_1;
		if (L_7)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::GetNext(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_GetNext_mE08CF4F0DEA6A65D7D91E98A773E031DA317B15D (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___buffer0, const RuntimeMethod* method)
{
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_0 = __this->get_sampleBuffer_17();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_1 = __this->get_sampleBuffer_17();
		NullCheck(L_1);
		Array_Clear_mEB42D172C5E0825D340F6209F28578BDDDDCE34F((RuntimeArray *)(RuntimeArray *)L_0, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))), /*hidden argument*/NULL);
		Synthesizer_FillWorkingBuffer_m53938F9A39846C0E6EC7A76C7A1DF39ABC640974(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_2 = ___buffer0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_3 = __this->get_sampleBuffer_17();
		Synthesizer_ConvertWorkingBuffer_mBC00D122A20A8577DC0AEAACA2B701D114E498FF(__this, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::FillWorkingBuffer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_FillWorkingBuffer_m53938F9A39846C0E6EC7A76C7A1DF39ABC640974 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Synthesizer_FillWorkingBuffer_m53938F9A39846C0E6EC7A76C7A1DF39ABC640974_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	MidiMessage_t3810404F35143BF297802CC153A00F2D69755B24  V_3;
	memset((&V_3), 0, sizeof(V_3));
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * V_4 = NULL;
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * V_5 = NULL;
	bool V_6 = false;
	{
		V_0 = 0;
		V_1 = 0;
		goto IL_013c;
	}

IL_000a:
	{
		Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * L_0 = __this->get_midiEventQueue_28();
		NullCheck(L_0);
		int32_t L_1 = Queue_1_get_Count_m6331507132D178B8517E38C3F2F0C1698AEC49D1_inline(L_0, /*hidden argument*/Queue_1_get_Count_m6331507132D178B8517E38C3F2F0C1698AEC49D1_RuntimeMethod_var);
		V_6 = (bool)((((int32_t)((((int32_t)L_1) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_6;
		if (L_2)
		{
			goto IL_006e;
		}
	}
	{
		V_2 = 0;
		goto IL_005c;
	}

IL_0027:
	{
		Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * L_3 = __this->get_midiEventQueue_28();
		NullCheck(L_3);
		MidiMessage_t3810404F35143BF297802CC153A00F2D69755B24  L_4 = Queue_1_Dequeue_m4313D1B2E96A63B32AAEB715CBD1940EEC4F692F(L_3, /*hidden argument*/Queue_1_Dequeue_m4313D1B2E96A63B32AAEB715CBD1940EEC4F692F_RuntimeMethod_var);
		V_3 = L_4;
		uint8_t L_5 = (&V_3)->get_channel_1();
		uint8_t L_6 = (&V_3)->get_command_2();
		uint8_t L_7 = (&V_3)->get_data1_3();
		uint8_t L_8 = (&V_3)->get_data2_4();
		Synthesizer_ProcessMidiMessage_m068A780E4843F4C24833F1C6BDCAFD33D22728BC(__this, L_5, L_6, L_7, L_8, /*hidden argument*/NULL);
		int32_t L_9 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_005c:
	{
		int32_t L_10 = V_2;
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_11 = __this->get_midiEventCounts_29();
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		int32_t L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		V_6 = (bool)((((int32_t)L_10) < ((int32_t)L_14))? 1 : 0);
		bool L_15 = V_6;
		if (L_15)
		{
			goto IL_0027;
		}
	}
	{
	}

IL_006e:
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_16 = __this->get_voiceManager_18();
		NullCheck(L_16);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_17 = L_16->get_activeVoices_3();
		NullCheck(L_17);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_18 = LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline(L_17, /*hidden argument*/LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var);
		V_4 = L_18;
		goto IL_0116;
	}

IL_0085:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_19 = V_4;
		NullCheck(L_19);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_20 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_19, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		int32_t L_21 = V_0;
		int32_t L_22 = V_0;
		int32_t L_23 = __this->get_microBufferSize_25();
		int32_t L_24 = __this->get_audioChannels_19();
		NullCheck(L_20);
		Voice_Process_m7893B616DA247441F4231BC8EEE204B01C0D348C(L_20, L_21, ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_23, (int32_t)L_24)))), /*hidden argument*/NULL);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_25 = V_4;
		NullCheck(L_25);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_26 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_25, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_26);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_27 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		int32_t L_28 = L_27->get_state_4();
		V_6 = (bool)((((int32_t)((((int32_t)L_28) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_29 = V_6;
		if (L_29)
		{
			goto IL_010a;
		}
	}
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_30 = V_4;
		V_5 = L_30;
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_31 = V_4;
		NullCheck(L_31);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_32 = LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA(L_31, /*hidden argument*/LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA_RuntimeMethod_var);
		V_4 = L_32;
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_33 = __this->get_voiceManager_18();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_34 = V_5;
		NullCheck(L_34);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_35 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_34, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_33);
		VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079(L_33, L_35, /*hidden argument*/NULL);
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_36 = __this->get_voiceManager_18();
		NullCheck(L_36);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_37 = L_36->get_activeVoices_3();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_38 = V_5;
		NullCheck(L_37);
		LinkedList_1_Remove_mECC8FD43A15126D559FFF8E2DDC62A1B6F27FADD(L_37, L_38, /*hidden argument*/LinkedList_1_Remove_mECC8FD43A15126D559FFF8E2DDC62A1B6F27FADD_RuntimeMethod_var);
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_39 = __this->get_voiceManager_18();
		NullCheck(L_39);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_40 = L_39->get_freeVoices_2();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_41 = V_5;
		NullCheck(L_40);
		LinkedList_1_AddFirst_m8B3FCFCACB01A06666CB311754F101AA005C505F(L_40, L_41, /*hidden argument*/LinkedList_1_AddFirst_m8B3FCFCACB01A06666CB311754F101AA005C505F_RuntimeMethod_var);
		goto IL_0115;
	}

IL_010a:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_42 = V_4;
		NullCheck(L_42);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_43 = LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA(L_42, /*hidden argument*/LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA_RuntimeMethod_var);
		V_4 = L_43;
	}

IL_0115:
	{
	}

IL_0116:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_44 = V_4;
		V_6 = (bool)((((int32_t)((((RuntimeObject*)(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *)L_44) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_45 = V_6;
		if (L_45)
		{
			goto IL_0085;
		}
	}
	{
		int32_t L_46 = V_0;
		int32_t L_47 = __this->get_microBufferSize_25();
		int32_t L_48 = __this->get_audioChannels_19();
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_47, (int32_t)L_48))));
		int32_t L_49 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_49, (int32_t)1));
	}

IL_013c:
	{
		int32_t L_50 = V_1;
		int32_t L_51 = __this->get_microBufferCount_26();
		V_6 = (bool)((((int32_t)L_50) < ((int32_t)L_51))? 1 : 0);
		bool L_52 = V_6;
		if (L_52)
		{
			goto IL_000a;
		}
	}
	{
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_53 = __this->get_midiEventCounts_29();
		Int32U5BU5D_t70F1BDC14B1786481B176D6139A5E3B87DC54C32* L_54 = __this->get_midiEventCounts_29();
		NullCheck(L_54);
		Array_Clear_mEB42D172C5E0825D340F6209F28578BDDDDCE34F((RuntimeArray *)(RuntimeArray *)L_53, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_54)->max_length)))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ConvertWorkingBuffer(System.Single[],System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_ConvertWorkingBuffer_mBC00D122A20A8577DC0AEAACA2B701D114E498FF (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___to0, SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___from1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	bool V_2 = false;
	{
		V_0 = 0;
		goto IL_0029;
	}

IL_0005:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_0 = ___from1;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		float L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		float L_4 = __this->get_mainVolume_23();
		float L_5 = SynthHelper_Clamp_mABDAD8ED1DC346DC83659F2142445186C92D3120(((float)il2cpp_codegen_multiply((float)L_3, (float)L_4)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		V_1 = L_5;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_6 = ___to0;
		int32_t L_7 = V_0;
		float L_8 = V_1;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (float)L_8);
		int32_t L_9 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_9, (int32_t)1));
	}

IL_0029:
	{
		int32_t L_10 = V_0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_11 = ___from1;
		NullCheck(L_11);
		V_2 = (bool)((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_11)->max_length))))))? 1 : 0);
		bool L_12 = V_2;
		if (L_12)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::NoteOn(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_NoteOn_m04E2EFEF32FD9B4E13480059E411E37F26FD5772 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, int32_t ___channel0, int32_t ___note1, int32_t ___velocity2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Synthesizer_NoteOn_m04E2EFEF32FD9B4E13480059E411E37F26FD5772_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * V_0 = NULL;
	Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * V_1 = NULL;
	int32_t V_2 = 0;
	VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * V_3 = NULL;
	int32_t V_4 = 0;
	bool V_5 = false;
	int32_t V_6 = 0;
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * V_7 = NULL;
	Voice_tA46F178E48B0D771712A0AA48175620981094D47 * V_8 = NULL;
	bool V_9 = false;
	int32_t G_B19_0 = 0;
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_0 = __this->get_synthChannels_27();
		int32_t L_1 = ___channel0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		V_0 = L_3;
		PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * L_4 = __this->get_bank_21();
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_5 = V_0;
		NullCheck(L_5);
		uint8_t L_6 = L_5->get_bankSelect_1();
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_7 = V_0;
		NullCheck(L_7);
		uint8_t L_8 = L_7->get_program_0();
		NullCheck(L_4);
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_9 = PatchBank_GetPatch_mC86BDB6F5B4657124EE0CD4474172176A4C38FDF(L_4, L_6, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_10 = V_1;
		V_9 = (bool)((((int32_t)((((RuntimeObject*)(Patch_t3144A9674F069F684A9B015803D7A714F366C7AD *)L_10) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_11 = V_9;
		if (L_11)
		{
			goto IL_0034;
		}
	}
	{
		goto IL_026e;
	}

IL_0034:
	{
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_12 = V_1;
		V_9 = (bool)((((int32_t)((!(((RuntimeObject*)(MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F *)((MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F *)IsInstClass((RuntimeObject*)L_12, MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F_il2cpp_TypeInfo_var))) <= ((RuntimeObject*)(RuntimeObject *)NULL)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_13 = V_9;
		if (L_13)
		{
			goto IL_005f;
		}
	}
	{
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_14 = V_1;
		int32_t L_15 = ___channel0;
		int32_t L_16 = ___note1;
		int32_t L_17 = ___velocity2;
		PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* L_18 = __this->get_layerList_30();
		NullCheck(((MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F *)CastclassClass((RuntimeObject*)L_14, MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F_il2cpp_TypeInfo_var)));
		int32_t L_19 = MultiPatch_FindPatches_m86E92FB9B2C65E94311DD2389C67152F477741EE(((MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F *)CastclassClass((RuntimeObject*)L_14, MultiPatch_t7D66DFFD94F4CEA350C7E41C6C43BCC9BD055B9F_il2cpp_TypeInfo_var)), L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		V_2 = L_19;
		goto IL_006c;
	}

IL_005f:
	{
		V_2 = 1;
		PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* L_20 = __this->get_layerList_30();
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_21 = V_1;
		NullCheck(L_20);
		ArrayElementTypeCheck (L_20, L_21);
		(L_20)->SetAt(static_cast<il2cpp_array_size_t>(0), (Patch_t3144A9674F069F684A9B015803D7A714F366C7AD *)L_21);
	}

IL_006c:
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_22 = __this->get_voiceManager_18();
		NullCheck(L_22);
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_23 = L_22->get_registry_4();
		int32_t L_24 = ___channel0;
		int32_t L_25 = ___note1;
		NullCheck(L_23);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_26 = (L_23)->GetAt(L_24, L_25);
		V_9 = (bool)((((RuntimeObject*)(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)L_26) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_27 = V_9;
		if (L_27)
		{
			goto IL_00ce;
		}
	}
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_28 = __this->get_voiceManager_18();
		NullCheck(L_28);
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_29 = L_28->get_registry_4();
		int32_t L_30 = ___channel0;
		int32_t L_31 = ___note1;
		NullCheck(L_29);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_32 = (L_29)->GetAt(L_30, L_31);
		V_3 = L_32;
		goto IL_00b2;
	}

IL_009d:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_33 = V_3;
		NullCheck(L_33);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_34 = L_33->get_Value_0();
		NullCheck(L_34);
		Voice_Stop_m8E10957D7FABBF4A40F8370F5D8D77EE39A64567(L_34, /*hidden argument*/NULL);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_35 = V_3;
		NullCheck(L_35);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_36 = L_35->get_Next_1();
		V_3 = L_36;
	}

IL_00b2:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_37 = V_3;
		V_9 = (bool)((((int32_t)((((RuntimeObject*)(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)L_37) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_38 = V_9;
		if (L_38)
		{
			goto IL_009d;
		}
	}
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_39 = __this->get_voiceManager_18();
		int32_t L_40 = ___channel0;
		int32_t L_41 = ___note1;
		NullCheck(L_39);
		VoiceManager_RemoveFromRegistry_mA5518367D8A75DE5B022440CE4B55CB75E502DE3(L_39, L_40, L_41, /*hidden argument*/NULL);
	}

IL_00ce:
	{
		V_4 = 0;
		goto IL_01c6;
	}

IL_00d6:
	{
		V_5 = (bool)1;
		int32_t L_42 = V_4;
		V_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_42, (int32_t)1));
		goto IL_0117;
	}

IL_00e2:
	{
		PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* L_43 = __this->get_layerList_30();
		int32_t L_44 = V_4;
		NullCheck(L_43);
		int32_t L_45 = L_44;
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_46 = (L_43)->GetAt(static_cast<il2cpp_array_size_t>(L_45));
		NullCheck(L_46);
		int32_t L_47 = Patch_get_ExclusiveGroupTarget_mC9F49AFFCEF49C1A5A40D5D87D7A9ABF89FC1AE9(L_46, /*hidden argument*/NULL);
		PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* L_48 = __this->get_layerList_30();
		int32_t L_49 = V_6;
		NullCheck(L_48);
		int32_t L_50 = L_49;
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_51 = (L_48)->GetAt(static_cast<il2cpp_array_size_t>(L_50));
		NullCheck(L_51);
		int32_t L_52 = Patch_get_ExclusiveGroupTarget_mC9F49AFFCEF49C1A5A40D5D87D7A9ABF89FC1AE9(L_51, /*hidden argument*/NULL);
		V_9 = (bool)((((int32_t)((((int32_t)L_47) == ((int32_t)L_52))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_53 = V_9;
		if (L_53)
		{
			goto IL_0110;
		}
	}
	{
		V_5 = (bool)0;
		goto IL_0125;
	}

IL_0110:
	{
		int32_t L_54 = V_6;
		V_6 = ((int32_t)il2cpp_codegen_subtract((int32_t)L_54, (int32_t)1));
	}

IL_0117:
	{
		int32_t L_55 = V_6;
		V_9 = (bool)((((int32_t)((((int32_t)L_55) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_56 = V_9;
		if (L_56)
		{
			goto IL_00e2;
		}
	}

IL_0125:
	{
		PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* L_57 = __this->get_layerList_30();
		int32_t L_58 = V_4;
		NullCheck(L_57);
		int32_t L_59 = L_58;
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_60 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		NullCheck(L_60);
		int32_t L_61 = Patch_get_ExclusiveGroupTarget_mC9F49AFFCEF49C1A5A40D5D87D7A9ABF89FC1AE9(L_60, /*hidden argument*/NULL);
		if (!L_61)
		{
			goto IL_013c;
		}
	}
	{
		bool L_62 = V_5;
		G_B19_0 = ((((int32_t)L_62) == ((int32_t)0))? 1 : 0);
		goto IL_013d;
	}

IL_013c:
	{
		G_B19_0 = 1;
	}

IL_013d:
	{
		V_9 = (bool)G_B19_0;
		bool L_63 = V_9;
		if (L_63)
		{
			goto IL_01bf;
		}
	}
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_64 = __this->get_voiceManager_18();
		NullCheck(L_64);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_65 = L_64->get_activeVoices_3();
		NullCheck(L_65);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_66 = LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline(L_65, /*hidden argument*/LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var);
		V_7 = L_66;
		goto IL_01b0;
	}

IL_0159:
	{
		PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* L_67 = __this->get_layerList_30();
		int32_t L_68 = V_4;
		NullCheck(L_67);
		int32_t L_69 = L_68;
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_70 = (L_67)->GetAt(static_cast<il2cpp_array_size_t>(L_69));
		NullCheck(L_70);
		int32_t L_71 = Patch_get_ExclusiveGroupTarget_mC9F49AFFCEF49C1A5A40D5D87D7A9ABF89FC1AE9(L_70, /*hidden argument*/NULL);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_72 = V_7;
		NullCheck(L_72);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_73 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_72, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_73);
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_74 = Voice_get_Patch_m4758BB409337AC45CCFB570CF5283E155D725574(L_73, /*hidden argument*/NULL);
		NullCheck(L_74);
		int32_t L_75 = Patch_get_ExclusiveGroup_m2E5B9F814EB6C23F1464473925B9DA97EC6529F6(L_74, /*hidden argument*/NULL);
		V_9 = (bool)((((int32_t)((((int32_t)L_71) == ((int32_t)L_75))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_76 = V_9;
		if (L_76)
		{
			goto IL_01a6;
		}
	}
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_77 = V_7;
		NullCheck(L_77);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_78 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_77, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_78);
		Voice_Stop_m8E10957D7FABBF4A40F8370F5D8D77EE39A64567(L_78, /*hidden argument*/NULL);
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_79 = __this->get_voiceManager_18();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_80 = V_7;
		NullCheck(L_80);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_81 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_80, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_79);
		VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079(L_79, L_81, /*hidden argument*/NULL);
	}

IL_01a6:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_82 = V_7;
		NullCheck(L_82);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_83 = LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA(L_82, /*hidden argument*/LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA_RuntimeMethod_var);
		V_7 = L_83;
	}

IL_01b0:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_84 = V_7;
		V_9 = (bool)((((int32_t)((((RuntimeObject*)(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *)L_84) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_85 = V_9;
		if (L_85)
		{
			goto IL_0159;
		}
	}
	{
	}

IL_01bf:
	{
		int32_t L_86 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_86, (int32_t)1));
	}

IL_01c6:
	{
		int32_t L_87 = V_4;
		int32_t L_88 = V_2;
		V_9 = (bool)((((int32_t)L_87) < ((int32_t)L_88))? 1 : 0);
		bool L_89 = V_9;
		if (L_89)
		{
			goto IL_00d6;
		}
	}
	{
		V_4 = 0;
		goto IL_0243;
	}

IL_01d9:
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_90 = __this->get_voiceManager_18();
		NullCheck(L_90);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_91 = VoiceManager_GetFreeVoice_mAA1AE9A540D670DCE54385FBE800AB099AEB3BF5(L_90, /*hidden argument*/NULL);
		V_8 = L_91;
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_92 = V_8;
		V_9 = (bool)((((int32_t)((((RuntimeObject*)(Voice_tA46F178E48B0D771712A0AA48175620981094D47 *)L_92) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_93 = V_9;
		if (L_93)
		{
			goto IL_01f7;
		}
	}
	{
		goto IL_024e;
	}

IL_01f7:
	{
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_94 = V_8;
		int32_t L_95 = ___channel0;
		int32_t L_96 = ___note1;
		int32_t L_97 = ___velocity2;
		PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* L_98 = __this->get_layerList_30();
		int32_t L_99 = V_4;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_102 = __this->get_synthChannels_27();
		int32_t L_103 = ___channel0;
		NullCheck(L_102);
		int32_t L_104 = L_103;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_105 = (L_102)->GetAt(static_cast<il2cpp_array_size_t>(L_104));
		NullCheck(L_94);
		Voice_Configure_mBA82ADC3E8D74554E84FEF95363DFB9343FEE6F8(L_94, L_95, L_96, L_97, L_101, L_105, /*hidden argument*/NULL);
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_106 = __this->get_voiceManager_18();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_107 = V_8;
		NullCheck(L_106);
		VoiceManager_AddToRegistry_m2055B93BCC9715FEE1E9D8D2C5E412F2EAD416F6(L_106, L_107, /*hidden argument*/NULL);
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_108 = __this->get_voiceManager_18();
		NullCheck(L_108);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_109 = L_108->get_activeVoices_3();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_110 = V_8;
		NullCheck(L_109);
		LinkedList_1_AddLast_m9358ECB9018B698E73EF40B3E5A61C65339835CA(L_109, L_110, /*hidden argument*/LinkedList_1_AddLast_m9358ECB9018B698E73EF40B3E5A61C65339835CA_RuntimeMethod_var);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_111 = V_8;
		NullCheck(L_111);
		Voice_Start_mFD62C40C95388C7CA2E2677758C8F25A75BCF807(L_111, /*hidden argument*/NULL);
		int32_t L_112 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_112, (int32_t)1));
	}

IL_0243:
	{
		int32_t L_113 = V_4;
		int32_t L_114 = V_2;
		V_9 = (bool)((((int32_t)L_113) < ((int32_t)L_114))? 1 : 0);
		bool L_115 = V_9;
		if (L_115)
		{
			goto IL_01d9;
		}
	}

IL_024e:
	{
		V_4 = 0;
		goto IL_0263;
	}

IL_0253:
	{
		PatchU5BU5D_tD8D955FE72392CBA6D6307994FB63881A005AC75* L_116 = __this->get_layerList_30();
		int32_t L_117 = V_4;
		NullCheck(L_116);
		ArrayElementTypeCheck (L_116, NULL);
		(L_116)->SetAt(static_cast<il2cpp_array_size_t>(L_117), (Patch_t3144A9674F069F684A9B015803D7A714F366C7AD *)NULL);
		int32_t L_118 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_118, (int32_t)1));
	}

IL_0263:
	{
		int32_t L_119 = V_4;
		int32_t L_120 = V_2;
		V_9 = (bool)((((int32_t)L_119) < ((int32_t)L_120))? 1 : 0);
		bool L_121 = V_9;
		if (L_121)
		{
			goto IL_0253;
		}
	}

IL_026e:
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::NoteOff(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_NoteOff_mAA9E92FBA1972D7413249B4C79CA46C064BEB2E8 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, int32_t ___channel0, int32_t ___note1, const RuntimeMethod* method)
{
	VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * V_0 = NULL;
	bool V_1 = false;
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_0 = __this->get_synthChannels_27();
		int32_t L_1 = ___channel0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		bool L_4 = L_3->get_holdPedal_12();
		V_1 = (bool)((((int32_t)L_4) == ((int32_t)0))? 1 : 0);
		bool L_5 = V_1;
		if (L_5)
		{
			goto IL_0053;
		}
	}
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_6 = __this->get_voiceManager_18();
		NullCheck(L_6);
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_7 = L_6->get_registry_4();
		int32_t L_8 = ___channel0;
		int32_t L_9 = ___note1;
		NullCheck(L_7);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_10 = (L_7)->GetAt(L_8, L_9);
		V_0 = L_10;
		goto IL_0045;
	}

IL_002b:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_11 = V_0;
		NullCheck(L_11);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_12 = L_11->get_Value_0();
		NullCheck(L_12);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_13 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		L_13->set_noteOffPending_3((bool)1);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_14 = V_0;
		NullCheck(L_14);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_15 = L_14->get_Next_1();
		V_0 = L_15;
	}

IL_0045:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_16 = V_0;
		V_1 = (bool)((((int32_t)((((RuntimeObject*)(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)L_16) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_17 = V_1;
		if (L_17)
		{
			goto IL_002b;
		}
	}
	{
		goto IL_0098;
	}

IL_0053:
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_18 = __this->get_voiceManager_18();
		NullCheck(L_18);
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_19 = L_18->get_registry_4();
		int32_t L_20 = ___channel0;
		int32_t L_21 = ___note1;
		NullCheck(L_19);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_22 = (L_19)->GetAt(L_20, L_21);
		V_0 = L_22;
		goto IL_007e;
	}

IL_0069:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_23 = V_0;
		NullCheck(L_23);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_24 = L_23->get_Value_0();
		NullCheck(L_24);
		Voice_Stop_m8E10957D7FABBF4A40F8370F5D8D77EE39A64567(L_24, /*hidden argument*/NULL);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_25 = V_0;
		NullCheck(L_25);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_26 = L_25->get_Next_1();
		V_0 = L_26;
	}

IL_007e:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_27 = V_0;
		V_1 = (bool)((((int32_t)((((RuntimeObject*)(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)L_27) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_28 = V_1;
		if (L_28)
		{
			goto IL_0069;
		}
	}
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_29 = __this->get_voiceManager_18();
		int32_t L_30 = ___channel0;
		int32_t L_31 = ___note1;
		NullCheck(L_29);
		VoiceManager_RemoveFromRegistry_mA5518367D8A75DE5B022440CE4B55CB75E502DE3(L_29, L_30, L_31, /*hidden argument*/NULL);
	}

IL_0098:
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::NoteOffAll(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_NoteOffAll_m09B0E9EA1805A76EDADE4E4E0443AFD9AB7FAA88 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, bool ___immediate0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Synthesizer_NoteOffAll_m09B0E9EA1805A76EDADE4E4E0443AFD9AB7FAA88_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * V_0 = NULL;
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * V_1 = NULL;
	VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * V_2 = NULL;
	bool V_3 = false;
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_0 = __this->get_voiceManager_18();
		NullCheck(L_0);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_1 = L_0->get_activeVoices_3();
		NullCheck(L_1);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_2 = LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline(L_1, /*hidden argument*/LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var);
		V_0 = L_2;
		bool L_3 = ___immediate0;
		V_3 = (bool)((((int32_t)L_3) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_3;
		if (L_4)
		{
			goto IL_0072;
		}
	}
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_5 = __this->get_voiceManager_18();
		NullCheck(L_5);
		VoiceManager_ClearRegistry_mE7A9A5542422604EFAE47D0D4AF3B4D0A2F7B096(L_5, /*hidden argument*/NULL);
		goto IL_0064;
	}

IL_0029:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_6 = V_0;
		NullCheck(L_6);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_7 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_6, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_7);
		Voice_StopImmediately_m3675EECC2EF7522DE6BF677F419648631A557D21(L_7, /*hidden argument*/NULL);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_8 = V_0;
		V_1 = L_8;
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_9 = V_0;
		NullCheck(L_9);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_10 = LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA(L_9, /*hidden argument*/LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA_RuntimeMethod_var);
		V_0 = L_10;
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_11 = __this->get_voiceManager_18();
		NullCheck(L_11);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_12 = L_11->get_activeVoices_3();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_13 = V_1;
		NullCheck(L_12);
		LinkedList_1_Remove_mECC8FD43A15126D559FFF8E2DDC62A1B6F27FADD(L_12, L_13, /*hidden argument*/LinkedList_1_Remove_mECC8FD43A15126D559FFF8E2DDC62A1B6F27FADD_RuntimeMethod_var);
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_14 = __this->get_voiceManager_18();
		NullCheck(L_14);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_15 = L_14->get_freeVoices_2();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_16 = V_1;
		NullCheck(L_15);
		LinkedList_1_AddFirst_m8B3FCFCACB01A06666CB311754F101AA005C505F(L_15, L_16, /*hidden argument*/LinkedList_1_AddFirst_m8B3FCFCACB01A06666CB311754F101AA005C505F_RuntimeMethod_var);
	}

IL_0064:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_17 = V_0;
		V_3 = (bool)((((int32_t)((((RuntimeObject*)(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *)L_17) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_18 = V_3;
		if (L_18)
		{
			goto IL_0029;
		}
	}
	{
		goto IL_00ec;
	}

IL_0072:
	{
		goto IL_00e0;
	}

IL_0075:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_19 = V_0;
		NullCheck(L_19);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_20 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_19, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_20);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_21 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_22 = V_2;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_state_4();
		V_3 = (bool)((((int32_t)((((int32_t)L_23) == ((int32_t)2))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_24 = V_3;
		if (L_24)
		{
			goto IL_00d8;
		}
	}
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_25 = __this->get_synthChannels_27();
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_26 = V_2;
		NullCheck(L_26);
		int32_t L_27 = L_26->get_channel_0();
		NullCheck(L_25);
		int32_t L_28 = L_27;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_29 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_28));
		NullCheck(L_29);
		bool L_30 = L_29->get_holdPedal_12();
		V_3 = (bool)((((int32_t)L_30) == ((int32_t)0))? 1 : 0);
		bool L_31 = V_3;
		if (L_31)
		{
			goto IL_00b7;
		}
	}
	{
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_32 = V_2;
		NullCheck(L_32);
		L_32->set_noteOffPending_3((bool)1);
		goto IL_00d7;
	}

IL_00b7:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_33 = V_0;
		NullCheck(L_33);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_34 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_33, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_34);
		Voice_Stop_m8E10957D7FABBF4A40F8370F5D8D77EE39A64567(L_34, /*hidden argument*/NULL);
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_35 = __this->get_voiceManager_18();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_36 = V_0;
		NullCheck(L_36);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_37 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_36, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_35);
		VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079(L_35, L_37, /*hidden argument*/NULL);
	}

IL_00d7:
	{
	}

IL_00d8:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_38 = V_0;
		NullCheck(L_38);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_39 = LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA(L_38, /*hidden argument*/LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA_RuntimeMethod_var);
		V_0 = L_39;
	}

IL_00e0:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_40 = V_0;
		V_3 = (bool)((((int32_t)((((RuntimeObject*)(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *)L_40) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_41 = V_3;
		if (L_41)
		{
			goto IL_0075;
		}
	}
	{
	}

IL_00ec:
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ProcessMidiMessage(System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_ProcessMidiMessage_m068A780E4843F4C24833F1C6BDCAFD33D22728BC (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, int32_t ___channel0, int32_t ___command1, int32_t ___data12, int32_t ___data23, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	bool V_1 = false;
	int16_t V_2 = 0;
	SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * G_B35_0 = NULL;
	SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * G_B34_0 = NULL;
	int32_t G_B36_0 = 0;
	SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * G_B36_1 = NULL;
	int32_t G_B49_0 = 0;
	{
		int32_t L_0 = ___command1;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) > ((int32_t)((int32_t)176))))
		{
			goto IL_0028;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)128))))
		{
			goto IL_004e;
		}
	}
	{
		int32_t L_3 = V_0;
		if ((((int32_t)L_3) == ((int32_t)((int32_t)144))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_4 = V_0;
		if ((((int32_t)L_4) == ((int32_t)((int32_t)176))))
		{
			goto IL_0083;
		}
	}
	{
		goto IL_05a7;
	}

IL_0028:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)192))))
		{
			goto IL_0549;
		}
	}
	{
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)((int32_t)208))))
		{
			goto IL_055a;
		}
	}
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) == ((int32_t)((int32_t)224))))
		{
			goto IL_056c;
		}
	}
	{
		goto IL_05a7;
	}

IL_004e:
	{
		int32_t L_8 = ___channel0;
		int32_t L_9 = ___data12;
		Synthesizer_NoteOff_mAA9E92FBA1972D7413249B4C79CA46C064BEB2E8(__this, L_8, L_9, /*hidden argument*/NULL);
		goto IL_05a9;
	}

IL_005c:
	{
		int32_t L_10 = ___data23;
		V_1 = (bool)((((int32_t)((((int32_t)L_10) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_11 = V_1;
		if (L_11)
		{
			goto IL_0073;
		}
	}
	{
		int32_t L_12 = ___channel0;
		int32_t L_13 = ___data12;
		Synthesizer_NoteOff_mAA9E92FBA1972D7413249B4C79CA46C064BEB2E8(__this, L_12, L_13, /*hidden argument*/NULL);
		goto IL_007e;
	}

IL_0073:
	{
		int32_t L_14 = ___channel0;
		int32_t L_15 = ___data12;
		int32_t L_16 = ___data23;
		Synthesizer_NoteOn_m04E2EFEF32FD9B4E13480059E411E37F26FD5772(__this, L_14, L_15, L_16, /*hidden argument*/NULL);
	}

IL_007e:
	{
		goto IL_05a9;
	}

IL_0083:
	{
		int32_t L_17 = ___data12;
		V_0 = L_17;
		int32_t L_18 = V_0;
		if ((((int32_t)L_18) > ((int32_t)((int32_t)43))))
		{
			goto IL_00f0;
		}
	}
	{
		int32_t L_19 = V_0;
		if ((((int32_t)L_19) > ((int32_t)((int32_t)11))))
		{
			goto IL_00c2;
		}
	}
	{
		int32_t L_20 = V_0;
		switch (L_20)
		{
			case 0:
			{
				goto IL_0141;
			}
			case 1:
			{
				goto IL_019d;
			}
		}
	}
	{
		int32_t L_21 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_21, (int32_t)6)))
		{
			case 0:
			{
				goto IL_03a8;
			}
			case 1:
			{
				goto IL_01ef;
			}
			case 2:
			{
				goto IL_0545;
			}
			case 3:
			{
				goto IL_0545;
			}
			case 4:
			{
				goto IL_0225;
			}
			case 5:
			{
				goto IL_0277;
			}
		}
	}
	{
		goto IL_0545;
	}

IL_00c2:
	{
		int32_t L_22 = V_0;
		if ((((int32_t)L_22) == ((int32_t)((int32_t)33))))
		{
			goto IL_01c6;
		}
	}
	{
		int32_t L_23 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_23, (int32_t)((int32_t)38))))
		{
			case 0:
			{
				goto IL_0423;
			}
			case 1:
			{
				goto IL_020a;
			}
			case 2:
			{
				goto IL_0545;
			}
			case 3:
			{
				goto IL_0545;
			}
			case 4:
			{
				goto IL_024e;
			}
			case 5:
			{
				goto IL_02a0;
			}
		}
	}
	{
		goto IL_0545;
	}

IL_00f0:
	{
		int32_t L_24 = V_0;
		if ((((int32_t)L_24) > ((int32_t)((int32_t)68))))
		{
			goto IL_010a;
		}
	}
	{
		int32_t L_25 = V_0;
		if ((((int32_t)L_25) == ((int32_t)((int32_t)64))))
		{
			goto IL_02c9;
		}
	}
	{
		int32_t L_26 = V_0;
		if ((((int32_t)L_26) == ((int32_t)((int32_t)68))))
		{
			goto IL_0306;
		}
	}
	{
		goto IL_0545;
	}

IL_010a:
	{
		int32_t L_27 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_27, (int32_t)((int32_t)98))))
		{
			case 0:
			{
				goto IL_033b;
			}
			case 1:
			{
				goto IL_031e;
			}
			case 2:
			{
				goto IL_0373;
			}
			case 3:
			{
				goto IL_0358;
			}
		}
	}
	{
		int32_t L_28 = V_0;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_28, (int32_t)((int32_t)120))))
		{
			case 0:
			{
				goto IL_038e;
			}
			case 1:
			{
				goto IL_0485;
			}
			case 2:
			{
				goto IL_0545;
			}
			case 3:
			{
				goto IL_039b;
			}
		}
	}
	{
		goto IL_0545;
	}

IL_0141:
	{
		int32_t L_29 = ___channel0;
		V_1 = (bool)((((int32_t)((((int32_t)L_29) == ((int32_t)((int32_t)9)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_30 = V_1;
		if (L_30)
		{
			goto IL_0157;
		}
	}
	{
		int32_t L_31 = ___data23;
		___data23 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)((int32_t)128)));
	}

IL_0157:
	{
		PatchBank_t9EA2867945997667019885EA45869E38FEE5EB70 * L_32 = __this->get_bank_21();
		int32_t L_33 = ___data23;
		NullCheck(L_32);
		bool L_34 = PatchBank_IsBankLoaded_m8C02279495AA8D73EDA721748226D7C4058E188E(L_32, L_33, /*hidden argument*/NULL);
		V_1 = (bool)((((int32_t)L_34) == ((int32_t)0))? 1 : 0);
		bool L_35 = V_1;
		if (L_35)
		{
			goto IL_017d;
		}
	}
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_36 = __this->get_synthChannels_27();
		int32_t L_37 = ___channel0;
		NullCheck(L_36);
		int32_t L_38 = L_37;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_39 = (L_36)->GetAt(static_cast<il2cpp_array_size_t>(L_38));
		int32_t L_40 = ___data23;
		NullCheck(L_39);
		L_39->set_bankSelect_1((uint8_t)(((int32_t)((uint8_t)L_40))));
		goto IL_0198;
	}

IL_017d:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_41 = __this->get_synthChannels_27();
		int32_t L_42 = ___channel0;
		NullCheck(L_41);
		int32_t L_43 = L_42;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_44 = (L_41)->GetAt(static_cast<il2cpp_array_size_t>(L_43));
		int32_t L_45 = ___channel0;
		G_B34_0 = L_44;
		if ((((int32_t)L_45) == ((int32_t)((int32_t)9))))
		{
			G_B35_0 = L_44;
			goto IL_018d;
		}
	}
	{
		G_B36_0 = 0;
		G_B36_1 = G_B34_0;
		goto IL_0192;
	}

IL_018d:
	{
		G_B36_0 = ((int32_t)128);
		G_B36_1 = G_B35_0;
	}

IL_0192:
	{
		NullCheck(G_B36_1);
		G_B36_1->set_bankSelect_1((uint8_t)G_B36_0);
	}

IL_0198:
	{
		goto IL_0547;
	}

IL_019d:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_46 = __this->get_synthChannels_27();
		int32_t L_47 = ___channel0;
		NullCheck(L_46);
		int32_t L_48 = L_47;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_49 = (L_46)->GetAt(static_cast<il2cpp_array_size_t>(L_48));
		NullCheck(L_49);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_50 = L_49->get_address_of_modRange_6();
		int32_t L_51 = ___data23;
		CCValue_set_Coarse_m5344D4D334F3D8D89B69019028B9DF82A2076649((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_50, (uint8_t)(((int32_t)((uint8_t)L_51))), /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_52 = __this->get_synthChannels_27();
		int32_t L_53 = ___channel0;
		NullCheck(L_52);
		int32_t L_54 = L_53;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		NullCheck(L_55);
		SynthParameters_UpdateCurrentMod_m8B938492578703025D6A3B5F01193E0D80B144E2(L_55, /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_01c6:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_56 = __this->get_synthChannels_27();
		int32_t L_57 = ___channel0;
		NullCheck(L_56);
		int32_t L_58 = L_57;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_59 = (L_56)->GetAt(static_cast<il2cpp_array_size_t>(L_58));
		NullCheck(L_59);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_60 = L_59->get_address_of_modRange_6();
		int32_t L_61 = ___data23;
		CCValue_set_Fine_mC5FE00B563D7B73BD9135AC48B16B2FFB5B65B6D((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_60, (uint8_t)(((int32_t)((uint8_t)L_61))), /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_62 = __this->get_synthChannels_27();
		int32_t L_63 = ___channel0;
		NullCheck(L_62);
		int32_t L_64 = L_63;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_65 = (L_62)->GetAt(static_cast<il2cpp_array_size_t>(L_64));
		NullCheck(L_65);
		SynthParameters_UpdateCurrentMod_m8B938492578703025D6A3B5F01193E0D80B144E2(L_65, /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_01ef:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_66 = __this->get_synthChannels_27();
		int32_t L_67 = ___channel0;
		NullCheck(L_66);
		int32_t L_68 = L_67;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_69 = (L_66)->GetAt(static_cast<il2cpp_array_size_t>(L_68));
		NullCheck(L_69);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_70 = L_69->get_address_of_volume_4();
		int32_t L_71 = ___data23;
		CCValue_set_Coarse_m5344D4D334F3D8D89B69019028B9DF82A2076649((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_70, (uint8_t)(((int32_t)((uint8_t)L_71))), /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_020a:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_72 = __this->get_synthChannels_27();
		int32_t L_73 = ___channel0;
		NullCheck(L_72);
		int32_t L_74 = L_73;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_75 = (L_72)->GetAt(static_cast<il2cpp_array_size_t>(L_74));
		NullCheck(L_75);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_76 = L_75->get_address_of_volume_4();
		int32_t L_77 = ___data23;
		CCValue_set_Fine_mC5FE00B563D7B73BD9135AC48B16B2FFB5B65B6D((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_76, (uint8_t)(((int32_t)((uint8_t)L_77))), /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_0225:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_78 = __this->get_synthChannels_27();
		int32_t L_79 = ___channel0;
		NullCheck(L_78);
		int32_t L_80 = L_79;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_81 = (L_78)->GetAt(static_cast<il2cpp_array_size_t>(L_80));
		NullCheck(L_81);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_82 = L_81->get_address_of_pan_3();
		int32_t L_83 = ___data23;
		CCValue_set_Coarse_m5344D4D334F3D8D89B69019028B9DF82A2076649((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_82, (uint8_t)(((int32_t)((uint8_t)L_83))), /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_84 = __this->get_synthChannels_27();
		int32_t L_85 = ___channel0;
		NullCheck(L_84);
		int32_t L_86 = L_85;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_87 = (L_84)->GetAt(static_cast<il2cpp_array_size_t>(L_86));
		NullCheck(L_87);
		SynthParameters_UpdateCurrentPan_m08069E906C15598A7B038FB984A6D5D56B593090(L_87, /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_024e:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_88 = __this->get_synthChannels_27();
		int32_t L_89 = ___channel0;
		NullCheck(L_88);
		int32_t L_90 = L_89;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_91 = (L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_90));
		NullCheck(L_91);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_92 = L_91->get_address_of_pan_3();
		int32_t L_93 = ___data23;
		CCValue_set_Fine_mC5FE00B563D7B73BD9135AC48B16B2FFB5B65B6D((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_92, (uint8_t)(((int32_t)((uint8_t)L_93))), /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_94 = __this->get_synthChannels_27();
		int32_t L_95 = ___channel0;
		NullCheck(L_94);
		int32_t L_96 = L_95;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_97 = (L_94)->GetAt(static_cast<il2cpp_array_size_t>(L_96));
		NullCheck(L_97);
		SynthParameters_UpdateCurrentPan_m08069E906C15598A7B038FB984A6D5D56B593090(L_97, /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_0277:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_98 = __this->get_synthChannels_27();
		int32_t L_99 = ___channel0;
		NullCheck(L_98);
		int32_t L_100 = L_99;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_101 = (L_98)->GetAt(static_cast<il2cpp_array_size_t>(L_100));
		NullCheck(L_101);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_102 = L_101->get_address_of_expression_5();
		int32_t L_103 = ___data23;
		CCValue_set_Coarse_m5344D4D334F3D8D89B69019028B9DF82A2076649((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_102, (uint8_t)(((int32_t)((uint8_t)L_103))), /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_104 = __this->get_synthChannels_27();
		int32_t L_105 = ___channel0;
		NullCheck(L_104);
		int32_t L_106 = L_105;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_107 = (L_104)->GetAt(static_cast<il2cpp_array_size_t>(L_106));
		NullCheck(L_107);
		SynthParameters_UpdateCurrentVolume_mF65FAC56F5D1EA02E14EA6FB0D4FBFBFE85C2C61(L_107, /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_02a0:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_108 = __this->get_synthChannels_27();
		int32_t L_109 = ___channel0;
		NullCheck(L_108);
		int32_t L_110 = L_109;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_111 = (L_108)->GetAt(static_cast<il2cpp_array_size_t>(L_110));
		NullCheck(L_111);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_112 = L_111->get_address_of_expression_5();
		int32_t L_113 = ___data23;
		CCValue_set_Fine_mC5FE00B563D7B73BD9135AC48B16B2FFB5B65B6D((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_112, (uint8_t)(((int32_t)((uint8_t)L_113))), /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_114 = __this->get_synthChannels_27();
		int32_t L_115 = ___channel0;
		NullCheck(L_114);
		int32_t L_116 = L_115;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_117 = (L_114)->GetAt(static_cast<il2cpp_array_size_t>(L_116));
		NullCheck(L_117);
		SynthParameters_UpdateCurrentVolume_mF65FAC56F5D1EA02E14EA6FB0D4FBFBFE85C2C61(L_117, /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_02c9:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_118 = __this->get_synthChannels_27();
		int32_t L_119 = ___channel0;
		NullCheck(L_118);
		int32_t L_120 = L_119;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_121 = (L_118)->GetAt(static_cast<il2cpp_array_size_t>(L_120));
		NullCheck(L_121);
		bool L_122 = L_121->get_holdPedal_12();
		if (!L_122)
		{
			goto IL_02e0;
		}
	}
	{
		int32_t L_123 = ___data23;
		G_B49_0 = ((((int32_t)L_123) > ((int32_t)((int32_t)63)))? 1 : 0);
		goto IL_02e1;
	}

IL_02e0:
	{
		G_B49_0 = 1;
	}

IL_02e1:
	{
		V_1 = (bool)G_B49_0;
		bool L_124 = V_1;
		if (L_124)
		{
			goto IL_02ee;
		}
	}
	{
		int32_t L_125 = ___channel0;
		Synthesizer_ReleaseHoldPedal_m8DD7E693144C996CC0EC5451B000B5F5C6203029(__this, L_125, /*hidden argument*/NULL);
	}

IL_02ee:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_126 = __this->get_synthChannels_27();
		int32_t L_127 = ___channel0;
		NullCheck(L_126);
		int32_t L_128 = L_127;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_129 = (L_126)->GetAt(static_cast<il2cpp_array_size_t>(L_128));
		int32_t L_130 = ___data23;
		NullCheck(L_129);
		L_129->set_holdPedal_12((bool)((((int32_t)L_130) > ((int32_t)((int32_t)63)))? 1 : 0));
		goto IL_0547;
	}

IL_0306:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_131 = __this->get_synthChannels_27();
		int32_t L_132 = ___channel0;
		NullCheck(L_131);
		int32_t L_133 = L_132;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_134 = (L_131)->GetAt(static_cast<il2cpp_array_size_t>(L_133));
		int32_t L_135 = ___data23;
		NullCheck(L_134);
		L_134->set_legatoPedal_13((bool)((((int32_t)L_135) > ((int32_t)((int32_t)63)))? 1 : 0));
		goto IL_0547;
	}

IL_031e:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_136 = __this->get_synthChannels_27();
		int32_t L_137 = ___channel0;
		NullCheck(L_136);
		int32_t L_138 = L_137;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_139 = (L_136)->GetAt(static_cast<il2cpp_array_size_t>(L_138));
		NullCheck(L_139);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_140 = L_139->get_address_of_rpn_14();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_140, (int16_t)((int32_t)16383), /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_033b:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_141 = __this->get_synthChannels_27();
		int32_t L_142 = ___channel0;
		NullCheck(L_141);
		int32_t L_143 = L_142;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_144 = (L_141)->GetAt(static_cast<il2cpp_array_size_t>(L_143));
		NullCheck(L_144);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_145 = L_144->get_address_of_rpn_14();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_145, (int16_t)((int32_t)16383), /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_0358:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_146 = __this->get_synthChannels_27();
		int32_t L_147 = ___channel0;
		NullCheck(L_146);
		int32_t L_148 = L_147;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_149 = (L_146)->GetAt(static_cast<il2cpp_array_size_t>(L_148));
		NullCheck(L_149);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_150 = L_149->get_address_of_rpn_14();
		int32_t L_151 = ___data23;
		CCValue_set_Coarse_m5344D4D334F3D8D89B69019028B9DF82A2076649((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_150, (uint8_t)(((int32_t)((uint8_t)L_151))), /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_0373:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_152 = __this->get_synthChannels_27();
		int32_t L_153 = ___channel0;
		NullCheck(L_152);
		int32_t L_154 = L_153;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_155 = (L_152)->GetAt(static_cast<il2cpp_array_size_t>(L_154));
		NullCheck(L_155);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_156 = L_155->get_address_of_rpn_14();
		int32_t L_157 = ___data23;
		CCValue_set_Fine_mC5FE00B563D7B73BD9135AC48B16B2FFB5B65B6D((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_156, (uint8_t)(((int32_t)((uint8_t)L_157))), /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_038e:
	{
		Synthesizer_NoteOffAll_m09B0E9EA1805A76EDADE4E4E0443AFD9AB7FAA88(__this, (bool)1, /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_039b:
	{
		Synthesizer_NoteOffAll_m09B0E9EA1805A76EDADE4E4E0443AFD9AB7FAA88(__this, (bool)0, /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_03a8:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_158 = __this->get_synthChannels_27();
		int32_t L_159 = ___channel0;
		NullCheck(L_158);
		int32_t L_160 = L_159;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_161 = (L_158)->GetAt(static_cast<il2cpp_array_size_t>(L_160));
		NullCheck(L_161);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_162 = L_161->get_address_of_rpn_14();
		int16_t L_163 = CCValue_get_Combined_mE4C5B314DE167D020D5A07C59295911EF384E5E4((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_162, /*hidden argument*/NULL);
		V_2 = L_163;
		int16_t L_164 = V_2;
		switch (L_164)
		{
			case 0:
			{
				goto IL_03cf;
			}
			case 1:
			{
				goto IL_03ef;
			}
			case 2:
			{
				goto IL_0407;
			}
		}
	}
	{
		goto IL_041c;
	}

IL_03cf:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_165 = __this->get_synthChannels_27();
		int32_t L_166 = ___channel0;
		NullCheck(L_165);
		int32_t L_167 = L_166;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_168 = (L_165)->GetAt(static_cast<il2cpp_array_size_t>(L_167));
		int32_t L_169 = ___data23;
		NullCheck(L_168);
		L_168->set_pitchBendRangeCoarse_8((uint8_t)(((int32_t)((uint8_t)L_169))));
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_170 = __this->get_synthChannels_27();
		int32_t L_171 = ___channel0;
		NullCheck(L_170);
		int32_t L_172 = L_171;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_173 = (L_170)->GetAt(static_cast<il2cpp_array_size_t>(L_172));
		NullCheck(L_173);
		SynthParameters_UpdateCurrentPitch_mE15F0D19B7BE2079C6347066DA363D578A4FEB08(L_173, /*hidden argument*/NULL);
		goto IL_041e;
	}

IL_03ef:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_174 = __this->get_synthChannels_27();
		int32_t L_175 = ___channel0;
		NullCheck(L_174);
		int32_t L_176 = L_175;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_177 = (L_174)->GetAt(static_cast<il2cpp_array_size_t>(L_176));
		NullCheck(L_177);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_178 = L_177->get_address_of_masterFineTune_11();
		int32_t L_179 = ___data23;
		CCValue_set_Coarse_m5344D4D334F3D8D89B69019028B9DF82A2076649((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_178, (uint8_t)(((int32_t)((uint8_t)L_179))), /*hidden argument*/NULL);
		goto IL_041e;
	}

IL_0407:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_180 = __this->get_synthChannels_27();
		int32_t L_181 = ___channel0;
		NullCheck(L_180);
		int32_t L_182 = L_181;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_183 = (L_180)->GetAt(static_cast<il2cpp_array_size_t>(L_182));
		int32_t L_184 = ___data23;
		NullCheck(L_183);
		L_183->set_masterCoarseTune_10((((int16_t)((int16_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_184, (int32_t)((int32_t)64)))))));
		goto IL_041e;
	}

IL_041c:
	{
		goto IL_041e;
	}

IL_041e:
	{
		goto IL_0547;
	}

IL_0423:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_185 = __this->get_synthChannels_27();
		int32_t L_186 = ___channel0;
		NullCheck(L_185);
		int32_t L_187 = L_186;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_188 = (L_185)->GetAt(static_cast<il2cpp_array_size_t>(L_187));
		NullCheck(L_188);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_189 = L_188->get_address_of_rpn_14();
		int16_t L_190 = CCValue_get_Combined_mE4C5B314DE167D020D5A07C59295911EF384E5E4((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_189, /*hidden argument*/NULL);
		V_2 = L_190;
		int16_t L_191 = V_2;
		switch (L_191)
		{
			case 0:
			{
				goto IL_0446;
			}
			case 1:
			{
				goto IL_0466;
			}
		}
	}
	{
		goto IL_047e;
	}

IL_0446:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_192 = __this->get_synthChannels_27();
		int32_t L_193 = ___channel0;
		NullCheck(L_192);
		int32_t L_194 = L_193;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_195 = (L_192)->GetAt(static_cast<il2cpp_array_size_t>(L_194));
		int32_t L_196 = ___data23;
		NullCheck(L_195);
		L_195->set_pitchBendRangeFine_9((uint8_t)(((int32_t)((uint8_t)L_196))));
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_197 = __this->get_synthChannels_27();
		int32_t L_198 = ___channel0;
		NullCheck(L_197);
		int32_t L_199 = L_198;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_200 = (L_197)->GetAt(static_cast<il2cpp_array_size_t>(L_199));
		NullCheck(L_200);
		SynthParameters_UpdateCurrentPitch_mE15F0D19B7BE2079C6347066DA363D578A4FEB08(L_200, /*hidden argument*/NULL);
		goto IL_0480;
	}

IL_0466:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_201 = __this->get_synthChannels_27();
		int32_t L_202 = ___channel0;
		NullCheck(L_201);
		int32_t L_203 = L_202;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_204 = (L_201)->GetAt(static_cast<il2cpp_array_size_t>(L_203));
		NullCheck(L_204);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_205 = L_204->get_address_of_masterFineTune_11();
		int32_t L_206 = ___data23;
		CCValue_set_Fine_mC5FE00B563D7B73BD9135AC48B16B2FFB5B65B6D((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_205, (uint8_t)(((int32_t)((uint8_t)L_206))), /*hidden argument*/NULL);
		goto IL_0480;
	}

IL_047e:
	{
		goto IL_0480;
	}

IL_0480:
	{
		goto IL_0547;
	}

IL_0485:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_207 = __this->get_synthChannels_27();
		int32_t L_208 = ___channel0;
		NullCheck(L_207);
		int32_t L_209 = L_208;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_210 = (L_207)->GetAt(static_cast<il2cpp_array_size_t>(L_209));
		NullCheck(L_210);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_211 = L_210->get_address_of_expression_5();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_211, (int16_t)((int32_t)16383), /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_212 = __this->get_synthChannels_27();
		int32_t L_213 = ___channel0;
		NullCheck(L_212);
		int32_t L_214 = L_213;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_215 = (L_212)->GetAt(static_cast<il2cpp_array_size_t>(L_214));
		NullCheck(L_215);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_216 = L_215->get_address_of_modRange_6();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_216, (int16_t)0, /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_217 = __this->get_synthChannels_27();
		int32_t L_218 = ___channel0;
		NullCheck(L_217);
		int32_t L_219 = L_218;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_220 = (L_217)->GetAt(static_cast<il2cpp_array_size_t>(L_219));
		NullCheck(L_220);
		bool L_221 = L_220->get_holdPedal_12();
		V_1 = (bool)((((int32_t)L_221) == ((int32_t)0))? 1 : 0);
		bool L_222 = V_1;
		if (L_222)
		{
			goto IL_04cd;
		}
	}
	{
		int32_t L_223 = ___channel0;
		Synthesizer_ReleaseHoldPedal_m8DD7E693144C996CC0EC5451B000B5F5C6203029(__this, L_223, /*hidden argument*/NULL);
	}

IL_04cd:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_224 = __this->get_synthChannels_27();
		int32_t L_225 = ___channel0;
		NullCheck(L_224);
		int32_t L_226 = L_225;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_227 = (L_224)->GetAt(static_cast<il2cpp_array_size_t>(L_226));
		NullCheck(L_227);
		L_227->set_holdPedal_12((bool)0);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_228 = __this->get_synthChannels_27();
		int32_t L_229 = ___channel0;
		NullCheck(L_228);
		int32_t L_230 = L_229;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_231 = (L_228)->GetAt(static_cast<il2cpp_array_size_t>(L_230));
		NullCheck(L_231);
		L_231->set_legatoPedal_13((bool)0);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_232 = __this->get_synthChannels_27();
		int32_t L_233 = ___channel0;
		NullCheck(L_232);
		int32_t L_234 = L_233;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_235 = (L_232)->GetAt(static_cast<il2cpp_array_size_t>(L_234));
		NullCheck(L_235);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_236 = L_235->get_address_of_rpn_14();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_236, (int16_t)((int32_t)16383), /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_237 = __this->get_synthChannels_27();
		int32_t L_238 = ___channel0;
		NullCheck(L_237);
		int32_t L_239 = L_238;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_240 = (L_237)->GetAt(static_cast<il2cpp_array_size_t>(L_239));
		NullCheck(L_240);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_241 = L_240->get_address_of_pitchBend_7();
		CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_241, (int16_t)((int32_t)8192), /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_242 = __this->get_synthChannels_27();
		int32_t L_243 = ___channel0;
		NullCheck(L_242);
		int32_t L_244 = L_243;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_245 = (L_242)->GetAt(static_cast<il2cpp_array_size_t>(L_244));
		NullCheck(L_245);
		L_245->set_channelAfterTouch_2((uint8_t)0);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_246 = __this->get_synthChannels_27();
		int32_t L_247 = ___channel0;
		NullCheck(L_246);
		int32_t L_248 = L_247;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_249 = (L_246)->GetAt(static_cast<il2cpp_array_size_t>(L_248));
		NullCheck(L_249);
		SynthParameters_UpdateCurrentPitch_mE15F0D19B7BE2079C6347066DA363D578A4FEB08(L_249, /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_250 = __this->get_synthChannels_27();
		int32_t L_251 = ___channel0;
		NullCheck(L_250);
		int32_t L_252 = L_251;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_253 = (L_250)->GetAt(static_cast<il2cpp_array_size_t>(L_252));
		NullCheck(L_253);
		SynthParameters_UpdateCurrentVolume_mF65FAC56F5D1EA02E14EA6FB0D4FBFBFE85C2C61(L_253, /*hidden argument*/NULL);
		goto IL_0547;
	}

IL_0545:
	{
		goto IL_05a9;
	}

IL_0547:
	{
		goto IL_05a9;
	}

IL_0549:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_254 = __this->get_synthChannels_27();
		int32_t L_255 = ___channel0;
		NullCheck(L_254);
		int32_t L_256 = L_255;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_257 = (L_254)->GetAt(static_cast<il2cpp_array_size_t>(L_256));
		int32_t L_258 = ___data12;
		NullCheck(L_257);
		L_257->set_program_0((uint8_t)(((int32_t)((uint8_t)L_258))));
		goto IL_05a9;
	}

IL_055a:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_259 = __this->get_synthChannels_27();
		int32_t L_260 = ___channel0;
		NullCheck(L_259);
		int32_t L_261 = L_260;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_262 = (L_259)->GetAt(static_cast<il2cpp_array_size_t>(L_261));
		int32_t L_263 = ___data23;
		NullCheck(L_262);
		L_262->set_channelAfterTouch_2((uint8_t)(((int32_t)((uint8_t)L_263))));
		goto IL_05a9;
	}

IL_056c:
	{
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_264 = __this->get_synthChannels_27();
		int32_t L_265 = ___channel0;
		NullCheck(L_264);
		int32_t L_266 = L_265;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_267 = (L_264)->GetAt(static_cast<il2cpp_array_size_t>(L_266));
		NullCheck(L_267);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_268 = L_267->get_address_of_pitchBend_7();
		int32_t L_269 = ___data23;
		CCValue_set_Coarse_m5344D4D334F3D8D89B69019028B9DF82A2076649((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_268, (uint8_t)(((int32_t)((uint8_t)L_269))), /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_270 = __this->get_synthChannels_27();
		int32_t L_271 = ___channel0;
		NullCheck(L_270);
		int32_t L_272 = L_271;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_273 = (L_270)->GetAt(static_cast<il2cpp_array_size_t>(L_272));
		NullCheck(L_273);
		CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B * L_274 = L_273->get_address_of_pitchBend_7();
		int32_t L_275 = ___data12;
		CCValue_set_Fine_mC5FE00B563D7B73BD9135AC48B16B2FFB5B65B6D((CCValue_t61555889DA4A27F441C685FBCFB1D14C2B068C6B *)L_274, (uint8_t)(((int32_t)((uint8_t)L_275))), /*hidden argument*/NULL);
		SynthParametersU5BU5D_tDE27622433D46B705CE69C80150018F500537EFE* L_276 = __this->get_synthChannels_27();
		int32_t L_277 = ___channel0;
		NullCheck(L_276);
		int32_t L_278 = L_277;
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_279 = (L_276)->GetAt(static_cast<il2cpp_array_size_t>(L_278));
		NullCheck(L_279);
		SynthParameters_UpdateCurrentPitch_mE15F0D19B7BE2079C6347066DA363D578A4FEB08(L_279, /*hidden argument*/NULL);
		goto IL_05a9;
	}

IL_05a7:
	{
		goto IL_05a9;
	}

IL_05a9:
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ReleaseAllHoldPedals()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_ReleaseAllHoldPedals_mAA8BF558958F700C063299A6DDC2D27B38F16239 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Synthesizer_ReleaseAllHoldPedals_mAA8BF558958F700C063299A6DDC2D27B38F16239_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * V_0 = NULL;
	bool V_1 = false;
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_0 = __this->get_voiceManager_18();
		NullCheck(L_0);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_1 = L_0->get_activeVoices_3();
		NullCheck(L_1);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_2 = LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline(L_1, /*hidden argument*/LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var);
		V_0 = L_2;
		goto IL_0054;
	}

IL_0014:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_3 = V_0;
		NullCheck(L_3);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_4 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_3, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_4);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_5 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		bool L_6 = L_5->get_noteOffPending_3();
		V_1 = (bool)((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
		bool L_7 = V_1;
		if (L_7)
		{
			goto IL_004c;
		}
	}
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_8 = V_0;
		NullCheck(L_8);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_9 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_8, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_9);
		Voice_Stop_m8E10957D7FABBF4A40F8370F5D8D77EE39A64567(L_9, /*hidden argument*/NULL);
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_10 = __this->get_voiceManager_18();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_11 = V_0;
		NullCheck(L_11);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_12 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_11, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_10);
		VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079(L_10, L_12, /*hidden argument*/NULL);
	}

IL_004c:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_13 = V_0;
		NullCheck(L_13);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_14 = LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA(L_13, /*hidden argument*/LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA_RuntimeMethod_var);
		V_0 = L_14;
	}

IL_0054:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_15 = V_0;
		V_1 = (bool)((((int32_t)((((RuntimeObject*)(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *)L_15) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_16 = V_1;
		if (L_16)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ReleaseHoldPedal(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer_ReleaseHoldPedal_m8DD7E693144C996CC0EC5451B000B5F5C6203029 (Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * __this, int32_t ___channel0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Synthesizer_ReleaseHoldPedal_m8DD7E693144C996CC0EC5451B000B5F5C6203029_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B4_0 = 0;
	{
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_0 = __this->get_voiceManager_18();
		NullCheck(L_0);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_1 = L_0->get_activeVoices_3();
		NullCheck(L_1);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_2 = LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline(L_1, /*hidden argument*/LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var);
		V_0 = L_2;
		goto IL_006b;
	}

IL_0014:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_3 = V_0;
		NullCheck(L_3);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_4 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_3, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_4);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_5 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_channel_0();
		int32_t L_7 = ___channel0;
		if ((!(((uint32_t)L_6) == ((uint32_t)L_7))))
		{
			goto IL_003d;
		}
	}
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_8 = V_0;
		NullCheck(L_8);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_9 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_8, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_9);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_10 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		bool L_11 = L_10->get_noteOffPending_3();
		G_B4_0 = ((((int32_t)L_11) == ((int32_t)0))? 1 : 0);
		goto IL_003e;
	}

IL_003d:
	{
		G_B4_0 = 1;
	}

IL_003e:
	{
		V_1 = (bool)G_B4_0;
		bool L_12 = V_1;
		if (L_12)
		{
			goto IL_0063;
		}
	}
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_13 = V_0;
		NullCheck(L_13);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_14 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_13, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_14);
		Voice_Stop_m8E10957D7FABBF4A40F8370F5D8D77EE39A64567(L_14, /*hidden argument*/NULL);
		VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * L_15 = __this->get_voiceManager_18();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_16 = V_0;
		NullCheck(L_16);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_17 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_16, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_15);
		VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079(L_15, L_17, /*hidden argument*/NULL);
	}

IL_0063:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_18 = V_0;
		NullCheck(L_18);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_19 = LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA(L_18, /*hidden argument*/LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA_RuntimeMethod_var);
		V_0 = L_19;
	}

IL_006b:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_20 = V_0;
		V_1 = (bool)((((int32_t)((((RuntimeObject*)(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *)L_20) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_21 = V_1;
		if (L_21)
		{
			goto IL_0014;
		}
	}
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Synthesizer__cctor_m33EB9B17280769B6A4DD40FC6C6F28BE0E8A8CF2 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Synthesizer__cctor_m33EB9B17280769B6A4DD40FC6C6F28BE0E8A8CF2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		((Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB_StaticFields*)il2cpp_codegen_static_fields_for(Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB_il2cpp_TypeInfo_var))->set_InterpolationMode_16(1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemCommonEvent::get_Channel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SystemCommonEvent_get_Channel_m6274962E4FFFB0C5821DFA421C5B8C6C40A3746A (SystemCommonEvent_t9ED10AE0931452F227792401D07F5DD1645A63CC * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = (-1);
		goto IL_0005;
	}

IL_0005:
	{
		int32_t L_0 = V_0;
		return L_0;
	}
}
// System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemCommonEvent::get_Command()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t SystemCommonEvent_get_Command_m30A25EC8C57C646000414E73CA7FBE545F84C894 (SystemCommonEvent_t9ED10AE0931452F227792401D07F5DD1645A63CC * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ((MidiEvent_t0A4643C6A8CBCA997171ABC2EFEC507BA8C5AE9C *)__this)->get_message_1();
		V_0 = ((int32_t)((int32_t)L_0&(int32_t)((int32_t)255)));
		goto IL_0010;
	}

IL_0010:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemCommonEvent::.ctor(System.Int32,System.Byte,System.Byte,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SystemCommonEvent__ctor_m28761B5E8EB12382AEB313AFDA05F256C81D8598 (SystemCommonEvent_t9ED10AE0931452F227792401D07F5DD1645A63CC * __this, int32_t ___delta0, uint8_t ___status1, uint8_t ___data12, uint8_t ___data23, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___delta0;
		uint8_t L_1 = ___status1;
		uint8_t L_2 = ___data12;
		uint8_t L_3 = ___data23;
		MidiEvent__ctor_mC48EC23C4103B22E1ABE6FCD2B2288E9EFCCA423(__this, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.String DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemCommonEvent::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SystemCommonEvent_ToString_m8DB3E6A0EDBB51469405DF4B6CE965AE75EE2CD5 (SystemCommonEvent_t9ED10AE0931452F227792401D07F5DD1645A63CC * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SystemCommonEvent_ToString_m8DB3E6A0EDBB51469405DF4B6CE965AE75EE2CD5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  L_0 = { reinterpret_cast<intptr_t> (SystemCommonTypeEnum_t8CD91F9148AD22D4C0B78296D4A1B7F233493B7D_0_0_0_var) };
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m8BB57524FF7F9DB1803BC561D2B3A4DBACEB385E(L_0, /*hidden argument*/NULL);
		int32_t L_2 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::get_Command() */, __this);
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_il2cpp_TypeInfo_var);
		String_t* L_5 = Enum_GetName_mA141F96AFDC64AD7020374311750DBA47BFCA8FA(L_1, L_4, /*hidden argument*/NULL);
		String_t* L_6 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteral59A7C853558F85DED57BAB572BA391C4F22417B3, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_0028;
	}

IL_0028:
	{
		String_t* L_7 = V_0;
		return L_7;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemExclusiveEvent::.ctor(System.Int32,System.Byte,System.Int16,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SystemExclusiveEvent__ctor_m01B4CA589EF833C8271C11978B0C27EA06F211BC (SystemExclusiveEvent_tC8271D73FDE64FB3488EAC4F9C891831E661A79A * __this, int32_t ___delta0, uint8_t ___status1, int16_t ___id2, ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data3, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___delta0;
		uint8_t L_1 = ___status1;
		int16_t L_2 = ___id2;
		int16_t L_3 = ___id2;
		SystemCommonEvent__ctor_m28761B5E8EB12382AEB313AFDA05F256C81D8598(__this, L_0, L_1, (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_2&(int32_t)((int32_t)255)))))), (uint8_t)(((int32_t)((uint8_t)((int32_t)((int32_t)L_3>>(int32_t)8))))), /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_4 = ___data3;
		__this->set_mdata_2(L_4);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Util.Tables::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Tables__cctor_mCCC5D545C915A3798B31D354AE06708823EDCB6A (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tables__cctor_mCCC5D545C915A3798B31D354AE06708823EDCB6A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9* L_0 = (SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9*)(SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9*)SZArrayNew(SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9_il2cpp_TypeInfo_var, (uint32_t)4);
		((Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields*)il2cpp_codegen_static_fields_for(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_il2cpp_TypeInfo_var))->set_EnvelopeTables_0(L_0);
		SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9* L_1 = ((Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields*)il2cpp_codegen_static_fields_for(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_il2cpp_TypeInfo_var))->get_EnvelopeTables_0();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_2 = Tables_CreateSustainTable_mAFC5D677E0BB7E15AD4E03BC43A0AB78C9F65BF0(((int32_t)128), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_3 = Tables_RemoveDenormals_mC91B0A68C891D1245952E94A24A2E79A61F32BC1(L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_3);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)L_3);
		SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9* L_4 = ((Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields*)il2cpp_codegen_static_fields_for(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_il2cpp_TypeInfo_var))->get_EnvelopeTables_0();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_5 = Tables_CreateLinearTable_mE8A52196591057548DDA3DE71766B8E8423D5427(((int32_t)128), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_6 = Tables_RemoveDenormals_mC91B0A68C891D1245952E94A24A2E79A61F32BC1(L_5, /*hidden argument*/NULL);
		NullCheck(L_4);
		ArrayElementTypeCheck (L_4, L_6);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(1), (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)L_6);
		SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9* L_7 = ((Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields*)il2cpp_codegen_static_fields_for(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_il2cpp_TypeInfo_var))->get_EnvelopeTables_0();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_8 = Tables_CreateConcaveTable_m8DA88814E45FAB3F4ECCCA237BDEDB9AE8A68EC9(((int32_t)128), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_9 = Tables_RemoveDenormals_mC91B0A68C891D1245952E94A24A2E79A61F32BC1(L_8, /*hidden argument*/NULL);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_9);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)L_9);
		SingleU5BU5DU5BU5D_tE98ABA33B056D447449236AA9007392350412EC9* L_10 = ((Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields*)il2cpp_codegen_static_fields_for(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_il2cpp_TypeInfo_var))->get_EnvelopeTables_0();
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_11 = Tables_CreateConvexTable_m805C0608C71A722855983AFAEF2DEA8F689C5590(((int32_t)128), /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_12 = Tables_RemoveDenormals_mC91B0A68C891D1245952E94A24A2E79A61F32BC1(L_11, /*hidden argument*/NULL);
		NullCheck(L_10);
		ArrayElementTypeCheck (L_10, L_12);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(3), (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)L_12);
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_13 = Tables_CreateCentTable_mDA1EEA30489EB4C540F4F76AE4D8D946138D6F45(/*hidden argument*/NULL);
		((Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields*)il2cpp_codegen_static_fields_for(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_il2cpp_TypeInfo_var))->set_CentTable_2(L_13);
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_14 = Tables_CreateSemitoneTable_m8A0B5078A2F897C4D3C6A3538248C6F0CF41C072(/*hidden argument*/NULL);
		((Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_StaticFields*)il2cpp_codegen_static_fields_for(Tables_tAD04A2F0C8FC076BC19028EB82F36520E2790699_il2cpp_TypeInfo_var))->set_SemitoneTable_1(L_14);
		return;
	}
}
// System.Double[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateCentTable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* Tables_CreateCentTable_mDA1EEA30489EB4C540F4F76AE4D8D946138D6F45 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tables_CreateCentTable_mDA1EEA30489EB4C540F4F76AE4D8D946138D6F45_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* V_0 = NULL;
	int32_t V_1 = 0;
	DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* V_2 = NULL;
	bool V_3 = false;
	{
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_0 = (DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB*)(DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB*)SZArrayNew(DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)201));
		V_0 = L_0;
		V_1 = 0;
		goto IL_003d;
	}

IL_0010:
	{
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_4 = Math_Pow_mC2C8700DAAD1316AA457A1D271F78CDF0D61AC2F((2.0), ((double)((double)((double)il2cpp_codegen_subtract((double)(((double)((double)L_3))), (double)(100.0)))/(double)(1200.0))), /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (double)L_4);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_003d:
	{
		int32_t L_6 = V_1;
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_7 = V_0;
		NullCheck(L_7);
		V_3 = (bool)((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length))))))? 1 : 0);
		bool L_8 = V_3;
		if (L_8)
		{
			goto IL_0010;
		}
	}
	{
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_9 = V_0;
		V_2 = L_9;
		goto IL_004b;
	}

IL_004b:
	{
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_10 = V_2;
		return L_10;
	}
}
// System.Double[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateSemitoneTable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* Tables_CreateSemitoneTable_m8A0B5078A2F897C4D3C6A3538248C6F0CF41C072 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tables_CreateSemitoneTable_m8A0B5078A2F897C4D3C6A3538248C6F0CF41C072_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* V_0 = NULL;
	int32_t V_1 = 0;
	DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* V_2 = NULL;
	bool V_3 = false;
	{
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_0 = (DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB*)(DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB*)SZArrayNew(DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB_il2cpp_TypeInfo_var, (uint32_t)((int32_t)255));
		V_0 = L_0;
		V_1 = 0;
		goto IL_003d;
	}

IL_0010:
	{
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_1 = V_0;
		int32_t L_2 = V_1;
		int32_t L_3 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_4 = Math_Pow_mC2C8700DAAD1316AA457A1D271F78CDF0D61AC2F((2.0), ((double)((double)((double)il2cpp_codegen_subtract((double)(((double)((double)L_3))), (double)(127.0)))/(double)(12.0))), /*hidden argument*/NULL);
		NullCheck(L_1);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(L_2), (double)L_4);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_003d:
	{
		int32_t L_6 = V_1;
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_7 = V_0;
		NullCheck(L_7);
		V_3 = (bool)((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length))))))? 1 : 0);
		bool L_8 = V_3;
		if (L_8)
		{
			goto IL_0010;
		}
	}
	{
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_9 = V_0;
		V_2 = L_9;
		goto IL_004b;
	}

IL_004b:
	{
		DoubleU5BU5D_t8E1B42EB2ABB79FBD193A6B8C8D97A7CDE44A4FB* L_10 = V_2;
		return L_10;
	}
}
// System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateSustainTable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* Tables_CreateSustainTable_mAFC5D677E0BB7E15AD4E03BC43A0AB78C9F65BF0 (int32_t ___size0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tables_CreateSustainTable_mAFC5D677E0BB7E15AD4E03BC43A0AB78C9F65BF0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* V_0 = NULL;
	int32_t V_1 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* V_2 = NULL;
	bool V_3 = false;
	{
		int32_t L_0 = ___size0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_1 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_0);
		V_0 = L_1;
		V_1 = 0;
		goto IL_001a;
	}

IL_000c:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_2 = V_0;
		int32_t L_3 = V_1;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (float)(1.0f));
		int32_t L_4 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_4, (int32_t)1));
	}

IL_001a:
	{
		int32_t L_5 = V_1;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_6 = V_0;
		NullCheck(L_6);
		V_3 = (bool)((((int32_t)L_5) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length))))))? 1 : 0);
		bool L_7 = V_3;
		if (L_7)
		{
			goto IL_000c;
		}
	}
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_8 = V_0;
		V_2 = L_8;
		goto IL_0028;
	}

IL_0028:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_9 = V_2;
		return L_9;
	}
}
// System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateLinearTable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* Tables_CreateLinearTable_mE8A52196591057548DDA3DE71766B8E8423D5427 (int32_t ___size0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tables_CreateLinearTable_mE8A52196591057548DDA3DE71766B8E8423D5427_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* V_0 = NULL;
	int32_t V_1 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* V_2 = NULL;
	bool V_3 = false;
	{
		int32_t L_0 = ___size0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_1 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_0);
		V_0 = L_1;
		V_1 = 0;
		goto IL_001c;
	}

IL_000c:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_2 = V_0;
		int32_t L_3 = V_1;
		int32_t L_4 = V_1;
		int32_t L_5 = ___size0;
		NullCheck(L_2);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (float)((float)((float)(((float)((float)L_4)))/(float)(((float)((float)((int32_t)il2cpp_codegen_subtract((int32_t)L_5, (int32_t)1))))))));
		int32_t L_6 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_001c:
	{
		int32_t L_7 = V_1;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_8 = V_0;
		NullCheck(L_8);
		V_3 = (bool)((((int32_t)L_7) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_8)->max_length))))))? 1 : 0);
		bool L_9 = V_3;
		if (L_9)
		{
			goto IL_000c;
		}
	}
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_10 = V_0;
		V_2 = L_10;
		goto IL_002a;
	}

IL_002a:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_11 = V_2;
		return L_11;
	}
}
// System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateConcaveTable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* Tables_CreateConcaveTable_m8DA88814E45FAB3F4ECCCA237BDEDB9AE8A68EC9 (int32_t ___size0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tables_CreateConcaveTable_m8DA88814E45FAB3F4ECCCA237BDEDB9AE8A68EC9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* V_4 = NULL;
	bool V_5 = false;
	{
		int32_t L_0 = ___size0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_1 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_0);
		V_0 = L_1;
		int32_t L_2 = ___size0;
		int32_t L_3 = ___size0;
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1)), (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)1))));
		V_2 = 0;
		goto IL_003a;
	}

IL_0014:
	{
		int32_t L_4 = ___size0;
		int32_t L_5 = V_2;
		V_3 = ((int32_t)il2cpp_codegen_subtract((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)1)), (int32_t)L_5));
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_6 = V_0;
		int32_t L_7 = V_2;
		int32_t L_8 = V_3;
		int32_t L_9 = V_3;
		int32_t L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_11 = log10(((double)((double)(((double)((double)((int32_t)il2cpp_codegen_multiply((int32_t)L_8, (int32_t)L_9)))))/(double)(((double)((double)L_10))))));
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (float)(((float)((float)((double)il2cpp_codegen_multiply((double)(-0.20833333333333334), (double)L_11))))));
		int32_t L_12 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
	}

IL_003a:
	{
		int32_t L_13 = V_2;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_14 = V_0;
		NullCheck(L_14);
		V_5 = (bool)((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_14)->max_length))))))? 1 : 0);
		bool L_15 = V_5;
		if (L_15)
		{
			goto IL_0014;
		}
	}
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_16 = V_0;
		int32_t L_17 = ___size0;
		NullCheck(L_16);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_subtract((int32_t)L_17, (int32_t)1))), (float)(1.0f));
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_18 = V_0;
		V_4 = L_18;
		goto IL_0055;
	}

IL_0055:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_19 = V_4;
		return L_19;
	}
}
// System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateConvexTable(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* Tables_CreateConvexTable_m805C0608C71A722855983AFAEF2DEA8F689C5590 (int32_t ___size0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tables_CreateConvexTable_m805C0608C71A722855983AFAEF2DEA8F689C5590_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* V_3 = NULL;
	bool V_4 = false;
	{
		int32_t L_0 = ___size0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_1 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)L_0);
		V_0 = L_1;
		int32_t L_2 = ___size0;
		int32_t L_3 = ___size0;
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)1)), (int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_3, (int32_t)1))));
		V_2 = 0;
		goto IL_003e;
	}

IL_0014:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_4 = V_0;
		int32_t L_5 = V_2;
		int32_t L_6 = V_2;
		int32_t L_7 = V_2;
		int32_t L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_9 = log10(((double)((double)(((double)((double)((int32_t)il2cpp_codegen_multiply((int32_t)L_6, (int32_t)L_7)))))/(double)(((double)((double)L_8))))));
		NullCheck(L_4);
		(L_4)->SetAt(static_cast<il2cpp_array_size_t>(L_5), (float)(((float)((float)((double)il2cpp_codegen_add((double)(1.0), (double)((double)il2cpp_codegen_multiply((double)(0.20833333333333334), (double)L_9))))))));
		int32_t L_10 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_003e:
	{
		int32_t L_11 = V_2;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_12 = V_0;
		NullCheck(L_12);
		V_4 = (bool)((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))))))? 1 : 0);
		bool L_13 = V_4;
		if (L_13)
		{
			goto IL_0014;
		}
	}
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_14 = V_0;
		NullCheck(L_14);
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(0), (float)(0.0f));
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_15 = V_0;
		V_3 = L_15;
		goto IL_0056;
	}

IL_0056:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_16 = V_3;
		return L_16;
	}
}
// System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::RemoveDenormals(System.Single[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* Tables_RemoveDenormals_mC91B0A68C891D1245952E94A24A2E79A61F32BC1 (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___data0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Tables_RemoveDenormals_mC91B0A68C891D1245952E94A24A2E79A61F32BC1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* V_1 = NULL;
	bool V_2 = false;
	{
		V_0 = 0;
		goto IL_0026;
	}

IL_0005:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_0 = ___data0;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		float L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		float L_4 = fabsf(L_3);
		V_2 = (bool)((!(((float)L_4) <= ((float)(9.99999935E-39f))))? 1 : 0);
		bool L_5 = V_2;
		if (L_5)
		{
			goto IL_0021;
		}
	}
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_6 = ___data0;
		int32_t L_7 = V_0;
		NullCheck(L_6);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (float)(0.0f));
	}

IL_0021:
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_8, (int32_t)1));
	}

IL_0026:
	{
		int32_t L_9 = V_0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_10 = ___data0;
		NullCheck(L_10);
		V_2 = (bool)((((int32_t)L_9) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length))))))? 1 : 0);
		bool L_11 = V_2;
		if (L_11)
		{
			goto IL_0005;
		}
	}
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_12 = ___data0;
		V_1 = L_12;
		goto IL_0034;
	}

IL_0034:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_13 = V_1;
		return L_13;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.TriangleGenerator::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriangleGenerator__ctor_m1922B9805C5F5B03370052B44309CC04E2C62743 (TriangleGenerator_tA00D6605C3FB1CD04F819ED1F1A2CBE3EC99F8E6 * __this, GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69 * ___description0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TriangleGenerator__ctor_m1922B9805C5F5B03370052B44309CC04E2C62743_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69 * L_0 = ___description0;
		IL2CPP_RUNTIME_CLASS_INIT(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E_il2cpp_TypeInfo_var);
		Generator__ctor_mBD9A3E5A89476277268E8BEBC0F17E5591A903A8(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_end_8();
		V_0 = (bool)((((int32_t)((((double)L_1) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (L_2)
		{
			goto IL_0030;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_end_8((1.25));
	}

IL_0030:
	{
		double L_3 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_start_7();
		V_0 = (bool)((((int32_t)((((double)L_3) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_0;
		if (L_4)
		{
			goto IL_0057;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_start_7((0.25));
	}

IL_0057:
	{
		double L_5 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopEnd_6();
		V_0 = (bool)((((int32_t)((((double)L_5) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_0;
		if (L_6)
		{
			goto IL_007b;
		}
	}
	{
		double L_7 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_end_8();
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_loopEnd_6(L_7);
	}

IL_007b:
	{
		double L_8 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopStart_5();
		V_0 = (bool)((((int32_t)((((double)L_8) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_0;
		if (L_9)
		{
			goto IL_009f;
		}
	}
	{
		double L_10 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_start_7();
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_loopStart_5(L_10);
	}

IL_009f:
	{
		double L_11 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_genPeriod_10();
		V_0 = (bool)((((int32_t)((((double)L_11) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_12 = V_0;
		if (L_12)
		{
			goto IL_00c6;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_genPeriod_10((1.0));
	}

IL_00c6:
	{
		int16_t L_13 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_root_12();
		V_0 = (bool)((((int32_t)((((int32_t)L_13) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_14 = V_0;
		if (L_14)
		{
			goto IL_00de;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_root_12((int16_t)((int32_t)69));
	}

IL_00de:
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_freq_11((440.0));
		return;
	}
}
// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.TriangleGenerator::GetValue(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float TriangleGenerator_GetValue_mAE8FFCF40053D2C226AA07F9091268DD787BCCAB (TriangleGenerator_tA00D6605C3FB1CD04F819ED1F1A2CBE3EC99F8E6 * __this, double ___phase0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TriangleGenerator_GetValue_mAE8FFCF40053D2C226AA07F9091268DD787BCCAB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		double L_0 = ___phase0;
		double L_1 = ___phase0;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_2 = floor(((double)il2cpp_codegen_add((double)L_1, (double)(0.5))));
		double L_3 = fabs(((double)il2cpp_codegen_subtract((double)L_0, (double)L_2)));
		V_0 = (((float)((float)((double)il2cpp_codegen_subtract((double)((double)il2cpp_codegen_multiply((double)L_3, (double)(4.0))), (double)(1.0))))));
		goto IL_0030;
	}

IL_0030:
	{
		float L_4 = V_0;
		return L_4;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.TriangleGenerator::GetValues(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters,System.Single[],System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TriangleGenerator_GetValues_mD60A13DC9FC0A17E9E83426BD83E3047A15E6470 (TriangleGenerator_tA00D6605C3FB1CD04F819ED1F1A2CBE3EC99F8E6 * __this, GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * ___generatorParams0, SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___blockBuffer1, double ___increment2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TriangleGenerator_GetValues_mD60A13DC9FC0A17E9E83426BD83E3047A15E6470_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	int32_t V_4 = 0;
	{
		V_0 = 0;
	}

IL_0003:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_0 = ___generatorParams0;
		NullCheck(L_0);
		double L_1 = L_0->get_currentEnd_2();
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_2 = ___generatorParams0;
		NullCheck(L_2);
		double L_3 = L_2->get_phase_0();
		double L_4 = ___increment2;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_5 = ceil(((double)((double)((double)il2cpp_codegen_subtract((double)L_1, (double)L_3))/(double)L_4)));
		V_1 = (((int32_t)((int32_t)L_5)));
		int32_t L_6 = V_1;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_7 = ___blockBuffer1;
		NullCheck(L_7);
		int32_t L_8 = V_0;
		V_3 = (bool)((((int32_t)((((int32_t)L_6) > ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))), (int32_t)L_8))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_3;
		if (L_9)
		{
			goto IL_0089;
		}
	}
	{
		goto IL_0079;
	}

IL_002c:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_10 = ___blockBuffer1;
		int32_t L_11 = V_0;
		int32_t L_12 = L_11;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_13 = ___generatorParams0;
		NullCheck(L_13);
		double L_14 = L_13->get_phase_0();
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_15 = ___generatorParams0;
		NullCheck(L_15);
		double L_16 = L_15->get_phase_0();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_17 = floor(((double)il2cpp_codegen_add((double)L_16, (double)(0.5))));
		double L_18 = fabs(((double)il2cpp_codegen_subtract((double)L_14, (double)L_17)));
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_12), (float)(((float)((float)((double)il2cpp_codegen_subtract((double)((double)il2cpp_codegen_multiply((double)L_18, (double)(4.0))), (double)(1.0)))))));
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_19 = ___generatorParams0;
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_20 = L_19;
		NullCheck(L_20);
		double L_21 = L_20->get_phase_0();
		double L_22 = ___increment2;
		NullCheck(L_20);
		L_20->set_phase_0(((double)il2cpp_codegen_add((double)L_21, (double)L_22)));
	}

IL_0079:
	{
		int32_t L_23 = V_0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_24 = ___blockBuffer1;
		NullCheck(L_24);
		V_3 = (bool)((((int32_t)L_23) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_24)->max_length))))))? 1 : 0);
		bool L_25 = V_3;
		if (L_25)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_0161;
	}

IL_0089:
	{
		int32_t L_26 = V_0;
		int32_t L_27 = V_1;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)L_27));
		goto IL_00dd;
	}

IL_0090:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_28 = ___blockBuffer1;
		int32_t L_29 = V_0;
		int32_t L_30 = L_29;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_31 = ___generatorParams0;
		NullCheck(L_31);
		double L_32 = L_31->get_phase_0();
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_33 = ___generatorParams0;
		NullCheck(L_33);
		double L_34 = L_33->get_phase_0();
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_35 = floor(((double)il2cpp_codegen_add((double)L_34, (double)(0.5))));
		double L_36 = fabs(((double)il2cpp_codegen_subtract((double)L_32, (double)L_35)));
		NullCheck(L_28);
		(L_28)->SetAt(static_cast<il2cpp_array_size_t>(L_30), (float)(((float)((float)((double)il2cpp_codegen_subtract((double)((double)il2cpp_codegen_multiply((double)L_36, (double)(4.0))), (double)(1.0)))))));
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_37 = ___generatorParams0;
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_38 = L_37;
		NullCheck(L_38);
		double L_39 = L_38->get_phase_0();
		double L_40 = ___increment2;
		NullCheck(L_38);
		L_38->set_phase_0(((double)il2cpp_codegen_add((double)L_39, (double)L_40)));
	}

IL_00dd:
	{
		int32_t L_41 = V_0;
		int32_t L_42 = V_2;
		V_3 = (bool)((((int32_t)L_41) < ((int32_t)L_42))? 1 : 0);
		bool L_43 = V_3;
		if (L_43)
		{
			goto IL_0090;
		}
	}
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_44 = ___generatorParams0;
		NullCheck(L_44);
		int32_t L_45 = L_44->get_currentState_3();
		V_4 = L_45;
		int32_t L_46 = V_4;
		switch (L_46)
		{
			case 0:
			{
				goto IL_0102;
			}
			case 1:
			{
				goto IL_0123;
			}
			case 2:
			{
				goto IL_013f;
			}
		}
	}
	{
		goto IL_0160;
	}

IL_0102:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_47 = ___generatorParams0;
		double L_48 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopStart_5();
		NullCheck(L_47);
		L_47->set_currentStart_1(L_48);
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_49 = ___generatorParams0;
		double L_50 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopEnd_6();
		NullCheck(L_49);
		L_49->set_currentEnd_2(L_50);
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_51 = ___generatorParams0;
		NullCheck(L_51);
		L_51->set_currentState_3(1);
		goto IL_0160;
	}

IL_0123:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_52 = ___generatorParams0;
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_53 = L_52;
		NullCheck(L_53);
		double L_54 = L_53->get_phase_0();
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_55 = ___generatorParams0;
		NullCheck(L_55);
		double L_56 = L_55->get_currentStart_1();
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_57 = ___generatorParams0;
		NullCheck(L_57);
		double L_58 = L_57->get_currentEnd_2();
		NullCheck(L_53);
		L_53->set_phase_0(((double)il2cpp_codegen_add((double)L_54, (double)((double)il2cpp_codegen_subtract((double)L_56, (double)L_58)))));
		goto IL_0160;
	}

IL_013f:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_59 = ___generatorParams0;
		NullCheck(L_59);
		L_59->set_currentState_3(3);
		goto IL_0154;
	}

IL_0148:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_60 = ___blockBuffer1;
		int32_t L_61 = V_0;
		int32_t L_62 = L_61;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_62, (int32_t)1));
		NullCheck(L_60);
		(L_60)->SetAt(static_cast<il2cpp_array_size_t>(L_62), (float)(0.0f));
	}

IL_0154:
	{
		int32_t L_63 = V_0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_64 = ___blockBuffer1;
		NullCheck(L_64);
		V_3 = (bool)((((int32_t)L_63) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_64)->max_length))))))? 1 : 0);
		bool L_65 = V_3;
		if (L_65)
		{
			goto IL_0148;
		}
	}
	{
		goto IL_0160;
	}

IL_0160:
	{
	}

IL_0161:
	{
		int32_t L_66 = V_0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_67 = ___blockBuffer1;
		NullCheck(L_67);
		V_3 = (bool)((((int32_t)L_66) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_67)->max_length))))))? 1 : 0);
		bool L_68 = V_3;
		if (L_68)
		{
			goto IL_0003;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::get_Patch()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * Voice_get_Patch_m4758BB409337AC45CCFB570CF5283E155D725574 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method)
{
	Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * V_0 = NULL;
	{
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_0 = __this->get_patch_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_1 = V_0;
		return L_1;
	}
}
// DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::get_VoiceParams()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method)
{
	VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * V_0 = NULL;
	{
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_0 = __this->get_voiceparams_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_1 = V_0;
		return L_1;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice__ctor_mC8F53C63FE90B4AC4517F3FB27EF8E2CF73ABA28 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Voice__ctor_mC8F53C63FE90B4AC4517F3FB27EF8E2CF73ABA28_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_0 = (VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 *)il2cpp_codegen_object_new(VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2_il2cpp_TypeInfo_var);
		VoiceParameters__ctor_m7778D2A8080260881ACA92965AC75360900DF955(L_0, /*hidden argument*/NULL);
		__this->set_voiceparams_1(L_0);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice_Start_mFD62C40C95388C7CA2E2677758C8F25A75BCF807 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_0 = __this->get_voiceparams_1();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_state_4();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		goto IL_0039;
	}

IL_0015:
	{
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_3 = __this->get_patch_0();
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_4 = __this->get_voiceparams_1();
		NullCheck(L_3);
		bool L_5 = VirtFuncInvoker1< bool, VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * >::Invoke(5 /* System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::Start(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters) */, L_3, L_4);
		V_0 = (bool)((((int32_t)L_5) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_0;
		if (L_6)
		{
			goto IL_0039;
		}
	}
	{
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_7 = __this->get_voiceparams_1();
		NullCheck(L_7);
		L_7->set_state_4(2);
	}

IL_0039:
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Stop()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice_Stop_m8E10957D7FABBF4A40F8370F5D8D77EE39A64567 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_0 = __this->get_voiceparams_1();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_state_4();
		V_0 = (bool)((((int32_t)L_1) == ((int32_t)2))? 1 : 0);
		bool L_2 = V_0;
		if (L_2)
		{
			goto IL_0015;
		}
	}
	{
		goto IL_0033;
	}

IL_0015:
	{
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_3 = __this->get_voiceparams_1();
		NullCheck(L_3);
		L_3->set_state_4(1);
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_4 = __this->get_patch_0();
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_5 = __this->get_voiceparams_1();
		NullCheck(L_4);
		VirtActionInvoker1< VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * >::Invoke(6 /* System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::Stop(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters) */, L_4, L_5);
	}

IL_0033:
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::StopImmediately()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice_StopImmediately_m3675EECC2EF7522DE6BF677F419648631A557D21 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method)
{
	{
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_0 = __this->get_voiceparams_1();
		NullCheck(L_0);
		L_0->set_state_4(0);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Process(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice_Process_m7893B616DA247441F4231BC8EEE204B01C0D348C (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, int32_t ___startIndex0, int32_t ___endIndex1, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_0 = __this->get_voiceparams_1();
		NullCheck(L_0);
		int32_t L_1 = L_0->get_state_4();
		V_0 = (bool)((((int32_t)((((int32_t)L_1) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		goto IL_002c;
	}

IL_0018:
	{
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_3 = __this->get_patch_0();
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_4 = __this->get_voiceparams_1();
		int32_t L_5 = ___startIndex0;
		int32_t L_6 = ___endIndex1;
		NullCheck(L_3);
		VirtActionInvoker3< VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 *, int32_t, int32_t >::Invoke(4 /* System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::Process(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters,System.Int32,System.Int32) */, L_3, L_4, L_5, L_6);
	}

IL_002c:
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Configure(System.Int32,System.Int32,System.Int32,DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch,DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Voice_Configure_mBA82ADC3E8D74554E84FEF95363DFB9343FEE6F8 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, int32_t ___channel0, int32_t ___note1, int32_t ___velocity2, Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * ___patch3, SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * ___synthParams4, const RuntimeMethod* method)
{
	{
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_0 = __this->get_voiceparams_1();
		NullCheck(L_0);
		VoiceParameters_Reset_mDA734937FFF5A02839B368F183E970EA3EF2AD88(L_0, /*hidden argument*/NULL);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_1 = __this->get_voiceparams_1();
		int32_t L_2 = ___channel0;
		NullCheck(L_1);
		L_1->set_channel_0(L_2);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_3 = __this->get_voiceparams_1();
		int32_t L_4 = ___note1;
		NullCheck(L_3);
		L_3->set_note_1(L_4);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_5 = __this->get_voiceparams_1();
		int32_t L_6 = ___velocity2;
		NullCheck(L_5);
		L_5->set_velocity_2(L_6);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_7 = __this->get_voiceparams_1();
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_8 = ___synthParams4;
		NullCheck(L_7);
		L_7->set_synthParams_9(L_8);
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_9 = ___patch3;
		__this->set_patch_0(L_9);
		return;
	}
}
// System.String DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Voice_ToString_mE732283D32E218AF05F567D5EF879665199EB580 (Voice_tA46F178E48B0D771712A0AA48175620981094D47 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Voice_ToString_mE732283D32E218AF05F567D5EF879665199EB580_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B2_1 = NULL;
	String_t* G_B1_0 = NULL;
	String_t* G_B1_1 = NULL;
	String_t* G_B3_0 = NULL;
	String_t* G_B3_1 = NULL;
	String_t* G_B3_2 = NULL;
	{
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_0 = __this->get_voiceparams_1();
		NullCheck(L_0);
		String_t* L_1 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_0);
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_2 = __this->get_patch_0();
		G_B1_0 = _stringLiteral6F9D4D7102B03DC5B3DF641B1F99444EF5B4F50F;
		G_B1_1 = L_1;
		if (!L_2)
		{
			G_B2_0 = _stringLiteral6F9D4D7102B03DC5B3DF641B1F99444EF5B4F50F;
			G_B2_1 = L_1;
			goto IL_0026;
		}
	}
	{
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_3 = __this->get_patch_0();
		NullCheck(L_3);
		String_t* L_4 = Patch_get_Name_m2536C44C9116E695C9BBE81DD5FF0A1A11DDC3B9(L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		goto IL_002b;
	}

IL_0026:
	{
		G_B3_0 = _stringLiteral5BEFD8CC60A79699B5BB00E37BAC5B62D371E174;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
	}

IL_002b:
	{
		String_t* L_5 = String_Concat_m89EAB4C6A96B0E5C3F87300D6BE78D386B9EFC44(G_B3_2, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		V_0 = L_5;
		goto IL_0034;
	}

IL_0034:
	{
		String_t* L_6 = V_0;
		return L_6;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager__ctor_mF7DAE448488693FE2E641B7351FE9BC4A49850DF (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, int32_t ___voiceCount0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoiceManager__ctor_mF7DAE448488693FE2E641B7351FE9BC4A49850DF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		__this->set_stealingMethod_0(1);
		int32_t L_0 = ___voiceCount0;
		__this->set_polyphony_1(L_0);
		int32_t L_1 = ___voiceCount0;
		VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A* L_2 = (VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A*)(VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A*)SZArrayNew(VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A_il2cpp_TypeInfo_var, (uint32_t)L_1);
		__this->set_voicePool_5(L_2);
		int32_t L_3 = ___voiceCount0;
		VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665* L_4 = (VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665*)(VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665*)SZArrayNew(VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665_il2cpp_TypeInfo_var, (uint32_t)L_3);
		V_0 = L_4;
		V_1 = 0;
		goto IL_0048;
	}

IL_002d:
	{
		VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A* L_5 = __this->get_voicePool_5();
		int32_t L_6 = V_1;
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_7 = (Voice_tA46F178E48B0D771712A0AA48175620981094D47 *)il2cpp_codegen_object_new(Voice_tA46F178E48B0D771712A0AA48175620981094D47_il2cpp_TypeInfo_var);
		Voice__ctor_mC8F53C63FE90B4AC4517F3FB27EF8E2CF73ABA28(L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_7);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(L_6), (Voice_tA46F178E48B0D771712A0AA48175620981094D47 *)L_7);
		VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665* L_8 = V_0;
		int32_t L_9 = V_1;
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_10 = (VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)il2cpp_codegen_object_new(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E_il2cpp_TypeInfo_var);
		VoiceNode__ctor_mC30870DD5AAC5E8AC4D00ED2ADD82BD739D35BE1(L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		ArrayElementTypeCheck (L_8, L_10);
		(L_8)->SetAt(static_cast<il2cpp_array_size_t>(L_9), (VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)L_10);
		int32_t L_11 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_0048:
	{
		int32_t L_12 = V_1;
		VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A* L_13 = __this->get_voicePool_5();
		NullCheck(L_13);
		V_2 = (bool)((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length))))))? 1 : 0);
		bool L_14 = V_2;
		if (L_14)
		{
			goto IL_002d;
		}
	}
	{
		VoiceNodeU5BU5D_t0F5629B9F044EE1FC8C27B23FF180F06270E5665* L_15 = V_0;
		Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * L_16 = (Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 *)il2cpp_codegen_object_new(Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8_il2cpp_TypeInfo_var);
		Stack_1__ctor_m1B694765A8AEC57CEF3638BAFAC8C7A68961D466(L_16, (RuntimeObject*)(RuntimeObject*)L_15, /*hidden argument*/Stack_1__ctor_m1B694765A8AEC57CEF3638BAFAC8C7A68961D466_RuntimeMethod_var);
		__this->set_vnodes_6(L_16);
		VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A* L_17 = __this->get_voicePool_5();
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_18 = (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 *)il2cpp_codegen_object_new(LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m1A0E7433ED02FAF1B17980448DC31395B6DFDC7F(L_18, (RuntimeObject*)(RuntimeObject*)L_17, /*hidden argument*/LinkedList_1__ctor_m1A0E7433ED02FAF1B17980448DC31395B6DFDC7F_RuntimeMethod_var);
		__this->set_freeVoices_2(L_18);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_19 = (LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 *)il2cpp_codegen_object_new(LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413_il2cpp_TypeInfo_var);
		LinkedList_1__ctor_m90B1CFAE4273CB108436915F74E71A1650440B74(L_19, /*hidden argument*/LinkedList_1__ctor_m90B1CFAE4273CB108436915F74E71A1650440B74_RuntimeMethod_var);
		__this->set_activeVoices_3(L_19);
		il2cpp_array_size_t L_21[] = { (il2cpp_array_size_t)((int32_t)16), (il2cpp_array_size_t)((int32_t)128) };
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_20 = (VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB*)GenArrayNew(VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB_il2cpp_TypeInfo_var, L_21);
		__this->set_registry_4(L_20);
		return;
	}
}
// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::GetFreeVoice()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Voice_tA46F178E48B0D771712A0AA48175620981094D47 * VoiceManager_GetFreeVoice_mAA1AE9A540D670DCE54385FBE800AB099AEB3BF5 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoiceManager_GetFreeVoice_mAA1AE9A540D670DCE54385FBE800AB099AEB3BF5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Voice_tA46F178E48B0D771712A0AA48175620981094D47 * V_0 = NULL;
	Voice_tA46F178E48B0D771712A0AA48175620981094D47 * V_1 = NULL;
	bool V_2 = false;
	int32_t V_3 = 0;
	{
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_0 = __this->get_freeVoices_2();
		NullCheck(L_0);
		int32_t L_1 = LinkedList_1_get_Count_m583DD5509171DBFF46F108B1FE6439956AA7E1D8_inline(L_0, /*hidden argument*/LinkedList_1_get_Count_m583DD5509171DBFF46F108B1FE6439956AA7E1D8_RuntimeMethod_var);
		V_2 = (bool)((((int32_t)((((int32_t)L_1) > ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_2;
		if (L_2)
		{
			goto IL_0038;
		}
	}
	{
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_3 = __this->get_freeVoices_2();
		NullCheck(L_3);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_4 = LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline(L_3, /*hidden argument*/LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var);
		NullCheck(L_4);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_5 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_4, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		V_0 = L_5;
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_6 = __this->get_freeVoices_2();
		NullCheck(L_6);
		LinkedList_1_RemoveFirst_m5BB3D19F10EFA916947A8A64808BB81F065FFA0B(L_6, /*hidden argument*/LinkedList_1_RemoveFirst_m5BB3D19F10EFA916947A8A64808BB81F065FFA0B_RuntimeMethod_var);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_7 = V_0;
		V_1 = L_7;
		goto IL_0065;
	}

IL_0038:
	{
		int32_t L_8 = __this->get_stealingMethod_0();
		V_3 = L_8;
		int32_t L_9 = V_3;
		switch (L_9)
		{
			case 0:
			{
				goto IL_004f;
			}
			case 1:
			{
				goto IL_0058;
			}
		}
	}
	{
		goto IL_0061;
	}

IL_004f:
	{
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_10 = VoiceManager_StealOldest_m2DC42C690A28F3065FD16CB8E11DF2FCEC9EE24B(__this, /*hidden argument*/NULL);
		V_1 = L_10;
		goto IL_0065;
	}

IL_0058:
	{
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_11 = VoiceManager_StealQuietestVoice_m0B1D16AD611B24B483268771F1D4D0F19AA0F826(__this, /*hidden argument*/NULL);
		V_1 = L_11;
		goto IL_0065;
	}

IL_0061:
	{
		V_1 = (Voice_tA46F178E48B0D771712A0AA48175620981094D47 *)NULL;
		goto IL_0065;
	}

IL_0065:
	{
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_12 = V_1;
		return L_12;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::AddToRegistry(DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager_AddToRegistry_m2055B93BCC9715FEE1E9D8D2C5E412F2EAD416F6 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, Voice_tA46F178E48B0D771712A0AA48175620981094D47 * ___voice0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoiceManager_AddToRegistry_m2055B93BCC9715FEE1E9D8D2C5E412F2EAD416F6_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * V_0 = NULL;
	{
		Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * L_0 = __this->get_vnodes_6();
		NullCheck(L_0);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_1 = Stack_1_Pop_m7119358526FF3EE669090369C7C813EBEF1B4563(L_0, /*hidden argument*/Stack_1_Pop_m7119358526FF3EE669090369C7C813EBEF1B4563_RuntimeMethod_var);
		V_0 = L_1;
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_2 = V_0;
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_3 = ___voice0;
		NullCheck(L_2);
		L_2->set_Value_0(L_3);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_4 = V_0;
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_5 = __this->get_registry_4();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_6 = ___voice0;
		NullCheck(L_6);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_7 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = L_7->get_channel_0();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_9 = ___voice0;
		NullCheck(L_9);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_10 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		int32_t L_11 = L_10->get_note_1();
		NullCheck(L_5);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_12 = (L_5)->GetAt(L_8, L_11);
		NullCheck(L_4);
		L_4->set_Next_1(L_12);
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_13 = __this->get_registry_4();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_14 = ___voice0;
		NullCheck(L_14);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_15 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_14, /*hidden argument*/NULL);
		NullCheck(L_15);
		int32_t L_16 = L_15->get_channel_0();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_17 = ___voice0;
		NullCheck(L_17);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_18 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = L_18->get_note_1();
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_20 = V_0;
		NullCheck(L_13);
		(L_13)->SetAt(L_16, L_19, L_20);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::RemoveFromRegistry(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager_RemoveFromRegistry_mA5518367D8A75DE5B022440CE4B55CB75E502DE3 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, int32_t ___channel0, int32_t ___note1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoiceManager_RemoveFromRegistry_mA5518367D8A75DE5B022440CE4B55CB75E502DE3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * V_0 = NULL;
	bool V_1 = false;
	{
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_0 = __this->get_registry_4();
		int32_t L_1 = ___channel0;
		int32_t L_2 = ___note1;
		NullCheck(L_0);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_3 = (L_0)->GetAt(L_1, L_2);
		V_0 = L_3;
		goto IL_0027;
	}

IL_0011:
	{
		Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * L_4 = __this->get_vnodes_6();
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_5 = V_0;
		NullCheck(L_4);
		Stack_1_Push_m98F98B3188600DDF0EFBCBC85110CD878E52E155(L_4, L_5, /*hidden argument*/Stack_1_Push_m98F98B3188600DDF0EFBCBC85110CD878E52E155_RuntimeMethod_var);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_6 = V_0;
		NullCheck(L_6);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_7 = L_6->get_Next_1();
		V_0 = L_7;
	}

IL_0027:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_8 = V_0;
		V_1 = (bool)((((int32_t)((((RuntimeObject*)(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)L_8) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_1;
		if (L_9)
		{
			goto IL_0011;
		}
	}
	{
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_10 = __this->get_registry_4();
		int32_t L_11 = ___channel0;
		int32_t L_12 = ___note1;
		NullCheck(L_10);
		(L_10)->SetAt(L_11, L_12, (VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)NULL);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::RemoveFromRegistry(DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, Voice_tA46F178E48B0D771712A0AA48175620981094D47 * ___voice0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * V_0 = NULL;
	VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * V_1 = NULL;
	bool V_2 = false;
	{
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_0 = __this->get_registry_4();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_1 = ___voice0;
		NullCheck(L_1);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_2 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = L_2->get_channel_0();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_4 = ___voice0;
		NullCheck(L_4);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_5 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_note_1();
		NullCheck(L_0);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_7 = (L_0)->GetAt(L_3, L_6);
		V_0 = L_7;
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_8 = V_0;
		V_2 = (bool)((((int32_t)((((RuntimeObject*)(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)L_8) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_2;
		if (L_9)
		{
			goto IL_0033;
		}
	}
	{
		goto IL_00c9;
	}

IL_0033:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_10 = V_0;
		NullCheck(L_10);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_11 = L_10->get_Value_0();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_12 = ___voice0;
		V_2 = (bool)((((int32_t)((((RuntimeObject*)(Voice_tA46F178E48B0D771712A0AA48175620981094D47 *)L_11) == ((RuntimeObject*)(Voice_tA46F178E48B0D771712A0AA48175620981094D47 *)L_12))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_13 = V_2;
		if (L_13)
		{
			goto IL_007a;
		}
	}
	{
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_14 = __this->get_registry_4();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_15 = ___voice0;
		NullCheck(L_15);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_16 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_15, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = L_16->get_channel_0();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_18 = ___voice0;
		NullCheck(L_18);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_19 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_18, /*hidden argument*/NULL);
		NullCheck(L_19);
		int32_t L_20 = L_19->get_note_1();
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_21 = V_0;
		NullCheck(L_21);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_22 = L_21->get_Next_1();
		NullCheck(L_14);
		(L_14)->SetAt(L_17, L_20, L_22);
		Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * L_23 = __this->get_vnodes_6();
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_24 = V_0;
		NullCheck(L_23);
		Stack_1_Push_m98F98B3188600DDF0EFBCBC85110CD878E52E155(L_23, L_24, /*hidden argument*/Stack_1_Push_m98F98B3188600DDF0EFBCBC85110CD878E52E155_RuntimeMethod_var);
		goto IL_00c9;
	}

IL_007a:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_25 = V_0;
		V_1 = L_25;
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_26 = V_0;
		NullCheck(L_26);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_27 = L_26->get_Next_1();
		V_0 = L_27;
		goto IL_00bd;
	}

IL_0086:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_28 = V_0;
		NullCheck(L_28);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_29 = L_28->get_Value_0();
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_30 = ___voice0;
		V_2 = (bool)((((int32_t)((((RuntimeObject*)(Voice_tA46F178E48B0D771712A0AA48175620981094D47 *)L_29) == ((RuntimeObject*)(Voice_tA46F178E48B0D771712A0AA48175620981094D47 *)L_30))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_31 = V_2;
		if (L_31)
		{
			goto IL_00b3;
		}
	}
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_32 = V_1;
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_33 = V_0;
		NullCheck(L_33);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_34 = L_33->get_Next_1();
		NullCheck(L_32);
		L_32->set_Next_1(L_34);
		Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * L_35 = __this->get_vnodes_6();
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_36 = V_0;
		NullCheck(L_35);
		Stack_1_Push_m98F98B3188600DDF0EFBCBC85110CD878E52E155(L_35, L_36, /*hidden argument*/Stack_1_Push_m98F98B3188600DDF0EFBCBC85110CD878E52E155_RuntimeMethod_var);
		goto IL_00c9;
	}

IL_00b3:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_37 = V_0;
		V_1 = L_37;
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_38 = V_0;
		NullCheck(L_38);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_39 = L_38->get_Next_1();
		V_0 = L_39;
	}

IL_00bd:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_40 = V_0;
		V_2 = (bool)((((int32_t)((((RuntimeObject*)(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)L_40) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_41 = V_2;
		if (L_41)
		{
			goto IL_0086;
		}
	}
	{
	}

IL_00c9:
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::ClearRegistry()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager_ClearRegistry_mE7A9A5542422604EFAE47D0D4AF3B4D0A2F7B096 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoiceManager_ClearRegistry_mE7A9A5542422604EFAE47D0D4AF3B4D0A2F7B096_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * V_0 = NULL;
	VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * V_1 = NULL;
	bool V_2 = false;
	{
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_0 = __this->get_activeVoices_3();
		NullCheck(L_0);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_1 = LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline(L_0, /*hidden argument*/LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var);
		V_0 = L_1;
		goto IL_0096;
	}

IL_0012:
	{
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_2 = __this->get_registry_4();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_3 = V_0;
		NullCheck(L_3);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_4 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_3, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_4);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_5 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		int32_t L_6 = L_5->get_channel_0();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_7 = V_0;
		NullCheck(L_7);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_8 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_7, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_8);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_9 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		int32_t L_10 = L_9->get_note_1();
		NullCheck(L_2);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_11 = (L_2)->GetAt(L_6, L_10);
		V_1 = L_11;
		goto IL_0057;
	}

IL_0041:
	{
		Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * L_12 = __this->get_vnodes_6();
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_13 = V_1;
		NullCheck(L_12);
		Stack_1_Push_m98F98B3188600DDF0EFBCBC85110CD878E52E155(L_12, L_13, /*hidden argument*/Stack_1_Push_m98F98B3188600DDF0EFBCBC85110CD878E52E155_RuntimeMethod_var);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_14 = V_1;
		NullCheck(L_14);
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_15 = L_14->get_Next_1();
		V_1 = L_15;
	}

IL_0057:
	{
		VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_16 = V_1;
		V_2 = (bool)((((int32_t)((((RuntimeObject*)(VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)L_16) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_17 = V_2;
		if (L_17)
		{
			goto IL_0041;
		}
	}
	{
		VoiceNodeU5BU2CU5D_tD00E084D66A867C6E6C3D4F7920A1F458ACA99BB* L_18 = __this->get_registry_4();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_19 = V_0;
		NullCheck(L_19);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_20 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_19, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_20);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_21 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		int32_t L_22 = L_21->get_channel_0();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_23 = V_0;
		NullCheck(L_23);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_24 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_23, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_24);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_25 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		int32_t L_26 = L_25->get_note_1();
		NullCheck(L_18);
		(L_18)->SetAt(L_22, L_26, (VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E *)NULL);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_27 = V_0;
		NullCheck(L_27);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_28 = LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA(L_27, /*hidden argument*/LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA_RuntimeMethod_var);
		V_0 = L_28;
	}

IL_0096:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_29 = V_0;
		V_2 = (bool)((((int32_t)((((RuntimeObject*)(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *)L_29) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_30 = V_2;
		if (L_30)
		{
			goto IL_0012;
		}
	}
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::UnloadPatches()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceManager_UnloadPatches_mE8C788BCE71CD06511BDD72292BA1339E7CB27D2 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoiceManager_UnloadPatches_mE8C788BCE71CD06511BDD72292BA1339E7CB27D2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * V_1 = NULL;
	Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF  V_2;
	memset((&V_2), 0, sizeof(V_2));
	bool V_3 = false;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		V_0 = 0;
		goto IL_0059;
	}

IL_0005:
	{
		VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A* L_0 = __this->get_voicePool_5();
		int32_t L_1 = V_0;
		NullCheck(L_0);
		int32_t L_2 = L_1;
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_3 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_2));
		NullCheck(L_3);
		Voice_Configure_mBA82ADC3E8D74554E84FEF95363DFB9343FEE6F8(L_3, 0, 0, 0, (Patch_t3144A9674F069F684A9B015803D7A714F366C7AD *)NULL, (SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 *)NULL, /*hidden argument*/NULL);
		Stack_1_t0465DCE956B580C8F35DBF3FC3836AE3A47D7EC8 * L_4 = __this->get_vnodes_6();
		NullCheck(L_4);
		Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF  L_5 = Stack_1_GetEnumerator_m9110C85EAA6292636F99B48A549DE02B81ABE62A(L_4, /*hidden argument*/Stack_1_GetEnumerator_m9110C85EAA6292636F99B48A549DE02B81ABE62A_RuntimeMethod_var);
		V_2 = L_5;
	}

IL_0026:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0037;
		}

IL_0028:
		{
			VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_6 = Enumerator_get_Current_m15363A9312B73B1AD3E358A70EBB8BDF280DE3E5((Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF *)(&V_2), /*hidden argument*/Enumerator_get_Current_m15363A9312B73B1AD3E358A70EBB8BDF280DE3E5_RuntimeMethod_var);
			V_1 = L_6;
			VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * L_7 = V_1;
			NullCheck(L_7);
			L_7->set_Value_0((Voice_tA46F178E48B0D771712A0AA48175620981094D47 *)NULL);
		}

IL_0037:
		{
			bool L_8 = Enumerator_MoveNext_mB7075ADE4B6DB1460DF2B652EE538430B536077B((Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF *)(&V_2), /*hidden argument*/Enumerator_MoveNext_mB7075ADE4B6DB1460DF2B652EE538430B536077B_RuntimeMethod_var);
			V_3 = L_8;
			bool L_9 = V_3;
			if (L_9)
			{
				goto IL_0028;
			}
		}

IL_0042:
		{
			IL2CPP_LEAVE(0x53, FINALLY_0044);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0044;
	}

FINALLY_0044:
	{ // begin finally (depth: 1)
		Enumerator_Dispose_m290DF4EA35F9E4F1D910D42A42F4334930DABAA5((Enumerator_tB8ED2E8BED6A5391660E9F835D8C6A4015C489FF *)(&V_2), /*hidden argument*/Enumerator_Dispose_m290DF4EA35F9E4F1D910D42A42F4334930DABAA5_RuntimeMethod_var);
		IL2CPP_END_FINALLY(68)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(68)
	{
		IL2CPP_JUMP_TBL(0x53, IL_0053)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
	}

IL_0053:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0059:
	{
		int32_t L_11 = V_0;
		VoiceU5BU5D_tE60AC1065666513993716CE09F4A243C1118814A* L_12 = __this->get_voicePool_5();
		NullCheck(L_12);
		V_3 = (bool)((((int32_t)L_11) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))))))? 1 : 0);
		bool L_13 = V_3;
		if (L_13)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::StealOldest()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Voice_tA46F178E48B0D771712A0AA48175620981094D47 * VoiceManager_StealOldest_m2DC42C690A28F3065FD16CB8E11DF2FCEC9EE24B (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoiceManager_StealOldest_m2DC42C690A28F3065FD16CB8E11DF2FCEC9EE24B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * V_0 = NULL;
	Voice_tA46F178E48B0D771712A0AA48175620981094D47 * V_1 = NULL;
	bool V_2 = false;
	int32_t G_B5_0 = 0;
	{
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_0 = __this->get_activeVoices_3();
		NullCheck(L_0);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_1 = LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline(L_0, /*hidden argument*/LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var);
		V_0 = L_1;
		goto IL_0016;
	}

IL_000f:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_2 = V_0;
		NullCheck(L_2);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_3 = LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA(L_2, /*hidden argument*/LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA_RuntimeMethod_var);
		V_0 = L_3;
	}

IL_0016:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_4 = V_0;
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_5 = V_0;
		NullCheck(L_5);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_6 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_5, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_6);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_7 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = L_7->get_state_4();
		G_B5_0 = ((((int32_t)L_8) == ((int32_t)2))? 1 : 0);
		goto IL_002f;
	}

IL_002e:
	{
		G_B5_0 = 0;
	}

IL_002f:
	{
		V_2 = (bool)G_B5_0;
		bool L_9 = V_2;
		if (L_9)
		{
			goto IL_000f;
		}
	}
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_10 = V_0;
		V_2 = (bool)((((int32_t)((((RuntimeObject*)(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *)L_10) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_11 = V_2;
		if (L_11)
		{
			goto IL_004b;
		}
	}
	{
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_12 = __this->get_activeVoices_3();
		NullCheck(L_12);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_13 = LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline(L_12, /*hidden argument*/LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var);
		V_0 = L_13;
	}

IL_004b:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_14 = V_0;
		NullCheck(L_14);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_15 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_14, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079(__this, L_15, /*hidden argument*/NULL);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_16 = __this->get_activeVoices_3();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_17 = V_0;
		NullCheck(L_16);
		LinkedList_1_Remove_mECC8FD43A15126D559FFF8E2DDC62A1B6F27FADD(L_16, L_17, /*hidden argument*/LinkedList_1_Remove_mECC8FD43A15126D559FFF8E2DDC62A1B6F27FADD_RuntimeMethod_var);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_18 = V_0;
		NullCheck(L_18);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_19 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_18, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_19);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_20 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		L_20->set_state_4(0);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_21 = V_0;
		NullCheck(L_21);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_22 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_21, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		V_1 = L_22;
		goto IL_007f;
	}

IL_007f:
	{
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_23 = V_1;
		return L_23;
	}
}
// DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::StealQuietestVoice()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Voice_tA46F178E48B0D771712A0AA48175620981094D47 * VoiceManager_StealQuietestVoice_m0B1D16AD611B24B483268771F1D4D0F19AA0F826 (VoiceManager_tD8872714154CC7B0395D8DA62A6AFC399DCB11D9 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoiceManager_StealQuietestVoice_m0B1D16AD611B24B483268771F1D4D0F19AA0F826_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * V_1 = NULL;
	LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * V_2 = NULL;
	float V_3 = 0.0f;
	Voice_tA46F178E48B0D771712A0AA48175620981094D47 * V_4 = NULL;
	bool V_5 = false;
	{
		V_0 = (1000.0f);
		V_1 = (LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *)NULL;
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_0 = __this->get_activeVoices_3();
		NullCheck(L_0);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_1 = LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline(L_0, /*hidden argument*/LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var);
		V_2 = L_1;
		goto IL_005f;
	}

IL_0017:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_2 = V_2;
		NullCheck(L_2);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_3 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_2, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_3);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_4 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = L_4->get_state_4();
		V_5 = (bool)((((int32_t)L_5) == ((int32_t)2))? 1 : 0);
		bool L_6 = V_5;
		if (L_6)
		{
			goto IL_0057;
		}
	}
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_7 = V_2;
		NullCheck(L_7);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_8 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_7, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_8);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_9 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		float L_10 = VoiceParameters_get_CombinedVolume_m94289D4D075359DB54ABF1CC96F2F6B5F84EF004(L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		float L_11 = V_3;
		float L_12 = V_0;
		V_5 = (bool)((((int32_t)((((float)L_11) < ((float)L_12))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_13 = V_5;
		if (L_13)
		{
			goto IL_0056;
		}
	}
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_14 = V_2;
		V_1 = L_14;
		float L_15 = V_3;
		V_0 = L_15;
	}

IL_0056:
	{
	}

IL_0057:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_16 = V_2;
		NullCheck(L_16);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_17 = LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA(L_16, /*hidden argument*/LinkedListNode_1_get_Next_m948491F09E358E1013266CF6D86C1768FC6EEFBA_RuntimeMethod_var);
		V_2 = L_17;
	}

IL_005f:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_18 = V_2;
		V_5 = (bool)((((int32_t)((((RuntimeObject*)(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *)L_18) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_19 = V_5;
		if (L_19)
		{
			goto IL_0017;
		}
	}
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_20 = V_1;
		V_5 = (bool)((((int32_t)((((RuntimeObject*)(LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 *)L_20) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_21 = V_5;
		if (L_21)
		{
			goto IL_0085;
		}
	}
	{
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_22 = __this->get_activeVoices_3();
		NullCheck(L_22);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_23 = LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_inline(L_22, /*hidden argument*/LinkedList_1_get_First_mE4FC749D92B8451EE9C19D86B01EC3B2A80BF167_RuntimeMethod_var);
		V_1 = L_23;
	}

IL_0085:
	{
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_24 = V_1;
		NullCheck(L_24);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_25 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_24, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079(__this, L_25, /*hidden argument*/NULL);
		LinkedList_1_t98F2BBA58266330FEB8B8E63F841DBEF47AF1413 * L_26 = __this->get_activeVoices_3();
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_27 = V_1;
		NullCheck(L_26);
		LinkedList_1_Remove_mECC8FD43A15126D559FFF8E2DDC62A1B6F27FADD(L_26, L_27, /*hidden argument*/LinkedList_1_Remove_mECC8FD43A15126D559FFF8E2DDC62A1B6F27FADD_RuntimeMethod_var);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_28 = V_1;
		NullCheck(L_28);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_29 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_28, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		NullCheck(L_29);
		VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * L_30 = Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5(L_29, /*hidden argument*/NULL);
		NullCheck(L_30);
		L_30->set_state_4(0);
		LinkedListNode_1_tD5E1973AE7996E57255067C26C3651C5B2346675 * L_31 = V_1;
		NullCheck(L_31);
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_32 = LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_inline(L_31, /*hidden argument*/LinkedListNode_1_get_Value_m8D90DC22C20D73BC44337EB3634ED37D709C9132_RuntimeMethod_var);
		V_4 = L_32;
		goto IL_00ba;
	}

IL_00ba:
	{
		Voice_tA46F178E48B0D771712A0AA48175620981094D47 * L_33 = V_4;
		return L_33;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::get_CombinedVolume()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float VoiceParameters_get_CombinedVolume_m94289D4D075359DB54ABF1CC96F2F6B5F84EF004 (VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		float L_0 = __this->get_mix1_14();
		float L_1 = __this->get_mix2_15();
		V_0 = ((float)il2cpp_codegen_add((float)L_0, (float)L_1));
		goto IL_0011;
	}

IL_0011:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceParameters__ctor_m7778D2A8080260881ACA92965AC75360900DF955 (VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoiceParameters__ctor_m7778D2A8080260881ACA92965AC75360900DF955_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_0 = (SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)SZArrayNew(SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA_il2cpp_TypeInfo_var, (uint32_t)((int32_t)64));
		__this->set_blockBuffer_7(L_0);
		UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F* L_1 = (UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F*)(UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F*)SZArrayNew(UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F_il2cpp_TypeInfo_var, (uint32_t)4);
		__this->set_pData_8(L_1);
		GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A* L_2 = (GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A*)(GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A*)SZArrayNew(GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A_il2cpp_TypeInfo_var, (uint32_t)4);
		__this->set_generatorParams_10(L_2);
		EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81* L_3 = (EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81*)(EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81*)SZArrayNew(EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81_il2cpp_TypeInfo_var, (uint32_t)4);
		__this->set_envelopes_11(L_3);
		FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1* L_4 = (FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1*)(FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1*)SZArrayNew(FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1_il2cpp_TypeInfo_var, (uint32_t)4);
		__this->set_filters_12(L_4);
		LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52* L_5 = (LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52*)(LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52*)SZArrayNew(LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52_il2cpp_TypeInfo_var, (uint32_t)4);
		__this->set_lfos_13(L_5);
		V_0 = 0;
		goto IL_008f;
	}

IL_0055:
	{
		GeneratorParametersU5BU5D_t9F1F6A52DE76C06CE532FB75D1E8D5A0228F460A* L_6 = __this->get_generatorParams_10();
		int32_t L_7 = V_0;
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_8 = (GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 *)il2cpp_codegen_object_new(GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237_il2cpp_TypeInfo_var);
		GeneratorParameters__ctor_m3236F25398ABFBF56DC2671F38892A2E11339D2A(L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		ArrayElementTypeCheck (L_6, L_8);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(L_7), (GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 *)L_8);
		EnvelopeU5BU5D_tFD1F516581AA915788D4D884F9BC02340DB11F81* L_9 = __this->get_envelopes_11();
		int32_t L_10 = V_0;
		Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40 * L_11 = (Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40 *)il2cpp_codegen_object_new(Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40_il2cpp_TypeInfo_var);
		Envelope__ctor_mC790D05C10CDD1066A2347F220978EC7C3D812CA(L_11, /*hidden argument*/NULL);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_11);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (Envelope_t5C3655E0F50666167C2C89F126641D645B83AA40 *)L_11);
		FilterU5BU5D_t8899C1811292631CA787A36C7E741CCF20A7A9A1* L_12 = __this->get_filters_12();
		int32_t L_13 = V_0;
		Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B * L_14 = (Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B *)il2cpp_codegen_object_new(Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B_il2cpp_TypeInfo_var);
		Filter__ctor_mF15108D7CC9AC5426630CDBB7A7BB87479FF0574(L_14, /*hidden argument*/NULL);
		NullCheck(L_12);
		ArrayElementTypeCheck (L_12, L_14);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(L_13), (Filter_tA3F12239A348CD85AB682140B35A8F7C3C615D0B *)L_14);
		LfoU5BU5D_t8068B202468929A419721DEAE30DD3F28C098B52* L_15 = __this->get_lfos_13();
		int32_t L_16 = V_0;
		Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A * L_17 = (Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A *)il2cpp_codegen_object_new(Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A_il2cpp_TypeInfo_var);
		Lfo__ctor_m35CD7DD1CD4D7AA08749FC169FB112E7D95A1D10(L_17, /*hidden argument*/NULL);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_17);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(L_16), (Lfo_t8AA7D4490C8DF75F529FF6CA51F48F1CCEC44F9A *)L_17);
		int32_t L_18 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_18, (int32_t)1));
	}

IL_008f:
	{
		int32_t L_19 = V_0;
		V_1 = (bool)((((int32_t)L_19) < ((int32_t)4))? 1 : 0);
		bool L_20 = V_1;
		if (L_20)
		{
			goto IL_0055;
		}
	}
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceParameters_Reset_mDA734937FFF5A02839B368F183E970EA3EF2AD88 (VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * __this, const RuntimeMethod* method)
{
	{
		__this->set_noteOffPending_3((bool)0);
		__this->set_pitchOffset_5(0);
		__this->set_volOffset_6((0.0f));
		UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F* L_0 = __this->get_pData_8();
		UnionDataU5BU5D_t1A33252F9DE16B608FF75E10CDF1EF951FD8DF6F* L_1 = __this->get_pData_8();
		NullCheck(L_1);
		Array_Clear_mEB42D172C5E0825D340F6209F28578BDDDDCE34F((RuntimeArray *)(RuntimeArray *)L_0, 0, (((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length)))), /*hidden argument*/NULL);
		__this->set_mix1_14((0.0f));
		__this->set_mix2_15((0.0f));
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::MixMonoToMonoInterp(System.Int32,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceParameters_MixMonoToMonoInterp_mE3ED349806D42205C944668D14C4252B4913FE14 (VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * __this, int32_t ___startIndex0, float ___volume1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		float L_0 = ___volume1;
		float L_1 = __this->get_mix1_14();
		V_0 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_0, (float)L_1))/(float)(64.0f)));
		V_1 = 0;
		goto IL_005b;
	}

IL_0014:
	{
		float L_2 = __this->get_mix1_14();
		float L_3 = V_0;
		__this->set_mix1_14(((float)il2cpp_codegen_add((float)L_2, (float)L_3)));
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_4 = __this->get_synthParams_9();
		NullCheck(L_4);
		Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * L_5 = L_4->get_synth_15();
		NullCheck(L_5);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_6 = L_5->get_sampleBuffer_17();
		int32_t L_7 = ___startIndex0;
		int32_t L_8 = V_1;
		NullCheck(L_6);
		float* L_9 = ((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)L_8)))));
		float L_10 = (*(float*)L_9);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_11 = __this->get_blockBuffer_7();
		int32_t L_12 = V_1;
		NullCheck(L_11);
		int32_t L_13 = L_12;
		float L_14 = (L_11)->GetAt(static_cast<il2cpp_array_size_t>(L_13));
		float L_15 = __this->get_mix1_14();
		*(float*)L_9 = ((float)il2cpp_codegen_add((float)L_10, (float)((float)il2cpp_codegen_multiply((float)L_14, (float)L_15))));
		int32_t L_16 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_16, (int32_t)1));
	}

IL_005b:
	{
		int32_t L_17 = V_1;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_18 = __this->get_blockBuffer_7();
		NullCheck(L_18);
		V_2 = (bool)((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_18)->max_length))))))? 1 : 0);
		bool L_19 = V_2;
		if (L_19)
		{
			goto IL_0014;
		}
	}
	{
		float L_20 = ___volume1;
		__this->set_mix1_14(L_20);
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::MixMonoToStereoInterp(System.Int32,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceParameters_MixMonoToStereoInterp_mB043AACA2FEDAA23A140CD498739F70FA9CC6583 (VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * __this, int32_t ___startIndex0, float ___leftVol1, float ___rightVol2, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	bool V_3 = false;
	{
		float L_0 = ___leftVol1;
		float L_1 = __this->get_mix1_14();
		V_0 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_0, (float)L_1))/(float)(64.0f)));
		float L_2 = ___rightVol2;
		float L_3 = __this->get_mix2_15();
		V_1 = ((float)((float)((float)il2cpp_codegen_subtract((float)L_2, (float)L_3))/(float)(64.0f)));
		V_2 = 0;
		goto IL_00b1;
	}

IL_0026:
	{
		float L_4 = __this->get_mix1_14();
		float L_5 = V_0;
		__this->set_mix1_14(((float)il2cpp_codegen_add((float)L_4, (float)L_5)));
		float L_6 = __this->get_mix2_15();
		float L_7 = V_1;
		__this->set_mix2_15(((float)il2cpp_codegen_add((float)L_6, (float)L_7)));
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_8 = __this->get_synthParams_9();
		NullCheck(L_8);
		Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * L_9 = L_8->get_synth_15();
		NullCheck(L_9);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_10 = L_9->get_sampleBuffer_17();
		int32_t L_11 = ___startIndex0;
		NullCheck(L_10);
		float* L_12 = ((L_10)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_11)));
		float L_13 = (*(float*)L_12);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_14 = __this->get_blockBuffer_7();
		int32_t L_15 = V_2;
		NullCheck(L_14);
		int32_t L_16 = L_15;
		float L_17 = (L_14)->GetAt(static_cast<il2cpp_array_size_t>(L_16));
		float L_18 = __this->get_mix1_14();
		*(float*)L_12 = ((float)il2cpp_codegen_add((float)L_13, (float)((float)il2cpp_codegen_multiply((float)L_17, (float)L_18))));
		SynthParameters_t84891F8611CDE28B6038C0DE5FA22A7AC6605524 * L_19 = __this->get_synthParams_9();
		NullCheck(L_19);
		Synthesizer_tCC862FB65D699D519B1FD9773807EF549E0F9DBB * L_20 = L_19->get_synth_15();
		NullCheck(L_20);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_21 = L_20->get_sampleBuffer_17();
		int32_t L_22 = ___startIndex0;
		NullCheck(L_21);
		float* L_23 = ((L_21)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)1)))));
		float L_24 = (*(float*)L_23);
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_25 = __this->get_blockBuffer_7();
		int32_t L_26 = V_2;
		NullCheck(L_25);
		int32_t L_27 = L_26;
		float L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		float L_29 = __this->get_mix2_15();
		*(float*)L_23 = ((float)il2cpp_codegen_add((float)L_24, (float)((float)il2cpp_codegen_multiply((float)L_28, (float)L_29))));
		int32_t L_30 = ___startIndex0;
		___startIndex0 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)2));
		int32_t L_31 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_31, (int32_t)1));
	}

IL_00b1:
	{
		int32_t L_32 = V_2;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_33 = __this->get_blockBuffer_7();
		NullCheck(L_33);
		V_3 = (bool)((((int32_t)L_32) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_33)->max_length))))))? 1 : 0);
		bool L_34 = V_3;
		if (L_34)
		{
			goto IL_0026;
		}
	}
	{
		float L_35 = ___leftVol1;
		__this->set_mix1_14(L_35);
		float L_36 = ___rightVol2;
		__this->set_mix2_15(L_36);
		return;
	}
}
// System.String DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* VoiceParameters_ToString_mD01B9CC55062092042898C61E8827A72819ECEEF (VoiceParameters_t3EF2DDB5B28BC9490522EE2F813A814343C8A9A2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (VoiceParameters_ToString_mD01B9CC55062092042898C61E8827A72819ECEEF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* V_1 = NULL;
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)4);
		V_1 = L_0;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = V_1;
		int32_t L_2 = __this->get_channel_0();
		int32_t L_3 = L_2;
		RuntimeObject * L_4 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_4);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_5 = V_1;
		int32_t L_6 = __this->get_note_1();
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_7);
		NullCheck(L_5);
		ArrayElementTypeCheck (L_5, L_8);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_8);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_9 = V_1;
		int32_t L_10 = __this->get_velocity_2();
		int32_t L_11 = L_10;
		RuntimeObject * L_12 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		ArrayElementTypeCheck (L_9, L_12);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_12);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_13 = V_1;
		int32_t L_14 = __this->get_state_4();
		int32_t L_15 = L_14;
		RuntimeObject * L_16 = Box(VoiceStateEnum_tE420EB9CD65647BED146DE806F4977C97B9BCD8E_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		ArrayElementTypeCheck (L_13, L_16);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_16);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_17 = V_1;
		String_t* L_18 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteral67EF81B40230A7823C595B3E224C3D4212075F4C, L_17, /*hidden argument*/NULL);
		V_0 = L_18;
		goto IL_004e;
	}

IL_004e:
	{
		String_t* L_19 = V_0;
		return L_19;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] DaggerfallWorkshop.AudioSynthesis.Wave.WaveHelper::GetChannelPcmData(System.Byte[],System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* WaveHelper_GetChannelPcmData_m5EB36FDDF8E6ACF093946271CBD782402302FFD5 (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___pcmData0, int32_t ___bits1, int32_t ___channelCount2, int32_t ___expectedChannels3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveHelper_GetChannelPcmData_m5EB36FDDF8E6ACF093946271CBD782402302FFD5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_2 = NULL;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_6 = NULL;
	bool V_7 = false;
	{
		int32_t L_0 = ___bits1;
		V_0 = ((int32_t)((int32_t)L_0/(int32_t)8));
		int32_t L_1 = ___expectedChannels3;
		int32_t L_2 = ___channelCount2;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		int32_t L_3 = Math_Min_m4C6E1589800A3AA57C1F430C3903847E8D7B4574(L_1, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = ___expectedChannels3;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = ___pcmData0;
		NullCheck(L_5);
		int32_t L_6 = ___channelCount2;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_4, (int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length))))/(int32_t)L_6)))));
		V_2 = L_7;
		int32_t L_8 = V_0;
		int32_t L_9 = ___channelCount2;
		V_3 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_8, (int32_t)L_9));
		int32_t L_10 = V_0;
		int32_t L_11 = V_1;
		V_4 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_10, (int32_t)L_11));
		V_5 = 0;
		goto IL_0041;
	}

IL_0028:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_12 = ___pcmData0;
		int32_t L_13 = V_5;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_14 = V_2;
		int32_t L_15 = V_5;
		int32_t L_16 = V_3;
		int32_t L_17 = V_4;
		int32_t L_18 = V_4;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_12, L_13, (RuntimeArray *)(RuntimeArray *)L_14, ((int32_t)il2cpp_codegen_multiply((int32_t)((int32_t)((int32_t)L_15/(int32_t)L_16)), (int32_t)L_17)), L_18, /*hidden argument*/NULL);
		int32_t L_19 = V_5;
		int32_t L_20 = V_3;
		V_5 = ((int32_t)il2cpp_codegen_add((int32_t)L_19, (int32_t)L_20));
	}

IL_0041:
	{
		int32_t L_21 = V_5;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_22 = ___pcmData0;
		NullCheck(L_22);
		V_7 = (bool)((((int32_t)L_21) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_22)->max_length))))))? 1 : 0);
		bool L_23 = V_7;
		if (L_23)
		{
			goto IL_0028;
		}
	}
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_24 = V_2;
		V_6 = L_24;
		goto IL_0053;
	}

IL_0053:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_25 = V_6;
		return L_25;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Wave.WaveHelper::SwapEndianess(System.Byte[],System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaveHelper_SwapEndianess_mB6A4A015BA00E24FB9527DD87B2AA23FB10AB9AC (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* ___data0, int32_t ___bits1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WaveHelper_SwapEndianess_mB6A4A015BA00E24FB9527DD87B2AA23FB10AB9AC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* V_0 = NULL;
	int32_t V_1 = 0;
	bool V_2 = false;
	{
		int32_t L_0 = ___bits1;
		___bits1 = ((int32_t)((int32_t)L_0/(int32_t)8));
		int32_t L_1 = ___bits1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_2 = (ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726*)SZArrayNew(ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726_il2cpp_TypeInfo_var, (uint32_t)L_1);
		V_0 = L_2;
		V_1 = 0;
		goto IL_0034;
	}

IL_0011:
	{
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_3 = ___data0;
		int32_t L_4 = V_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_5 = V_0;
		int32_t L_6 = ___bits1;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_3, L_4, (RuntimeArray *)(RuntimeArray *)L_5, 0, L_6, /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_7 = V_0;
		Array_Reverse_mB87373AFAC1DBE600CAA60B79A985DD09555BF7D((RuntimeArray *)(RuntimeArray *)L_7, /*hidden argument*/NULL);
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_8 = V_0;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_9 = ___data0;
		int32_t L_10 = V_1;
		int32_t L_11 = ___bits1;
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_8, 0, (RuntimeArray *)(RuntimeArray *)L_9, L_10, L_11, /*hidden argument*/NULL);
		int32_t L_12 = V_1;
		int32_t L_13 = ___bits1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)L_13));
	}

IL_0034:
	{
		int32_t L_14 = V_1;
		ByteU5BU5D_tDBBEB0E8362242FA7223000D978B0DD19D4B0726* L_15 = ___data0;
		NullCheck(L_15);
		V_2 = (bool)((((int32_t)L_14) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_15)->max_length))))))? 1 : 0);
		bool L_16 = V_2;
		if (L_16)
		{
			goto IL_0011;
		}
	}
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.WhiteNoiseGenerator::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhiteNoiseGenerator__ctor_mFA126753A3BAA805CA77B1B93A962EAF9B61A8D3 (WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA * __this, GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69 * ___description0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhiteNoiseGenerator__ctor_mFA126753A3BAA805CA77B1B93A962EAF9B61A8D3_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	{
		GeneratorDescriptor_tE8D41E1A01D14B9D638B5B39D8603A0D82750D69 * L_0 = ___description0;
		IL2CPP_RUNTIME_CLASS_INIT(Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E_il2cpp_TypeInfo_var);
		Generator__ctor_mBD9A3E5A89476277268E8BEBC0F17E5591A903A8(__this, L_0, /*hidden argument*/NULL);
		double L_1 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_end_8();
		V_0 = (bool)((((int32_t)((((double)L_1) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_2 = V_0;
		if (L_2)
		{
			goto IL_0030;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_end_8((1.0));
	}

IL_0030:
	{
		double L_3 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_start_7();
		V_0 = (bool)((((int32_t)((((double)L_3) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_4 = V_0;
		if (L_4)
		{
			goto IL_0057;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_start_7((0.0));
	}

IL_0057:
	{
		double L_5 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopEnd_6();
		V_0 = (bool)((((int32_t)((((double)L_5) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_6 = V_0;
		if (L_6)
		{
			goto IL_007b;
		}
	}
	{
		double L_7 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_end_8();
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_loopEnd_6(L_7);
	}

IL_007b:
	{
		double L_8 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopStart_5();
		V_0 = (bool)((((int32_t)((((double)L_8) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_0;
		if (L_9)
		{
			goto IL_009f;
		}
	}
	{
		double L_10 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_start_7();
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_loopStart_5(L_10);
	}

IL_009f:
	{
		double L_11 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_genPeriod_10();
		V_0 = (bool)((((int32_t)((((double)L_11) < ((double)(0.0)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_12 = V_0;
		if (L_12)
		{
			goto IL_00c6;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_genPeriod_10((1.0));
	}

IL_00c6:
	{
		int16_t L_13 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_root_12();
		V_0 = (bool)((((int32_t)((((int32_t)L_13) < ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_14 = V_0;
		if (L_14)
		{
			goto IL_00de;
		}
	}
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_root_12((int16_t)((int32_t)69));
	}

IL_00de:
	{
		((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->set_freq_11((440.0));
		return;
	}
}
// System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.WhiteNoiseGenerator::GetValue(System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float WhiteNoiseGenerator_GetValue_m444D451C4A8B7B12460EF17BCC2E6FABC9C1B49C (WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA * __this, double ___phase0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhiteNoiseGenerator_GetValue_m444D451C4A8B7B12460EF17BCC2E6FABC9C1B49C_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_il2cpp_TypeInfo_var);
		Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * L_0 = ((WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_StaticFields*)il2cpp_codegen_static_fields_for(WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_il2cpp_TypeInfo_var))->get_random_16();
		NullCheck(L_0);
		double L_1 = VirtFuncInvoker0< double >::Invoke(8 /* System.Double System.Random::NextDouble() */, L_0);
		V_0 = (((float)((float)((double)il2cpp_codegen_subtract((double)((double)il2cpp_codegen_multiply((double)L_1, (double)(2.0))), (double)(1.0))))));
		goto IL_0023;
	}

IL_0023:
	{
		float L_2 = V_0;
		return L_2;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.WhiteNoiseGenerator::GetValues(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters,System.Single[],System.Double)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhiteNoiseGenerator_GetValues_m7ABC9C1794FADE89766A90495F6B75598FBD9316 (WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA * __this, GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * ___generatorParams0, SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* ___blockBuffer1, double ___increment2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhiteNoiseGenerator_GetValues_m7ABC9C1794FADE89766A90495F6B75598FBD9316_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	bool V_3 = false;
	int32_t V_4 = 0;
	{
		V_0 = 0;
	}

IL_0003:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_0 = ___generatorParams0;
		NullCheck(L_0);
		double L_1 = L_0->get_currentEnd_2();
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_2 = ___generatorParams0;
		NullCheck(L_2);
		double L_3 = L_2->get_phase_0();
		double L_4 = ___increment2;
		IL2CPP_RUNTIME_CLASS_INIT(Math_tA269614262430118C9FC5C4D9EF4F61C812568F0_il2cpp_TypeInfo_var);
		double L_5 = ceil(((double)((double)((double)il2cpp_codegen_subtract((double)L_1, (double)L_3))/(double)L_4)));
		V_1 = (((int32_t)((int32_t)L_5)));
		int32_t L_6 = V_1;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_7 = ___blockBuffer1;
		NullCheck(L_7);
		int32_t L_8 = V_0;
		V_3 = (bool)((((int32_t)((((int32_t)L_6) > ((int32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_7)->max_length)))), (int32_t)L_8))))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_9 = V_3;
		if (L_9)
		{
			goto IL_0072;
		}
	}
	{
		goto IL_0062;
	}

IL_002c:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_10 = ___blockBuffer1;
		int32_t L_11 = V_0;
		int32_t L_12 = L_11;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_12, (int32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_il2cpp_TypeInfo_var);
		Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * L_13 = ((WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_StaticFields*)il2cpp_codegen_static_fields_for(WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_il2cpp_TypeInfo_var))->get_random_16();
		NullCheck(L_13);
		double L_14 = VirtFuncInvoker0< double >::Invoke(8 /* System.Double System.Random::NextDouble() */, L_13);
		NullCheck(L_10);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(L_12), (float)(((float)((float)((double)il2cpp_codegen_subtract((double)((double)il2cpp_codegen_multiply((double)L_14, (double)(2.0))), (double)(1.0)))))));
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_15 = ___generatorParams0;
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_16 = L_15;
		NullCheck(L_16);
		double L_17 = L_16->get_phase_0();
		double L_18 = ___increment2;
		NullCheck(L_16);
		L_16->set_phase_0(((double)il2cpp_codegen_add((double)L_17, (double)L_18)));
	}

IL_0062:
	{
		int32_t L_19 = V_0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_20 = ___blockBuffer1;
		NullCheck(L_20);
		V_3 = (bool)((((int32_t)L_19) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_20)->max_length))))))? 1 : 0);
		bool L_21 = V_3;
		if (L_21)
		{
			goto IL_002c;
		}
	}
	{
		goto IL_0133;
	}

IL_0072:
	{
		int32_t L_22 = V_0;
		int32_t L_23 = V_1;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_22, (int32_t)L_23));
		goto IL_00af;
	}

IL_0079:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_24 = ___blockBuffer1;
		int32_t L_25 = V_0;
		int32_t L_26 = L_25;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_26, (int32_t)1));
		IL2CPP_RUNTIME_CLASS_INIT(WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_il2cpp_TypeInfo_var);
		Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * L_27 = ((WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_StaticFields*)il2cpp_codegen_static_fields_for(WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_il2cpp_TypeInfo_var))->get_random_16();
		NullCheck(L_27);
		double L_28 = VirtFuncInvoker0< double >::Invoke(8 /* System.Double System.Random::NextDouble() */, L_27);
		NullCheck(L_24);
		(L_24)->SetAt(static_cast<il2cpp_array_size_t>(L_26), (float)(((float)((float)((double)il2cpp_codegen_subtract((double)((double)il2cpp_codegen_multiply((double)L_28, (double)(2.0))), (double)(1.0)))))));
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_29 = ___generatorParams0;
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_30 = L_29;
		NullCheck(L_30);
		double L_31 = L_30->get_phase_0();
		double L_32 = ___increment2;
		NullCheck(L_30);
		L_30->set_phase_0(((double)il2cpp_codegen_add((double)L_31, (double)L_32)));
	}

IL_00af:
	{
		int32_t L_33 = V_0;
		int32_t L_34 = V_2;
		V_3 = (bool)((((int32_t)L_33) < ((int32_t)L_34))? 1 : 0);
		bool L_35 = V_3;
		if (L_35)
		{
			goto IL_0079;
		}
	}
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_36 = ___generatorParams0;
		NullCheck(L_36);
		int32_t L_37 = L_36->get_currentState_3();
		V_4 = L_37;
		int32_t L_38 = V_4;
		switch (L_38)
		{
			case 0:
			{
				goto IL_00d4;
			}
			case 1:
			{
				goto IL_00f5;
			}
			case 2:
			{
				goto IL_0111;
			}
		}
	}
	{
		goto IL_0132;
	}

IL_00d4:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_39 = ___generatorParams0;
		double L_40 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopStart_5();
		NullCheck(L_39);
		L_39->set_currentStart_1(L_40);
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_41 = ___generatorParams0;
		double L_42 = ((Generator_t0ED091205512AF38BABDD7FD0CED5DEBF9EC605E *)__this)->get_loopEnd_6();
		NullCheck(L_41);
		L_41->set_currentEnd_2(L_42);
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_43 = ___generatorParams0;
		NullCheck(L_43);
		L_43->set_currentState_3(1);
		goto IL_0132;
	}

IL_00f5:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_44 = ___generatorParams0;
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_45 = L_44;
		NullCheck(L_45);
		double L_46 = L_45->get_phase_0();
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_47 = ___generatorParams0;
		NullCheck(L_47);
		double L_48 = L_47->get_currentStart_1();
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_49 = ___generatorParams0;
		NullCheck(L_49);
		double L_50 = L_49->get_currentEnd_2();
		NullCheck(L_45);
		L_45->set_phase_0(((double)il2cpp_codegen_add((double)L_46, (double)((double)il2cpp_codegen_subtract((double)L_48, (double)L_50)))));
		goto IL_0132;
	}

IL_0111:
	{
		GeneratorParameters_t776E6B03419C3A1DBB030FC5AA242EEACDD0E237 * L_51 = ___generatorParams0;
		NullCheck(L_51);
		L_51->set_currentState_3(3);
		goto IL_0126;
	}

IL_011a:
	{
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_52 = ___blockBuffer1;
		int32_t L_53 = V_0;
		int32_t L_54 = L_53;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_54, (int32_t)1));
		NullCheck(L_52);
		(L_52)->SetAt(static_cast<il2cpp_array_size_t>(L_54), (float)(0.0f));
	}

IL_0126:
	{
		int32_t L_55 = V_0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_56 = ___blockBuffer1;
		NullCheck(L_56);
		V_3 = (bool)((((int32_t)L_55) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_56)->max_length))))))? 1 : 0);
		bool L_57 = V_3;
		if (L_57)
		{
			goto IL_011a;
		}
	}
	{
		goto IL_0132;
	}

IL_0132:
	{
	}

IL_0133:
	{
		int32_t L_58 = V_0;
		SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA* L_59 = ___blockBuffer1;
		NullCheck(L_59);
		V_3 = (bool)((((int32_t)L_58) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_59)->max_length))))))? 1 : 0);
		bool L_60 = V_3;
		if (L_60)
		{
			goto IL_0003;
		}
	}
	{
		return;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.WhiteNoiseGenerator::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WhiteNoiseGenerator__cctor_m2692BF76CDB9FA3654951A44D17441BC49B11655 (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (WhiteNoiseGenerator__cctor_m2692BF76CDB9FA3654951A44D17441BC49B11655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 * L_0 = (Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118 *)il2cpp_codegen_object_new(Random_t6C9E9775A149D0ADCFEB4B252C408F03EE870118_il2cpp_TypeInfo_var);
		Random__ctor_mF40AD1812BABC06235B661CCE513E4F74EEE9F05(L_0, /*hidden argument*/NULL);
		((WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_StaticFields*)il2cpp_codegen_static_fields_for(WhiteNoiseGenerator_t094B76D94460278E05C55E421CE439E1B279F8BA_il2cpp_TypeInfo_var))->set_random_16(L_0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[] DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::get_Modulators()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* Zone_get_Modulators_m819BC5A415FDD098C62F176644BC7B832E9D65DF (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * __this, const RuntimeMethod* method)
{
	ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* V_0 = NULL;
	{
		ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* L_0 = __this->get_modulators_0();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* L_1 = V_0;
		return L_1;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::set_Modulators(DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zone_set_Modulators_mCD8A63184ED534C3F54CB8C231624B866588FD75 (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * __this, ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* ___value0, const RuntimeMethod* method)
{
	{
		ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* L_0 = ___value0;
		__this->set_modulators_0(L_0);
		return;
	}
}
// DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[] DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::get_Generators()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* Zone_get_Generators_m3BDD4F7A064D556CD8FD224F4828417D82EAA019 (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * __this, const RuntimeMethod* method)
{
	GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* V_0 = NULL;
	{
		GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* L_0 = __this->get_generators_1();
		V_0 = L_0;
		goto IL_000a;
	}

IL_000a:
	{
		GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* L_1 = V_0;
		return L_1;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::set_Generators(DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zone_set_Generators_m051434F7E04B03839D9E45F687EF1D168E486E3D (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * __this, GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* ___value0, const RuntimeMethod* method)
{
	{
		GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* L_0 = ___value0;
		__this->set_generators_1(L_0);
		return;
	}
}
// System.String DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Zone_ToString_m136116DA43FF3C8E3CB7CA0E9517D84815B05EAF (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Zone_ToString_m136116DA43FF3C8E3CB7CA0E9517D84815B05EAF_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	String_t* G_B2_0 = NULL;
	String_t* G_B1_0 = NULL;
	int32_t G_B3_0 = 0;
	String_t* G_B3_1 = NULL;
	RuntimeObject * G_B5_0 = NULL;
	String_t* G_B5_1 = NULL;
	RuntimeObject * G_B4_0 = NULL;
	String_t* G_B4_1 = NULL;
	int32_t G_B6_0 = 0;
	RuntimeObject * G_B6_1 = NULL;
	String_t* G_B6_2 = NULL;
	{
		GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* L_0 = __this->get_generators_1();
		G_B1_0 = _stringLiteral609FA33CCE34CF9B337A270AFB44DC4BA1ED1C4F;
		if (!L_0)
		{
			G_B2_0 = _stringLiteral609FA33CCE34CF9B337A270AFB44DC4BA1ED1C4F;
			goto IL_0018;
		}
	}
	{
		GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* L_1 = __this->get_generators_1();
		NullCheck(L_1);
		G_B3_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))));
		G_B3_1 = G_B1_0;
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
	}

IL_0019:
	{
		int32_t L_2 = G_B3_0;
		RuntimeObject * L_3 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_2);
		ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* L_4 = __this->get_modulators_0();
		G_B4_0 = L_3;
		G_B4_1 = G_B3_1;
		if (!L_4)
		{
			G_B5_0 = L_3;
			G_B5_1 = G_B3_1;
			goto IL_0031;
		}
	}
	{
		ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* L_5 = __this->get_modulators_0();
		NullCheck(L_5);
		G_B6_0 = (((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length))));
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		goto IL_0032;
	}

IL_0031:
	{
		G_B6_0 = 0;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
	}

IL_0032:
	{
		int32_t L_6 = G_B6_0;
		RuntimeObject * L_7 = Box(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046_il2cpp_TypeInfo_var, &L_6);
		String_t* L_8 = String_Format_m8D1CB0410C35E052A53AE957C914C841E54BAB66(G_B6_2, G_B6_1, L_7, /*hidden argument*/NULL);
		V_0 = L_8;
		goto IL_0040;
	}

IL_0040:
	{
		String_t* L_9 = V_0;
		return L_9;
	}
}
// System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Zone__ctor_mFEE2F08A75809A1B25C605D48A2581E51922DF97 (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk::.ctor(System.String,System.Int32,System.IO.BinaryReader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ZoneChunk__ctor_m4A647A21B1CB9F0430922669AF13D0A66539C513 (ZoneChunk_t894317B0A9F9371149E84A5F5D3AC09B86524F5A * __this, String_t* ___id0, int32_t ___size1, BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * ___reader2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZoneChunk__ctor_m4A647A21B1CB9F0430922669AF13D0A66539C513_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * V_0 = NULL;
	int32_t V_1 = 0;
	RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * V_2 = NULL;
	bool V_3 = false;
	{
		String_t* L_0 = ___id0;
		int32_t L_1 = ___size1;
		Chunk__ctor_m09D15006E79DF3FA8C2E92747839186ECB2661A4(__this, L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = ___size1;
		V_3 = (bool)((((int32_t)((int32_t)((int32_t)L_2%(int32_t)4))) == ((int32_t)0))? 1 : 0);
		bool L_3 = V_3;
		if (L_3)
		{
			goto IL_001f;
		}
	}
	{
		Exception_t * L_4 = (Exception_t *)il2cpp_codegen_object_new(Exception_t_il2cpp_TypeInfo_var);
		Exception__ctor_m8ECDE8ACA7F2E0EF1144BD1200FB5DB2870B5F11(L_4, _stringLiteralA06B43CA30AE28998E1D4DD043AA4381F1EDC6E8, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4, ZoneChunk__ctor_m4A647A21B1CB9F0430922669AF13D0A66539C513_RuntimeMethod_var);
	}

IL_001f:
	{
		int32_t L_5 = ___size1;
		RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E* L_6 = (RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E*)(RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E*)SZArrayNew(RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)L_5/(int32_t)4)));
		__this->set_zoneData_2(L_6);
		V_0 = (RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 *)NULL;
		V_1 = 0;
		goto IL_0094;
	}

IL_0033:
	{
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_7 = (RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 *)il2cpp_codegen_object_new(RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682_il2cpp_TypeInfo_var);
		RawZoneData__ctor_mC6A341B8DBB243161F41FA43121208581B74605D(L_7, /*hidden argument*/NULL);
		V_2 = L_7;
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_8 = V_2;
		BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * L_9 = ___reader2;
		NullCheck(L_9);
		uint16_t L_10 = VirtFuncInvoker0< uint16_t >::Invoke(15 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_9);
		NullCheck(L_8);
		L_8->set_generatorIndex_0(L_10);
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_11 = V_2;
		BinaryReader_t4F45C15FF44F8E1C105704A21FFBE58D60015128 * L_12 = ___reader2;
		NullCheck(L_12);
		uint16_t L_13 = VirtFuncInvoker0< uint16_t >::Invoke(15 /* System.UInt16 System.IO.BinaryReader::ReadUInt16() */, L_12);
		NullCheck(L_11);
		L_11->set_modulatorIndex_1(L_13);
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_14 = V_0;
		V_3 = (bool)((((RuntimeObject*)(RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 *)L_14) == ((RuntimeObject*)(RuntimeObject *)NULL))? 1 : 0);
		bool L_15 = V_3;
		if (L_15)
		{
			goto IL_0084;
		}
	}
	{
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_16 = V_0;
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_17 = V_2;
		NullCheck(L_17);
		uint16_t L_18 = L_17->get_generatorIndex_0();
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_19 = V_0;
		NullCheck(L_19);
		uint16_t L_20 = L_19->get_generatorIndex_0();
		NullCheck(L_16);
		L_16->set_generatorCount_2((uint16_t)(((int32_t)((uint16_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_18, (int32_t)L_20))))));
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_21 = V_0;
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_22 = V_2;
		NullCheck(L_22);
		uint16_t L_23 = L_22->get_modulatorIndex_1();
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_24 = V_0;
		NullCheck(L_24);
		uint16_t L_25 = L_24->get_modulatorIndex_1();
		NullCheck(L_21);
		L_21->set_modulatorCount_3((uint16_t)(((int32_t)((uint16_t)((int32_t)il2cpp_codegen_subtract((int32_t)L_23, (int32_t)L_25))))));
	}

IL_0084:
	{
		RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E* L_26 = __this->get_zoneData_2();
		int32_t L_27 = V_1;
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_28 = V_2;
		NullCheck(L_26);
		ArrayElementTypeCheck (L_26, L_28);
		(L_26)->SetAt(static_cast<il2cpp_array_size_t>(L_27), (RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 *)L_28);
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_29 = V_2;
		V_0 = L_29;
		int32_t L_30 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_30, (int32_t)1));
	}

IL_0094:
	{
		int32_t L_31 = V_1;
		RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E* L_32 = __this->get_zoneData_2();
		NullCheck(L_32);
		V_3 = (bool)((((int32_t)L_31) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_32)->max_length))))))? 1 : 0);
		bool L_33 = V_3;
		if (L_33)
		{
			goto IL_0033;
		}
	}
	{
		return;
	}
}
// DaggerfallWorkshop.AudioSynthesis.Sf2.Zone[] DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk::ToZones(DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[],DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5* ZoneChunk_ToZones_m24160C8D66DBB5B4D8322607AE687113B21EB205 (ZoneChunk_t894317B0A9F9371149E84A5F5D3AC09B86524F5A * __this, ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* ___modulators0, GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* ___generators1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (ZoneChunk_ToZones_m24160C8D66DBB5B4D8322607AE687113B21EB205_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5* V_0 = NULL;
	int32_t V_1 = 0;
	RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * V_2 = NULL;
	Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * V_3 = NULL;
	ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5* V_4 = NULL;
	bool V_5 = false;
	{
		RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E* L_0 = __this->get_zoneData_2();
		NullCheck(L_0);
		ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5* L_1 = (ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5*)(ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5*)SZArrayNew(ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5_il2cpp_TypeInfo_var, (uint32_t)((int32_t)il2cpp_codegen_subtract((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_0)->max_length)))), (int32_t)1)));
		V_0 = L_1;
		V_1 = 0;
		goto IL_0086;
	}

IL_0015:
	{
		RawZoneDataU5BU5D_t82C38538896FA969D54377FB7985307E8577308E* L_2 = __this->get_zoneData_2();
		int32_t L_3 = V_1;
		NullCheck(L_2);
		int32_t L_4 = L_3;
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_5 = (L_2)->GetAt(static_cast<il2cpp_array_size_t>(L_4));
		V_2 = L_5;
		Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * L_6 = (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 *)il2cpp_codegen_object_new(Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54_il2cpp_TypeInfo_var);
		Zone__ctor_mFEE2F08A75809A1B25C605D48A2581E51922DF97(L_6, /*hidden argument*/NULL);
		V_3 = L_6;
		Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * L_7 = V_3;
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_8 = V_2;
		NullCheck(L_8);
		uint16_t L_9 = L_8->get_generatorCount_2();
		GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* L_10 = (GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE*)(GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE*)SZArrayNew(GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE_il2cpp_TypeInfo_var, (uint32_t)L_9);
		NullCheck(L_7);
		Zone_set_Generators_m051434F7E04B03839D9E45F687EF1D168E486E3D(L_7, L_10, /*hidden argument*/NULL);
		GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* L_11 = ___generators1;
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_12 = V_2;
		NullCheck(L_12);
		uint16_t L_13 = L_12->get_generatorIndex_0();
		Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * L_14 = V_3;
		NullCheck(L_14);
		GeneratorU5BU5D_t03B7915A4450BDFC544BBB3F51F28E9BE7EE44DE* L_15 = Zone_get_Generators_m3BDD4F7A064D556CD8FD224F4828417D82EAA019(L_14, /*hidden argument*/NULL);
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_16 = V_2;
		NullCheck(L_16);
		uint16_t L_17 = L_16->get_generatorCount_2();
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_11, L_13, (RuntimeArray *)(RuntimeArray *)L_15, 0, L_17, /*hidden argument*/NULL);
		Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * L_18 = V_3;
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_19 = V_2;
		NullCheck(L_19);
		uint16_t L_20 = L_19->get_modulatorCount_3();
		ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* L_21 = (ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7*)(ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7*)SZArrayNew(ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7_il2cpp_TypeInfo_var, (uint32_t)L_20);
		NullCheck(L_18);
		Zone_set_Modulators_mCD8A63184ED534C3F54CB8C231624B866588FD75(L_18, L_21, /*hidden argument*/NULL);
		ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* L_22 = ___modulators0;
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_23 = V_2;
		NullCheck(L_23);
		uint16_t L_24 = L_23->get_modulatorIndex_1();
		Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * L_25 = V_3;
		NullCheck(L_25);
		ModulatorU5BU5D_tE15643DB5D62CD5FB86CEA30B4F2CFCD2FE80AB7* L_26 = Zone_get_Modulators_m819BC5A415FDD098C62F176644BC7B832E9D65DF(L_25, /*hidden argument*/NULL);
		RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * L_27 = V_2;
		NullCheck(L_27);
		uint16_t L_28 = L_27->get_modulatorCount_3();
		Array_Copy_m3F127FFB5149532135043FFE285F9177C80CB877((RuntimeArray *)(RuntimeArray *)L_22, L_24, (RuntimeArray *)(RuntimeArray *)L_26, 0, L_28, /*hidden argument*/NULL);
		ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5* L_29 = V_0;
		int32_t L_30 = V_1;
		Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 * L_31 = V_3;
		NullCheck(L_29);
		ArrayElementTypeCheck (L_29, L_31);
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(L_30), (Zone_tB0459C36675BA85D42F949AC78F562E8CD97EE54 *)L_31);
		int32_t L_32 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_32, (int32_t)1));
	}

IL_0086:
	{
		int32_t L_33 = V_1;
		ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5* L_34 = V_0;
		NullCheck(L_34);
		V_5 = (bool)((((int32_t)L_33) < ((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_34)->max_length))))))? 1 : 0);
		bool L_35 = V_5;
		if (L_35)
		{
			goto IL_0015;
		}
	}
	{
		ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5* L_36 = V_0;
		V_4 = L_36;
		goto IL_0097;
	}

IL_0097:
	{
		ZoneU5BU5D_t3ED0843E0A2633BB5B49AE4A6F3798C2BCBD26E5* L_37 = V_4;
		return L_37;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope_EnvelopeStage::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void EnvelopeStage__ctor_m15449496B9957EF2C541D2448A55A51277EC840B (EnvelopeStage_t9331A322417D3601C393D2C1A091043DFAD8B0E3 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		__this->set_time_0(0);
		__this->set_graph_1((SingleU5BU5D_t47E8DBF5B597C122478D1FFBD9DD57399A0650FA*)NULL);
		__this->set_scale_2((0.0f));
		__this->set_offset_3((0.0f));
		__this->set_reverse_4((bool)0);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.InstrumentChunk_RawInstrument::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RawInstrument__ctor_mE9AF8C41617F181C318B00E034BD93698945A831 (RawInstrument_tE571AEC155C68A7AC59C381739A8EC772671ED30 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PatchInterval__ctor_m5A583D82AC90E9F5308FFFC9A0206981179EF39F (PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835 * __this, Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * ___patch0, uint8_t ___startChannel1, uint8_t ___endChannel2, uint8_t ___startKey3, uint8_t ___endKey4, uint8_t ___startVelocity5, uint8_t ___endVelocity6, const RuntimeMethod* method)
{
	{
		__this->set_patch_0((Patch_t3144A9674F069F684A9B015803D7A714F366C7AD *)NULL);
		__this->set_startChannel_1((uint8_t)0);
		__this->set_startKey_2((uint8_t)0);
		__this->set_startVelocity_3((uint8_t)0);
		__this->set_endChannel_4((uint8_t)((int32_t)15));
		__this->set_endKey_5((uint8_t)((int32_t)127));
		__this->set_endVelocity_6((uint8_t)((int32_t)127));
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_0 = ___patch0;
		__this->set_patch_0(L_0);
		uint8_t L_1 = ___startChannel1;
		__this->set_startChannel_1(L_1);
		uint8_t L_2 = ___endChannel2;
		__this->set_endChannel_4(L_2);
		uint8_t L_3 = ___startKey3;
		__this->set_startKey_2(L_3);
		uint8_t L_4 = ___endKey4;
		__this->set_endKey_5(L_4);
		uint8_t L_5 = ___startVelocity5;
		__this->set_startVelocity_3(L_5);
		uint8_t L_6 = ___endVelocity6;
		__this->set_endVelocity_6(L_6);
		return;
	}
}
// System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::CheckAllIntervals(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PatchInterval_CheckAllIntervals_m9D366399E10C1D68E62EAC9F60371C2C70FF5E94 (PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835 * __this, int32_t ___channel0, int32_t ___key1, int32_t ___velocity2, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B7_0 = 0;
	int32_t G_B9_0 = 0;
	{
		int32_t L_0 = ___channel0;
		uint8_t L_1 = __this->get_startChannel_1();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_2 = ___channel0;
		uint8_t L_3 = __this->get_endChannel_4();
		if ((((int32_t)L_2) > ((int32_t)L_3)))
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_4 = ___key1;
		uint8_t L_5 = __this->get_startKey_2();
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_6 = ___key1;
		uint8_t L_7 = __this->get_endKey_5();
		if ((((int32_t)L_6) > ((int32_t)L_7)))
		{
			goto IL_0040;
		}
	}
	{
		int32_t L_8 = ___velocity2;
		uint8_t L_9 = __this->get_startVelocity_3();
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_003c;
		}
	}
	{
		int32_t L_10 = ___velocity2;
		uint8_t L_11 = __this->get_endVelocity_6();
		G_B7_0 = ((((int32_t)((((int32_t)L_10) > ((int32_t)L_11))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_003d;
	}

IL_003c:
	{
		G_B7_0 = 0;
	}

IL_003d:
	{
		G_B9_0 = G_B7_0;
		goto IL_0041;
	}

IL_0040:
	{
		G_B9_0 = 0;
	}

IL_0041:
	{
		V_0 = (bool)G_B9_0;
		goto IL_0045;
	}

IL_0045:
	{
		bool L_12 = V_0;
		return L_12;
	}
}
// System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::CheckChannelAndKey(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PatchInterval_CheckChannelAndKey_mACC1DB3C02A06917029FEB385B68112D0BC507F0 (PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835 * __this, int32_t ___channel0, int32_t ___key1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = ___channel0;
		uint8_t L_1 = __this->get_startChannel_1();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = ___channel0;
		uint8_t L_3 = __this->get_endChannel_4();
		if ((((int32_t)L_2) > ((int32_t)L_3)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_4 = ___key1;
		uint8_t L_5 = __this->get_startKey_2();
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = ___key1;
		uint8_t L_7 = __this->get_endKey_5();
		G_B5_0 = ((((int32_t)((((int32_t)L_6) > ((int32_t)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B5_0 = 0;
	}

IL_002b:
	{
		G_B7_0 = G_B5_0;
		goto IL_002f;
	}

IL_002e:
	{
		G_B7_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B7_0;
		goto IL_0033;
	}

IL_0033:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::CheckKeyAndVelocity(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PatchInterval_CheckKeyAndVelocity_m86E567491485B761A62EF162FE12B924FCF28086 (PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835 * __this, int32_t ___key0, int32_t ___velocity1, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B5_0 = 0;
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = ___key0;
		uint8_t L_1 = __this->get_startKey_2();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_2 = ___key0;
		uint8_t L_3 = __this->get_endKey_5();
		if ((((int32_t)L_2) > ((int32_t)L_3)))
		{
			goto IL_002e;
		}
	}
	{
		int32_t L_4 = ___velocity1;
		uint8_t L_5 = __this->get_startVelocity_3();
		if ((((int32_t)L_4) < ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = ___velocity1;
		uint8_t L_7 = __this->get_endVelocity_6();
		G_B5_0 = ((((int32_t)((((int32_t)L_6) > ((int32_t)L_7))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B5_0 = 0;
	}

IL_002b:
	{
		G_B7_0 = G_B5_0;
		goto IL_002f;
	}

IL_002e:
	{
		G_B7_0 = 0;
	}

IL_002f:
	{
		V_0 = (bool)G_B7_0;
		goto IL_0033;
	}

IL_0033:
	{
		bool L_8 = V_0;
		return L_8;
	}
}
// System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::CheckKey(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PatchInterval_CheckKey_mC0CFD05F2651654CE790B017FDA5294086F2749C (PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835 * __this, int32_t ___key0, const RuntimeMethod* method)
{
	bool V_0 = false;
	int32_t G_B3_0 = 0;
	{
		int32_t L_0 = ___key0;
		uint8_t L_1 = __this->get_startKey_2();
		if ((((int32_t)L_0) < ((int32_t)L_1)))
		{
			goto IL_0018;
		}
	}
	{
		int32_t L_2 = ___key0;
		uint8_t L_3 = __this->get_endKey_5();
		G_B3_0 = ((((int32_t)((((int32_t)L_2) > ((int32_t)L_3))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		goto IL_0019;
	}

IL_0018:
	{
		G_B3_0 = 0;
	}

IL_0019:
	{
		V_0 = (bool)G_B3_0;
		goto IL_001d;
	}

IL_001d:
	{
		bool L_4 = V_0;
		return L_4;
	}
}
// System.String DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PatchInterval_ToString_m9E72F8C7309019A9B214AC620B39E94B8F5EAF9D (PatchInterval_t354CA4767B4F4B1F339F63786F916A20DEA25835 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PatchInterval_ToString_m9E72F8C7309019A9B214AC620B39E94B8F5EAF9D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* V_1 = NULL;
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_0 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)7);
		V_1 = L_0;
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_1 = V_1;
		Patch_t3144A9674F069F684A9B015803D7A714F366C7AD * L_2 = __this->get_patch_0();
		NullCheck(L_1);
		ArrayElementTypeCheck (L_1, L_2);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_2);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_3 = V_1;
		uint8_t L_4 = __this->get_startChannel_1();
		uint8_t L_5 = L_4;
		RuntimeObject * L_6 = Box(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(1), (RuntimeObject *)L_6);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_7 = V_1;
		uint8_t L_8 = __this->get_endChannel_4();
		uint8_t L_9 = L_8;
		RuntimeObject * L_10 = Box(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_7);
		ArrayElementTypeCheck (L_7, L_10);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (RuntimeObject *)L_10);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_11 = V_1;
		uint8_t L_12 = __this->get_startKey_2();
		uint8_t L_13 = L_12;
		RuntimeObject * L_14 = Box(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(3), (RuntimeObject *)L_14);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_15 = V_1;
		uint8_t L_16 = __this->get_endKey_5();
		uint8_t L_17 = L_16;
		RuntimeObject * L_18 = Box(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var, &L_17);
		NullCheck(L_15);
		ArrayElementTypeCheck (L_15, L_18);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(4), (RuntimeObject *)L_18);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_19 = V_1;
		uint8_t L_20 = __this->get_startVelocity_3();
		uint8_t L_21 = L_20;
		RuntimeObject * L_22 = Box(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var, &L_21);
		NullCheck(L_19);
		ArrayElementTypeCheck (L_19, L_22);
		(L_19)->SetAt(static_cast<il2cpp_array_size_t>(5), (RuntimeObject *)L_22);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_23 = V_1;
		uint8_t L_24 = __this->get_endVelocity_6();
		uint8_t L_25 = L_24;
		RuntimeObject * L_26 = Box(Byte_t0111FAB8B8685667EDDAF77683F0D8F86B659056_il2cpp_TypeInfo_var, &L_25);
		NullCheck(L_23);
		ArrayElementTypeCheck (L_23, L_26);
		(L_23)->SetAt(static_cast<il2cpp_array_size_t>(6), (RuntimeObject *)L_26);
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_27 = V_1;
		String_t* L_28 = String_Format_mCED6767EA5FEE6F15ABCD5B4F9150D1284C2795B(_stringLiteralDFFFD27E2BB06614153E94D05DE45D16CB3C4D04, L_27, /*hidden argument*/NULL);
		V_0 = L_28;
		goto IL_0073;
	}

IL_0073:
	{
		String_t* L_29 = V_0;
		return L_29;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk_RawPreset::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RawPreset__ctor_mB2ED8A0F79A1CC0B9A6038176BFB6FEF4494E7E4 (RawPreset_tA6795EFE241E18A1A886023CF5E17AE4ADF6A309 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void VoiceNode__ctor_mC30870DD5AAC5E8AC4D00ED2ADD82BD739D35BE1 (VoiceNode_tBAD19A26CB7CA75B51911810598025C065EB770E * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk_RawZoneData::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RawZoneData__ctor_mC6A341B8DBB243161F41FA43121208581B74605D (RawZoneData_tBCE40761242A5779B8D2E317F523263631D13682 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t Queue_1_get_Count_m6331507132D178B8517E38C3F2F0C1698AEC49D1_gshared_inline (Queue_1_tFA858DFE2FA06D7935F260D8FA45E56996525965 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get__size_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * LinkedList_1_get_First_m572476A3C4A5CF4A2EBBDF50A76FF35493337871_gshared_inline (LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * __this, const RuntimeMethod* method)
{
	{
		LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * L_0 = (LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F *)__this->get_head_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * LinkedListNode_1_get_Value_m9A641BD5F0614D0060E19C703E9A756391DF7A32_gshared_inline (LinkedListNode_1_tE9D663EA27667E3E9F8E311FC56B32F3E3D9B07F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = (RuntimeObject *)__this->get_item_3();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR int32_t LinkedList_1_get_Count_m5AD2F760211A61C7FFD9BE298CFA10F1BF4BE54A_gshared_inline (LinkedList_1_tF98410EEA26883FF7ECC1DFD10068A4CC744550B * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_count_1();
		return L_0;
	}
}
