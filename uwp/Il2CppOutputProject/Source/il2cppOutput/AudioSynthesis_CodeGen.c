﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Collections.Generic.List`1<DaggerfallWorkshop.AudioSynthesis.Bank.PatchAsset> DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager::get_PatchAssetList()
extern void AssetManager_get_PatchAssetList_m4919F86B2396C783FA88855454D70940882D7CFD (void);
// 0x00000002 System.Collections.Generic.List`1<DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset> DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager::get_SampleAssetList()
extern void AssetManager_get_SampleAssetList_m5C1C19CD8C2FC38258837789DC69A010AB1AA5B0 (void);
// 0x00000003 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager::.ctor()
extern void AssetManager__ctor_m3DE927F316B8ABF92AB42FF6E5B7EB5EF265455B (void);
// 0x00000004 DaggerfallWorkshop.AudioSynthesis.Bank.PatchAsset DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager::FindPatch(System.String)
extern void AssetManager_FindPatch_mC3680C40A08DF1F75C08EF60F5CA98EA5AD52276 (void);
// 0x00000005 DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager::FindSample(System.String)
extern void AssetManager_FindSample_mA6AF930A3D579071BFAE104F0375D5C203C6E094 (void);
// 0x00000006 System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::get_Value()
extern void Envelope_get_Value_mFDADAC3A6E03234E53E59700435377B263B983CD (void);
// 0x00000007 DaggerfallWorkshop.AudioSynthesis.Bank.Components.EnvelopeStateEnum DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::get_CurrentState()
extern void Envelope_get_CurrentState_mFC9436FB9F34981B22FC85BB1E283C1805B6CBCC (void);
// 0x00000008 System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::get_Depth()
extern void Envelope_get_Depth_mDA2A2B8691ADCCB730E03126EC0EA46034C86413 (void);
// 0x00000009 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::set_Depth(System.Single)
extern void Envelope_set_Depth_m1B8EE21D4652A1C613777537AEDFBE1DE9C6DE6F (void);
// 0x0000000A System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::.ctor()
extern void Envelope__ctor_mC790D05C10CDD1066A2347F220978EC7C3D812CA (void);
// 0x0000000B System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::QuickSetup(System.Int32,System.Single,DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.EnvelopeDescriptor)
extern void Envelope_QuickSetup_mF7A54B3456A2497291870E46DCDF7BA9B51905D6 (void);
// 0x0000000C System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::QuickSetupSf2(System.Int32,System.Int32,System.Int16,System.Int16,System.Boolean,DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.EnvelopeDescriptor)
extern void Envelope_QuickSetupSf2_mB89B546A9E220C2625D3641F2F2A63D10A12C888 (void);
// 0x0000000D System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::Increment(System.Int32)
extern void Envelope_Increment_m5F0B17426E7218A1693E40085D66747442D18965 (void);
// 0x0000000E System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::Release(System.Single)
extern void Envelope_Release_mBC0AB141572D1D9E71FBB12AB17DBFDDC9C8D232 (void);
// 0x0000000F System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::ReleaseSf2VolumeEnvelope()
extern void Envelope_ReleaseSf2VolumeEnvelope_mE59B635170F2A4B898DAC1D29D7D569E4EE476A5 (void);
// 0x00000010 System.String DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope::ToString()
extern void Envelope_ToString_m013A148181B6E3E19C0F7E1061DC8B4C80887F7E (void);
// 0x00000011 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Envelope_EnvelopeStage::.ctor()
extern void EnvelopeStage__ctor_m15449496B9957EF2C541D2448A55A51277EC840B (void);
// 0x00000012 System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::get_Cutoff()
extern void Filter_get_Cutoff_m56BFBCD4F5E209A0E00B73673A54F08CD92C5DC7 (void);
// 0x00000013 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::set_Cutoff(System.Double)
extern void Filter_set_Cutoff_m22F345AF3C647CFB9292830BA5811A97E57C7A70 (void);
// 0x00000014 System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::get_CoeffNeedsUpdating()
extern void Filter_get_CoeffNeedsUpdating_mC873AFEC9997670D31C82FF76D4F61D7CCF9C109 (void);
// 0x00000015 System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::get_Enabled()
extern void Filter_get_Enabled_mAC11045571C09BE64B3E753986A10E549C27619A (void);
// 0x00000016 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::Disable()
extern void Filter_Disable_m0C7618D592A126BA98DCA21C047F442B72E57100 (void);
// 0x00000017 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::QuickSetup(System.Int32,System.Int32,System.Single,DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.FilterDescriptor)
extern void Filter_QuickSetup_m24E1AC84855F774E1069551DF910ADEF7AD4E559 (void);
// 0x00000018 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::ApplyFilter(System.Single[])
extern void Filter_ApplyFilter_m8C8ADE4458BEAEB77B52E46F751A09D559BC6E3E (void);
// 0x00000019 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::ApplyFilterInterp(System.Single[],System.Int32)
extern void Filter_ApplyFilterInterp_mE961F8378D33EA0D7349F9107C0D919FC3402BF2 (void);
// 0x0000001A System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::UpdateCoeff(System.Int32)
extern void Filter_UpdateCoeff_m87AD063C42B19160403E7AA421741F4EA29DBAA3 (void);
// 0x0000001B System.String DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::ToString()
extern void Filter_ToString_mE1343E4A0D405094E1C81DBAF4B35D27F74079FF (void);
// 0x0000001C System.Single[] DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::GenerateFilterCoeff(System.Double,System.Double)
extern void Filter_GenerateFilterCoeff_m93B77FD2247A74319F4BEBD8FD77A2F6DB18A186 (void);
// 0x0000001D System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Filter::.ctor()
extern void Filter__ctor_mF15108D7CC9AC5426630CDBB7A7BB87479FF0574 (void);
// 0x0000001E DaggerfallWorkshop.AudioSynthesis.Bank.Components.LoopModeEnum DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_LoopMode()
extern void Generator_get_LoopMode_m1F777968BE450DE19B3315C8C8F409BBB7AEBE45 (void);
// 0x0000001F System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_LoopMode(DaggerfallWorkshop.AudioSynthesis.Bank.Components.LoopModeEnum)
extern void Generator_set_LoopMode_m47B37F1C21E8142C8CDD4CB3C4DB9754EE5BE2EA (void);
// 0x00000020 System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_LoopStartPhase()
extern void Generator_get_LoopStartPhase_mBB8BED1D86068FF786052A45C5A4E3755F82C915 (void);
// 0x00000021 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_LoopStartPhase(System.Double)
extern void Generator_set_LoopStartPhase_m7500CCF2406B2308DEF55058CBE9B0DDD6DD3CAD (void);
// 0x00000022 System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_LoopEndPhase()
extern void Generator_get_LoopEndPhase_mD59DC0FA802D7E65936742193516DE463F93C25A (void);
// 0x00000023 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_LoopEndPhase(System.Double)
extern void Generator_set_LoopEndPhase_m104CD96C976F8D8CEA704638EB877FE4985E81EC (void);
// 0x00000024 System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_StartPhase()
extern void Generator_get_StartPhase_mD65594668CBD23FEC029335FE82CF89702F981DE (void);
// 0x00000025 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_StartPhase(System.Double)
extern void Generator_set_StartPhase_m73FD4319FC28DA3B907505A5CC8C600493529766 (void);
// 0x00000026 System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_EndPhase()
extern void Generator_get_EndPhase_mEF0F456A45FD13C55E9B668131ACDC69A250FDE1 (void);
// 0x00000027 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_EndPhase(System.Double)
extern void Generator_set_EndPhase_mE998A81E855DF8AC96190E65C0C8E7B94FE18C03 (void);
// 0x00000028 System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_Offset()
extern void Generator_get_Offset_m0FB3FE9E48E325DF9C6C4D384061229661D77138 (void);
// 0x00000029 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_Offset(System.Double)
extern void Generator_set_Offset_mFECCA21556A098DED1274F59939A40177C657046 (void);
// 0x0000002A System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_Period()
extern void Generator_get_Period_m5FBC957BC65D782563D188AEDFE85BDE1E805748 (void);
// 0x0000002B System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_Period(System.Double)
extern void Generator_set_Period_m0DEFCF018BB1D21BA17CFF12A38789C7CDDE5816 (void);
// 0x0000002C System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_Frequency()
extern void Generator_get_Frequency_m561992C435661C92ED7CF77BE0C7F996112028FF (void);
// 0x0000002D System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_Frequency(System.Double)
extern void Generator_set_Frequency_m44B71B48AC001D46D7A086E133BA5C32922849E0 (void);
// 0x0000002E System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_RootKey()
extern void Generator_get_RootKey_m2807E9537D24D9141C63EA102AC5A48C35609192 (void);
// 0x0000002F System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_RootKey(System.Int16)
extern void Generator_set_RootKey_m0B3712DB717113A5EF57B67CECBD50D84094A750 (void);
// 0x00000030 System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_KeyTrack()
extern void Generator_get_KeyTrack_mDD560FC0453D03611853AB7C482CEF29AC8F6B35 (void);
// 0x00000031 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_KeyTrack(System.Int16)
extern void Generator_set_KeyTrack_m418D6D0DBC29C8CDC187F2F967D41725CFF862EB (void);
// 0x00000032 System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_VelocityTrack()
extern void Generator_get_VelocityTrack_m6CA88D079501105FE8BB166845F29FCF6C0DADA8 (void);
// 0x00000033 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_VelocityTrack(System.Int16)
extern void Generator_set_VelocityTrack_m20826E907C686C42919C83E333DF3DF1D8EF8A4C (void);
// 0x00000034 System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::get_Tune()
extern void Generator_get_Tune_m11A9F8968AEFDC9A731AE3C07857C8AA5FA0733C (void);
// 0x00000035 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::set_Tune(System.Int16)
extern void Generator_set_Tune_m92A8590A8AF8D40C16C657C5B492E20DF58A14E7 (void);
// 0x00000036 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor)
extern void Generator__ctor_mBD9A3E5A89476277268E8BEBC0F17E5591A903A8 (void);
// 0x00000037 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::Release(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters)
extern void Generator_Release_mA1D24E06999AA0EFE658547005073356AF6AA904 (void);
// 0x00000038 System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::GetValue(System.Double)
// 0x00000039 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::GetValues(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters,System.Single[],System.Double)
// 0x0000003A System.String DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::ToString()
extern void Generator_ToString_mA4C7588F1531CE8887C9DF761FCB977D88BBFA64 (void);
// 0x0000003B System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator::.cctor()
extern void Generator__cctor_mAEDA590698CF25590027ECACB3089BE0FEBB3278 (void);
// 0x0000003C System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters::QuickSetup(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator)
extern void GeneratorParameters_QuickSetup_m9AA4510E07A2A9F45EDBE27399364287E0CC6B6B (void);
// 0x0000003D System.String DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters::ToString()
extern void GeneratorParameters_ToString_m6ADC2698C34BFB7E7075C9528988FE237C579635 (void);
// 0x0000003E System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters::.ctor()
extern void GeneratorParameters__ctor_m3236F25398ABFBF56DC2671F38892A2E11339D2A (void);
// 0x0000003F System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SampleGenerator::set_Samples(DaggerfallWorkshop.AudioSynthesis.Wave.PcmData)
extern void SampleGenerator_set_Samples_m33E24F2CDB5E6377F65397F4D3158C5450DB6CA3 (void);
// 0x00000040 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SampleGenerator::.ctor()
extern void SampleGenerator__ctor_mC08D6D832885D13AC213CA5EE6211C57BD226DFB (void);
// 0x00000041 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SampleGenerator::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor,DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager)
extern void SampleGenerator__ctor_m40133755356EAEE1AEBE92F470820DD8CB2FFFD4 (void);
// 0x00000042 System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SampleGenerator::GetValue(System.Double)
extern void SampleGenerator_GetValue_mCD2E584E108E5B8871E25396BFAB77261B05196B (void);
// 0x00000043 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SampleGenerator::GetValues(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters,System.Single[],System.Double)
extern void SampleGenerator_GetValues_m41555268A0D83F7E000AFA6D9BD00AF13A198EE4 (void);
// 0x00000044 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SampleGenerator::Interpolate(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters,System.Single[],System.Double,System.Int32,System.Int32)
extern void SampleGenerator_Interpolate_mF1949CB6F648886527630E49C380224504DD5CDC (void);
// 0x00000045 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SawGenerator::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor)
extern void SawGenerator__ctor_m2787DDB2CDC0C2913D522FB32F6D6EEE43FE1D75 (void);
// 0x00000046 System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SawGenerator::GetValue(System.Double)
extern void SawGenerator_GetValue_mECBED93E5F92207F3242CB60D9A89DB05F440D1C (void);
// 0x00000047 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SawGenerator::GetValues(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters,System.Single[],System.Double)
extern void SawGenerator_GetValues_m33F509704941B8E106F7BF690C9A23ACF9CCB2E8 (void);
// 0x00000048 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SineGenerator::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor)
extern void SineGenerator__ctor_m16939292F31A4C269D20D0F551CFABDFC1B17AA4 (void);
// 0x00000049 System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SineGenerator::GetValue(System.Double)
extern void SineGenerator_GetValue_m008504840217B0D3F6104E682DD4A1E7681E9EA1 (void);
// 0x0000004A System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SineGenerator::GetValues(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters,System.Single[],System.Double)
extern void SineGenerator_GetValues_mE768ABA0E21C2389BE341CD0758C48F58C4022B9 (void);
// 0x0000004B System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SquareGenerator::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor)
extern void SquareGenerator__ctor_mA92ED26FD608B9BCB0ABB3C7301956A490E75761 (void);
// 0x0000004C System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SquareGenerator::GetValue(System.Double)
extern void SquareGenerator_GetValue_mC0B58C49002D6C70197F1441E663A26ADB156F5D (void);
// 0x0000004D System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.SquareGenerator::GetValues(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters,System.Single[],System.Double)
extern void SquareGenerator_GetValues_m917D82D8B7EC0FC6DCFB2100C07EC85602F63081 (void);
// 0x0000004E System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.TriangleGenerator::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor)
extern void TriangleGenerator__ctor_m1922B9805C5F5B03370052B44309CC04E2C62743 (void);
// 0x0000004F System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.TriangleGenerator::GetValue(System.Double)
extern void TriangleGenerator_GetValue_mAE8FFCF40053D2C226AA07F9091268DD787BCCAB (void);
// 0x00000050 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.TriangleGenerator::GetValues(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters,System.Single[],System.Double)
extern void TriangleGenerator_GetValues_mD60A13DC9FC0A17E9E83426BD83E3047A15E6470 (void);
// 0x00000051 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.WhiteNoiseGenerator::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor)
extern void WhiteNoiseGenerator__ctor_mFA126753A3BAA805CA77B1B93A962EAF9B61A8D3 (void);
// 0x00000052 System.Single DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.WhiteNoiseGenerator::GetValue(System.Double)
extern void WhiteNoiseGenerator_GetValue_m444D451C4A8B7B12460EF17BCC2E6FABC9C1B49C (void);
// 0x00000053 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.WhiteNoiseGenerator::GetValues(DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.GeneratorParameters,System.Single[],System.Double)
extern void WhiteNoiseGenerator_GetValues_m7ABC9C1794FADE89766A90495F6B75598FBD9316 (void);
// 0x00000054 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.WhiteNoiseGenerator::.cctor()
extern void WhiteNoiseGenerator__cctor_m2692BF76CDB9FA3654951A44D17441BC49B11655 (void);
// 0x00000055 System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::get_Value()
extern void Lfo_get_Value_mBBCE09F004227DB330042D9335E6861B05C9CC73 (void);
// 0x00000056 System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::get_Depth()
extern void Lfo_get_Depth_m07C75821EC6E85EE398C032E3008C7B582F00ACB (void);
// 0x00000057 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::set_Depth(System.Double)
extern void Lfo_set_Depth_m98031B7CFB0D73F8AA70523A27E1DE80AA5186F7 (void);
// 0x00000058 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::QuickSetup(System.Int32,DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.LfoDescriptor)
extern void Lfo_QuickSetup_mACA8407FBB32CF6906A2A9652939865D6FB0A057 (void);
// 0x00000059 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::Increment(System.Int32)
extern void Lfo_Increment_m674CDC38AE1908079ABF66375EAB9E6D2608BE9D (void);
// 0x0000005A System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::Reset()
extern void Lfo_Reset_m8D62D36CFD3DE0E3A8ABC987B7C15A9563546C2A (void);
// 0x0000005B System.String DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::ToString()
extern void Lfo_ToString_mBBF8E1BDF3C31F469FAA67F1F8327B4B44D88632 (void);
// 0x0000005C System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Components.Lfo::.ctor()
extern void Lfo__ctor_m35CD7DD1CD4D7AA08749FC169FB112E7D95A1D10 (void);
// 0x0000005D System.Void DaggerfallWorkshop.AudioSynthesis.Bank.DescriptorList::.ctor(System.IO.BinaryReader)
extern void DescriptorList__ctor_mBBA43A85CFF244D7F11081AE11709B1A31281AD7 (void);
// 0x0000005E DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.CustomDescriptor DaggerfallWorkshop.AudioSynthesis.Bank.DescriptorList::FindCustomDescriptor(System.String)
extern void DescriptorList_FindCustomDescriptor_m8BF0CB4B7776664ECBBD70701171D2E976474B50 (void);
// 0x0000005F System.String DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.CustomDescriptor::get_ID()
extern void CustomDescriptor_get_ID_m33EA8FA29176F78286A3CB0DF253E8FB99B941D6 (void);
// 0x00000060 System.Object[] DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.CustomDescriptor::get_Objects()
extern void CustomDescriptor_get_Objects_mDBD4CC738BEC0F613D8CA4DBD41C823A1B228914 (void);
// 0x00000061 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.CustomDescriptor::.ctor(System.String,System.Int32)
extern void CustomDescriptor__ctor_mEE5D3AF3DA3107D0D0E6CEDDF1ED43822DBCEFD4 (void);
// 0x00000062 System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.CustomDescriptor::Read(System.IO.BinaryReader)
extern void CustomDescriptor_Read_m618B76D4CC5E1DF448C536542CE46BD3B74835AE (void);
// 0x00000063 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.EnvelopeDescriptor::.ctor()
extern void EnvelopeDescriptor__ctor_m4686B23FE89756116F2F9ACE0A162DE10DE157A3 (void);
// 0x00000064 System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.EnvelopeDescriptor::Read(System.IO.BinaryReader)
extern void EnvelopeDescriptor_Read_mCCDEBFD87DA449E81749FBBDF61E67EDB9354295 (void);
// 0x00000065 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.EnvelopeDescriptor::ApplyDefault()
extern void EnvelopeDescriptor_ApplyDefault_m28D8929F39B7F0C4E07E56DF47147B1D6564295B (void);
// 0x00000066 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.EnvelopeDescriptor::CheckValidParameters()
extern void EnvelopeDescriptor_CheckValidParameters_m61C0E3FDB1F9DEEB35949D53D7C93B582FD5A995 (void);
// 0x00000067 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.FilterDescriptor::.ctor()
extern void FilterDescriptor__ctor_m0888273C225EC37D44DFBE0D561E64EFA622B1E5 (void);
// 0x00000068 System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.FilterDescriptor::Read(System.IO.BinaryReader)
extern void FilterDescriptor_Read_mA7C076B909910AF7D67CCC40426A1355387773EC (void);
// 0x00000069 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.FilterDescriptor::ApplyDefault()
extern void FilterDescriptor_ApplyDefault_m81F58AED83CCB64AB149510BD224D434712A30BD (void);
// 0x0000006A System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.FilterDescriptor::CheckValidParameters()
extern void FilterDescriptor_CheckValidParameters_mAD0E570DA02D12B80E455C6FAB260088271D0426 (void);
// 0x0000006B System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::.ctor()
extern void GeneratorDescriptor__ctor_mF6C0A30B3C6BCF94813054257D6A8CD89A0D2263 (void);
// 0x0000006C System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::Read(System.IO.BinaryReader)
extern void GeneratorDescriptor_Read_m2974F881034D62C83496FE386359641B4F650570 (void);
// 0x0000006D DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::ToGenerator(DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager)
extern void GeneratorDescriptor_ToGenerator_mF93D7C0CF1BA52ACDB92014ED269AF14D32F313D (void);
// 0x0000006E System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.GeneratorDescriptor::ApplyDefault()
extern void GeneratorDescriptor_ApplyDefault_m6126E2B7BC3385D700A99109AD04B0C711E41844 (void);
// 0x0000006F System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.LfoDescriptor::.ctor()
extern void LfoDescriptor__ctor_mB27DAC07B6F12A9B84F1AE6147F682E669206A61 (void);
// 0x00000070 System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.LfoDescriptor::Read(System.IO.BinaryReader)
extern void LfoDescriptor_Read_m3F7DCE22A9C534E9BB3D0D3C64647A014E48D169 (void);
// 0x00000071 DaggerfallWorkshop.AudioSynthesis.Bank.Components.Generators.Generator DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.LfoDescriptor::GetGenerator(DaggerfallWorkshop.AudioSynthesis.Bank.Components.WaveformEnum)
extern void LfoDescriptor_GetGenerator_m6396A58AF13C080B7AF06BDEE77E3F112DA1990C (void);
// 0x00000072 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.LfoDescriptor::ApplyDefault()
extern void LfoDescriptor_ApplyDefault_m31D4D48F19A2500183B5539CAFEECB7A6696A03A (void);
// 0x00000073 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Descriptors.LfoDescriptor::CheckValidParameters()
extern void LfoDescriptor_CheckValidParameters_m53325290FDE98F62B54A2D2C64A3FAEA90ECF007 (void);
// 0x00000074 System.String DaggerfallWorkshop.AudioSynthesis.Bank.PatchAsset::get_Name()
extern void PatchAsset_get_Name_m59FB69332A701034D8DD1B29E0B550C324EF45F2 (void);
// 0x00000075 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch DaggerfallWorkshop.AudioSynthesis.Bank.PatchAsset::get_Patch()
extern void PatchAsset_get_Patch_mDB2CC40B52B51FBCFF4D52DC708C5EA4806999EC (void);
// 0x00000076 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.PatchAsset::.ctor(System.String,DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch)
extern void PatchAsset__ctor_mF2483F4EB0D7C259E455A5F8107763F7B297DAD7 (void);
// 0x00000077 System.String DaggerfallWorkshop.AudioSynthesis.Bank.PatchAsset::ToString()
extern void PatchAsset_ToString_m3FF553301638B3115E303E35DA16DC9018789F0E (void);
// 0x00000078 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::.cctor()
extern void PatchBank__cctor_m6096DF18F4605E74C3159110FCCA29EC6836C0F4 (void);
// 0x00000079 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::ClearCustomPatchTypes()
extern void PatchBank_ClearCustomPatchTypes_mCCC7BC60B8F74A05D2D803029FC010BD4F3D1280 (void);
// 0x0000007A System.Void DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::.ctor(DaggerfallWorkshop.AudioSynthesis.IResource)
extern void PatchBank__ctor_mE5CC8AFFF8E92DD3C8037AF1BC76B76C8FDF4E0B (void);
// 0x0000007B System.Void DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::LoadBank(DaggerfallWorkshop.AudioSynthesis.IResource)
extern void PatchBank_LoadBank_mB32C6DA5FD41B5EABFECEA743A05934D5B2CA275 (void);
// 0x0000007C DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::GetPatch(System.Int32,System.Int32)
extern void PatchBank_GetPatch_mC86BDB6F5B4657124EE0CD4474172176A4C38FDF (void);
// 0x0000007D System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::IsBankLoaded(System.Int32)
extern void PatchBank_IsBankLoaded_m8C02279495AA8D73EDA721748226D7C4058E188E (void);
// 0x0000007E System.Void DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::LoadMyBank(System.IO.Stream)
extern void PatchBank_LoadMyBank_m521A460A87D5DDC235D0B007CA3563834D155C5F (void);
// 0x0000007F System.Void DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::LoadSf2(System.IO.Stream)
extern void PatchBank_LoadSf2_m0B987D7D80123E5ABB1B81F07033184D5F535D9F (void);
// 0x00000080 DaggerfallWorkshop.AudioSynthesis.Sf2.Sf2Region[][] DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::ReadSf2Instruments(DaggerfallWorkshop.AudioSynthesis.Sf2.Instrument[])
extern void PatchBank_ReadSf2Instruments_m479EFBF4F8A35829AE11D2CE1799E36BB12D4DBD (void);
// 0x00000081 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::ReadSf2Region(DaggerfallWorkshop.AudioSynthesis.Sf2.Sf2Region,DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[],DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[],System.Boolean)
extern void PatchBank_ReadSf2Region_m3C3B6775F01EAD0AC85A5A17F86136B20FB8BB0B (void);
// 0x00000082 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank::AssignPatchToBank(DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch,System.Int32,System.Int32,System.Int32)
extern void PatchBank_AssignPatchToBank_mF647B2713BC87ED1CB54D5D577B92216913006DB (void);
// 0x00000083 System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::get_ExclusiveGroupTarget()
extern void Patch_get_ExclusiveGroupTarget_mC9F49AFFCEF49C1A5A40D5D87D7A9ABF89FC1AE9 (void);
// 0x00000084 System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::get_ExclusiveGroup()
extern void Patch_get_ExclusiveGroup_m2E5B9F814EB6C23F1464473925B9DA97EC6529F6 (void);
// 0x00000085 System.String DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::get_Name()
extern void Patch_get_Name_m2536C44C9116E695C9BBE81DD5FF0A1A11DDC3B9 (void);
// 0x00000086 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::.ctor(System.String)
extern void Patch__ctor_mB4BDDD7E1B0F2F9E0E53615FAD31C03457C44968 (void);
// 0x00000087 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::Process(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters,System.Int32,System.Int32)
// 0x00000088 System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::Start(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
// 0x00000089 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::Stop(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
// 0x0000008A System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::Load(DaggerfallWorkshop.AudioSynthesis.Bank.DescriptorList,DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager)
// 0x0000008B System.String DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch::ToString()
extern void Patch_ToString_m3966C5B5B18EABFA7D0FE8B37D382514AEE4BA3E (void);
// 0x0000008C System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.BasicPatch::Start(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
extern void BasicPatch_Start_m457B8CF1EF2F76C2CD57BB8039D4312657C41697 (void);
// 0x0000008D System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.BasicPatch::Stop(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
extern void BasicPatch_Stop_mA550D006184AC8A9F70245FDEA3349BEAFA1F45C (void);
// 0x0000008E System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.BasicPatch::Process(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters,System.Int32,System.Int32)
extern void BasicPatch_Process_mB22892089664D3B1FB462DF6915683B9E35BADE8 (void);
// 0x0000008F System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.BasicPatch::Load(DaggerfallWorkshop.AudioSynthesis.Bank.DescriptorList,DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager)
extern void BasicPatch_Load_m49E7D82991C29B6AADE6260FEF9C17F7BC17AF71 (void);
// 0x00000090 System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Fm2Patch::Start(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
extern void Fm2Patch_Start_mF3C4B8394BBB6C1E317B94DED0D353BA94EF6CF6 (void);
// 0x00000091 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Fm2Patch::Stop(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
extern void Fm2Patch_Stop_m499E1FDE483B982F0ECFB13DB106B8079853CE1F (void);
// 0x00000092 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Fm2Patch::Process(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters,System.Int32,System.Int32)
extern void Fm2Patch_Process_mB2DCB5F80F73E14EB2AB71C9ED90C0BCE92178D9 (void);
// 0x00000093 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Fm2Patch::Load(DaggerfallWorkshop.AudioSynthesis.Bank.DescriptorList,DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager)
extern void Fm2Patch_Load_m3D9677BE78B989C89A657A2FC6EE16C5C595F50D (void);
// 0x00000094 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Fm2Patch_SyncMode DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Fm2Patch::GetSyncModeFromString(System.String)
extern void Fm2Patch_GetSyncModeFromString_m4D8F0DF7EA5575636716D0A27895AE460232B669 (void);
// 0x00000095 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::.ctor(System.String)
extern void MultiPatch__ctor_m9B143F78D2E97970C6FD0F2AEF0E6C44F5E6E89A (void);
// 0x00000096 System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::FindPatches(System.Int32,System.Int32,System.Int32,DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch[])
extern void MultiPatch_FindPatches_m86E92FB9B2C65E94311DD2389C67152F477741EE (void);
// 0x00000097 System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::Start(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
extern void MultiPatch_Start_m051BEFA1324C3EB9F79980EF80D55E82A5E36A64 (void);
// 0x00000098 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::Stop(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
extern void MultiPatch_Stop_m6D37D02FB90C72B9752F2F701B235C4EA54F2EDB (void);
// 0x00000099 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::Process(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters,System.Int32,System.Int32)
extern void MultiPatch_Process_m86E732F414C87F1576BCF0C80D29E67F36888110 (void);
// 0x0000009A System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::Load(DaggerfallWorkshop.AudioSynthesis.Bank.DescriptorList,DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager)
extern void MultiPatch_Load_m9CF0FE343ED66CD76DE2DA425A72130376A37907 (void);
// 0x0000009B System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::LoadSf2(DaggerfallWorkshop.AudioSynthesis.Sf2.Sf2Region[],DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager)
extern void MultiPatch_LoadSf2_m816E0916550720B55AD945071915D7337CBC8CD5 (void);
// 0x0000009C System.String DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::ToString()
extern void MultiPatch_ToString_m9D5BECB5C7858B58ABBB928827EDC57EB4645F20 (void);
// 0x0000009D System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch::DetermineIntervalType()
extern void MultiPatch_DetermineIntervalType_m6A58251047CC6D2651CB9AFACFFC10F0877E48E3 (void);
// 0x0000009E System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::.ctor(DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte,System.Byte)
extern void PatchInterval__ctor_m5A583D82AC90E9F5308FFFC9A0206981179EF39F (void);
// 0x0000009F System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::CheckAllIntervals(System.Int32,System.Int32,System.Int32)
extern void PatchInterval_CheckAllIntervals_m9D366399E10C1D68E62EAC9F60371C2C70FF5E94 (void);
// 0x000000A0 System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::CheckChannelAndKey(System.Int32,System.Int32)
extern void PatchInterval_CheckChannelAndKey_mACC1DB3C02A06917029FEB385B68112D0BC507F0 (void);
// 0x000000A1 System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::CheckKeyAndVelocity(System.Int32,System.Int32)
extern void PatchInterval_CheckKeyAndVelocity_m86E567491485B761A62EF162FE12B924FCF28086 (void);
// 0x000000A2 System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::CheckKey(System.Int32)
extern void PatchInterval_CheckKey_mC0CFD05F2651654CE790B017FDA5294086F2749C (void);
// 0x000000A3 System.String DaggerfallWorkshop.AudioSynthesis.Bank.Patches.MultiPatch_PatchInterval::ToString()
extern void PatchInterval_ToString_m9E72F8C7309019A9B214AC620B39E94B8F5EAF9D (void);
// 0x000000A4 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Sf2Patch::.ctor(System.String)
extern void Sf2Patch__ctor_m34F0B9831C7C64AD8C752200761646D2C396D97B (void);
// 0x000000A5 System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Sf2Patch::Start(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
extern void Sf2Patch_Start_mCA0629C86F14D0399D599E34A62EEAEF02C9E8D5 (void);
// 0x000000A6 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Sf2Patch::Stop(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
extern void Sf2Patch_Stop_m5ED8FCFAD57F8C91FE47803ED8D980041233C137 (void);
// 0x000000A7 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Sf2Patch::Process(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters,System.Int32,System.Int32)
extern void Sf2Patch_Process_mB01B0921F9DD5BF2AD2B1F09C67FA18CB482692A (void);
// 0x000000A8 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Sf2Patch::Load(DaggerfallWorkshop.AudioSynthesis.Bank.DescriptorList,DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager)
extern void Sf2Patch_Load_m0C9C8B27C519097C4FE2BA414F835C0487EA92E5 (void);
// 0x000000A9 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Sf2Patch::Load(DaggerfallWorkshop.AudioSynthesis.Sf2.Sf2Region,DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager)
extern void Sf2Patch_Load_mE52F229D66295A5354946E0B935A69FEFAF5F20F (void);
// 0x000000AA System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Sf2Patch::LoadGen(DaggerfallWorkshop.AudioSynthesis.Sf2.Sf2Region,DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager)
extern void Sf2Patch_LoadGen_mFC4270F5FFE152E2FFA12F56AD7010D35054594A (void);
// 0x000000AB System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Sf2Patch::LoadEnvelopes(DaggerfallWorkshop.AudioSynthesis.Sf2.Sf2Region)
extern void Sf2Patch_LoadEnvelopes_mB3A39A6D057F2EC593D8E8281804D8B56E1516E5 (void);
// 0x000000AC System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Sf2Patch::LoadLfos(DaggerfallWorkshop.AudioSynthesis.Sf2.Sf2Region)
extern void Sf2Patch_LoadLfos_m0232755D160006A95E6495D58BB77C24E0695879 (void);
// 0x000000AD System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Sf2Patch::LoadFilter(DaggerfallWorkshop.AudioSynthesis.Sf2.Sf2Region)
extern void Sf2Patch_LoadFilter_m7A1C2704EEED571C55F87A323F91528F4D4DF590 (void);
// 0x000000AE System.Double DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Sf2Patch::CalculateModulator(DaggerfallWorkshop.AudioSynthesis.Sf2.SourceTypeEnum,DaggerfallWorkshop.AudioSynthesis.Sf2.TransformEnum,DaggerfallWorkshop.AudioSynthesis.Sf2.DirectionEnum,DaggerfallWorkshop.AudioSynthesis.Sf2.PolarityEnum,System.Int32,System.Int32,System.Int32)
extern void Sf2Patch_CalculateModulator_mDE07DED84CDC2C9BC5A5B468D82FDB61DE245C19 (void);
// 0x000000AF System.Boolean DaggerfallWorkshop.AudioSynthesis.Bank.Patches.SfzPatch::Start(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
extern void SfzPatch_Start_m9944BD5780CF8070D9A82994ACEFDDF97A78D9E5 (void);
// 0x000000B0 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.SfzPatch::Stop(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters)
extern void SfzPatch_Stop_m02DD085C8414BCA882C238276D874EED371F8BB0 (void);
// 0x000000B1 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.SfzPatch::Process(DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters,System.Int32,System.Int32)
extern void SfzPatch_Process_m0A5EEC92F25DB69A3AF6AFE791C554EA744813CF (void);
// 0x000000B2 System.Void DaggerfallWorkshop.AudioSynthesis.Bank.Patches.SfzPatch::Load(DaggerfallWorkshop.AudioSynthesis.Bank.DescriptorList,DaggerfallWorkshop.AudioSynthesis.Bank.AssetManager)
extern void SfzPatch_Load_m411AE9DBC31344AE7ECC40DEACE459823B3B4172 (void);
// 0x000000B3 System.String DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::get_Name()
extern void SampleDataAsset_get_Name_mB5F813F5D5C554E57743DF4682E2DE94B2564E1E (void);
// 0x000000B4 System.Int32 DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::get_SampleRate()
extern void SampleDataAsset_get_SampleRate_m72F5D41F212AE832F04796A93E7354CDCDB02147 (void);
// 0x000000B5 System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::get_RootKey()
extern void SampleDataAsset_get_RootKey_m4EC201B4F7D75853CC38680AF422FEEA3DF61A45 (void);
// 0x000000B6 System.Int16 DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::get_Tune()
extern void SampleDataAsset_get_Tune_m4EF2AFA8266AF3189AB30006F89C857D9877CA89 (void);
// 0x000000B7 System.Double DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::get_Start()
extern void SampleDataAsset_get_Start_m8C900602A9E1F49BC9F909CB869CE2226FBDB608 (void);
// 0x000000B8 System.Double DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::get_End()
extern void SampleDataAsset_get_End_m619927E328ECA85DEA2F49D98ED7E18C4AD03204 (void);
// 0x000000B9 System.Double DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::get_LoopStart()
extern void SampleDataAsset_get_LoopStart_mFE7455CBBD28B47161689F30C3DC499EF82C9E60 (void);
// 0x000000BA System.Double DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::get_LoopEnd()
extern void SampleDataAsset_get_LoopEnd_mB5502D6506AB406DF25F642E7B9BBE790E7B43D3 (void);
// 0x000000BB DaggerfallWorkshop.AudioSynthesis.Wave.PcmData DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::get_SampleData()
extern void SampleDataAsset_get_SampleData_mDB001795EFAF87C2D9F2E8FCE3748217DB03719B (void);
// 0x000000BC System.Void DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::.ctor(System.Int32,System.IO.BinaryReader)
extern void SampleDataAsset__ctor_m03CEC353DF8A76949130742CFF06FC5D3E18235D (void);
// 0x000000BD System.Void DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::.ctor(DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader,DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontSampleData)
extern void SampleDataAsset__ctor_m02094D526978D18DCE060E2718D92A0DD80E56CA (void);
// 0x000000BE System.String DaggerfallWorkshop.AudioSynthesis.Bank.SampleDataAsset::ToString()
extern void SampleDataAsset_ToString_m5C136D7AA9017B153D91C75E50C377DA33C57F4E (void);
// 0x000000BF System.Boolean DaggerfallWorkshop.AudioSynthesis.IResource::ReadAllowed()
// 0x000000C0 System.String DaggerfallWorkshop.AudioSynthesis.IResource::GetName()
// 0x000000C1 System.IO.Stream DaggerfallWorkshop.AudioSynthesis.IResource::OpenResourceForRead()
// 0x000000C2 System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::get_DeltaTime()
extern void MidiEvent_get_DeltaTime_m97C2F3FDBD30E16E0FCBA88A59289043D39B0C19 (void);
// 0x000000C3 System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::set_DeltaTime(System.Int32)
extern void MidiEvent_set_DeltaTime_mCFB6BA7BF0A09287F63D746D453B9C3ACF44443D (void);
// 0x000000C4 System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::get_Channel()
extern void MidiEvent_get_Channel_m746C48C59EADC05D90248D955AA3C3105E00CF80 (void);
// 0x000000C5 System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::get_Command()
extern void MidiEvent_get_Command_mC30ADB45D3CA1A281C6741A19435DFCA2DBC35B1 (void);
// 0x000000C6 System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::get_Data1()
extern void MidiEvent_get_Data1_mD48E2705A950E3729D0A955E5442D4857FEF8686 (void);
// 0x000000C7 System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::get_Data2()
extern void MidiEvent_get_Data2_m9921858DB1EBB3286C25D4EBE6E4BA79911080BB (void);
// 0x000000C8 System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::.ctor(System.Int32,System.Byte,System.Byte,System.Byte)
extern void MidiEvent__ctor_mC48EC23C4103B22E1ABE6FCD2B2288E9EFCCA423 (void);
// 0x000000C9 System.String DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent::ToString()
extern void MidiEvent_ToString_m6B6F4232AB1B4F99F3EB7DE82F25FC76153C1E1C (void);
// 0x000000CA System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MetaEvent::get_Channel()
extern void MetaEvent_get_Channel_m5517AAB4E52A60D8ACA1FEF8CF4E59A9B4CF4390 (void);
// 0x000000CB System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MetaEvent::get_Command()
extern void MetaEvent_get_Command_m3136D62D72671C5036B32EB445B4395E67EAFEC4 (void);
// 0x000000CC System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.MetaEvent::.ctor(System.Int32,System.Byte,System.Byte,System.Byte)
extern void MetaEvent__ctor_m217FEBC781DEF27B72CF041F85BA3094D874E3CE (void);
// 0x000000CD System.String DaggerfallWorkshop.AudioSynthesis.Midi.Event.MetaEvent::ToString()
extern void MetaEvent_ToString_m46D27E742A9303DA09F1E32FD6A05C732E9740F2 (void);
// 0x000000CE System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.MetaDataEvent::.ctor(System.Int32,System.Byte,System.Byte,System.Byte[])
extern void MetaDataEvent__ctor_m085860E95DB1EBAC21DFB5F55414E540494AE646 (void);
// 0x000000CF System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MetaNumberEvent::get_Value()
extern void MetaNumberEvent_get_Value_m3A4E94990267B14B4533C27AD080E888B9A3152F (void);
// 0x000000D0 System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.MetaNumberEvent::.ctor(System.Int32,System.Byte,System.Byte,System.Int32)
extern void MetaNumberEvent__ctor_m951D8E03733B413403D06EBFCC32ED4E3A097BCD (void);
// 0x000000D1 System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.MetaTextEvent::.ctor(System.Int32,System.Byte,System.Byte,System.String)
extern void MetaTextEvent__ctor_mCEF54304664FCA060E16147B8A1F04EB4A7DED20 (void);
// 0x000000D2 System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.RealTimeEvent::get_Channel()
extern void RealTimeEvent_get_Channel_mC79D78BDAED328E36C9B0C97BA3A9DFBA95CC3E3 (void);
// 0x000000D3 System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.RealTimeEvent::get_Command()
extern void RealTimeEvent_get_Command_m8C5165ED6A7B412B53577ACF6F414856BC7E0A7C (void);
// 0x000000D4 System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.RealTimeEvent::.ctor(System.Int32,System.Byte,System.Byte,System.Byte)
extern void RealTimeEvent__ctor_m5BE4D0C0AB6D82FF6A7F66D39B565EE665D3ABE9 (void);
// 0x000000D5 System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemCommonEvent::get_Channel()
extern void SystemCommonEvent_get_Channel_m6274962E4FFFB0C5821DFA421C5B8C6C40A3746A (void);
// 0x000000D6 System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemCommonEvent::get_Command()
extern void SystemCommonEvent_get_Command_m30A25EC8C57C646000414E73CA7FBE545F84C894 (void);
// 0x000000D7 System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemCommonEvent::.ctor(System.Int32,System.Byte,System.Byte,System.Byte)
extern void SystemCommonEvent__ctor_m28761B5E8EB12382AEB313AFDA05F256C81D8598 (void);
// 0x000000D8 System.String DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemCommonEvent::ToString()
extern void SystemCommonEvent_ToString_m8DB3E6A0EDBB51469405DF4B6CE965AE75EE2CD5 (void);
// 0x000000D9 System.Void DaggerfallWorkshop.AudioSynthesis.Midi.Event.SystemExclusiveEvent::.ctor(System.Int32,System.Byte,System.Int16,System.Byte[])
extern void SystemExclusiveEvent__ctor_m01B4CA589EF833C8271C11978B0C27EA06F211BC (void);
// 0x000000DA System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::get_Division()
extern void MidiFile_get_Division_mA6875C36607123C23EA227F186188E056ED05D5E (void);
// 0x000000DB DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack[] DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::get_Tracks()
extern void MidiFile_get_Tracks_m10D22CE5FFDD6DFC6B375BC6D0AA52D4C170B3DF (void);
// 0x000000DC System.Void DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::.ctor(DaggerfallWorkshop.AudioSynthesis.IResource)
extern void MidiFile__ctor_m37364DFD91D1C6C52AA2172C7D6F74D14F36C28D (void);
// 0x000000DD System.Void DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::CombineTracks()
extern void MidiFile_CombineTracks_m2050F8CF6EDDE6B264C91CC5C28F54949045D2A3 (void);
// 0x000000DE DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::MergeTracks()
extern void MidiFile_MergeTracks_m27DC4470C42B91A050847D997F0E686A0183DA8A (void);
// 0x000000DF System.Void DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::LoadStream(System.IO.BinaryReader)
extern void MidiFile_LoadStream_mCE8F1A3F13DCB71E996C3114CFAEAFB0B1A64360 (void);
// 0x000000E0 System.Void DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::ReadHeader(System.IO.BinaryReader)
extern void MidiFile_ReadHeader_mCA741D6ACF8EA655854CDAE185EA1AC6521B0B9D (void);
// 0x000000E1 DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::ReadTrack(System.IO.BinaryReader)
extern void MidiFile_ReadTrack_m567C17DC36129CD32E1B8B724F3437F547DC734A (void);
// 0x000000E2 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::ReadMetaMessage(System.IO.BinaryReader,System.Int32,System.Byte)
extern void MidiFile_ReadMetaMessage_mF336B48E2AEA27F8D9DC32E89C9F70E269B4BF4D (void);
// 0x000000E3 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::ReadRealTimeMessage(System.IO.BinaryReader,System.Int32,System.Byte)
extern void MidiFile_ReadRealTimeMessage_m237D704C9BC54F13322A622CD960B63CC4F20F45 (void);
// 0x000000E4 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::ReadSystemCommonMessage(System.IO.BinaryReader,System.Int32,System.Byte)
extern void MidiFile_ReadSystemCommonMessage_mBEA607FB23C4362BA692FA3621CE3B1A647F279B (void);
// 0x000000E5 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::ReadVoiceMessage(System.IO.BinaryReader,System.Int32,System.Byte,System.Byte)
extern void MidiFile_ReadVoiceMessage_mC1A4B09A621D3C770ED07E0EB3165C5D567AD540 (void);
// 0x000000E6 System.Void DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::TrackVoiceStats(DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent,System.Collections.Generic.List`1<System.Byte>,System.Collections.Generic.List`1<System.Byte>,System.Int32&,System.Int32&)
extern void MidiFile_TrackVoiceStats_m833D32DFAA958A7609FB0BFC4835648B4DDF86DB (void);
// 0x000000E7 System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::ReadVariableLength(System.IO.BinaryReader)
extern void MidiFile_ReadVariableLength_m1E07A05BEBF4C71494AECB08A8B0E8340AB55E47 (void);
// 0x000000E8 System.String DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::ReadString(System.IO.BinaryReader)
extern void MidiFile_ReadString_m94AF20C60D01CF9127B48F3632514F143F2890EE (void);
// 0x000000E9 System.Boolean DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile::FindHead(System.IO.BinaryReader,System.Int32)
extern void MidiFile_FindHead_m33101C32BEEF7CF946CC7880149C68C3ACE78020 (void);
// 0x000000EA System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack::get_NoteOnCount()
extern void MidiTrack_get_NoteOnCount_m2CD1532D935D387E49A6B4A698908830F75446C1 (void);
// 0x000000EB System.Void DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack::set_NoteOnCount(System.Int32)
extern void MidiTrack_set_NoteOnCount_m5D0F587CCFA51735C402C7E551435337BAA20A36 (void);
// 0x000000EC System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack::get_EndTime()
extern void MidiTrack_get_EndTime_m0A9531D9560D6F05B95CD4C4D78BD0BD3AECD77D (void);
// 0x000000ED System.Void DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack::set_EndTime(System.Int32)
extern void MidiTrack_set_EndTime_m3C08B0AE73980F90F5793FD8111812A671EA8D5C (void);
// 0x000000EE System.Int32 DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack::get_ActiveChannels()
extern void MidiTrack_get_ActiveChannels_mDC38C6D57ECCBAE6EAEC6276715FA1D8408FC0BF (void);
// 0x000000EF System.Void DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack::set_ActiveChannels(System.Int32)
extern void MidiTrack_set_ActiveChannels_m2EEE759B29507306B600E1B5AA161C9E8447E56C (void);
// 0x000000F0 DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent[] DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack::get_MidiEvents()
extern void MidiTrack_get_MidiEvents_mA994C8B330229D4BB96FB270B0357D9C08193847 (void);
// 0x000000F1 System.Byte[] DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack::get_Instruments()
extern void MidiTrack_get_Instruments_m9D7658AE0405A3D35A98D579F90B1E68F336EEB1 (void);
// 0x000000F2 System.Byte[] DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack::get_DrumInstruments()
extern void MidiTrack_get_DrumInstruments_m3321A11FDF950A298B87D0E1F0ABB02C625B12A5 (void);
// 0x000000F3 System.Void DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack::.ctor(System.Byte[],System.Byte[],DaggerfallWorkshop.AudioSynthesis.Midi.Event.MidiEvent[])
extern void MidiTrack__ctor_mCA651C5B7988495506EE919D365C7CAED9DE925D (void);
// 0x000000F4 System.String DaggerfallWorkshop.AudioSynthesis.Midi.MidiTrack::ToString()
extern void MidiTrack_ToString_m62A0DBCF60018C8A545C6DB13696B8EDA894746C (void);
// 0x000000F5 System.Boolean DaggerfallWorkshop.AudioSynthesis.Sequencer.MidiFileSequencer::get_IsPlaying()
extern void MidiFileSequencer_get_IsPlaying_m3690B94C7D26581B9C5D934A51FD4AA5D9A94AC9 (void);
// 0x000000F6 System.Boolean DaggerfallWorkshop.AudioSynthesis.Sequencer.MidiFileSequencer::get_IsMidiLoaded()
extern void MidiFileSequencer_get_IsMidiLoaded_m3CA214B55D7EE7080D21C782AB749FCF8C657B53 (void);
// 0x000000F7 System.Int32 DaggerfallWorkshop.AudioSynthesis.Sequencer.MidiFileSequencer::get_CurrentTime()
extern void MidiFileSequencer_get_CurrentTime_m0AB689D395D3E121721C93F0FB94C432D4303067 (void);
// 0x000000F8 System.Int32 DaggerfallWorkshop.AudioSynthesis.Sequencer.MidiFileSequencer::get_EndTime()
extern void MidiFileSequencer_get_EndTime_m356E5D952A0F60A1E1597E2BD783F15AC52C5D0B (void);
// 0x000000F9 System.Void DaggerfallWorkshop.AudioSynthesis.Sequencer.MidiFileSequencer::.ctor(DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer)
extern void MidiFileSequencer__ctor_m182E67FCD95540D20D99CE683C45052184BEEFF4 (void);
// 0x000000FA System.Boolean DaggerfallWorkshop.AudioSynthesis.Sequencer.MidiFileSequencer::LoadMidi(DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile)
extern void MidiFileSequencer_LoadMidi_m287ABEF78AF528CB674CDCD0DCAD0DD49409CB4E (void);
// 0x000000FB System.Void DaggerfallWorkshop.AudioSynthesis.Sequencer.MidiFileSequencer::Play()
extern void MidiFileSequencer_Play_mBF3A6D5A67C829C8EBD646104C2A2C064036F8CE (void);
// 0x000000FC System.Void DaggerfallWorkshop.AudioSynthesis.Sequencer.MidiFileSequencer::Stop()
extern void MidiFileSequencer_Stop_m80533BAD37D6E05AAA8021F98EB43742E4AD7164 (void);
// 0x000000FD System.Void DaggerfallWorkshop.AudioSynthesis.Sequencer.MidiFileSequencer::FillMidiEventQueue()
extern void MidiFileSequencer_FillMidiEventQueue_m583982C5D32351F12F9BA82102D29C2A04B22F6C (void);
// 0x000000FE System.Void DaggerfallWorkshop.AudioSynthesis.Sequencer.MidiFileSequencer::LoadMidiFile(DaggerfallWorkshop.AudioSynthesis.Midi.MidiFile)
extern void MidiFileSequencer_LoadMidiFile_mEC394F5310157D05AFC418EF0886D3786C98E8F9 (void);
// 0x000000FF System.Void DaggerfallWorkshop.AudioSynthesis.Util.Riff.Chunk::.ctor(System.String,System.Int32)
extern void Chunk__ctor_m09D15006E79DF3FA8C2E92747839186ECB2661A4 (void);
// 0x00000100 DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[] DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.GeneratorChunk::get_Generators()
extern void GeneratorChunk_get_Generators_m995FDB9DC64121C8EF7A0173E12F561C8C133A6B (void);
// 0x00000101 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.GeneratorChunk::.ctor(System.String,System.Int32,System.IO.BinaryReader)
extern void GeneratorChunk__ctor_m84365B0F70A8E62D84F6E5C8EC7C6D0418C22CBC (void);
// 0x00000102 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.InstrumentChunk::.ctor(System.String,System.Int32,System.IO.BinaryReader)
extern void InstrumentChunk__ctor_m613700DD9AC753A5DBD3A3D2B23A079EDE30C869 (void);
// 0x00000103 DaggerfallWorkshop.AudioSynthesis.Sf2.Instrument[] DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.InstrumentChunk::ToInstruments(DaggerfallWorkshop.AudioSynthesis.Sf2.Zone[])
extern void InstrumentChunk_ToInstruments_m2AC461F8B733FFE32897CCFE3506AEF117870BA8 (void);
// 0x00000104 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.InstrumentChunk_RawInstrument::.ctor()
extern void RawInstrument__ctor_mE9AF8C41617F181C318B00E034BD93698945A831 (void);
// 0x00000105 DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[] DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ModulatorChunk::get_Modulators()
extern void ModulatorChunk_get_Modulators_m39332FE630A9EE3DD1EE0CE550A440003257DDC6 (void);
// 0x00000106 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ModulatorChunk::.ctor(System.String,System.Int32,System.IO.BinaryReader)
extern void ModulatorChunk__ctor_mFB66610A327D5B5B9C5895DA9F488D2E88781957 (void);
// 0x00000107 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk::.ctor(System.String,System.Int32,System.IO.BinaryReader)
extern void PresetHeaderChunk__ctor_m0F2D9995AA2B7EEAA9FC7FF8EAE3D1441D58B0F3 (void);
// 0x00000108 DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader[] DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk::ToPresets(DaggerfallWorkshop.AudioSynthesis.Sf2.Zone[])
extern void PresetHeaderChunk_ToPresets_m42674778BE988CAA5E368F33364F6D0D12A1E9A1 (void);
// 0x00000109 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.PresetHeaderChunk_RawPreset::.ctor()
extern void RawPreset__ctor_mB2ED8A0F79A1CC0B9A6038176BFB6FEF4494E7E4 (void);
// 0x0000010A DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader[] DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.SampleHeaderChunk::get_SampleHeaders()
extern void SampleHeaderChunk_get_SampleHeaders_m640AF6A68DA7BE215F57BD230259B0D1083C139B (void);
// 0x0000010B System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.SampleHeaderChunk::.ctor(System.String,System.Int32,System.IO.BinaryReader)
extern void SampleHeaderChunk__ctor_m56E521C2D2506E8FF189B43BF3CAA1A4C058A1AC (void);
// 0x0000010C System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk::.ctor(System.String,System.Int32,System.IO.BinaryReader)
extern void ZoneChunk__ctor_m4A647A21B1CB9F0430922669AF13D0A66539C513 (void);
// 0x0000010D DaggerfallWorkshop.AudioSynthesis.Sf2.Zone[] DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk::ToZones(DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[],DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[])
extern void ZoneChunk_ToZones_m24160C8D66DBB5B4D8322607AE687113B21EB205 (void);
// 0x0000010E System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Chunks.ZoneChunk_RawZoneData::.ctor()
extern void RawZoneData__ctor_mC6A341B8DBB243161F41FA43121208581B74605D (void);
// 0x0000010F System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Generator::.ctor(System.IO.BinaryReader)
extern void Generator__ctor_m563F2E3AE03C058CEF6501765FE05D705B9F4A01 (void);
// 0x00000110 DaggerfallWorkshop.AudioSynthesis.Sf2.GeneratorEnum DaggerfallWorkshop.AudioSynthesis.Sf2.Generator::get_GeneratorType()
extern void Generator_get_GeneratorType_m0C291F4777285D205B56432D86F68E9C18976DAD (void);
// 0x00000111 System.Int16 DaggerfallWorkshop.AudioSynthesis.Sf2.Generator::get_AmountInt16()
extern void Generator_get_AmountInt16_m772159529C599F06171652F7E49C5DB9781FEFF9 (void);
// 0x00000112 System.String DaggerfallWorkshop.AudioSynthesis.Sf2.Generator::ToString()
extern void Generator_ToString_m9B969188C843C16A82908ED1169913D91D396138 (void);
// 0x00000113 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Instrument::set_Name(System.String)
extern void Instrument_set_Name_m668B546F792570A9FF0449EA56F4F4030A3F1813 (void);
// 0x00000114 DaggerfallWorkshop.AudioSynthesis.Sf2.Zone[] DaggerfallWorkshop.AudioSynthesis.Sf2.Instrument::get_Zones()
extern void Instrument_get_Zones_m3DE84F9EE84E517D687DFAF3188ECFD32088BFA9 (void);
// 0x00000115 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Instrument::set_Zones(DaggerfallWorkshop.AudioSynthesis.Sf2.Zone[])
extern void Instrument_set_Zones_mEA995312D785A38DDEA62C31C0CD3EE3D820F4E7 (void);
// 0x00000116 System.String DaggerfallWorkshop.AudioSynthesis.Sf2.Instrument::ToString()
extern void Instrument_ToString_mF543B3F862F3077804D70CF8FEDFAAA9D9061EB9 (void);
// 0x00000117 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Instrument::.ctor()
extern void Instrument__ctor_m6DDD5A89CB963B188C360CB6CFF8981777A58C2A (void);
// 0x00000118 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator::.ctor(System.IO.BinaryReader)
extern void Modulator__ctor_mAE34CAFA1779EEDA2C574F470A32F42A0AD96A56 (void);
// 0x00000119 System.String DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator::ToString()
extern void Modulator_ToString_m0B470B15DD6D0442D2D0182E943F260149C45CAF (void);
// 0x0000011A System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.ModulatorType::.ctor(System.IO.BinaryReader)
extern void ModulatorType__ctor_m7BDBD5C387B89EF90D6DB2F0AD6F8865CB5DC7CD (void);
// 0x0000011B System.String DaggerfallWorkshop.AudioSynthesis.Sf2.ModulatorType::ToString()
extern void ModulatorType_ToString_m6E6EA4E694E3F5F6EBD4F34FB6F6A3435AA23613 (void);
// 0x0000011C System.String DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::get_Name()
extern void PresetHeader_get_Name_m4387DB61C972D6385E8DECCEBBDB8A4971A5316F (void);
// 0x0000011D System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::set_Name(System.String)
extern void PresetHeader_set_Name_m7413D3AB365F9FBCF30F3B3F6BEB66E0B84C5438 (void);
// 0x0000011E System.Int32 DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::get_PatchNumber()
extern void PresetHeader_get_PatchNumber_m85ACEA1182B25537CA0E66AB83D88593918F191B (void);
// 0x0000011F System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::set_PatchNumber(System.Int32)
extern void PresetHeader_set_PatchNumber_mCF27F8B5C4A2E2680179DB9C37A27891E48B3C63 (void);
// 0x00000120 System.Int32 DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::get_BankNumber()
extern void PresetHeader_get_BankNumber_m051EF27EE23F425052F72EBE34F1780B58C51375 (void);
// 0x00000121 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::set_BankNumber(System.Int32)
extern void PresetHeader_set_BankNumber_mD844D872D7B7E34E826CB5D29C98981235E2CA97 (void);
// 0x00000122 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::set_Library(System.Int32)
extern void PresetHeader_set_Library_m995DA7156D2F40BA42EEEB78E9650B688E0FBF93 (void);
// 0x00000123 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::set_Genre(System.Int32)
extern void PresetHeader_set_Genre_m73494990E6EA80456B7DC25532FBBE4DFEF9BF42 (void);
// 0x00000124 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::set_Morphology(System.Int32)
extern void PresetHeader_set_Morphology_m687D8D2FE3718FC4DD22FF999CA607BE63556C9C (void);
// 0x00000125 DaggerfallWorkshop.AudioSynthesis.Sf2.Zone[] DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::get_Zones()
extern void PresetHeader_get_Zones_m15723AD367886748961F61C9356383E575881F65 (void);
// 0x00000126 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::set_Zones(DaggerfallWorkshop.AudioSynthesis.Sf2.Zone[])
extern void PresetHeader_set_Zones_m72C3FE1482FB7AFABF0E854F4CBF6D51D041FD70 (void);
// 0x00000127 System.String DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::ToString()
extern void PresetHeader_ToString_m5E92AB4A4B3127F762F77B6EE554F5BABEF9BFEF (void);
// 0x00000128 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader::.ctor()
extern void PresetHeader__ctor_m130ECD4566B469F09EEC647023B91D7F56150973 (void);
// 0x00000129 System.String DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader::get_Name()
extern void SampleHeader_get_Name_m4B7FA1213B10DAA8F8BD88423EC57F934CE80315 (void);
// 0x0000012A System.Int32 DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader::get_Start()
extern void SampleHeader_get_Start_mE39326D8AC9AA8058D0653CF9BDDAACBC8ACD792 (void);
// 0x0000012B System.Int32 DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader::get_End()
extern void SampleHeader_get_End_mEEF30E298093DA62DB8830A05943D05ED0896C8D (void);
// 0x0000012C System.Int32 DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader::get_StartLoop()
extern void SampleHeader_get_StartLoop_m856466629F8A38DD92763411CD037DB1161FEB6D (void);
// 0x0000012D System.Int32 DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader::get_EndLoop()
extern void SampleHeader_get_EndLoop_m440CC51C81F2F50AFAA91B36329A8DF669937FFB (void);
// 0x0000012E System.Int32 DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader::get_SampleRate()
extern void SampleHeader_get_SampleRate_m6701A223849FF436297F9A7619AEB8CAACBB4365 (void);
// 0x0000012F System.Byte DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader::get_RootKey()
extern void SampleHeader_get_RootKey_m4EFFDAF87F3DDA1706BD4C314BDC4563FF79047D (void);
// 0x00000130 System.Int16 DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader::get_Tune()
extern void SampleHeader_get_Tune_m0432A2D9DCAAEB69F77920E99D733C1AAF338EAC (void);
// 0x00000131 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader::.ctor(System.IO.BinaryReader)
extern void SampleHeader__ctor_mF5CE982CB4398C4C87CBA90B14C2DC5014DC50CC (void);
// 0x00000132 System.String DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader::ToString()
extern void SampleHeader_ToString_mBE513699B4D236F17B8F6D9444DF38CFEEA54702 (void);
// 0x00000133 System.Int16[] DaggerfallWorkshop.AudioSynthesis.Sf2.Sf2Region::get_Generators()
extern void Sf2Region_get_Generators_m688C8A6176FB485DA1917C5755A2DEAC3D825BE9 (void);
// 0x00000134 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Sf2Region::.ctor()
extern void Sf2Region__ctor_mEA669A892C0F853AD2666A96986280CE63A598F6 (void);
// 0x00000135 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Sf2Region::ApplyDefaultValues()
extern void Sf2Region_ApplyDefaultValues_m33EFEA2A4EB6CA14C82679CA07DDD2B9443F8039 (void);
// 0x00000136 DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontInfo DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFont::get_Info()
extern void SoundFont_get_Info_m359D1B7B33589A9BA45D0AF3150BBE0783818C6A (void);
// 0x00000137 DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontSampleData DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFont::get_SampleData()
extern void SoundFont_get_SampleData_mDCD7B4A6E683CE1B73DE09E1A36BCFD4564E0C8B (void);
// 0x00000138 DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontPresets DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFont::get_Presets()
extern void SoundFont_get_Presets_m9A955EC4019609BBCFF0EB82C9A39E1C055A70CC (void);
// 0x00000139 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFont::.ctor(System.IO.Stream)
extern void SoundFont__ctor_m81BE9723A39D59EE81C0BA6BF31572B640823221 (void);
// 0x0000013A System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFont::Load(System.IO.Stream)
extern void SoundFont_Load_mF6172B11DE4D7A83D3385E350421A34F535A2FE9 (void);
// 0x0000013B System.String DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontInfo::get_BankName()
extern void SoundFontInfo_get_BankName_mFBA41925C4F204A45AB07778510F232A0E06207D (void);
// 0x0000013C System.String DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontInfo::get_Comments()
extern void SoundFontInfo_get_Comments_m59C2E16E2B0C61BD4DA5C3442703422D8235C4CF (void);
// 0x0000013D System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontInfo::.ctor(System.IO.BinaryReader)
extern void SoundFontInfo__ctor_m907B995C74C74FD59700E847ABE1D263F767C4A1 (void);
// 0x0000013E System.String DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontInfo::ToString()
extern void SoundFontInfo_ToString_mF5F166AAF6FAFBCE3C45439C60972B1BD9769D68 (void);
// 0x0000013F DaggerfallWorkshop.AudioSynthesis.Sf2.SampleHeader[] DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontPresets::get_SampleHeaders()
extern void SoundFontPresets_get_SampleHeaders_m1FF7DB75102B1862F455F434F08E6C3D03F65B20 (void);
// 0x00000140 DaggerfallWorkshop.AudioSynthesis.Sf2.PresetHeader[] DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontPresets::get_PresetHeaders()
extern void SoundFontPresets_get_PresetHeaders_mCB6EA2C1033E700128E10DBA204070AA9CC13812 (void);
// 0x00000141 DaggerfallWorkshop.AudioSynthesis.Sf2.Instrument[] DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontPresets::get_Instruments()
extern void SoundFontPresets_get_Instruments_mF96F3A36C2776FC28987F2D2A668E7406F11B14F (void);
// 0x00000142 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontPresets::.ctor(System.IO.BinaryReader)
extern void SoundFontPresets__ctor_m6928685F8804077E4AF1736C53FAE8DD266D1619 (void);
// 0x00000143 System.Int32 DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontSampleData::get_BitsPerSample()
extern void SoundFontSampleData_get_BitsPerSample_mC0D8308DC30F0C2E5CB7ED087860254CD19A100D (void);
// 0x00000144 System.Byte[] DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontSampleData::get_SampleData()
extern void SoundFontSampleData_get_SampleData_m28CAAB537EC75754A48FEECECFCA14849C205FB1 (void);
// 0x00000145 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.SoundFontSampleData::.ctor(System.IO.BinaryReader)
extern void SoundFontSampleData__ctor_mCD737B974C250250F3D8E2F61699E940942C33F6 (void);
// 0x00000146 DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[] DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::get_Modulators()
extern void Zone_get_Modulators_m819BC5A415FDD098C62F176644BC7B832E9D65DF (void);
// 0x00000147 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::set_Modulators(DaggerfallWorkshop.AudioSynthesis.Sf2.Modulator[])
extern void Zone_set_Modulators_mCD8A63184ED534C3F54CB8C231624B866588FD75 (void);
// 0x00000148 DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[] DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::get_Generators()
extern void Zone_get_Generators_m3BDD4F7A064D556CD8FD224F4828417D82EAA019 (void);
// 0x00000149 System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::set_Generators(DaggerfallWorkshop.AudioSynthesis.Sf2.Generator[])
extern void Zone_set_Generators_m051434F7E04B03839D9E45F687EF1D168E486E3D (void);
// 0x0000014A System.String DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::ToString()
extern void Zone_ToString_m136116DA43FF3C8E3CB7CA0E9517D84815B05EAF (void);
// 0x0000014B System.Void DaggerfallWorkshop.AudioSynthesis.Sf2.Zone::.ctor()
extern void Zone__ctor_mFEE2F08A75809A1B25C605D48A2581E51922DF97 (void);
// 0x0000014C System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage::.ctor(System.Byte,System.Byte,System.Byte,System.Byte)
extern void MidiMessage__ctor_mAD45EAB09928A792C10A4BE1937A00F6B4061EBD_AdjustorThunk (void);
// 0x0000014D System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage::.ctor(System.Int32,System.Byte,System.Byte,System.Byte,System.Byte)
extern void MidiMessage__ctor_m59BF4D6F32B5DA99D5DFC19D597E554864667A00_AdjustorThunk (void);
// 0x0000014E System.String DaggerfallWorkshop.AudioSynthesis.Synthesis.MidiMessage::ToString()
extern void MidiMessage_ToString_m055AEB532CF77AAEBAB7E9F7B91A1EA552E95651_AdjustorThunk (void);
// 0x0000014F System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.PanComponent::.ctor(System.Single,DaggerfallWorkshop.AudioSynthesis.Synthesis.PanFormulaEnum)
extern void PanComponent__ctor_mD7FAB1EB204E99DA52960E149378CE90BF2846F3_AdjustorThunk (void);
// 0x00000150 System.String DaggerfallWorkshop.AudioSynthesis.Synthesis.PanComponent::ToString()
extern void PanComponent_ToString_mF747C37B8CD9699F43E15DA4C8C342C86172388A_AdjustorThunk (void);
// 0x00000151 System.Byte DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::get_Coarse()
extern void CCValue_get_Coarse_m758E91200E1E2D2798B94B495612FF936A7CE0F9_AdjustorThunk (void);
// 0x00000152 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::set_Coarse(System.Byte)
extern void CCValue_set_Coarse_m5344D4D334F3D8D89B69019028B9DF82A2076649_AdjustorThunk (void);
// 0x00000153 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::set_Fine(System.Byte)
extern void CCValue_set_Fine_mC5FE00B563D7B73BD9135AC48B16B2FFB5B65B6D_AdjustorThunk (void);
// 0x00000154 System.Int16 DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::get_Combined()
extern void CCValue_get_Combined_mE4C5B314DE167D020D5A07C59295911EF384E5E4_AdjustorThunk (void);
// 0x00000155 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::set_Combined(System.Int16)
extern void CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00_AdjustorThunk (void);
// 0x00000156 System.String DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::ToString()
extern void CCValue_ToString_m57A63889A06F27A2A50581402258E27EEFBBAE6E_AdjustorThunk (void);
// 0x00000157 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::UpdateCombined()
extern void CCValue_UpdateCombined_mBE04F2338665F67B2D3A5FFF057D5B703582279B_AdjustorThunk (void);
// 0x00000158 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.CCValue::UpdateCoarseFinePair()
extern void CCValue_UpdateCoarseFinePair_mFD9A00C5E418B55566DC83315E5FA46158F9607F_AdjustorThunk (void);
// 0x00000159 System.Double DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::Clamp(System.Double,System.Double,System.Double)
extern void SynthHelper_Clamp_mE5F96B195032983A5C4642B4FCAE05F1766CAEDF (void);
// 0x0000015A System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::Clamp(System.Single,System.Single,System.Single)
extern void SynthHelper_Clamp_mABDAD8ED1DC346DC83659F2142445186C92D3120 (void);
// 0x0000015B System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::Clamp(System.Int32,System.Int32,System.Int32)
extern void SynthHelper_Clamp_m8900A1DC809E91761D20C656937E5EE6B04454F1 (void);
// 0x0000015C System.Int16 DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::Clamp(System.Int16,System.Int16,System.Int16)
extern void SynthHelper_Clamp_mBAC49A911FF04B30E21575B6C2E07A7B1F43C2BD (void);
// 0x0000015D System.Double DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::DBtoLinear(System.Double)
extern void SynthHelper_DBtoLinear_m224252A9D521905D87410CE27FA28E548613E53D (void);
// 0x0000015E System.Double DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::KeyToFrequency(System.Double,System.Int32)
extern void SynthHelper_KeyToFrequency_m82BB6EF80E9FBE00A0FD57B67E794D9B01E6C9A4 (void);
// 0x0000015F System.Double DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthHelper::CentsToPitch(System.Int32)
extern void SynthHelper_CentsToPitch_mDD57BCD740AA6583A7A5DB8F690AE9B03AE7CAB8 (void);
// 0x00000160 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::.ctor(DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer)
extern void SynthParameters__ctor_mAAD2A21AC9314C750B70EA524E730F4235373711 (void);
// 0x00000161 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::ResetControllers()
extern void SynthParameters_ResetControllers_m0027611CEA3F87605275C99E8640F89D27B896E8 (void);
// 0x00000162 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentVolume()
extern void SynthParameters_UpdateCurrentVolume_mF65FAC56F5D1EA02E14EA6FB0D4FBFBFE85C2C61 (void);
// 0x00000163 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentPitch()
extern void SynthParameters_UpdateCurrentPitch_mE15F0D19B7BE2079C6347066DA363D578A4FEB08 (void);
// 0x00000164 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentMod()
extern void SynthParameters_UpdateCurrentMod_m8B938492578703025D6A3B5F01193E0D80B144E2 (void);
// 0x00000165 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters::UpdateCurrentPan()
extern void SynthParameters_UpdateCurrentPan_m08069E906C15598A7B038FB984A6D5D56B593090 (void);
// 0x00000166 System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::get_MicroBufferSize()
extern void Synthesizer_get_MicroBufferSize_mBD18E712321364ECB8646B7AB30FF00A03D65FF4 (void);
// 0x00000167 System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::get_WorkingBufferSize()
extern void Synthesizer_get_WorkingBufferSize_mC0FF10DBED7D3B78209E74A42AB6303D62A151DE (void);
// 0x00000168 System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::get_MixGain()
extern void Synthesizer_get_MixGain_m1834D1A1C4F51536F338A4C7AB998A4A359B14A3 (void);
// 0x00000169 System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::get_SampleRate()
extern void Synthesizer_get_SampleRate_m600A7A592D514CD27764AF39B3586E022E6E2147 (void);
// 0x0000016A System.Int32 DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::get_AudioChannels()
extern void Synthesizer_get_AudioChannels_mFC9566B67AEF5AA6CB4F7742F8976A9E43AA42D2 (void);
// 0x0000016B System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Synthesizer__ctor_m5B2E9379382A890BBEA8FE575F2B4F01A8B83408 (void);
// 0x0000016C System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::LoadBank(DaggerfallWorkshop.AudioSynthesis.IResource)
extern void Synthesizer_LoadBank_m5A866814011F2BC24C0AAD5F3E6BF71FD7D40580 (void);
// 0x0000016D System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::LoadBank(DaggerfallWorkshop.AudioSynthesis.Bank.PatchBank)
extern void Synthesizer_LoadBank_mB0296667D3FDB5AB407923A080E4A6C0B9A5D783 (void);
// 0x0000016E System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::UnloadBank()
extern void Synthesizer_UnloadBank_m622C2A0C97C8AA07176FBFDC52E41D9CABE3FDA0 (void);
// 0x0000016F System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ResetSynthControls()
extern void Synthesizer_ResetSynthControls_mD469453AF5D6CF0F67E9D50FDF7DB78DCBCF7D16 (void);
// 0x00000170 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ResetPrograms()
extern void Synthesizer_ResetPrograms_m5EF661697DAC332F0B06B51195B76F63DDB5AD5F (void);
// 0x00000171 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::GetNext(System.Single[])
extern void Synthesizer_GetNext_mE08CF4F0DEA6A65D7D91E98A773E031DA317B15D (void);
// 0x00000172 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::FillWorkingBuffer()
extern void Synthesizer_FillWorkingBuffer_m53938F9A39846C0E6EC7A76C7A1DF39ABC640974 (void);
// 0x00000173 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ConvertWorkingBuffer(System.Single[],System.Single[])
extern void Synthesizer_ConvertWorkingBuffer_mBC00D122A20A8577DC0AEAACA2B701D114E498FF (void);
// 0x00000174 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::NoteOn(System.Int32,System.Int32,System.Int32)
extern void Synthesizer_NoteOn_m04E2EFEF32FD9B4E13480059E411E37F26FD5772 (void);
// 0x00000175 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::NoteOff(System.Int32,System.Int32)
extern void Synthesizer_NoteOff_mAA9E92FBA1972D7413249B4C79CA46C064BEB2E8 (void);
// 0x00000176 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::NoteOffAll(System.Boolean)
extern void Synthesizer_NoteOffAll_m09B0E9EA1805A76EDADE4E4E0443AFD9AB7FAA88 (void);
// 0x00000177 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ProcessMidiMessage(System.Int32,System.Int32,System.Int32,System.Int32)
extern void Synthesizer_ProcessMidiMessage_m068A780E4843F4C24833F1C6BDCAFD33D22728BC (void);
// 0x00000178 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ReleaseAllHoldPedals()
extern void Synthesizer_ReleaseAllHoldPedals_mAA8BF558958F700C063299A6DDC2D27B38F16239 (void);
// 0x00000179 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::ReleaseHoldPedal(System.Int32)
extern void Synthesizer_ReleaseHoldPedal_m8DD7E693144C996CC0EC5451B000B5F5C6203029 (void);
// 0x0000017A System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Synthesizer::.cctor()
extern void Synthesizer__cctor_m33EB9B17280769B6A4DD40FC6C6F28BE0E8A8CF2 (void);
// 0x0000017B DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::get_Patch()
extern void Voice_get_Patch_m4758BB409337AC45CCFB570CF5283E155D725574 (void);
// 0x0000017C DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::get_VoiceParams()
extern void Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5 (void);
// 0x0000017D System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::.ctor()
extern void Voice__ctor_mC8F53C63FE90B4AC4517F3FB27EF8E2CF73ABA28 (void);
// 0x0000017E System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Start()
extern void Voice_Start_mFD62C40C95388C7CA2E2677758C8F25A75BCF807 (void);
// 0x0000017F System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Stop()
extern void Voice_Stop_m8E10957D7FABBF4A40F8370F5D8D77EE39A64567 (void);
// 0x00000180 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::StopImmediately()
extern void Voice_StopImmediately_m3675EECC2EF7522DE6BF677F419648631A557D21 (void);
// 0x00000181 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Process(System.Int32,System.Int32)
extern void Voice_Process_m7893B616DA247441F4231BC8EEE204B01C0D348C (void);
// 0x00000182 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::Configure(System.Int32,System.Int32,System.Int32,DaggerfallWorkshop.AudioSynthesis.Bank.Patches.Patch,DaggerfallWorkshop.AudioSynthesis.Synthesis.SynthParameters)
extern void Voice_Configure_mBA82ADC3E8D74554E84FEF95363DFB9343FEE6F8 (void);
// 0x00000183 System.String DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice::ToString()
extern void Voice_ToString_mE732283D32E218AF05F567D5EF879665199EB580 (void);
// 0x00000184 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::.ctor(System.Int32)
extern void VoiceManager__ctor_mF7DAE448488693FE2E641B7351FE9BC4A49850DF (void);
// 0x00000185 DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::GetFreeVoice()
extern void VoiceManager_GetFreeVoice_mAA1AE9A540D670DCE54385FBE800AB099AEB3BF5 (void);
// 0x00000186 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::AddToRegistry(DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice)
extern void VoiceManager_AddToRegistry_m2055B93BCC9715FEE1E9D8D2C5E412F2EAD416F6 (void);
// 0x00000187 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::RemoveFromRegistry(System.Int32,System.Int32)
extern void VoiceManager_RemoveFromRegistry_mA5518367D8A75DE5B022440CE4B55CB75E502DE3 (void);
// 0x00000188 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::RemoveFromRegistry(DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice)
extern void VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079 (void);
// 0x00000189 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::ClearRegistry()
extern void VoiceManager_ClearRegistry_mE7A9A5542422604EFAE47D0D4AF3B4D0A2F7B096 (void);
// 0x0000018A System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::UnloadPatches()
extern void VoiceManager_UnloadPatches_mE8C788BCE71CD06511BDD72292BA1339E7CB27D2 (void);
// 0x0000018B DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::StealOldest()
extern void VoiceManager_StealOldest_m2DC42C690A28F3065FD16CB8E11DF2FCEC9EE24B (void);
// 0x0000018C DaggerfallWorkshop.AudioSynthesis.Synthesis.Voice DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager::StealQuietestVoice()
extern void VoiceManager_StealQuietestVoice_m0B1D16AD611B24B483268771F1D4D0F19AA0F826 (void);
// 0x0000018D System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceManager_VoiceNode::.ctor()
extern void VoiceNode__ctor_mC30870DD5AAC5E8AC4D00ED2ADD82BD739D35BE1 (void);
// 0x0000018E System.Single DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::get_CombinedVolume()
extern void VoiceParameters_get_CombinedVolume_m94289D4D075359DB54ABF1CC96F2F6B5F84EF004 (void);
// 0x0000018F System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::.ctor()
extern void VoiceParameters__ctor_m7778D2A8080260881ACA92965AC75360900DF955 (void);
// 0x00000190 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::Reset()
extern void VoiceParameters_Reset_mDA734937FFF5A02839B368F183E970EA3EF2AD88 (void);
// 0x00000191 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::MixMonoToMonoInterp(System.Int32,System.Single)
extern void VoiceParameters_MixMonoToMonoInterp_mE3ED349806D42205C944668D14C4252B4913FE14 (void);
// 0x00000192 System.Void DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::MixMonoToStereoInterp(System.Int32,System.Single,System.Single)
extern void VoiceParameters_MixMonoToStereoInterp_mB043AACA2FEDAA23A140CD498739F70FA9CC6583 (void);
// 0x00000193 System.String DaggerfallWorkshop.AudioSynthesis.Synthesis.VoiceParameters::ToString()
extern void VoiceParameters_ToString_mD01B9CC55062092042898C61E8827A72819ECEEF (void);
// 0x00000194 System.Char[] DaggerfallWorkshop.AudioSynthesis.Util.IOHelper::Read8BitChars(System.IO.BinaryReader,System.Int32)
extern void IOHelper_Read8BitChars_mAEB63E4D825338A17108F877C66C8C4FF0AEA2D7 (void);
// 0x00000195 System.String DaggerfallWorkshop.AudioSynthesis.Util.IOHelper::Read8BitString(System.IO.BinaryReader,System.Int32)
extern void IOHelper_Read8BitString_mAF56EB825FE6028F67A7845ABD9C6ED5FEEA6ED1 (void);
// 0x00000196 System.String DaggerfallWorkshop.AudioSynthesis.Util.IOHelper::GetExtension(System.String)
extern void IOHelper_GetExtension_m9AB3F3AEC7DBC8BDFA94AA1912EE65154307D930 (void);
// 0x00000197 System.String DaggerfallWorkshop.AudioSynthesis.Util.IOHelper::GetFileNameWithExtension(System.String)
extern void IOHelper_GetFileNameWithExtension_m92281328174A26D725727D6FD169F10D9B6B1E88 (void);
// 0x00000198 System.String DaggerfallWorkshop.AudioSynthesis.Util.IOHelper::GetFileNameWithoutExtension(System.String)
extern void IOHelper_GetFileNameWithoutExtension_m477CE8D5C15583C9C733B4F21F16C67EBDBCBD41 (void);
// 0x00000199 System.Int16 DaggerfallWorkshop.AudioSynthesis.Util.BigEndianHelper::ReadInt16(System.IO.BinaryReader)
extern void BigEndianHelper_ReadInt16_m0CE648EEC3EF70CF37236613C3F9EECF7D833722 (void);
// 0x0000019A System.Int32 DaggerfallWorkshop.AudioSynthesis.Util.BigEndianHelper::ReadInt32(System.IO.BinaryReader)
extern void BigEndianHelper_ReadInt32_m7B02A98448EF2C7A0875DE69BFBE8EF71E17971C (void);
// 0x0000019B System.String DaggerfallWorkshop.AudioSynthesis.Util.Riff.RiffTypeChunk::get_TypeId()
extern void RiffTypeChunk_get_TypeId_mBA0CCB0CB2A8DCA6856FD5EB7007C19DB003FBFE (void);
// 0x0000019C System.Void DaggerfallWorkshop.AudioSynthesis.Util.Riff.RiffTypeChunk::.ctor(System.String,System.Int32,System.IO.BinaryReader)
extern void RiffTypeChunk__ctor_mA5F9CF569D23C38A6B58DC4279147CB7F24D915F (void);
// 0x0000019D System.Void DaggerfallWorkshop.AudioSynthesis.Util.Tables::.cctor()
extern void Tables__cctor_mCCC5D545C915A3798B31D354AE06708823EDCB6A (void);
// 0x0000019E System.Double[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateCentTable()
extern void Tables_CreateCentTable_mDA1EEA30489EB4C540F4F76AE4D8D946138D6F45 (void);
// 0x0000019F System.Double[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateSemitoneTable()
extern void Tables_CreateSemitoneTable_m8A0B5078A2F897C4D3C6A3538248C6F0CF41C072 (void);
// 0x000001A0 System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateSustainTable(System.Int32)
extern void Tables_CreateSustainTable_mAFC5D677E0BB7E15AD4E03BC43A0AB78C9F65BF0 (void);
// 0x000001A1 System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateLinearTable(System.Int32)
extern void Tables_CreateLinearTable_mE8A52196591057548DDA3DE71766B8E8423D5427 (void);
// 0x000001A2 System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateConcaveTable(System.Int32)
extern void Tables_CreateConcaveTable_m8DA88814E45FAB3F4ECCCA237BDEDB9AE8A68EC9 (void);
// 0x000001A3 System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::CreateConvexTable(System.Int32)
extern void Tables_CreateConvexTable_m805C0608C71A722855983AFAEF2DEA8F689C5590 (void);
// 0x000001A4 System.Single[] DaggerfallWorkshop.AudioSynthesis.Util.Tables::RemoveDenormals(System.Single[])
extern void Tables_RemoveDenormals_mC91B0A68C891D1245952E94A24A2E79A61F32BC1 (void);
// 0x000001A5 System.Int32 DaggerfallWorkshop.AudioSynthesis.Wave.PcmData::get_Length()
extern void PcmData_get_Length_m7C10AF58A80EEEB00719D811BCAC16D45129C153 (void);
// 0x000001A6 System.Void DaggerfallWorkshop.AudioSynthesis.Wave.PcmData::.ctor(System.Int32,System.Byte[],System.Boolean)
extern void PcmData__ctor_mBC1ED2B9C753C402476F9E6A0136C7593A5FD788 (void);
// 0x000001A7 System.Single DaggerfallWorkshop.AudioSynthesis.Wave.PcmData::get_Item(System.Int32)
// 0x000001A8 DaggerfallWorkshop.AudioSynthesis.Wave.PcmData DaggerfallWorkshop.AudioSynthesis.Wave.PcmData::Create(System.Int32,System.Byte[],System.Boolean)
extern void PcmData_Create_m525A3F13F7472943B3C84631F50533DD4C51C64D (void);
// 0x000001A9 System.Void DaggerfallWorkshop.AudioSynthesis.Wave.PcmData8Bit::.ctor(System.Int32,System.Byte[],System.Boolean)
extern void PcmData8Bit__ctor_mCE6E9AA48F5B838607D5424D1BB44DE3E5E7A04D (void);
// 0x000001AA System.Single DaggerfallWorkshop.AudioSynthesis.Wave.PcmData8Bit::get_Item(System.Int32)
extern void PcmData8Bit_get_Item_mC0FD831C54F4E195A6596BE2D6AB7A3D40FB38F2 (void);
// 0x000001AB System.Void DaggerfallWorkshop.AudioSynthesis.Wave.PcmData16Bit::.ctor(System.Int32,System.Byte[],System.Boolean)
extern void PcmData16Bit__ctor_m87C39D32AD69C4F6DA59886B13938496533846B7 (void);
// 0x000001AC System.Single DaggerfallWorkshop.AudioSynthesis.Wave.PcmData16Bit::get_Item(System.Int32)
extern void PcmData16Bit_get_Item_m474E919DA06E2D9AFD2D805370D628E4730647D8 (void);
// 0x000001AD System.Void DaggerfallWorkshop.AudioSynthesis.Wave.PcmData24Bit::.ctor(System.Int32,System.Byte[],System.Boolean)
extern void PcmData24Bit__ctor_m731009DE56F1B4458DD51852B61378E55CBD63EE (void);
// 0x000001AE System.Single DaggerfallWorkshop.AudioSynthesis.Wave.PcmData24Bit::get_Item(System.Int32)
extern void PcmData24Bit_get_Item_m0EB862C540FAB8358256B9FC59BF2DF77CE09D72 (void);
// 0x000001AF System.Void DaggerfallWorkshop.AudioSynthesis.Wave.PcmData32Bit::.ctor(System.Int32,System.Byte[],System.Boolean)
extern void PcmData32Bit__ctor_m7FF8F71267EEE04BFE73D9BBB079E16040F3849D (void);
// 0x000001B0 System.Single DaggerfallWorkshop.AudioSynthesis.Wave.PcmData32Bit::get_Item(System.Int32)
extern void PcmData32Bit_get_Item_mFDCA4FF1B9431FE3857820675608EFDAC98EA0EA (void);
// 0x000001B1 System.Byte[] DaggerfallWorkshop.AudioSynthesis.Wave.WaveHelper::GetChannelPcmData(System.Byte[],System.Int32,System.Int32,System.Int32)
extern void WaveHelper_GetChannelPcmData_m5EB36FDDF8E6ACF093946271CBD782402302FFD5 (void);
// 0x000001B2 System.Void DaggerfallWorkshop.AudioSynthesis.Wave.WaveHelper::SwapEndianess(System.Byte[],System.Int32)
extern void WaveHelper_SwapEndianess_mB6A4A015BA00E24FB9527DD87B2AA23FB10AB9AC (void);
static Il2CppMethodPointer s_methodPointers[434] = 
{
	AssetManager_get_PatchAssetList_m4919F86B2396C783FA88855454D70940882D7CFD,
	AssetManager_get_SampleAssetList_m5C1C19CD8C2FC38258837789DC69A010AB1AA5B0,
	AssetManager__ctor_m3DE927F316B8ABF92AB42FF6E5B7EB5EF265455B,
	AssetManager_FindPatch_mC3680C40A08DF1F75C08EF60F5CA98EA5AD52276,
	AssetManager_FindSample_mA6AF930A3D579071BFAE104F0375D5C203C6E094,
	Envelope_get_Value_mFDADAC3A6E03234E53E59700435377B263B983CD,
	Envelope_get_CurrentState_mFC9436FB9F34981B22FC85BB1E283C1805B6CBCC,
	Envelope_get_Depth_mDA2A2B8691ADCCB730E03126EC0EA46034C86413,
	Envelope_set_Depth_m1B8EE21D4652A1C613777537AEDFBE1DE9C6DE6F,
	Envelope__ctor_mC790D05C10CDD1066A2347F220978EC7C3D812CA,
	Envelope_QuickSetup_mF7A54B3456A2497291870E46DCDF7BA9B51905D6,
	Envelope_QuickSetupSf2_mB89B546A9E220C2625D3641F2F2A63D10A12C888,
	Envelope_Increment_m5F0B17426E7218A1693E40085D66747442D18965,
	Envelope_Release_mBC0AB141572D1D9E71FBB12AB17DBFDDC9C8D232,
	Envelope_ReleaseSf2VolumeEnvelope_mE59B635170F2A4B898DAC1D29D7D569E4EE476A5,
	Envelope_ToString_m013A148181B6E3E19C0F7E1061DC8B4C80887F7E,
	EnvelopeStage__ctor_m15449496B9957EF2C541D2448A55A51277EC840B,
	Filter_get_Cutoff_m56BFBCD4F5E209A0E00B73673A54F08CD92C5DC7,
	Filter_set_Cutoff_m22F345AF3C647CFB9292830BA5811A97E57C7A70,
	Filter_get_CoeffNeedsUpdating_mC873AFEC9997670D31C82FF76D4F61D7CCF9C109,
	Filter_get_Enabled_mAC11045571C09BE64B3E753986A10E549C27619A,
	Filter_Disable_m0C7618D592A126BA98DCA21C047F442B72E57100,
	Filter_QuickSetup_m24E1AC84855F774E1069551DF910ADEF7AD4E559,
	Filter_ApplyFilter_m8C8ADE4458BEAEB77B52E46F751A09D559BC6E3E,
	Filter_ApplyFilterInterp_mE961F8378D33EA0D7349F9107C0D919FC3402BF2,
	Filter_UpdateCoeff_m87AD063C42B19160403E7AA421741F4EA29DBAA3,
	Filter_ToString_mE1343E4A0D405094E1C81DBAF4B35D27F74079FF,
	Filter_GenerateFilterCoeff_m93B77FD2247A74319F4BEBD8FD77A2F6DB18A186,
	Filter__ctor_mF15108D7CC9AC5426630CDBB7A7BB87479FF0574,
	Generator_get_LoopMode_m1F777968BE450DE19B3315C8C8F409BBB7AEBE45,
	Generator_set_LoopMode_m47B37F1C21E8142C8CDD4CB3C4DB9754EE5BE2EA,
	Generator_get_LoopStartPhase_mBB8BED1D86068FF786052A45C5A4E3755F82C915,
	Generator_set_LoopStartPhase_m7500CCF2406B2308DEF55058CBE9B0DDD6DD3CAD,
	Generator_get_LoopEndPhase_mD59DC0FA802D7E65936742193516DE463F93C25A,
	Generator_set_LoopEndPhase_m104CD96C976F8D8CEA704638EB877FE4985E81EC,
	Generator_get_StartPhase_mD65594668CBD23FEC029335FE82CF89702F981DE,
	Generator_set_StartPhase_m73FD4319FC28DA3B907505A5CC8C600493529766,
	Generator_get_EndPhase_mEF0F456A45FD13C55E9B668131ACDC69A250FDE1,
	Generator_set_EndPhase_mE998A81E855DF8AC96190E65C0C8E7B94FE18C03,
	Generator_get_Offset_m0FB3FE9E48E325DF9C6C4D384061229661D77138,
	Generator_set_Offset_mFECCA21556A098DED1274F59939A40177C657046,
	Generator_get_Period_m5FBC957BC65D782563D188AEDFE85BDE1E805748,
	Generator_set_Period_m0DEFCF018BB1D21BA17CFF12A38789C7CDDE5816,
	Generator_get_Frequency_m561992C435661C92ED7CF77BE0C7F996112028FF,
	Generator_set_Frequency_m44B71B48AC001D46D7A086E133BA5C32922849E0,
	Generator_get_RootKey_m2807E9537D24D9141C63EA102AC5A48C35609192,
	Generator_set_RootKey_m0B3712DB717113A5EF57B67CECBD50D84094A750,
	Generator_get_KeyTrack_mDD560FC0453D03611853AB7C482CEF29AC8F6B35,
	Generator_set_KeyTrack_m418D6D0DBC29C8CDC187F2F967D41725CFF862EB,
	Generator_get_VelocityTrack_m6CA88D079501105FE8BB166845F29FCF6C0DADA8,
	Generator_set_VelocityTrack_m20826E907C686C42919C83E333DF3DF1D8EF8A4C,
	Generator_get_Tune_m11A9F8968AEFDC9A731AE3C07857C8AA5FA0733C,
	Generator_set_Tune_m92A8590A8AF8D40C16C657C5B492E20DF58A14E7,
	Generator__ctor_mBD9A3E5A89476277268E8BEBC0F17E5591A903A8,
	Generator_Release_mA1D24E06999AA0EFE658547005073356AF6AA904,
	NULL,
	NULL,
	Generator_ToString_mA4C7588F1531CE8887C9DF761FCB977D88BBFA64,
	Generator__cctor_mAEDA590698CF25590027ECACB3089BE0FEBB3278,
	GeneratorParameters_QuickSetup_m9AA4510E07A2A9F45EDBE27399364287E0CC6B6B,
	GeneratorParameters_ToString_m6ADC2698C34BFB7E7075C9528988FE237C579635,
	GeneratorParameters__ctor_m3236F25398ABFBF56DC2671F38892A2E11339D2A,
	SampleGenerator_set_Samples_m33E24F2CDB5E6377F65397F4D3158C5450DB6CA3,
	SampleGenerator__ctor_mC08D6D832885D13AC213CA5EE6211C57BD226DFB,
	SampleGenerator__ctor_m40133755356EAEE1AEBE92F470820DD8CB2FFFD4,
	SampleGenerator_GetValue_mCD2E584E108E5B8871E25396BFAB77261B05196B,
	SampleGenerator_GetValues_m41555268A0D83F7E000AFA6D9BD00AF13A198EE4,
	SampleGenerator_Interpolate_mF1949CB6F648886527630E49C380224504DD5CDC,
	SawGenerator__ctor_m2787DDB2CDC0C2913D522FB32F6D6EEE43FE1D75,
	SawGenerator_GetValue_mECBED93E5F92207F3242CB60D9A89DB05F440D1C,
	SawGenerator_GetValues_m33F509704941B8E106F7BF690C9A23ACF9CCB2E8,
	SineGenerator__ctor_m16939292F31A4C269D20D0F551CFABDFC1B17AA4,
	SineGenerator_GetValue_m008504840217B0D3F6104E682DD4A1E7681E9EA1,
	SineGenerator_GetValues_mE768ABA0E21C2389BE341CD0758C48F58C4022B9,
	SquareGenerator__ctor_mA92ED26FD608B9BCB0ABB3C7301956A490E75761,
	SquareGenerator_GetValue_mC0B58C49002D6C70197F1441E663A26ADB156F5D,
	SquareGenerator_GetValues_m917D82D8B7EC0FC6DCFB2100C07EC85602F63081,
	TriangleGenerator__ctor_m1922B9805C5F5B03370052B44309CC04E2C62743,
	TriangleGenerator_GetValue_mAE8FFCF40053D2C226AA07F9091268DD787BCCAB,
	TriangleGenerator_GetValues_mD60A13DC9FC0A17E9E83426BD83E3047A15E6470,
	WhiteNoiseGenerator__ctor_mFA126753A3BAA805CA77B1B93A962EAF9B61A8D3,
	WhiteNoiseGenerator_GetValue_m444D451C4A8B7B12460EF17BCC2E6FABC9C1B49C,
	WhiteNoiseGenerator_GetValues_m7ABC9C1794FADE89766A90495F6B75598FBD9316,
	WhiteNoiseGenerator__cctor_m2692BF76CDB9FA3654951A44D17441BC49B11655,
	Lfo_get_Value_mBBCE09F004227DB330042D9335E6861B05C9CC73,
	Lfo_get_Depth_m07C75821EC6E85EE398C032E3008C7B582F00ACB,
	Lfo_set_Depth_m98031B7CFB0D73F8AA70523A27E1DE80AA5186F7,
	Lfo_QuickSetup_mACA8407FBB32CF6906A2A9652939865D6FB0A057,
	Lfo_Increment_m674CDC38AE1908079ABF66375EAB9E6D2608BE9D,
	Lfo_Reset_m8D62D36CFD3DE0E3A8ABC987B7C15A9563546C2A,
	Lfo_ToString_mBBF8E1BDF3C31F469FAA67F1F8327B4B44D88632,
	Lfo__ctor_m35CD7DD1CD4D7AA08749FC169FB112E7D95A1D10,
	DescriptorList__ctor_mBBA43A85CFF244D7F11081AE11709B1A31281AD7,
	DescriptorList_FindCustomDescriptor_m8BF0CB4B7776664ECBBD70701171D2E976474B50,
	CustomDescriptor_get_ID_m33EA8FA29176F78286A3CB0DF253E8FB99B941D6,
	CustomDescriptor_get_Objects_mDBD4CC738BEC0F613D8CA4DBD41C823A1B228914,
	CustomDescriptor__ctor_mEE5D3AF3DA3107D0D0E6CEDDF1ED43822DBCEFD4,
	CustomDescriptor_Read_m618B76D4CC5E1DF448C536542CE46BD3B74835AE,
	EnvelopeDescriptor__ctor_m4686B23FE89756116F2F9ACE0A162DE10DE157A3,
	EnvelopeDescriptor_Read_mCCDEBFD87DA449E81749FBBDF61E67EDB9354295,
	EnvelopeDescriptor_ApplyDefault_m28D8929F39B7F0C4E07E56DF47147B1D6564295B,
	EnvelopeDescriptor_CheckValidParameters_m61C0E3FDB1F9DEEB35949D53D7C93B582FD5A995,
	FilterDescriptor__ctor_m0888273C225EC37D44DFBE0D561E64EFA622B1E5,
	FilterDescriptor_Read_mA7C076B909910AF7D67CCC40426A1355387773EC,
	FilterDescriptor_ApplyDefault_m81F58AED83CCB64AB149510BD224D434712A30BD,
	FilterDescriptor_CheckValidParameters_mAD0E570DA02D12B80E455C6FAB260088271D0426,
	GeneratorDescriptor__ctor_mF6C0A30B3C6BCF94813054257D6A8CD89A0D2263,
	GeneratorDescriptor_Read_m2974F881034D62C83496FE386359641B4F650570,
	GeneratorDescriptor_ToGenerator_mF93D7C0CF1BA52ACDB92014ED269AF14D32F313D,
	GeneratorDescriptor_ApplyDefault_m6126E2B7BC3385D700A99109AD04B0C711E41844,
	LfoDescriptor__ctor_mB27DAC07B6F12A9B84F1AE6147F682E669206A61,
	LfoDescriptor_Read_m3F7DCE22A9C534E9BB3D0D3C64647A014E48D169,
	LfoDescriptor_GetGenerator_m6396A58AF13C080B7AF06BDEE77E3F112DA1990C,
	LfoDescriptor_ApplyDefault_m31D4D48F19A2500183B5539CAFEECB7A6696A03A,
	LfoDescriptor_CheckValidParameters_m53325290FDE98F62B54A2D2C64A3FAEA90ECF007,
	PatchAsset_get_Name_m59FB69332A701034D8DD1B29E0B550C324EF45F2,
	PatchAsset_get_Patch_mDB2CC40B52B51FBCFF4D52DC708C5EA4806999EC,
	PatchAsset__ctor_mF2483F4EB0D7C259E455A5F8107763F7B297DAD7,
	PatchAsset_ToString_m3FF553301638B3115E303E35DA16DC9018789F0E,
	PatchBank__cctor_m6096DF18F4605E74C3159110FCCA29EC6836C0F4,
	PatchBank_ClearCustomPatchTypes_mCCC7BC60B8F74A05D2D803029FC010BD4F3D1280,
	PatchBank__ctor_mE5CC8AFFF8E92DD3C8037AF1BC76B76C8FDF4E0B,
	PatchBank_LoadBank_mB32C6DA5FD41B5EABFECEA743A05934D5B2CA275,
	PatchBank_GetPatch_mC86BDB6F5B4657124EE0CD4474172176A4C38FDF,
	PatchBank_IsBankLoaded_m8C02279495AA8D73EDA721748226D7C4058E188E,
	PatchBank_LoadMyBank_m521A460A87D5DDC235D0B007CA3563834D155C5F,
	PatchBank_LoadSf2_m0B987D7D80123E5ABB1B81F07033184D5F535D9F,
	PatchBank_ReadSf2Instruments_m479EFBF4F8A35829AE11D2CE1799E36BB12D4DBD,
	PatchBank_ReadSf2Region_m3C3B6775F01EAD0AC85A5A17F86136B20FB8BB0B,
	PatchBank_AssignPatchToBank_mF647B2713BC87ED1CB54D5D577B92216913006DB,
	Patch_get_ExclusiveGroupTarget_mC9F49AFFCEF49C1A5A40D5D87D7A9ABF89FC1AE9,
	Patch_get_ExclusiveGroup_m2E5B9F814EB6C23F1464473925B9DA97EC6529F6,
	Patch_get_Name_m2536C44C9116E695C9BBE81DD5FF0A1A11DDC3B9,
	Patch__ctor_mB4BDDD7E1B0F2F9E0E53615FAD31C03457C44968,
	NULL,
	NULL,
	NULL,
	NULL,
	Patch_ToString_m3966C5B5B18EABFA7D0FE8B37D382514AEE4BA3E,
	BasicPatch_Start_m457B8CF1EF2F76C2CD57BB8039D4312657C41697,
	BasicPatch_Stop_mA550D006184AC8A9F70245FDEA3349BEAFA1F45C,
	BasicPatch_Process_mB22892089664D3B1FB462DF6915683B9E35BADE8,
	BasicPatch_Load_m49E7D82991C29B6AADE6260FEF9C17F7BC17AF71,
	Fm2Patch_Start_mF3C4B8394BBB6C1E317B94DED0D353BA94EF6CF6,
	Fm2Patch_Stop_m499E1FDE483B982F0ECFB13DB106B8079853CE1F,
	Fm2Patch_Process_mB2DCB5F80F73E14EB2AB71C9ED90C0BCE92178D9,
	Fm2Patch_Load_m3D9677BE78B989C89A657A2FC6EE16C5C595F50D,
	Fm2Patch_GetSyncModeFromString_m4D8F0DF7EA5575636716D0A27895AE460232B669,
	MultiPatch__ctor_m9B143F78D2E97970C6FD0F2AEF0E6C44F5E6E89A,
	MultiPatch_FindPatches_m86E92FB9B2C65E94311DD2389C67152F477741EE,
	MultiPatch_Start_m051BEFA1324C3EB9F79980EF80D55E82A5E36A64,
	MultiPatch_Stop_m6D37D02FB90C72B9752F2F701B235C4EA54F2EDB,
	MultiPatch_Process_m86E732F414C87F1576BCF0C80D29E67F36888110,
	MultiPatch_Load_m9CF0FE343ED66CD76DE2DA425A72130376A37907,
	MultiPatch_LoadSf2_m816E0916550720B55AD945071915D7337CBC8CD5,
	MultiPatch_ToString_m9D5BECB5C7858B58ABBB928827EDC57EB4645F20,
	MultiPatch_DetermineIntervalType_m6A58251047CC6D2651CB9AFACFFC10F0877E48E3,
	PatchInterval__ctor_m5A583D82AC90E9F5308FFFC9A0206981179EF39F,
	PatchInterval_CheckAllIntervals_m9D366399E10C1D68E62EAC9F60371C2C70FF5E94,
	PatchInterval_CheckChannelAndKey_mACC1DB3C02A06917029FEB385B68112D0BC507F0,
	PatchInterval_CheckKeyAndVelocity_m86E567491485B761A62EF162FE12B924FCF28086,
	PatchInterval_CheckKey_mC0CFD05F2651654CE790B017FDA5294086F2749C,
	PatchInterval_ToString_m9E72F8C7309019A9B214AC620B39E94B8F5EAF9D,
	Sf2Patch__ctor_m34F0B9831C7C64AD8C752200761646D2C396D97B,
	Sf2Patch_Start_mCA0629C86F14D0399D599E34A62EEAEF02C9E8D5,
	Sf2Patch_Stop_m5ED8FCFAD57F8C91FE47803ED8D980041233C137,
	Sf2Patch_Process_mB01B0921F9DD5BF2AD2B1F09C67FA18CB482692A,
	Sf2Patch_Load_m0C9C8B27C519097C4FE2BA414F835C0487EA92E5,
	Sf2Patch_Load_mE52F229D66295A5354946E0B935A69FEFAF5F20F,
	Sf2Patch_LoadGen_mFC4270F5FFE152E2FFA12F56AD7010D35054594A,
	Sf2Patch_LoadEnvelopes_mB3A39A6D057F2EC593D8E8281804D8B56E1516E5,
	Sf2Patch_LoadLfos_m0232755D160006A95E6495D58BB77C24E0695879,
	Sf2Patch_LoadFilter_m7A1C2704EEED571C55F87A323F91528F4D4DF590,
	Sf2Patch_CalculateModulator_mDE07DED84CDC2C9BC5A5B468D82FDB61DE245C19,
	SfzPatch_Start_m9944BD5780CF8070D9A82994ACEFDDF97A78D9E5,
	SfzPatch_Stop_m02DD085C8414BCA882C238276D874EED371F8BB0,
	SfzPatch_Process_m0A5EEC92F25DB69A3AF6AFE791C554EA744813CF,
	SfzPatch_Load_m411AE9DBC31344AE7ECC40DEACE459823B3B4172,
	SampleDataAsset_get_Name_mB5F813F5D5C554E57743DF4682E2DE94B2564E1E,
	SampleDataAsset_get_SampleRate_m72F5D41F212AE832F04796A93E7354CDCDB02147,
	SampleDataAsset_get_RootKey_m4EC201B4F7D75853CC38680AF422FEEA3DF61A45,
	SampleDataAsset_get_Tune_m4EF2AFA8266AF3189AB30006F89C857D9877CA89,
	SampleDataAsset_get_Start_m8C900602A9E1F49BC9F909CB869CE2226FBDB608,
	SampleDataAsset_get_End_m619927E328ECA85DEA2F49D98ED7E18C4AD03204,
	SampleDataAsset_get_LoopStart_mFE7455CBBD28B47161689F30C3DC499EF82C9E60,
	SampleDataAsset_get_LoopEnd_mB5502D6506AB406DF25F642E7B9BBE790E7B43D3,
	SampleDataAsset_get_SampleData_mDB001795EFAF87C2D9F2E8FCE3748217DB03719B,
	SampleDataAsset__ctor_m03CEC353DF8A76949130742CFF06FC5D3E18235D,
	SampleDataAsset__ctor_m02094D526978D18DCE060E2718D92A0DD80E56CA,
	SampleDataAsset_ToString_m5C136D7AA9017B153D91C75E50C377DA33C57F4E,
	NULL,
	NULL,
	NULL,
	MidiEvent_get_DeltaTime_m97C2F3FDBD30E16E0FCBA88A59289043D39B0C19,
	MidiEvent_set_DeltaTime_mCFB6BA7BF0A09287F63D746D453B9C3ACF44443D,
	MidiEvent_get_Channel_m746C48C59EADC05D90248D955AA3C3105E00CF80,
	MidiEvent_get_Command_mC30ADB45D3CA1A281C6741A19435DFCA2DBC35B1,
	MidiEvent_get_Data1_mD48E2705A950E3729D0A955E5442D4857FEF8686,
	MidiEvent_get_Data2_m9921858DB1EBB3286C25D4EBE6E4BA79911080BB,
	MidiEvent__ctor_mC48EC23C4103B22E1ABE6FCD2B2288E9EFCCA423,
	MidiEvent_ToString_m6B6F4232AB1B4F99F3EB7DE82F25FC76153C1E1C,
	MetaEvent_get_Channel_m5517AAB4E52A60D8ACA1FEF8CF4E59A9B4CF4390,
	MetaEvent_get_Command_m3136D62D72671C5036B32EB445B4395E67EAFEC4,
	MetaEvent__ctor_m217FEBC781DEF27B72CF041F85BA3094D874E3CE,
	MetaEvent_ToString_m46D27E742A9303DA09F1E32FD6A05C732E9740F2,
	MetaDataEvent__ctor_m085860E95DB1EBAC21DFB5F55414E540494AE646,
	MetaNumberEvent_get_Value_m3A4E94990267B14B4533C27AD080E888B9A3152F,
	MetaNumberEvent__ctor_m951D8E03733B413403D06EBFCC32ED4E3A097BCD,
	MetaTextEvent__ctor_mCEF54304664FCA060E16147B8A1F04EB4A7DED20,
	RealTimeEvent_get_Channel_mC79D78BDAED328E36C9B0C97BA3A9DFBA95CC3E3,
	RealTimeEvent_get_Command_m8C5165ED6A7B412B53577ACF6F414856BC7E0A7C,
	RealTimeEvent__ctor_m5BE4D0C0AB6D82FF6A7F66D39B565EE665D3ABE9,
	SystemCommonEvent_get_Channel_m6274962E4FFFB0C5821DFA421C5B8C6C40A3746A,
	SystemCommonEvent_get_Command_m30A25EC8C57C646000414E73CA7FBE545F84C894,
	SystemCommonEvent__ctor_m28761B5E8EB12382AEB313AFDA05F256C81D8598,
	SystemCommonEvent_ToString_m8DB3E6A0EDBB51469405DF4B6CE965AE75EE2CD5,
	SystemExclusiveEvent__ctor_m01B4CA589EF833C8271C11978B0C27EA06F211BC,
	MidiFile_get_Division_mA6875C36607123C23EA227F186188E056ED05D5E,
	MidiFile_get_Tracks_m10D22CE5FFDD6DFC6B375BC6D0AA52D4C170B3DF,
	MidiFile__ctor_m37364DFD91D1C6C52AA2172C7D6F74D14F36C28D,
	MidiFile_CombineTracks_m2050F8CF6EDDE6B264C91CC5C28F54949045D2A3,
	MidiFile_MergeTracks_m27DC4470C42B91A050847D997F0E686A0183DA8A,
	MidiFile_LoadStream_mCE8F1A3F13DCB71E996C3114CFAEAFB0B1A64360,
	MidiFile_ReadHeader_mCA741D6ACF8EA655854CDAE185EA1AC6521B0B9D,
	MidiFile_ReadTrack_m567C17DC36129CD32E1B8B724F3437F547DC734A,
	MidiFile_ReadMetaMessage_mF336B48E2AEA27F8D9DC32E89C9F70E269B4BF4D,
	MidiFile_ReadRealTimeMessage_m237D704C9BC54F13322A622CD960B63CC4F20F45,
	MidiFile_ReadSystemCommonMessage_mBEA607FB23C4362BA692FA3621CE3B1A647F279B,
	MidiFile_ReadVoiceMessage_mC1A4B09A621D3C770ED07E0EB3165C5D567AD540,
	MidiFile_TrackVoiceStats_m833D32DFAA958A7609FB0BFC4835648B4DDF86DB,
	MidiFile_ReadVariableLength_m1E07A05BEBF4C71494AECB08A8B0E8340AB55E47,
	MidiFile_ReadString_m94AF20C60D01CF9127B48F3632514F143F2890EE,
	MidiFile_FindHead_m33101C32BEEF7CF946CC7880149C68C3ACE78020,
	MidiTrack_get_NoteOnCount_m2CD1532D935D387E49A6B4A698908830F75446C1,
	MidiTrack_set_NoteOnCount_m5D0F587CCFA51735C402C7E551435337BAA20A36,
	MidiTrack_get_EndTime_m0A9531D9560D6F05B95CD4C4D78BD0BD3AECD77D,
	MidiTrack_set_EndTime_m3C08B0AE73980F90F5793FD8111812A671EA8D5C,
	MidiTrack_get_ActiveChannels_mDC38C6D57ECCBAE6EAEC6276715FA1D8408FC0BF,
	MidiTrack_set_ActiveChannels_m2EEE759B29507306B600E1B5AA161C9E8447E56C,
	MidiTrack_get_MidiEvents_mA994C8B330229D4BB96FB270B0357D9C08193847,
	MidiTrack_get_Instruments_m9D7658AE0405A3D35A98D579F90B1E68F336EEB1,
	MidiTrack_get_DrumInstruments_m3321A11FDF950A298B87D0E1F0ABB02C625B12A5,
	MidiTrack__ctor_mCA651C5B7988495506EE919D365C7CAED9DE925D,
	MidiTrack_ToString_m62A0DBCF60018C8A545C6DB13696B8EDA894746C,
	MidiFileSequencer_get_IsPlaying_m3690B94C7D26581B9C5D934A51FD4AA5D9A94AC9,
	MidiFileSequencer_get_IsMidiLoaded_m3CA214B55D7EE7080D21C782AB749FCF8C657B53,
	MidiFileSequencer_get_CurrentTime_m0AB689D395D3E121721C93F0FB94C432D4303067,
	MidiFileSequencer_get_EndTime_m356E5D952A0F60A1E1597E2BD783F15AC52C5D0B,
	MidiFileSequencer__ctor_m182E67FCD95540D20D99CE683C45052184BEEFF4,
	MidiFileSequencer_LoadMidi_m287ABEF78AF528CB674CDCD0DCAD0DD49409CB4E,
	MidiFileSequencer_Play_mBF3A6D5A67C829C8EBD646104C2A2C064036F8CE,
	MidiFileSequencer_Stop_m80533BAD37D6E05AAA8021F98EB43742E4AD7164,
	MidiFileSequencer_FillMidiEventQueue_m583982C5D32351F12F9BA82102D29C2A04B22F6C,
	MidiFileSequencer_LoadMidiFile_mEC394F5310157D05AFC418EF0886D3786C98E8F9,
	Chunk__ctor_m09D15006E79DF3FA8C2E92747839186ECB2661A4,
	GeneratorChunk_get_Generators_m995FDB9DC64121C8EF7A0173E12F561C8C133A6B,
	GeneratorChunk__ctor_m84365B0F70A8E62D84F6E5C8EC7C6D0418C22CBC,
	InstrumentChunk__ctor_m613700DD9AC753A5DBD3A3D2B23A079EDE30C869,
	InstrumentChunk_ToInstruments_m2AC461F8B733FFE32897CCFE3506AEF117870BA8,
	RawInstrument__ctor_mE9AF8C41617F181C318B00E034BD93698945A831,
	ModulatorChunk_get_Modulators_m39332FE630A9EE3DD1EE0CE550A440003257DDC6,
	ModulatorChunk__ctor_mFB66610A327D5B5B9C5895DA9F488D2E88781957,
	PresetHeaderChunk__ctor_m0F2D9995AA2B7EEAA9FC7FF8EAE3D1441D58B0F3,
	PresetHeaderChunk_ToPresets_m42674778BE988CAA5E368F33364F6D0D12A1E9A1,
	RawPreset__ctor_mB2ED8A0F79A1CC0B9A6038176BFB6FEF4494E7E4,
	SampleHeaderChunk_get_SampleHeaders_m640AF6A68DA7BE215F57BD230259B0D1083C139B,
	SampleHeaderChunk__ctor_m56E521C2D2506E8FF189B43BF3CAA1A4C058A1AC,
	ZoneChunk__ctor_m4A647A21B1CB9F0430922669AF13D0A66539C513,
	ZoneChunk_ToZones_m24160C8D66DBB5B4D8322607AE687113B21EB205,
	RawZoneData__ctor_mC6A341B8DBB243161F41FA43121208581B74605D,
	Generator__ctor_m563F2E3AE03C058CEF6501765FE05D705B9F4A01,
	Generator_get_GeneratorType_m0C291F4777285D205B56432D86F68E9C18976DAD,
	Generator_get_AmountInt16_m772159529C599F06171652F7E49C5DB9781FEFF9,
	Generator_ToString_m9B969188C843C16A82908ED1169913D91D396138,
	Instrument_set_Name_m668B546F792570A9FF0449EA56F4F4030A3F1813,
	Instrument_get_Zones_m3DE84F9EE84E517D687DFAF3188ECFD32088BFA9,
	Instrument_set_Zones_mEA995312D785A38DDEA62C31C0CD3EE3D820F4E7,
	Instrument_ToString_mF543B3F862F3077804D70CF8FEDFAAA9D9061EB9,
	Instrument__ctor_m6DDD5A89CB963B188C360CB6CFF8981777A58C2A,
	Modulator__ctor_mAE34CAFA1779EEDA2C574F470A32F42A0AD96A56,
	Modulator_ToString_m0B470B15DD6D0442D2D0182E943F260149C45CAF,
	ModulatorType__ctor_m7BDBD5C387B89EF90D6DB2F0AD6F8865CB5DC7CD,
	ModulatorType_ToString_m6E6EA4E694E3F5F6EBD4F34FB6F6A3435AA23613,
	PresetHeader_get_Name_m4387DB61C972D6385E8DECCEBBDB8A4971A5316F,
	PresetHeader_set_Name_m7413D3AB365F9FBCF30F3B3F6BEB66E0B84C5438,
	PresetHeader_get_PatchNumber_m85ACEA1182B25537CA0E66AB83D88593918F191B,
	PresetHeader_set_PatchNumber_mCF27F8B5C4A2E2680179DB9C37A27891E48B3C63,
	PresetHeader_get_BankNumber_m051EF27EE23F425052F72EBE34F1780B58C51375,
	PresetHeader_set_BankNumber_mD844D872D7B7E34E826CB5D29C98981235E2CA97,
	PresetHeader_set_Library_m995DA7156D2F40BA42EEEB78E9650B688E0FBF93,
	PresetHeader_set_Genre_m73494990E6EA80456B7DC25532FBBE4DFEF9BF42,
	PresetHeader_set_Morphology_m687D8D2FE3718FC4DD22FF999CA607BE63556C9C,
	PresetHeader_get_Zones_m15723AD367886748961F61C9356383E575881F65,
	PresetHeader_set_Zones_m72C3FE1482FB7AFABF0E854F4CBF6D51D041FD70,
	PresetHeader_ToString_m5E92AB4A4B3127F762F77B6EE554F5BABEF9BFEF,
	PresetHeader__ctor_m130ECD4566B469F09EEC647023B91D7F56150973,
	SampleHeader_get_Name_m4B7FA1213B10DAA8F8BD88423EC57F934CE80315,
	SampleHeader_get_Start_mE39326D8AC9AA8058D0653CF9BDDAACBC8ACD792,
	SampleHeader_get_End_mEEF30E298093DA62DB8830A05943D05ED0896C8D,
	SampleHeader_get_StartLoop_m856466629F8A38DD92763411CD037DB1161FEB6D,
	SampleHeader_get_EndLoop_m440CC51C81F2F50AFAA91B36329A8DF669937FFB,
	SampleHeader_get_SampleRate_m6701A223849FF436297F9A7619AEB8CAACBB4365,
	SampleHeader_get_RootKey_m4EFFDAF87F3DDA1706BD4C314BDC4563FF79047D,
	SampleHeader_get_Tune_m0432A2D9DCAAEB69F77920E99D733C1AAF338EAC,
	SampleHeader__ctor_mF5CE982CB4398C4C87CBA90B14C2DC5014DC50CC,
	SampleHeader_ToString_mBE513699B4D236F17B8F6D9444DF38CFEEA54702,
	Sf2Region_get_Generators_m688C8A6176FB485DA1917C5755A2DEAC3D825BE9,
	Sf2Region__ctor_mEA669A892C0F853AD2666A96986280CE63A598F6,
	Sf2Region_ApplyDefaultValues_m33EFEA2A4EB6CA14C82679CA07DDD2B9443F8039,
	SoundFont_get_Info_m359D1B7B33589A9BA45D0AF3150BBE0783818C6A,
	SoundFont_get_SampleData_mDCD7B4A6E683CE1B73DE09E1A36BCFD4564E0C8B,
	SoundFont_get_Presets_m9A955EC4019609BBCFF0EB82C9A39E1C055A70CC,
	SoundFont__ctor_m81BE9723A39D59EE81C0BA6BF31572B640823221,
	SoundFont_Load_mF6172B11DE4D7A83D3385E350421A34F535A2FE9,
	SoundFontInfo_get_BankName_mFBA41925C4F204A45AB07778510F232A0E06207D,
	SoundFontInfo_get_Comments_m59C2E16E2B0C61BD4DA5C3442703422D8235C4CF,
	SoundFontInfo__ctor_m907B995C74C74FD59700E847ABE1D263F767C4A1,
	SoundFontInfo_ToString_mF5F166AAF6FAFBCE3C45439C60972B1BD9769D68,
	SoundFontPresets_get_SampleHeaders_m1FF7DB75102B1862F455F434F08E6C3D03F65B20,
	SoundFontPresets_get_PresetHeaders_mCB6EA2C1033E700128E10DBA204070AA9CC13812,
	SoundFontPresets_get_Instruments_mF96F3A36C2776FC28987F2D2A668E7406F11B14F,
	SoundFontPresets__ctor_m6928685F8804077E4AF1736C53FAE8DD266D1619,
	SoundFontSampleData_get_BitsPerSample_mC0D8308DC30F0C2E5CB7ED087860254CD19A100D,
	SoundFontSampleData_get_SampleData_m28CAAB537EC75754A48FEECECFCA14849C205FB1,
	SoundFontSampleData__ctor_mCD737B974C250250F3D8E2F61699E940942C33F6,
	Zone_get_Modulators_m819BC5A415FDD098C62F176644BC7B832E9D65DF,
	Zone_set_Modulators_mCD8A63184ED534C3F54CB8C231624B866588FD75,
	Zone_get_Generators_m3BDD4F7A064D556CD8FD224F4828417D82EAA019,
	Zone_set_Generators_m051434F7E04B03839D9E45F687EF1D168E486E3D,
	Zone_ToString_m136116DA43FF3C8E3CB7CA0E9517D84815B05EAF,
	Zone__ctor_mFEE2F08A75809A1B25C605D48A2581E51922DF97,
	MidiMessage__ctor_mAD45EAB09928A792C10A4BE1937A00F6B4061EBD_AdjustorThunk,
	MidiMessage__ctor_m59BF4D6F32B5DA99D5DFC19D597E554864667A00_AdjustorThunk,
	MidiMessage_ToString_m055AEB532CF77AAEBAB7E9F7B91A1EA552E95651_AdjustorThunk,
	PanComponent__ctor_mD7FAB1EB204E99DA52960E149378CE90BF2846F3_AdjustorThunk,
	PanComponent_ToString_mF747C37B8CD9699F43E15DA4C8C342C86172388A_AdjustorThunk,
	CCValue_get_Coarse_m758E91200E1E2D2798B94B495612FF936A7CE0F9_AdjustorThunk,
	CCValue_set_Coarse_m5344D4D334F3D8D89B69019028B9DF82A2076649_AdjustorThunk,
	CCValue_set_Fine_mC5FE00B563D7B73BD9135AC48B16B2FFB5B65B6D_AdjustorThunk,
	CCValue_get_Combined_mE4C5B314DE167D020D5A07C59295911EF384E5E4_AdjustorThunk,
	CCValue_set_Combined_m6DF917177FB6541C7234B4247E4FCA809FAADE00_AdjustorThunk,
	CCValue_ToString_m57A63889A06F27A2A50581402258E27EEFBBAE6E_AdjustorThunk,
	CCValue_UpdateCombined_mBE04F2338665F67B2D3A5FFF057D5B703582279B_AdjustorThunk,
	CCValue_UpdateCoarseFinePair_mFD9A00C5E418B55566DC83315E5FA46158F9607F_AdjustorThunk,
	SynthHelper_Clamp_mE5F96B195032983A5C4642B4FCAE05F1766CAEDF,
	SynthHelper_Clamp_mABDAD8ED1DC346DC83659F2142445186C92D3120,
	SynthHelper_Clamp_m8900A1DC809E91761D20C656937E5EE6B04454F1,
	SynthHelper_Clamp_mBAC49A911FF04B30E21575B6C2E07A7B1F43C2BD,
	SynthHelper_DBtoLinear_m224252A9D521905D87410CE27FA28E548613E53D,
	SynthHelper_KeyToFrequency_m82BB6EF80E9FBE00A0FD57B67E794D9B01E6C9A4,
	SynthHelper_CentsToPitch_mDD57BCD740AA6583A7A5DB8F690AE9B03AE7CAB8,
	SynthParameters__ctor_mAAD2A21AC9314C750B70EA524E730F4235373711,
	SynthParameters_ResetControllers_m0027611CEA3F87605275C99E8640F89D27B896E8,
	SynthParameters_UpdateCurrentVolume_mF65FAC56F5D1EA02E14EA6FB0D4FBFBFE85C2C61,
	SynthParameters_UpdateCurrentPitch_mE15F0D19B7BE2079C6347066DA363D578A4FEB08,
	SynthParameters_UpdateCurrentMod_m8B938492578703025D6A3B5F01193E0D80B144E2,
	SynthParameters_UpdateCurrentPan_m08069E906C15598A7B038FB984A6D5D56B593090,
	Synthesizer_get_MicroBufferSize_mBD18E712321364ECB8646B7AB30FF00A03D65FF4,
	Synthesizer_get_WorkingBufferSize_mC0FF10DBED7D3B78209E74A42AB6303D62A151DE,
	Synthesizer_get_MixGain_m1834D1A1C4F51536F338A4C7AB998A4A359B14A3,
	Synthesizer_get_SampleRate_m600A7A592D514CD27764AF39B3586E022E6E2147,
	Synthesizer_get_AudioChannels_mFC9566B67AEF5AA6CB4F7742F8976A9E43AA42D2,
	Synthesizer__ctor_m5B2E9379382A890BBEA8FE575F2B4F01A8B83408,
	Synthesizer_LoadBank_m5A866814011F2BC24C0AAD5F3E6BF71FD7D40580,
	Synthesizer_LoadBank_mB0296667D3FDB5AB407923A080E4A6C0B9A5D783,
	Synthesizer_UnloadBank_m622C2A0C97C8AA07176FBFDC52E41D9CABE3FDA0,
	Synthesizer_ResetSynthControls_mD469453AF5D6CF0F67E9D50FDF7DB78DCBCF7D16,
	Synthesizer_ResetPrograms_m5EF661697DAC332F0B06B51195B76F63DDB5AD5F,
	Synthesizer_GetNext_mE08CF4F0DEA6A65D7D91E98A773E031DA317B15D,
	Synthesizer_FillWorkingBuffer_m53938F9A39846C0E6EC7A76C7A1DF39ABC640974,
	Synthesizer_ConvertWorkingBuffer_mBC00D122A20A8577DC0AEAACA2B701D114E498FF,
	Synthesizer_NoteOn_m04E2EFEF32FD9B4E13480059E411E37F26FD5772,
	Synthesizer_NoteOff_mAA9E92FBA1972D7413249B4C79CA46C064BEB2E8,
	Synthesizer_NoteOffAll_m09B0E9EA1805A76EDADE4E4E0443AFD9AB7FAA88,
	Synthesizer_ProcessMidiMessage_m068A780E4843F4C24833F1C6BDCAFD33D22728BC,
	Synthesizer_ReleaseAllHoldPedals_mAA8BF558958F700C063299A6DDC2D27B38F16239,
	Synthesizer_ReleaseHoldPedal_m8DD7E693144C996CC0EC5451B000B5F5C6203029,
	Synthesizer__cctor_m33EB9B17280769B6A4DD40FC6C6F28BE0E8A8CF2,
	Voice_get_Patch_m4758BB409337AC45CCFB570CF5283E155D725574,
	Voice_get_VoiceParams_m58A4429B13BBE75289320E6569306ED9F1B016C5,
	Voice__ctor_mC8F53C63FE90B4AC4517F3FB27EF8E2CF73ABA28,
	Voice_Start_mFD62C40C95388C7CA2E2677758C8F25A75BCF807,
	Voice_Stop_m8E10957D7FABBF4A40F8370F5D8D77EE39A64567,
	Voice_StopImmediately_m3675EECC2EF7522DE6BF677F419648631A557D21,
	Voice_Process_m7893B616DA247441F4231BC8EEE204B01C0D348C,
	Voice_Configure_mBA82ADC3E8D74554E84FEF95363DFB9343FEE6F8,
	Voice_ToString_mE732283D32E218AF05F567D5EF879665199EB580,
	VoiceManager__ctor_mF7DAE448488693FE2E641B7351FE9BC4A49850DF,
	VoiceManager_GetFreeVoice_mAA1AE9A540D670DCE54385FBE800AB099AEB3BF5,
	VoiceManager_AddToRegistry_m2055B93BCC9715FEE1E9D8D2C5E412F2EAD416F6,
	VoiceManager_RemoveFromRegistry_mA5518367D8A75DE5B022440CE4B55CB75E502DE3,
	VoiceManager_RemoveFromRegistry_m568472682371ED7008964FB024833B024635F079,
	VoiceManager_ClearRegistry_mE7A9A5542422604EFAE47D0D4AF3B4D0A2F7B096,
	VoiceManager_UnloadPatches_mE8C788BCE71CD06511BDD72292BA1339E7CB27D2,
	VoiceManager_StealOldest_m2DC42C690A28F3065FD16CB8E11DF2FCEC9EE24B,
	VoiceManager_StealQuietestVoice_m0B1D16AD611B24B483268771F1D4D0F19AA0F826,
	VoiceNode__ctor_mC30870DD5AAC5E8AC4D00ED2ADD82BD739D35BE1,
	VoiceParameters_get_CombinedVolume_m94289D4D075359DB54ABF1CC96F2F6B5F84EF004,
	VoiceParameters__ctor_m7778D2A8080260881ACA92965AC75360900DF955,
	VoiceParameters_Reset_mDA734937FFF5A02839B368F183E970EA3EF2AD88,
	VoiceParameters_MixMonoToMonoInterp_mE3ED349806D42205C944668D14C4252B4913FE14,
	VoiceParameters_MixMonoToStereoInterp_mB043AACA2FEDAA23A140CD498739F70FA9CC6583,
	VoiceParameters_ToString_mD01B9CC55062092042898C61E8827A72819ECEEF,
	IOHelper_Read8BitChars_mAEB63E4D825338A17108F877C66C8C4FF0AEA2D7,
	IOHelper_Read8BitString_mAF56EB825FE6028F67A7845ABD9C6ED5FEEA6ED1,
	IOHelper_GetExtension_m9AB3F3AEC7DBC8BDFA94AA1912EE65154307D930,
	IOHelper_GetFileNameWithExtension_m92281328174A26D725727D6FD169F10D9B6B1E88,
	IOHelper_GetFileNameWithoutExtension_m477CE8D5C15583C9C733B4F21F16C67EBDBCBD41,
	BigEndianHelper_ReadInt16_m0CE648EEC3EF70CF37236613C3F9EECF7D833722,
	BigEndianHelper_ReadInt32_m7B02A98448EF2C7A0875DE69BFBE8EF71E17971C,
	RiffTypeChunk_get_TypeId_mBA0CCB0CB2A8DCA6856FD5EB7007C19DB003FBFE,
	RiffTypeChunk__ctor_mA5F9CF569D23C38A6B58DC4279147CB7F24D915F,
	Tables__cctor_mCCC5D545C915A3798B31D354AE06708823EDCB6A,
	Tables_CreateCentTable_mDA1EEA30489EB4C540F4F76AE4D8D946138D6F45,
	Tables_CreateSemitoneTable_m8A0B5078A2F897C4D3C6A3538248C6F0CF41C072,
	Tables_CreateSustainTable_mAFC5D677E0BB7E15AD4E03BC43A0AB78C9F65BF0,
	Tables_CreateLinearTable_mE8A52196591057548DDA3DE71766B8E8423D5427,
	Tables_CreateConcaveTable_m8DA88814E45FAB3F4ECCCA237BDEDB9AE8A68EC9,
	Tables_CreateConvexTable_m805C0608C71A722855983AFAEF2DEA8F689C5590,
	Tables_RemoveDenormals_mC91B0A68C891D1245952E94A24A2E79A61F32BC1,
	PcmData_get_Length_m7C10AF58A80EEEB00719D811BCAC16D45129C153,
	PcmData__ctor_mBC1ED2B9C753C402476F9E6A0136C7593A5FD788,
	NULL,
	PcmData_Create_m525A3F13F7472943B3C84631F50533DD4C51C64D,
	PcmData8Bit__ctor_mCE6E9AA48F5B838607D5424D1BB44DE3E5E7A04D,
	PcmData8Bit_get_Item_mC0FD831C54F4E195A6596BE2D6AB7A3D40FB38F2,
	PcmData16Bit__ctor_m87C39D32AD69C4F6DA59886B13938496533846B7,
	PcmData16Bit_get_Item_m474E919DA06E2D9AFD2D805370D628E4730647D8,
	PcmData24Bit__ctor_m731009DE56F1B4458DD51852B61378E55CBD63EE,
	PcmData24Bit_get_Item_m0EB862C540FAB8358256B9FC59BF2DF77CE09D72,
	PcmData32Bit__ctor_m7FF8F71267EEE04BFE73D9BBB079E16040F3849D,
	PcmData32Bit_get_Item_mFDCA4FF1B9431FE3857820675608EFDAC98EA0EA,
	WaveHelper_GetChannelPcmData_m5EB36FDDF8E6ACF093946271CBD782402302FFD5,
	WaveHelper_SwapEndianess_mB6A4A015BA00E24FB9527DD87B2AA23FB10AB9AC,
};
static const int32_t s_InvokerIndices[434] = 
{
	14,
	14,
	23,
	28,
	28,
	761,
	10,
	761,
	339,
	23,
	1055,
	2323,
	32,
	339,
	23,
	14,
	23,
	466,
	340,
	89,
	89,
	23,
	2324,
	26,
	139,
	32,
	14,
	2325,
	23,
	10,
	32,
	466,
	340,
	466,
	340,
	466,
	340,
	466,
	340,
	466,
	340,
	466,
	340,
	466,
	340,
	251,
	617,
	251,
	617,
	251,
	617,
	251,
	617,
	26,
	26,
	1276,
	2326,
	14,
	3,
	26,
	14,
	23,
	26,
	23,
	27,
	1276,
	2326,
	2327,
	26,
	1276,
	2326,
	26,
	1276,
	2326,
	26,
	1276,
	2326,
	26,
	1276,
	2326,
	26,
	1276,
	2326,
	3,
	466,
	466,
	340,
	62,
	32,
	23,
	14,
	23,
	26,
	28,
	14,
	14,
	139,
	121,
	23,
	121,
	23,
	23,
	23,
	121,
	23,
	23,
	23,
	121,
	28,
	23,
	23,
	121,
	43,
	23,
	23,
	14,
	14,
	27,
	14,
	3,
	3,
	26,
	26,
	207,
	30,
	26,
	26,
	28,
	993,
	209,
	10,
	10,
	14,
	26,
	35,
	9,
	26,
	27,
	14,
	9,
	26,
	35,
	27,
	9,
	26,
	35,
	27,
	94,
	26,
	2328,
	9,
	26,
	35,
	27,
	27,
	14,
	23,
	2329,
	806,
	52,
	52,
	30,
	14,
	26,
	9,
	26,
	35,
	27,
	27,
	27,
	26,
	26,
	26,
	2330,
	9,
	26,
	35,
	27,
	14,
	10,
	251,
	251,
	466,
	466,
	466,
	466,
	14,
	62,
	27,
	14,
	89,
	14,
	14,
	10,
	32,
	10,
	10,
	10,
	10,
	2331,
	14,
	10,
	10,
	2331,
	14,
	2332,
	10,
	2333,
	2332,
	10,
	10,
	2331,
	10,
	10,
	2331,
	14,
	2334,
	10,
	14,
	26,
	23,
	14,
	26,
	26,
	0,
	710,
	710,
	710,
	2335,
	2336,
	94,
	0,
	106,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	14,
	14,
	214,
	14,
	89,
	89,
	10,
	10,
	26,
	9,
	23,
	23,
	23,
	26,
	139,
	14,
	116,
	116,
	28,
	23,
	14,
	116,
	116,
	28,
	23,
	14,
	116,
	116,
	114,
	23,
	26,
	10,
	251,
	14,
	26,
	14,
	26,
	14,
	23,
	26,
	14,
	26,
	14,
	14,
	26,
	10,
	32,
	10,
	32,
	32,
	32,
	32,
	14,
	26,
	14,
	23,
	14,
	10,
	10,
	10,
	10,
	10,
	89,
	251,
	26,
	14,
	14,
	23,
	23,
	14,
	14,
	14,
	26,
	26,
	14,
	14,
	26,
	14,
	14,
	14,
	14,
	26,
	10,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	23,
	85,
	2337,
	14,
	2273,
	14,
	89,
	31,
	31,
	251,
	617,
	14,
	23,
	23,
	2338,
	1741,
	183,
	2339,
	441,
	444,
	286,
	26,
	23,
	23,
	23,
	23,
	23,
	10,
	10,
	761,
	10,
	10,
	526,
	26,
	26,
	23,
	23,
	23,
	26,
	23,
	27,
	38,
	138,
	31,
	343,
	23,
	32,
	3,
	14,
	14,
	23,
	23,
	23,
	23,
	138,
	2340,
	14,
	32,
	14,
	26,
	138,
	26,
	23,
	23,
	14,
	14,
	23,
	761,
	23,
	23,
	1053,
	2341,
	14,
	124,
	124,
	0,
	0,
	0,
	246,
	94,
	14,
	116,
	3,
	4,
	4,
	43,
	43,
	43,
	43,
	0,
	10,
	1183,
	1626,
	2342,
	1183,
	1626,
	1183,
	1626,
	1183,
	1626,
	1183,
	1626,
	211,
	375,
};
extern const Il2CppCodeGenModule g_AudioSynthesisCodeGenModule;
const Il2CppCodeGenModule g_AudioSynthesisCodeGenModule = 
{
	"AudioSynthesis.dll",
	434,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
