﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.AssetBundle::.ctor()
extern void AssetBundle__ctor_mCE6DB7758AAD0EDDB044FC67C5BC7EC987BF3F71 (void);
// 0x00000002 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync_Internal(System.String,System.UInt32,System.UInt64)
extern void AssetBundle_LoadFromFileAsync_Internal_m5946F1D5D2D456570FA97C194593B4BB7BCBD290 (void);
// 0x00000003 UnityEngine.AssetBundleCreateRequest UnityEngine.AssetBundle::LoadFromFileAsync(System.String)
extern void AssetBundle_LoadFromFileAsync_m32FB2B292114D2ED2DCAF0170E9DE16B21E9DAD8 (void);
// 0x00000004 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromFile_Internal(System.String,System.UInt32,System.UInt64)
extern void AssetBundle_LoadFromFile_Internal_m27AD5A25C493DDF033E2C0EB8B5E73D1A498D3D9 (void);
// 0x00000005 UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromFile(System.String)
extern void AssetBundle_LoadFromFile_m326379558FA2CA731C294D5F9905EA3EAE3B5E52 (void);
// 0x00000006 System.Boolean UnityEngine.AssetBundle::Contains(System.String)
extern void AssetBundle_Contains_m00D431624B6594F6222D946F50A7DCA8559B1B44 (void);
// 0x00000007 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String)
extern void AssetBundle_LoadAsset_m598BB4E86B07C2BADAE6ED6FFBE5BB5E7A3392D5 (void);
// 0x00000008 T UnityEngine.AssetBundle::LoadAsset(System.String)
// 0x00000009 UnityEngine.Object UnityEngine.AssetBundle::LoadAsset(System.String,System.Type)
extern void AssetBundle_LoadAsset_m9139320F8B6D3E43B7D29AA7A60030306AE0A2C6 (void);
// 0x0000000A UnityEngine.Object UnityEngine.AssetBundle::LoadAsset_Internal(System.String,System.Type)
extern void AssetBundle_LoadAsset_Internal_mFB165539087545C4B5763BA8B590D84318C6FE1B (void);
// 0x0000000B UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String)
extern void AssetBundle_LoadAssetAsync_m033CE66F7C38A5A46C627004B0D3AD830C4CE4F3 (void);
// 0x0000000C UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_m63C7C5654FA8D0824DC920A1B1530C37CCB3DF6E (void);
// 0x0000000D UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync()
// 0x0000000E UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAllAssetsAsync(System.Type)
extern void AssetBundle_LoadAllAssetsAsync_m213D935C1F40DB2098F00BD852B5799F00542137 (void);
// 0x0000000F UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetAsync_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetAsync_Internal_m89D45EFB48D3C4CF7E481EF41F7ACF24500E7019 (void);
// 0x00000010 System.Void UnityEngine.AssetBundle::Unload(System.Boolean)
extern void AssetBundle_Unload_m0DEBACB284F6CECA8DF21486D1BBE1189F6A5D66 (void);
// 0x00000011 System.String[] UnityEngine.AssetBundle::GetAllAssetNames()
extern void AssetBundle_GetAllAssetNames_m7012B92C4E0BDF3975EBC22DE2515AFEB0E9D409 (void);
// 0x00000012 UnityEngine.AssetBundleRequest UnityEngine.AssetBundle::LoadAssetWithSubAssetsAsync_Internal(System.String,System.Type)
extern void AssetBundle_LoadAssetWithSubAssetsAsync_Internal_mD671A7674C5E61F00AF0127C2D92F56DD34E3DCB (void);
// 0x00000013 UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern void AssetBundleCreateRequest_get_assetBundle_m608C1516A7DC8E4B1F9D63EDCF6EE8D6C2CFF013 (void);
// 0x00000014 System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern void AssetBundleCreateRequest__ctor_m7B04FFC9566D1B0F820DDE9844BA2822A74881B6 (void);
// 0x00000015 UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern void AssetBundleRequest_get_asset_mB0A96FBC026D143638E467DEB37228ACD55F1813 (void);
// 0x00000016 System.Void UnityEngine.AssetBundleRequest::.ctor()
extern void AssetBundleRequest__ctor_mD09AF030644EF7F3386ABB3B5C593F61ADE25017 (void);
static Il2CppMethodPointer s_methodPointers[22] = 
{
	AssetBundle__ctor_mCE6DB7758AAD0EDDB044FC67C5BC7EC987BF3F71,
	AssetBundle_LoadFromFileAsync_Internal_m5946F1D5D2D456570FA97C194593B4BB7BCBD290,
	AssetBundle_LoadFromFileAsync_m32FB2B292114D2ED2DCAF0170E9DE16B21E9DAD8,
	AssetBundle_LoadFromFile_Internal_m27AD5A25C493DDF033E2C0EB8B5E73D1A498D3D9,
	AssetBundle_LoadFromFile_m326379558FA2CA731C294D5F9905EA3EAE3B5E52,
	AssetBundle_Contains_m00D431624B6594F6222D946F50A7DCA8559B1B44,
	AssetBundle_LoadAsset_m598BB4E86B07C2BADAE6ED6FFBE5BB5E7A3392D5,
	NULL,
	AssetBundle_LoadAsset_m9139320F8B6D3E43B7D29AA7A60030306AE0A2C6,
	AssetBundle_LoadAsset_Internal_mFB165539087545C4B5763BA8B590D84318C6FE1B,
	AssetBundle_LoadAssetAsync_m033CE66F7C38A5A46C627004B0D3AD830C4CE4F3,
	AssetBundle_LoadAssetAsync_m63C7C5654FA8D0824DC920A1B1530C37CCB3DF6E,
	NULL,
	AssetBundle_LoadAllAssetsAsync_m213D935C1F40DB2098F00BD852B5799F00542137,
	AssetBundle_LoadAssetAsync_Internal_m89D45EFB48D3C4CF7E481EF41F7ACF24500E7019,
	AssetBundle_Unload_m0DEBACB284F6CECA8DF21486D1BBE1189F6A5D66,
	AssetBundle_GetAllAssetNames_m7012B92C4E0BDF3975EBC22DE2515AFEB0E9D409,
	AssetBundle_LoadAssetWithSubAssetsAsync_Internal_mD671A7674C5E61F00AF0127C2D92F56DD34E3DCB,
	AssetBundleCreateRequest_get_assetBundle_m608C1516A7DC8E4B1F9D63EDCF6EE8D6C2CFF013,
	AssetBundleCreateRequest__ctor_m7B04FFC9566D1B0F820DDE9844BA2822A74881B6,
	AssetBundleRequest_get_asset_mB0A96FBC026D143638E467DEB37228ACD55F1813,
	AssetBundleRequest__ctor_mD09AF030644EF7F3386ABB3B5C593F61ADE25017,
};
static const int32_t s_InvokerIndices[22] = 
{
	23,
	1990,
	0,
	1990,
	0,
	9,
	28,
	-1,
	114,
	114,
	28,
	114,
	-1,
	28,
	114,
	31,
	14,
	114,
	14,
	23,
	14,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[2] = 
{
	{ 0x06000008, { 0, 2 } },
	{ 0x0600000D, { 2, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[3] = 
{
	{ (Il2CppRGCTXDataType)1, 13797 },
	{ (Il2CppRGCTXDataType)2, 13797 },
	{ (Il2CppRGCTXDataType)1, 27467 },
};
extern const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_AssetBundleModuleCodeGenModule = 
{
	"UnityEngine.AssetBundleModule.dll",
	22,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	2,
	s_rgctxIndices,
	3,
	s_rgctxValues,
	NULL,
};
