﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.WWW::.ctor(System.String)
extern void WWW__ctor_mE77AD6C372CC76F48A893C5E2F91A81193A9F8C5 (void);
// 0x00000002 System.String UnityEngine.WWW::get_error()
extern void WWW_get_error_mB278F5EC90EF99FEF70D80112940CFB49E79C9BC (void);
// 0x00000003 System.Boolean UnityEngine.WWW::get_keepWaiting()
extern void WWW_get_keepWaiting_m231A6A7A835610182D78FC414665CC75195ABD70 (void);
// 0x00000004 System.Void UnityEngine.WWW::Dispose()
extern void WWW_Dispose_mF5A8B944281564903043545BC1E7F1CAD941519F (void);
// 0x00000005 UnityEngine.Object UnityEngine.WWW::GetAudioClipInternal(System.Boolean,System.Boolean,System.Boolean,UnityEngine.AudioType)
extern void WWW_GetAudioClipInternal_mF8229045A1132E05315D8A45C99291D0A00EF940 (void);
// 0x00000006 UnityEngine.AudioClip UnityEngine.WWW::GetAudioClip()
extern void WWW_GetAudioClip_m7DEB77D23B10F28DF9E2047B0B3E3302E0CBB43A (void);
// 0x00000007 UnityEngine.AudioClip UnityEngine.WWW::GetAudioClip(System.Boolean,System.Boolean)
extern void WWW_GetAudioClip_m6BB7008EDBC79E3A8179C24CC1F69D9932CD71A2 (void);
// 0x00000008 UnityEngine.AudioClip UnityEngine.WWW::GetAudioClip(System.Boolean,System.Boolean,UnityEngine.AudioType)
extern void WWW_GetAudioClip_m8E8051E071CD814FDBBEF9857533C66456E18D38 (void);
// 0x00000009 UnityEngine.AudioClip UnityEngine.Networking.WebRequestWWW::InternalCreateAudioClipUsingDH(UnityEngine.Networking.DownloadHandler,System.String,System.Boolean,System.Boolean,UnityEngine.AudioType)
extern void WebRequestWWW_InternalCreateAudioClipUsingDH_m98E33581B83CCB0C310A7A894869B0AB2E0E65E5 (void);
static Il2CppMethodPointer s_methodPointers[9] = 
{
	WWW__ctor_mE77AD6C372CC76F48A893C5E2F91A81193A9F8C5,
	WWW_get_error_mB278F5EC90EF99FEF70D80112940CFB49E79C9BC,
	WWW_get_keepWaiting_m231A6A7A835610182D78FC414665CC75195ABD70,
	WWW_Dispose_mF5A8B944281564903043545BC1E7F1CAD941519F,
	WWW_GetAudioClipInternal_mF8229045A1132E05315D8A45C99291D0A00EF940,
	WWW_GetAudioClip_m7DEB77D23B10F28DF9E2047B0B3E3302E0CBB43A,
	WWW_GetAudioClip_m6BB7008EDBC79E3A8179C24CC1F69D9932CD71A2,
	WWW_GetAudioClip_m8E8051E071CD814FDBBEF9857533C66456E18D38,
	WebRequestWWW_InternalCreateAudioClipUsingDH_m98E33581B83CCB0C310A7A894869B0AB2E0E65E5,
};
static const int32_t s_InvokerIndices[9] = 
{
	26,
	14,
	89,
	23,
	2181,
	14,
	372,
	2182,
	2183,
};
extern const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_UnityWebRequestWWWModuleCodeGenModule = 
{
	"UnityEngine.UnityWebRequestWWWModule.dll",
	9,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
