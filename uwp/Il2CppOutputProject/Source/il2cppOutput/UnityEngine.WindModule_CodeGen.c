﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void UnityEngine.WindZone::set_mode(UnityEngine.WindZoneMode)
extern void WindZone_set_mode_mAD558427D8DD6EFC77801F211A4263DEB7876583 (void);
// 0x00000002 System.Void UnityEngine.WindZone::set_windMain(System.Single)
extern void WindZone_set_windMain_mB256E2DBD0453C3E1F08AB9C114FD4AE115F395C (void);
// 0x00000003 System.Void UnityEngine.WindZone::set_windTurbulence(System.Single)
extern void WindZone_set_windTurbulence_m1072207BC214F82A5C3AADC490DFF82842A25BFD (void);
// 0x00000004 System.Void UnityEngine.WindZone::set_windPulseMagnitude(System.Single)
extern void WindZone_set_windPulseMagnitude_m1F9A4FAD633F3C659E3E8B616DDCD6C7D0D41522 (void);
// 0x00000005 System.Void UnityEngine.WindZone::set_windPulseFrequency(System.Single)
extern void WindZone_set_windPulseFrequency_m723B4C6610AA3D7042F3EA122D182B4669680A0E (void);
static Il2CppMethodPointer s_methodPointers[5] = 
{
	WindZone_set_mode_mAD558427D8DD6EFC77801F211A4263DEB7876583,
	WindZone_set_windMain_mB256E2DBD0453C3E1F08AB9C114FD4AE115F395C,
	WindZone_set_windTurbulence_m1072207BC214F82A5C3AADC490DFF82842A25BFD,
	WindZone_set_windPulseMagnitude_m1F9A4FAD633F3C659E3E8B616DDCD6C7D0D41522,
	WindZone_set_windPulseFrequency_m723B4C6610AA3D7042F3EA122D182B4669680A0E,
};
static const int32_t s_InvokerIndices[5] = 
{
	32,
	339,
	339,
	339,
	339,
};
extern const Il2CppCodeGenModule g_UnityEngine_WindModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_WindModuleCodeGenModule = 
{
	"UnityEngine.WindModule.dll",
	5,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
