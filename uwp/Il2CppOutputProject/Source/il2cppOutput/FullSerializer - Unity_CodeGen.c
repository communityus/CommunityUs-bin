﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean FullSerializer.fsAotCompilationManager::HasMember(FullSerializer.fsAotVersionInfo,FullSerializer.fsAotVersionInfo_Member)
extern void fsAotCompilationManager_HasMember_m66DDC47A65C25A5D8D4D2430C12E6115CA0D3CEF (void);
// 0x00000002 System.Boolean FullSerializer.fsAotCompilationManager::IsAotModelUpToDate(FullSerializer.fsMetaType,FullSerializer.fsIAotConverter)
extern void fsAotCompilationManager_IsAotModelUpToDate_m46DD2E8A5A60175B5D6B0EA71F1CBD23EBE7D447 (void);
// 0x00000003 System.Void FullSerializer.fsAotCompilationManager::.cctor()
extern void fsAotCompilationManager__cctor_m6055098E5D14A592DCF27E323110E96637372EA8 (void);
// 0x00000004 System.Boolean FullSerializer.fsAotConfiguration::TryFindEntry(System.Type,FullSerializer.fsAotConfiguration_Entry&)
extern void fsAotConfiguration_TryFindEntry_m5893FD04F254270095912923A82B50F82729372C (void);
// 0x00000005 System.Void FullSerializer.fsAotConfiguration::UpdateOrAddEntry(FullSerializer.fsAotConfiguration_Entry)
extern void fsAotConfiguration_UpdateOrAddEntry_m2D4CCBA9EC142ACA22F854DBDB3021AB70119E7F (void);
// 0x00000006 System.Void FullSerializer.fsAotConfiguration::.ctor()
extern void fsAotConfiguration__ctor_m35D09998C41E61E1B3CD143DD1803638B949EB1A (void);
// 0x00000007 System.Void FullSerializer.fsAotVersionInfo_Member::.ctor(FullSerializer.Internal.fsMetaProperty)
extern void Member__ctor_m181EBAB12DB4A72885AFB6506C50B423DEBF34D9_AdjustorThunk (void);
// 0x00000008 System.Boolean FullSerializer.fsAotVersionInfo_Member::Equals(System.Object)
extern void Member_Equals_m83CBA4E72F8BE5A3FAA1983BE9D4A637D9B550B5_AdjustorThunk (void);
// 0x00000009 System.Int32 FullSerializer.fsAotVersionInfo_Member::GetHashCode()
extern void Member_GetHashCode_m607F6829A84289125664739469AD2DEF6E699520_AdjustorThunk (void);
// 0x0000000A System.Boolean FullSerializer.fsAotVersionInfo_Member::op_Equality(FullSerializer.fsAotVersionInfo_Member,FullSerializer.fsAotVersionInfo_Member)
extern void Member_op_Equality_mF001F4E8A22D795DE519F2D39B791649466A6F10 (void);
// 0x0000000B System.Type FullSerializer.fsIAotConverter::get_ModelType()
// 0x0000000C FullSerializer.fsAotVersionInfo FullSerializer.fsIAotConverter::get_VersionInfo()
// 0x0000000D System.Void FullSerializer.fsConverterRegistrar::.cctor()
extern void fsConverterRegistrar__cctor_mDD66D604F371F73FFAACB55A260CC57820537D49 (void);
// 0x0000000E System.Object FullSerializer.fsBaseConverter::CreateInstance(FullSerializer.fsData,System.Type)
extern void fsBaseConverter_CreateInstance_mD08461D25F4388F566175E59CEE6B16A8E8F315B (void);
// 0x0000000F System.Boolean FullSerializer.fsBaseConverter::RequestCycleSupport(System.Type)
extern void fsBaseConverter_RequestCycleSupport_mA99440DC57D745CEB1FE368B768BA37D8C387FFA (void);
// 0x00000010 System.Boolean FullSerializer.fsBaseConverter::RequestInheritanceSupport(System.Type)
extern void fsBaseConverter_RequestInheritanceSupport_m5712B5BF26F4AC67AF1BFF2F6EAF3974DAC685C7 (void);
// 0x00000011 FullSerializer.fsResult FullSerializer.fsBaseConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
// 0x00000012 FullSerializer.fsResult FullSerializer.fsBaseConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
// 0x00000013 FullSerializer.fsResult FullSerializer.fsBaseConverter::FailExpectedType(FullSerializer.fsData,FullSerializer.fsDataType[])
extern void fsBaseConverter_FailExpectedType_m7A879F5F63FA9AA7C23B40383BA1D29E3BD835A7 (void);
// 0x00000014 FullSerializer.fsResult FullSerializer.fsBaseConverter::CheckType(FullSerializer.fsData,FullSerializer.fsDataType)
extern void fsBaseConverter_CheckType_mACFF822768709E985EA98A2C7AB8A2707C3049D9 (void);
// 0x00000015 FullSerializer.fsResult FullSerializer.fsBaseConverter::CheckKey(FullSerializer.fsData,System.String,FullSerializer.fsData&)
extern void fsBaseConverter_CheckKey_m383C74FE0CA032E50C8EF955F5C7E70B7E00E2FF (void);
// 0x00000016 FullSerializer.fsResult FullSerializer.fsBaseConverter::CheckKey(System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>,System.String,FullSerializer.fsData&)
extern void fsBaseConverter_CheckKey_m63950C633C1F1C72A15EC9DF85340CD5DBB57AFB (void);
// 0x00000017 FullSerializer.fsResult FullSerializer.fsBaseConverter::SerializeMember(System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>,System.Type,System.String,T)
// 0x00000018 FullSerializer.fsResult FullSerializer.fsBaseConverter::DeserializeMember(System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>,System.Type,System.String,T&)
// 0x00000019 System.Void FullSerializer.fsBaseConverter::.ctor()
extern void fsBaseConverter__ctor_m4394D0608D06A61924AEB97244F4EFD896B3CF5D (void);
// 0x0000001A System.Void FullSerializer.fsBaseConverter_<>c::.cctor()
extern void U3CU3Ec__cctor_mA5E0BB6B1B51FE2AD655603319D00118F00350B1 (void);
// 0x0000001B System.Void FullSerializer.fsBaseConverter_<>c::.ctor()
extern void U3CU3Ec__ctor_m6170F4437798D08AEB0EA10BFFB60BF8F5A16A15 (void);
// 0x0000001C System.String FullSerializer.fsBaseConverter_<>c::<FailExpectedType>b__6_0(FullSerializer.fsDataType)
extern void U3CU3Ec_U3CFailExpectedTypeU3Eb__6_0_m5ECAF6BA7525D7F2D963B681DFD472502430C491 (void);
// 0x0000001D System.Void FullSerializer.fsGlobalConfig::.cctor()
extern void fsGlobalConfig__cctor_mF26298670949647B9EABFC275FD844F9BE0B3C68 (void);
// 0x0000001E System.Void FullSerializer.fsConfig::.ctor()
extern void fsConfig__ctor_m57C4CC6949E28C36BC571D9D5B2B873D5A859E74 (void);
// 0x0000001F System.Void FullSerializer.fsConfig_<>c::.cctor()
extern void U3CU3Ec__cctor_m3608C110B6A50A5FC8C4E00AC8795CB1D546E664 (void);
// 0x00000020 System.Void FullSerializer.fsConfig_<>c::.ctor()
extern void U3CU3Ec__ctor_m043E6F36878A7A1C88A49E5448595021FA512782 (void);
// 0x00000021 System.String FullSerializer.fsConfig_<>c::<.ctor>b__11_0(System.String,System.Reflection.MemberInfo)
extern void U3CU3Ec_U3C_ctorU3Eb__11_0_m2904DACE629D2354154DE429C0D9CD9F42E7090F (void);
// 0x00000022 System.Void FullSerializer.fsContext::Set(T)
// 0x00000023 T FullSerializer.fsContext::Get()
// 0x00000024 System.Void FullSerializer.fsContext::.ctor()
extern void fsContext__ctor_m04AEE56509EA89E24FB5CC180CA1E10446D2E3F4 (void);
// 0x00000025 System.Boolean FullSerializer.fsConverter::CanProcess(System.Type)
// 0x00000026 System.Void FullSerializer.fsConverter::.ctor()
extern void fsConverter__ctor_m128C860A18FCA4ADE7AE973F67E6F5610A311810 (void);
// 0x00000027 System.Void FullSerializer.fsData::.ctor()
extern void fsData__ctor_mC8D82C31F29BFAE52982A720221D210121185CB7 (void);
// 0x00000028 System.Void FullSerializer.fsData::.ctor(System.Boolean)
extern void fsData__ctor_m93E085E9D60FEC28307C480D920596EE792B7CC9 (void);
// 0x00000029 System.Void FullSerializer.fsData::.ctor(System.Double)
extern void fsData__ctor_m822DE439103E3DDA8826208CF9A85DF688A12BBA (void);
// 0x0000002A System.Void FullSerializer.fsData::.ctor(System.Int64)
extern void fsData__ctor_m0929EC78A236579973BD6F91FB552AA5DCB3C49F (void);
// 0x0000002B System.Void FullSerializer.fsData::.ctor(System.String)
extern void fsData__ctor_mC147F4BD92B3CF84E81C93DAC07DC9757BB6C1EA (void);
// 0x0000002C System.Void FullSerializer.fsData::.ctor(System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>)
extern void fsData__ctor_mD629E2C45F8DFD34B8555B3F2725E84662101BF4 (void);
// 0x0000002D System.Void FullSerializer.fsData::.ctor(System.Collections.Generic.List`1<FullSerializer.fsData>)
extern void fsData__ctor_m51D7CCCE42EFB72C8CD7F83DB2A475671A4C27C5 (void);
// 0x0000002E FullSerializer.fsData FullSerializer.fsData::CreateDictionary()
extern void fsData_CreateDictionary_m1288D9EBE32243B57226CEBD1DE1B2953C9F1CE8 (void);
// 0x0000002F FullSerializer.fsData FullSerializer.fsData::CreateList(System.Int32)
extern void fsData_CreateList_mDE68F5F7DCF61BCD2B46A452AF6E9F52C14DAE24 (void);
// 0x00000030 System.Void FullSerializer.fsData::BecomeDictionary()
extern void fsData_BecomeDictionary_mA48BEF00EDDE512D34A1BC15B7000793B074BC04 (void);
// 0x00000031 FullSerializer.fsData FullSerializer.fsData::Clone()
extern void fsData_Clone_mB1BE6F037078857E438663E91308E91286075E1A (void);
// 0x00000032 FullSerializer.fsDataType FullSerializer.fsData::get_Type()
extern void fsData_get_Type_m99176BD2C65FAAF228ACC532373D686E51114D4C (void);
// 0x00000033 System.Boolean FullSerializer.fsData::get_IsNull()
extern void fsData_get_IsNull_mB4A35DB48C95508F759A0E312556ACB92729D66F (void);
// 0x00000034 System.Boolean FullSerializer.fsData::get_IsDouble()
extern void fsData_get_IsDouble_mF71B7633267340E6727F409675969F903A6E1C70 (void);
// 0x00000035 System.Boolean FullSerializer.fsData::get_IsInt64()
extern void fsData_get_IsInt64_m1BD8C0AA38BB6D445C4B32B0329C907970E2560C (void);
// 0x00000036 System.Boolean FullSerializer.fsData::get_IsBool()
extern void fsData_get_IsBool_m99072802C0DA49D31737688D91BAA4C737711144 (void);
// 0x00000037 System.Boolean FullSerializer.fsData::get_IsString()
extern void fsData_get_IsString_m6EC7158047914D70B1A29B1DF9E943DA81F23E77 (void);
// 0x00000038 System.Boolean FullSerializer.fsData::get_IsDictionary()
extern void fsData_get_IsDictionary_mEE2E3BF69B445172B0B9D6765DE9F3DBC371B3D3 (void);
// 0x00000039 System.Boolean FullSerializer.fsData::get_IsList()
extern void fsData_get_IsList_mCC301B672339996E175F44CCA3FB24EB073F8E5B (void);
// 0x0000003A System.Double FullSerializer.fsData::get_AsDouble()
extern void fsData_get_AsDouble_m1DD0B96B2DFA32B05F306939A5844EED0B153464 (void);
// 0x0000003B System.Int64 FullSerializer.fsData::get_AsInt64()
extern void fsData_get_AsInt64_m3DFDC6B68BAC884963A5445B1BC5A6CFECD07BB2 (void);
// 0x0000003C System.Boolean FullSerializer.fsData::get_AsBool()
extern void fsData_get_AsBool_mB8360A00381318F6BAA903A65302E628789A3255 (void);
// 0x0000003D System.String FullSerializer.fsData::get_AsString()
extern void fsData_get_AsString_m5D58DE99A58A981366E8592A18EE25E6B46BA6E7 (void);
// 0x0000003E System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData> FullSerializer.fsData::get_AsDictionary()
extern void fsData_get_AsDictionary_mDF9877487F2278358C4B94556633D11843715CA1 (void);
// 0x0000003F System.Collections.Generic.List`1<FullSerializer.fsData> FullSerializer.fsData::get_AsList()
extern void fsData_get_AsList_m5639D699A2C4E28959D3676934DC3B32DA28B193 (void);
// 0x00000040 T FullSerializer.fsData::Cast()
// 0x00000041 System.String FullSerializer.fsData::ToString()
extern void fsData_ToString_m2BAE1989932AB7058E1A0AAAAF77D5D7BA2D9233 (void);
// 0x00000042 System.Boolean FullSerializer.fsData::Equals(System.Object)
extern void fsData_Equals_m699AB1A7424F56FBF99282CE1F610A6A3B98349C (void);
// 0x00000043 System.Boolean FullSerializer.fsData::Equals(FullSerializer.fsData)
extern void fsData_Equals_mE6A7CC7EB494558C8ACE3BB80CD3BCF14C3F5BDB (void);
// 0x00000044 System.Boolean FullSerializer.fsData::op_Equality(FullSerializer.fsData,FullSerializer.fsData)
extern void fsData_op_Equality_m7551403EB676E0D232C048EE2063EF80BFB8DDDC (void);
// 0x00000045 System.Boolean FullSerializer.fsData::op_Inequality(FullSerializer.fsData,FullSerializer.fsData)
extern void fsData_op_Inequality_mB28D25FC34D90D5E8447338C560A1A845E97056D (void);
// 0x00000046 System.Int32 FullSerializer.fsData::GetHashCode()
extern void fsData_GetHashCode_m0116E244D126518483FA3699B7846EABDD076921 (void);
// 0x00000047 System.Void FullSerializer.fsData::.cctor()
extern void fsData__cctor_m73E6F5D149288399A43BD7847302425F01D2602A (void);
// 0x00000048 System.Type FullSerializer.fsDirectConverter::get_ModelType()
// 0x00000049 System.Void FullSerializer.fsDirectConverter::.ctor()
extern void fsDirectConverter__ctor_m97790897C194D24C858E342216E248590112F770 (void);
// 0x0000004A System.Type FullSerializer.fsDirectConverter`1::get_ModelType()
// 0x0000004B FullSerializer.fsResult FullSerializer.fsDirectConverter`1::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
// 0x0000004C FullSerializer.fsResult FullSerializer.fsDirectConverter`1::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
// 0x0000004D FullSerializer.fsResult FullSerializer.fsDirectConverter`1::DoSerialize(TModel,System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>)
// 0x0000004E FullSerializer.fsResult FullSerializer.fsDirectConverter`1::DoDeserialize(System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>,TModel&)
// 0x0000004F System.Void FullSerializer.fsDirectConverter`1::.ctor()
// 0x00000050 System.Void FullSerializer.fsMissingVersionConstructorException::.ctor(System.Type,System.Type)
extern void fsMissingVersionConstructorException__ctor_mDCA44EBEDC2E7BCB9C160D1794808C4C03297E7C (void);
// 0x00000051 System.Void FullSerializer.fsDuplicateVersionNameException::.ctor(System.Type,System.Type,System.String)
extern void fsDuplicateVersionNameException__ctor_m26023CDF30C5CFE02E66CFF61A116F35FD3869B4 (void);
// 0x00000052 System.Void FullSerializer.fsIgnoreAttribute::.ctor()
extern void fsIgnoreAttribute__ctor_m7C69538D942D74FA7B41E9A097A6BBEA3EF3C55C (void);
// 0x00000053 System.Void FullSerializer.fsISerializationCallbacks::OnBeforeSerialize(System.Type)
// 0x00000054 System.Void FullSerializer.fsISerializationCallbacks::OnAfterSerialize(System.Type,FullSerializer.fsData&)
// 0x00000055 System.Void FullSerializer.fsISerializationCallbacks::OnBeforeDeserialize(System.Type,FullSerializer.fsData&)
// 0x00000056 System.Void FullSerializer.fsISerializationCallbacks::OnAfterDeserialize(System.Type)
// 0x00000057 FullSerializer.fsResult FullSerializer.fsJsonParser::MakeFailure(System.String)
extern void fsJsonParser_MakeFailure_m612A123C722F9875E72442B6A56FD798D9F9CDF4 (void);
// 0x00000058 System.Boolean FullSerializer.fsJsonParser::TryMoveNext()
extern void fsJsonParser_TryMoveNext_m42AFAC37ED84902917CF00C9A9C33BDA8024D2DB (void);
// 0x00000059 System.Boolean FullSerializer.fsJsonParser::HasValue()
extern void fsJsonParser_HasValue_m0AD646A8050F1CCDC5B4F61CBC1179F774049D0D (void);
// 0x0000005A System.Boolean FullSerializer.fsJsonParser::HasValue(System.Int32)
extern void fsJsonParser_HasValue_m68F75FA6054CBC9EA104900970084B880AC42742 (void);
// 0x0000005B System.Char FullSerializer.fsJsonParser::Character()
extern void fsJsonParser_Character_m2B5EED96858028E4CE851B37EA5570AA7F797526 (void);
// 0x0000005C System.Char FullSerializer.fsJsonParser::Character(System.Int32)
extern void fsJsonParser_Character_m06FA063EDC83406FD4E75E617A7DC519A4FECC97 (void);
// 0x0000005D System.Void FullSerializer.fsJsonParser::SkipSpace()
extern void fsJsonParser_SkipSpace_m13CB76DE0B5D73D30CDDB33AA39EBEC81D0A6E48 (void);
// 0x0000005E System.Boolean FullSerializer.fsJsonParser::IsHex(System.Char)
extern void fsJsonParser_IsHex_m711371C5FEBA48A0D73D96A1CFAF0741D106A6E6 (void);
// 0x0000005F System.UInt32 FullSerializer.fsJsonParser::ParseSingleChar(System.Char,System.UInt32)
extern void fsJsonParser_ParseSingleChar_m525B152F178186173E1C79D68BEA60C1424D4B1C (void);
// 0x00000060 System.UInt32 FullSerializer.fsJsonParser::ParseUnicode(System.Char,System.Char,System.Char,System.Char)
extern void fsJsonParser_ParseUnicode_m06AE25ACB11245DAB010109E0FD16C78D2365039 (void);
// 0x00000061 FullSerializer.fsResult FullSerializer.fsJsonParser::TryUnescapeChar(System.Char&)
extern void fsJsonParser_TryUnescapeChar_mCCA0DE32B3AAE324A873C67B50F1D9BF0704A172 (void);
// 0x00000062 FullSerializer.fsResult FullSerializer.fsJsonParser::TryParseExact(System.String)
extern void fsJsonParser_TryParseExact_m369468D91045859027F0FBE15790B6D0331A43E3 (void);
// 0x00000063 FullSerializer.fsResult FullSerializer.fsJsonParser::TryParseTrue(FullSerializer.fsData&)
extern void fsJsonParser_TryParseTrue_m98139D5AB205298E8A505B88C2EB5BFCE9FBD80C (void);
// 0x00000064 FullSerializer.fsResult FullSerializer.fsJsonParser::TryParseFalse(FullSerializer.fsData&)
extern void fsJsonParser_TryParseFalse_m3DCB93B7013A92F402E601DD54DF4A394283D54A (void);
// 0x00000065 FullSerializer.fsResult FullSerializer.fsJsonParser::TryParseNull(FullSerializer.fsData&)
extern void fsJsonParser_TryParseNull_mC5601B890CB0444D52AE694630D2069ACF42BF10 (void);
// 0x00000066 System.Boolean FullSerializer.fsJsonParser::IsSeparator(System.Char)
extern void fsJsonParser_IsSeparator_m8A65CBF3058C4C5434A75522037EC7FDDF5CC2F3 (void);
// 0x00000067 FullSerializer.fsResult FullSerializer.fsJsonParser::TryParseNumber(FullSerializer.fsData&)
extern void fsJsonParser_TryParseNumber_m741A678D1849D6624BD9A9627B3F3E63A3527711 (void);
// 0x00000068 FullSerializer.fsResult FullSerializer.fsJsonParser::TryParseString(System.String&)
extern void fsJsonParser_TryParseString_m19F4E21155FC9074FCA92A09566482B4ED174CE8 (void);
// 0x00000069 FullSerializer.fsResult FullSerializer.fsJsonParser::TryParseArray(FullSerializer.fsData&)
extern void fsJsonParser_TryParseArray_m10047D56B043C1BA67560571BD3DC50EF7293537 (void);
// 0x0000006A FullSerializer.fsResult FullSerializer.fsJsonParser::TryParseObject(FullSerializer.fsData&)
extern void fsJsonParser_TryParseObject_m73C7F4A7320508BF673B9F07EA01BCE0D83EE064 (void);
// 0x0000006B FullSerializer.fsResult FullSerializer.fsJsonParser::RunParse(FullSerializer.fsData&)
extern void fsJsonParser_RunParse_m86B44D912E5925CDD6C69450BAE78B69F057D440 (void);
// 0x0000006C FullSerializer.fsResult FullSerializer.fsJsonParser::Parse(System.String,FullSerializer.fsData&)
extern void fsJsonParser_Parse_mE36DEC1FECE1891331BB0DC82C45EAA85765F295 (void);
// 0x0000006D FullSerializer.fsData FullSerializer.fsJsonParser::Parse(System.String)
extern void fsJsonParser_Parse_m10D50DC78837EE0BA8088085562AD41FD615B769 (void);
// 0x0000006E System.Void FullSerializer.fsJsonParser::.ctor(System.String)
extern void fsJsonParser__ctor_mB0750A7C76FC1104BBF901893C7C91434930EEDB (void);
// 0x0000006F System.Void FullSerializer.fsJsonPrinter::InsertSpacing(System.IO.TextWriter,System.Int32)
extern void fsJsonPrinter_InsertSpacing_m3F878957FF0526D0A0FC72A3986840C71E956D26 (void);
// 0x00000070 System.String FullSerializer.fsJsonPrinter::EscapeString(System.String)
extern void fsJsonPrinter_EscapeString_m444589C7FFF33F320026A99FC25F6D8743EDEE44 (void);
// 0x00000071 System.Void FullSerializer.fsJsonPrinter::BuildCompressedString(FullSerializer.fsData,System.IO.TextWriter)
extern void fsJsonPrinter_BuildCompressedString_m95FCFCD70D26F0FDC76AB6686A89C4FE15272BB4 (void);
// 0x00000072 System.Void FullSerializer.fsJsonPrinter::BuildPrettyString(FullSerializer.fsData,System.IO.TextWriter,System.Int32)
extern void fsJsonPrinter_BuildPrettyString_mF5E14807E45BE6897DF39F403A01CA869FD86553 (void);
// 0x00000073 System.String FullSerializer.fsJsonPrinter::PrettyJson(FullSerializer.fsData)
extern void fsJsonPrinter_PrettyJson_m35525633CE381A97328383ABBA16C576AFABD682 (void);
// 0x00000074 System.String FullSerializer.fsJsonPrinter::CompressedJson(FullSerializer.fsData)
extern void fsJsonPrinter_CompressedJson_mCEE98FB0A27EDB5ABDF58E6F3DDFBAD70B5D536A (void);
// 0x00000075 System.String FullSerializer.fsJsonPrinter::ConvertDoubleToString(System.Double)
extern void fsJsonPrinter_ConvertDoubleToString_m1406E1C1D733C25125064D96A38F2B5E2857F4BE (void);
// 0x00000076 System.Void FullSerializer.fsObjectAttribute::.ctor()
extern void fsObjectAttribute__ctor_m08EC26A024C177A584D0185203E2F888E03EED3C (void);
// 0x00000077 System.Void FullSerializer.fsObjectAttribute::.ctor(System.String,System.Type[])
extern void fsObjectAttribute__ctor_m6F39B6439ECB2102676B8AE891DAD0C14A6615E9 (void);
// 0x00000078 System.Boolean FullSerializer.fsObjectProcessor::CanProcess(System.Type)
extern void fsObjectProcessor_CanProcess_m2EF960183524FA90FDE56F7F79E86E4144F44389 (void);
// 0x00000079 System.Void FullSerializer.fsObjectProcessor::OnBeforeSerialize(System.Type,System.Object)
extern void fsObjectProcessor_OnBeforeSerialize_mF97CC0F2F2A163AAFF86BF522B039B34A3CD5C6B (void);
// 0x0000007A System.Void FullSerializer.fsObjectProcessor::OnAfterSerialize(System.Type,System.Object,FullSerializer.fsData&)
extern void fsObjectProcessor_OnAfterSerialize_mF1B5BFA5E5CBF6244AA2D704A17FCE4645E25CA9 (void);
// 0x0000007B System.Void FullSerializer.fsObjectProcessor::OnBeforeDeserialize(System.Type,FullSerializer.fsData&)
extern void fsObjectProcessor_OnBeforeDeserialize_m1A73B45EF8646F9C01B3CA43734252160B5B8F6C (void);
// 0x0000007C System.Void FullSerializer.fsObjectProcessor::OnBeforeDeserializeAfterInstanceCreation(System.Type,System.Object,FullSerializer.fsData&)
extern void fsObjectProcessor_OnBeforeDeserializeAfterInstanceCreation_m753C7410A1E54DEE741958749BCB26511EA27DA0 (void);
// 0x0000007D System.Void FullSerializer.fsObjectProcessor::OnAfterDeserialize(System.Type,System.Object)
extern void fsObjectProcessor_OnAfterDeserialize_m10E2A98F03D60EFB4BA9938540800140B4433696 (void);
// 0x0000007E System.Void FullSerializer.fsObjectProcessor::.ctor()
extern void fsObjectProcessor__ctor_m721DF9C53412F9ADA8EA59591A26C9FB216A7196 (void);
// 0x0000007F System.Void FullSerializer.fsPropertyAttribute::.ctor()
extern void fsPropertyAttribute__ctor_m6C06F09C709F250D1BF6668DFF5D92ADE6F15AE2 (void);
// 0x00000080 System.Void FullSerializer.fsPropertyAttribute::.ctor(System.String)
extern void fsPropertyAttribute__ctor_mC5D2ABE1EABB17F625A6F392140CAC4371830D7E (void);
// 0x00000081 System.Void FullSerializer.fsResult::AddMessage(System.String)
extern void fsResult_AddMessage_mDAAC5F5664BAD0975CEB87CB78F57840F9ECA07A_AdjustorThunk (void);
// 0x00000082 System.Void FullSerializer.fsResult::AddMessages(FullSerializer.fsResult)
extern void fsResult_AddMessages_mB0BFF0A53D4E67FD22928A3AA90FCEC7C24BACAC_AdjustorThunk (void);
// 0x00000083 FullSerializer.fsResult FullSerializer.fsResult::Merge(FullSerializer.fsResult)
extern void fsResult_Merge_mCA43BA4A0C84F86B1C741B870A36DE64990EE316_AdjustorThunk (void);
// 0x00000084 FullSerializer.fsResult FullSerializer.fsResult::Warn(System.String)
extern void fsResult_Warn_m441CA3313DDEC3881842B39F80CFACC77DEEB3DC (void);
// 0x00000085 FullSerializer.fsResult FullSerializer.fsResult::Fail(System.String)
extern void fsResult_Fail_m53DFE0A162FEBB169CA8351746E3358DF684C202 (void);
// 0x00000086 FullSerializer.fsResult FullSerializer.fsResult::op_Addition(FullSerializer.fsResult,FullSerializer.fsResult)
extern void fsResult_op_Addition_m4D6866C260A2A17083DF96D3BA88B172E279A8EA (void);
// 0x00000087 System.Boolean FullSerializer.fsResult::get_Failed()
extern void fsResult_get_Failed_m9C792FBD2658DA77759BE224A949470679F99074_AdjustorThunk (void);
// 0x00000088 System.Boolean FullSerializer.fsResult::get_Succeeded()
extern void fsResult_get_Succeeded_m5F05E03DF89C8C09B064B4DBFC94705F1049D19C_AdjustorThunk (void);
// 0x00000089 System.Boolean FullSerializer.fsResult::get_HasWarnings()
extern void fsResult_get_HasWarnings_mAEA08C91A4C28DF287C1C77AC89957D555F44682_AdjustorThunk (void);
// 0x0000008A FullSerializer.fsResult FullSerializer.fsResult::AssertSuccess()
extern void fsResult_AssertSuccess_m75D00F1F4EAE7C15386F766E9FFB2A2FE3018B18_AdjustorThunk (void);
// 0x0000008B FullSerializer.fsResult FullSerializer.fsResult::AssertSuccessWithoutWarnings()
extern void fsResult_AssertSuccessWithoutWarnings_m4F6BF6FDE3A244CFED2CAF6B098174B2AB7891C5_AdjustorThunk (void);
// 0x0000008C System.Exception FullSerializer.fsResult::get_AsException()
extern void fsResult_get_AsException_mF123DDA70FE30796B88229EDD88E4675A1D5A323_AdjustorThunk (void);
// 0x0000008D System.Collections.Generic.IEnumerable`1<System.String> FullSerializer.fsResult::get_RawMessages()
extern void fsResult_get_RawMessages_mAE4B17BA4E72973DB0221595A19CAC56C7390521_AdjustorThunk (void);
// 0x0000008E System.String FullSerializer.fsResult::get_FormattedMessages()
extern void fsResult_get_FormattedMessages_mE62818C25A5F30B17E6D4E40A065B78C0C83DD1E_AdjustorThunk (void);
// 0x0000008F System.Void FullSerializer.fsResult::.cctor()
extern void fsResult__cctor_m40CAFD0891A35C76E7195E6A1F02B046BA9C0030 (void);
// 0x00000090 System.Void FullSerializer.fsSerializer::.cctor()
extern void fsSerializer__cctor_m0B3EE9E67511EA8D1983BC4CB238CACD48C86BFE (void);
// 0x00000091 System.Boolean FullSerializer.fsSerializer::IsReservedKeyword(System.String)
extern void fsSerializer_IsReservedKeyword_m25F3FE6BD8A6CAE492CEC661B52BB1AD9E014BB4 (void);
// 0x00000092 System.Boolean FullSerializer.fsSerializer::IsObjectReference(FullSerializer.fsData)
extern void fsSerializer_IsObjectReference_mC1A653A92438E92B526CEB6E205A3BA71B0A9B71 (void);
// 0x00000093 System.Boolean FullSerializer.fsSerializer::IsObjectDefinition(FullSerializer.fsData)
extern void fsSerializer_IsObjectDefinition_m80D08FE5248C23E7DC9286FD7C3A21A2AB50D888 (void);
// 0x00000094 System.Boolean FullSerializer.fsSerializer::IsVersioned(FullSerializer.fsData)
extern void fsSerializer_IsVersioned_mD27D19963C5148F9082DB848414059B6DE4E9763 (void);
// 0x00000095 System.Boolean FullSerializer.fsSerializer::IsTypeSpecified(FullSerializer.fsData)
extern void fsSerializer_IsTypeSpecified_mD254FE00C93CB9FB2AE01E5BF5F916EC667F224E (void);
// 0x00000096 System.Boolean FullSerializer.fsSerializer::IsWrappedData(FullSerializer.fsData)
extern void fsSerializer_IsWrappedData_mCCC3F4731B3563FC4811C63A0E5A4B3E4CFDAB4C (void);
// 0x00000097 System.Void FullSerializer.fsSerializer::ConvertLegacyData(FullSerializer.fsData&)
extern void fsSerializer_ConvertLegacyData_m58FA7B22FBC34FFE6240BFA650935D4CDF3770DF (void);
// 0x00000098 System.Void FullSerializer.fsSerializer::Invoke_OnBeforeSerialize(System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>,System.Type,System.Object)
extern void fsSerializer_Invoke_OnBeforeSerialize_m33007E2C939E91FD36C3370E99EC5CF42FE191B7 (void);
// 0x00000099 System.Void FullSerializer.fsSerializer::Invoke_OnAfterSerialize(System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>,System.Type,System.Object,FullSerializer.fsData&)
extern void fsSerializer_Invoke_OnAfterSerialize_mFF3B979FD2B5E838C133BF8C70822551D688AE01 (void);
// 0x0000009A System.Void FullSerializer.fsSerializer::Invoke_OnBeforeDeserialize(System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>,System.Type,FullSerializer.fsData&)
extern void fsSerializer_Invoke_OnBeforeDeserialize_mFD63E50B79BAC8CC936043F0175FB0C649EAE83B (void);
// 0x0000009B System.Void FullSerializer.fsSerializer::Invoke_OnBeforeDeserializeAfterInstanceCreation(System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>,System.Type,System.Object,FullSerializer.fsData&)
extern void fsSerializer_Invoke_OnBeforeDeserializeAfterInstanceCreation_mE1009D54923B4846F7C90252924217F19470B065 (void);
// 0x0000009C System.Void FullSerializer.fsSerializer::Invoke_OnAfterDeserialize(System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>,System.Type,System.Object)
extern void fsSerializer_Invoke_OnAfterDeserialize_mB5D30C6CE065F4FF4B0A76A0B76AD16929363BA6 (void);
// 0x0000009D System.Void FullSerializer.fsSerializer::EnsureDictionary(FullSerializer.fsData)
extern void fsSerializer_EnsureDictionary_m94CE569EF6B6AEF509BA5A0BB5400703372C9D8A (void);
// 0x0000009E System.Void FullSerializer.fsSerializer::RemapAbstractStorageTypeToDefaultType(System.Type&)
extern void fsSerializer_RemapAbstractStorageTypeToDefaultType_m22A9C7048D23CAD809A884F9FEAE4D5C2F1C5D75 (void);
// 0x0000009F System.Void FullSerializer.fsSerializer::.ctor()
extern void fsSerializer__ctor_mDCC774233DFDA009C15E222E01A2B8CFBD763348 (void);
// 0x000000A0 System.Void FullSerializer.fsSerializer::SetDefaultStorageType(System.Type,System.Type)
extern void fsSerializer_SetDefaultStorageType_m4A3C49D785CFF8E743DBFD1F226CB63F43317B6C (void);
// 0x000000A1 System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor> FullSerializer.fsSerializer::GetProcessors(System.Type)
extern void fsSerializer_GetProcessors_mDAF7C82F1ABFFF195B03D598EA344FEF462536BB (void);
// 0x000000A2 System.Void FullSerializer.fsSerializer::AddConverter(FullSerializer.fsBaseConverter)
extern void fsSerializer_AddConverter_mF2138B22425127876B9EC6DAF1C7E9292642A3C9 (void);
// 0x000000A3 FullSerializer.fsBaseConverter FullSerializer.fsSerializer::GetConverter(System.Type,System.Type)
extern void fsSerializer_GetConverter_m0356C48F956FDE808C5658A284EB82A6F81DFBD2 (void);
// 0x000000A4 FullSerializer.fsResult FullSerializer.fsSerializer::TrySerialize(T,FullSerializer.fsData&)
// 0x000000A5 FullSerializer.fsResult FullSerializer.fsSerializer::TryDeserialize(FullSerializer.fsData,T&)
// 0x000000A6 FullSerializer.fsResult FullSerializer.fsSerializer::TrySerialize(System.Type,System.Object,FullSerializer.fsData&)
extern void fsSerializer_TrySerialize_mDEBE5C853C6A5709B814EE1657A944CBCE83BD74 (void);
// 0x000000A7 FullSerializer.fsResult FullSerializer.fsSerializer::TrySerialize(System.Type,System.Type,System.Object,FullSerializer.fsData&)
extern void fsSerializer_TrySerialize_m30AB528A4F96321F879C0591A27DC940D4523D52 (void);
// 0x000000A8 FullSerializer.fsResult FullSerializer.fsSerializer::InternalSerialize_1_ProcessCycles(System.Type,System.Type,System.Object,FullSerializer.fsData&)
extern void fsSerializer_InternalSerialize_1_ProcessCycles_m749503133D842B3489A34F23D844D5D6A08E1D1E (void);
// 0x000000A9 FullSerializer.fsResult FullSerializer.fsSerializer::InternalSerialize_2_Inheritance(System.Type,System.Type,System.Object,FullSerializer.fsData&)
extern void fsSerializer_InternalSerialize_2_Inheritance_m45860AE5AD5BD2B35DED9B15D42E502EE413D35D (void);
// 0x000000AA FullSerializer.fsResult FullSerializer.fsSerializer::InternalSerialize_3_ProcessVersioning(System.Type,System.Object,FullSerializer.fsData&)
extern void fsSerializer_InternalSerialize_3_ProcessVersioning_m284865BFDBBBA993E48765D77371B806609A63F6 (void);
// 0x000000AB FullSerializer.fsResult FullSerializer.fsSerializer::InternalSerialize_4_Converter(System.Type,System.Object,FullSerializer.fsData&)
extern void fsSerializer_InternalSerialize_4_Converter_m9322EC99B5E3B68C5E4AE75AB9447961141111A9 (void);
// 0x000000AC FullSerializer.fsResult FullSerializer.fsSerializer::TryDeserialize(FullSerializer.fsData,System.Type,System.Object&)
extern void fsSerializer_TryDeserialize_m5D8B5415536E3C941E8AB295157C3351BDC68CD7 (void);
// 0x000000AD FullSerializer.fsResult FullSerializer.fsSerializer::TryDeserialize(FullSerializer.fsData,System.Type,System.Type,System.Object&)
extern void fsSerializer_TryDeserialize_m9CFBD1374B1817BFAD920FAFCBF9411357B9A504 (void);
// 0x000000AE FullSerializer.fsResult FullSerializer.fsSerializer::InternalDeserialize_1_CycleReference(System.Type,FullSerializer.fsData,System.Type,System.Object&,System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>&)
extern void fsSerializer_InternalDeserialize_1_CycleReference_m22D8297763917BEB6A769507DCA3ACCA7C931FA2 (void);
// 0x000000AF FullSerializer.fsResult FullSerializer.fsSerializer::InternalDeserialize_2_Version(System.Type,FullSerializer.fsData,System.Type,System.Object&,System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>&)
extern void fsSerializer_InternalDeserialize_2_Version_m313AA0DE8D76CE18973A7707DC98A8183B1C5D4C (void);
// 0x000000B0 FullSerializer.fsResult FullSerializer.fsSerializer::InternalDeserialize_3_Inheritance(System.Type,FullSerializer.fsData,System.Type,System.Object&,System.Collections.Generic.List`1<FullSerializer.fsObjectProcessor>&)
extern void fsSerializer_InternalDeserialize_3_Inheritance_mC55306E74111FB61DEA4BDEE3CAD4A2A22DD76EA (void);
// 0x000000B1 FullSerializer.fsResult FullSerializer.fsSerializer::InternalDeserialize_4_Cycles(System.Type,FullSerializer.fsData,System.Type,System.Object&)
extern void fsSerializer_InternalDeserialize_4_Cycles_m7BA0BA3490976189D09A3584C39B6C4CB23FBCCD (void);
// 0x000000B2 FullSerializer.fsResult FullSerializer.fsSerializer::InternalDeserialize_5_Converter(System.Type,FullSerializer.fsData,System.Type,System.Object&)
extern void fsSerializer_InternalDeserialize_5_Converter_mFC4B8FBBF3801524D2BC33277BDC155307B1D9F4 (void);
// 0x000000B3 System.Void FullSerializer.fsSerializer_fsLazyCycleDefinitionWriter::WriteDefinition(System.Int32,FullSerializer.fsData)
extern void fsLazyCycleDefinitionWriter_WriteDefinition_mC35395DBE40534AA3582CEC7A4919373B0E54970 (void);
// 0x000000B4 System.Void FullSerializer.fsSerializer_fsLazyCycleDefinitionWriter::WriteReference(System.Int32,System.Collections.Generic.Dictionary`2<System.String,FullSerializer.fsData>)
extern void fsLazyCycleDefinitionWriter_WriteReference_m14D0337B69F0D1B4E8547A10E08BD75FEF4A5F6C (void);
// 0x000000B5 System.Void FullSerializer.fsSerializer_fsLazyCycleDefinitionWriter::Clear()
extern void fsLazyCycleDefinitionWriter_Clear_mB21696F24CA545B5BF1D493EE84035ED22137E15 (void);
// 0x000000B6 System.Void FullSerializer.fsSerializer_fsLazyCycleDefinitionWriter::.ctor()
extern void fsLazyCycleDefinitionWriter__ctor_mC16AD1D5B8BE78D8E8337D809D5D5C1C7866009D (void);
// 0x000000B7 System.String FullSerializer.fsTypeExtensions::CSharpName(System.Type)
extern void fsTypeExtensions_CSharpName_m329D538830145F7DE3ED45C4C2CC162E704E9418 (void);
// 0x000000B8 System.String FullSerializer.fsTypeExtensions::CSharpName(System.Type,System.Boolean)
extern void fsTypeExtensions_CSharpName_m0F73C266C9363A86384AA3713D5EAA6DFE7D1CE9 (void);
// 0x000000B9 System.Boolean FullSerializer.fsTypeExtensions::IsInterface(System.Type)
extern void fsTypeExtensions_IsInterface_m260C783B1B5AA4E53899A70615D8924C88EFAA4E (void);
// 0x000000BA System.Boolean FullSerializer.fsTypeExtensions::IsAbstract(System.Type)
extern void fsTypeExtensions_IsAbstract_m91D0B4C01902D0B0A7F66F37718397BF4C03AD4E (void);
// 0x000000BB System.Boolean FullSerializer.fsTypeExtensions::IsGenericType(System.Type)
extern void fsTypeExtensions_IsGenericType_mAD3AEA81AEEA2D64B6C2C2EFECCC9D890B4ED519 (void);
// 0x000000BC System.Void FullSerializer.fsTypeExtensions_<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_mF043F098B7C9413209A3F73786F7189957631CFD (void);
// 0x000000BD System.String FullSerializer.fsTypeExtensions_<>c__DisplayClass2_0::<CSharpName>b__0(System.Type)
extern void U3CU3Ec__DisplayClass2_0_U3CCSharpNameU3Eb__0_m7A828679557446316814E42D2A4C161D142FFACB (void);
// 0x000000BE FullSerializer.fsMetaType FullSerializer.fsMetaType::Get(FullSerializer.fsConfig,System.Type)
extern void fsMetaType_Get_m2AE5A33AC1592854E7F756628DCE5B6A832D7D35 (void);
// 0x000000BF System.Void FullSerializer.fsMetaType::.ctor(FullSerializer.fsConfig,System.Type)
extern void fsMetaType__ctor_m11EF37FF04A3B0E49E1991F95236C6D33A65BB2E (void);
// 0x000000C0 System.Void FullSerializer.fsMetaType::CollectProperties(FullSerializer.fsConfig,System.Collections.Generic.List`1<FullSerializer.Internal.fsMetaProperty>,System.Type)
extern void fsMetaType_CollectProperties_m78426CD58E6407C7B861ED637124EB5CC2F10F5A (void);
// 0x000000C1 System.Boolean FullSerializer.fsMetaType::IsAutoProperty(System.Reflection.PropertyInfo,System.Reflection.MemberInfo[])
extern void fsMetaType_IsAutoProperty_mA44D45C2B218B577C1D959F806289A686ECB2B1D (void);
// 0x000000C2 System.Boolean FullSerializer.fsMetaType::CanSerializeProperty(FullSerializer.fsConfig,System.Reflection.PropertyInfo,System.Reflection.MemberInfo[],System.Boolean)
extern void fsMetaType_CanSerializeProperty_m8187E14736132D253305EB6AA59BB4C67C6E5B07 (void);
// 0x000000C3 System.Boolean FullSerializer.fsMetaType::CanSerializeField(FullSerializer.fsConfig,System.Reflection.FieldInfo,System.Boolean)
extern void fsMetaType_CanSerializeField_m053BD9A9C623E273680401E6EDCA4B5B5A4022F1 (void);
// 0x000000C4 System.Void FullSerializer.fsMetaType::EmitAotData(System.Boolean)
extern void fsMetaType_EmitAotData_mE098781B57B0F12A8EB5F534F8435D6632921084 (void);
// 0x000000C5 FullSerializer.Internal.fsMetaProperty[] FullSerializer.fsMetaType::get_Properties()
extern void fsMetaType_get_Properties_mF2C3B7E8407690D2A3805DDFBC65A24DE90B4356 (void);
// 0x000000C6 System.Void FullSerializer.fsMetaType::set_Properties(FullSerializer.Internal.fsMetaProperty[])
extern void fsMetaType_set_Properties_mBF98B3B352C40881052A927BE6CCD3FE62541EC3 (void);
// 0x000000C7 System.Boolean FullSerializer.fsMetaType::get_HasDefaultConstructor()
extern void fsMetaType_get_HasDefaultConstructor_mAE8A9AAD9C54CA90618A43DCBF9DB67C2717A551 (void);
// 0x000000C8 System.Boolean FullSerializer.fsMetaType::get_IsDefaultConstructorPublic()
extern void fsMetaType_get_IsDefaultConstructorPublic_mFBE8E77BFC92A56CA3036ADA5EF7DEF73B625C09 (void);
// 0x000000C9 System.Object FullSerializer.fsMetaType::CreateInstance()
extern void fsMetaType_CreateInstance_m58CC0A6520E4D1761A2433A22AC312479E8D019F (void);
// 0x000000CA System.Void FullSerializer.fsMetaType::.cctor()
extern void fsMetaType__cctor_m700E39EAC99BA6FE1AFCBC2E2FBC2443BE6B654F (void);
// 0x000000CB System.Void FullSerializer.fsMetaType_AotFailureException::.ctor(System.String)
extern void AotFailureException__ctor_m9E351420347AAE80A058010D8F3C4161E78A3CA8 (void);
// 0x000000CC System.Void FullSerializer.fsMetaType_<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m29773E469E6CF1B64873718C2908B737DB740527 (void);
// 0x000000CD System.Boolean FullSerializer.fsMetaType_<>c__DisplayClass5_0::<CollectProperties>b__0(System.Type)
extern void U3CU3Ec__DisplayClass5_0_U3CCollectPropertiesU3Eb__0_m3A5AF42B3400B8F0DCBC01B3A111B5D63D201E35 (void);
// 0x000000CE System.Boolean FullSerializer.fsMetaType_<>c__DisplayClass5_0::<CollectProperties>b__1(System.Type)
extern void U3CU3Ec__DisplayClass5_0_U3CCollectPropertiesU3Eb__1_m3125FA2755F2F81376E4DAC56D72FA257DB52329 (void);
// 0x000000CF System.Boolean FullSerializer.fsMetaType_<>c__DisplayClass5_0::<CollectProperties>b__2(System.Type)
extern void U3CU3Ec__DisplayClass5_0_U3CCollectPropertiesU3Eb__2_m508E450F935FE76DE616E89F7E4036B27EA9DBC0 (void);
// 0x000000D0 System.Void FullSerializer.fsMetaType_<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mBE65B941CD688255AA98653EFC92BA989F51B3A5 (void);
// 0x000000D1 System.Boolean FullSerializer.fsMetaType_<>c__DisplayClass7_0::<CanSerializeProperty>b__0(System.Type)
extern void U3CU3Ec__DisplayClass7_0_U3CCanSerializePropertyU3Eb__0_m5D39FA95DCA4A822876CD9CEA936D0DC91FD0312 (void);
// 0x000000D2 System.Boolean FullSerializer.fsMetaType_<>c__DisplayClass7_0::<CanSerializeProperty>b__1(System.Type)
extern void U3CU3Ec__DisplayClass7_0_U3CCanSerializePropertyU3Eb__1_m30EDF83500A004B4E1CE55944144380509C33C0B (void);
// 0x000000D3 System.Void FullSerializer.fsMetaType_<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_mD9BD3C025632AB89805EB36943E9AF92D91F9073 (void);
// 0x000000D4 System.Boolean FullSerializer.fsMetaType_<>c__DisplayClass8_0::<CanSerializeField>b__0(System.Type)
extern void U3CU3Ec__DisplayClass8_0_U3CCanSerializeFieldU3Eb__0_m5EE4B6D0A4ADD655A52B6ADE4082795B5C3C22CC (void);
// 0x000000D5 System.Boolean FullSerializer.fsMetaType_<>c__DisplayClass8_0::<CanSerializeField>b__1(System.Type)
extern void U3CU3Ec__DisplayClass8_0_U3CCanSerializeFieldU3Eb__1_m36D291B112353E30A5B42241B1CE68DF7D7D54C3 (void);
// 0x000000D6 System.Boolean FullSerializer.Internal.fsArrayConverter::CanProcess(System.Type)
extern void fsArrayConverter_CanProcess_mDA9196F37D4A12B611F2AE459897E82C48004D07 (void);
// 0x000000D7 System.Boolean FullSerializer.Internal.fsArrayConverter::RequestCycleSupport(System.Type)
extern void fsArrayConverter_RequestCycleSupport_mEDB81088E7D59A6CB2895D2F550746F39F0A8E2B (void);
// 0x000000D8 System.Boolean FullSerializer.Internal.fsArrayConverter::RequestInheritanceSupport(System.Type)
extern void fsArrayConverter_RequestInheritanceSupport_m30290179FB2AC8B2D0EBDE6A35B2A764AC2FC4AF (void);
// 0x000000D9 FullSerializer.fsResult FullSerializer.Internal.fsArrayConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsArrayConverter_TrySerialize_m4C1165BC2B8D6D28CB6E9AD21EF0F630D37D8A8C (void);
// 0x000000DA FullSerializer.fsResult FullSerializer.Internal.fsArrayConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsArrayConverter_TryDeserialize_m38FE0EAF5BB5CACB6B80A20BDBA21DFAF26B1941 (void);
// 0x000000DB System.Object FullSerializer.Internal.fsArrayConverter::CreateInstance(FullSerializer.fsData,System.Type)
extern void fsArrayConverter_CreateInstance_mAFD196BBFC6699F76F8A94771AB519EF467BA76F (void);
// 0x000000DC System.Void FullSerializer.Internal.fsArrayConverter::.ctor()
extern void fsArrayConverter__ctor_mD8C8CFF409F0ACD30ADF2441DC11A5B82AB9E7AA (void);
// 0x000000DD System.String FullSerializer.Internal.fsDateConverter::get_DateTimeFormatString()
extern void fsDateConverter_get_DateTimeFormatString_mBEEE108FC4646A0DC5B062B07C569263121D56A7 (void);
// 0x000000DE System.Boolean FullSerializer.Internal.fsDateConverter::CanProcess(System.Type)
extern void fsDateConverter_CanProcess_m79A33B10FA608FF79B413B08D772992B6DB7D546 (void);
// 0x000000DF FullSerializer.fsResult FullSerializer.Internal.fsDateConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsDateConverter_TrySerialize_mC39BB16A68E067E7087BD33696219B41D2A58ED5 (void);
// 0x000000E0 FullSerializer.fsResult FullSerializer.Internal.fsDateConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsDateConverter_TryDeserialize_mD6FCF549AF74E383C9D70BB84DFBBF5E80ABD49D (void);
// 0x000000E1 System.Void FullSerializer.Internal.fsDateConverter::.ctor()
extern void fsDateConverter__ctor_m8E38DD596A478F128642ACF5B300D6635FABC32A (void);
// 0x000000E2 System.Boolean FullSerializer.Internal.fsDictionaryConverter::CanProcess(System.Type)
extern void fsDictionaryConverter_CanProcess_m89E8B29E93416FD1D1F27CBDA188D43D2DD69B4B (void);
// 0x000000E3 System.Object FullSerializer.Internal.fsDictionaryConverter::CreateInstance(FullSerializer.fsData,System.Type)
extern void fsDictionaryConverter_CreateInstance_m221116A0AB0A93C259B9844C2D86084F10C793C6 (void);
// 0x000000E4 FullSerializer.fsResult FullSerializer.Internal.fsDictionaryConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsDictionaryConverter_TryDeserialize_m9E370CCC4270DB6E2332F2B61C13DF99FE5B8D28 (void);
// 0x000000E5 FullSerializer.fsResult FullSerializer.Internal.fsDictionaryConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsDictionaryConverter_TrySerialize_m3EA99C3B4C0A4C201EACBAB9F6A121F8360CBE8C (void);
// 0x000000E6 FullSerializer.fsResult FullSerializer.Internal.fsDictionaryConverter::AddItemToDictionary(System.Collections.IDictionary,System.Object,System.Object)
extern void fsDictionaryConverter_AddItemToDictionary_m5780744D2E9A544BF108093E2C222FCE7B039763 (void);
// 0x000000E7 System.Void FullSerializer.Internal.fsDictionaryConverter::GetKeyValueTypes(System.Type,System.Type&,System.Type&)
extern void fsDictionaryConverter_GetKeyValueTypes_mE8343E439747221EC907412B2048FAB6F7E03BA3 (void);
// 0x000000E8 System.Void FullSerializer.Internal.fsDictionaryConverter::.ctor()
extern void fsDictionaryConverter__ctor_mFBEE65904B2D19F828A5160610CA219AA6E6F81B (void);
// 0x000000E9 System.Boolean FullSerializer.Internal.fsEnumConverter::CanProcess(System.Type)
extern void fsEnumConverter_CanProcess_m2CC1B27E327C14C78C3722776BC595D3E0D5D734 (void);
// 0x000000EA System.Boolean FullSerializer.Internal.fsEnumConverter::RequestCycleSupport(System.Type)
extern void fsEnumConverter_RequestCycleSupport_mCD0D4D34A8C8542AB472D8B13C9F5857384D931C (void);
// 0x000000EB System.Boolean FullSerializer.Internal.fsEnumConverter::RequestInheritanceSupport(System.Type)
extern void fsEnumConverter_RequestInheritanceSupport_mC8C24DADB1DCBEBC241D1CBF207C58EFF4D6AD71 (void);
// 0x000000EC System.Object FullSerializer.Internal.fsEnumConverter::CreateInstance(FullSerializer.fsData,System.Type)
extern void fsEnumConverter_CreateInstance_mCC77C55577AC4E4D201EE10E6D648AC6C251EBA2 (void);
// 0x000000ED FullSerializer.fsResult FullSerializer.Internal.fsEnumConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsEnumConverter_TrySerialize_m7D7886681B17167DC0B1D28EAB047D954BC2C5A5 (void);
// 0x000000EE FullSerializer.fsResult FullSerializer.Internal.fsEnumConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsEnumConverter_TryDeserialize_mCE91043CA547D5BE77B3487EF9D8FA7E74486F6E (void);
// 0x000000EF System.Boolean FullSerializer.Internal.fsEnumConverter::ArrayContains(T[],T)
// 0x000000F0 System.Void FullSerializer.Internal.fsEnumConverter::.ctor()
extern void fsEnumConverter__ctor_mD39F5DACA8CF34DE1849C08F7259C2BE3975C67C (void);
// 0x000000F1 System.Void FullSerializer.Internal.fsForwardConverter::.ctor(FullSerializer.fsForwardAttribute)
extern void fsForwardConverter__ctor_m3122D771F4B68D939408DBD69AFA85225D12E8D7 (void);
// 0x000000F2 System.Boolean FullSerializer.Internal.fsForwardConverter::CanProcess(System.Type)
extern void fsForwardConverter_CanProcess_m436A5F71B9A5D022FB54E1C0D1C10B25CD45330D (void);
// 0x000000F3 FullSerializer.fsResult FullSerializer.Internal.fsForwardConverter::GetProperty(System.Object,FullSerializer.Internal.fsMetaProperty&)
extern void fsForwardConverter_GetProperty_m8B0D2BDCF491DD8D99730141053CE777F3F6A5E5 (void);
// 0x000000F4 FullSerializer.fsResult FullSerializer.Internal.fsForwardConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsForwardConverter_TrySerialize_m282D0F38E1D37D4823F78CFD30A3D80ADF5A0F09 (void);
// 0x000000F5 FullSerializer.fsResult FullSerializer.Internal.fsForwardConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsForwardConverter_TryDeserialize_m0EC388A75980903DD116A5D141B36A432B703C7F (void);
// 0x000000F6 System.Object FullSerializer.Internal.fsForwardConverter::CreateInstance(FullSerializer.fsData,System.Type)
extern void fsForwardConverter_CreateInstance_m821856F88EAF66372CD983154860E666B2480CE7 (void);
// 0x000000F7 System.Boolean FullSerializer.Internal.fsGuidConverter::CanProcess(System.Type)
extern void fsGuidConverter_CanProcess_m7AD44FE61099E61A8FFCE9F649D1761242AD4000 (void);
// 0x000000F8 System.Boolean FullSerializer.Internal.fsGuidConverter::RequestCycleSupport(System.Type)
extern void fsGuidConverter_RequestCycleSupport_m88A1D5EF5DF4EFB68B2FB41E50446D1C7A1E45F5 (void);
// 0x000000F9 System.Boolean FullSerializer.Internal.fsGuidConverter::RequestInheritanceSupport(System.Type)
extern void fsGuidConverter_RequestInheritanceSupport_mD69648D56EF07D1BC41986DBAC3D9BE9AAA1D44E (void);
// 0x000000FA FullSerializer.fsResult FullSerializer.Internal.fsGuidConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsGuidConverter_TrySerialize_m86FB7F063BDE8879AB48190A8E2DF474B5FEFB52 (void);
// 0x000000FB FullSerializer.fsResult FullSerializer.Internal.fsGuidConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsGuidConverter_TryDeserialize_mBB1593E9F72B658DCEC48BE58596896DB76B9036 (void);
// 0x000000FC System.Object FullSerializer.Internal.fsGuidConverter::CreateInstance(FullSerializer.fsData,System.Type)
extern void fsGuidConverter_CreateInstance_m624F31F5BAA34E5AB181C110ACF1C49060DFD7A0 (void);
// 0x000000FD System.Void FullSerializer.Internal.fsGuidConverter::.ctor()
extern void fsGuidConverter__ctor_m3D21AF16315254F14B35164F028DD87830450690 (void);
// 0x000000FE System.Boolean FullSerializer.Internal.fsIEnumerableConverter::CanProcess(System.Type)
extern void fsIEnumerableConverter_CanProcess_mF332478FDF9DEA1ACAD8274E40DA4C3DBEF6616F (void);
// 0x000000FF System.Object FullSerializer.Internal.fsIEnumerableConverter::CreateInstance(FullSerializer.fsData,System.Type)
extern void fsIEnumerableConverter_CreateInstance_m8E890EB30EBB661A20B913B240ADFD3050BC46F0 (void);
// 0x00000100 FullSerializer.fsResult FullSerializer.Internal.fsIEnumerableConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsIEnumerableConverter_TrySerialize_mD370459F57194F2242B25CEE289A3D823F93AF68 (void);
// 0x00000101 System.Boolean FullSerializer.Internal.fsIEnumerableConverter::IsStack(System.Type)
extern void fsIEnumerableConverter_IsStack_m70C317AC78BBF149B37CAC7654D42A9528F5D354 (void);
// 0x00000102 FullSerializer.fsResult FullSerializer.Internal.fsIEnumerableConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsIEnumerableConverter_TryDeserialize_m25F35628ED115C7751F9E39B4761ED6D62913020 (void);
// 0x00000103 System.Int32 FullSerializer.Internal.fsIEnumerableConverter::HintSize(System.Collections.IEnumerable)
extern void fsIEnumerableConverter_HintSize_m6D545FCAC3FBFC6AC6658FDA2982408A9AEB5B30 (void);
// 0x00000104 System.Type FullSerializer.Internal.fsIEnumerableConverter::GetElementType(System.Type)
extern void fsIEnumerableConverter_GetElementType_m6D7F1EE4FC5095E740015A1D3DBDD0EBFC6D3D5A (void);
// 0x00000105 System.Void FullSerializer.Internal.fsIEnumerableConverter::TryClear(System.Type,System.Object)
extern void fsIEnumerableConverter_TryClear_mC15556022454D865C459E4A5228E372132DF2B61 (void);
// 0x00000106 System.Int32 FullSerializer.Internal.fsIEnumerableConverter::TryGetExistingSize(System.Type,System.Object)
extern void fsIEnumerableConverter_TryGetExistingSize_m6C91B5DBFF946A8101FFD372211A1BC03CB567FC (void);
// 0x00000107 System.Reflection.MethodInfo FullSerializer.Internal.fsIEnumerableConverter::GetAddMethod(System.Type)
extern void fsIEnumerableConverter_GetAddMethod_m492687B961C7780CBACE58DCC49183EADE31697F (void);
// 0x00000108 System.Void FullSerializer.Internal.fsIEnumerableConverter::.ctor()
extern void fsIEnumerableConverter__ctor_mA31125C0A06077DA32216D56B612E4CE55A01B91 (void);
// 0x00000109 System.Boolean FullSerializer.Internal.fsKeyValuePairConverter::CanProcess(System.Type)
extern void fsKeyValuePairConverter_CanProcess_mD5C0A501C9605550681D72AE8937CFF841E3D470 (void);
// 0x0000010A System.Boolean FullSerializer.Internal.fsKeyValuePairConverter::RequestCycleSupport(System.Type)
extern void fsKeyValuePairConverter_RequestCycleSupport_mBB3EE2138663A620953BEB26CB7061D64C65C768 (void);
// 0x0000010B System.Boolean FullSerializer.Internal.fsKeyValuePairConverter::RequestInheritanceSupport(System.Type)
extern void fsKeyValuePairConverter_RequestInheritanceSupport_m04CBDAF7516E408AD938FD20A00BACC759CA27D8 (void);
// 0x0000010C FullSerializer.fsResult FullSerializer.Internal.fsKeyValuePairConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsKeyValuePairConverter_TryDeserialize_m6E8B1E202DFEE0BCF1665D69D6D70ADD8AA199AF (void);
// 0x0000010D FullSerializer.fsResult FullSerializer.Internal.fsKeyValuePairConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsKeyValuePairConverter_TrySerialize_m2CC70BF863CF20EF06A5463A4A38C8A01B97CE83 (void);
// 0x0000010E System.Void FullSerializer.Internal.fsKeyValuePairConverter::.ctor()
extern void fsKeyValuePairConverter__ctor_mEF52DAC721E15BFAD57B4BBC6A95722750769551 (void);
// 0x0000010F System.Boolean FullSerializer.Internal.fsNullableConverter::CanProcess(System.Type)
extern void fsNullableConverter_CanProcess_m5AD935506F8D8B3F032EFC1AF6313EADBB639B43 (void);
// 0x00000110 FullSerializer.fsResult FullSerializer.Internal.fsNullableConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsNullableConverter_TrySerialize_mE518482C071C0E2B9C3466D520E6D2A1DCCB0371 (void);
// 0x00000111 FullSerializer.fsResult FullSerializer.Internal.fsNullableConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsNullableConverter_TryDeserialize_m5E6D9E4498F91F649CAB1D62127CCDBC47B281F9 (void);
// 0x00000112 System.Object FullSerializer.Internal.fsNullableConverter::CreateInstance(FullSerializer.fsData,System.Type)
extern void fsNullableConverter_CreateInstance_m0CA3A71214FB0CF5C5B74F01470AB554B7FE10A2 (void);
// 0x00000113 System.Void FullSerializer.Internal.fsNullableConverter::.ctor()
extern void fsNullableConverter__ctor_m0AF68115DBA4C368E58DADC1C537B88C1653552A (void);
// 0x00000114 System.Boolean FullSerializer.Internal.fsPrimitiveConverter::CanProcess(System.Type)
extern void fsPrimitiveConverter_CanProcess_m116C5AF57DA7FA90D89842880E88226FDA70C2FF (void);
// 0x00000115 System.Boolean FullSerializer.Internal.fsPrimitiveConverter::RequestCycleSupport(System.Type)
extern void fsPrimitiveConverter_RequestCycleSupport_m8B7A1DF8A9DB84DE3C841976F815B0F53D9EF85D (void);
// 0x00000116 System.Boolean FullSerializer.Internal.fsPrimitiveConverter::RequestInheritanceSupport(System.Type)
extern void fsPrimitiveConverter_RequestInheritanceSupport_mEC8C785E28075BDFFF9E3B05F1909BCA0B8552B9 (void);
// 0x00000117 System.Boolean FullSerializer.Internal.fsPrimitiveConverter::UseBool(System.Type)
extern void fsPrimitiveConverter_UseBool_mBDBC77DA32FCC70B8364A833FC6FEDE9F518BBE4 (void);
// 0x00000118 System.Boolean FullSerializer.Internal.fsPrimitiveConverter::UseInt64(System.Type)
extern void fsPrimitiveConverter_UseInt64_mC7CA889263AD304472FB57F3552A61A58AC12DCD (void);
// 0x00000119 System.Boolean FullSerializer.Internal.fsPrimitiveConverter::UseDouble(System.Type)
extern void fsPrimitiveConverter_UseDouble_mC19B43880692E6F2BCF687D29080577F4DC0BFD9 (void);
// 0x0000011A System.Boolean FullSerializer.Internal.fsPrimitiveConverter::UseString(System.Type)
extern void fsPrimitiveConverter_UseString_m3B727CA46D206F785A72E303048A713C8791CA0C (void);
// 0x0000011B FullSerializer.fsResult FullSerializer.Internal.fsPrimitiveConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsPrimitiveConverter_TrySerialize_m0368752F59C6E171971B06A63B2E1D1550F7DF26 (void);
// 0x0000011C FullSerializer.fsResult FullSerializer.Internal.fsPrimitiveConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsPrimitiveConverter_TryDeserialize_mB945799F8479FDD03CBB1644064AF3DB962DCC96 (void);
// 0x0000011D System.Void FullSerializer.Internal.fsPrimitiveConverter::.ctor()
extern void fsPrimitiveConverter__ctor_mCF9C078AF65749200BF02E125475BF99DD4CCFF3 (void);
// 0x0000011E System.Boolean FullSerializer.Internal.fsReflectedConverter::CanProcess(System.Type)
extern void fsReflectedConverter_CanProcess_m1C7097A1FE9B31DBD1BC3F32703082C8C71B2189 (void);
// 0x0000011F FullSerializer.fsResult FullSerializer.Internal.fsReflectedConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsReflectedConverter_TrySerialize_m860FBBDE2A4A1187AA16717025710676DF8AF737 (void);
// 0x00000120 FullSerializer.fsResult FullSerializer.Internal.fsReflectedConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsReflectedConverter_TryDeserialize_mC1BB2F7321306B1A9D2A8BDA3D4A3A5FD39E0547 (void);
// 0x00000121 System.Object FullSerializer.Internal.fsReflectedConverter::CreateInstance(FullSerializer.fsData,System.Type)
extern void fsReflectedConverter_CreateInstance_m7C340379381192064EEF7C11F30A4BEB8CB7DE5E (void);
// 0x00000122 System.Void FullSerializer.Internal.fsReflectedConverter::.ctor()
extern void fsReflectedConverter__ctor_m0C320BAF8D6D0D83BA02BBD481731A1B4D62F876 (void);
// 0x00000123 System.Boolean FullSerializer.Internal.fsTypeConverter::CanProcess(System.Type)
extern void fsTypeConverter_CanProcess_m3A8444470D10B2E651E16999F3F80D9D3C613590 (void);
// 0x00000124 System.Boolean FullSerializer.Internal.fsTypeConverter::RequestCycleSupport(System.Type)
extern void fsTypeConverter_RequestCycleSupport_m71319471317A38508ADDBE2BC42E7A4267492C6C (void);
// 0x00000125 System.Boolean FullSerializer.Internal.fsTypeConverter::RequestInheritanceSupport(System.Type)
extern void fsTypeConverter_RequestInheritanceSupport_mE3ACFAAE1FD9C16F93D251CEE5A51C0A508C4661 (void);
// 0x00000126 FullSerializer.fsResult FullSerializer.Internal.fsTypeConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsTypeConverter_TrySerialize_mEA024B54FE9A829B240F64A6B9FB00682A440004 (void);
// 0x00000127 FullSerializer.fsResult FullSerializer.Internal.fsTypeConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsTypeConverter_TryDeserialize_mE91A3B3358CA002E0EC9F64818F4C04FF065A7CA (void);
// 0x00000128 System.Object FullSerializer.Internal.fsTypeConverter::CreateInstance(FullSerializer.fsData,System.Type)
extern void fsTypeConverter_CreateInstance_mB24C7F3B762541C83DA1206E67E6C6AC0E49D839 (void);
// 0x00000129 System.Void FullSerializer.Internal.fsTypeConverter::.ctor()
extern void fsTypeConverter__ctor_mDD227968F22A01786684FC69935B89894DEA2D5C (void);
// 0x0000012A System.Boolean FullSerializer.Internal.fsWeakReferenceConverter::CanProcess(System.Type)
extern void fsWeakReferenceConverter_CanProcess_m1CD7B3E14C7C36F9347DBEF6CA2953E99A57E5D4 (void);
// 0x0000012B System.Boolean FullSerializer.Internal.fsWeakReferenceConverter::RequestCycleSupport(System.Type)
extern void fsWeakReferenceConverter_RequestCycleSupport_m4C42171E8AFCEFC06C609C6BB43245C1B1F951BA (void);
// 0x0000012C System.Boolean FullSerializer.Internal.fsWeakReferenceConverter::RequestInheritanceSupport(System.Type)
extern void fsWeakReferenceConverter_RequestInheritanceSupport_m70C634B09BA5043F44043723F35EB540F6BFE8A7 (void);
// 0x0000012D FullSerializer.fsResult FullSerializer.Internal.fsWeakReferenceConverter::TrySerialize(System.Object,FullSerializer.fsData&,System.Type)
extern void fsWeakReferenceConverter_TrySerialize_mA5D0012B90EB8F7E985827178BFFF489536CC563 (void);
// 0x0000012E FullSerializer.fsResult FullSerializer.Internal.fsWeakReferenceConverter::TryDeserialize(FullSerializer.fsData,System.Object&,System.Type)
extern void fsWeakReferenceConverter_TryDeserialize_mD6399A749ADA73471D7F1BA8770F31DE1793FACE (void);
// 0x0000012F System.Object FullSerializer.Internal.fsWeakReferenceConverter::CreateInstance(FullSerializer.fsData,System.Type)
extern void fsWeakReferenceConverter_CreateInstance_m86A716E05DF20BE5974EF1ACDD1085F94AAF6D35 (void);
// 0x00000130 System.Void FullSerializer.Internal.fsWeakReferenceConverter::.ctor()
extern void fsWeakReferenceConverter__ctor_m533DB1F2512F46B6139FD3B3AE233DCB7623C4FE (void);
// 0x00000131 System.Boolean FullSerializer.Internal.fsSerializationCallbackProcessor::CanProcess(System.Type)
extern void fsSerializationCallbackProcessor_CanProcess_mB20A08DBF8C0D0CE6B736B1A9091F3B61C48C105 (void);
// 0x00000132 System.Void FullSerializer.Internal.fsSerializationCallbackProcessor::OnBeforeSerialize(System.Type,System.Object)
extern void fsSerializationCallbackProcessor_OnBeforeSerialize_m8E9974D8C7548DC0B8B560CDD976E765E4D4FADE (void);
// 0x00000133 System.Void FullSerializer.Internal.fsSerializationCallbackProcessor::OnAfterSerialize(System.Type,System.Object,FullSerializer.fsData&)
extern void fsSerializationCallbackProcessor_OnAfterSerialize_mDB4957BE4D7289ACC32ACF16231105F5E1666E27 (void);
// 0x00000134 System.Void FullSerializer.Internal.fsSerializationCallbackProcessor::OnBeforeDeserializeAfterInstanceCreation(System.Type,System.Object,FullSerializer.fsData&)
extern void fsSerializationCallbackProcessor_OnBeforeDeserializeAfterInstanceCreation_m5B9E14A3D05B8CEA9067C217E644CA2D12DBD824 (void);
// 0x00000135 System.Void FullSerializer.Internal.fsSerializationCallbackProcessor::OnAfterDeserialize(System.Type,System.Object)
extern void fsSerializationCallbackProcessor_OnAfterDeserialize_mFB1C79367BF24E46B5ADDD7FC41B409144A3F0F5 (void);
// 0x00000136 System.Void FullSerializer.Internal.fsSerializationCallbackProcessor::.ctor()
extern void fsSerializationCallbackProcessor__ctor_mBE9B03156E4E821247C8F0FAD3CD16497E64BC10 (void);
// 0x00000137 System.Boolean FullSerializer.Internal.fsSerializationCallbackReceiverProcessor::CanProcess(System.Type)
extern void fsSerializationCallbackReceiverProcessor_CanProcess_m041F8D542F630F4852DA3CFF5CC2C8BD7F99BBBA (void);
// 0x00000138 System.Void FullSerializer.Internal.fsSerializationCallbackReceiverProcessor::OnBeforeSerialize(System.Type,System.Object)
extern void fsSerializationCallbackReceiverProcessor_OnBeforeSerialize_mD56FBF412F1C38A40A141DA56BBEE72A057DBCA1 (void);
// 0x00000139 System.Void FullSerializer.Internal.fsSerializationCallbackReceiverProcessor::OnAfterDeserialize(System.Type,System.Object)
extern void fsSerializationCallbackReceiverProcessor_OnAfterDeserialize_mDAB98255E654770C62EBA7E44249562D3C98961E (void);
// 0x0000013A System.Void FullSerializer.Internal.fsSerializationCallbackReceiverProcessor::.ctor()
extern void fsSerializationCallbackReceiverProcessor__ctor_m5516C0B0D8B6CF935A2F906CAB48F8AE0B6004A0 (void);
// 0x0000013B System.Void FullSerializer.Internal.fsCyclicReferenceManager::Enter()
extern void fsCyclicReferenceManager_Enter_m58E6DDD0CBD151BEBDAAE9B0CCE5A77BE3F8FC04 (void);
// 0x0000013C System.Boolean FullSerializer.Internal.fsCyclicReferenceManager::Exit()
extern void fsCyclicReferenceManager_Exit_m3C0C0291572DA2A40200DABA32773F0EDA99FF92 (void);
// 0x0000013D System.Object FullSerializer.Internal.fsCyclicReferenceManager::GetReferenceObject(System.Int32)
extern void fsCyclicReferenceManager_GetReferenceObject_m5E1B77539D1A797643ADCD715D1DDAFDB04E9836 (void);
// 0x0000013E System.Void FullSerializer.Internal.fsCyclicReferenceManager::AddReferenceWithId(System.Int32,System.Object)
extern void fsCyclicReferenceManager_AddReferenceWithId_m858FAC1C2C579F8089C495E81C223475916D9C76 (void);
// 0x0000013F System.Int32 FullSerializer.Internal.fsCyclicReferenceManager::GetReferenceId(System.Object)
extern void fsCyclicReferenceManager_GetReferenceId_mF3ACBFE709CACB9548C5CC1CF5E8137B89368BFF (void);
// 0x00000140 System.Boolean FullSerializer.Internal.fsCyclicReferenceManager::IsReference(System.Object)
extern void fsCyclicReferenceManager_IsReference_mD2276E941273B839AF93CF9FEF7F078BD2AB5707 (void);
// 0x00000141 System.Void FullSerializer.Internal.fsCyclicReferenceManager::MarkSerialized(System.Object)
extern void fsCyclicReferenceManager_MarkSerialized_m6FD82154B18CF301679234E6BAF97C7B42DB35D1 (void);
// 0x00000142 System.Void FullSerializer.Internal.fsCyclicReferenceManager::.ctor()
extern void fsCyclicReferenceManager__ctor_mD4943AE432A11DE13F27DC617740B492BE944FD4 (void);
// 0x00000143 System.Boolean FullSerializer.Internal.fsCyclicReferenceManager_ObjectReferenceEqualityComparator::System.Collections.Generic.IEqualityComparer<System.Object>.Equals(System.Object,System.Object)
extern void ObjectReferenceEqualityComparator_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_Equals_m11104763EC9F33C807338311AD2DCBFD13AFB3D3 (void);
// 0x00000144 System.Int32 FullSerializer.Internal.fsCyclicReferenceManager_ObjectReferenceEqualityComparator::System.Collections.Generic.IEqualityComparer<System.Object>.GetHashCode(System.Object)
extern void ObjectReferenceEqualityComparator_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_GetHashCode_m079FDA17639618D647E141539A47599D6D049F15 (void);
// 0x00000145 System.Void FullSerializer.Internal.fsCyclicReferenceManager_ObjectReferenceEqualityComparator::.ctor()
extern void ObjectReferenceEqualityComparator__ctor_mB21EBD7D76738113CC3A12676D02DED7D3E79EB7 (void);
// 0x00000146 System.Void FullSerializer.Internal.fsCyclicReferenceManager_ObjectReferenceEqualityComparator::.cctor()
extern void ObjectReferenceEqualityComparator__cctor_m07A51BEB553F802680C96CAE3A6485F9EFCF7AC7 (void);
// 0x00000147 System.Boolean FullSerializer.Internal.fsOption`1::get_HasValue()
// 0x00000148 System.Boolean FullSerializer.Internal.fsOption`1::get_IsEmpty()
// 0x00000149 T FullSerializer.Internal.fsOption`1::get_Value()
// 0x0000014A System.Void FullSerializer.Internal.fsOption`1::.ctor(T)
// 0x0000014B FullSerializer.Internal.fsOption`1<T> FullSerializer.Internal.fsOption::Just(T)
// 0x0000014C System.Boolean FullSerializer.Internal.fsPortableReflection::HasAttribute(System.Reflection.MemberInfo,System.Type)
extern void fsPortableReflection_HasAttribute_mC193D9B4B81117E1EE77FF8BAE5C85A86660EA7F (void);
// 0x0000014D System.Boolean FullSerializer.Internal.fsPortableReflection::HasAttribute(System.Reflection.MemberInfo,System.Type,System.Boolean)
extern void fsPortableReflection_HasAttribute_m8E68968BA00BB48075B32531EF9D60663BA766E4 (void);
// 0x0000014E System.Attribute FullSerializer.Internal.fsPortableReflection::GetAttribute(System.Reflection.MemberInfo,System.Type,System.Boolean)
extern void fsPortableReflection_GetAttribute_m53782C927761C4FBCE5C1FA9AC00B5B09EBCB747 (void);
// 0x0000014F TAttribute FullSerializer.Internal.fsPortableReflection::GetAttribute(System.Reflection.MemberInfo,System.Boolean)
// 0x00000150 TAttribute FullSerializer.Internal.fsPortableReflection::GetAttribute(System.Reflection.MemberInfo)
// 0x00000151 System.Reflection.PropertyInfo FullSerializer.Internal.fsPortableReflection::GetDeclaredProperty(System.Type,System.String)
extern void fsPortableReflection_GetDeclaredProperty_m5EFE34BADF45559AA2ECF8085DE9CDB79F2898AA (void);
// 0x00000152 System.Reflection.MethodInfo FullSerializer.Internal.fsPortableReflection::GetDeclaredMethod(System.Type,System.String)
extern void fsPortableReflection_GetDeclaredMethod_m39C7E61BAE99B3ECC6C8365854C3D477D67F9885 (void);
// 0x00000153 System.Reflection.ConstructorInfo FullSerializer.Internal.fsPortableReflection::GetDeclaredConstructor(System.Type,System.Type[])
extern void fsPortableReflection_GetDeclaredConstructor_mFBEC50CE1160B7A404137E45DB56EAA3CE1FE308 (void);
// 0x00000154 System.Reflection.ConstructorInfo[] FullSerializer.Internal.fsPortableReflection::GetDeclaredConstructors(System.Type)
extern void fsPortableReflection_GetDeclaredConstructors_m87013D2231E45F8357911CD57C60A71DFA7E329E (void);
// 0x00000155 System.Reflection.MethodInfo FullSerializer.Internal.fsPortableReflection::GetFlattenedMethod(System.Type,System.String)
extern void fsPortableReflection_GetFlattenedMethod_m1A7658BA15F4C7828B1EC4E4D0B0B42E270FF67F (void);
// 0x00000156 System.Reflection.PropertyInfo FullSerializer.Internal.fsPortableReflection::GetFlattenedProperty(System.Type,System.String)
extern void fsPortableReflection_GetFlattenedProperty_mC547515DEF313C5215EF6D99504E6CE64C3A05B3 (void);
// 0x00000157 System.Reflection.MethodInfo[] FullSerializer.Internal.fsPortableReflection::GetDeclaredMethods(System.Type)
extern void fsPortableReflection_GetDeclaredMethods_mD206EEABDA024B8DAE981AA6ACF7CEA351961E00 (void);
// 0x00000158 System.Reflection.PropertyInfo[] FullSerializer.Internal.fsPortableReflection::GetDeclaredProperties(System.Type)
extern void fsPortableReflection_GetDeclaredProperties_mBC31F9584DA0FC6281A97D0AB76E07DBBF599E13 (void);
// 0x00000159 System.Reflection.FieldInfo[] FullSerializer.Internal.fsPortableReflection::GetDeclaredFields(System.Type)
extern void fsPortableReflection_GetDeclaredFields_mBBCBA8A61D2377852B746388F8B05D15F353CBAF (void);
// 0x0000015A System.Reflection.MemberInfo[] FullSerializer.Internal.fsPortableReflection::GetDeclaredMembers(System.Type)
extern void fsPortableReflection_GetDeclaredMembers_mA60D39CFE264BA0C43D23BB6BDB9CFA46DC44648 (void);
// 0x0000015B System.Type FullSerializer.Internal.fsPortableReflection::Resolve(System.Type)
extern void fsPortableReflection_Resolve_m3210E900E0B4AADCC533E0B40EAC6CD20B3ED847 (void);
// 0x0000015C System.Void FullSerializer.Internal.fsPortableReflection::.cctor()
extern void fsPortableReflection__cctor_m9EEEBD307F4EBF8CAE6A7FB25656B708FF2E62E1 (void);
// 0x0000015D System.Boolean FullSerializer.Internal.fsPortableReflection_AttributeQueryComparator::Equals(FullSerializer.Internal.fsPortableReflection_AttributeQuery,FullSerializer.Internal.fsPortableReflection_AttributeQuery)
extern void AttributeQueryComparator_Equals_mFA7777007C909658410153E9261EE521AECE6BC3 (void);
// 0x0000015E System.Int32 FullSerializer.Internal.fsPortableReflection_AttributeQueryComparator::GetHashCode(FullSerializer.Internal.fsPortableReflection_AttributeQuery)
extern void AttributeQueryComparator_GetHashCode_m099ED57ADC98C03EC737FA1419ECF0EECDEF6094 (void);
// 0x0000015F System.Void FullSerializer.Internal.fsPortableReflection_AttributeQueryComparator::.ctor()
extern void AttributeQueryComparator__ctor_mCD4C24E7CEF33B43AD8A4BB8607096BB2E6905E1 (void);
// 0x00000160 System.Object FullSerializer.Internal.fsVersionedType::Migrate(System.Object)
extern void fsVersionedType_Migrate_m439EC7E6E73C38BB4A269DA59B20FE22205A1831_AdjustorThunk (void);
// 0x00000161 System.String FullSerializer.Internal.fsVersionedType::ToString()
extern void fsVersionedType_ToString_m8C4111D55FA21DE440FEC4488AB07E6F4B316AC5_AdjustorThunk (void);
// 0x00000162 System.Boolean FullSerializer.Internal.fsVersionedType::Equals(System.Object)
extern void fsVersionedType_Equals_mA100885FB81984E2880836B09D5BD026887356BC_AdjustorThunk (void);
// 0x00000163 System.Int32 FullSerializer.Internal.fsVersionedType::GetHashCode()
extern void fsVersionedType_GetHashCode_mAD0932F3F4306EBA1F5F0904DED77F990EE755F3_AdjustorThunk (void);
// 0x00000164 FullSerializer.fsResult FullSerializer.Internal.fsVersionManager::GetVersionImportPath(System.String,FullSerializer.Internal.fsVersionedType,System.Collections.Generic.List`1<FullSerializer.Internal.fsVersionedType>&)
extern void fsVersionManager_GetVersionImportPath_m12B83D9F9F6E11C9C46C0A7283947373CA2FC14B (void);
// 0x00000165 System.Boolean FullSerializer.Internal.fsVersionManager::GetVersionImportPathRecursive(System.Collections.Generic.List`1<FullSerializer.Internal.fsVersionedType>,System.String,FullSerializer.Internal.fsVersionedType)
extern void fsVersionManager_GetVersionImportPathRecursive_mB40DDC3D620C4303507F6B7ECAD65F06FD220C67 (void);
// 0x00000166 FullSerializer.Internal.fsOption`1<FullSerializer.Internal.fsVersionedType> FullSerializer.Internal.fsVersionManager::GetVersionedType(System.Type)
extern void fsVersionManager_GetVersionedType_m54CA5F1A4A07313EAE276E690A9AED0BD688A2B8 (void);
// 0x00000167 System.Void FullSerializer.Internal.fsVersionManager::VerifyConstructors(FullSerializer.Internal.fsVersionedType)
extern void fsVersionManager_VerifyConstructors_m23E752411E16CE828C25D9E58D4D2405B7A65243 (void);
// 0x00000168 System.Void FullSerializer.Internal.fsVersionManager::VerifyUniqueVersionStrings(FullSerializer.Internal.fsVersionedType)
extern void fsVersionManager_VerifyUniqueVersionStrings_mA9FBB9959012D319F154399D1F877EA0994ED47D (void);
// 0x00000169 System.Void FullSerializer.Internal.fsVersionManager::.cctor()
extern void fsVersionManager__cctor_m31C87DC0AE466A11DBA356F5E81DD9D81ECC2311 (void);
// 0x0000016A System.Void FullSerializer.Internal.fsMetaProperty::.ctor(FullSerializer.fsConfig,System.Reflection.FieldInfo)
extern void fsMetaProperty__ctor_m63917891A0F46B096287A7CBEB4AE4476B18FE34 (void);
// 0x0000016B System.Void FullSerializer.Internal.fsMetaProperty::.ctor(FullSerializer.fsConfig,System.Reflection.PropertyInfo)
extern void fsMetaProperty__ctor_m78B20EE411B9BF09EE58955BF452691EDA36CE2C (void);
// 0x0000016C System.Void FullSerializer.Internal.fsMetaProperty::CommonInitialize(FullSerializer.fsConfig)
extern void fsMetaProperty_CommonInitialize_mB8DAA6A5C117B3F345C7E5E03DB0DAAFD792FB53 (void);
// 0x0000016D System.Type FullSerializer.Internal.fsMetaProperty::get_StorageType()
extern void fsMetaProperty_get_StorageType_m269BF18DBE46AB8BCA6C5992E82C4A1BBB2F079B (void);
// 0x0000016E System.Void FullSerializer.Internal.fsMetaProperty::set_StorageType(System.Type)
extern void fsMetaProperty_set_StorageType_mA7B8A6541BA41FBA0E4F6466B5663D86EAB90A70 (void);
// 0x0000016F System.Type FullSerializer.Internal.fsMetaProperty::get_OverrideConverterType()
extern void fsMetaProperty_get_OverrideConverterType_m587F21F7CFFC5F81F642925F4D6764942D9887FE (void);
// 0x00000170 System.Void FullSerializer.Internal.fsMetaProperty::set_OverrideConverterType(System.Type)
extern void fsMetaProperty_set_OverrideConverterType_m501045ADD8FBD138C85EB5A2D735876977210585 (void);
// 0x00000171 System.Boolean FullSerializer.Internal.fsMetaProperty::get_CanRead()
extern void fsMetaProperty_get_CanRead_m83D837742308E46EA03B99B5662782E12AD297B5 (void);
// 0x00000172 System.Void FullSerializer.Internal.fsMetaProperty::set_CanRead(System.Boolean)
extern void fsMetaProperty_set_CanRead_m11615509F7E45E8B97A4505587E74D7842BD8B50 (void);
// 0x00000173 System.Boolean FullSerializer.Internal.fsMetaProperty::get_CanWrite()
extern void fsMetaProperty_get_CanWrite_mA432592BE154B5F7631344184EACE002819561A7 (void);
// 0x00000174 System.Void FullSerializer.Internal.fsMetaProperty::set_CanWrite(System.Boolean)
extern void fsMetaProperty_set_CanWrite_m30273D87D256A748E0EF5711990AF2466DB1D90E (void);
// 0x00000175 System.String FullSerializer.Internal.fsMetaProperty::get_JsonName()
extern void fsMetaProperty_get_JsonName_m2C9F17D4C28E0E092EA39C5621DAFE9D7C1BE94F (void);
// 0x00000176 System.Void FullSerializer.Internal.fsMetaProperty::set_JsonName(System.String)
extern void fsMetaProperty_set_JsonName_m9964BDDC354DB02C1D384F828E5FA2CF29C456BA (void);
// 0x00000177 System.String FullSerializer.Internal.fsMetaProperty::get_MemberName()
extern void fsMetaProperty_get_MemberName_m9A4E46BD5B896D3A9C06DAD1CF38D909F53F9044 (void);
// 0x00000178 System.Void FullSerializer.Internal.fsMetaProperty::set_MemberName(System.String)
extern void fsMetaProperty_set_MemberName_mE0A7D25E375D310C545F8399A8C7AF052341FF31 (void);
// 0x00000179 System.Boolean FullSerializer.Internal.fsMetaProperty::get_IsPublic()
extern void fsMetaProperty_get_IsPublic_mCFF5F3118FC99F7B0B214E3684CEECA88C58CA0B (void);
// 0x0000017A System.Void FullSerializer.Internal.fsMetaProperty::set_IsPublic(System.Boolean)
extern void fsMetaProperty_set_IsPublic_m483015FF435A2870B40BCC1E47637A44D2B606A3 (void);
// 0x0000017B System.Boolean FullSerializer.Internal.fsMetaProperty::get_IsReadOnly()
extern void fsMetaProperty_get_IsReadOnly_m9A12681B1A7E2B7B0BD90CD9196F438CCCF3EAA2 (void);
// 0x0000017C System.Void FullSerializer.Internal.fsMetaProperty::set_IsReadOnly(System.Boolean)
extern void fsMetaProperty_set_IsReadOnly_m79407798A01B4B9922114DE9C49DDC35BECC3E2A (void);
// 0x0000017D System.Void FullSerializer.Internal.fsMetaProperty::Write(System.Object,System.Object)
extern void fsMetaProperty_Write_mE9503E7A4D405161D3349D5138C984F86AEEB87A (void);
// 0x0000017E System.Object FullSerializer.Internal.fsMetaProperty::Read(System.Object)
extern void fsMetaProperty_Read_m5A3517EFEF236FC8A9EF5F99B3CF2AEAE65E5F30 (void);
// 0x0000017F System.Type FullSerializer.Internal.fsReflectionUtility::GetInterface(System.Type,System.Type)
extern void fsReflectionUtility_GetInterface_m877F77A78B3D3D8C046CFAC6E866263395D9F9B1 (void);
// 0x00000180 System.Void FullSerializer.Internal.fsTypeCache::.cctor()
extern void fsTypeCache__cctor_mB905555273D9B7CA7D35BD573A4B355367911BA3 (void);
// 0x00000181 System.Void FullSerializer.Internal.fsTypeCache::OnAssemblyLoaded(System.Object,System.AssemblyLoadEventArgs)
extern void fsTypeCache_OnAssemblyLoaded_m42910E3CD4FBE67B3417605E924EEFD443987DEA (void);
// 0x00000182 System.Boolean FullSerializer.Internal.fsTypeCache::TryDirectTypeLookup(System.String,System.String,System.Type&)
extern void fsTypeCache_TryDirectTypeLookup_m0CD7F1B3D40F3E92CD80E441A8A281CC9BA5647C (void);
// 0x00000183 System.Boolean FullSerializer.Internal.fsTypeCache::TryIndirectTypeLookup(System.String,System.Type&)
extern void fsTypeCache_TryIndirectTypeLookup_mF31900D05CD5DA246B7FDDC0C33994D554BC70E3 (void);
// 0x00000184 System.Type FullSerializer.Internal.fsTypeCache::GetType(System.String)
extern void fsTypeCache_GetType_m2FCF8B5971C64DC8FDF6823CB1B43703A957B8CD (void);
// 0x00000185 System.Type FullSerializer.Internal.fsTypeCache::GetType(System.String,System.String)
extern void fsTypeCache_GetType_mEF9CFFB08266C359A8CE8B070E9A3841907827FF (void);
static Il2CppMethodPointer s_methodPointers[389] = 
{
	fsAotCompilationManager_HasMember_m66DDC47A65C25A5D8D4D2430C12E6115CA0D3CEF,
	fsAotCompilationManager_IsAotModelUpToDate_m46DD2E8A5A60175B5D6B0EA71F1CBD23EBE7D447,
	fsAotCompilationManager__cctor_m6055098E5D14A592DCF27E323110E96637372EA8,
	fsAotConfiguration_TryFindEntry_m5893FD04F254270095912923A82B50F82729372C,
	fsAotConfiguration_UpdateOrAddEntry_m2D4CCBA9EC142ACA22F854DBDB3021AB70119E7F,
	fsAotConfiguration__ctor_m35D09998C41E61E1B3CD143DD1803638B949EB1A,
	Member__ctor_m181EBAB12DB4A72885AFB6506C50B423DEBF34D9_AdjustorThunk,
	Member_Equals_m83CBA4E72F8BE5A3FAA1983BE9D4A637D9B550B5_AdjustorThunk,
	Member_GetHashCode_m607F6829A84289125664739469AD2DEF6E699520_AdjustorThunk,
	Member_op_Equality_mF001F4E8A22D795DE519F2D39B791649466A6F10,
	NULL,
	NULL,
	fsConverterRegistrar__cctor_mDD66D604F371F73FFAACB55A260CC57820537D49,
	fsBaseConverter_CreateInstance_mD08461D25F4388F566175E59CEE6B16A8E8F315B,
	fsBaseConverter_RequestCycleSupport_mA99440DC57D745CEB1FE368B768BA37D8C387FFA,
	fsBaseConverter_RequestInheritanceSupport_m5712B5BF26F4AC67AF1BFF2F6EAF3974DAC685C7,
	NULL,
	NULL,
	fsBaseConverter_FailExpectedType_m7A879F5F63FA9AA7C23B40383BA1D29E3BD835A7,
	fsBaseConverter_CheckType_mACFF822768709E985EA98A2C7AB8A2707C3049D9,
	fsBaseConverter_CheckKey_m383C74FE0CA032E50C8EF955F5C7E70B7E00E2FF,
	fsBaseConverter_CheckKey_m63950C633C1F1C72A15EC9DF85340CD5DBB57AFB,
	NULL,
	NULL,
	fsBaseConverter__ctor_m4394D0608D06A61924AEB97244F4EFD896B3CF5D,
	U3CU3Ec__cctor_mA5E0BB6B1B51FE2AD655603319D00118F00350B1,
	U3CU3Ec__ctor_m6170F4437798D08AEB0EA10BFFB60BF8F5A16A15,
	U3CU3Ec_U3CFailExpectedTypeU3Eb__6_0_m5ECAF6BA7525D7F2D963B681DFD472502430C491,
	fsGlobalConfig__cctor_mF26298670949647B9EABFC275FD844F9BE0B3C68,
	fsConfig__ctor_m57C4CC6949E28C36BC571D9D5B2B873D5A859E74,
	U3CU3Ec__cctor_m3608C110B6A50A5FC8C4E00AC8795CB1D546E664,
	U3CU3Ec__ctor_m043E6F36878A7A1C88A49E5448595021FA512782,
	U3CU3Ec_U3C_ctorU3Eb__11_0_m2904DACE629D2354154DE429C0D9CD9F42E7090F,
	NULL,
	NULL,
	fsContext__ctor_m04AEE56509EA89E24FB5CC180CA1E10446D2E3F4,
	NULL,
	fsConverter__ctor_m128C860A18FCA4ADE7AE973F67E6F5610A311810,
	fsData__ctor_mC8D82C31F29BFAE52982A720221D210121185CB7,
	fsData__ctor_m93E085E9D60FEC28307C480D920596EE792B7CC9,
	fsData__ctor_m822DE439103E3DDA8826208CF9A85DF688A12BBA,
	fsData__ctor_m0929EC78A236579973BD6F91FB552AA5DCB3C49F,
	fsData__ctor_mC147F4BD92B3CF84E81C93DAC07DC9757BB6C1EA,
	fsData__ctor_mD629E2C45F8DFD34B8555B3F2725E84662101BF4,
	fsData__ctor_m51D7CCCE42EFB72C8CD7F83DB2A475671A4C27C5,
	fsData_CreateDictionary_m1288D9EBE32243B57226CEBD1DE1B2953C9F1CE8,
	fsData_CreateList_mDE68F5F7DCF61BCD2B46A452AF6E9F52C14DAE24,
	fsData_BecomeDictionary_mA48BEF00EDDE512D34A1BC15B7000793B074BC04,
	fsData_Clone_mB1BE6F037078857E438663E91308E91286075E1A,
	fsData_get_Type_m99176BD2C65FAAF228ACC532373D686E51114D4C,
	fsData_get_IsNull_mB4A35DB48C95508F759A0E312556ACB92729D66F,
	fsData_get_IsDouble_mF71B7633267340E6727F409675969F903A6E1C70,
	fsData_get_IsInt64_m1BD8C0AA38BB6D445C4B32B0329C907970E2560C,
	fsData_get_IsBool_m99072802C0DA49D31737688D91BAA4C737711144,
	fsData_get_IsString_m6EC7158047914D70B1A29B1DF9E943DA81F23E77,
	fsData_get_IsDictionary_mEE2E3BF69B445172B0B9D6765DE9F3DBC371B3D3,
	fsData_get_IsList_mCC301B672339996E175F44CCA3FB24EB073F8E5B,
	fsData_get_AsDouble_m1DD0B96B2DFA32B05F306939A5844EED0B153464,
	fsData_get_AsInt64_m3DFDC6B68BAC884963A5445B1BC5A6CFECD07BB2,
	fsData_get_AsBool_mB8360A00381318F6BAA903A65302E628789A3255,
	fsData_get_AsString_m5D58DE99A58A981366E8592A18EE25E6B46BA6E7,
	fsData_get_AsDictionary_mDF9877487F2278358C4B94556633D11843715CA1,
	fsData_get_AsList_m5639D699A2C4E28959D3676934DC3B32DA28B193,
	NULL,
	fsData_ToString_m2BAE1989932AB7058E1A0AAAAF77D5D7BA2D9233,
	fsData_Equals_m699AB1A7424F56FBF99282CE1F610A6A3B98349C,
	fsData_Equals_mE6A7CC7EB494558C8ACE3BB80CD3BCF14C3F5BDB,
	fsData_op_Equality_m7551403EB676E0D232C048EE2063EF80BFB8DDDC,
	fsData_op_Inequality_mB28D25FC34D90D5E8447338C560A1A845E97056D,
	fsData_GetHashCode_m0116E244D126518483FA3699B7846EABDD076921,
	fsData__cctor_m73E6F5D149288399A43BD7847302425F01D2602A,
	NULL,
	fsDirectConverter__ctor_m97790897C194D24C858E342216E248590112F770,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	fsMissingVersionConstructorException__ctor_mDCA44EBEDC2E7BCB9C160D1794808C4C03297E7C,
	fsDuplicateVersionNameException__ctor_m26023CDF30C5CFE02E66CFF61A116F35FD3869B4,
	fsIgnoreAttribute__ctor_m7C69538D942D74FA7B41E9A097A6BBEA3EF3C55C,
	NULL,
	NULL,
	NULL,
	NULL,
	fsJsonParser_MakeFailure_m612A123C722F9875E72442B6A56FD798D9F9CDF4,
	fsJsonParser_TryMoveNext_m42AFAC37ED84902917CF00C9A9C33BDA8024D2DB,
	fsJsonParser_HasValue_m0AD646A8050F1CCDC5B4F61CBC1179F774049D0D,
	fsJsonParser_HasValue_m68F75FA6054CBC9EA104900970084B880AC42742,
	fsJsonParser_Character_m2B5EED96858028E4CE851B37EA5570AA7F797526,
	fsJsonParser_Character_m06FA063EDC83406FD4E75E617A7DC519A4FECC97,
	fsJsonParser_SkipSpace_m13CB76DE0B5D73D30CDDB33AA39EBEC81D0A6E48,
	fsJsonParser_IsHex_m711371C5FEBA48A0D73D96A1CFAF0741D106A6E6,
	fsJsonParser_ParseSingleChar_m525B152F178186173E1C79D68BEA60C1424D4B1C,
	fsJsonParser_ParseUnicode_m06AE25ACB11245DAB010109E0FD16C78D2365039,
	fsJsonParser_TryUnescapeChar_mCCA0DE32B3AAE324A873C67B50F1D9BF0704A172,
	fsJsonParser_TryParseExact_m369468D91045859027F0FBE15790B6D0331A43E3,
	fsJsonParser_TryParseTrue_m98139D5AB205298E8A505B88C2EB5BFCE9FBD80C,
	fsJsonParser_TryParseFalse_m3DCB93B7013A92F402E601DD54DF4A394283D54A,
	fsJsonParser_TryParseNull_mC5601B890CB0444D52AE694630D2069ACF42BF10,
	fsJsonParser_IsSeparator_m8A65CBF3058C4C5434A75522037EC7FDDF5CC2F3,
	fsJsonParser_TryParseNumber_m741A678D1849D6624BD9A9627B3F3E63A3527711,
	fsJsonParser_TryParseString_m19F4E21155FC9074FCA92A09566482B4ED174CE8,
	fsJsonParser_TryParseArray_m10047D56B043C1BA67560571BD3DC50EF7293537,
	fsJsonParser_TryParseObject_m73C7F4A7320508BF673B9F07EA01BCE0D83EE064,
	fsJsonParser_RunParse_m86B44D912E5925CDD6C69450BAE78B69F057D440,
	fsJsonParser_Parse_mE36DEC1FECE1891331BB0DC82C45EAA85765F295,
	fsJsonParser_Parse_m10D50DC78837EE0BA8088085562AD41FD615B769,
	fsJsonParser__ctor_mB0750A7C76FC1104BBF901893C7C91434930EEDB,
	fsJsonPrinter_InsertSpacing_m3F878957FF0526D0A0FC72A3986840C71E956D26,
	fsJsonPrinter_EscapeString_m444589C7FFF33F320026A99FC25F6D8743EDEE44,
	fsJsonPrinter_BuildCompressedString_m95FCFCD70D26F0FDC76AB6686A89C4FE15272BB4,
	fsJsonPrinter_BuildPrettyString_mF5E14807E45BE6897DF39F403A01CA869FD86553,
	fsJsonPrinter_PrettyJson_m35525633CE381A97328383ABBA16C576AFABD682,
	fsJsonPrinter_CompressedJson_mCEE98FB0A27EDB5ABDF58E6F3DDFBAD70B5D536A,
	fsJsonPrinter_ConvertDoubleToString_m1406E1C1D733C25125064D96A38F2B5E2857F4BE,
	fsObjectAttribute__ctor_m08EC26A024C177A584D0185203E2F888E03EED3C,
	fsObjectAttribute__ctor_m6F39B6439ECB2102676B8AE891DAD0C14A6615E9,
	fsObjectProcessor_CanProcess_m2EF960183524FA90FDE56F7F79E86E4144F44389,
	fsObjectProcessor_OnBeforeSerialize_mF97CC0F2F2A163AAFF86BF522B039B34A3CD5C6B,
	fsObjectProcessor_OnAfterSerialize_mF1B5BFA5E5CBF6244AA2D704A17FCE4645E25CA9,
	fsObjectProcessor_OnBeforeDeserialize_m1A73B45EF8646F9C01B3CA43734252160B5B8F6C,
	fsObjectProcessor_OnBeforeDeserializeAfterInstanceCreation_m753C7410A1E54DEE741958749BCB26511EA27DA0,
	fsObjectProcessor_OnAfterDeserialize_m10E2A98F03D60EFB4BA9938540800140B4433696,
	fsObjectProcessor__ctor_m721DF9C53412F9ADA8EA59591A26C9FB216A7196,
	fsPropertyAttribute__ctor_m6C06F09C709F250D1BF6668DFF5D92ADE6F15AE2,
	fsPropertyAttribute__ctor_mC5D2ABE1EABB17F625A6F392140CAC4371830D7E,
	fsResult_AddMessage_mDAAC5F5664BAD0975CEB87CB78F57840F9ECA07A_AdjustorThunk,
	fsResult_AddMessages_mB0BFF0A53D4E67FD22928A3AA90FCEC7C24BACAC_AdjustorThunk,
	fsResult_Merge_mCA43BA4A0C84F86B1C741B870A36DE64990EE316_AdjustorThunk,
	fsResult_Warn_m441CA3313DDEC3881842B39F80CFACC77DEEB3DC,
	fsResult_Fail_m53DFE0A162FEBB169CA8351746E3358DF684C202,
	fsResult_op_Addition_m4D6866C260A2A17083DF96D3BA88B172E279A8EA,
	fsResult_get_Failed_m9C792FBD2658DA77759BE224A949470679F99074_AdjustorThunk,
	fsResult_get_Succeeded_m5F05E03DF89C8C09B064B4DBFC94705F1049D19C_AdjustorThunk,
	fsResult_get_HasWarnings_mAEA08C91A4C28DF287C1C77AC89957D555F44682_AdjustorThunk,
	fsResult_AssertSuccess_m75D00F1F4EAE7C15386F766E9FFB2A2FE3018B18_AdjustorThunk,
	fsResult_AssertSuccessWithoutWarnings_m4F6BF6FDE3A244CFED2CAF6B098174B2AB7891C5_AdjustorThunk,
	fsResult_get_AsException_mF123DDA70FE30796B88229EDD88E4675A1D5A323_AdjustorThunk,
	fsResult_get_RawMessages_mAE4B17BA4E72973DB0221595A19CAC56C7390521_AdjustorThunk,
	fsResult_get_FormattedMessages_mE62818C25A5F30B17E6D4E40A065B78C0C83DD1E_AdjustorThunk,
	fsResult__cctor_m40CAFD0891A35C76E7195E6A1F02B046BA9C0030,
	fsSerializer__cctor_m0B3EE9E67511EA8D1983BC4CB238CACD48C86BFE,
	fsSerializer_IsReservedKeyword_m25F3FE6BD8A6CAE492CEC661B52BB1AD9E014BB4,
	fsSerializer_IsObjectReference_mC1A653A92438E92B526CEB6E205A3BA71B0A9B71,
	fsSerializer_IsObjectDefinition_m80D08FE5248C23E7DC9286FD7C3A21A2AB50D888,
	fsSerializer_IsVersioned_mD27D19963C5148F9082DB848414059B6DE4E9763,
	fsSerializer_IsTypeSpecified_mD254FE00C93CB9FB2AE01E5BF5F916EC667F224E,
	fsSerializer_IsWrappedData_mCCC3F4731B3563FC4811C63A0E5A4B3E4CFDAB4C,
	fsSerializer_ConvertLegacyData_m58FA7B22FBC34FFE6240BFA650935D4CDF3770DF,
	fsSerializer_Invoke_OnBeforeSerialize_m33007E2C939E91FD36C3370E99EC5CF42FE191B7,
	fsSerializer_Invoke_OnAfterSerialize_mFF3B979FD2B5E838C133BF8C70822551D688AE01,
	fsSerializer_Invoke_OnBeforeDeserialize_mFD63E50B79BAC8CC936043F0175FB0C649EAE83B,
	fsSerializer_Invoke_OnBeforeDeserializeAfterInstanceCreation_mE1009D54923B4846F7C90252924217F19470B065,
	fsSerializer_Invoke_OnAfterDeserialize_mB5D30C6CE065F4FF4B0A76A0B76AD16929363BA6,
	fsSerializer_EnsureDictionary_m94CE569EF6B6AEF509BA5A0BB5400703372C9D8A,
	fsSerializer_RemapAbstractStorageTypeToDefaultType_m22A9C7048D23CAD809A884F9FEAE4D5C2F1C5D75,
	fsSerializer__ctor_mDCC774233DFDA009C15E222E01A2B8CFBD763348,
	fsSerializer_SetDefaultStorageType_m4A3C49D785CFF8E743DBFD1F226CB63F43317B6C,
	fsSerializer_GetProcessors_mDAF7C82F1ABFFF195B03D598EA344FEF462536BB,
	fsSerializer_AddConverter_mF2138B22425127876B9EC6DAF1C7E9292642A3C9,
	fsSerializer_GetConverter_m0356C48F956FDE808C5658A284EB82A6F81DFBD2,
	NULL,
	NULL,
	fsSerializer_TrySerialize_mDEBE5C853C6A5709B814EE1657A944CBCE83BD74,
	fsSerializer_TrySerialize_m30AB528A4F96321F879C0591A27DC940D4523D52,
	fsSerializer_InternalSerialize_1_ProcessCycles_m749503133D842B3489A34F23D844D5D6A08E1D1E,
	fsSerializer_InternalSerialize_2_Inheritance_m45860AE5AD5BD2B35DED9B15D42E502EE413D35D,
	fsSerializer_InternalSerialize_3_ProcessVersioning_m284865BFDBBBA993E48765D77371B806609A63F6,
	fsSerializer_InternalSerialize_4_Converter_m9322EC99B5E3B68C5E4AE75AB9447961141111A9,
	fsSerializer_TryDeserialize_m5D8B5415536E3C941E8AB295157C3351BDC68CD7,
	fsSerializer_TryDeserialize_m9CFBD1374B1817BFAD920FAFCBF9411357B9A504,
	fsSerializer_InternalDeserialize_1_CycleReference_m22D8297763917BEB6A769507DCA3ACCA7C931FA2,
	fsSerializer_InternalDeserialize_2_Version_m313AA0DE8D76CE18973A7707DC98A8183B1C5D4C,
	fsSerializer_InternalDeserialize_3_Inheritance_mC55306E74111FB61DEA4BDEE3CAD4A2A22DD76EA,
	fsSerializer_InternalDeserialize_4_Cycles_m7BA0BA3490976189D09A3584C39B6C4CB23FBCCD,
	fsSerializer_InternalDeserialize_5_Converter_mFC4B8FBBF3801524D2BC33277BDC155307B1D9F4,
	fsLazyCycleDefinitionWriter_WriteDefinition_mC35395DBE40534AA3582CEC7A4919373B0E54970,
	fsLazyCycleDefinitionWriter_WriteReference_m14D0337B69F0D1B4E8547A10E08BD75FEF4A5F6C,
	fsLazyCycleDefinitionWriter_Clear_mB21696F24CA545B5BF1D493EE84035ED22137E15,
	fsLazyCycleDefinitionWriter__ctor_mC16AD1D5B8BE78D8E8337D809D5D5C1C7866009D,
	fsTypeExtensions_CSharpName_m329D538830145F7DE3ED45C4C2CC162E704E9418,
	fsTypeExtensions_CSharpName_m0F73C266C9363A86384AA3713D5EAA6DFE7D1CE9,
	fsTypeExtensions_IsInterface_m260C783B1B5AA4E53899A70615D8924C88EFAA4E,
	fsTypeExtensions_IsAbstract_m91D0B4C01902D0B0A7F66F37718397BF4C03AD4E,
	fsTypeExtensions_IsGenericType_mAD3AEA81AEEA2D64B6C2C2EFECCC9D890B4ED519,
	U3CU3Ec__DisplayClass2_0__ctor_mF043F098B7C9413209A3F73786F7189957631CFD,
	U3CU3Ec__DisplayClass2_0_U3CCSharpNameU3Eb__0_m7A828679557446316814E42D2A4C161D142FFACB,
	fsMetaType_Get_m2AE5A33AC1592854E7F756628DCE5B6A832D7D35,
	fsMetaType__ctor_m11EF37FF04A3B0E49E1991F95236C6D33A65BB2E,
	fsMetaType_CollectProperties_m78426CD58E6407C7B861ED637124EB5CC2F10F5A,
	fsMetaType_IsAutoProperty_mA44D45C2B218B577C1D959F806289A686ECB2B1D,
	fsMetaType_CanSerializeProperty_m8187E14736132D253305EB6AA59BB4C67C6E5B07,
	fsMetaType_CanSerializeField_m053BD9A9C623E273680401E6EDCA4B5B5A4022F1,
	fsMetaType_EmitAotData_mE098781B57B0F12A8EB5F534F8435D6632921084,
	fsMetaType_get_Properties_mF2C3B7E8407690D2A3805DDFBC65A24DE90B4356,
	fsMetaType_set_Properties_mBF98B3B352C40881052A927BE6CCD3FE62541EC3,
	fsMetaType_get_HasDefaultConstructor_mAE8A9AAD9C54CA90618A43DCBF9DB67C2717A551,
	fsMetaType_get_IsDefaultConstructorPublic_mFBE8E77BFC92A56CA3036ADA5EF7DEF73B625C09,
	fsMetaType_CreateInstance_m58CC0A6520E4D1761A2433A22AC312479E8D019F,
	fsMetaType__cctor_m700E39EAC99BA6FE1AFCBC2E2FBC2443BE6B654F,
	AotFailureException__ctor_m9E351420347AAE80A058010D8F3C4161E78A3CA8,
	U3CU3Ec__DisplayClass5_0__ctor_m29773E469E6CF1B64873718C2908B737DB740527,
	U3CU3Ec__DisplayClass5_0_U3CCollectPropertiesU3Eb__0_m3A5AF42B3400B8F0DCBC01B3A111B5D63D201E35,
	U3CU3Ec__DisplayClass5_0_U3CCollectPropertiesU3Eb__1_m3125FA2755F2F81376E4DAC56D72FA257DB52329,
	U3CU3Ec__DisplayClass5_0_U3CCollectPropertiesU3Eb__2_m508E450F935FE76DE616E89F7E4036B27EA9DBC0,
	U3CU3Ec__DisplayClass7_0__ctor_mBE65B941CD688255AA98653EFC92BA989F51B3A5,
	U3CU3Ec__DisplayClass7_0_U3CCanSerializePropertyU3Eb__0_m5D39FA95DCA4A822876CD9CEA936D0DC91FD0312,
	U3CU3Ec__DisplayClass7_0_U3CCanSerializePropertyU3Eb__1_m30EDF83500A004B4E1CE55944144380509C33C0B,
	U3CU3Ec__DisplayClass8_0__ctor_mD9BD3C025632AB89805EB36943E9AF92D91F9073,
	U3CU3Ec__DisplayClass8_0_U3CCanSerializeFieldU3Eb__0_m5EE4B6D0A4ADD655A52B6ADE4082795B5C3C22CC,
	U3CU3Ec__DisplayClass8_0_U3CCanSerializeFieldU3Eb__1_m36D291B112353E30A5B42241B1CE68DF7D7D54C3,
	fsArrayConverter_CanProcess_mDA9196F37D4A12B611F2AE459897E82C48004D07,
	fsArrayConverter_RequestCycleSupport_mEDB81088E7D59A6CB2895D2F550746F39F0A8E2B,
	fsArrayConverter_RequestInheritanceSupport_m30290179FB2AC8B2D0EBDE6A35B2A764AC2FC4AF,
	fsArrayConverter_TrySerialize_m4C1165BC2B8D6D28CB6E9AD21EF0F630D37D8A8C,
	fsArrayConverter_TryDeserialize_m38FE0EAF5BB5CACB6B80A20BDBA21DFAF26B1941,
	fsArrayConverter_CreateInstance_mAFD196BBFC6699F76F8A94771AB519EF467BA76F,
	fsArrayConverter__ctor_mD8C8CFF409F0ACD30ADF2441DC11A5B82AB9E7AA,
	fsDateConverter_get_DateTimeFormatString_mBEEE108FC4646A0DC5B062B07C569263121D56A7,
	fsDateConverter_CanProcess_m79A33B10FA608FF79B413B08D772992B6DB7D546,
	fsDateConverter_TrySerialize_mC39BB16A68E067E7087BD33696219B41D2A58ED5,
	fsDateConverter_TryDeserialize_mD6FCF549AF74E383C9D70BB84DFBBF5E80ABD49D,
	fsDateConverter__ctor_m8E38DD596A478F128642ACF5B300D6635FABC32A,
	fsDictionaryConverter_CanProcess_m89E8B29E93416FD1D1F27CBDA188D43D2DD69B4B,
	fsDictionaryConverter_CreateInstance_m221116A0AB0A93C259B9844C2D86084F10C793C6,
	fsDictionaryConverter_TryDeserialize_m9E370CCC4270DB6E2332F2B61C13DF99FE5B8D28,
	fsDictionaryConverter_TrySerialize_m3EA99C3B4C0A4C201EACBAB9F6A121F8360CBE8C,
	fsDictionaryConverter_AddItemToDictionary_m5780744D2E9A544BF108093E2C222FCE7B039763,
	fsDictionaryConverter_GetKeyValueTypes_mE8343E439747221EC907412B2048FAB6F7E03BA3,
	fsDictionaryConverter__ctor_mFBEE65904B2D19F828A5160610CA219AA6E6F81B,
	fsEnumConverter_CanProcess_m2CC1B27E327C14C78C3722776BC595D3E0D5D734,
	fsEnumConverter_RequestCycleSupport_mCD0D4D34A8C8542AB472D8B13C9F5857384D931C,
	fsEnumConverter_RequestInheritanceSupport_mC8C24DADB1DCBEBC241D1CBF207C58EFF4D6AD71,
	fsEnumConverter_CreateInstance_mCC77C55577AC4E4D201EE10E6D648AC6C251EBA2,
	fsEnumConverter_TrySerialize_m7D7886681B17167DC0B1D28EAB047D954BC2C5A5,
	fsEnumConverter_TryDeserialize_mCE91043CA547D5BE77B3487EF9D8FA7E74486F6E,
	NULL,
	fsEnumConverter__ctor_mD39F5DACA8CF34DE1849C08F7259C2BE3975C67C,
	fsForwardConverter__ctor_m3122D771F4B68D939408DBD69AFA85225D12E8D7,
	fsForwardConverter_CanProcess_m436A5F71B9A5D022FB54E1C0D1C10B25CD45330D,
	fsForwardConverter_GetProperty_m8B0D2BDCF491DD8D99730141053CE777F3F6A5E5,
	fsForwardConverter_TrySerialize_m282D0F38E1D37D4823F78CFD30A3D80ADF5A0F09,
	fsForwardConverter_TryDeserialize_m0EC388A75980903DD116A5D141B36A432B703C7F,
	fsForwardConverter_CreateInstance_m821856F88EAF66372CD983154860E666B2480CE7,
	fsGuidConverter_CanProcess_m7AD44FE61099E61A8FFCE9F649D1761242AD4000,
	fsGuidConverter_RequestCycleSupport_m88A1D5EF5DF4EFB68B2FB41E50446D1C7A1E45F5,
	fsGuidConverter_RequestInheritanceSupport_mD69648D56EF07D1BC41986DBAC3D9BE9AAA1D44E,
	fsGuidConverter_TrySerialize_m86FB7F063BDE8879AB48190A8E2DF474B5FEFB52,
	fsGuidConverter_TryDeserialize_mBB1593E9F72B658DCEC48BE58596896DB76B9036,
	fsGuidConverter_CreateInstance_m624F31F5BAA34E5AB181C110ACF1C49060DFD7A0,
	fsGuidConverter__ctor_m3D21AF16315254F14B35164F028DD87830450690,
	fsIEnumerableConverter_CanProcess_mF332478FDF9DEA1ACAD8274E40DA4C3DBEF6616F,
	fsIEnumerableConverter_CreateInstance_m8E890EB30EBB661A20B913B240ADFD3050BC46F0,
	fsIEnumerableConverter_TrySerialize_mD370459F57194F2242B25CEE289A3D823F93AF68,
	fsIEnumerableConverter_IsStack_m70C317AC78BBF149B37CAC7654D42A9528F5D354,
	fsIEnumerableConverter_TryDeserialize_m25F35628ED115C7751F9E39B4761ED6D62913020,
	fsIEnumerableConverter_HintSize_m6D545FCAC3FBFC6AC6658FDA2982408A9AEB5B30,
	fsIEnumerableConverter_GetElementType_m6D7F1EE4FC5095E740015A1D3DBDD0EBFC6D3D5A,
	fsIEnumerableConverter_TryClear_mC15556022454D865C459E4A5228E372132DF2B61,
	fsIEnumerableConverter_TryGetExistingSize_m6C91B5DBFF946A8101FFD372211A1BC03CB567FC,
	fsIEnumerableConverter_GetAddMethod_m492687B961C7780CBACE58DCC49183EADE31697F,
	fsIEnumerableConverter__ctor_mA31125C0A06077DA32216D56B612E4CE55A01B91,
	fsKeyValuePairConverter_CanProcess_mD5C0A501C9605550681D72AE8937CFF841E3D470,
	fsKeyValuePairConverter_RequestCycleSupport_mBB3EE2138663A620953BEB26CB7061D64C65C768,
	fsKeyValuePairConverter_RequestInheritanceSupport_m04CBDAF7516E408AD938FD20A00BACC759CA27D8,
	fsKeyValuePairConverter_TryDeserialize_m6E8B1E202DFEE0BCF1665D69D6D70ADD8AA199AF,
	fsKeyValuePairConverter_TrySerialize_m2CC70BF863CF20EF06A5463A4A38C8A01B97CE83,
	fsKeyValuePairConverter__ctor_mEF52DAC721E15BFAD57B4BBC6A95722750769551,
	fsNullableConverter_CanProcess_m5AD935506F8D8B3F032EFC1AF6313EADBB639B43,
	fsNullableConverter_TrySerialize_mE518482C071C0E2B9C3466D520E6D2A1DCCB0371,
	fsNullableConverter_TryDeserialize_m5E6D9E4498F91F649CAB1D62127CCDBC47B281F9,
	fsNullableConverter_CreateInstance_m0CA3A71214FB0CF5C5B74F01470AB554B7FE10A2,
	fsNullableConverter__ctor_m0AF68115DBA4C368E58DADC1C537B88C1653552A,
	fsPrimitiveConverter_CanProcess_m116C5AF57DA7FA90D89842880E88226FDA70C2FF,
	fsPrimitiveConverter_RequestCycleSupport_m8B7A1DF8A9DB84DE3C841976F815B0F53D9EF85D,
	fsPrimitiveConverter_RequestInheritanceSupport_mEC8C785E28075BDFFF9E3B05F1909BCA0B8552B9,
	fsPrimitiveConverter_UseBool_mBDBC77DA32FCC70B8364A833FC6FEDE9F518BBE4,
	fsPrimitiveConverter_UseInt64_mC7CA889263AD304472FB57F3552A61A58AC12DCD,
	fsPrimitiveConverter_UseDouble_mC19B43880692E6F2BCF687D29080577F4DC0BFD9,
	fsPrimitiveConverter_UseString_m3B727CA46D206F785A72E303048A713C8791CA0C,
	fsPrimitiveConverter_TrySerialize_m0368752F59C6E171971B06A63B2E1D1550F7DF26,
	fsPrimitiveConverter_TryDeserialize_mB945799F8479FDD03CBB1644064AF3DB962DCC96,
	fsPrimitiveConverter__ctor_mCF9C078AF65749200BF02E125475BF99DD4CCFF3,
	fsReflectedConverter_CanProcess_m1C7097A1FE9B31DBD1BC3F32703082C8C71B2189,
	fsReflectedConverter_TrySerialize_m860FBBDE2A4A1187AA16717025710676DF8AF737,
	fsReflectedConverter_TryDeserialize_mC1BB2F7321306B1A9D2A8BDA3D4A3A5FD39E0547,
	fsReflectedConverter_CreateInstance_m7C340379381192064EEF7C11F30A4BEB8CB7DE5E,
	fsReflectedConverter__ctor_m0C320BAF8D6D0D83BA02BBD481731A1B4D62F876,
	fsTypeConverter_CanProcess_m3A8444470D10B2E651E16999F3F80D9D3C613590,
	fsTypeConverter_RequestCycleSupport_m71319471317A38508ADDBE2BC42E7A4267492C6C,
	fsTypeConverter_RequestInheritanceSupport_mE3ACFAAE1FD9C16F93D251CEE5A51C0A508C4661,
	fsTypeConverter_TrySerialize_mEA024B54FE9A829B240F64A6B9FB00682A440004,
	fsTypeConverter_TryDeserialize_mE91A3B3358CA002E0EC9F64818F4C04FF065A7CA,
	fsTypeConverter_CreateInstance_mB24C7F3B762541C83DA1206E67E6C6AC0E49D839,
	fsTypeConverter__ctor_mDD227968F22A01786684FC69935B89894DEA2D5C,
	fsWeakReferenceConverter_CanProcess_m1CD7B3E14C7C36F9347DBEF6CA2953E99A57E5D4,
	fsWeakReferenceConverter_RequestCycleSupport_m4C42171E8AFCEFC06C609C6BB43245C1B1F951BA,
	fsWeakReferenceConverter_RequestInheritanceSupport_m70C634B09BA5043F44043723F35EB540F6BFE8A7,
	fsWeakReferenceConverter_TrySerialize_mA5D0012B90EB8F7E985827178BFFF489536CC563,
	fsWeakReferenceConverter_TryDeserialize_mD6399A749ADA73471D7F1BA8770F31DE1793FACE,
	fsWeakReferenceConverter_CreateInstance_m86A716E05DF20BE5974EF1ACDD1085F94AAF6D35,
	fsWeakReferenceConverter__ctor_m533DB1F2512F46B6139FD3B3AE233DCB7623C4FE,
	fsSerializationCallbackProcessor_CanProcess_mB20A08DBF8C0D0CE6B736B1A9091F3B61C48C105,
	fsSerializationCallbackProcessor_OnBeforeSerialize_m8E9974D8C7548DC0B8B560CDD976E765E4D4FADE,
	fsSerializationCallbackProcessor_OnAfterSerialize_mDB4957BE4D7289ACC32ACF16231105F5E1666E27,
	fsSerializationCallbackProcessor_OnBeforeDeserializeAfterInstanceCreation_m5B9E14A3D05B8CEA9067C217E644CA2D12DBD824,
	fsSerializationCallbackProcessor_OnAfterDeserialize_mFB1C79367BF24E46B5ADDD7FC41B409144A3F0F5,
	fsSerializationCallbackProcessor__ctor_mBE9B03156E4E821247C8F0FAD3CD16497E64BC10,
	fsSerializationCallbackReceiverProcessor_CanProcess_m041F8D542F630F4852DA3CFF5CC2C8BD7F99BBBA,
	fsSerializationCallbackReceiverProcessor_OnBeforeSerialize_mD56FBF412F1C38A40A141DA56BBEE72A057DBCA1,
	fsSerializationCallbackReceiverProcessor_OnAfterDeserialize_mDAB98255E654770C62EBA7E44249562D3C98961E,
	fsSerializationCallbackReceiverProcessor__ctor_m5516C0B0D8B6CF935A2F906CAB48F8AE0B6004A0,
	fsCyclicReferenceManager_Enter_m58E6DDD0CBD151BEBDAAE9B0CCE5A77BE3F8FC04,
	fsCyclicReferenceManager_Exit_m3C0C0291572DA2A40200DABA32773F0EDA99FF92,
	fsCyclicReferenceManager_GetReferenceObject_m5E1B77539D1A797643ADCD715D1DDAFDB04E9836,
	fsCyclicReferenceManager_AddReferenceWithId_m858FAC1C2C579F8089C495E81C223475916D9C76,
	fsCyclicReferenceManager_GetReferenceId_mF3ACBFE709CACB9548C5CC1CF5E8137B89368BFF,
	fsCyclicReferenceManager_IsReference_mD2276E941273B839AF93CF9FEF7F078BD2AB5707,
	fsCyclicReferenceManager_MarkSerialized_m6FD82154B18CF301679234E6BAF97C7B42DB35D1,
	fsCyclicReferenceManager__ctor_mD4943AE432A11DE13F27DC617740B492BE944FD4,
	ObjectReferenceEqualityComparator_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_Equals_m11104763EC9F33C807338311AD2DCBFD13AFB3D3,
	ObjectReferenceEqualityComparator_System_Collections_Generic_IEqualityComparerU3CSystem_ObjectU3E_GetHashCode_m079FDA17639618D647E141539A47599D6D049F15,
	ObjectReferenceEqualityComparator__ctor_mB21EBD7D76738113CC3A12676D02DED7D3E79EB7,
	ObjectReferenceEqualityComparator__cctor_m07A51BEB553F802680C96CAE3A6485F9EFCF7AC7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	fsPortableReflection_HasAttribute_mC193D9B4B81117E1EE77FF8BAE5C85A86660EA7F,
	fsPortableReflection_HasAttribute_m8E68968BA00BB48075B32531EF9D60663BA766E4,
	fsPortableReflection_GetAttribute_m53782C927761C4FBCE5C1FA9AC00B5B09EBCB747,
	NULL,
	NULL,
	fsPortableReflection_GetDeclaredProperty_m5EFE34BADF45559AA2ECF8085DE9CDB79F2898AA,
	fsPortableReflection_GetDeclaredMethod_m39C7E61BAE99B3ECC6C8365854C3D477D67F9885,
	fsPortableReflection_GetDeclaredConstructor_mFBEC50CE1160B7A404137E45DB56EAA3CE1FE308,
	fsPortableReflection_GetDeclaredConstructors_m87013D2231E45F8357911CD57C60A71DFA7E329E,
	fsPortableReflection_GetFlattenedMethod_m1A7658BA15F4C7828B1EC4E4D0B0B42E270FF67F,
	fsPortableReflection_GetFlattenedProperty_mC547515DEF313C5215EF6D99504E6CE64C3A05B3,
	fsPortableReflection_GetDeclaredMethods_mD206EEABDA024B8DAE981AA6ACF7CEA351961E00,
	fsPortableReflection_GetDeclaredProperties_mBC31F9584DA0FC6281A97D0AB76E07DBBF599E13,
	fsPortableReflection_GetDeclaredFields_mBBCBA8A61D2377852B746388F8B05D15F353CBAF,
	fsPortableReflection_GetDeclaredMembers_mA60D39CFE264BA0C43D23BB6BDB9CFA46DC44648,
	fsPortableReflection_Resolve_m3210E900E0B4AADCC533E0B40EAC6CD20B3ED847,
	fsPortableReflection__cctor_m9EEEBD307F4EBF8CAE6A7FB25656B708FF2E62E1,
	AttributeQueryComparator_Equals_mFA7777007C909658410153E9261EE521AECE6BC3,
	AttributeQueryComparator_GetHashCode_m099ED57ADC98C03EC737FA1419ECF0EECDEF6094,
	AttributeQueryComparator__ctor_mCD4C24E7CEF33B43AD8A4BB8607096BB2E6905E1,
	fsVersionedType_Migrate_m439EC7E6E73C38BB4A269DA59B20FE22205A1831_AdjustorThunk,
	fsVersionedType_ToString_m8C4111D55FA21DE440FEC4488AB07E6F4B316AC5_AdjustorThunk,
	fsVersionedType_Equals_mA100885FB81984E2880836B09D5BD026887356BC_AdjustorThunk,
	fsVersionedType_GetHashCode_mAD0932F3F4306EBA1F5F0904DED77F990EE755F3_AdjustorThunk,
	fsVersionManager_GetVersionImportPath_m12B83D9F9F6E11C9C46C0A7283947373CA2FC14B,
	fsVersionManager_GetVersionImportPathRecursive_mB40DDC3D620C4303507F6B7ECAD65F06FD220C67,
	fsVersionManager_GetVersionedType_m54CA5F1A4A07313EAE276E690A9AED0BD688A2B8,
	fsVersionManager_VerifyConstructors_m23E752411E16CE828C25D9E58D4D2405B7A65243,
	fsVersionManager_VerifyUniqueVersionStrings_mA9FBB9959012D319F154399D1F877EA0994ED47D,
	fsVersionManager__cctor_m31C87DC0AE466A11DBA356F5E81DD9D81ECC2311,
	fsMetaProperty__ctor_m63917891A0F46B096287A7CBEB4AE4476B18FE34,
	fsMetaProperty__ctor_m78B20EE411B9BF09EE58955BF452691EDA36CE2C,
	fsMetaProperty_CommonInitialize_mB8DAA6A5C117B3F345C7E5E03DB0DAAFD792FB53,
	fsMetaProperty_get_StorageType_m269BF18DBE46AB8BCA6C5992E82C4A1BBB2F079B,
	fsMetaProperty_set_StorageType_mA7B8A6541BA41FBA0E4F6466B5663D86EAB90A70,
	fsMetaProperty_get_OverrideConverterType_m587F21F7CFFC5F81F642925F4D6764942D9887FE,
	fsMetaProperty_set_OverrideConverterType_m501045ADD8FBD138C85EB5A2D735876977210585,
	fsMetaProperty_get_CanRead_m83D837742308E46EA03B99B5662782E12AD297B5,
	fsMetaProperty_set_CanRead_m11615509F7E45E8B97A4505587E74D7842BD8B50,
	fsMetaProperty_get_CanWrite_mA432592BE154B5F7631344184EACE002819561A7,
	fsMetaProperty_set_CanWrite_m30273D87D256A748E0EF5711990AF2466DB1D90E,
	fsMetaProperty_get_JsonName_m2C9F17D4C28E0E092EA39C5621DAFE9D7C1BE94F,
	fsMetaProperty_set_JsonName_m9964BDDC354DB02C1D384F828E5FA2CF29C456BA,
	fsMetaProperty_get_MemberName_m9A4E46BD5B896D3A9C06DAD1CF38D909F53F9044,
	fsMetaProperty_set_MemberName_mE0A7D25E375D310C545F8399A8C7AF052341FF31,
	fsMetaProperty_get_IsPublic_mCFF5F3118FC99F7B0B214E3684CEECA88C58CA0B,
	fsMetaProperty_set_IsPublic_m483015FF435A2870B40BCC1E47637A44D2B606A3,
	fsMetaProperty_get_IsReadOnly_m9A12681B1A7E2B7B0BD90CD9196F438CCCF3EAA2,
	fsMetaProperty_set_IsReadOnly_m79407798A01B4B9922114DE9C49DDC35BECC3E2A,
	fsMetaProperty_Write_mE9503E7A4D405161D3349D5138C984F86AEEB87A,
	fsMetaProperty_Read_m5A3517EFEF236FC8A9EF5F99B3CF2AEAE65E5F30,
	fsReflectionUtility_GetInterface_m877F77A78B3D3D8C046CFAC6E866263395D9F9B1,
	fsTypeCache__cctor_mB905555273D9B7CA7D35BD573A4B355367911BA3,
	fsTypeCache_OnAssemblyLoaded_m42910E3CD4FBE67B3417605E924EEFD443987DEA,
	fsTypeCache_TryDirectTypeLookup_m0CD7F1B3D40F3E92CD80E441A8A281CC9BA5647C,
	fsTypeCache_TryIndirectTypeLookup_mF31900D05CD5DA246B7FDDC0C33994D554BC70E3,
	fsTypeCache_GetType_m2FCF8B5971C64DC8FDF6823CB1B43703A957B8CD,
	fsTypeCache_GetType_mEF9CFFB08266C359A8CE8B070E9A3841907827FF,
};
static const int32_t s_InvokerIndices[389] = 
{
	2343,
	142,
	3,
	832,
	2344,
	23,
	26,
	9,
	10,
	2345,
	14,
	2346,
	3,
	114,
	9,
	9,
	2347,
	2347,
	2348,
	2349,
	2350,
	2350,
	-1,
	-1,
	23,
	3,
	23,
	34,
	3,
	23,
	3,
	23,
	114,
	-1,
	-1,
	23,
	9,
	23,
	23,
	31,
	340,
	215,
	26,
	26,
	26,
	4,
	43,
	23,
	14,
	10,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	466,
	189,
	89,
	14,
	14,
	14,
	-1,
	14,
	9,
	9,
	142,
	142,
	10,
	3,
	14,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	27,
	214,
	23,
	26,
	644,
	644,
	26,
	2351,
	89,
	89,
	30,
	251,
	422,
	23,
	244,
	513,
	2352,
	2353,
	2351,
	2353,
	2353,
	2353,
	244,
	2353,
	2353,
	2353,
	2353,
	2353,
	2354,
	0,
	26,
	375,
	0,
	144,
	212,
	0,
	0,
	98,
	23,
	27,
	9,
	27,
	629,
	644,
	629,
	27,
	23,
	23,
	26,
	26,
	2355,
	2356,
	2357,
	2357,
	2358,
	89,
	89,
	89,
	2359,
	2359,
	14,
	14,
	14,
	3,
	3,
	109,
	109,
	109,
	109,
	109,
	109,
	17,
	203,
	2360,
	701,
	2360,
	203,
	168,
	6,
	23,
	27,
	28,
	26,
	114,
	-1,
	-1,
	2350,
	2361,
	2361,
	2361,
	2350,
	2350,
	2350,
	2361,
	2362,
	2362,
	2362,
	2361,
	2361,
	62,
	62,
	23,
	23,
	0,
	125,
	109,
	109,
	109,
	23,
	28,
	1,
	27,
	203,
	142,
	2363,
	222,
	31,
	14,
	26,
	89,
	89,
	14,
	3,
	26,
	23,
	9,
	9,
	9,
	23,
	9,
	9,
	23,
	9,
	9,
	9,
	9,
	9,
	2347,
	2347,
	114,
	23,
	14,
	9,
	2347,
	2347,
	23,
	9,
	114,
	2347,
	2347,
	2364,
	467,
	23,
	9,
	9,
	9,
	114,
	2347,
	2347,
	-1,
	23,
	26,
	9,
	2365,
	2347,
	2347,
	114,
	9,
	9,
	9,
	2347,
	2347,
	114,
	23,
	9,
	114,
	2347,
	9,
	2347,
	94,
	0,
	144,
	145,
	0,
	23,
	9,
	9,
	9,
	2347,
	2347,
	23,
	9,
	2347,
	2347,
	114,
	23,
	9,
	9,
	9,
	109,
	109,
	109,
	109,
	2347,
	2347,
	23,
	9,
	2347,
	2347,
	114,
	23,
	9,
	9,
	9,
	2347,
	2347,
	114,
	23,
	9,
	9,
	9,
	2347,
	2347,
	114,
	23,
	9,
	27,
	629,
	629,
	27,
	23,
	9,
	27,
	27,
	23,
	23,
	89,
	34,
	62,
	121,
	9,
	26,
	23,
	90,
	121,
	23,
	3,
	-1,
	-1,
	-1,
	-1,
	-1,
	142,
	222,
	221,
	-1,
	-1,
	1,
	1,
	1,
	0,
	1,
	1,
	0,
	0,
	0,
	0,
	0,
	3,
	2366,
	2367,
	23,
	28,
	14,
	9,
	10,
	2368,
	2369,
	2370,
	2371,
	2371,
	3,
	27,
	27,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	14,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	27,
	28,
	1,
	3,
	144,
	392,
	230,
	0,
	1,
};
static const Il2CppTokenRangePair s_rgctxIndices[13] = 
{
	{ 0x02000015, { 10, 4 } },
	{ 0x0200003B, { 21, 1 } },
	{ 0x06000017, { 0, 2 } },
	{ 0x06000018, { 2, 2 } },
	{ 0x06000022, { 4, 2 } },
	{ 0x06000023, { 6, 2 } },
	{ 0x06000040, { 8, 2 } },
	{ 0x060000A4, { 14, 2 } },
	{ 0x060000A5, { 16, 2 } },
	{ 0x060000EF, { 18, 3 } },
	{ 0x0600014B, { 22, 2 } },
	{ 0x0600014F, { 24, 2 } },
	{ 0x06000150, { 26, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[27] = 
{
	{ (Il2CppRGCTXDataType)1, 16318 },
	{ (Il2CppRGCTXDataType)2, 16318 },
	{ (Il2CppRGCTXDataType)1, 16320 },
	{ (Il2CppRGCTXDataType)2, 16320 },
	{ (Il2CppRGCTXDataType)1, 16339 },
	{ (Il2CppRGCTXDataType)2, 16339 },
	{ (Il2CppRGCTXDataType)1, 16340 },
	{ (Il2CppRGCTXDataType)2, 16340 },
	{ (Il2CppRGCTXDataType)2, 16346 },
	{ (Il2CppRGCTXDataType)1, 16346 },
	{ (Il2CppRGCTXDataType)1, 16351 },
	{ (Il2CppRGCTXDataType)2, 16351 },
	{ (Il2CppRGCTXDataType)3, 49199 },
	{ (Il2CppRGCTXDataType)3, 49200 },
	{ (Il2CppRGCTXDataType)1, 16374 },
	{ (Il2CppRGCTXDataType)2, 16374 },
	{ (Il2CppRGCTXDataType)2, 16376 },
	{ (Il2CppRGCTXDataType)1, 16376 },
	{ (Il2CppRGCTXDataType)3, 49201 },
	{ (Il2CppRGCTXDataType)2, 27506 },
	{ (Il2CppRGCTXDataType)3, 49202 },
	{ (Il2CppRGCTXDataType)3, 49203 },
	{ (Il2CppRGCTXDataType)2, 16461 },
	{ (Il2CppRGCTXDataType)3, 49204 },
	{ (Il2CppRGCTXDataType)1, 16464 },
	{ (Il2CppRGCTXDataType)2, 16464 },
	{ (Il2CppRGCTXDataType)3, 49205 },
};
extern const Il2CppCodeGenModule g_FullSerializerU20U2DU20UnityCodeGenModule;
const Il2CppCodeGenModule g_FullSerializerU20U2DU20UnityCodeGenModule = 
{
	"FullSerializer - Unity.dll",
	389,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	13,
	s_rgctxIndices,
	27,
	s_rgctxValues,
	NULL,
};
